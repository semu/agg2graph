/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.matching;

import de.fub.agg2graph.distances.EuclideanDistance;
import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.structs.GPSPoint;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * 
 * @author Sebastian Müller sebastian.mueller@fu-berlin.de
 */
public class PointMatchingTest {

	@Before
	public void setUp() {
		ResourceManager.envOK();
	}

	@Test
	public void testInput() throws URISyntaxException, JAXBException, Exception {
		PointMatching match = new BruteForcePointMatching();
		System.out.println("BruteForcePointMatching:");
		evaluateMatching(match);
		match = new RTreePointMatching();
		System.out.println("RTreePointMatching:");
		evaluateMatching(match);
	}

	private void evaluateMatching(PointMatching match)
			throws ParserConfigurationException, SAXException, IOException {
		File f1 = new File("test/input/berlin/1058439-0001.gpx");
		File f2 = new File("test/input/berlin/1199499-0001.gpx");
		File f3 = new File("test/input/berlin/1206317-0001.gpx");
		File f4 = new File("test/input/berlin/page0000-0004.gpx");
		File f5 = new File("test/input/berlin/page0000-0002.gpx");

		System.out.println(System.currentTimeMillis());

		List<GPSPoint> list1 = GPXReader.getOrderedPoints(f1);
		List<GPSPoint> list2 = GPXReader.getOrderedPoints(f2);
		List<GPSPoint> list3 = GPXReader.getOrderedPoints(f3);
		List<GPSPoint> list4 = GPXReader.getOrderedPoints(f4);
		List<GPSPoint> list5 = GPXReader.getOrderedPoints(f5);

		long timeBefore = System.currentTimeMillis();
		
		List<GPSPoint> tra = list1;
		List<List<GPSPoint>> agg = new ArrayList<List<GPSPoint>>();
		agg.add(list2);
		agg.add(list3);
		agg.add(list4);
		agg.add(list5);
		
		TreeMap<GPSPoint, GPSPoint> matches = match.match(agg, tra, new EuclideanDistance(), 50);
				
		long time = System.currentTimeMillis() - timeBefore;

		System.out.println(time);
	}
}
