/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.boundary.Point;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.WptType;
import java.io.File;
import java.math.BigDecimal;
import javax.xml.bind.JAXBException;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static java.lang.Math.sqrt;

/**
 *
 * @author doggy
 */
public class TestProjection {
    GpxType gpx;
    
    GPSkPoint southWest, northEast;
    
    JXPathContext context;
    
    @Before
    public void setUp() throws JAXBException{
        FileImport fi = new FileImport();
        gpx = fi.parseFile(new File("test/gpsk/projectionTest01.gpx"));
        context = JXPathContext.newContext(gpx);
        
        southWest = Point.fromGPX((WptType) context.getValue("//wpt[name='southWest']"));
        northEast = Point.fromGPX((WptType) context.getValue("//wpt[name='northEast']"));
       
    }
    
    @Test
    public void testUTM(){
        UTMProjection proj = new UTMProjection(southWest, northEast);
        GPSkPoint p1 = Point.fromGPX((WptType) context.getValue("//wpt[name='Point01']"));
        proj.setProjection(p1);
        
        GPSkPoint p2 = Point.fromGPX((WptType) context.getValue("//wpt[name='Point02']"));
        proj.setProjection(p2);
        
        assertThat(p1.getEast(), is(lessThan(p2.getEast())));
        
        TrigonometricDistance td = new TrigonometricDistance();
        double d1 = td.calculate(p1, p2);
        
        double diffEast = p1.getEast() - p2.getEast();
        double diffNorth = p1.getNorth() - p2.getNorth();
        
        double d2 = sqrt((diffEast * diffEast) + (diffNorth * diffNorth));
        
        assertThat(d1, is(closeTo(d2, 10)));
        
        
        
    }
}
