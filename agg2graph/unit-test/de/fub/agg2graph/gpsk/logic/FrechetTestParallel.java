/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.boundary.Segment;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.RteType;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import javax.xml.bind.JAXBException;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 *
 * @author doggy
 */
@RunWith(Parameterized.class)
public class FrechetTestParallel {
    
    GpxType gpx;
    FrechetDistance fd;
    
    GPSkSegment seg01, seg02, seg03, seg04;
    
    @Parameterized.Parameters
    public static Collection<Object[]> params() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        return FrechetInitializer.getDistancesAsParam();
    }
    
    public FrechetTestParallel(FrechetDistance fd){
        this.fd = fd;
    }
    
    @Before
    public void setUp() throws JAXBException{
        FileImport fi = new FileImport();
        gpx = fi.parseFile(new File("test/gpsk/frechetPara01.gpx"));
        
        JXPathContext context = JXPathContext.newContext(gpx);
        UTMProjection p = new UTMProjection(52, 13, 54, 14);
        RteType track01 = (RteType) context.getValue("//rte[name='track01']");
        RteType track02 = (RteType) context.getValue("//rte[name='track02']");
        RteType track03 = (RteType) context.getValue("//rte[name='track03']");
        RteType track04 = (RteType) context.getValue("//rte[name='track04']");
        
        seg01 = Segment.fromGPX(track01);
        seg02 = Segment.fromGPX(track02);
        seg03 = Segment.fromGPX(track03);
        seg04 = Segment.fromGPX(track04);
        
        FrechetInitializer.setProjection(seg01);
        FrechetInitializer.setProjection(seg02);
        FrechetInitializer.setProjection(seg03);
        FrechetInitializer.setProjection(seg04);
    }
    
    @Test
    public void testParallel01(){
        
        
        for(int i = 0; i <= 1500; i += 15){
            boolean t1_to_t2 = fd.isInFrechetDistance(seg01, seg02, i);
            boolean t2_to_t1 = fd.isInFrechetDistance(seg02, seg01, i);
            assertEquals("frechet distance delivers different results, "
                    + "if you test the distance between one track to "
                    + "another and otherwise. " 
                    + fd.getClass().getSimpleName(), t1_to_t2, t2_to_t1);
            
            
            boolean t1_to_t3 = fd.isInFrechetDistance(seg01, seg03, i);
            
            if(t1_to_t3){
                assertTrue(fd.getClass().getSimpleName()
                        + ": It says that the distance from t1 to t3 is OK, "
                        + "but for t1 to t2 not",
                        t1_to_t2);
            } 
            if(!t1_to_t2){
                assertFalse(fd.getClass().getSimpleName()
                        + ": The frechet distance between t1 and t2 isn't OK, but between t1 and t3",
                        t1_to_t3);
            }
        }
    }
    
    @Test
    public void testParallel02(){
        for(int i = 0; i <= 1500; i += 20){
            boolean t1_to_t4 = fd.isInFrechetDistance(seg01, seg04, i);
            boolean t4_to_t1 = fd.isInFrechetDistance(seg04, seg01, i);
            assertEquals(fd.getClass().getSimpleName() 
                    + ": the distances work not vice versa for "
                    + "track01 and track04",
                    t1_to_t4,
                    t4_to_t1);
            
            boolean t3_to_t4 = fd.isInFrechetDistance(seg03, seg04, i);
            if(t1_to_t4){
                assertTrue(t3_to_t4);
            }
            if(!t3_to_t4){
                assertFalse(t1_to_t4);
            }
        }
    }
    
    @Test
    public void testParallel03(){
        //the frechet distance must be more than 5 meters
        assertFalse(fd.isInFrechetDistance(seg01, seg03, 5));
        //the frechet distance must be within 1000 meters
        assertTrue(fd.isInFrechetDistance(seg01, seg03, 1000));
        
        assertTrue(fd.isInFrechetDistance(seg01, seg02, 500));
        
        assertFalse(fd.isInFrechetDistance(seg01, seg02, 0));
    }
}
