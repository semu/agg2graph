/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Christian Windolf
 */
@RunWith(Parameterized.class)
public class FrechetTest {
    private static List<GPSkSegment> testSegments = new LinkedList<>();
    
    private static GPSkEnvironment crossing01;
    
    private FrechetDistance fd;
    
    public FrechetTest(FrechetDistance fd){
        this.fd = fd;
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> params(){
        Object[][] fds = new Object[][]{
            {new JensFrechet()},
            {new JTSDistance()},
            {new HausdorffDistance()},
            {new WindolfFrechet()}
        };
        return Arrays.asList(fds);
    }
    

    @BeforeClass
    public static void setUp() throws JAXBException, Exception{
        GPSkPoint origin = new GPSkPoint(52.523070, 13.325989);
        
        
        //Turmstraße. Abschnitt zwischen Beusselstraße und Oldenburger Straße
        GPSkSegment seg01 = new GPSkSegment();
        seg01.add(new GPSkPoint(52.527483, 13.328821, origin));
        seg01.add(new GPSkPoint(52.527170, 13.330066, origin));
        seg01.add(new GPSkPoint(52.527104, 13.331139, origin));
        seg01.add(new GPSkPoint(52.526909, 13.332727, origin));
        seg01.add(new GPSkPoint(52.526896, 13.333220, origin));
        
        GPSkSegment seg02 = new GPSkSegment();
        seg02.add(new GPSkPoint(52.527052, 13.333328, origin));
        seg02.add(new GPSkPoint(52.527131, 13.332748, origin));
        seg02.add(new GPSkPoint(52.527157, 13.331890, origin));
        seg02.add(new GPSkPoint(52.527444, 13.329637, origin));
        seg02.add(new GPSkPoint(52.527470, 13.328800, origin));
        
        
        GPSkSegment seg03 = new GPSkSegment();
        seg03.add(new GPSkPoint(52.514693, 13.377502));
        seg03.add(new GPSkPoint(52.515372, 13.382909));
        seg03.add(new GPSkPoint(52.515790, 13.385827));
        seg03.add(new GPSkPoint(52.517096, 13.385656));
        
        testSegments.add(seg01);
        testSegments.add(seg02);
        testSegments.add(seg03);
        
        ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
        ci.setFile(new File("test/gpsk/frechetCross01.gpx"));
        crossing01 = ci.call();
    }
    
    @Test
    public void testFrechet(){
        for(int i = 0; i <= 1000; i = i + 10){
            boolean b = fd.isInFrechetDistance(testSegments.get(0), testSegments.get(1), i);
            System.out.println("Distance: " + i + "m Test: " + b);
            if(b){
                break;
            }
        }
        GPSkSegment reverted = testSegments.get(1).reverse();
        for(int i = 0; i <= 1000; i = i + 10){
            boolean b = fd.isInFrechetDistance(testSegments.get(0), reverted, i);
            System.out.println("Reverted distance: " + i + "m Test: " + b);
            if(b){
                break;
            }
        }
    }
    
    @Test
    public void testIdentity(){
        assertTrue("The input of the segments is identic. Result should be zero:(",
                fd.isInFrechetDistance(testSegments.get(0), testSegments.get(0), 1.0));
        assertTrue("The input of the segments is identic. Result should be zero:(",
                fd.isInFrechetDistance(testSegments.get(1), testSegments.get(1), 1.0));
    }
    
   
    
    @Test
    public void testFrechet3(){
        System.out.println("Test with " + fd.getClass().getSimpleName());
        for(int i = 0; i < 600; i += 20){
            if(fd.isInFrechetDistance(testSegments.get(0), testSegments.get(1), i)){
                System.out.println("(1) isFrechet: " + i);
            } else {
                System.out.println("(1) not frechet anymore: " + i);
            }
        }
        for(int i = 0; i < 10000; i += 500){
            if(fd.isInFrechetDistance(testSegments.get(0), testSegments.get(2), i)){
                System.out.println("(2) is Frechet: " + i);
            } else {
                System.out.println("(2) not frechet anymore: " + i);
            }
        }
    }
    
    
}
