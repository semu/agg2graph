/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.boundary.Segment;
import de.fub.agg2graph.gpsk.data.IndexProcedure;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.RteType;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXBException;

import javolution.testing.TestSuite;

import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;


import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.Assert.assertFalse;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class SegmentNeighbourhood {
    private GPSkEnvironment env;
    private GpxType gpx;
    
    private HashMap<String, GPSkSegment> map = new HashMap<>();
    
    
    
    public SegmentNeighbourhood() throws JAXBException, Exception{
        
        
        FileImport fi = new FileImport();
        gpx = fi.parseFile(new File("test/gpsk/neighbourSegments.gpx"));
        JXPathContext context = JXPathContext.newContext(gpx);
        List<GPSkSegment> list = new LinkedList<>();
        
        for(int i = 1; i <= 4; i++){
            String name = "cluster01track0" + i;
            String xpath = "//rte[name='cluster01track0" + i + "']";
            RteType route = (RteType) context.getValue(xpath);
            GPSkSegment seg = Segment.fromGPX(route);
            seg.setTitle(xpath);
            list.add(seg);
            map.put(name, seg);
        }
        
        for(int i = 1; i <= 4; i++){
            String name = "cluster02track0" + i;
            String xpath = "//rte[name='" + name + "']";
            RteType route = (RteType) context.getValue(xpath);
            GPSkSegment seg = Segment.fromGPX(route);
            seg.setTitle(xpath);
            list.add(seg);
            map.put(name, seg);
        }
        
        RteType r1 = (RteType) context.getValue("//rte[name='noise01']");
        RteType r2 = (RteType) context.getValue("//rte[name='noise02']");
        GPSkSegment n1 = Segment.fromGPX(r1);
        n1.setTitle("noise01");
        GPSkSegment n2 = Segment.fromGPX(r2);
        n2.setTitle("noise02");
        list.add(n1);
        list.add(n2);
        map.put("noise01", n1);
        map.put("noise02", n2);
        
        EnvironmentBuilder builder = new EnvironmentBuilder(list);
        builder.useProjection().detectBounds();
        env = builder.build();
        

    }
    
    @Before
    public void setUp(){
        env.activateSegmentIndex();
    }
    
    @Test
    public void doesNotContainItSelf(){
        for(GPSkSegment seg : env.getSegments()){
        	IndexProcedure<GPSkSegment> ip = new de.fub.agg2graph.gpsk.data.DefaultSegmentProcedure(seg, 100);
        	env.processSegments(ip);
            List<GPSkSegment> n = ip.getResults();
            for(GPSkSegment seg2 : n){
                assertFalse(seg.equals(seg2));
            }
            
        }
        
    }
    
    
    @Test
    public void checkNeighbours1(){
        GPSkSegment seg1 = map.get("cluster01track01");
        IndexProcedure<GPSkSegment> ip = new de.fub.agg2graph.gpsk.data.DefaultSegmentProcedure(seg1, 250);
        env.activateSegmentIndex();
        
        env.processSegments(ip);
        List<GPSkSegment> n = ip.getResults();
        assertThat(n, hasSize(3));
        GPSkSegment seg2 = map.get("cluster01track02");
        
        assertThat(n, hasItem(seg2));
        GPSkSegment seg3 = map.get("cluster01track03");
        
        assertThat(n, hasItem(seg3));
        GPSkSegment seg4 = map.get("cluster01track04");
        
        assertThat(n, hasItem(seg4));
    }
    
    @Test
    public void chekcNeighbours2(){
        GPSkSegment noise1 = map.get("noise01");
        IndexProcedure<GPSkSegment> ip1 = new de.fub.agg2graph.gpsk.data.DefaultSegmentProcedure(noise1, 100);
        env.processSegments(ip1);
        List<GPSkSegment> n1 = ip1.getResults();
        assertThat(n1, hasSize(0));
        
        GPSkSegment noise2 = map.get("noise02");
        IndexProcedure<GPSkSegment> ip2 = new de.fub.agg2graph.gpsk.data.DefaultSegmentProcedure(noise2, 100);
        env.processSegments(ip2);
        List<GPSkSegment> n2 = ip2.getResults();
        assertThat(n2, hasSize(0));
    }
    
    @Ignore
    @Test
    public void myTest(){
        GPSkSegment seg1 = map.get("cluster01track01");
        GPSkSegment seg2 = map.get("cluster01track02");
        JensFrechet jf = new JensFrechet();
        for(int i =  5; i < 2000; i += 10){
            boolean f = jf.isInFrechetDistance(seg1, seg2, i);
            System.out.println("is in Frechet " + i + ": " + f);
        }
    }
    
    
    
    
    
    
}
