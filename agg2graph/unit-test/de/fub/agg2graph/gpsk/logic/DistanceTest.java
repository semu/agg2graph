/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


/**
 *
 * @author Christian Windolf
 */
public class DistanceTest {
    private static final int LAT1 = 0, LON1 = 1, LAT2 = 2, LON2 = 3, RESULT = 4;
    
    private List<double[]> testdata;
    
    @Before
    public void setUp(){
        testdata = new LinkedList<double[]>();
        testdata.add(new double[]{
            52.530843,
            13.326573,
            52.526353,
            13.343139,
            1227
        });//Turmstraße/Beusselstraße -- U Turmstraße
        
        testdata.add(new double[]{
            52.435031,
            13.260362,
            52.452165,
            13.286927,
            2621
        });//Berliner Straße/Clayalle -- Silberlaube
        
        testdata.add(new double[]{
            52.455696,
            13.297012,
            52.530843,
            13.325980,
            8583
        });//Takustraße 9 -- Wittstocker Straße 20
        
        testdata.add(new double[]{
            52.534498,
            13.199115,
            52.612605,
            12.885749,
            22888
        });//Spandau train station -- Nauen train station
        
        testdata.add(new double[]{
            25.767985,
            -80.134821,
            25.773228,
            -80.190167,
            5573
        });//Miame: South Point -- Downtown
        
    }
    

    
    
}
