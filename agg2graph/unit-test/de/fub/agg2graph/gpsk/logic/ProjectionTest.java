/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.io.DefaultTestLoader;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
@RunWith(Parameterized.class)
public class ProjectionTest {

    Class<? extends Projection> projectionClass;
    List<GPSkEnvironment> list;

    public ProjectionTest(Class c) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        this.projectionClass = c;
        list = DefaultTestLoader.load();
        

        for (GPSkEnvironment env : list) {
            Projection p = ProjectionFactory.getInstance().createProjection(env.getBounds(), c);
            
            env.setProjection(p);
            env.updateProjection();
        }
    }
    
    @Parameters
    public static Collection<Object[]> params(){
        Object[][] classes = new Object[][]{
            {UTMProjection.class},
            {SimpleProjection.class}
        };
        return Arrays.asList(classes);
    }

    @Test
    public void testDifference() {
        double distance = 0;
        TrigonometricDistance d = new TrigonometricDistance();

        for (GPSkEnvironment env : list) {
            Projection proj = env.getProjection();
            for (GPSkPoint point : env.getPoints()) {
                
                double d2;
                GPSkPoint p;
                try {
                    p = (GPSkPoint) proj.toLatLon(point.getEast(), point.getNorth());
                    
                } catch (UnsupportedOperationException ex) {

                    System.out.println(projectionClass.getSimpleName() + " does not support a backward projection");

                    return;
                }
                d2 = d.calculate(p, point);
                distance += (d2 * d2);
            }
        }
        System.out.println("distance for " + projectionClass.getSimpleName() + ": " + distance);
    }
}
