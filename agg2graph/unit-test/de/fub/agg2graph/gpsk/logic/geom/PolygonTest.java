/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic.geom;

import org.junit.Test;

import de.fub.agg2graph.gpsk.logic.geom.Polygon.Builder;
import de.fub.agg2graph.gpsk.logic.geom.Polygon.RectangleBuilder;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


/**
 * @author Christian Windolf
 * 
 */
public class PolygonTest {

	@Test(expected = IllegalArgumentException.class)
	public void testMinimumPoints() {
		Polygon pg = new Polygon(new Point(0, 1), new Point(0, 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotNull() {
		new Polygon(null);
	}

	@Test
	public void testBuilder1() {
		Polygon.Builder builder = new Polygon.Builder();
		assertNull(builder.build());
	}

	@Test
	public void testBuilder2() {
		Polygon.Builder builder = new Polygon.Builder();
		builder.add(0, 5).add(1, 5).add(2, 5);
		builder.add(2, 1).add(2, 0).add(0, 0);
		Polygon pg = builder.build();
		assertThat(pg.getPoints().length, equalTo(4));
		assertThat(pg.getEdges().length, equalTo(4));

	}

	@Test(expected = IllegalArgumentException.class)
	public void testBuilder3() {
		Polygon.Builder builder = new Polygon.Builder();
		builder.add(0, 5).add(0, 5).add(0, 6).add(0, 3);
	}

	@Test
	public void testPolygoneBounds() {
		Polygon.Builder builder = new Polygon.Builder();
		builder.add(0, 5).add(10, 5).add(10, 0).add(0, 0);
		Polygon pg = builder.build();
		assertThat((int) pg.getBottomPoint().y, equalTo(0));
		assertThat((int) pg.getTopPoint().y, equalTo(5));
		assertThat((int) pg.getLeftMostPoint().x, equalTo(0));
		assertThat((int) pg.getRightMostPoint().x, equalTo(10));
	}
	
	@Test
	public void testContains(){
		Polygon.Builder builder = new Polygon.Builder();
		builder.add(0, 10).add(12,9).add(11,0).add(0,0);
		Point inside = new Point(5,5);
		Point outside1 = new Point(-1, 3);
		Point outside2 = new Point(11.5,0);
		Polygon pg = builder.build();
		assertTrue(pg.contains(inside));
		assertFalse(pg.contains(outside1));
		assertFalse(pg.contains(outside2));
		
	}
	
	@Test
	public void testRectangleBuilder(){
		Polygon.RectangleBuilder rectBuilder = new Polygon.RectangleBuilder();
		rectBuilder.setLeftUp(new Point(0,4));
		rectBuilder.setRightDown(new Point(5,0));
		Polygon pg = rectBuilder.build();
		assertThat(pg.getPoints().length, is(equalTo(4)));
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRectangleBuilder2(){
		Polygon.RectangleBuilder rectBuilder = new Polygon.RectangleBuilder();
		rectBuilder.setLeftUp(new Point(0,0));
		rectBuilder.setRightDown(new Point(0,0));
		rectBuilder.build();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRectangleBuilder3(){
		Polygon.RectangleBuilder rectBuilder = new Polygon.RectangleBuilder();
		rectBuilder.setLeftUp(0,1);
		rectBuilder.setRightDown(0,4);
		rectBuilder.build();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCross(){
		Polygon.Builder builder = new Polygon.Builder();
		builder.add(0,0).add(0,10).add(30, 10).add(30, 30);
		builder.build();
		
	}

}
