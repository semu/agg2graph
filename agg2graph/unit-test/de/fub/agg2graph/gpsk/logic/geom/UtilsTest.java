/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic.geom;

import static de.fub.agg2graph.gpsk.logic.geom.PolygonUtils.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import de.fub.agg2graph.gpsk.logic.geom.Polygon.Builder;

/**
 * @author Christian Windolf
 * 
 */
public class UtilsTest {

	static List<Polygon> polygonList = new LinkedList<>();

	@BeforeClass
	public static void before() {
		Polygon.Builder builder1 = new Polygon.Builder();
		builder1.add(20, 20);
		builder1.add(40, 40);
		builder1.add(40, 20);
		polygonList.add(builder1.build());
		
		Polygon.Builder builder2 = new Polygon.Builder();
		builder2.add(20, 40);
		builder2.add(40, 40);
		builder2.add(40, 20);
		builder2.add(20, 20);
		polygonList.add(builder2.build());
		
		Polygon.Builder builder3 = new Polygon.Builder();
		builder3.add(500, 600);
		builder3.add(520, 600);
		builder3.add(520, 550);
		builder3.add(500, 550);
		polygonList.add(builder3.build());
		
		Polygon.Builder builder4 = new Polygon.Builder();
		builder4.add(10, 50);
		builder4.add(30, 50);
		builder4.add(30, 30);
		builder4.add(10, 30);
		polygonList.add(builder4.build());
		
		Polygon.RectangleBuilder rectBuilder = new Polygon.RectangleBuilder();
		rectBuilder.setLeftUp(20, 50);
		rectBuilder.setRightDown(40, 30);
		polygonList.add(rectBuilder.build());
	}

	/**
	 * 
	 */
	public UtilsTest() {
		// TODO Auto-generated constructor stub
	}

	@Test
	public void testIdentity() {
		for (Polygon pg : polygonList) {
			assertTrue(
					"two identic polygons should overlay each other, but they don't",
					overlap(pg, pg));
			Polygon pg2 = merge(pg, pg);
			assertThat(pg2.getPoints().length, equalTo(pg.getPoints().length));
			Point[] array1 = pg.getPoints();
			Point[] array2 = pg2.getPoints();
			for(int i = 0; i < array1.length; i++){
				assertThat(array1[i].getX(), is(closeTo(array2[i].getX(), 0.5)));
				assertThat(array1[i].getY(), is(closeTo(array2[i].getY(), 0.5)));
			}
		}

	}
	
	@Test
	public void testSameEdge(){
		Polygon pg1 = polygonList.get(0);
		Polygon pg2 = polygonList.get(1);
		assertTrue(overlap(pg1, pg2));
		Polygon pg3 = merge(pg1, pg2);
		assertThat(pg3.getPoints().length, equalTo(4));
		Point[] resultPoints = pg3.getPoints();
		boolean[] array = new boolean[4];
		for(int i = 0; i < 4; i++){
			array[i] = false;
			for(Point p : pg2.getPoints()){
				boolean b1 = Math.abs(p.getX() - resultPoints[i].getX()) < 0.5;
				boolean b2 = Math.abs(p.getY() - resultPoints[i].getY()) < 0.5;
				if(b1 && b2){
					array[i] = true;
				}
			}
		}
		for(int i = 0; i < 4; i++){
			assertTrue(array[i]);
		}
		
		
	}
	
	@Test
	public void testOutOfReach(){
		Polygon pg1 = polygonList.get(0);
		Polygon pg2 = polygonList.get(1);
		Polygon pg3 = polygonList.get(2);
		assertFalse(overlap(pg1, pg3));
		assertFalse(overlap(pg2, pg3));
	}
	
	
	@Test
	public void testMerging(){
		Polygon pg1 = polygonList.get(1);
		Polygon pg2 = polygonList.get(3);
		assertTrue(overlap(pg1, pg2));
		Polygon pg3 = merge(pg1, pg2);
		assertThat(pg3.getPoints().length, equalTo(8));
		Point[] expectedPoints = new Point[]{
				new Point(10,50),
				new Point(30,50),
				new Point(30,40),
				new Point(40,40),
				new Point(40,20),
				new Point(20,20),
				new Point(20,30),
				new Point(10,30)
		};
		for(Point p : expectedPoints){
			polygonContainsPoint(pg3, p);
		}
	}
	
	@Test
	public void testOverLappingEdge(){
		Polygon pg1 = polygonList.get(1);
		Polygon pg2 = polygonList.get(4);
		assertTrue(overlap(pg1, pg2));
		Polygon pg3 = merge(pg1, pg2);
		Point[] points = new Point[]{
				new Point(20, 50),
				new Point(40, 50),
				new Point(40, 20),
				new Point(20, 20)
		};
		for(Point p : points){
			polygonContainsPoint(pg3, p);
		}
	}
	
	private void polygonContainsPoint(Polygon pg, Point p){
		
		for(Point point : pg.getPoints()){
			boolean b1 = Math.abs(point.getX() - p.getX()) < 0.5;
			boolean b2 = Math.abs(point.getY() - p.getY()) < 0.5;
			if(b1 && b2){
				return;
			}
		}
		fail("Point " + p + " is not contained in " + pg);
	}

}
