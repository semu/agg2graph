/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic.geom;

import java.util.Arrays;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * @author Christian Windolf
 * 
 */
public class EdgeTest {

	/**
	 * 
	 */
	public EdgeTest() {

	}

	/**
	 * Tests, that the cross of two edges is found
	 * 
	 * | -+- |
	 * 
	 * @throws CollinearException
	 */
	@Test
	public void testCross() throws CollinearException {
		Edge e1 = new Edge(0, 1, 3, 1);
		Edge e2 = new Edge(2, 0, 2, 2);

		Point cross = e1.crosses(e2);

		assertThat(cross.x, is(closeTo(2, 0.0001)));
		assertThat(cross.y, is(closeTo(1, 0.0001)));
	}

	@Test(expected = CollinearException.class)
	public void testColliniar1() throws CollinearException {
		Edge e = new Edge(1, 2, 3, 4);
		e.crosses(e);
	}

	@Test(expected = CollinearException.class)
	public void testColliniar2() throws CollinearException {
		Edge e1 = new Edge(0, 0, 5, 0);
		Edge e2 = new Edge(10, 0, 15, 0);
		e1.crosses(e2);
	}

	/**
	 * 
	 * | ---- | |
	 * 
	 * @throws CollinearException
	 */
	@Test
	public void testOutOfReach() throws CollinearException {
		Edge e1 = new Edge(0, 0, 5, 0);
		Edge e2 = new Edge(10, 0, 10, 12);
		Point cross = e1.crosses(e2);
		assertNull("Expected to be null, but it was " + cross,
				cross);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoPointInConstructor() {
		new Edge(0, 0, 0, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNaNinConstructor() {
		new Edge(Double.NaN, 0, 2, 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInfinityInConstructor1() {
		new Edge(Double.POSITIVE_INFINITY, 0, 1, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInfinityInConstructor2() {
		new Edge(12, 0, Double.NEGATIVE_INFINITY, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPointConstructor() {
		Point p = new Point(0, 0);
		new Edge(p, p);
	}

	@Test
	public void testConsitency() {
		Point p1 = new Point(0, 1);
		Point p2 = new Point(1, 2);
		Edge e = new Edge(p1, p2);
		assertThat(e.x1, is(closeTo(0, 0.001)));
		assertThat(e.y1, is(closeTo(1, 0.001)));
		assertThat(e.x2, is(closeTo(1, 0.001)));
		assertThat(e.y2, is(closeTo(2, 0.001)));
	}

	@Test
	public void testEquals() {
		Point p1 = new Point(0, 1);
		Point p2 = new Point(1, 2);
		Point p3 = new Point(-1, 5);
		Edge e1 = new Edge(p1, p2);
		assertTrue(e1.equals(e1));
		Edge e2 = new Edge(p1, p3);
		assertFalse(e1.equals(e2));

	}

}
