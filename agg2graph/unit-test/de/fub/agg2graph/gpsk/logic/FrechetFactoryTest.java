/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
@RunWith(value = Parameterized.class)
public class FrechetFactoryTest {

    private FrechetDistance fd;

    public FrechetFactoryTest(Class c) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        fd = FrechetFactory.getInstance().createNewInstance(c);
    }

    @Parameters
    public static Collection data() {
        Class[][] classes = new Class[][]{{JensFrechet.class}, {JTSDistance.class},{HausdorffDistance.class}};
        return Arrays.asList(classes);

    }
    
    @Test
    public void testParameters(){
        System.out.println("class: " + fd.getClass().getSimpleName());
    }
    
    GPSkEnvironment env;
    
    @Before
    public void setUp() throws JAXBException, Exception{
        EnvironmentBuilder builder = new EnvironmentBuilder();
        builder.setProjection(EnvironmentBuilder.PROJECTION_ON);
        ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
        ci.setFile(new File("test/gpsk/frechetCross01.gpx"));
        env = ci.call();
    }
    
    @Test
    public void testWithCrossroad() throws JAXBException, Exception{
        
        
        List<GPSkSegment> segments = env.getSegments();
        boolean b = fd.isInFrechetDistance(segments.get(0),segments.get(1), 1000);
        System.out.println(fd.getClass().getSimpleName() + ": " + b);
    }
    
    @Test
    public void testIdentity(){
        List<GPSkSegment> segmentList = env.getSegments();
        for(GPSkSegment seg : segmentList){
            // a tolerance of one meter should be fine...
            boolean b = fd.isInFrechetDistance(seg, seg, 1);
            assertTrue(b);
        }
    }
    
    @Test
    public void testAlwaysSameClass(){
    	FrechetFactory factory = FrechetFactory.getInstance();
    	Class<? extends FrechetDistance> defaultClass = factory.getDefaultClass();
    	FrechetDistance withoutEps = factory.createNewInstance();
    	FrechetDistance withEps = factory.createNewInstance(50);
    	assertEquals(defaultClass, withoutEps.getClass());
    	assertEquals(defaultClass, withEps.getClass());
    	assertThat(withEps.getEps(), is(closeTo(50, 0.1)));
    	assertTrue(Double.isNaN(withoutEps.getEps()));
    	
    	
    }
}
