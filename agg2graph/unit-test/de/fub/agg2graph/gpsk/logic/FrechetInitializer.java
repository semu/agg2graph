/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

/**
 *
 * @author doggy
 */
public class FrechetInitializer {
    protected static FrechetDistance[] getDistances() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        FrechetImplementation[] fi = FrechetImplementation.values();
        FrechetDistance[] fd = new FrechetDistance[fi.length];
        for(int i = 0; i < fi.length; i++){
            fd[i] = FrechetFactory.getInstance().createNewInstance(fi[i].getImplementation());
        }
        return fd;
    }
    
    protected static Collection<Object[]> getDistancesAsParam() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        FrechetDistance[] fd = getDistances();
        Object[][] params = new Object[fd.length][1];
        for(int i = 0; i < params.length; i++){
            params[i] = new Object[]{fd[i]};
        }
        return Arrays.asList(params);
    }
    
    public static void setProjection(GPSkSegment seg){
        UTMProjection p = new UTMProjection(seg.getMinLatitude(), seg.getMinLongitude(), seg.getMaxLatitude(), seg.getMaxLongitude());
        for(GPSkPoint point : seg){
            p.setProjection(point);
        }
    }
}
