/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk;

import de.fub.agg2graph.gpsk.algorithms.NormalizeOptions;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class TestStore {
    
    @Before
    public void setUp(){
        
    }
    
    @Test
    public void testCorrectClass() throws Exception{
        NormalizeOptions no = (NormalizeOptions)
                KeyValueStore.loadOptions(NormalizeOptions.class);
    }
    
    @Test
    public void testCorrectLoading() throws Exception{
        KeyValueStore.putValue("normalize.distance", 100);
        NormalizeOptions no = (NormalizeOptions)
                KeyValueStore.loadOptions(NormalizeOptions.class);
        assertEquals(no.distance, 100, 0.004);
        KeyValueStore.putValue("normalize.distance", new NormalizeOptions().distance);
    }
}
