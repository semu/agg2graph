/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.io.DefaultTestLoader;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@RunWith(Parameterized.class)
public class TestRDP extends Algorithm{
    
    @Parameters
    public static Collection<Object[]> params(){
        RDPOptions options = new RDPOptions();
        List<GPSkEnvironment> list = DefaultTestLoader.list;
        Object[][] params = new Object[2][list.size()];
        for(int i = 0; i < params.length; i++){
            RDPFilter filter = new RDPFilter();
            filter.setOptions(options);
            params[i] = new Object[]{
                filter,
                list.get(i)
            };
        }
        return Arrays.asList(params);
        
    }
    
    public TestRDP(RDPFilter filter, GPSkEnvironment env) throws Exception {
        super(filter, env);
    }
    
    @Test
    public void testSamePointAmount(){
        assertThat(output.getPoints().size(), is(lessThan(input.getPoints().size())));
    }
    
}
