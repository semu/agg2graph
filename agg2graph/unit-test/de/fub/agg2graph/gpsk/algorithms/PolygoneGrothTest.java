/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpsk.algorithms.PolygonCrossroad.Crossroad;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.logic.geom.Polygon;
import de.fub.agg2graph.gpsk.logic.geom.Polygon.RectangleBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Christian Windolf
 *
 */
public class PolygoneGrothTest {
	
	private PGOptions pgOptions = new PGOptions();
	private PolygonCrossroad pc = new PolygonCrossroad();

	/**
	 * 
	 */
	public PolygoneGrothTest() {
		// TODO Auto-generated constructor stub
	}
	
	@Before
	public void setUp(){
		pgOptions.startingRectSize = 20;
		pgOptions.extensionSize = 10;
		pc.setOptions(pgOptions);
		
	}
	
	
	@Test
	public void testConstructor(){
		GPSkPoint p = new GPSkPoint(53,13);
		p.setEast(20);
		p.setNorth(30);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p);
		assertThat(pcc.getContainingPoints(), hasSize(1));
		Polygon pg = pcc.getPolygon();
		assertThat(pg.getPoints().length, equalTo(4));
		assertThat(pg.getPoints()[0].getX(), closeTo(0, 0.0001));
		assertThat(pg.getPoints()[0].getY(), closeTo(50, 0.0001));
		assertThat(pg.getPoints()[2].getX(), closeTo(40, 0.0001));
		assertThat(pg.getPoints()[2].getY(), closeTo(10, 0.0001));
	}
	
	@Test
	public void testRandomConstructor(){
		Random generator = new Random();
		GPSkPoint p = new GPSkPoint(53, 13);
		double east = generator.nextDouble() * 500;
		double north = generator.nextDouble() * 500;
		p.setEast(east);
		p.setNorth(north);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p);
		assertThat(pcc.getContainingPoints(), hasSize(1));
		Polygon pg = pcc.getPolygon();
		assertThat(pg.getPoints()[1].getX() - pg.getPoints()[0].getX(), closeTo( 2 * pgOptions.startingRectSize, 0.0001));
		assertThat(pg.getPoints()[0].getY() - pg.getPoints()[3].getY(), closeTo( 2 * pgOptions.startingRectSize, 0.0001));
		
	}
	
	@Test
	public void testValidation1(){
		GPSkPoint p = new GPSkPoint(53, 13);
		p.setEast(500);
		p.setNorth(400);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p);
		Polygon.RectangleBuilder rectBuilder1 = new Polygon.RectangleBuilder();
		rectBuilder1.setLeftUp(0, 100);
		rectBuilder1.setRightDown(100, 0);
		
		Polygon pg1 = rectBuilder1.build();
		
		List<GPSkPoint> pointList1 = new LinkedList<>();
		pointList1.add(getPoint(5,80));//I
		pointList1.add(getPoint(6, 79));//I
		pointList1.add(getPoint(5.5, 91));//I
		pointList1.add(getPoint(15, 88));//I
		pointList1.add(getPoint(21, 81));//I
		assertFalse(pcc.isValid(pointList1, pg1));
		

	}
	
	@Test
	public void testValidation2(){
		GPSkPoint p = new GPSkPoint(53, 13);
		p.setEast(500);
		p.setNorth(400);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p);
		Polygon.RectangleBuilder rectBuilder1 = new Polygon.RectangleBuilder();
		rectBuilder1.setLeftUp(0, 100);
		rectBuilder1.setRightDown(100, 0);
		
		Polygon pg1 = rectBuilder1.build();
		List<GPSkPoint> pointList2 = new LinkedList<>();
		pointList2.add(getPoint(5,80));//I
		pointList2.add(getPoint(6,81));//I
		pointList2.add(getPoint(52, 90));//II
		pointList2.add(getPoint(53, 98.5));//II
		pointList2.add(getPoint(66, 6));//III
		pointList2.add(getPoint(67, 7));//III
		assertTrue(pcc.isValid(pointList2, pg1));
	}
	
	@Test
	public void testValidation3(){
		GPSkPoint p = new GPSkPoint(53, 13);
		p.setEast(500);
		p.setNorth(400);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p);
		Polygon.RectangleBuilder rectBuilder1 = new Polygon.RectangleBuilder();
		rectBuilder1.setLeftUp(0, 100);
		rectBuilder1.setRightDown(100, 0);
		
		Polygon pg1 = rectBuilder1.build();
		
		List<GPSkPoint> pointList3 = new LinkedList<>();
		pointList3.add(getPoint(4,3));//IV
		pointList3.add(getPoint(5,5));//IV
		pointList3.add(getPoint(6,6));//IV
		pointList3.add(getPoint(44, 54));//I
		pointList3.add(getPoint(44, 66));//I
		pointList3.add(getPoint(45, 65));//I
		pointList3.add(getPoint(69,33)); //III
		pointList3.add(getPoint(54, 33)); //III
		pointList3.add(getPoint(55, 33)); //III
		pointList3.add(getPoint(56, 36)); //III
		
		pointList3.add(getPoint(88, 88));//II
		assertTrue(pcc.isValid(pointList3, pg1));
	}
	
	@Test
	public void testValidation4(){
		GPSkPoint p = new GPSkPoint(53, 13);
		p.setEast(500);
		p.setNorth(400);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p);
		Polygon.RectangleBuilder rectBuilder1 = new Polygon.RectangleBuilder();
		rectBuilder1.setLeftUp(0, 100);
		rectBuilder1.setRightDown(100, 0);
		
		Polygon pg1 = rectBuilder1.build();
		
		List<GPSkPoint> pointList4 = new LinkedList<>();
		pointList4.add(getPoint(5,5));//IV
		pointList4.add(getPoint(59,4));//III
		pointList4.add(getPoint(5, 55)); //I
		pointList4.add(getPoint(5,56)); //I
		pointList4.add(getPoint(6,56)); //I
		pointList4.add(getPoint(7,57)); //I
		pointList4.add(getPoint(8,68)); //I
		pointList4.add(getPoint(9, 99)); //I
		pointList4.add(getPoint(91, 92)); //II
		
		assertFalse(pcc.isValid(pointList4, pg1));
	}
	
	private static GPSkPoint getPoint(double x, double y){
		GPSkPoint p = new GPSkPoint(53, 13);
		p.setEast(x);
		p.setNorth(y);
		return p;
	}

}
