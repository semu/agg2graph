/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.util.List;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class Algorithm {
    GPSkEnvironment input;
    GPSkEnvironment output;
    
    public Algorithm(AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> algo, GPSkEnvironment input) throws Exception{
        this.input = input;
        algo.setInput(input);
        this.output = algo.call();
    }
    
    @Test
    public void testBounds(){
        if(!output.hasBounds()){
            return;
        }
        testBounds(output.getPoints(), output.getBounds());
        for(GPSkSegment seg : output.getSegments()){
            testBounds(seg, output.getBounds());
        }
    }
    
    private void testBounds(List<GPSkPoint> points, double[] bounds){
        for(GPSkPoint p : points){
            assertThat(bounds[0], lessThanOrEqualTo(new Double(p.getLat())));
            assertThat(bounds[1], lessThanOrEqualTo(new Double(p.getLon())));
            assertThat(bounds[2], greaterThanOrEqualTo(new Double(p.getLat())));
            assertThat(bounds[3], greaterThanOrEqualTo(new Double(p.getLon())));
        }
    }
    
    @Test
    public void checkNotNull(){
        checkNotNull(output.getPoints());
        for(GPSkSegment seg : output.getSegments()){
            checkNotNull(seg);
        }
    }
    
    private void checkNotNull(List<GPSkPoint> list){
        for(GPSkPoint p : list){
            assertNotNull(p);
        }
        
    }
    
    @Test
    public void testConsistency(){
        List<GPSkPoint> points = output.getPoints();
        List<GPSkSegment> segments = output.getSegments();
        for(GPSkSegment seg : segments){
            for(GPSkPoint p : seg){
                assertThat(points, hasItem(p));
            }
        }
    }
}
