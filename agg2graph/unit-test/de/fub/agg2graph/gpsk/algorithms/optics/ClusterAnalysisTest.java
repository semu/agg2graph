/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * 
 * @author Christian Windolf, christianwindolf@web.de
 */
public class ClusterAnalysisTest {

	public static List<OPTICSObject> createData(List<Double> list) {
		List<OPTICSObject> output = new LinkedList<>();
		for (Double d : list) {
			DefaultOPTICSObject o = new DefaultOPTICSObject();
			o.setReachabilityDistance(d);
			output.add(o);
		}
		return output;
	}

	public static List<Double> asList(double[] data) {
		List<Double> list = new LinkedList<>();

		for (double d : data) {
			list.add(new Double(d));
		}
		return list;
	}

	double[] testData;
	double[] testData2;
	SteepOptions so;

	@Before
	public void fillTestData() {
		testData = new double[] { 50, // 00, down
				49, // 01, down
				44, // 02,
				44, // 03
				44, // 04, ???down
				43.9, // 05, down
				42, // 06,
				42, // 07, up
				49, // 08, down
				44, // 09, down
				40, // 10, ???down

				39.9, // 11, down
				38, // 12
				38, // 13
				38, // 14, down
				35, // 15, down
				34, // 16
				34, // 17, up
				40, // 18
				40, // 19, ??up
				40.1, // 20 ??down

				40, // 21
				41, // 22 up
				44, // 23 up
				45, // 24
				45, // 25 up
				46, // 26
				46, // 27
				46, // 28
				46, // 29
				46, // 30

				46, // 31
				46, // 32 up
				48, // 33 up
				50 // 34
		};

		testData2 = new double[] { 50, // 00
				50, // 01
				50, // 02, down
				45, // 03, down
				40, // 04, down
				35, // 05, down
				30, // 06, down
				25, // 07, down
				20, // 08´
				20, // 09

				20, // 10
				20, // 11
				20, // 12
				20, // 13
				20, // 14
				20, // 15
				20, // 16
				20, // 17
				20, // 18
				20, // 19

				20, // 20
				20, // 21, up
				25, // 22, up
				30, // 23, up
				35, // 24,
				35, // 25 up
				40, // 26 up
				45, // 27, up
				50, // 28
				50, // 29

				50, // 30
				50, // 31
				50, // 32, down
				45, // 33, down
				40, // 34, down
				35, // 35, down
				30, // 36, down
				25, // 37
				25, // 38
				25, // 39, down

				20, // 40, down
				15, // 41
				15, // 42
				15, // 43
				15, // 44
				15, // 45
				15, // 46
				15, // 47
				15, // 48
				15, // 49

				15, // 50, up
				20, // 51, up
				25, // 52, down
				20, // 53, down
				15, // 54
				15, // 55
				15, // 56
				15, // 57
				15, // 58, up
				20, // 59, up

				25, // 60, up
				30, // 61, up
				35, // 62, up
				40, // 63, up
				45, // 64, up
				50 // 65
		};

		so = new SteepOptions();
		so.maxDiameter = 120;
		so.xi = 0.01d;
		so.minPts = 5;
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadData1() {
		double[] tooShort = new double[] { 1, 2, 3 };
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(tooShort)),
				so.xi, so.minPts);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadData2() {
		testData[5] = Double.NaN;
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(testData)),
				so.xi, so.minPts);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadData3() {
		testData[7] = Double.POSITIVE_INFINITY;
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(testData)),
				so.xi, so.minPts);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadData4() {
		testData[14] = Double.NEGATIVE_INFINITY;
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(testData)),
				so.xi, so.minPts);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadData5() {
		testData[22] = -1;
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(testData)),
				so.xi, so.minPts);
	}

	@Test(timeout = 5000)
	public void testCorrectSteepPoints() {
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(testData)),
				so.xi, so.minPts);
		ca.detectSteepPointsAndSetIndizes();
		assertTrue(ca.list.get(0).isDownPoint());
		assertFalse(ca.list.get(0).isUpPoint());

		assertTrue(ca.list.get(1).isDownPoint());
		assertFalse(ca.list.get(1).isUpPoint());

		assertFalse(ca.list.get(2).isDownPoint());
		assertFalse(ca.list.get(2).isUpPoint());

		assertFalse(ca.list.get(3).isDownPoint());
		assertFalse(ca.list.get(3).isUpPoint());

		assertTrue(ca.list.get(5).isDownPoint());
		assertFalse(ca.list.get(5).isUpPoint());

		assertFalse(ca.list.get(32).isDownPoint());
		assertTrue(ca.list.get(32).isUpPoint());

		assertFalse(ca.list.get(33).isDownPoint());
		assertTrue(ca.list.get(33).isUpPoint());
	}

	@Test
	public void testCA1() {
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(testData2)),
				so.xi, so.minPts);
		ca.detectSteepPointsAndSetIndizes();
		ca.detectClusters();
		Set<Cluster> clusterSet = ca.clusterSet;
		for (Cluster c : clusterSet) {

			List<OPTICSObject> l = c.getList();
			double rd_start = l.get(0).getReachabilityDistance();
			double rd_end = l.get(l.size() - 1).getReachabilityDistance();

			assertThat(rd_start, is(closeTo(rd_end, 0.1d)));
			List<OPTICSObject> l2 = l.subList(0, l.size() - 1);
			for (OPTICSObject oo : l2) {
				assertThat(oo.getReachabilityDistance(),
						lessThanOrEqualTo(rd_start));
				assertThat(oo.getReachabilityDistance(),
						lessThanOrEqualTo(rd_end));
			}
		}
	}

	@Test
	public void testCA2() {
		testData2[30] = 52;
		ClusterAnalysis ca = new ClusterAnalysis(createData(asList(testData2)),
				so.xi, so.minPts);
		ca.detectSteepPointsAndSetIndizes();
		ca.detectClusters();
		Set<Cluster> clusterSet = ca.clusterSet;
		for (Cluster c : clusterSet) {

			List<OPTICSObject> l = c.getList();
			double rd_start = l.get(0).getReachabilityDistance();
			double rd_end = l.get(l.size() - 1).getReachabilityDistance();

			assertThat(rd_start, is(closeTo(rd_end, 0.1d)));
			List<OPTICSObject> l2 = l.subList(0, l.size() - 1);
			for (OPTICSObject oo : l2) {
				assertThat(oo.getReachabilityDistance(),
						lessThanOrEqualTo(rd_start));
				assertThat(oo.getReachabilityDistance(),
						lessThanOrEqualTo(rd_end));
			}
		}
	}

}
