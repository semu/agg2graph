/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class DefaultOPTICSObject implements OPTICSObject{
    
    double reachabilityDistance;
    boolean up;
    boolean down;
    int index = -1;
    

    @Override
    public void setReachabilityDistance(double rd) {
        this.reachabilityDistance = rd;
    }

    @Override
    public double getReachabilityDistance() {
        return reachabilityDistance;
    }

    @Override
    public boolean isUndefined() {
        return Double.isInfinite(reachabilityDistance);
    }

    @Override
    public void setUpPoint(boolean b) {
        this.up = b;
    }

    @Override
    public boolean isUpPoint() {
        return up;
    }

    @Override
    public void setDownPoint(boolean b) {
        this.down = b;
    }

    @Override
    public boolean isDownPoint() {
        return down;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int getIndex() {
    	
        return index;
    }
    
}
