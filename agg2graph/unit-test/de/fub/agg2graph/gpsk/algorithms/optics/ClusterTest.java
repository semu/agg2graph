/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Christian Windolf
 * 
 */
public class ClusterTest {

	public static Cluster createCluster(int start, int end) {
		List<OPTICSObject> list = new LinkedList<>();
		for (int i = start; i <= end; i++) {
			OPTICSObject oo = new DefaultOPTICSObject();
			oo.setIndex(i);
			list.add(oo);
		}
		return new Cluster(list);
	}

	Cluster c1;
	Cluster c2;
	Cluster c3;
	Cluster c4;
	Cluster c5;
	Cluster c6;
	Cluster c7;
	
	int minPts;

	@Before
	public void setUp() {
		c1 = createCluster(5, 20);
		c2 = createCluster(10, 20);
		c3 = createCluster(25, 49);
		c4 = createCluster(27, 52);
		c5 = createCluster(21, 49);
		c6 = createCluster(22, 52);
		c7 = createCluster(15, 71);
		
		minPts = 5;
	}

	@Test
	public void testContains() {

		assertTrue(c1.contains(c2));
		assertFalse(c2.contains(c1));
		assertTrue(c1.contains(c1));

		assertFalse(c1.contains(c3));
		assertFalse(c3.contains(c1));
	}

	@Test
	public void testMergable() {
		assertTrue(c1.isMergable(c2, minPts));
		assertTrue(c2.isMergable(c1, minPts));
		assertTrue(c1.isMergable(c1, 1));
		
		assertFalse(c1.isMergable(c3, minPts));
		assertFalse(c3.isMergable(c1, minPts));
		
		assertTrue(c3.isMergable(c4, minPts));
		assertTrue(c4.isMergable(c3, minPts));
		
		assertTrue(c3.isMergable(c5, minPts));
		assertTrue(c5.isMergable(c3, minPts));
		
		assertTrue(c3.isMergable(c6, minPts));
		assertTrue(c6.isMergable(c3, minPts));
		
		assertFalse(c3.isMergable(c7, minPts));
		assertFalse(c7.isMergable(c3, minPts));

	}
	
	@Test
	public void testMerging1(){
		Cluster m1 = c1.merge(c2);
		testCluster(m1);
		assertThat(m1.startIndex, equalTo(5));
		assertThat(m1.endIndex, equalTo(20));
		
		Cluster m2 = c2.merge(c1);
		testCluster(m2);
		assertThat(m2.startIndex, equalTo(5));
		assertThat(m2.endIndex, equalTo(20));
	}
	
	@Test
	public void testMerging2(){
		Cluster m = c3.merge(c3);
		testCluster(m);
		assertThat(m.startIndex, equalTo(c3.startIndex));
		assertThat(m.endIndex, equalTo(c3.endIndex));
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testMerging3(){
		Cluster m = c2.merge(c3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testMerging4(){
		Cluster m = c3.merge(c2);
	}
	
	@Test
	public void testMerging5(){
		Cluster m1 = c3.merge(c4);
		testCluster(m1);
		assertThat(m1.startIndex, equalTo(25));
		assertThat(m1.endIndex, equalTo(52));
		
		Cluster m2 = c4.merge(c3);
		testCluster(m2);
		assertThat(m2.startIndex, equalTo(25));
		assertThat(m2.endIndex, equalTo(52));
	}
	
	@Test
	public void testMerging6(){
		Cluster m1 = c3.merge(c5);
		testCluster(m1);
		assertThat(m1.startIndex, equalTo(21));
		assertThat(m1.endIndex, equalTo(49));
		
		Cluster m2 = c5.merge(c3);
		testCluster(m2);
		assertThat(m2.startIndex, equalTo(21));
		assertThat(m2.endIndex, equalTo(49));
	}
	
	@Test
	public void testMerging7(){
		Cluster m1 = c3.merge(c6);
		testCluster(m1);
		assertThat(m1.startIndex, equalTo(22));
		assertThat(m1.endIndex, equalTo(52));
		
		Cluster m2 = c6.merge(c3);
		testCluster(m2);
		assertThat(m2.startIndex, equalTo(22));
		assertThat(m2.endIndex, equalTo(52));
	}
	
	@Test
	public void testMerging8(){
		Cluster m1 = c3.merge(c7);
		testCluster(m1);
		assertThat(m1.startIndex, equalTo(15));
		assertThat(m1.endIndex, equalTo(71));
		
		Cluster m2 = c7.merge(c3);
		testCluster(m2);
		assertThat(m2.startIndex, equalTo(15));
		assertThat(m2.endIndex, equalTo(71));
	}
	
	
	private void testCluster(Cluster c){
		Iterator<OPTICSObject> it = c.list.iterator();
		OPTICSObject first = it.next();
		while(it.hasNext()){
			OPTICSObject second = it.next();
			assertThat(second.getIndex(), equalTo(first.getIndex() + 1));
			first = second;
		}
	}

}
