/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.io.DefaultTestLoader;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@RunWith(Parameterized.class)
public class TestCleaner extends Algorithm{
    
    @Parameterized.Parameters
    public static Collection<Object[]> params(){
        List<GPSkEnvironment> list = DefaultTestLoader.list;
        Object[][] params = new Object[list.size()][2];
        for(int i = 0; i < params.length; i++){
            params[i] = new Object[]{
                new Cleaner(), list.get(i)
            };
        }

        return Arrays.asList(params);
        
    }
    
    public TestCleaner(Cleaner c, GPSkEnvironment env) throws Exception{
        super(c, env);
    }
}
