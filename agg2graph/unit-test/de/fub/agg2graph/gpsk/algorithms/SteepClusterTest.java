/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpsk.algorithms.optics.OPTICSPoint;
import de.fub.agg2graph.gpsk.algorithms.optics.SteepClustering;
import de.fub.agg2graph.gpsk.algorithms.optics.SteepOptions;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Christian Windolf
 *
 */
public class SteepClusterTest {
	GPSkEnvironment input;
	SteepOptions so = new SteepOptions();
	
	NumberFormat nf = new DecimalFormat("00");

	/**
	 * @throws Exception 
	 * 
	 */
	public SteepClusterTest() throws Exception {

		ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
		ci.setFile(new File("/home/doggy/.gpsk/test/optics.gpx"));
		input = ci.call();
		
		
	}
	
	
	@Before
	public void setValues(){
		so.maxDiameter = 120;
		so.xi = 0.01d;
		so.minPts = 5;
	}
	
	

	@Test
	public void testAtLeastOneCrossroad() throws Exception{
		SteepClustering sc = new SteepClustering(input);
		sc.setOptions(so);
		GPSkEnvironment output = sc.call();
		assertThat(output.getPoints(), hasSize(greaterThanOrEqualTo(1)));
		
		for(GPSkPoint p : output.getPoints()){
			System.out.println(p);
		}
	}
}
