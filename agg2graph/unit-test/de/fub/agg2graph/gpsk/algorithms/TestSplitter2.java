/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.io.File;
import java.util.Arrays;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;

import static de.fub.agg2graph.gpsk.algorithms.Splitter.*;
import static de.fub.agg2graph.gpsk.utils.UtilConstants.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;


/**
 * @author Christian Windolf
 *
 */
public class TestSplitter2  {
	GPSkEnvironment crossroads;
	GPSkEnvironment input;
	GPSkEnvironment output;
	
	private SplitterOptions options = new SplitterOptions();
	

	public TestSplitter2() throws Exception{
		options.eps = 30;
		ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
		ci.setFile(new File(ResourceManager.localFolder, "test/moabit-cleaned.gpx"));
		input = ci.call();
		
		ci.setFile(new File(ResourceManager.localFolder, "test/moabit-crossroads.gpx"));
		crossroads = ci.call();
	}
	
	@Before
	public void setUp() throws Exception{
		Splitter splitter = new Splitter();
		splitter.setInput(input, TRACKS);
		splitter.setInput(crossroads, CROSSROAD);
		splitter.setOptions(options);
		output = splitter.call();
	}
	
	@Test
	public void testBounds(){
		double[] inputBounds = input.getBounds();
		double[] outputBounds = output.getBounds();
		for(int i = 0; i < inputBounds.length; i++){
			assertThat(inputBounds[i], is(closeTo(outputBounds[i], 0.005)));
		}
		
		for(GPSkPoint p : output.getPoints()){
			assertThat(outputBounds[SOUTHWEST_LAT], is(lessThanOrEqualTo(p.getLat())));
			assertThat(outputBounds[SOUTHWEST_LON], is(lessThanOrEqualTo(p.getLon())));
			assertThat(outputBounds[NORTHEAST_LAT], is(greaterThanOrEqualTo(p.getLat())));
			assertThat(outputBounds[NORTHEAST_LON], is(greaterThanOrEqualTo(p.getLon())));
		}
	}
	
	@Test
	public void testNoEmptySegments(){
		for(GPSkSegment seg : output.getSegments()){
			assertThat(seg.size(), greaterThan(1));
		}
	}
	
	@Test
	public void testConsistency(){
		for(GPSkSegment seg : output.getSegments()){
			for(GPSkPoint p : seg){
				assertThat(seg, hasItem(p));
			}
		}
	}

}
