/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.boundary.Point;
import de.fub.agg2graph.gpsk.boundary.Segment;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class TestSplitter {
    GPSkEnvironment tracks;
    GPSkEnvironment crossroad;
    
    public TestSplitter() throws JAXBException{
        FileImport fi = new FileImport();
        GpxType gpx = fi.parseFile(new File("test/gpsk/splitter01.gpx"));
        List<GPSkPoint> c = Point.extractPoints(gpx);
        EnvironmentBuilder builder1 = new EnvironmentBuilder(c);
        builder1.detectBounds();
        crossroad = builder1.build();
        
        List<GPSkSegment> s = Segment.extractSegments(gpx.getRte());
        EnvironmentBuilder builder2 = new EnvironmentBuilder(s);
        builder2.detectBounds();
        tracks = builder2.build();
    }
    
    @Test
    public void testAmountOfTracks() throws Exception{
        Splitter splitter = new Splitter();
        splitter.setInput(crossroad, Splitter.CROSSROAD);
        splitter.setInput(tracks, Splitter.TRACKS);
        
        SplitterOptions so = new SplitterOptions();
        so.eps = 50;
        splitter.setOptions(so);
        GPSkEnvironment splitted = splitter.call();
        assertThat(splitted.getSegments(), hasSize(8));
    }
    
    
}
