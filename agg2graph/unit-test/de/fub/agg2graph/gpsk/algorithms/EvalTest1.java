/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import java.io.File;
import javax.xml.bind.JAXBException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class EvalTest1 {
    
    GPSkEnvironment env1;
    GPSkEnvironment env2;
    GPSkEnvironment env3;
    
    
    CDEvalOptions cdeo;
    
    @Before
    public void setUp() throws JAXBException, Exception{
        ConvertedImport<GPSkEnvironment> ci1 = new ConvertedImport<>(new EnvironmentImport());
        
        ci1.setFile(new File("test/gpsk/evalTest01.gpx"));
        env1 = ci1.call();
        
        ConvertedImport<GPSkEnvironment> ci2 = new ConvertedImport<>(new EnvironmentImport());
        
        ci2.setFile(new File("test/gpsk/evalTest02.gpx"));
        env2 = ci2.call();
        
        ConvertedImport<GPSkEnvironment> ci3 = new ConvertedImport<>(new EnvironmentImport());
        ci3.setFile(new File("test/gpsk/evalTest03.gpx"));
        env3 = ci3.call();
        
        cdeo = new CDEvalOptions();
        cdeo.maxDistance = 30;
        
    }
    
    @Test
    public void equalsTest() throws Exception{
        CDEvaluation cde1 = new CDEvaluation();
        cde1.input[0] = env1;
        cde1.input[1] = env1;
        cde1.setOptions(cdeo);
        cde1.call();
        
        assertThat(cde1.errorRate, is(closeTo(0, 0.01d)));
        assertThat(cde1.detectionRate, is(closeTo(1, 0.01d)));
        
        CDEvaluation cde2 = new CDEvaluation();
        cde2.setInput(env2, 0);
        cde2.setInput(env2, 1);
        cde2.setOptions(cdeo);
        cde2.call();
        
        assertThat(cde2.errorRate, is(closeTo(0, 0.01d)));
        assertThat(cde2.detectionRate, is(closeTo(1, 0.1d)));
    }
    
    @Test
    public void testRate1() throws Exception{
        CDEvaluation cd1 = new CDEvaluation();
        cd1.setOptions(cdeo);
        cd1.setInput(env1, 0);
        cd1.setInput(env2, 1);
        
        cd1.call();
        
        
        assertThat(cd1.errorRate, is(closeTo(0.20, 0.01d)));
        assertThat(cd1.detectionRate, is(closeTo(1, 0.01d)));
        
        CDEvaluation cd2 = new CDEvaluation();
        cd2.setOptions(cdeo);
        cd2.setInput(env2, 0);
        cd2.setInput(env1, 1);
        
        cd2.call();
        
        assertThat(cd2.errorRate, is(closeTo(0.25, 0.01d)));
        assertThat(cd2.detectionRate, is(closeTo(1, 0.01d)));
    }
    
    @Test
    public void dataTest(){
    	assertThat(env1.getPoints(), hasSize(4));
    	assertThat(env2.getPoints(), hasSize(5));
    	assertThat(env3.getPoints(), hasSize(5));
    }
    
    @Test
    public void testRate2() throws Exception{
    	CDEvaluation cde = new CDEvaluation();
    	cde.setOptions(cdeo);
    	cde.setInput(env1, 0);
    	cde.setInput(env3, 1);
    	
    	cde.call();
    	assertThat(cde.errorRate, is(closeTo(0.4d, 0.01d)));
    	assertThat(cde.detectionRate, is(closeTo(0.75d, 0.01d)));
    }
    
   
   
}
