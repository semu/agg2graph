/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.io.File;

import org.junit.BeforeClass;
import org.junit.Test;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Christian Windolf
 *
 */
public class PGTest extends Algorithm{
	
	private static PolygonCrossroad pc;
	private static GPSkEnvironment env;
	
	
	@BeforeClass
	public static void before() throws Exception{
		PGOptions options = new PGOptions();
		options.extensionSize = 10;
		options.startingRectSize = 20;
		pc = new PolygonCrossroad();
		pc.setOptions(options);
		ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
		ci.setFile(new File("test/gpsk/pgtest.gpx"));
		env = ci.call();
		
	}

	/**
	 * @throws Exception 
	 * 
	 */
	public PGTest() throws Exception {
		super(pc, env);
		
		
	}
	
	@Test
	public void testAmountOfCrossroads(){
		
		assertThat(output.getSegments(), hasSize(3));
	}

}
