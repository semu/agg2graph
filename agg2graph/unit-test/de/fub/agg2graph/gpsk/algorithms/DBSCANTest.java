/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import java.io.File;
import javax.xml.bind.JAXBException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class DBSCANTest extends Algorithm {
    
    static GPSkEnvironment env;
    static DBSCAN algo = new DBSCAN();
    
    @BeforeClass
    public static void before() throws JAXBException, Exception{
        ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
        ci.setFile(new File("test/gpsk/neighbourSegments.gpx"));
        env = ci.call();
        DBSCANoptions options = new DBSCANoptions();
        options.minPts = 2;
        algo.setOptions(options);
    }

    public DBSCANTest() throws Exception {
        super(algo, env);
    }
    
    @Test
    public void checkResult(){
        assertThat(output.getSegments(), hasSize(2));
    }
    
}
