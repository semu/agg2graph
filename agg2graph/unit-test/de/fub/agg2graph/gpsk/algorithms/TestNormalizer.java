/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import de.fub.agg2graph.io.FileExport;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.RteType;
import de.fub.agg2graph.schemas.gpx.WptType;
import java.io.File;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javax.xml.bind.JAXBException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Ignore;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class TestNormalizer {

    private GPSkEnvironment env;
    private List<GPSkSegment> testSegments;
    private GPSkEnvironment env2;

    @Before
    public void setUp() throws JAXBException, Exception {
        ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(
                new EnvironmentImport());
        ci.setFile(new File(ResourceManager.localFolder, "test/moabit-klein.gpx"));
        env = ci.call();

        File f = new File("test/gpsk/testNormalizer01.gpx");

        FileImport fi = new FileImport();
        GpxType gpx = fi.parseFile(f);
        RteType route = gpx.getRte().get(0);
        GPSkSegment seg = new GPSkSegment();
        for (WptType waypoint : route.getRtept()) {
            seg.add(new GPSkPoint(
                    waypoint.getLat().doubleValue(),
                    waypoint.getLon().doubleValue()));
        }
        testSegments = new LinkedList<>();
        testSegments.add(seg);

        File f1 = ResourceManager.localFolder;
        File f2 = new File(f1, "test/moabit-normalized.gpx");
        ci.setFile(f2);
        env2 = ci.call();
    }

    @Ignore("just temporary")
    @Test
    public void testMaximumDistance() throws Exception {
        double eps = 50;
        NormalizeOptions options = new NormalizeOptions();
        options.distance = 50;

        Normalizer normalizer = new Normalizer(env);
        normalizer.setOptions(options);
        GPSkEnvironment result = normalizer.call();
        for (GPSkSegment seg : result.getSegments()) {
            Iterator<GPSkPoint> it = seg.listIterator();
            GPSkPoint first = it.next();
            while (it.hasNext()) {
                GPSkPoint second = it.next();
                double d = first.distanceTo(second);
                if (it.hasNext()) {
                    assertThat(d, is(closeTo(options.distance, options.distance * (options.tolerance + 0.1))));
                } else {
                    assertThat(d, is(lessThan(options.distance + (options.distance * options.tolerance))));
                }
                first = second;
            }

        }

    }

    @Ignore("tempory")
    @Test
    public void testByFrechet() {
        Normalizer norm = new Normalizer();
        NormalizeOptions no = new NormalizeOptions();
        norm.setOptions(no);

        try {
            norm.call();
        } catch (Exception ex) {

            System.out.println("expecting nullpointer exception: " + ex);
            // do nothing
        }
        List<GPSkSegment> segList = env.getSegments();
        for (GPSkSegment seg : segList) {
            GPSkSegment seg2 = norm.normalizeSegment(seg);

            assertTrue(seg.isFrechet(seg2, no.distance * 2));
        }
    }

    @Ignore("just temporary")
    @Test
    public void testBingSegment() throws Exception {
        Normalizer norm = new Normalizer();
        NormalizeOptions no = new NormalizeOptions();
        no.distance = 10;
        no.tolerance = 0.2;
        norm.setOptions(no);

        try {
            norm.call();
        } catch (NullPointerException ex) {
            // only null pointer is expected here.
        }
        GPSkSegment seg = norm.normalizeSegment(testSegments.get(0));
        RteType route = new RteType();
        route.setName("testNormalizer01Results");
        for (GPSkPoint p : seg) {
            WptType wp = new WptType();
            wp.setLat(new BigDecimal(p.getLat()));
            wp.setLon(new BigDecimal(p.getLon()));
            route.getRtept().add(wp);
        }
        GpxType gpx = new GpxType();
        gpx.setCreator("gpsk-test");
        gpx.setVersion("1.1");
        gpx.getRte().add(route);
        FileExport fe = new FileExport();
        fe.export(new File("/tmp/testNormalizer.gpx"), gpx);
    }

    @Test
    public void avoidEmptySegments() throws Exception {

        NormalizeOptions no = new NormalizeOptions();
        no.distance = 10;
        no.tolerance = 0.2;

        Normalizer normalizer = new Normalizer(env2);
        normalizer.setOptions(no);

        GPSkEnvironment output = normalizer.call();
        for (GPSkSegment seg : output.getSegments()) {
            assertThat(seg.size(), greaterThan(1));
        }
    }


}
