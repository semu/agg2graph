/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


import de.fub.agg2graph.gpsk.algorithms.PolygonCrossroad.Crossroad;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.logic.Projection;
import de.fub.agg2graph.gpsk.logic.UTMProjection;
import de.fub.agg2graph.gpsk.logic.geom.Point;
import de.fub.agg2graph.gpsk.logic.geom.Polygon;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;

/**
 * @author Christian Windolf
 * 
 */
public class PolygoneGrothTest2 {

	private Projection proj = new UTMProjection(new double[] { 53, 13, 54, 14 });

	private PGOptions pgOptions = new PGOptions();

	/**
	 * 
	 */
	public PolygoneGrothTest2() {

	}

	@Before
	public void setUp() {
		pgOptions.extensionSize = 10;
		pgOptions.startingRectSize = 20;
	}

	@Test(timeout = 2500)
	public void testExtending1() {
		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(50, 150);
		list.add(p1);
		GPSkPoint p2 = getPoint(50, 130);
		list.add(p2);
		GPSkPoint p3 = getPoint(50, 125);
		list.add(p3);

		PolygonCrossroad pc = createPG(list);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p1);
		pcc.firstStep();
		while (pcc.extend());
		assertThat(pcc.getContainingPoints(), hasSize(3));
	}

	@Test
	public void testInit1() {
		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(50, 150);
		list.add(p1);
		GPSkPoint p2 = getPoint(60, 150);
		list.add(p2);
		GPSkPoint p3 = getPoint(75, 150);
		list.add(p3);

		PolygonCrossroad pc1 = createPG(list);
		PolygonCrossroad.Crossroad pcc1 = pc1.new Crossroad(p1);
		pcc1.firstStep();
		PolygonCrossroad.Crossroad pcc2 = pc1.new Crossroad(p3);
		pcc2.firstStep();

		assertThat(pcc1.getContainingPoints(), hasSize(2));
		assertThat(pcc2.getContainingPoints(), hasSize(1));
	}

	
	@Test(timeout = 1000)
	public void testNorthWestExtension(){
		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(5000,5000);
		list.add(p1);
		GPSkPoint p2 = getPoint(4975, 5025);
		list.add(p2);
		PolygonCrossroad pc = createPG(list);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p1);
		pcc.firstStep();
		int counter = 0;
		while(pcc.extend()){
			counter++;
		}
		assertThat(counter, equalTo(1));
		
		assertThat(pcc.getContainingPoints(), hasSize(2));
		Polygon pg = pcc.getPolygon();
		Point[] points = pg.getPoints();
		assertThat(points.length, equalTo(4));
		assertThat(points[0].getX(), is(closeTo(p1.getEast() - 30, 0.2)));
		assertThat(points[0].getY(), is(closeTo(p1.getNorth() + 30, 0.2)));
		
		assertThat(points[2].getX(), is(closeTo(p1.getEast() + 20, 0.2)));
		assertThat(points[2].getY(), is(closeTo(p1.getNorth() - 20, 0.2)));
	}
	
	@Test(timeout = 1000)
	public void testNorthEastExtension(){
		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(500,500);
		list.add(p1);
		GPSkPoint p2 = getPoint(525, 525);
		list.add(p2);
		PolygonCrossroad pc = createPG(list);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p1);
		pcc.firstStep();
		int counter = 0;
		while(pcc.extend()){
			counter++;
		}
		assertThat(counter, equalTo(1));
		assertThat(pcc.getContainingPoints(), hasSize(2));
		assertThat(pcc.getContainingPoints(), hasItem(p1));
		assertThat(pcc.getContainingPoints(), hasItem(p2));
		Point[] points = pcc.getPolygon().getPoints();
		assertThat(points[0].getX(), is(closeTo(p1.getEast() - 20, 0.2)));
		assertThat(points[0].getY(), is(closeTo(p1.getNorth() + 30, 0.2)));
		assertThat(points[2].getX(), is(closeTo(p1.getEast() + 30, 0.2)));
		assertThat(points[2].getY(), is(closeTo(p1.getNorth() - 20, 0.2)));
	}
	
	@Test(timeout = 1000)
	public void testSouthEastExtension(){
		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(800, 600);
		GPSkPoint p2 = getPoint(825, 575);
		list.add(p1);
		list.add(p2);
		PolygonCrossroad pc = createPG(list);
		PolygonCrossroad.Crossroad pcc = pc.new Crossroad(p1);
		pcc.firstStep();
		int counter = 0;
		while(pcc.extend()){
			counter++;
		}
		assertThat(counter, equalTo(1));
		assertThat(pcc.getContainingPoints(), hasItem(p1));
		assertThat(pcc.getContainingPoints(), hasItem(p2));
		assertThat(pcc.getContainingPoints(), hasSize(2));
		Point[] points = pcc.getPolygon().getPoints();
		assertThat(points[0].getX(), is(closeTo(p1.getEast() - 20, 0.2)));
		assertThat(points[0].getY(), is(closeTo(p1.getNorth() + 20, 0.2)));
		assertThat(points[2].getX(), is(closeTo(p1.getEast() + 30, 0.2)));
		assertThat(points[2].getY(), is(closeTo(p1.getNorth() - 30, 0.2)));
	}
	
	@Test(timeout = 1000)
	public void testSouthWestExtension(){
		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(999, 999);
		GPSkPoint p2 = getPoint(975, 975);
		list.add(p1);
		list.add(p2);
		PolygonCrossroad pc = createPG(list);
		Crossroad pcc = pc.new Crossroad(p1);
		pcc.firstStep();
		int counter = 0;
		while(pcc.extend()){
			counter++;
		}
		assertThat(counter, equalTo(1));
		assertThat(pcc.getContainingPoints(), hasItem(p1));
		assertThat(pcc.getContainingPoints(), hasItem(p2));
		assertThat(pcc.getContainingPoints(), hasSize(2));
		Point[] points = pcc.getPolygon().getPoints();
		assertThat(points[0].getX(), is(closeTo(p1.getEast() - 30, 0.2)));
		assertThat(points[0].getY(), is(closeTo(p1.getNorth() + 20, 0.2)));
		assertThat(points[2].getX(), is(closeTo(p1.getEast() + 20, 0.2)));
		assertThat(points[2].getY(), is(closeTo(p1.getNorth() - 30, 0.2)));
	}
	


	@Test(timeout = 2500)
	public void testExtend2() {

		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(100, 100);
		list.add(p1);

		GPSkPoint p2 = getPoint(500, 500);
		list.add(p2);
		PolygonCrossroad pc = createPG(list);
		PolygonCrossroad.Crossroad pcc1 = pc.new Crossroad(p1);
		pcc1.firstStep();
		while (pcc1.extend())
			;
		assertThat(pcc1.getContainingPoints(), hasSize(1));

		PolygonCrossroad.Crossroad pcc2 = pc.new Crossroad(p2);
		pcc2.firstStep();
		while (pcc2.extend())
			;
		assertThat(pcc2.getContainingPoints(), hasSize(1));

	}

	@Test(timeout = 3000)
	public void testExtend3() {
		List<GPSkPoint> list = new LinkedList<>();
		GPSkPoint p1 = getPoint(350, 50);
		list.add(p1);
		list.add(getPoint(335, 56));
		list.add(getPoint(345, 42));
		list.add(getPoint(352, 56));
		GPSkPoint p2 = getPoint(357, 77);
		list.add(p2);
		GPSkPoint p3 = getPoint(372, 49);
		list.add(p3);
		GPSkPoint p4 = getPoint(324, 24);
		list.add(p4);
		GPSkPoint p5 = getPoint(319, 18);
		list.add(p5);
		GPSkPoint p6 = getPoint(309, 8);
		list.add(p6);
		GPSkPoint p7 = getPoint(392, 51);
		list.add(p7);

		PolygonCrossroad pg = createPG(list);
		PolygonCrossroad.Crossroad pcc = pg.new Crossroad(p1);
		pcc.firstStep();
		assertThat(pcc.getContainingPoints(), hasSize(4));

		assertTrue(pcc.extend());

		assertThat(pcc.getContainingPoints(), hasSize(6));
		assertThat(pcc.getContainingPoints(), hasItem(p2));
		assertThat(pcc.getContainingPoints(), hasItem(p3));

		while (pcc.extend());

		assertThat(pcc.getContainingPoints(), not(hasItem(p7)));

	}

	private PolygonCrossroad createPG(List<GPSkPoint> pointList) {
		EnvironmentBuilder builder = new EnvironmentBuilder(pointList);
		builder.detectBounds().useProjection(proj);
		GPSkEnvironment env = builder.build();
		env.activatePointIndex();
		PolygonCrossroad pg = new PolygonCrossroad();
		pg.setInput(env);

		return pg;
	}

	private GPSkPoint getPoint(double x, double y) {
		
		GPSkPoint latLon = (GPSkPoint) proj.toLatLon(x, y);
		proj.setProjection(latLon);
		
		return latLon;
	}

}
