/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.gpsk.algorithms.AbstractFilter.ThreadInformation;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;

import static org.junit.Assert.*;

/**
 * @author Christian Windolf
 * 
 */
public class FilterFactoryTest {
	FilterFactory ff = FilterFactory.getInstance();

	List<GPSkPoint> points;

	/**
	 * @throws Exception
	 * 
	 */
	public FilterFactoryTest() throws Exception {
		ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(
				new EnvironmentImport());
		ci.setFile(new File(ResourceManager.localFolder,
				"test/moabit-klein.gpx"));
		GPSkEnvironment env = ci.call();
		points = env.getPoints();
	}

	@Test
	public void testFactory() {
		ThreadInformation ti = new ThreadInformation(1, 1, 0, points.size(),
				points.size());

		AbstractFilter<GPSkPoint> f = ff.<GPSkPoint> createNewFilter(ti,
				points, Filter.class, this);
		assertTrue(f.getClass().equals(Filter.class));
	}

	public class Filter extends AbstractFilter<GPSkPoint> {

		/**
		 * @param data
		 * @param ti
		 */
		public Filter(
				List<GPSkPoint> data,
				de.fub.agg2graph.gpsk.algorithms.AbstractFilter.ThreadInformation ti) {
			super(data, ti);
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public List<GPSkPoint> call() throws Exception {
			// TODO Auto-generated method stub
			return null;
		}

	}

}
