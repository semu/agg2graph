/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import static de.fub.agg2graph.gpsk.Contants.testFolder;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import javax.xml.bind.JAXBException;

import org.apache.commons.jxpath.JXPathContext;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fub.agg2graph.gpsk.beans.DataSet;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.ImmutablePointContainer;
import de.fub.agg2graph.gpsk.boundary.DataSetFactory;
import de.fub.agg2graph.gpsk.boundary.Point;
import de.fub.agg2graph.gpsk.io.SourceFile;
import de.fub.agg2graph.gpsk.logic.UTMProjection;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.WptType;

/**
 * @author Christian Windolf
 *
 */
public class CircleProcessorTest {
	private static GpxType gpx;
	private static SourceFile sc;
	private static JXPathContext context;
	private static GPSkPoint center;
	
	@BeforeClass
	public static void setUp() throws JAXBException{
		FileImport fileImport = new FileImport();
		sc = new SourceFile(testFolder.resolve("neighbourhoodTest01.gpx"));
		gpx = fileImport.parseFile(sc.getPath());
		context = JXPathContext.newContext(gpx);
		WptType tmpCenter = (WptType) context.getValue("//wpt[name='center']");
		center = Point.fromGPX(tmpCenter);
	}

	
	/**
	 * 
	 */
	public CircleProcessorTest() {
		
	}
	
	DataSetFactory fac;
	DataSet ds;
	ImmutablePointContainer ipc;
	Index<GPSkPoint> index;
	
	@Before 
	public void startUp(){
		fac = new DataSetFactory();
		ds = fac.createDataSet(gpx, sc);
		ipc = (ImmutablePointContainer) ds;
		index = ipc.getOrCreatePointIndex();
		ipc.setProjection(new UTMProjection(ipc.getBounds()));
	}
	
	@Test
	public void testNotContainingItSelf(){
		
		for(GPSkPoint p : ipc.getPoints()){
			CircleProcessor cp = new CircleProcessor(p, 50);
			assertThat(cp.getResults(), not(hasItem(p)));
		}
	}
	
	@Test
	public void testCenter(){
		CircleProcessor cp = new CircleProcessor(center, 60);
		assertThat(cp.getResults(), hasSize(3));
	}
	
	@After
	public void tearDown(){
		ipc.returnPointIndex(index);
	}
	
	@Test
	public void testNoise(){
		WptType tmpNoise = (WptType) context.getValue("//wpt[name='noise']");
		GPSkPoint noise = Point.fromGPX(tmpNoise);
		CircleProcessor cp = new CircleProcessor(center, 40);
		assertThat(cp.getResults(), hasSize(0));
	}

}
