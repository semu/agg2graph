/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.File;

import javax.xml.bind.JAXBException;

import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.boundary.Point;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.WptType;

/**
 *
 * @author Christian Windolf
 */
public class UtilsTest {
    
    GpxType gpx;
    JXPathContext context;
    
    public UtilsTest() throws JAXBException{
        File f = new File("test/gpsk/midPoint01.gpx");
        FileImport fi = new FileImport();
        gpx = fi.parseFile(f);
        context = JXPathContext.newContext(gpx);
    }
    
    @Test
    public void testMidPoint(){
        WptType wp1 = (WptType) context.getValue("//wpt[name='P1']");
        WptType wp2 = (WptType) context.getValue("//wpt[name='P2']");
        
        GPSkPoint p1 = Point.fromGPX(wp1);
        GPSkPoint p2 = Point.fromGPX(wp2);
        
        
        double bearing = PointUtils.bearingToRadians(p1, p2);
        
        
        for(int i = 100; i < 1500; i += 100){
            double[] latlon = PointUtils.destinationByBearingAndDistance(p1, bearing, i);
            System.out.println(i + ":  " + latlon[0] + "," + latlon[1]);
        }
        
        WptType wp4 = (WptType) context.getValue("//wpt[name='P4']");
        
        GPSkPoint p4 = Point.fromGPX(wp4);
        bearing = PointUtils.bearingToRadians(p4, p1);
        for(int i = 100; i < 1500; i += 100){
            double[] latlon = PointUtils.destinationByBearingAndDistance(p1, bearing, i);
            System.out.println(i + ":  " + latlon[0] + "," + latlon[1]);
        }
                
    }
    
    @Test
    public void testInsideBounds(){
    	double[] bounds = new double[]{52, 13, 54, 14};
    	GPSkPoint inside = new GPSkPoint(52.5, 13.4);
    	assertTrue(PointUtils.insideBounds(inside, bounds));
    	GPSkPoint outsideNorth = new GPSkPoint(55, 13.5);
    	assertFalse(PointUtils.insideBounds(outsideNorth, bounds));
    	GPSkPoint outsideWest = new GPSkPoint(53, 9);
    	assertFalse(PointUtils.insideBounds(outsideWest, bounds));
    	GPSkPoint outsideBoth = new GPSkPoint(59, 18);
    	assertFalse(PointUtils.insideBounds(outsideBoth, bounds));
    }
    

}
