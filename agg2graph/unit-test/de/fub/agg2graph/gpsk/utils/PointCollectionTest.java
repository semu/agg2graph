/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.utils;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.boundary.Point;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.WptType;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static de.fub.agg2graph.gpsk.utils.UtilConstants.*;
import java.util.Iterator;



/**
 *
 * @author Christian Windolf
 */
public class PointCollectionTest {
    
    @Test
    public void testBoundDetection() throws JAXBException{
        File f = new File("test/gpsk/projectionTest01.gpx");
        FileImport fi = new FileImport();
        GpxType gpx = fi.parseFile(f);
        JXPathContext context = JXPathContext.newContext(gpx);
        Iterator it = context.iterate("//wpt");
        List<GPSkPoint> list = new LinkedList<>();
        while(it.hasNext()){
            GPSkPoint p = Point.fromGPX((WptType) it.next());
            list.add(p);
        }
        
        double[] bounds = PointCollection.getBounds(list);
        
        assertTrue(bounds.length == 4);
        
        WptType southWest = (WptType) context.getValue("//wpt[name='southWest']");
        WptType northEast = (WptType) context.getValue("//wpt[name='northEast']");
        
        assertThat(southWest.getLat().doubleValue(), is(closeTo(bounds[SOUTHWEST_LAT], 0.0001)));
        assertThat(southWest.getLon().doubleValue(), is(closeTo(bounds[SOUTHWEST_LON], 0.0001)));
        assertThat(northEast.getLat().doubleValue(), is(closeTo(bounds[NORTHEAST_LAT], 0.0001)));
        assertThat(northEast.getLon().doubleValue(), is(closeTo(bounds[NORTHEAST_LON], 0.0001)));
        
    }
    @Test
    public void testBoundDetection2(){
    	double[] array = PointCollection.getBounds(Collections.<GPSkPoint>emptyList());
    	for(double d : array){
    		assertTrue(Double.isNaN(d));
    	}
    }
    
    @Test
    public void testBoundDetection3(){
    	List<GPSkPoint> list = Collections.singletonList(new GPSkPoint(52,14));
    	double[] array = PointCollection.getBounds(list);
    	assertTrue(PointUtils.insideBounds(list.get(0), array));
    	assertThat(list.get(0).getLat(), is(closeTo(52, 0.005)));
    	assertThat(list.get(0).getLat(), is(closeTo(52, 0.005)));
    	assertThat(list.get(0).getLon(), is(closeTo(14, 0.005)));
    	assertThat(list.get(0).getLon(), is(closeTo(14, 0.005)));
    }
    
    
    
    
}
