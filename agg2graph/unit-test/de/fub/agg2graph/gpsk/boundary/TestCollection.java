/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.boundary;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.BeforeClass;
import org.junit.Test;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;

/**
 * @author Christian Windolf
 *
 */
public class TestCollection {
	public static GpxType gpx1;
	public static GpxType gpx2;
	public static GpxType gpx3;
	

	
	@BeforeClass
	public static void setUp() throws JAXBException{
		FileImport fi = new FileImport();
		Path folder = Paths.get("test/gpsk");
		gpx1 = fi.parseFile(folder.resolve("container01.gpx"));
		gpx2 = fi.parseFile(folder.resolve("container02.gpx"));
		gpx3 = fi.parseFile(folder.resolve("container03.gpx"));
	}
	
	@Test
	public void testCorrectSize() {
		List<GPSkPoint> pList1 = Container.extractPoints(gpx1);
		assertThat(pList1, hasSize(0));
		List<GPSkSegment> sList1 = Container.extractSegments(gpx1);
		assertThat(sList1, hasSize(3));
		
		List<GPSkPoint> pList2 = Container.extractPoints(gpx2);
		assertThat(pList2, hasSize(0));
		List<GPSkSegment> sList2 = Container.extractSegments(gpx2);
		assertThat(sList2, hasSize(3));
		
		List<GPSkPoint> pList3 = Container.extractPoints(gpx3);
		assertThat(pList3, hasSize(5));
		List<GPSkSegment> sList3 = Container.extractSegments(gpx3);
		assertThat(sList3, hasSize(0));
	}

}
