/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.io;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.io.ConvertedImport;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.io.GPSPointImport;
import de.fub.agg2graph.io.GPSSegmentImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.WptType;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXBException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.apache.commons.jxpath.CompiledExpression;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Before;
import org.junit.Test;

import static de.fub.agg2graph.gpsk.utils.UtilConstants.*;
import static java.lang.Math.min;
import static java.lang.Math.max;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class InputTest {
    
    @Before
    public void setUp(){
        ResourceManager.envOK();
    }
 
    @Test
    public void testInput() throws URISyntaxException, JAXBException, Exception{
        File f = new File("test/input/berlin/1058439-0001.gpx");
//        File f = new File("/home/doggy/moabit.gpx");
        List<GPSSegment> list1 = GPXReader.getSegments(f);
        ConvertedImport<List<GPSSegment>> ci = 
                new ConvertedImport<List<GPSSegment>>(new GPSSegmentImport(), f);
        
        List<GPSSegment> list2 = ci.call();
        assertThat(list2, hasSize(list1.size()));
        
        List<GPSPoint> list3 = GPXReader.getOrderedPoints(f);
        ConvertedImport<List<GPSPoint>> ci2 = 
                new ConvertedImport<>(new GPSPointImport(), f);
        List<GPSPoint> list4 = ci2.call();
        assertThat(list4, hasSize(list3.size()));
    }
    
    @Test
    public void testBounds() throws Exception {
    	File f = new File(ResourceManager.localFolder, "test");
    	FileImport fi = new FileImport();
    	
    	for(File f2 : f.listFiles()){
    		if(f2.getName().endsWith(".gpx")){
    			System.out.println("processing: " + f2.getName());
    			GpxType gpx = fi.parseFile(f2);
    			JXPathContext context = JXPathContext.newContext(gpx);
    			double[] b1 = bounds((Iterator<WptType>) context.iterate("//wpt"));
    			double[] b2 = bounds((Iterator<WptType>) context.iterate("//rtept"));
    			double[] b3 = bounds((Iterator<WptType>) context.iterate("//trkpt"));
    			
    			double[] bounds1 = new double[]{
    					min(b1[SOUTHWEST_LAT], min(b2[SOUTHWEST_LAT], b3[SOUTHWEST_LAT])),
    					min(b1[SOUTHWEST_LON], min(b2[SOUTHWEST_LON], b3[SOUTHWEST_LON])),
    					max(b1[NORTHEAST_LAT], max(b2[NORTHEAST_LAT], b3[NORTHEAST_LAT])),
    					max(b1[NORTHEAST_LON], max(b2[NORTHEAST_LON], b3[NORTHEAST_LON]))
    			};
    			ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
    			ci.setFile(f2);
    			GPSkEnvironment env = ci.call();
    			double[] bounds2 = env.getBounds();
    			assertThat(bounds1.length, equalTo(bounds2.length));
    			for(int i = 0; i < bounds1.length; i++){
    				assertThat(bounds1[i], closeTo(bounds2[i], 0.001));
    			}
    			env.detectBounds();
    			double[] bounds3 = env.getBounds();
    			assertThat(bounds3.length, equalTo(4));
    			for(int i = 0; i < bounds3.length; i++){
    				assertThat(bounds1[i], closeTo(bounds2[i], 0.001));
    			}
    			
    		}
    	}
    }
    
    private double[] bounds(Iterator<WptType> waypoints){
    	double[] bounds = new double[4];
    	bounds[SOUTHWEST_LAT] = Double.MAX_VALUE;
    	bounds[SOUTHWEST_LON] = Double.MAX_VALUE;
    	
    	bounds[NORTHEAST_LAT] = Double.MIN_VALUE;
    	bounds[NORTHEAST_LON] = Double.MIN_VALUE;
    	while(waypoints.hasNext()){
    		WptType wp = waypoints.next();
    		double lat = wp.getLat().doubleValue();
    		double lon = wp.getLon().doubleValue();
    		
    		if(lat < bounds[SOUTHWEST_LAT]){
    			bounds[SOUTHWEST_LAT] = lat;
    		}
    		
    		if(lon < bounds[SOUTHWEST_LON]){
    			bounds[SOUTHWEST_LON] = lon;
    		}
    		
    		if(lat > bounds[NORTHEAST_LAT]){
    			bounds[NORTHEAST_LAT] = lat;
    		}
    		
    		if(lon > bounds[NORTHEAST_LON]){
    			bounds[NORTHEAST_LON] = lon;
    		}
    	}
    	
    	return bounds;
    }
}
