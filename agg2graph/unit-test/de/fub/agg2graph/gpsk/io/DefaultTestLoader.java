/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.io;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import java.io.File;
import java.io.FilenameFilter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class DefaultTestLoader {

    public static List<GPSkEnvironment> list;

    public static List<GPSkEnvironment> load() {
        List<GPSkEnvironment> tmpList = new LinkedList<>();
        File folder = new File("test/gpsk");
        String[] gpxFiles = folder.list(new FilenameFilter() {

            @Override
            public boolean accept(File file, String string) {
                if (string.endsWith(".gpx")) {
                    return true;
                } else {
                    return false;
                }

            }
        });
        for(int i = 0; i < gpxFiles.length; i++){
            try {
                ConvertedImport<GPSkEnvironment> ci = new ConvertedImport<>(new EnvironmentImport());
                ci.setFile(new File(folder.getPath() + File.separator + gpxFiles[i]));
                GPSkEnvironment env = ci.call();
                tmpList.add(env);
                System.out.println(env);
            } catch (JAXBException ex) {
                System.out.println("failed to read " + gpxFiles[i]);
                ex.printStackTrace();
            } catch (Exception ex){
                System.out.println("failed to read " + gpxFiles[i]);
                ex.printStackTrace();;
            }
        }
        return tmpList;
        
    }
    
    static{
        list = load();
    }
}
