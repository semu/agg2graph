/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.io;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import static de.fub.agg2graph.gpsk.boundary.Point.*;
import de.fub.agg2graph.gpsk.boundary.Segment;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class TestBoundary {
    
    @Test
    public void testPoints(){
        GPSkEnvironment env = DefaultTestLoader.list.get(0);
        for(GPSkPoint p : env.getPoints()){
            GPSkPoint p2 = fromGPX(toGPX(p));
            assertEquals(p, p2);
        }
    }
    
    @Test
    public void testSegments(){
        for(GPSkEnvironment env : DefaultTestLoader.list){
            for(GPSkSegment seg : env.getSegments()){
                int size = seg.size();
                assertEquals(size, Segment.toGPX(seg).getRtept().size());
            }
        }
    }
    
}
