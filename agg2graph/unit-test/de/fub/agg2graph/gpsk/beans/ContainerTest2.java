/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import static java.lang.Double.isNaN;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.nio.file.Paths;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fub.agg2graph.gpsk.boundary.DataSetFactory;
import de.fub.agg2graph.gpsk.io.SourceFile;
import de.fub.agg2graph.gpsk.logic.Projection;
import de.fub.agg2graph.gpsk.logic.UTMProjection;
import de.fub.agg2graph.gpsk.utils.PointCollection;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;

/**
 * @author Christian Windolf
 *
 */

public class ContainerTest2 {
	private static FileImport fileImport;
	private static DataSetFactory dsf;
	
	private static GpxType gpx;
	private ImmutablePointContainer ipc;
	
	@BeforeClass
	public static void startTest() throws JAXBException{
		fileImport = new FileImport();
		gpx = fileImport.parseFile(Paths.get("res/GpxFiles/berlin.gpx"));
		dsf = new DataSetFactory();
	}
	
	
	
	@Before
	public void setUp(){
		ipc = (ImmutablePointContainer) dsf.createDataSet(gpx, new SourceFile(Paths.get("res/GpxFiles/Berlin.gpx")));
	}
	
	@Test
	public void testProjection(){
		Projection projection = new UTMProjection(ipc.getBounds());
		ipc.setProjection(projection);
		assertEquals(projection, projection);
		for(GPSkPoint point : ipc.getPoints()){
			assertFalse(isNaN(point.getEast()));
			assertFalse(isNaN(point.getNorth()));
		}
	}
	
	
	@Test
	public void testBoundary(){
		double[] bounds = ipc.getBounds();
		assertTrue(PointCollection.insideBounds(ipc.getPoints(), bounds));
	}
	
	private class Listener implements PropertyChangeListener{
		private PropertyChangeEvent pce;

		/* (non-Javadoc)
		 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
		 */
		@Override
		public void propertyChange(PropertyChangeEvent evt) {

			pce = evt;
			
		}
		
	}

}
