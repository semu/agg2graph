/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import de.fub.agg2graph.gpsk.io.DefaultTestLoader;
import java.util.List;
import java.util.ListIterator;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Test for the GPSkSegment class
 * @author Christian Windolf, christianwindolf@web.de
 */
public class SegmentTest {
    
    
    /**
     * Tests that the equals method delivers correct results
     */
    @Test
    public void testEquals(){
        List<GPSkEnvironment> list  = DefaultTestLoader.list;
        for(GPSkEnvironment env : list){
            for(GPSkSegment seg : env.getSegments()){
                assertThat(seg, equalTo(seg));
                assertTrue(seg.equals(seg));
            }
            if(!env.getSegments().isEmpty()){
                ListIterator<GPSkSegment> li = env.getSegments().listIterator();
                GPSkSegment first = li.next();
                while(li.hasNext()){
                    GPSkSegment seg = li.next();
                    assertThat(seg, not(equalTo(first)));
                    assertFalse(seg.equals(first));
                }
            }
        }
        
        
    }
    
}
