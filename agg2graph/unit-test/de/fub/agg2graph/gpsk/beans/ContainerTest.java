/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.JAXBException;

import org.junit.BeforeClass;
import org.junit.Test;

import de.fub.agg2graph.gpsk.boundary.Container;
import de.fub.agg2graph.gpsk.boundary.DataSetFactory;
import de.fub.agg2graph.gpsk.boundary.TestCollection;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class ContainerTest {
    
	
	@BeforeClass
	public static void setUp() throws JAXBException{
		TestCollection.setUp();
	}
	
	@Test
	public void testDataSetFactory1(){
		DataSetFactory dsf = new DataSetFactory();
		DataSet ds = dsf.createDataSet(TestCollection.gpx1, null);
		assertTrue(ds instanceof ImmutableSegmentContainer);
		ImmutableSegmentContainer isc = (ImmutableSegmentContainer) ds;
		assertThat(isc.getSegments(), hasSize(3));
		
	}
	
	@Test
	public void testDataSetFactory2(){
		DataSetFactory dsf = new DataSetFactory();
		DataSet ds = dsf.createDataSet(TestCollection.gpx2, null);
		assertTrue(ds instanceof ImmutableSegmentContainer);
		ImmutableSegmentContainer isc = (ImmutableSegmentContainer) ds;
		assertThat(isc.getSegments(), hasSize(3));
	}
	
	@Test
	public void testDataSetFactory3(){
		DataSetFactory dsf = new DataSetFactory();
		DataSet ds = dsf.createDataSet(TestCollection.gpx3, null);
		assertTrue(ds instanceof ImmutablePointContainer);
		ImmutablePointContainer ipc = (ImmutablePointContainer) ds;
		assertThat(ipc.getPoints(), hasSize(5));
		
	}
}
