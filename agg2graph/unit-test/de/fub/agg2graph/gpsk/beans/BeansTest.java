/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import de.fub.agg2graph.gpsk.logic.DistanceFactory;
import de.fub.agg2graph.gpsk.logic.PointDistance;
import de.fub.agg2graph.gpsk.utils.PointUtils;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import org.junit.Before;


import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Christian Windolf
 */
public class BeansTest {
    public List<GPSkPoint> testPoints = new LinkedList<GPSkPoint>();
    PointDistance pd;
    
    public BeansTest(){
        pd = DistanceFactory.getInstance().createNewInstance();
    }
    
    @Before
    public void setUp(){
        
        
        testPoints.add(new GPSkPoint("südlich U Birkenstraße", 52.531792, 13.341291));
        testPoints.add(new GPSkPoint("U Birkenstraße", 52.532366, 13.341248));
        testPoints.add(new GPSkPoint("östlich U Birkenstraße", 52.532373, 13.342053));
        testPoints.add(new GPSkPoint("westlich U Birkenstraße", 52.532360, 13.340293));
        
        
    }
    
    
    
    @Test
    public void testBearing(){
        double bearing01 = testPoints.get(0).bearingTo(testPoints.get(1));
        assertThat(bearing01, is(closeTo(360, 3.0)));
        double bearing02 = testPoints.get(1).bearingTo(testPoints.get(0));
        assertThat(bearing02, is(closeTo(180, 3.0)));
        
        double bearing03 = testPoints.get(1).bearingTo(testPoints.get(2));
        assertThat(bearing03, is(closeTo(90, 3)));
        double bearing04 = testPoints.get(2).bearingTo(testPoints.get(1));
        assertThat(bearing04, is(closeTo(270, 3)));
        
        double bearing05 = testPoints.get(1).bearingTo(testPoints.get(3));
        assertThat(bearing05, is(closeTo(270, 3)));
        double bearing06 = testPoints.get(3).bearingTo(testPoints.get(1));
        assertThat(bearing06, is(closeTo(90, 3)));
        
    }
    @Ignore("interface changed, this test will fail and it does not matter")
    @Test
    public void testSegments(){
        GPSkSegment seg01 = new GPSkSegment();
        for(GPSkPoint point : testPoints){
            seg01.add(point);
        }
        GPSkSegment seg02 = new GPSkSegment();
        seg02.addAll(testPoints);
        
        assertThat(seg01.calculateLength(), is(closeTo(seg02.calculateLength(), 0.05)));
        
    }
    
    @Test
    public void testAgg2GraphVersion(){
        GPSkSegment s = new GPSkSegment();
        for(GPSkPoint p : testPoints){
            s.add(p);
        }
        GPSSegment agg2graph = s.agg2graphVersion();
        assertThat(agg2graph, hasSize(s.size()));
    }

    @Test
    public void testMidPoint(){
        GPSkPoint p1 = testPoints.get(1);
        GPSkPoint p2 = testPoints.get(2);
        
        double eps = 1000;
        GPSkPoint midPoint = PointUtils.midPointGeneration(p1, p2, eps);
        
        assertThat(p2.getLon() - p2.getLon(), is(closeTo(Math.toDegrees(p1.distanceTo(p2) / GPSkPoint.RADIUS), 0.0005)));
        assertThat(pd.calculate(p1, midPoint), is(closeTo(eps, 1.0)));
        
    }
    
    @Test
    public void testMidPoint2(){
        GPSkPoint p1 = testPoints.get(0);
        GPSkPoint p2 = testPoints.get(1);
        PointDistance pd = DistanceFactory.getInstance().createNewInstance();
        double bearing = PointUtils.bearingToRadians(p1, p2);
        double[] latlon = PointUtils.destinationByBearingAndDistance(p1, bearing, 50);
        GPSkPoint midPoint = new GPSkPoint(latlon[0], latlon[1]);
        System.out.println(midPoint);
        System.out.println("distance: " + pd.calculate(p1, p2));
    
    }
    
    @Test
    public void testMidPoint3(){
        GPSkPoint p1 = testPoints.get(2);
        GPSkPoint p2 = testPoints.get(3);
        
        double[] testDistances = new double[]{
            50, 100, 200, 225, 400, 600, 500, 1000, 2000, 900
        };
        for(int i = 0; i < testDistances.length; i++){
            GPSkPoint midPoint = PointUtils.midPointGeneration(p1, p2, testDistances[i]);
            double tolerance = testDistances[i] * 0.02;
            assertThat(pd.calculate(p1, midPoint), is(closeTo(testDistances[i], tolerance)));
        }
        
        GPSkPoint midPoint2 = PointUtils.midPointGeneration(p1, p2, pd.calculate(p1, p2));
        assertThat(p2.distanceTo(midPoint2), is(closeTo(0, 1.0)));
        assertThat(p1.distanceTo(p2), is(closeTo(p1.distanceTo(p2), 1.0)));
    }
}
