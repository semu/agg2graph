/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import de.fub.agg2graph.gpsk.boundary.Segment;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.RteType;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static java.lang.Double.isNaN;
import java.util.LinkedList;
import org.apache.commons.jxpath.JXPathContext;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.junit.Ignore;

import static org.junit.Assert.assertTrue;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@RunWith(Parameterized.class)
public class EnvBuilderTest {
    GpxType gpx;
    
    public EnvBuilderTest(File f) throws JAXBException{
        FileImport fi = new FileImport(de.fub.agg2graph.schemas.gpsk.ObjectFactory.class);
        gpx = fi.parseFile(f);
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> parameters(){
        File f = new File("test/gpsk");
        String[] files = f.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                if(name.endsWith(".gpx")){
                    return true;
                }
                return false;
            }
        });
        
        Object[][] array = new Object[files.length][1];
        for(int i = 0; i < files.length; i++){
            array[i][0] = new File(f.getPath() + File.separator + files[i]);
        }
        
        return Arrays.asList(array); 
    }
    
    
    /**
     * Tests that all points have a projection
     */
    @Test
    public void testProjection(){
        EnvironmentBuilder builder = new EnvironmentBuilder();
        builder.setProjection(EnvironmentBuilder.PROJECTION_ON);
        EnvironmentImport ei = new EnvironmentImport(builder);
        GPSkEnvironment env = ei.convert(gpx);
        List<GPSkPoint> points = env.getPoints();
        for(GPSkPoint p : points){
            assertFalse(isNaN(p.getEast()));
            assertFalse(isNaN(p.getNorth()));
        }
        assertNotNull(env.getProjection());
    }
    
    
    /**
     * does the same like {@link EnvBuilderTest#testProjection() } ...
     */
    @Test
    public void testProjection2(){
        EnvironmentBuilder builder = new EnvironmentBuilder();
        builder.setProjection(EnvironmentBuilder.PROJECTION_IF_THERE_IS_NONE);
        EnvironmentImport ei = new EnvironmentImport(builder);
        GPSkEnvironment env = ei.convert(gpx);
        assertThat(env.getBounds()[0], greaterThan(-90d));
        assertThat(env.getBounds()[1], greaterThan(-180d));
        assertThat(env.getBounds()[2], lessThan(90d));
        assertThat(env.getBounds()[3], lessThan(180d));
        
        assertThat(env.getBounds()[0], lessThan(env.getBounds()[2]));
        assertThat(env.getBounds()[1], lessThan(env.getBounds()[3]));
        List<GPSkPoint> points = env.getPoints();
        for(GPSkPoint p : points){
            assertFalse(isNaN(p.getEast()));
            assertFalse(isNaN(p.getNorth()));
        }
        assertNotNull(env.getProjection());
    }
    
    
    /**
     * Test that each segment, that has been added can also be found in the environment
     */
    @Test
    public void testSegments(){
        JXPathContext context = JXPathContext.newContext(gpx);
        if(gpx.getTrk().isEmpty()) return;
        Collection c = (Collection) context.getValue("//rte");
        List<GPSkSegment> list = new LinkedList<>();
        for(Object o : c){
            RteType route = (RteType) o;
            GPSkSegment seg = Segment.fromGPX(route);
            list.add(seg);
        }
        
        EnvironmentBuilder builder = new EnvironmentBuilder(list);
        GPSkEnvironment env = builder.build();
        for(GPSkSegment seg : list){
            boolean found = false;
            for(GPSkSegment seg2 : env.getSegments()){
                if(seg.equals(seg2)) found = true;
            }
            assertTrue(found);
            
        }
    }
}
