/*
 * Copyright 2013 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;

public class GpxTransform {

	@Before
	public void setUp() {
		ResourceManager.envOK();
	}

	@Test
	public void testInput() throws URISyntaxException, JAXBException, Exception {
		File f = new File("test/input/berlin/1058439-0001.gpx");
		List<GPSSegment> list1 = GPXReader.getSegments(f);
		ConvertedImport<List<GPSSegment>> ci = new ConvertedImport<List<GPSSegment>>(
				new GPSSegmentImport(), f);

		List<GPSSegment> list2 = ci.call();

		List<GPSPoint> list3 = GPXReader.getOrderedPoints(f);
		ConvertedImport<List<GPSPoint>> ci2 = new ConvertedImport<>(
				new GPSPointImport(), f);
		List<GPSPoint> list4 = ci2.call();

		File out = new File("test/output/berlin/1058439-0001.csv");

		GpxToCsvConverter.convertGpxToCsv(f, out);
		
		GpxToCsvConverter.convertGpxFolderToCsv("test/gpseval/input/tracks/bodily_powered/walking", "test/gpseval/input/tracks/bodily_powered/walking");
		GpxToCsvConverter.convertGpxFolderToCsv("test/gpseval/input/tracks/bodily_powered/bicycle", "test/gpseval/input/tracks/bodily_powered/bicycle");
		GpxToCsvConverter.convertGpxFolderToCsv("test/gpseval/input/tracks/motorized/bus", "test/gpseval/input/tracks/motorized/bus");
		GpxToCsvConverter.convertGpxFolderToCsv("test/gpseval/input/tracks/motorized/car", "test/gpseval/input/tracks/motorized/car");
		GpxToCsvConverter.convertGpxFolderToCsv("test/gpseval/input/tracks/motorized/train", "test/gpseval/input/tracks/motorized/train");
	}


}
