package de.fub.agg2graph.pt;

import org.junit.Test;

import com.infomatiq.jsi.Point;
import com.viniciusfortuna.transit.gtfs.Stop;

import junit.framework.TestCase;

public class StopTreeTest extends TestCase{
	@Test
	public void testInit() {
		StopTree st = new StopTree();
		st.init();
		Point testPoint = new Point(13.5F, 52.4F);
		st.getNearest(testPoint);
		Double dist = st.getNearestDistance(testPoint);
		System.out.println("The calculated distance to the next stop is: " + dist);
		Stop stop = st.getNearestStop(testPoint);
		System.out.println("The name of the next stop is: " + stop.name);
		testPoint = new Point(13.44F, 52.48F);
		st.getNearest(testPoint);
		dist = st.getNearestDistance(testPoint);
		System.out.println("The calculated distance to the next stop is: " + dist);
		stop = st.getNearestStop(testPoint);
		System.out.println("The name of the next stop is: " + stop.name);
		assertEquals(186, dist, 20);
	}

}
