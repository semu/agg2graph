package de.fub.agg2graph.gpseval.features;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpseval.data.Waypoint;

public class MaxAccelerationFeatureTest {

	private MaxAccelerationFeature mFeature;

	@Before
	public void setUp() throws Exception {
		mFeature = new MaxAccelerationFeature();
		addWaypoints();
	}

	private void addWaypoints() {
		String data[][] = new String[][] {
				new String[] { "1", "0", "0", "0", "0", "0", "0", "1", "2012-11-16T10:55:11.000Z" },
				new String[] { "1", "0", "0", "0", "0", "0", "0", "2", "2012-11-16T10:55:12.000Z" },
				new String[] { "1", "0", "0", "0", "0", "0", "0", "3", "2012-11-16T10:55:13.000Z" },
				new String[] { "2", "0", "0", "0", "0", "0", "0", "10", "2012-11-16T10:55:14.000Z" },
				new String[] { "2", "0", "0", "0", "0", "0", "0", "13", "2012-11-16T10:55:15.000Z" }, // <-- max Acc.
				new String[] { "2", "0", "0", "0", "0", "0", "0", "14", "2012-11-16T10:55:16.000Z" },
				new String[] { "3", "0", "0", "0", "0", "0", "0", "20", "2012-11-16T10:55:17.000Z" },
				new String[] { "3", "0", "0", "0", "0", "0", "0", "21", "2012-11-16T10:55:18.000Z" },
				new String[] { "3", "0", "0", "0", "0", "0", "0", "22", "2012-11-16T10:55:19.000Z" },
				new String[] { "4", "0", "0", "0", "0", "0", "0", "30", "2012-11-16T10:55:20.000Z" } };

		for (String[] singleData : data) {
			mFeature.addWaypoint(new Waypoint(singleData));
		}
	}

	@Test
	public void testGetResult() {
		double result = mFeature.getResult();
		assertEquals(3.0, result, 0);
	}

	@Test
	public void testReset() {
		double result = mFeature.getResult();
		
		mFeature.reset();
		addWaypoints();
		double result2 = mFeature.getResult();
		
		assertEquals(result2, result, 0);
	}

}
