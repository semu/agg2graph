package de.fub.agg2graph.gpseval.features;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpseval.data.Waypoint;

public class AvgPrecisionFeatureTest {

	private AvgPrecisionFeature mFeature;
	
	@Before
	public void setUp() throws Exception {
		mFeature = new AvgPrecisionFeature();
		addWaypoints();
	}

	private void addWaypoints() {
		String data[][] = new String[][] {
				new String[] { "1", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "1", "0", "0", "0", "0", "0", "5", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "1", "0", "0", "0", "0", "0", "10", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "2", "0", "0", "0", "0", "0", "5", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "2", "0", "0", "0", "0", "0", "10", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "2", "0", "0", "0", "0", "0", "5", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "3", "0", "0", "0", "0", "0", "5", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "3", "0", "0", "0", "0", "0", "15", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "3", "0", "0", "0", "0", "0", "5", "0", "2012-11-16T10:55:11.000Z" },
				new String[] { "4", "0", "0", "0", "0", "0", "10", "0", "2012-11-16T10:55:11.000Z" } };

		for (String[] singleData : data) {
			mFeature.addWaypoint(new Waypoint(singleData));
		}
	}

	@Test
	public void testGetResult() {
		double result = mFeature.getResult();
		assertEquals(7.0, result, 0);
	}

	@Test
	public void testReset() {
		double result = mFeature.getResult();
		
		mFeature.reset();
		addWaypoints();
		double result2 = mFeature.getResult();
		
		assertEquals(result2, result, 0);
	}

}
