package de.fub.agg2graph.gpseval.evaluator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.vividsolutions.jts.util.Assert;

public class ForestToGraphTreesTest {

	String path = "test/gpseval/unit-test/forests/";

	public String readFileToString(String filePath) throws IOException {
		String s = "";
		FileReader freader = new FileReader(filePath);
		BufferedReader br = new BufferedReader(freader);
		String line;
		while ((line = br.readLine()) != null) {
			s += line + "\n";
		}
		br.close();
		return s;
	}

	public List<String> readResultFileToString(String filePath)
			throws IOException {
		String s = readFileToString(filePath);
		return Arrays.asList(s.split(";\n"));
	}
	
	public void compareTwoFiles(String expectedTreesFile, String wekaForestStringFile) throws IOException {
		String wekaForestString = readFileToString(wekaForestStringFile);
		List<String> expectedTrees = readResultFileToString(expectedTreesFile);
		
		Forest forest = new Forest(wekaForestString);
		// System.out.println(forest);
		List<String> actualTrees = forest.getTrees();
		
		for (int i = 0; i < expectedTrees.size(); ++i) {
			Assert.equals(expectedTrees.get(i), actualTrees.get(i));
		}
	}

	@Test
	public void testInputForest1() throws IOException {
		compareTwoFiles(path + "forest1_graph.txt", path + "forest1.txt");
	}

	@Test
	public void testInputForest2() throws IOException {
		compareTwoFiles(path + "forest2_graph.txt", path + "forest2.txt");
	}
	
	@Test
	public void testInputForest3() throws IOException {
		compareTwoFiles(path + "forest3_graph.txt", path + "forest3.txt");
	}
	
	@Test
	public void testInputForest4() throws IOException {
		compareTwoFiles(path + "forest4_graph.txt", path + "forest4.txt");
	}
}
