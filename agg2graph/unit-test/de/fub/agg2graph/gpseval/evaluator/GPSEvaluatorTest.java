package de.fub.agg2graph.gpseval.evaluator;

import de.fub.agg2graph.gpseval.evaluator.GPSEvaluator;

public class GPSEvaluatorTest {
	
	static String[] path = {"unit-test/de/fub/agg2graph/gpseval/evaluator/config/"};

	public static void main(String[] args) {
		System.out.println("test");
		try {
			GPSEvaluator app = new GPSEvaluator();
			app.start(path);
			System.out.println("FOREST:\n"+app.getForests());
			System.out.println("DECISION TREE:\n"+app.getTrees());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
