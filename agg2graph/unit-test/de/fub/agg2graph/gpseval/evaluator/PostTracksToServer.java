package de.fub.agg2graph.gpseval.evaluator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class PostTracksToServer {
	
	static String server = "http://localhost:8080/gpseval-server/";
	//static String server = "http://projects.mi.fu-berlin.de/gpseval/";
	
	private static boolean sendFileToServer(File file) {
		
		String serverUrl = server + "GPSCapture/setCapture";
		
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost request = new HttpPost(serverUrl);
        
        //read text from file
        StringBuilder text = readFile(file);
        
        ArrayList<NameValuePair> postParameters;
        postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("capture", text.toString()));
        
        try {
			request.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        
        System.out.println("Send plain string:\"" + text.toString() + "\"");
        
        try {
        	HttpResponse response = httpclient.execute(request);
            int responseCode = response.getStatusLine().getStatusCode();
            // String result = EntityUtils.toString(response.getEntity());
            readRespone(response.getEntity().getContent());
            
            if (responseCode == 200) {
            	System.out.println("Server connection ok!");
            	return true;
            } else {
            	System.err.println("Server connection failed. Status code: " + responseCode);
            } 
        } catch (IOException e) {
        	e.printStackTrace();
        }
		
        return false;
	}
	
	private static StringBuilder readFile(File file) {

        StringBuilder text = new StringBuilder();
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
        	e.printStackTrace();
        }
        
        // System.out.println(text.toString());
        
        return text;
	}
	
	private static void readRespone(InputStream stream) {
		StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
        	e.printStackTrace();
        }
        System.out.println(text.toString());
	}
	
	private static void sendFiles(File... params) {

        for (File param : params) {
            if (sendFileToServer(param)) {
                // param.delete();
            }
        }
	}

	public static void main(String[] args) {
		String path = "test/gpseval/input/tracks_to_send/";
		File file = new File(path + "Bus-2014-09-09 11-48-11.936.csv");
		
		// readFile(file);
		sendFiles(file);
		
	}

}
