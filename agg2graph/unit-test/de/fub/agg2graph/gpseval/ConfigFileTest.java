package de.fub.agg2graph.gpseval;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpseval.data.filter.TrackFilter;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilter;
import de.fub.agg2graph.gpseval.features.Feature;

public class ConfigFileTest {
	ConfigFile mConfig;

	@Before
	public void setUp() throws Exception {
		Path p = Paths.get("test/gpseval/unit-test/config.xml");
		mConfig = new ConfigFile(p);
	}

	@Test
	public void testGetClassesFolderMapping() {
		Map<String, List<String>> map = mConfig.getClassesFolderMapping();
		assertEquals(2, map.size());

		// check class 1
		List<String> cls1 = map.get("Slow");
		assertNotNull(cls1);
		assertEquals(2, cls1.size());
		assertTrue(cls1.contains("./test/gpseval/input/tracks/slow"));
		assertTrue(cls1.contains("./test/gpseval/input/tracks/slow-2"));

		// check class 2
		List<String> cls2 = map.get("Fast");
		assertNotNull(cls2);
		assertEquals(1, cls2.size());
		assertTrue(cls2.contains("./test/gpseval/input/tracks/fast"));
	}

	@Test
	public void testGetFeatures() {
		// List of features the ConfigFile-obj must return!
		List<String> required = new ArrayList<>();
		required.add("MaxSpeed");
		required.add("AvgSpeed");
		required.add("Segments");
		required.add("AvgBearingChange");
		required.add("MinPrecision");
		required.add("MaxPrecision");
		required.add("AvgPrecision");
		required.add("AvgTransportationDistance");
		required.add("MaxAcceleration");

		List<Feature> features = mConfig.getFeatures();
		assertEquals(required.size(), features.size());

		// Check if each feature from the config file is returned only once.
		// Check if they have correct parameters.
		for (Feature f : features) {
			String id = f.getIdentifier();
			assertTrue(required.remove(id)); // False means: Feature is returned
												// more than once

			// check parameters
			if (id.equals("AvgBearingChange")) {
				int val = f.getIntParam("bearingChangeThreshold", 0);
				assertEquals(2, val);
			}
		}

		// If "required" does still contain elements then getFeature
		// didn't return all features!
		assertTrue(required.isEmpty());
	}

	@Test
	public void testGetTrackFilters() {
		List<TrackFilter> filters = mConfig.getTrackFilters();
		assertEquals(1, filters.size());
		TrackFilter filter = filters.get(0);
		assertEquals("LimitPerClass", filter.getIdentifier());
		assertEquals(1, filter.getIntParam("limit", 0));
	}

	@Test
	public void testGetWaypointFilters() {
		// List of waypoint filters the ConfigFile-obj must return!
		List<String> required = new ArrayList<>();
		required.add("Limit");
		required.add("MinTimeDiff");

		List<WaypointFilter> filters = mConfig.getWaypointFilters();
		assertEquals(2, filters.size());

		// Check if each waypoint filter from the config file is returned only
		// once.
		// Check if they have correct parameters.
		for (WaypointFilter f : filters) {
			String id = f.getIdentifier();
			assertTrue(required.remove(id)); // False means: Filter is returned
												// more than once

			// check parameters
			if (id.equals("Limit")) {
				int val = f.getIntParam("limit", 0);
				assertEquals(50, val);

			} else if (id.equals("MinTimeDiff")) {
				int val = f.getIntParam("timeDiff", 0);
				assertEquals(5, val);
			}
		}

		// If "required" does still contain elements then getWaypointFilters
		// didn't return all filters!
		assertTrue(required.isEmpty());
	}

	@Test
	public void testGetTrainingSetSize() {
		assertEquals(0.75, mConfig.getTrainingSetSize(), 0);
	}

	@Test
	public void testGetCrossValidationFolds() {
		assertEquals(2, mConfig.getCrossValidationFolds());
	}

	@Test
	public void testGetName() {
		assertEquals("Example Config", mConfig.getName());
	}

}
