package de.fub.agg2graph.gpseval.data;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpseval.data.filter.TrackFilter;
import de.fub.agg2graph.gpseval.data.filter.TrackFilterFactory;
import de.fub.agg2graph.gpseval.features.FeatureFactory;

public class DataLoaderTest {
	public DataLoader mLoader;
	
	@Before
	public void setUp() throws Exception {
		mLoader = new DataLoader();
		mLoader.addFeature(FeatureFactory.getFactory().newFeature("MaxSpeed"));
		
		TrackFilter filter = TrackFilterFactory.getFactory().newTrackFilter("LimitPerClass");
		filter.setParam("limit", "1");
		filter.init(); // apply parameters
		
		List<TrackFilter> trackFilters = new ArrayList<>();
		trackFilters.add(filter);
		mLoader.addTrackFilters(trackFilters);
	}

	@Test
	public void testLoadDataFolder() {
		// Check if TrackFilter is applied!
		try {
			List<AggregatedData> data = mLoader.loadDataFolder("walking", "test/gpseval/unit-test/tracks");
			assertEquals(1, data.size()); // if no filter is used, 2 files would have been read
			
		} catch (IOException e) {
			fail("Loading data failed!");
		}
	}

}
