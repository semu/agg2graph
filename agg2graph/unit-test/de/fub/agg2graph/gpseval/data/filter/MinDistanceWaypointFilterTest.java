package de.fub.agg2graph.gpseval.data.filter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpseval.data.Waypoint;

public class MinDistanceWaypointFilterTest {
	MinDistanceWaypointFilter mFilter;

	Waypoint mW1 = new Waypoint(new String[] { "1", "0", "52.455813", "13.296065", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW2 = new Waypoint(new String[] { "1", "0", "52.455724", "13.295933", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW3 = new Waypoint(new String[] { "2", "0", "52.455756", "13.294743", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW4 = new Waypoint(new String[] { "2", "0", "52.455718", "13.294576", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW5 = new Waypoint(new String[] { "3", "0", "52.455928", "13.295092", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW6 = new Waypoint(new String[] { "3", "0", "52.455875", "13.295011", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW7 = new Waypoint(new String[] { "4", "0", "52.45621", "13.295318", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW8 = new Waypoint(new String[] { "4", "0", "52.456186", "13.295267", "0", "350", "0", "0", "2012-11-16T10:55:11.000Z" });

	@Before
	public void setUp() throws Exception {
		mFilter = new MinDistanceWaypointFilter();
		mFilter.setParam("distance", "10");
		mFilter.reset();
	}

	@Test
	public void testFilter() {
		assertTrue(mFilter.filter(mW1)); // is first waypoint
		assertTrue(mFilter.filter(mW2)); // 13 meters
		assertTrue(mFilter.filter(mW3)); // 80 meters
		assertTrue(mFilter.filter(mW4)); // 12 meters
		assertTrue(mFilter.filter(mW5)); // 42 meters
		assertFalse(mFilter.filter(mW6)); // 8 meters
		assertTrue(mFilter.filter(mW7)); // 42 meters
		assertFalse(mFilter.filter(mW8)); // 4 meters
	}

	@Test
	public void testReset() {
		assertTrue(mFilter.filter(mW1)); // is first waypoint
		assertTrue(mFilter.filter(mW2)); // 13 meters
		assertTrue(mFilter.filter(mW3)); // 80 meters
		assertTrue(mFilter.filter(mW4)); // 12 meters
		assertTrue(mFilter.filter(mW5)); // 42 meters
		assertFalse(mFilter.filter(mW6)); // 8 meters
		assertTrue(mFilter.filter(mW7)); // 42 meters
		assertFalse(mFilter.filter(mW8)); // 4 meters
		
		mFilter.reset();

		assertTrue(mFilter.filter(mW1)); // is first waypoint
		assertTrue(mFilter.filter(mW2)); // 13 meters
		assertTrue(mFilter.filter(mW3)); // 80 meters
		assertTrue(mFilter.filter(mW4)); // 12 meters
		assertTrue(mFilter.filter(mW5)); // 42 meters
		assertFalse(mFilter.filter(mW6)); // 8 meters
		assertTrue(mFilter.filter(mW7)); // 42 meters
		assertFalse(mFilter.filter(mW8)); // 4 meters
	}
}
