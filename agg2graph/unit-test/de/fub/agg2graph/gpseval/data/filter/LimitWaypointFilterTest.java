package de.fub.agg2graph.gpseval.data.filter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LimitWaypointFilterTest {
	LimitWaypointFilter mFilter;

	@Before
	public void setUp() throws Exception {
		mFilter = new LimitWaypointFilter();
		mFilter.setParam("limit", "5");
		mFilter.reset(); // init
	}

	@Test
	public void testFilter() {
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));

		assertFalse(mFilter.filter(null));
	}

	@Test
	public void testReset() {
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertFalse(mFilter.filter(null));
		
		mFilter.reset();

		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertTrue(mFilter.filter(null));
		assertFalse(mFilter.filter(null));
	}

}
