package de.fub.agg2graph.gpseval.data.filter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LimitPerClassTrackFilterTest {
	LimitPerClassTrackFilter mFilter;

	@Before
	public void setUp() throws Exception {
		mFilter = new LimitPerClassTrackFilter();
		mFilter.setParam("limit", "5");
		mFilter.init();
	}

	@Test
	public void testFilter() {
		assertTrue(mFilter.filter(null, "Class1"));
		assertTrue(mFilter.filter(null, "Class1"));
		assertTrue(mFilter.filter(null, "Class1"));
		assertTrue(mFilter.filter(null, "Class1"));
		assertTrue(mFilter.filter(null, "Class1"));
		assertTrue(mFilter.filter(null, "Class2"));
		assertTrue(mFilter.filter(null, "Class2"));
		assertTrue(mFilter.filter(null, "Class2"));
		assertTrue(mFilter.filter(null, "Class2"));
		assertTrue(mFilter.filter(null, "Class2"));

		assertFalse(mFilter.filter(null, "Class1"));
		assertFalse(mFilter.filter(null, "Class2"));
	}

}
