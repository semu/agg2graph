package de.fub.agg2graph.gpseval.data.filter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpseval.data.Waypoint;

public class MinTimeDiffWaypointFilterTest {
	MinTimeDiffWaypointFilter mFilter;

	Waypoint mW1 = new Waypoint(new String[] { "1", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:11.000Z" });
	Waypoint mW2 = new Waypoint(new String[] { "1", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:13.000Z" });
	Waypoint mW3 = new Waypoint(new String[] { "2", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:15.000Z" });
	Waypoint mW4 = new Waypoint(new String[] { "2", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:17.000Z" });
	Waypoint mW5 = new Waypoint(new String[] { "3", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:19.000Z" });
	Waypoint mW6 = new Waypoint(new String[] { "3", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:21.000Z" });
	Waypoint mW7 = new Waypoint(new String[] { "4", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:23.000Z" });
	Waypoint mW8 = new Waypoint(new String[] { "4", "0", "0", "0", "0", "0", "0", "0", "2012-11-16T10:55:25.000Z" });

	@Before
	public void setUp() throws Exception {
		mFilter = new MinTimeDiffWaypointFilter();
		mFilter.setParam("timeDiff", "5");
		mFilter.reset();
	}

	@Test
	public void testFilter() {
		assertTrue(mFilter.filter(mW1)); // is first waypoint
		assertFalse(mFilter.filter(mW2)); 
		assertFalse(mFilter.filter(mW3)); 
		assertTrue(mFilter.filter(mW4)); 
		assertFalse(mFilter.filter(mW5)); 
		assertFalse(mFilter.filter(mW6)); 
		assertTrue(mFilter.filter(mW7)); 
		assertFalse(mFilter.filter(mW8));
	}

	@Test
	public void testReset() {
		assertTrue(mFilter.filter(mW1)); // is first waypoint
		assertFalse(mFilter.filter(mW2)); 
		assertFalse(mFilter.filter(mW3)); 
		assertTrue(mFilter.filter(mW4)); 
		assertFalse(mFilter.filter(mW5)); 
		assertFalse(mFilter.filter(mW6)); 
		assertTrue(mFilter.filter(mW7)); 
		assertFalse(mFilter.filter(mW8));
		
		mFilter.reset();
		
		assertTrue(mFilter.filter(mW1)); // is first waypoint
		assertFalse(mFilter.filter(mW2)); 
		assertFalse(mFilter.filter(mW3)); 
		assertTrue(mFilter.filter(mW4)); 
		assertFalse(mFilter.filter(mW5)); 
		assertFalse(mFilter.filter(mW6)); 
		assertTrue(mFilter.filter(mW7)); 
		assertFalse(mFilter.filter(mW8));
	}
}
