/*
 * Copyright 2013 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpseval.data.processing;

import static org.junit.Assert.assertEquals;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.Before;
import org.junit.Test;
import de.fub.agg2graph.gpseval.data.file.MyTracksCSVFile;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilter;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilterFactory;

public class CSVFileProcessorTest {
	MyTracksCSVFile mFile;
	CSVFileProcessor mProc;
	
	@Before
	public void setUp() throws Exception {
		mFile = new MyTracksCSVFile();
	}

	@Test
	public void testCopy() {
		mFile = new MyTracksCSVFile();
		mFile.setDataFile(Paths.get("test/gpseval/unit-test/tracks/track.csv"));
		WaypointFilter filter = WaypointFilterFactory.getFactory()
				.newWaypointFilter("RemoveHome");
		filter.setParam("homesList", "test/gpseval/unit-test/homes.list");
		filter.reset(); // apply params
		mFile.addWaypointFilter(filter);
		mProc = new CSVFileProcessor(mFile);
		mProc.copyCSV(new File("test/gpseval/unit-test/tracks/copy.csv"));
		assertEquals(3, 3); 
	}
	
	@Test
	public void testCopyDir() {
		CSVFolderProcessor proc = new CSVFolderProcessor("test/gpseval/unit-test/tracks/");
		proc.eraseSecretLocations("test/gpseval/unit-test/copies/", "test/gpseval/unit-test/homes.list");
	}
	
	@Test
	public void testDirGpx() throws DatatypeConfigurationException, JAXBException, IOException {
		CSVFolderProcessor proc = new CSVFolderProcessor("test/gpseval/unit-test/tracks/");
		proc.convertFolderToGpx("test/gpseval/unit-test/gpxtracks/");
	}

}
