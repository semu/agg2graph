package de.fub.agg2graph.gpseval.data.file;

import static org.junit.Assert.*;

import java.nio.file.Paths;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import de.fub.agg2graph.gpseval.data.Waypoint;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilter;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilterFactory;

public class MyTracksCSVFileTest {
	MyTracksCSVFile mFile;

	@Before
	public void setUp() throws Exception {
		mFile = new MyTracksCSVFile();
		mFile.setDataFile(Paths.get("test/gpseval/unit-test/tracks/track.csv"));

		// Add a filter so that mFile.iterator() will return less files than
		// mFile.rawIterator().
		WaypointFilter filter = WaypointFilterFactory.getFactory()
				.newWaypointFilter("Limit");
		filter.setParam("limit", "3");
		filter.reset(); // apply params
		mFile.addWaypointFilter(filter);
	}

	@Test
	public void testRawIterator() {
		// The list of waypoints which should be returned by rawIterator().
		String[][] waypoints = new String[][] {
				new String[] { "1", "1", "52.455813", "13.296065", "90.0",
						"1.0", "35", "1", "2012-11-16T10:54:20.723Z" },
				new String[] { "1", "2", "52.455724", "13.295933", "90.0",
						"2.0", "36", "2", "2012-11-16T10:54:21.723Z" },
				new String[] { "1", "3", "52.455724", "13.295933", "90.0",
						"3.0", "37", "3", "2012-11-16T10:54:22.719Z" },
				new String[] { "1", "4", "52.455812", "13.295971", "88.0",
						"4.0", "38", "4", "2012-11-16T10:54:23.719Z" },
				new String[] { "1", "5", "52.455812", "13.295971", "88.0",
						"5.0", "39", "5", "2012-11-16T10:54:25.721Z" } };

		int i = 0;
		Iterator<Waypoint> iter = mFile.rawIterator();
		while (iter.hasNext()) {
			assertTrue("rawIterator returned too many waypoints.", i < waypoints.length);
			Waypoint wp = iter.next();
			Waypoint wpc = new Waypoint(waypoints[i]);
			assertTrue(wpc.equals(wp));
			i++;
		}
		assertEquals(5, i); // all 5 waypoints were returned
	}

	@Test
	public void testIterator() {
		// A limit-filter is used (limit set to 3, s.a.).
		// The list of waypoints which should be returned by iterator().
		String[][] waypoints = new String[][] {
				new String[] { "1", "1", "52.455813", "13.296065", "90.0",
						"1.0", "35", "1", "2012-11-16T10:54:20.723Z" },
				new String[] { "1", "2", "52.455724", "13.295933", "90.0",
						"2.0", "36", "2", "2012-11-16T10:54:21.723Z" },
				new String[] { "1", "3", "52.455724", "13.295933", "90.0",
						"3.0", "37", "3", "2012-11-16T10:54:22.719Z" } };

		int i = 0;
		for (Waypoint wp : mFile) {
			assertTrue("iterator returned too many waypoints.", i < waypoints.length);
			Waypoint wpc = new Waypoint(waypoints[i]);
			assertTrue(wpc.equals(wp));
			i++;
		}
		assertEquals(3, i); // all 3 waypoints were returned
	}

}
