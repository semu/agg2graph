/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;


/**
 * 
 * @author Sebastian Müller sebastian.mueller@fu-berlin.de
 */
public class TrajectoryEdgeDistanceTest {

	@Before
	public void setUp() {
		ResourceManager.envOK();
	}

	@Test
	public void testInput() throws URISyntaxException, JAXBException, Exception {
		TrajectoryEdgeDistance dist = new AngleHausdorffDistance();
		EdgeDistance ed = new AngleEuclideanDistance();
		System.out.println("AngleHausdorffDistance:");
		evaluateDistance(dist, ed);
		dist = new AngleL1FrechetDistance();
		ed = new AngleEuclideanDistance();
		System.out.println("AngleL1FrechetDistance:");
		evaluateDistance(dist, ed);
		dist = new AngleLInfinityFrechetDistance();
		ed = new AngleEuclideanDistance();
		System.out.println("AngleLInfinityFrechetDistance:");
		evaluateDistance(dist, ed);
	}

	private void evaluateDistance(TrajectoryEdgeDistance dist, EdgeDistance ed)
			throws ParserConfigurationException, SAXException, IOException {
		File f1 = new File("test/input/berlin/1058439-0001.gpx");
		File f3 = new File("test/input/berlin/1206317-0001.gpx");
		File f4 = new File("test/input/berlin/page0000-0004.gpx");
		File f5 = new File("test/input/berlin/page0000-0002.gpx");

		System.out.println(System.currentTimeMillis());

		List<GPSPoint> list1 = GPXReader.getOrderedPoints(f1);
		List<GPSPoint> list3 = GPXReader.getOrderedPoints(f3);
		List<GPSPoint> list4 = GPXReader.getOrderedPoints(f4);
		List<GPSPoint> list5 = GPXReader.getOrderedPoints(f5);

		long timeBefore = System.currentTimeMillis();
		int count1 = 0;
		int count2 = 0;
		int count3 = 0;
		int count4 = 0;
		int count5 = 0;

		boolean br = true;
		Iterator<GPSPoint> it1 = list1.iterator();
		GPSPoint old1 = null;
		List<GPSEdge> oldl1 = new ArrayList<GPSEdge>();
		while (it1.hasNext() && br) {
			GPSPoint p1 = it1.next();
			if (old1!=null)	oldl1.add(new GPSEdge(old1,p1));
				Iterator<GPSPoint> it3 = list3.iterator();
				GPSPoint old3 = null;
				List<GPSEdge> oldl3 = new ArrayList<GPSEdge>();
				while (it3.hasNext() && br) {
					GPSPoint p3 = it3.next();
					if (old3!=null)	oldl3.add(new GPSEdge(old3,p3));
					Iterator<GPSPoint> it4 = list4.iterator();
					GPSPoint old4 = null;
					List<GPSEdge> oldl4 = new ArrayList<GPSEdge>();
					while (it4.hasNext() && br) {
						GPSPoint p4 = it4.next();
						if (old4!=null)	oldl4.add(new GPSEdge(old4,p4));
						Iterator<GPSPoint> it5 = list5.iterator();
						GPSPoint old5 = null;
						List<GPSEdge> oldl5 = new ArrayList<GPSEdge>();
						while (it5.hasNext() && br) {
							GPSPoint p5 = it5.next();
							if (old5!=null)	oldl5.add(new GPSEdge(old5,p5));
							if (oldl1.size() >= 5 && oldl3.size() >= 5 && oldl4.size() >= 5 && oldl5.size() >= 5){
							double d13 = dist.calculate(oldl1, oldl3, ed);
							double d14 = dist.calculate(oldl1, oldl4, ed);
							double d34 = dist.calculate(oldl3, oldl4, ed);
							double d15 = dist.calculate(oldl1, oldl5, ed);
							double d35 = dist.calculate(oldl3, oldl5, ed);
							double d45 = dist.calculate(oldl4, oldl5, ed);
							count5++;
							}
							old5=p5;
							if (oldl5.size()>5){
								oldl5.remove(0);
							}
						}
						long elapsed = System.currentTimeMillis() - timeBefore;
						if (elapsed > 5000){
							System.out.println("elapsed (5000): " + count5);
							br = false;
						}
						count4++;
						old4=p4;
						if (oldl4.size()>5){
							oldl4.remove(0);
						}
					}
					count3++;
					old3=p3;
					if (oldl3.size()>5){
						oldl3.remove(0);
					}
			}
			count1++;
			old1=p1;
			if (oldl1.size()>5){
				oldl1.remove(0);
			}
		}

		long time = System.currentTimeMillis() - timeBefore;

		System.out.println(time);
		System.out.println(count5);
		System.out.println(count4);
		System.out.println(count3);
		System.out.println(count2);
		System.out.println(count1);
	}
}
