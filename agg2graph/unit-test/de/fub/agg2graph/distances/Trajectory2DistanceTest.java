/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.structs.GPSPoint;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * 
 * @author Sebastian Müller sebastian.mueller@fu-berlin.de
 */
public class Trajectory2DistanceTest {

	@Before
	public void setUp() {
		ResourceManager.envOK();
	}

	@Test
	public void testInput() throws URISyntaxException, JAXBException, Exception {
		TrajectoryDistance dist = new HausdorffDistance();
		PointDistance pd = new EuclideanDistance();
		System.out.println("HausdorffDistance:");
		evaluateDistance(dist, pd);

//		dist = new Wouter2FrechetDistance();
//		pd = new EuclideanDistance();
//		System.out.println("Wouter2FrechetDistance:");
//		evaluateDistance(dist, pd);

//		dist = new WouterFrechetDistance();
//		pd = new EuclideanDistance();
//		System.out.println("WouterFrechetDistance:");
//		evaluateDistance(dist, pd);

		//		dist = new JTSFrechetDistance();
//		pd = new EuclideanDistance();
//		System.out.println("FrechetDistance:");
//		evaluateDistance(dist, pd);
	}

	private void evaluateDistance(TrajectoryDistance dist, PointDistance pd)
			throws ParserConfigurationException, SAXException, IOException {
		File f1 = new File("test/input/berlin/1058439-0001.gpx");
		File f2 = new File("test/input/berlin/1199499-0001.gpx");
		File f3 = new File("test/input/berlin/1206317-0001.gpx");
		File f4 = new File("test/input/berlin/page0000-0004.gpx");
		File f5 = new File("test/input/berlin/page0000-0002.gpx");

		System.out.println(System.currentTimeMillis());

		List<GPSPoint> list1 = GPXReader.getOrderedPoints(f1);
		List<GPSPoint> list2 = GPXReader.getOrderedPoints(f2);
		List<GPSPoint> list3 = GPXReader.getOrderedPoints(f3);
		List<GPSPoint> list4 = GPXReader.getOrderedPoints(f4);
		List<GPSPoint> list5 = GPXReader.getOrderedPoints(f5);

		long timeBefore = System.currentTimeMillis();
		int count1 = 0;
		int count2 = 0;
		int count3 = 0;
		int count4 = 0;
		int count5 = 0;

		
		boolean br = true;
		Iterator<GPSPoint> it1 = list1.iterator();
		List<GPSPoint> old1 = new ArrayList<GPSPoint>();
		while (it1.hasNext() && br) {
			GPSPoint p1 = it1.next();
			old1.add(p1);
			Iterator<GPSPoint> it2 = list2.iterator();
			List<GPSPoint> old2 = new ArrayList<GPSPoint>();
			while (it2.hasNext() && br) {
				GPSPoint p2 = it2.next();
				old2.add(p2);
				Iterator<GPSPoint> it3 = list3.iterator();
				List<GPSPoint> old3 = new ArrayList<GPSPoint>();
				while (it3.hasNext() && br) {
					GPSPoint p3 = it3.next();
					old3.add(p3);
					Iterator<GPSPoint> it4 = list4.iterator();
					List<GPSPoint> old4 = new ArrayList<GPSPoint>();
					while (it4.hasNext() && br) {
						GPSPoint p4 = it4.next();
						old4.add(p4);
						Iterator<GPSPoint> it5 = list5.iterator();
						List<GPSPoint> old5 = new ArrayList<GPSPoint>();
						while (it5.hasNext() && br) {
							GPSPoint p5 = it5.next();
							old5.add(p5);
							if (old1.size() >= 1 && old2.size() >= 1 && old3.size() >= 1 && old4.size() >= 1 && old5.size() >= 1){
							double d12 = dist.calculate(old1, old2, pd);
							double d23 = dist.calculate(old2, old3, pd);
							double d13 = dist.calculate(old1, old3, pd);
							double d14 = dist.calculate(old1, old4, pd);
							double d24 = dist.calculate(old2, old4, pd);
							double d34 = dist.calculate(old3, old4, pd);
							double d15 = dist.calculate(old1, old5, pd);
							double d25 = dist.calculate(old2, old5, pd);
							double d35 = dist.calculate(old3, old5, pd);
							double d45 = dist.calculate(old4, old5, pd);
							count5++;
							}
						}
						long elapsed = System.currentTimeMillis() - timeBefore;
						if (elapsed > 5000){
							System.out.println("elapsed (5000): " + count5);
							br = false;
						}
						count4++;
					}
					count3++;
				}
				count2++;
			}
			count1++;
		}

		long time = System.currentTimeMillis() - timeBefore;

		System.out.println(time);
		System.out.println(count5);
		System.out.println(count4);
		System.out.println(count3);
		System.out.println(count2);
		System.out.println(count1);
	}
}
