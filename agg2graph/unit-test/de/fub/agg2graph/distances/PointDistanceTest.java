/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.structs.GPSPoint;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;


/**
 * 
 * @author Sebastian Müller sebastian.mueller@fu-berlin.de
 */
public class PointDistanceTest {

	@Before
	public void setUp() {
		ResourceManager.envOK();
	}

	@Test
	public void testInput() throws URISyntaxException, JAXBException, Exception {
		PointDistance dist = new EuclideanDistance();
		System.out.println("EuclideanDistance:");
		evaluateDistance(dist);

		dist = new ManhattanDistance();
		System.out.println("ManhattanDistance:");
		evaluateDistance(dist);

//		dist = new AndroidGeodesicDistance();
//		System.out.println("AndroidGeodesicDistance:");
//		evaluateDistance(dist);

//		dist = new FastVincentyDistance();
//		System.out.println("FastVincentyDistance:");
//		evaluateDistance(dist);

		dist = new SimpleDistance();
		System.out.println("SimpleDistance:");
		evaluateDistance(dist);

		dist = new HaversineDistance();
		System.out.println("HaversineDistance:");
		evaluateDistance(dist);
	}

	private void evaluateDistance(PointDistance dist)
			throws ParserConfigurationException, SAXException, IOException {
		File f1 = new File("test/input/berlin/1058439-0001.gpx");
		File f2 = new File("test/input/berlin/1199499-0001.gpx");
		File f3 = new File("test/input/berlin/1206317-0001.gpx");
		File f4 = new File("test/input/berlin/page0000-0004.gpx");
		File f5 = new File("test/input/berlin/page0000-0002.gpx");

		System.out.println(System.currentTimeMillis());

		List<GPSPoint> list1 = GPXReader.getOrderedPoints(f1);
		List<GPSPoint> list2 = GPXReader.getOrderedPoints(f2);
		List<GPSPoint> list3 = GPXReader.getOrderedPoints(f3);
		List<GPSPoint> list4 = GPXReader.getOrderedPoints(f4);
		List<GPSPoint> list5 = GPXReader.getOrderedPoints(f5);

		long timeBefore = System.currentTimeMillis();
		int count1 = 0;
		int count2 = 0;
		int count3 = 0;
		int count4 = 0;
		int count5 = 0;

		
		Iterator<GPSPoint> it1 = list1.iterator();
		while (it1.hasNext()) {
			GPSPoint p1 = it1.next();
			Iterator<GPSPoint> it2 = list2.iterator();
			while (it2.hasNext()) {
				GPSPoint p2 = it2.next();
				Iterator<GPSPoint> it3 = list3.iterator();
				while (it3.hasNext()) {
					GPSPoint p3 = it3.next();
					Iterator<GPSPoint> it4 = list4.iterator();
					while (it4.hasNext()) {
						GPSPoint p4 = it4.next();
						Iterator<GPSPoint> it5 = list5.iterator();
						while (it5.hasNext()) {
							GPSPoint p5 = it5.next();
							double d12 = dist.calculate(p1, p2);
							double d23 = dist.calculate(p2, p3);
							double d13 = dist.calculate(p1, p3);
							double d14 = dist.calculate(p1, p4);
							double d24 = dist.calculate(p1, p4);
							double d34 = dist.calculate(p1, p4);
							double d15 = dist.calculate(p1, p5);
							double d25 = dist.calculate(p2, p5);
							double d35 = dist.calculate(p3, p5);
							double d45 = dist.calculate(p4, p5);
							count5++;
						}
						count4++;
					}
					count3++;
				}
				count2++;
			}
			count1++;
		}

		long time = System.currentTimeMillis() - timeBefore;

		System.out.println(time);
		System.out.println(count5);
		System.out.println(count4);
		System.out.println(count3);
		System.out.println(count2);
		System.out.println(count1);
	}
}
