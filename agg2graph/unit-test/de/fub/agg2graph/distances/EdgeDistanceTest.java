/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.structs.GPSPoint;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * 
 * @author Sebastian Müller sebastian.mueller@fu-berlin.de
 */
public class EdgeDistanceTest {

	@Before
	public void setUp() {
		ResourceManager.envOK();
	}

	@Test
	public void testInput() throws URISyntaxException, JAXBException, Exception {
		EdgeDistance dist = new PerpEdgeDistance();
		System.out.println("PerpEdgeDistance:");
		evaluateDistance(dist);
		dist = new AngleEuclideanDistance();
		System.out.println("AngleEuclideanDistance:");
		evaluateDistance(dist);
		dist = new AnglePerpDistance();
		System.out.println("AnglePerpDistance:");
		evaluateDistance(dist);
		dist = new PerpShiftDistance();
		System.out.println("PerpShiftDistance:");
		evaluateDistance(dist);

	}

	private void evaluateDistance(EdgeDistance dist)
			throws ParserConfigurationException, SAXException, IOException {
		File f1 = new File("test/input/berlin/1058439-0001.gpx");
		File f2 = new File("test/input/berlin/1199499-0001.gpx");
		File f3 = new File("test/input/berlin/1206317-0001.gpx");
		File f4 = new File("test/input/berlin/page0000-0004.gpx");
		File f5 = new File("test/input/berlin/page0000-0002.gpx");

		System.out.println(System.currentTimeMillis());

		List<GPSPoint> list1 = GPXReader.getOrderedPoints(f1);
		List<GPSPoint> list2 = GPXReader.getOrderedPoints(f2);
		List<GPSPoint> list3 = GPXReader.getOrderedPoints(f3);
		List<GPSPoint> list4 = GPXReader.getOrderedPoints(f4);
		List<GPSPoint> list5 = GPXReader.getOrderedPoints(f5);

		long timeBefore = System.currentTimeMillis();
		int count1 = 0;
		int count2 = 0;
		int count3 = 0;
		int count4 = 0;
		int count5 = 0;

		
		Iterator<GPSPoint> it1 = list1.iterator();
		GPSPoint old1 = null;
		while (it1.hasNext()) {
			GPSPoint p1 = it1.next();
			Iterator<GPSPoint> it2 = list2.iterator();
			GPSPoint old2 = null;
			while (it2.hasNext()) {
				GPSPoint p2 = it2.next();
				Iterator<GPSPoint> it3 = list3.iterator();
				GPSPoint old3 = null;
				while (it3.hasNext()) {
					GPSPoint p3 = it3.next();
					Iterator<GPSPoint> it4 = list4.iterator();
					GPSPoint old4 = null;
					while (it4.hasNext()) {
						GPSPoint p4 = it4.next();
						Iterator<GPSPoint> it5 = list5.iterator();
						GPSPoint old5 = null;
						while (it5.hasNext()) {
							GPSPoint p5 = it5.next();
							if (old1 != null && old2 != null && old3 != null && old4 != null && old5 != null){
							double d12 = dist.calculate(old1, p1, old2, p2);
							double d23 = dist.calculate(old2, p2, old3, p3);
							double d13 = dist.calculate(old1, p1, old3, p3);
							double d14 = dist.calculate(old1, p1, old4, p4);
							double d24 = dist.calculate(old2, p2, old4, p4);
							double d34 = dist.calculate(old3, p3, old4, p4);
							double d15 = dist.calculate(old1, p1, old5, p5);
							double d25 = dist.calculate(old2, p2, old5, p5);
							double d35 = dist.calculate(old3, p3, old5, p5);
							double d45 = dist.calculate(old4, p4, old5, p5);
							count5++;
							}
							old5 = p5;
						}
						count4++;
						old4 = p4;
					}
					count3++;
					old3 = p3;
				}
				count2++;
				old2 = p2;
			}
			count1++;
			old1 = p1;
		}

		long time = System.currentTimeMillis() - timeBefore;

		System.out.println(time);
		System.out.println(count5);
		System.out.println(count4);
		System.out.println(count3);
		System.out.println(count2);
		System.out.println(count1);
	}
}
