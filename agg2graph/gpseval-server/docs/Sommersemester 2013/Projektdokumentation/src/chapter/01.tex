\section{Allgemeiner Überblick}
Dieses Dokument stellt die Projektdokumentation für das Projekt \emph{Dynamische Erkennung des Mobilitätsmodus} des Kurses Projektseminar \glqq Datenverwaltung\grqq~dar. Das Dokument wird neben der Problemstellung und dem Projektziel einen Überblick über die Umsetzung der Projektziele geben. Dazu gehören Erläuterungen der Probleme und theoretischen Grundlagen, sowie die Lösungen zu den erläuterten Problemen. Außerdem werden die verwendeten Technologien und Tools besprochen.

Im Folgenden wird genauer auf das Projekt an sich Bezug genommen, indem die eigentliche Problemstellung und das Projektziel erläutert werden. Aufgrund der Verwendung des \emph{agg2graph}-Projekts\footnote{\url{https://github.com/sebastian-fu/agg2graph}} als Grundlage für die Mobilitätserkennung sind Vorgaben entstanden, die erwähnt werden. Außerdem werden die Annahmen und die verwendeten Basistechnologien berücksichtigt und die Abnahmekriterien beschrieben.  Anschließend werden die Grundlagen für die Umsetzung des Projekts erklärt. Dazu gehört die Beschreibung der Testumgebung, auf die das die dynamische Mobilitätserkennung aufbaut.

\subsection{Problemstellung}
Auf Basis eines bereits existierenden Projekts zur Erkennung des Mobilitätsmodus aus vorliegenden GPS Spuren, namens \emph{GPSEval} \cite{ManuelKotlarski2013}, soll eine dynamische Erkennung realisiert werden. Das bedeutet, dass mit Hilfe von Echtzeit-GPS-Daten entschieden werden soll, welches Verkehrsmittel momentan benutzt wird. Der Mobilitätsmodus gibt Auskunft über die Art der Bewegung einer Person. Dafür wird eine GPS Spur erzeugt und ausgewertet. Anhand von Geschwindigkeitsmustern kann so z.B. erkannt werden, ob man gerade im Bus sitzt oder mit dem Fahrrad unterwegs ist.

\subsection{Projektziel}
Ziel des Projekts ist die Entwicklung einer Android-Applikation, welche den Bewegungsverlauf mitschneidet und direkt auswertet. Die Auswertung wird dem Benutzer angezeigt. Die verwendeten Regeln sollten vorher auf Basis eines Lernalgorithmus trainiert werden. Das heißt, es müssen Features (siehe Abschnitt \ref{sec:grundlagen}) aus den GPS Spuren extrahiert werden, z.B. die Durchschnittsgeschwindigkeit. Welche Klassifikatoren und Features wie verwendet werden können, wurde bereits durch das vorherige Projekt ermittelt und in \cite{ManuelKotlarski2013}, \cite{Kotlarski2013} und in \cite{Simons2013} evaluiert. Im Rahmen dieses Projekts werden die Features Durchschnittsgeschwindigkeit, Maximalgeschwindigkeit und durchschnittlicher Abstand zu einer Bushaltestelle in den Entscheidungsbaum integriert (Beispiel, siehe Abbildung \ref{fig:dtree}).

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/dtree.pdf}
\caption{Der Entscheidungsbaum wird von Weka erzeugt. Die Datenmenge ist ein wichtiges Kriterium, um einen geeigneten Baum zu erhalten.}
\label{fig:dtree}
\end{figure}

\subsection{Vorgaben}
Das vorangehende Projekt GPSEval beinhaltet bereits die Integration der GPS-Spuren in die Weka Data-Mining Softwarelösung, allerdings soll dies nun in Echtzeit geschehen. Zum einen muss also ein Android-Prototyp entwickelt werden, der GPS-Koordinaten aufnimmt. Der Entscheidungsbaum, den Weka durch den Lernvorgang mit GPS-Daten produziert, wird in die Androidanwendung importiert und für Moblitätsentscheidungen herangezogen. Außerdem ist eine Serveranwendung geplant, die von den Mobilklienten aufgenommene GPS Spuren empfängt und in einer Datenbank archiviert, um in Zukunft auch aus diesen Daten lernen zu können.

\subsection{Annahmen und verwendete Basistechnologien}
Es wurde angenommen, dass es nicht Teil unserer Aufgabe ist, GPS-Dateien aufzunehmen. Diese lagen im agg2graph-Projekt bereits vor, die von früheren Projekten zur Verfügung gestellt wurden und teilweise von Quellen wie \emph{MyTracks}\footnote{\url{http://www.google.com/mobile/mytracks/}} stammen. Für die Androidanwendung benutzen wir das Android-Framework und programmieren somit in Java. Für die Qualitätskontrolle werden einzelne Module und Klassen mit Hilfe von JUnit-Tests getestet. Außerdem benutzen wir nach Aufgabenstellung die Weka-Software bzw. das GPSEval-Projekt, die die Weka-Software bereits integriert und aufruft und ebenfalls in ein Java-Projekt eingebunden und genutzt werden kann. 
Da der Server unabhängig vom agg2graph-Projekt entwickelt wird und nur dazu dient, mit der App zu kommunizieren, ist somit keine Server-Technologie genauer spezifiziert. Von daher haben wir uns entschieden, für den Server eine Grails-Anwendung auf Basis von Java zu verwenden. Grails bietet dynamische Methoden, die es erlauben, unabhängig von einer spezifischen Datenbank eine Datenbankanwendung zu schreiben, da die nötigen Adapter inkludiert und genutzt werden. Wir haben uns vor allem deshalb für Grails und nicht für eine andere Server-Technologie entschieden, da wir bereits sowohl privat als auch beruflich viele Erfahrungen mit Grails sammeln konnten und somit die Einarbeitungszeit in eine Server-Technologie sparen konnten. Die Server-Anwendung nutzt als Datenbank eine h2-Datenbank, da diese von Grails standardmäßig eingestellt ist und effizient unter Grails läuft. Die Datenbank ist jedoch aufgrund der für Grails-Anwendungen vorgegebenen Struktur sehr leicht mit einer anderen Datenbank auszutauschen. In der Kurzdokumentation kann dies nachgelesen werden.

\subsection{Abnahme}
Die Anwendung befindet sich im Git-Repository\footnote{Repository unter \url{https://github.com/sebastian-fu/agg2graph}}. Die Abnahme erfolgt ebenfalls über dieses Repository. Dort befindet sich der Sourcecode, eine .apk-Datei (Android-App), eine .war-Datei (Serveranwendung), die Projektdokumentation und die Kurzdokumentation. Der Sourcecode wird mit Javadoc dokumentiert sein. Damit getestet werden kann, wie die Android-App mit dem Server kommuniziert und neue Entscheidungsbäume erhält, muss die .war auf einem Server deployed werden. Allerdings wird die App so bereitgestellt, dass sie offline auf einem lokal vorhandenen Entscheidungsbaum arbeiten kann, sodass die dynamische Mobilitätserkennung auch dann funktioniert, wenn das Mobilgerät keinen Internetempfang hat. Für die Wiederverwendbarkeit des Projekts wird zu dieser Projektdokumentation auch eine Kurzdokumentation bereitgestellt, die die wesentlichen Schritte zur Installation und Einrichtung des Projekts und der Software ausführlich beschreibt.

\subsection{Ablauf der dynamischen Transportmoduserkennung}
Die Android-Applikation, namens \emph{GPSEvaluator} (siehe Abbildung \ref{fig:gpsevaluator}), läuft auf einem mobilen Endgerät und misst im Hintergrund die GPS-Tracks des Nutzers. Mit Hilfe eines lokal vorhandenen Entscheidungsbaumes wird ermittelt, in welchem Transportmodus sich das Gerät befindet (a). Sobald der Modus erkannt wurde, wird dieser Modus dem Nutzer über eine Push-Benachrichtigung mitgeteilt (b). Der Nutzer kann mithilfe eines Feedback-Buttons zustimmen oder aus einer Liste an vorhandenen Modi den korrekten bestimmen (c). Diese Entscheidungen werden bei bestehender Internetverbindung an einen Server gesendet, auf dem ein globaler Entscheidungsbaum vorliegt und anhand des Nutzerfeedbacks weiter lernt. Der aktualisierte Entscheidungsbaum wird dann an das Gerät gesendet.

\begin{figure}[ht]
        \centering
        \begin{subfigure}[b]{0.3\textwidth}
                \centering
                \includegraphics[width=\textwidth]{images/bild1}
                \caption{}
                \label{fig:bild1}
        \end{subfigure}%
        ~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
          %(or a blank line to force the subfigure onto a new line)
        \begin{subfigure}[b]{0.3\textwidth}
                \centering
                \includegraphics[width=\textwidth]{images/bild2}
                \caption{}                
                \label{fig:bild2}
        \end{subfigure}
        ~ %add desired spacing between images, e. g. ~, \quad, \qquad etc.
          %(or a blank line to force the subfigure onto a new line)
        \begin{subfigure}[b]{0.3\textwidth}
                \centering
                \includegraphics[width=\textwidth]{images/bild3}
                \caption{}
                \label{fig:bild3}
        \end{subfigure}
        \caption{Der Benutzer hat selbst die Entscheidung, den GPS-Track zu speichern (a). Der Mobilitätsmodus wird dem Benutzer auch per Push-Benachrichtigung mitgeteilt (b). Diesen kann der Benutzer korrigieren (c).}\label{fig:gpsevaluator}
\end{figure}

\subsection{Grundlagen}
\label{sec:grundlagen}
Um den Transportmodus zu erkennen, sind in erster Linie GPS-Tracks nötig, die mithilfe der mobilen Applikation aufgezeichnet werden. Anhand dieser GPS-Tracks und einer Auswahl von Features soll es dann möglich sein, einen Entscheidungsbaum zu erzeugen, der auf den mobilen Endgeräten vorhanden ist und zur Erkennung des Transportmodus herangezogen werden soll. Mithilfe von Data-Mining (Klassifikation) ist es möglich, anhand von GPS-Tracks für verschiedene Transportmodi einen Entscheidungsbaum für eine Feature-Menge zu erzeugen. Ein Feature ist eine Information, die sich aus den GPS-Daten eines Tracks ableiten lässt (z.B. Durchschnittssgeschwindigkeit). Die theoretischen Grundlagen für das Verständnis der Umsetzung des Projekts sind bereits in \cite{ManuelKotlarski2013} vermittelt. Dort wird erklärt, was ein Feature ist, was ein Klassifikator ist und welche es vor allem gibt. 

In diesem Abschnitt sollen vor allem die Grundlagen vermittelt werden, die für Umsetzung dieses Projekts relevant sind. Diese Grundlagen beinhalten neben theoretischen Grundlagen auch die Erwähnung von Software, die als Grundlage für die Android-Anwendung genutzt werden.

\subsubsection{GPSEval}
Es existiert bereits eine Testumgebung unter \texttt{de.fub.agg2graph.gpseval}, mit der zu gegebenen GPS-Tracks und einem Feature-Set ein Entscheidungsbaum erstellt werden kann. Für die Erstellung eines Entscheidungsbaumes ist dabei eine XML-Konfigurationsdatei nötig, die für die Erstellung des Baumes notwendige Informationen erhält. Dazu gehören die Klassen, die für jede Klasse vorhandenen GPS-Tracks, die zu beachtenden Features und einige weitere Parameter. Die Testumgebung ist modular aufgebaut, sodass mehrere Konfigurationsdateien übergeben werden können und zu jeder Konfiguration ein Baum erzeugt werden kann. Die Testumgebung bietet neben dem J48-Algorithmus zur Erstellung eines Entscheidungsbaumes auch weitere Klassifikatoren an (z.B. RandomForest), aber für unsere Anforderungen genügt der J48-Klassifikator.

Zu Beginn wird die Konfigurationsdatei der Testumgebung übergeben. Diese beschreibt den Ort, an dem die GPS-Spuren abgelegt sind, damit diese in die Testumgebung eingelesen werden können. Anschließend werden mithilfe der in der Konfiguration genannten Features auf Basis der einzelnen Wegpunkte in den Tracks Feature-Werte berechnet. Die GPS-Tracks liegen im CSV-Format vor. Die Testumgebung kümmert sich bereist mithilfe der OpenCSV-Bibliothek um das Einlesen dieser. Anschließend werden die eingelesenen Daten an Weka übergeben.

\subsubsection{Evaluation mittels Weka}
Weka ist eine Bibliothek, die für maschinelles Lernen und Data-Mining eine Menge an Tools zur Verfügung stellt. Es ist eine in Java geschriebene Bibliothek und wurde an der Universität von Waikato entwickelt. Neben vielen anderen Tools bietet Weka verschiedene Klassifikatoren an, wobei für uns der J48-Algorithmus zur Erstellung des Entscheidungsbaumes wichtig ist.
\footnote{Weka 3: Data Mining Software in Java (\url{http://www.cs.waikato.ac.nz/ml/weka/})}

Die Testumgebung hat bereits die Übergabe der eingelesenen Daten an Weka und das Auslesen der Ergebnisse bereits implementiert. Sie bietet dabei zwei verschiedene Methoden zur Klassifikation: Cross-Validation und Training/Test. Bei Training/Test werden die GPS-Daten in zwei disjunkte Mengen aufgeteilt. Die Trainings-Menge wird dabei verwendet, um dem Lernalgorithmus Lerndaten zur Verfügung zu stellen. Der Algorithmus trainiert also anhand dieser Daten. Mit der Test-Menge wird anschließend die Zuordnungsgenauigkeit des gelernten Algorithmus getestet. Bei Cross-Validation werden verschiedene Training/Test-Kombinationen getestet. 

Das Arbeiten und Testen mit Cross-Validation war zu Beginn des Projekts und innerhalb des Projektplans nicht gefordert, sodass wir nur die Training/Test-Methoden der Testumgebung genutzt haben. Wie wir genau die Testumgebung auf dem Server nutzen, um einen Entscheidungsbaum zu erzeugen, wird in Kapitel ~\ref{sec:Weka_einbindung} detaillierter beschrieben. In der Konfigurationsdatei wird über einen Parameter festgelegt, wie viel Prozent der GPS-Tracks als Trainingsmenge verwendet werden. Im GPSEval-Projekt liegt dieser bei $75\%$. In unserem Fall haben wir aufgrund der am Anfang geringen Datenmenge auf $65\%$ verringert, um einen Entscheidungsbaum mit allen drei Features zu erhalten. Dieser Wert kann aber einfach bei einer größeren Datenmenge erhöht werden.