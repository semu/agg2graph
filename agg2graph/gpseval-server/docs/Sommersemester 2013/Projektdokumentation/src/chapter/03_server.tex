\subsection{Server}
\label{chapter03-server}
Die Berechnung des Entscheidungsbaums dauert länger, je mehr GPS-Daten aufgenommen werden. Ein Mobiltelefon verfügt dagegen nur über stark begrenzte Ressourcen (geringe Rechenleistung, geringer Speicherplatz, knapper Akku, etc.), sodass die Berechnung eines Entscheidungsbaums nur unter erheblichen Zeitverzug, einer hohen Auslastung der CPU und damit geringer Akkudauer geschieht.

Außerdem werden nur die Daten des eigenen Mobiltelefons mit in die Berechnung einbezogen. Daten anderer Mobilgeräte bleiben diesen vorbehalten, sodass kein Klient von den Daten anderer Klienten profitieren kann. Entsprechend dauert es für jedes Gerät länger, einen verbesserten Entscheidungsbaum zu erzeugen. Schließlich muss erst einmal eine hinreichend große Menge an GPS-Koordinaten an einem Gerät gesammelt werden.

Die Lösung hierfür ist es, die Berechnungen auf einen Server zu verlagern, der mit den Thin-Clients über definierte Schnittstellen die Rechenergebnisse sowie Rechenparameter austauschen kann. Der Server ist dadurch die zentrale Komponente beim Erstellen des Entscheidungsbaums. Er verwaltet die GPS-Daten aller Mobilgeräte an einer zentralen Stelle und führt alle notwendigen Schritte zum Erstellen des Entscheidungsbaums aus. Durch diese Trennung fällt der große Teil der Programmlogik auf den Server, die Mobilklienten hingegen führen keine rechenintensiven Aufgaben durch.

\begin{figure}[t]
\centering
\includegraphics[width=1\linewidth]{images/ablauf} 
\label{img_app_uml}
\caption{Ablauf von dem Versenden von GPS Aufnahmen bis zum Empfangen des Entscheidungsbaums}%
\end{figure}

Folgende Funktionen werden vom Server abgedeckt:
\begin{enumerate}
\item Empfangen der GPS Aufnahmen
\item Speichern der GPS Aufnahmen in einer Datenbank
\item Auslesen der GPS Aufnahmen aus der Datenbank
\item Aufteilen der GPS Aufnahmen in Teilaufnahmen
\item Importieren der GPS Teilaufnahmen in Weka
\item Entscheidungsbaum mit Weka berechnen
\item Entscheidungsbaum in der Datenbank speichern
\item Versenden des letzten berechneten Entscheidungsbaums
\end{enumerate}

Der Ablauf sieht dabei folgendermaßen aus: Die mobilen Klienten verschicken ihre CSV Aufnahmen an den Server (1). Dieser speichert die gesammelten CSV Tracks in der Datenbank (2). Dies geschieht praktisch die gesamte Zeit im Hintergrund. Da eine GPS Aufnahme mehr als nur einen Mobiltitätsmodus enthalten kann (schließlich kann man auf einer Tour mal laufen, mal Fahrrad fahren und auch andere Mobiltitätsmodi nutzen), müssen die empfangen CSV Aufnahmen noch vorverarbeitet werden. Dazu läuft ein Cronjob in einer konfigurierbaren Zeitspanne über die gesammelten Daten und teilt diese in Teilaufnahmen auf (3, 4). Jeder Mobilitätsmodus innerhalb einer empfangen Datei erhält so seine eigene Datei. Wenn ein Klient also in einer CSV Aufnahme vier verschiedene Mobiltitätsmodi aufgenommen hat, so entstehen nach diesem Schritt auch vier verschiedene CSV Teilaufnahmen. Dieser Schritt ist notwendig, weil Weka nur bereits in dieser Form aufgeteilte Aufnahmen als Eingabe erhalten kann. Sobald die Aufnahmen aufgeteilt vorliegen, können sie von Weka benutzt werden, um den Entscheidungsbaum zu erstellen (5, 6). Nachdem Weka den Entscheidungsbaum erstellt hat, wird dieser in der Datenbank gespeichert (7). Der Entscheidungsbaum kann nun an die Klienten versendet werden (8).

Da die Erstellung des Entscheidungsbaums unter Umständen einer längeren Zeitspanne bedarf, werden alle Schritte von 4 bis 7 im Hintergrund von einem Cronjob erledigt. Dies ist erforderlich, damit währenddessen weitere GPS-Aufnahmen erhalten werden können und der Server weiterhin für Anfragen erreichbar ist.

\subsubsection{Architektur}
Auf der Serverseite wird das Webframework Grails eingesetzt. Grails ist ein durch Ruby on Rails beeinflusstes MVC-Webframework \cite{GroveOzkan2011}. Durch den Einsatz der populären dynamisch typisierten Programmier- und Skriptsprache Groovy, weiteren Konzepten wie Scaffolding, automatischen Validatoren, Internationalisierung sowie einem umfangreichen Pluginverzeichnis wird Grails eingesetzt um effektiv Webprojekte auf Basis der JVM umzusetzen. Grails läuft komplett auf einer installierten JVM. Andere Java-Bibliotheken wie beispielsweise das \emph{agg2graph} Projekt lassen sich so in Form einer Jar integrieren.

\subsubsection*{Ordnerstruktur}
Die Ordnerstruktur des Servers orientiert sich am MVC-Prinzip von Grails und arbeitet mit Convention over Configuration Techniken. Zu solchen Konventionen gehört die MVC-Ordnerstruktur, siehe Abbildung \ref{grailsdirs}. Dieser Ausschnitt der Ordnerstruktur des GPSEval Servers zeigt die Trennung zwischen Domain Models und der Controller, die aber durch richtige Benennung bereits konfiguriert und integriert werden. So werden die Controller, in denen die Business Logik implementiert ist, ähnlich wie die Domain Models benannt, nur mit dem Zusatz "Controller". Indem also die Grails Konventionen eingehalten werden, verzichtet der Server auf umständliche XML-Konfiguration wie das in älteren Java Webframeworks der Fall ist. Die Domain Models werden von GORM (Grails ORM) in Datenbanktabellen übersetzt, aber durch die Persistenzschicht (Hibernate) wird nicht mit Tabellen, sondern mit Objekten gearbeitet.

\begin{figure}[htbp]
\dirtree{%
.1 gpseval-server/.
 .2 conf.
 .3 BuildConfig.groovy.
 .3 Config.groovy.
 .3 DataSource.groovy.
 .3 UrlMappings.groovy.
 .2 domain.
 .3 DecisionTree.groovy.
 .3 GPSCapture.groovy.
 .2 controllers.
 .3 DecisionTreeController.groovy.
 .3 GPSCaptureController.groovy.
 .2 services.
 .3 DecisionTreeService.groovy.
 .3 CsvService.groovy.
 .2 lib.
 .2 web-app.
}
\caption{Ordnerstruktur des GPSEval Servers}
\label{grailsdirs}
\end{figure}

\subsubsection{Server Konfiguration}
Die Konfiguration des Grails-Frameworks befindet sich im Ordner \texttt{conf}. In der \texttt{BuildConfig.groovy} werden Einstellungen gespeichert, die benutzt werden für Grails Kommandos, wie z. B. \texttt{grails~compile}, \texttt{grails~doc}, etc. Dagegen wird die \\\texttt{Config.groovy} für Einstellungen benutzt, die notwendig sind, wenn der Server \textit{bereits läuft}. Das bedeutet, dass die \texttt{Config.groovy} ein Teil der später deployten Anwendung ist, die \texttt{BuildConfig.groovy} aber nicht. Der GPSEval Server benutzt weitgehend die Standardeinstellungen des Frameworks. Lediglich die Konfiguration des Cronjobs ist in der \texttt{Config.groovy} abgelegt. Der Cronjob hat als Konfigurationsparameter das Interval, in dem der Cronjob einen neuen Baum berechnet.  Die Einheit ist hierbei Millisekunden (ms), sodass wir standardmäßig $600000~ms$, also $10~min$, für das Interval eingestellt haben.

\subsubsection*{Datenbankkonfiguration}
Die Datei \texttt{DataSource.groovy} beinhaltet die Datenbankkonfiguration. Dort lässt sich ebenfalls einstellen, in welchen Umgebungen, welche Datenbank benutzt werden soll. So sollen in den Dev- und Testumgebungen H2-Datenbanken verwendet werden, wobei in der Testumgebung eine Inmemory-Datenbank eingesetzt werden soll. Gerade beim Debuggen ist es nützlich in der Entwicklung die Datenbank nicht im Arbeitsspeicher zu haben, sondern als Datei auf der Festplatte. In der Produktionsumgebung sollte dann eine bewährte Datenbank wie Postgres zum Einsatz kommen. Der Parameter \texttt{dbCreate} steht dafür, was mit der Datenbank passieren soll, wenn die Anwendung gestartet bzw. deployed wird. \texttt{create-drop} steht dabei dafür, dass die Datenbank erstellt werden soll beim Start und gelöscht werden soll beim Herunterfahren. \texttt{update} behält die bestehende Datenbank mit allen Einträgen und befüllt die Datenbank weiter.

Der Server ist nicht in der Produktionsumgebung installiert, sondern in der Dev-Umgebung, da in der WAR-Datei eine H2-Datenbank inkludiert werden kann und man mit der WAR nicht angewiesen ist auf installierte Datenbanken. Um in die Prod-Umgebung zu wechseln und beispielsweise Postgres zu nutzen, muss die WAR-Datei neu gebaut werden. Dazu müssen die Datenbankeinstellungen eingetragen werden (Datenbankname, Benutzername, Passwort). Danach gibt man den Befehl \texttt{grails~prod~war} in der Grails Konsole ein und erhält die zu deployende WAR-Datei.

\subsubsection*{REST-Schnittstelle}
\label{chapter03-rest}
Die \texttt{UrlMappings.groovy} beinhaltet die URL Mapping Infrastruktur, also die Konventionen, die die REST-Schnittstelle definieren \cite{RichardsonRubyBook}. Diese Einstellungen sind der Standard in Grails 2.1.1. Mit Grails 2.3 werden mehr Möglichkeiten in die URL-Mappings eingeführt, sodass auch höhere Stufen im REST-Reifegradmodell erreicht werden können. Zur Zeit befindet sich die Version 2.3 noch in der Entwicklung, sodass der Server mit Grails 2.1.1 implementiert ist und daher mit obiger Schnittstellenbeschreibung auskommt. In \label{chapter03-controller} ist beschrieben, wie diese Schnittstelle zum Einsatz kommt.

\subsubsection{Domain Models}
\label{chapter03-domain}
Der GPSEval-Server beinhaltet die Domain Models \texttt{DecisionTree.groovy} und \\ \texttt{GPSCapture.groovy}, die aber keine Relationen zueinander haben.

\begin{figure}[h!]
\centering
\includegraphics[width=1\linewidth]{images/uml-domain} 
\label{uml-domain}
\caption{Domain Models des GPSEval Servers}%
\end{figure}

Die Klasse \texttt{DecisionTree.groovy} beinhaltet einen Entscheidungsbaum, der im J48 Format vorliegt, sowie das Erstellungsdatum des Entscheidungsbaums. Das Erstellungsdatum ist von Interesse, da man dadurch die Entstehung der Entscheidungsbäume über einem Zeitraum visualisieren kann. Die Klasse \texttt{GPSCapture.groovy} beinhaltet die GPX-Aufnahme als String sowie ebenfalls das Datum des Tabelleneintrags. Wenn man sich die Schritte von Abschnitt \ref{chapter03-server} vergegenwärtigt, so sind die Domain Models in den Schritten 2, 3 (\texttt{GPSCapture.groovy}) und 7,8 (\texttt{DecisionTree.groovy}) von Bedeutung.

\subsubsection{Controller}
\label{chapter03-controller}
Die Controller sind zuständig, um Webanfragen zu verarbeiten und zu beantworten. In Grails befinden sich die Controller im \texttt{grails-app/controllers} Verzeichnis. Controller können direkt Views erstellen und diese Views als Antwort übersenden. Diese Variante empfiehlt sich, wenn die Ergebnisse direkt im Browser betrachtet werden, aber bei der Arbeit mit Klienten unterschiedlicher Platformen empfiehlt sich eine Trennung vom Backend und Frontend. Für die Zwecke des GPSEval Servers ist es daher nötig, die Ergebnisse in einem leichtgewichtigen und auf Androidseite leicht parsebaren Format zu erstellen und zu versenden ohne die Views mitzuerstellen. In unserem Fall haben wir uns für das JSON-Format entschieden, da dieses leichtgewichtig ist, auf androidfähigen Geräten leicht geparst werden kann und sich mittlerweile im Internet zu einem Quasistandard bei der Kommunikation zwischen Back- und Frontend etabliert.

Ein Controller hat mehrere so genannte Actions, in denen dann die Business Logik implementiert ist. Diese Actions stellen einen bedeutenden Teil der REST-Api da. In unserem Fall sehen die REST-Schnittstellen wie folgt aus:

\begin{center}
  \begin{tabular}{ p{2.1cm} | p{2.2cm} | p{1.4cm} | p{5.4cm} }
    \hline
    \textbf{Controller} & \textbf{Action} & \textbf{Param} & \textbf{Funktion} \\ \hline
    GPSCapture & setCapture() & capture & Speichere eine GPS Aufnahme. \\\hline
    DecisionTree & getLastTree() & - & Erhalte den aktuellsten Entscheidungsbaum. \\\hline
    DecisionTree & getTrees() & - & Erhalte alle bisher berechneten Entscheidungsbäume (beinhaltet auch veraltete). \\
  \end{tabular}
\end{center}

Durch diese Eigenschaften und den URL-Mappings aus Abschnitt \ref{chapter03-rest} erhalten wir URLs wie z. B.: \\\texttt{gpseval-server/GPSCapture/setCapture?capture=CAPTURE}.

\subsubsection{Services}
\label{sec:Weka_einbindung}
Die Services dienen dazu, Logik zu implementieren, die unabhängig von Controllern von überall aus auf dem Server erreichbar sein soll. Im GPSEval Server Projekt befinden sich zwei Services: Der \texttt{CsvService} bitete Hilfsfunktionen für die Arbeit mit CSV-Dateien, die genau das Format haben wie sie auch im GPSEval-Projekt genutzt werden. Der \texttt{DecisionTreeService} ist zuständig für die Erstellung von CSV-Dateien, für die Einbindung des GPSEval-Projekts auf dem Server und für die Erstellung des Entscheidungsbaumes.

\subsubsection*{Einbindung des Weka-Services}
Da das GPSEval-Projekt neben dem J48-Algorithmus zur Erstellung eines Entscheidungsbaums auch andere Klassifikatoren aufruft und zwischendurch Tests ausführt, die für uns nicht relevant sind, haben wir im Paket \path{de.fub.agg2graph.gpseval.evaluator} eigene Wrapper geschrieben, die nur die Teile des GPSEval-Projekts ausführen, die für uns wichtig sind. Die Struktur der Konfigurationsdateien haben wir dabei beibehalten.

Im \path{de.fub.agg2graph.gpseval.evaluator} Paket ist die Klasse \\ \path{GPSEvaluator.java} der Haupteinstiegspunkt zum Ansprechen des Weka-Services bzw. des gspeval-Projekts. Diese Klasse besitzt die Klassenmethode \\ \texttt{public void start(String[] args)}, die als Argument den Pfad zu einem Ordner bekommt, in dem sich die Konfigurationsdatei, namens \path{default.xml}, befindet. Dies wurde bewusst so gewählt, da man im Ordner so mehrere Konfigurationen ablegen kann und den Weka-Service hintereinander mit verschiedenen Einstellungen mit nur einem Aufruf ausführen kann. Für jede Konfiguration wird ein Baum erstellt und in einer Klassenvariable festgehalten. Am Ende eines Aufrufs können über die Methode \texttt{public List<String> getTrees()} alle erstellten Bäume zurückgegeben werden.

Um den Weka-Service in unserem Server einzubinden, muss in der Serveranwendung das agg2graph-Projekt als Jar in den Classpath eingebunden werden. Im \\\path{DecisionTreeService} kann dann das Paket \path{de.fub.agg2graph.gpseval.evaluator} eingebunden und der Weka-Service genutzt werden. Dies wird durch die Service-Methode \texttt{DecisionTreeService} \texttt{.createTree()} implementiert, die für die eigentliche Erstellung des Entscheidungsbaumes zuständig ist.

\subsubsection*{Erstellung des Entscheidungsbaums}
Die Methode \texttt{DecisionTreeService.createTree()} hat zwei Hauptfunktionen: Es holt sich zum einen die Captures aus der Datenbank und erstellt aus diesen CSV-Dateien und zum anderen werden mithilfe dieser CSV-Dateien der entscheidungsbaum erstellt und in der Datenbank gesichert. Dafür ruft die Methode \\\texttt{DecisionTreeService.createTree()} zwei private Methoden aufruft: \\\texttt{DecisionTreeService.createFiles()} und \texttt{DecisionTreeService} \texttt{.useWeka()}.

Wenn der Server Captures erhält, speichert dieser die Daten in der Datenbank. \texttt{DecisionTreeService.createFiles()} erzeugt die CSV-Daten anhand der Daten in der Datenbank und lagert diese im Ordner \texttt{tmpfiles}, der sich im \texttt{web-app}-Ordner befindet, wo auch Ordner für JavaScript- und CSS-Dateien vorhanden sind. 

\texttt{DecisionTreeService.useWeka()} überprüft zu Beginn, ob dieser \texttt{tmpfiles}-Ordner existiert und falls ja, ob mindestens zwei verschiedene Klassen vorhanden sind (z.B. Bus und Laufen). Erst wenn mindestens zwei vorhanden sind, wird ein Entscheidungsbaum erstellt. Nach der Überprüfung wird das Template \texttt{default\_template.xml} für die Konfigurationsdatei ausgelesen. Anschließend wird im \texttt{tmpfiles}-Ordner geschaut, welche Klassen vorhanden sind. Wenn z.B. Ordner für Bus- und Zug-Daten vorhanden sind, wird mithilfe des Templates eine Konfigurationsdatei gebaut, die im \texttt{webb-app/config}-Ordner abgelegt wird und die Pfade zu diesen beiden Ordnern enthält. Das ist notwendig, da sich Pfade je nach Server-Kontext ändern können und der Server-Nutzer sich nicht um die manuelle Anpassung der Pfade kümmern muss. Anschließend wird diese Konfigurationsdatei der \texttt{public void start(String[] args)} Methode übergeben, der Entscheidungsbaum wird berechne, über die Methode \texttt{public List<String> getTrees()} aufgerufen und in der Datenbank gespeichert.

\subsubsection*{Problem mit dem AvgTransportationDistance Feature}
Bei der Arbeit mit dem agg2graph-Projekt und dem Feature AvgTransportationDistance sind zwei Probleme aufgetreten, die nur dann auftreten, wenn das agg2graph-Projekt in Form einer Jar auf dem Server eingebunden wird. 

Wenn wir z.B. die \texttt{GPSEvaluator.java} Klasse innerhalb des agg2graph-Projekts instanziieren und aufrufen, so wird das AvgTransportationDistance-Feature normal berechnet und ist auch im Entscheidungsbaum zu sehen. Wird stattdessen das agg2graph-Projekt als Jar eingebunden und die Klasse \texttt{GPSEvaluator.java} auf dem Server instanziiert und der Entscheidungsbaum berechnet, so wird immer eine \texttt{FileNotFound} Exception geworfen. Beim Debuggen ist uns aufgefallen, dass es daran lag, dass auf dem Server der Pfad zur \texttt{BVG.zip} nicht aufgelöst werden konnte, der in \\ \texttt{de.fub.agg2graph.pt.StopTree} als relativer Pfad zu einem Unterordner im agg2graph-Projekt in der Jar definiert war. Um das zu beheben, haben wir für diese Variable einen Setter geschrieben, die \texttt{BVG.zip} im \texttt{web-app}-Ordner abgelegt und zur Server-Laufzeit den Pfad zu der Zip-Datei bestimmt. So konnten wir einen korrekten Pfad ermitteln und diese über den Setter noch vor dem Aufruf von \texttt{public void start(String[] args)} der \texttt{StopTree}-Klasse übergeben.

\subsubsection*{Problem mit der Einbindung des agg2graph-Projekts}
In Grails-Anwendungen werden Jar-Dateien üblicherweise auf folgende Art eingebunden: Die Jar-Datei wird z.B. in Eclipse erstellt (normale Jar-Dateien, keine \emph{Runnable Jar}-Datei). Dabei können einzelne Ordner des zu archivierenden Projekts abgewählt werden, die nicht benötigt werden. Anschließend muss die erstellte Jar-Dateien in das \path{lib}-Verzeichnis des Grails-Projekts und im Classpath eingefügt werden. Damit Grails die Änderungen bemerkt, muss in dem Projekt über die Kommandozeile \texttt{grails compile --refresh-dependencies} ausgeführt werden. Wenn dieser Befehl erfolgreich terminiert, kann man die Klassen aus dem agg2graph-Projekt einfach in den gewünschten Groovy-Klassen importieren \footnote{Siehe \url{http://grails.1312388.n4.nabble.com/Importing-java-jars-into-grails-td3336768.html}}. 

Allerdings hat das bei uns nicht funktioniert, und zwar konnte die importierte Klasse nicht aufgelöst werden. Wir vermuteten, dass es an der Entwicklungsumgebung \emph{GGTS} liegt, die wir für Grails-Anwendungen nutzen. Als dies ebenfalls von der Konsole aus nicht funktionierte, haben wir die Grails-Mailingliste angeschrieben. Diese hat vermutet, dass es an der Grails Version liegt, die wir nutzen\footnote{\footnote{Grails 2.1.1}}, und dass diese die Libraries innerhalb der agg2graph Jar-Datei nicht korrekt als Abhängigkeiten auflösen kann. Als Lösung hat einer der Hauptentwickler von Grails vorgeschlagen, die benötigten Bibliotheken innerhalb des agg2graph-Projekts nochmals in das lib-Verzeichnis der Grails-Anwendung zu kopieren bzw. einzubinden und das agg2graph-Projekt als Runnable Jar-Dateien zu exportieren bzw. diese Jar-Datei in der Grails-Anwendung einzubinden. Die Main-Klasse der Runnable Jar-Datei muss dabei \path{de.fub.agg2graph.gpseval.evaluator.GPSEvaluator} sein. Die Entwickler haben dieses Verhalten als Bug festgehalten. Für uns bedeutet das, dass einige Bibliotheken zwar redundant vorhanden sind und auch nicht im Nachhinein aus dem Archiv entfernt werden können, aber so können die Abhängigkeiten korrekt aufgelöst und das agg2graph-Projekt genutzt werden. Außerdem wird durch die Redundanz die Performance keineswegs verschlechtert, sodass das für uns keine Nachteile mit sich bringt.