#!/usr/bin/env bash
SRC_DIR="src/"

case $1 in
"")
cd $SRC_DIR
latex --interaction=nonstopmode thesis.tex
bibtex thesis.aux
pdflatex --interaction=nonstopmode thesis.tex
pdflatex --interaction=nonstopmode thesis.tex
mv thesis.pdf ../
;;

clean)
echo "Entferne temporäre Dateien"
rm thesis.pdf
cd $SRC_DIR
rm  *.aux *.bbl *.blg *.dvi *.htm *.html *.css *.log *.out *.pdf *.ps \
	*.toc *.blg *.lol *.lot *.lof *.glg *.gls\
  *.ist *.nlo *.acn *.acr *.alg *.glo *.ilg *.ind *.nls \
  thesis-blx.bib &> /dev/null
;;

*)
echo "Usage: $0 [clean]"
;;
esac
