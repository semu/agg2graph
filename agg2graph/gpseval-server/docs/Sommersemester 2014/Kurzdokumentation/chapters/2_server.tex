\section{Einrichtung des Servers}
In diesem Abschnitt wird beschrieben, wie die Entwicklungsumgebung für die Entwicklung der Server-Applikation und die Server-Applikation selbst als Projekt eingerichtet werden kann. Außerdem wird erklärt, wie man am Ende einer Entwicklung aus dem Server-Projekt eine ausführbare war-Datei\footnote{Web-Archiv Datei, eine Datei, die eine vollständige Webanwendung nach Java-Servlet-Spezifikation beschreibt} erstellen kann, diese auf einen Server deployen kann und welche Fallstricke bei der Arbeit und beim Deployen des Servers auftreten können. In diesem Abschnitt wird angenommen, dass bereits ein Apache Tomcat Server auf einem beliebigen Rechner eingerichtet ist. Dementsprechend werden die Schritte beschrieben. Falls ein anderer Server eingerichtet ist, so ist bitte der Server-Dokumentation das Deployment der Anwendung zu entnehmen.

\subsection{Grails}
Auf dem Server wird das Webframework \emph{Grails} eingesetzt. Grails ist ein durch Ruby on Rails beeinflusstes MVC-Webframework. Durch den Einsatz der populären dynamisch typisierten Programmier- und Skriptsprache Groovy, weiteren Konzepten wie Scaffolding, automatischen Validatoren, Internationalisierung sowie einem umfangreichen Pluginverzeichnis wird Grails eingesetzt, um effektiv Webprojekte auf Basis der JVM umzusetzen. Grails-Anwendungen laufen komplett auf einer installierten JVM.

Für die Entwicklung des Servers wurde Grails 2.1.1\footnote{Downloadbar unter \url{http://dist.springframework.org.s3.amazonaws.com/release/GRAILS/grails-2.1.1.zip}} verwendet. Für die Entwicklung des Servers reicht es aus, das zip-Paket lediglich an einem beliebigen Ort zu entpacken. Das Anlegen von Umgebungsvariablen ist optional, aber nicht notwendig, da das bei Verwendung der speziell für Grails vorhandenen Entwicklungsumgebung nicht gebraucht wird.

\subsection{GGTS}
Die \emph{Groovy/Grails Tool Suite} (GGTS) ist eine Eclipse-Entwicklungsumgebung für die Entwicklung von Groovy und Grails Anwendungen. Gegenüber der Entwicklung mit der Kommandozeile bietet GGTS viele Vorteile:
\begin{itemize}
\item Groovy und Grails Projekterstellungs- und verwaltungswerkzeuge
\item Grails Perspektive
\item Grails Plugin Manager
\item Groovy 1.7/1.8/2.x Support
\item Grails 1.3/2.x Support
\end{itemize}
Unter\footnote{\url{http://www.springsource.org/downloads/sts-ggts}} kann GGTS heruntergeladen werden. Während der Installation wird der Nutzer gefragt, ob neben der IDE auch der \emph{vFabric tc Server} und \emph{Grails 2.2.1} mitinstalliert werden soll. Der vFabric tc Server ist von SpringSource und ist ein schlanker Server, der auf den Apache Tomcat Server basiert. Dieser Server dient dazu, die Grails Anwendung in einer virtuellen Serverumgebung zu testen, sodass man die Anwendung nicht immer wieder neu deployen muss. Den Server sollte man auswählen, Grails 2.2.1 jedoch nicht, da die Anwendung in Grails 2.1.1 entwickelt wurde und das manuell in GGTS noch angepasst werden muss. Ist GGTS installiert, muss zuerst das \emph{agg2graph}-Repository\footnote{\url{https://github.com/sebastian-fu/agg2graph.git}} geklont werden (z.B. mittels EGit, welches in GGTS bereits vorhanden ist). In diesem Repository befindet sich unter anderem das \emph{gpseval-server}-Projekt, welches importiert werden muss. Ist das Projekt geklont, so muss in der Git Repository View über Rechtsklick und dann unter \texttt{Import -> Import Existing Projects} das \emph{gpseval-server} Projekt als \texttt{Grails Project} importiert werden. Ist das Projekt importiert, so muss nun GGTS so eingestellt werden, dass es bei der Arbeit \emph{Grails 2.1.1} verwendet. Unter \texttt{Preferences -> Groovy -> Grails} kann man beliebig viele Grails-Binaries eintragen. Bei frischer Installation sollte dort kein Eintrag vorliegen. Um für die Entwicklung am Server Grails 2.1.1 zu nutzen, drückt man auf \texttt{Add}, sucht sich über \texttt{Browse} den heruntergeladenen Grails 2.1.1-Ordner heraus und klickt zwei Mal auf \texttt{Ok}. Anschließend kann man am Servercode entwickeln. 

\subsubsection{Ausführen der Anwendung}
Es empfiehlt sich, die Anwendung noch vor dem Deployen auf einem Server zuerst lokal auf dem mitgelieferten vFabric tc Server zu testen. Dazu muss man lediglich im Feld \path{Grails Command History} den Befehl \texttt{run-app} eingeben und Enter drücken (siehe Abbildung~\ref{fig:commandHistory}). 
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\linewidth]{images/commandHistory}
\caption{Klickt man auf die Command History (oberer Kreis), so öffnet sich die GGTS-eigene Grails-Kommandozeile (unterer Kreis). Dort kann man beliebige Grails-Befehle absetzen.}
\label{fig:commandHistory}
\end{figure}
Ist das virtuelle Deployment bereit, so erscheint in der IDE-Konsole der Link \\ \\ \texttt{http://localhost:8080/gpseval-server}. \\ \\ Damit ist die Server-Anwendung lokal deployt und kann über den Link genutzt werden. Auf der Startseite der Anwendung erhält man eine Auflistung aller Server-Schnittstellen, die im Javadoc und in der Projektdokumentation \cite{doku-2013} beschrieben sind.

\subsubsection{Environments}
In jedem Grails-Projekt befindet sich die \path{grails-app/conf/DataSource.groovy} Datei. In dieser Datei können verschiedene Ausführungsumgebungen~\cite{lucastex2011} definiert werden. Für jede Umgebung kann eine darunterliegende Datenbank definiert werden. So kann man unter \texttt{development} z.B. eine \emph{H2-InMemory} Datenbank verwenden, um bei der Entwicklung schnell Funktionen testen zu können und anschließend die \texttt{production} Umgebung nutzen, in der z.B. eine Postgres-Datenbank eingetragen ist und die Bedingungen auf dem Production-Server wieder spiegeln soll. Wenn man in der Grails Command History \texttt{run-app} eingibt, wird standardmäßig im Hintergrund die \texttt{development}-Umgebung genutzt. Möchte man eine andere Umgebung nutzen, so gibt man den Befehl \texttt{<Umgebungabk.> run-app} ein, z.B. \texttt{prod run-app} für die \texttt{production} Umgebung. Im \emph{gpseval-server}-Projekt ist in der \texttt{development}-Umgebung eine H2-Datenbank in einer Datei eingestellt.

\subsection{Deployen auf einem Server}
Um die fertige Grails-Anwendung auf einen Server zu deployen, muss in der Grails Command History \texttt{dev war} eingegeben werden\footnote{oder \texttt{prod war}, wenn man auf dem Zielserver keine h2-Datenbank, sondern eine in der production-Umgebung definierte Datenbank verwenden möchte}. Die erstellte war-Datei wird dann unter \path{<Grails-Projekt>/target} abgelegt. 

Anschließend benutzt man den Apache Tomcat Manager\footnote{Oder den Manager des eigenen Servers} seines Servers, deployt seine war-Datei und beim erfolgreichen Deployen kann die Anwendung über seine Schnittstellen genutzt werden.

\subsection{Ausführen der Server-Tests}
Für die Wrapper-Klassen (siehe Abschnitt \ref{sec:wrapper}) existieren keine Tests, da diese nur Wrapper für die GPSEval-Testumgebung sind, welche bereits Tests haben. Für die Server-Anwendung existieren sowohl Unit- als auch Integrationstests. Bei Unit-Tests werden meist individuelle Methoden, Codeblöcke oder Komponenten getestet, ohne an das Zusammenspiel mehrere Komponenten zu beachten. Unit-Tests betrachten also keine Datenbanken, Socket-Verbindungen oder Dateioperationen. Im Gegensatz dazu hat man bei Integrationstests den vollen Zugriff auf die Grails Umgebung und auf die Datenbank. Die Tests befinden sich bei jedem Grails-Projekt im Hauptverzeichnis unter \path{test}\footnote{Siehe \url{https://github.com/sebastian-fu/agg2graph/tree/master/agg2graph/gpseval-server/test}} und können über die GGTS- eigene Kommandozeile über \texttt{test-app} (oder Rechtsklick auf das Projekt, dort unter \texttt{Run as} auch \texttt{test-app}).

\subsection{GPSEval Wrapper}
\label{sec:wrapper}
Im agg2graph-Projekt existiert eine Testumgebung, namens \\ \path{de.fub.agg2graph.gpseval}~\cite{ManuelKotlarski2013}, welche den Mobilitätsmodus erkennt. Da nicht alle Funktionen des gpseval-Projekts verwendet werden mussten, wurden Wrapper für das gpseval-Projekt unter \\ \texttt{de.fub-agg2graph.gpseval.evaluator} geschrieben, die die Funktionen des gpseval-Projekts in der benötigten Form aufrufen und in einem für den Server verständlichen Format speichern. 

Möchte man an den Wrappern Änderungen vornehmen, so müssen einige Dinge beachtet werden, um diese der Server-Anwendung bekannt zu machen. Wenn generell am agg2graph-Projekt Änderungen vorgenommen werden, muss das agg2graph-Projekt erneut in eine Jar-Datei exportiert und diese in der Server-Anwendung neu eingebunden werden. In der Projektdokumentation 2013 \cite{doku-2013} wurde im Kapitel \glqq Problem mit dem AvgTransportationDistance Feature\grqq  bzw. \glqq Problem mit der Einbindung des agg2graph-Projekts\grqq  beschrieben, dass bei der Entwicklung der Server-Applikation Probleme aufgetreten sind. Es hat sich herausgestellt, dass es sich dabei um einen Bug in der Grails 2.1.1 Version handelt, welches einige Libraries in der agg2graph Jar-Datei nicht als Abhängigkeit auflösen konnte, sodass Klassen des agg2graph-Projekts im Grails-Projekt nicht importiert werden konnten. Um trotzdem agg2graph nutzen zu können, haben uns die Grails-Entwickler nach Anfrage in der Mailingliste folgenden Lösungsvorschlag gemacht: Es müssen zum einen die benötigten Libraries aus dem agg2graph-Projekt in der Grails-Anwendung nochmals eingebunden werden und zum anderen darf das agg2graph-Projekt keine normale Jar-Datei sein, sondern es muss eine runnable Jar-Datei sein, die als Main-Klasse die Klasse \path{de.fub.agg2graph.gpseval.evaluator.GPSEvaluator} besitzt.

Kurz gefasst bedeutet das, dass bei Änderungen im agg2graph-Projekt, die der Server-Anwendung ebenfalls bekannt gemacht werden sollen, das agg2graph-Projekt als \textbf{Runnable Jar-Datei} exportiert und dementsprechend in der Grails-Anwendung eingebunden werden muss.

\subsection{Mögliche Probleme}
\label{sec:moegliche_probleme}
Entwickelt und führt man die Server-Applikation auf einem virtuellen Server oder unter einem lokalen Server mit einer Java 7 Version, so treten beim Deployen keine Probleme auf. Nutzt man jedoch eine Serverumgebung mit Java 6 oder geringer, so kriegt man beim Deployen und Ausführung der Server-Applikation die folgende oder ähnliche Exception:
\begin{verbatim}
java.lang.UnsupportedClassVersionError:
de/fub/agg2graph/gpseval/evaluator/GPSEvaluator : 
Unsupported major.minor version 51.0 (unable to load class
de.fub.agg2graph.gpseval.evaluator.GPSEvaluator)
\end{verbatim}
Erhält man diese oder ähnliche Exception, so liegt das daran, dass einige Klassen im agg2graph-Projekt in Java 7 erstellt und kompiliert wurden und nicht von geringeren Java-Versionen unterstützt werden. In unserem Fall werden für die Berechnung des \texttt{AvgTransportationDistance} Features unter anderem der sogenannte \texttt{Diamant-Operator} verwendet, den es erst in Java 7 gibt und somit diese Klassen auf dem Server nicht kompiliert werden können, wenn eine ältere Java-Version installiert ist.

Während unserer Entwicklung trat dieses Problem auf, da auf unserem Testserver Java 5 installiert war. Wir haben es gelöst, indem wir schlicht Java 7 sowohl JDK als auch JRE installiert haben. Danach konnte die Anwendung problemlos benutzt werden.
