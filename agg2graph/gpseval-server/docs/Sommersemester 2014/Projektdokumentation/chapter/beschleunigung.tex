\section{Ausarbeitung zum Beschleunigungssensor}\label{sec:Beschleunigungssensor}

In diesem zusätzlichen Kapitel geht es um die Nutzung von Beschleunigungsmessungen im Gerät für die Bestimmung des Mobilitätsmodus. Das Zählen von Schritten ist ein Beispiel dafür, wo der Beschleunigungssensor häufig Verwendung findet. Wir möchten in diesem Kapitel beleuchten, wie man mithilfe des Beschleunigungssensors zwischen den verschiedenen Arten sich fort zu bewegen unterscheiden kann. Dabei geben wir zunächst einen Überblick über die Verwendung eines Beschleunigungssensors in Android. Anschließend betrachten wir das Berechnen von Features aus Beschleunigungswerten sowie deren Klassifizierung anhand eines ausgewählten wissenschaftlichen Artikels.

\subsection{Bewegungssensoren in Android}
Android stellt eine API\footnote{\url{http://developer.android.com/guide/topics/sensors/sensors_motion.html}} für die Verwendung von Bewegungssensoren bereit. Es können somit die Sensoren auf jedem Gerät in der selben Art und Weise angesprochen werden, wenn der Gerätehersteller sie zur Android-Plattform kompatibel gemacht hat.\\
Es gibt zwei generelle Arten von Bewegungssensoren - den Beschleunigungs- sowie den Drehungssensor (Gyroskop). Beide sind rein hardwarebasiert. Davon abgeleitet gibt es aber noch einige weitere Sensoren, die software- oder hardwareseitig Daten aufnehmen. Zum Beispiel kann ein Sensor seine Daten von einem Beschleunigungs- \& Magnetfeldsensor erhalten und per interner Software verarbeiten, während eine andere Implementierung den Drehungssensor rein hardwaretechnisch in einem bestimmten Rhythmus abfragt und dadurch zu den gleichen Ergebnissen kommt.\\
\\
In Abbildung \ref{android_sensoren} ist eine Übersicht zu einigen Sensoren schematisch dargestellt, die aus Beschleunigungs- und Drehungssensor abgeleitet sind.
\begin{figure}[htpb]
	\center
	\includegraphics[scale=0.6]{images/Android_Bewegunssensoren.png}
	\caption{Bewegungssensoren in Android}
	\label{android_sensoren}
\end{figure}\\
Zum Beispiel berechnet ein Gravitationssensor den Betrag der momentan wirkenden Gravitation auf das Gerät sowie dessen Richtung mithilfe des Beschleunigungssensors. Der reine Beschleunigungssensor misst die Beschleunigung in X-,Y- und Z-Richtung im 3-Achsen-Koordinatensystem, wie in Bild \ref{android_koordinatensystem} zu sehen. Dabei wirkt allerdings auch die natürliche Gravitation des Planeten mit ein. 

\begin{figure}[htpb]
	\center
	\includegraphics[scale=0.6]{images/android_axis_device.png}
	\caption{Koordinatensystem relativ zum Gerät}
	\label{android_koordinatensystem}
\end{figure}

In vielen Fällen möchte man diese Gravitation herausfiltern. Dafür bietet Android den linearen Beschleunigungssensor, welcher den Einfluss der Gravitation von den Messwerten abzieht. Für unsere App verwenden genau diesen, da uns nur die Beschleunigung interessiert, die durch Bewegung des Gerätes entsteht. Wobei Bewegung auch bedeutet, dass sich das Gerät eventuell ruhig in einer Hosentasche befindet, aber gleichzeitig auch innerhalb eines fahrenden Autos, so dass hier auch die Beschleunigung des Autos gemessen werden kann.

\subsection{Android Sensor-API}\label{sec:android-sensor-api}
Am Beispiel des linearen Beschleunigungssensors möchten wir die Verwendung der Sensor-API mit einem Codebeispiel demonstrieren:
\begin{lstlisting}
// Sensor registrieren
public class MainActivity extends Activity {
    ...
    mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    mSensorManager.registerListener(sensorTracker, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    ...

// Sensordaten erhalten
public class SensorTracker implements SensorEventListener {
    @Override
    public void onSensorChanged(SensorEvent event) {
        float X = event.values[0];
        float Y = event.values[1];
        float Z = event.values[2];
        ...
\end{lstlisting}
Eine Android-Anwendung besteht aus mindestens einer Aktivität. Eine davon wird beim Start der App geladen und ausgeführt, wie in unserem Fall die \textbf{MainActivity}. Um Daten vom Beschleunigungssensor zu erhalten, muss dafür ein Listener angemeldet werden. Dafür legt man zunächst einen \textbf{SensorManager} an (Zeile 4), welcher Referenzen auf den gewünschten Sensor zurück gibt (Zeile 5). In Zeile 6 wird der Sensor angemeldet. Die Konstante \textbf{$SensorManager.SENSOR\_DELAY\_NORMAL$} steht für die erlaubte Verzögerung zwischen Messen und Erhalten der Daten. Für Anwendungen wie z.B. Simulationsspiele, kann es wichtig sein, die Daten so schnell wie möglich zu erhalten. Wir haben beim Testen keine starken Verzögerungen bemerkt und auch mit \textbf{$SensorManager.SENSOR\_DELAY\_NORMAL$} unmittelbar die Beschleunigungswerte erhalten. Die \textbf{registerListener}-Methode benötigt zusätzlich noch eine Listener-Instanz als Parameter. In unserem Fall ist das der \textbf{sensorTracker}. Dessen Implementierung ist kurz in Zeile 10-15 skizziert.\\
\\
Um die Beschleunigungswerte vom angemeldeten Listener zu erhalten, muss das \textbf{SensorEventListener}-Interface implementiert werden (Zeile 10), welches u.a. die Methode \textbf{onSensorChanged} enthält. Ihr wird ein \textbf{SensorEvent} übergeben, welches die Werte für die Beschleunigung in X-,Y- und Z-Richtung enthält (Zeile 11-15). Die Methode wird von Android jedes Mal aufgerufen, wenn der Sensor eine Änderung bemerkt.

\subsection{Erkennung des Mobilitätsmodus anhand der Beschleunigung}
Im wissenschaftlichen Artikel \textit{"`Using Mobile Phones to Determine Transportation Modes"'} \cite{Reddy:2010:UMP:1689239.1689243} der Universität Kalifornien wird ein System vorgestellt, dass den Mobilitätsmodus eines mobilen Gerätes erkennen kann. Das Gerät muss dabei mit einem GPS-Empfänger sowie mit einem Beschleunigungssensor ausgestattet sein, was auf die meisten modernen Smartphones heutzutage zutrifft. Das vorgestellte System kann anhand der aufgenommen Werte zwischen den Modi "`Ruhen"', "`Gehen"', "`Schnell Laufen"', "`Fahrrad fahren"' und "`Motorisiert fahren"' unterscheiden. Man kann erkennen, dass sich der Artikel mit dem gleichen Problem auseinandersetzt wie wir in diesem Projektseminar.\\
\\
Zu Beginn des Artikels wird diskutiert, welche Sensoren eines gängigen Smartphones zur Erkennung des Mobilitätsmodus am geeignetsten sind. Es stellt sich heraus, dass GSM, Wifi und Bluetooth kaum einen Mehrwert gegenüber GPS und Beschleunigungssensor bringen. Außerdem wird erklärt, dass die Kombination aus GPS-Empfänger- und Beschleunigungswerten $10-20\%$ höhere Erkennungsraten ergibt, als wenn nur einer der beiden Sensoren verwendet wird. Um beispielhaft die Bedeutung des Beschleunigungssensors zu verdeutlichen, wurde für den Artikel die Verteilung von Geschwindigkeiten der unterschiedlichen Transportmodi 30 Minuten aufgenommen sowie die zugehörige Variation der Beschleunigung (Abbildung \ref{verlauf_geschwindigkeiten_beschleunigung}).

\begin{figure}[ht]
	\begin{subfigure}[b]{0.8\textwidth}
	\includegraphics[width=\textwidth]{images/beschleunigung1.png}
	\caption{Geschwindigkeitsverteilung nach 30 Minuten  \cite{Reddy:2010:UMP:1689239.1689243}}
	\label{verteilung_geschwindigkeiten}
	\end{subfigure}
	\begin{subfigure}[b]{0.7\textwidth}
	\includegraphics[width=\textwidth]{images/beschleunigung2.png}
	\caption{Beschleunigungsverlauf nach 30 Minuten \cite{Reddy:2010:UMP:1689239.1689243}}
	\label{variaton_beschleunigung}
	\end{subfigure}
	\caption{}
	\label{verlauf_geschwindigkeiten_beschleunigung}
\end{figure}

Es ist zu erkennen, dass die Geschwindigkeiten für "`Schnell Laufen"' ("`Run"') und "`Fahrrad fahren"' ("`Bike"') in Abb. \ref{verteilung_geschwindigkeiten} ziemlich ähnlich sind, dagegen aber die Beschleunigung von "`Schnell Laufen"' in Abb. \ref{variaton_beschleunigung} viel höhere Maximal- und Minimalwerte annimmt als bei "`Fahrrad fahren"'. Somit können solche kurzzeitigen An- und Abstiege der Beschleunigung, wie sie vom Beschleunigungssensor registriert werden, auch zur eindeutigen Klassifizierung beitragen.

\subsubsection{Berechnung der Features}\label{sec:beschleunigung_featureberechnung}
Um die Beschleunigung unabhängig von der Lage und Ausrichtung des Gerätes zu berechnen, wird im Artikel der Betrag aus allen 3 räumlichen Achsen wie folgt berechnet:
\[ A_{mag} = \sqrt{ (A_x)^2 + (A_y)^2 + (A_z)^2} \]

Als Abtastrate für den Beschleunigungssensor wurde 1-10 Hz gewählt, da sich dort die besten Ergebnisse ergaben. Bei kleineren Raten gab es große Schwankungen in der Genauigkeit und bei größeren treten mehr Einflüsse von Störungen auf, die durch verschiedene gleichzeitig wirkende Kräfte verursacht werden.\\
Als Features für die Beschleunigungsmesswerte wurden \textbf{Durchschnitt, Varianz, Energie und DFT (Diskrete Fourier Transformation)} evaluiert. Letztendlich wurden DFT und Varianz für die Klassifizierung gewählt.\\
\\
Für die GPS-Daten wurde als Feature die \textbf{Geschwindigkeit} gewählt, wobei in einem Vorberechnungsschritt stark abweichende Werte ausgefiltert werden.

\subsubsection{Klassifikatoren}
Das Schema der Klassifizierung wird in Abbildung \ref{paper_klassifizierungsschema} dargestellt. 

\begin{figure}[htpb]
	\center
	\includegraphics[scale=0.15]{images/paper_klassifizierungsschema.png}
	\caption{Ablauf der Klassifizierung nach \cite{Reddy:2010:UMP:1689239.1689243}}
	\label{paper_klassifizierungsschema}
\end{figure}

Es wird ein zweistufiges Klassifizierungssystem verwendet. Für den ersten Schritt wurden die folgenden Klassifikatoren verglichen: \textbf{Decision Tree (DT), K-Means Clustering (KMC), Naive Bayes (NB), Nearest Neighbor (NN)} und  \textbf{Support Vector Machines (SVM)}. Als Bester dieser Auswahl wurde der \textbf{Decision Tree (DT)} ermittelt.\\
Für die 2. Stufe wird ein \textbf{Hidden Markov Model (DHMM)} benutzt, dass wie ein Zustandsautomat die Ergebnisse der ersten 1. Stufe in Endergebnisse für die Klassifikation umwandelt. Dabei werden Zustandsübergänge mit Wahrscheinlichksbeziehungen zwischen den Klassen trainiert.

\subsubsection{Resultate}
Für die Evaluation des Artikels wurden Daten von 16 Personen für jeweils 15 Minuten pro Transportmodus gesammelt. Als Erkennungsrate nur für die 1. Stufe, d.h. nur mit dem Decision Tree als Klassifikator, ergab sich eine Quote von $91,6\%$. Kombiniert mit dem Hidden Markov Model als 2. Stufe stieg die Erkennungsrate auf $93,7\%$.
