\documentclass[ucs,9pt]{beamer}

% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice.
%
% Modified by Tobias G. Pfeiffer <tobias.pfeiffer@math.fu-berlin.de>
% to show usage of some features specific to the FU Berlin template.

% remove this line and the "ucs" option to the documentclass when your editor is not utf8-capable
\usepackage[utf8x]{inputenc}    % to make utf-8 input possible
\usepackage[english]{babel}     % hyphenation etc., alternatively use 'german' as parameter
\usepackage{algorithm}
\usepackage{algorithmic}
\usefonttheme[onlymath]{serif}

\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true
  tabsize=3
}

\include{fu-beamer-template}  % THIS is the line that includes the FU template!

\usepackage{arev,t1enc} % looks nicer than the standard sans-serif font
% if you experience problems, comment out the line above and change
% the documentclass option "9pt" to "10pt"

% image to be shown on the title page (without file extension, should be pdf or png)
\titleimage{fu_500}

\title[Dynamische Mobilit\"atserkennung]{Dynamische Mobilit\"atserkennung \newline Projektseminar Datenverwaltung}


\author{Daniel Neumann \and Martin Liesenberg}
% - Give the names in the same order as the appear in the paper.

\institute[FU Berlin] % (optional, but mostly needed)
{Freie Universität Berlin}
% - Keep it simple, no one is interested in your street address.

\date{Abschlusspr\"asentation, \today}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

% you can redefine the text shown in the footline. use a combination of
% \insertshortauthor, \insertshortinstitute, \insertshorttitle, \insertshortdate, ...
\renewcommand{\footlinetext}{\insertshortinstitute, \insertshorttitle, \insertshortdate}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSubsection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\beamertemplatenavigationsymbolsempty

\begin{document}
\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}

\section{Motivation \& \"Uberblick}

\begin{frame}{Motivation \& Herausforderungen}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[T]{0.5\textwidth} % each column can also be its own environment
			\centering \textbf{Motivation}
			\begin{itemize}
				\setlength{\itemsep}{20pt}
		  		\item Automatisch gesammelte Statistiken zu Bewegungsmustern im öffentlichen Raum
				\item Gezieltere Platzierung von Werbung
				\item Verbesserte Verkehrsauskunft
				\item Kontextinformationen
  			\end{itemize}
  		\end{column} \pause
  		\begin{column}[T]{0.5\textwidth} % each column can also be its own environment
  			\centering \textbf{Herausforderungen}
			\begin{itemize}
				\setlength{\itemsep}{20pt}
				\item Einfache Unterscheidung an Hand der Geschwindigkeit nicht m\"oglich
				\item Unterschiedliche Transportmodi pro Trip
				\item Dynamische \"Anderungen in Verkehrsbedingungen und Wetter
  			\end{itemize}		
		\end{column}
  	\end{columns}
\end{frame}

\begin{frame}{\"Uberblick -- Architektur}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[T]{0.5\textwidth} % each column can also be its own environment
			\begin{figure}[hbtp]
				\includegraphics[scale=0.25]{sequence.pdf}
				\caption{Sequenzdiagramm App Server Interaktion \cite{3}}
			\end{figure}		
  		\end{column} \pause
  		\begin{column}[T]{0.5\textwidth} % each column can also be its own environment
			\begin{figure}[hbtp]
				\includegraphics[scale=0.25]{verlauf_app.png}
				\caption{Funktionsablauf innerhalb der Android App \cite{3}}
			\end{figure}		
		\end{column}
  	\end{columns}
\end{frame}

%\begin{frame}{\"Uberblick -- Funktionalit\"at}
%	\begin{itemize}
%		\item Erkennung des Mobilitätsmodus
%		\item Android App, die Fortbewegung des Nutzers aufzeichnet
%		\item App sendet Daten an Server, der einen Klassifikator trainiert 
%		\item App empfängt Parameter des trainierten Klassifikator und nutzt diese zur dynamischen Erkennung des Mobilitätsmodus
%	\end{itemize}
%\end{frame}

\begin{frame}{\"Uberblick -- Ausgangslage}
	\begin{itemize}
	\setlength{\itemsep}{10pt}
		\item Android App
	    \begin{columns}
		    \begin{column}{0.7\textwidth} % each column can also be its own environment
            	\begin{itemize}
             		\item Weka zu groß für App\\ $\rightarrow$ eigene Implementierung für die Auswertung des aktuellen Modus nötig \medskip
              		\item Decision Tree Klassifikator
              		\item 3 Features: \textit{MaxSpeed}, \textit{AvgSpeed} und \textit{AvgTransportationDistance}
            	\end{itemize}
            \end{column}
            \begin{column}{0.1\textwidth} % each column can also be its own environment
            	\begin{figure}[hbtp]
				    \includegraphics[scale=0.1]{Android_Robot_200.png}
			    \end{figure}
            \end{column}
        \end{columns}
		\item agg2graph Bibliothek
		\begin{itemize}
		  	\item stellt Funktionen zum Klassifizieren der CSV Dateien bereit
		 	\item Nutzung der Weka Klassifikatoren: Decision Tree, Random Forest, Naive Bayes und MultilayerPerceptron
		  	\item 19 Features implementiert
		\end{itemize}
		\item Grails Server
		\begin{columns}
		    \begin{column}{0.7\textwidth} % each column can also be its own environment
            	\begin{itemize}
		          	\item Kommunikation mit App und Nutzung von agg2graph zum Klassifikator Training
		          	\item Durch technische Limitierung der App nur 3 Features + Decision Tree verwendet
            	\end{itemize}
            \end{column}
            \begin{column}{0.1\textwidth} % each column can also be its own environment
            	\begin{figure}[hbtp]
				    \includegraphics[scale=0.3]{grails.png}
			    \end{figure}
            \end{column}
        \end{columns}
	\end{itemize}
\end{frame}

\section{Neurungen}
\subsection{Random Forest}
\begin{frame}{Random Forest -- \"Uberblick}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[T]{0.5\textwidth} % each column can also be its own environment
  			\begin{itemize}
  			\setlength{\itemsep}{15pt}
    			\item Ensemble einzelner Entscheidungsb\"aume
    			\item Mehrheitsvotum entscheidet
    			\item Konstruktionskomplexit\"at \cite{1} $O(M * (nm \log n)$, mit
    			\begin{itemize}
    				\item[$M$] Anzahl der B\"aume
    				\item[$n$] Gr\"o\ss e des Trainingssets
    				\item[$m$] Anzahl der Features
    			\end{itemize}
  			\end{itemize}
  		\end{column}
  		\begin{column}[T]{0.5\textwidth} % each column can also be its own environment
			\begin{figure}[hbtp]
				\includegraphics[scale=0.9]{random_forest.png}
			\end{figure}		
		\end{column}
  	\end{columns}
\end{frame}
\begin{frame}{Random Forest -- Training}
	\begin{algorithm}[H]
		\begin{algorithmic}
			\FOR{$i$ from $1$ to$t$}
				\FOR{$j$ from $1$ to e.size}
					\STATE $e_{j}^{'} \leftarrow e_{rand(1,n)}$ 
				\ENDFOR
				\STATE $T_i \leftarrow RandomTree(\langle e_{1}^{'}, \ldots e_{n}^{'} \rangle, f)$
			\ENDFOR
			\RETURN $T_1 \ldots T_t$
		\end{algorithmic}
		\caption{RandomForest(examples e, features f, trees t)}
	\end{algorithm}
\end{frame}

\subsection{Features}
\begin{frame}{Features -- Wetter -- \"Uberblick}
	\begin{figure}[hbtp]
		\includegraphics[scale=0.21]{weather_api.png}
		\caption{Funktionen von openweathermap.org \cite{2}}
	\end{figure}		
\end{frame}

\begin{frame}{Features -- Wetter -- openweathermap API}
  	\begin{figure}[hbtp]
		\includegraphics[scale=0.55]{api_response.png}
		\caption{JSON Darstellung einer Antwort der API}
	\end{figure}	
\end{frame}

\begin{frame}{Features -- Weitere -- Stoprate}
	\centering \textbf{Stoprate}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[c]{0.35\textwidth} % each column can also be its own environment
			\begin{align*}
				SR &= \frac{\vert P_s \vert}{Distance} \\
				P_s &= \lbrace p_i \vert p_i \in P \wedge p_i.V < V_s \rbrace
			\end{align*}
  		\end{column}
  		\begin{column}[c]{0.65\textwidth} % each column can also be its own environment
			\begin{figure}[hbtp]
				\includegraphics[scale=0.23]{stoprate.png}
				\caption{Illustration Stoprate f\"ur verschiedene Transportmittel \cite{4}}
			\end{figure}		
		\end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Features -- Heading Change Rate (HCR)}
	\centering \textbf{Heading Change Rate}
	\begin{figure}[hbtp]
		\includegraphics[scale=0.25]{hcr.png}
		\caption{Illustration HCR Auto vs Fu\ss g\"anger \cite{4}}
	\end{figure}
	\begin{align*}
		HCR &= \frac{\vert P_c \vert}{Distance} \\
		P_c &= \lbrace p_i \vert p_i \in P \wedge p_i.HC < HC_s \rbrace
	\end{align*}
\end{frame}

\subsection{Beschleunigungssensor}
\begin{frame}{Beschleunigungssensor}
	\begin{itemize} \setlength{\itemsep}{10pt}
		\item Bewegungssensoren in Android:
		\begin{itemize} \setlength{\itemsep}{5pt}
		    \item Erkennen von Bewegungen wie Neigen, Drehen, Schütteln, Schwingen...
		    \item 2 Hardwarearten: Beschleunigungssensor und Gyroskop
		    \item davon abgeleitet noch weitere: z.B. folgende
		\end{itemize}
	\end{itemize}
			    		        \begin{figure}[hbtp]
            \includegraphics[scale=0.4]{Android_Bewegunssensoren.png}
        \end{figure}
\end{frame}

\begin{frame}[fragile]{Beschleunigungssensor}
    \begin{lstlisting}
    // Sensor registrieren
    public class MainActivity extends Activity {
        ...
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        
        mSensorManager.registerListener(sensorTracker, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        ...
    \end{lstlisting}
    \begin{lstlisting}
    // Sensordaten erhalten
    public class SensorTracker implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float X = event.values[0];
            float Y = event.values[1];
            float Z = event.values[2];
            ...
    \end{lstlisting}
\end{frame}

\begin{frame}{Beschleunigungssensor}
	\begin{itemize}	\setlength{\itemsep}{5pt}
		\item Mobilitätsmoduserkennung nach \cite{6}:
		\begin{itemize} \setlength{\itemsep}{5pt}
		    \item X-, Y- und Z-Achsenwerte sind je nach Ausrichtung des Gerätes unterschiedlich
		    \item Nutze Betrag des Vektors \\
		    \[ | \vec{V} | = \sqrt{X^2 + Y^2 + Z^2} \]
		    \item Festgelegte Aufzeichnungsfrequenz: z.B. 1-10Hz
		    \item Features wie Durchschnitt, Varianz und DFT (Diskrete Fourier Transformation) Koeffizienten\\ 
		    können berechnet werden \\ \bigskip
		    \begin{tabular}{|c|c|} \hline
		        Feature & Information Gain Score \\ \hline
		        GPS Speed & 1.431 \\ \hline
                Accelerometer Variance & 1.426 \\ \hline
                Accelerometer DFT (3 Hz) & 1.205 \\ \hline
                Accelerometer DFT (2 Hz) & 1.125 \\ \hline
                Accelerometer DFT (1 Hz) & 0.915 \\ \hline
		    \end{tabular}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Beschleunigungssensor}
    \begin{itemize}
        \item 10\%-20\% höhere Genauigkeit, wenn GPS- + Beschleunigungssensordaten genutzt werden anstatt nur eins von beiden
    \end{itemize}
    \begin{columns}[c] % the "c" option specifies center vertical alignment
        \begin{column}[c]{0.4\textwidth} % each column can also be its own environment
            \begin{figure}[hbtp]
                \includegraphics[scale=0.075]{beschleunigung1.png}
                \caption{Geschwindigkeitsverteilung 30min \cite{6}}
            \end{figure}
        \end{column}
        \begin{column}[c]{0.6\textwidth} % each column can also be its own environment
            \begin{figure}[hbtp]
                \includegraphics[scale=0.07]{beschleunigung2.png}
                \caption{Beschleunigungsverlauf \cite{6}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Beschleunigungssensor}
    \begin{itemize}
        \item Unsere Implementierung:
        \begin{itemize}
            \item Betrag bei jeder Änderung (ab bestimmten Delta, da sehr empfindlich) aufsummieren
            \item Bei Aufnahme eines GPS Punktes wird Summe mit gespeichert und zurückgesetzt
            \item Noch offenes Problem: Größe der Summe abhängig von Ortsänderung \\
            \begin{figure}[hbtp]
                \includegraphics[scale=0.3]{csv_beschleunigung.png}
            \end{figure}
            \item Feature: Durchschnittlicher Betrag implementiert
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Zusammenfassung}
\begin{frame}{Zusammenfassung}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[c]{0.5\textwidth} % each column can also be its own environment
            \begin{itemize}
                \item Android App: \medskip
                \begin{itemize}
                \setlength{\itemsep}{10pt}
                    \item Unterstützung weiterer Features (15 + Wetter + Beschleunigungssensor)
                    \item Random Forest als neuer Klassifikator
                    \item Generischer Feature-Mechanismus
                    \item Internationalisierung: Strings auch auf Deutsch
                    \item Menü geändert + Verwendung von externem Speicher möglich
                \end{itemize}
            \end{itemize}
  	    \end{column}
  			
    \begin{column}[c]{0.5\textwidth} % each column can also be its own environment		
            \begin{figure}[hbtp]
            \includegraphics[scale=0.1]{app_screenshot1.png}
            \caption{Android App}
            \end{figure}		
        \end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Zusammenfassung}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[c]{0.5\textwidth} % each column can also be its own environment
            \begin{itemize}
                \item Android App: \medskip
                \begin{itemize}
                \setlength{\itemsep}{10pt}
                    \item Unterstützung weiterer Features (15 + Wetter + Beschleunigungssensor)
                    \item Random Forest als neuer Klassifikator
                    \item Generischer Feature-Mechanismus
                    \item Internationalisierung: Strings auch auf Deutsch
                    \item Menü geändert + Verwendung von externem Speicher möglich
                \end{itemize}
            \end{itemize}
  	    \end{column}
  			
    \begin{column}[c]{0.5\textwidth} % each column can also be its own environment		
            \begin{figure}[hbtp]
            \includegraphics[scale=0.1]{app_screenshot2.png}
            \caption{Android App}
            \end{figure}		
        \end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Zusammenfassung}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[c]{0.5\textwidth} % each column can also be its own environment
            \begin{itemize}
                \item Android App: \medskip
                \begin{itemize}
                \setlength{\itemsep}{10pt}
                    \item Unterstützung weiterer Features (15 + Wetter + Beschleunigungssensor)
                    \item Random Forest als neuer Klassifikator
                    \item Generischer Feature-Mechanismus
                    \item Internationalisierung: Strings auch auf Deutsch
                    \item Menü geändert + Verwendung von externem Speicher möglich
                \end{itemize}
            \end{itemize}
  	    \end{column}
  			
    \begin{column}[c]{0.5\textwidth} % each column can also be its own environment		
            \begin{figure}[hbtp]
            \includegraphics[scale=0.15]{app_screenshot5.png}
            \caption{Android App}
            \end{figure}		
        \end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Zusammenfassung}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[c]{0.5\textwidth} % each column can also be its own environment
            \begin{itemize}
                \item Sommersemester 2013
            \end{itemize}
            \begin{figure}[hbtp]
            \includegraphics[scale=0.3]{app_screenshot4.png}
            \end{figure}	
  	    \end{column}
  			
    \begin{column}[c]{0.5\textwidth} % each column can also be its own environment		
            \begin{itemize}
                \item Sommersemester 2014
            \end{itemize}
            \begin{figure}[hbtp]
            \includegraphics[scale=0.13]{app_screenshot3.png}
            \end{figure}		
        \end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Zusammenfassung}
	\begin{columns}[c] % the "c" option specifies center vertical alignment
		\begin{column}[c]{0.5\textwidth} % each column can also be its own environment
        \begin{itemize}
            \setlength{\itemsep}{20pt}
            \item agg2graph:
            \begin{itemize} \setlength{\itemsep}{5pt}
                \item Implementierung der neuen Features für Wetter und Beschleunigungssensor
                \item Random Forest: Konvertierung der Weka Ausgabe nach digraph Format
             \end{itemize}
             \item Grails Server:
             \begin{itemize} \setlength{\itemsep}{5pt}
                \item Nutzung Random Forest + aller Features der App
                \item neue Kommunikationsendpunkte (JSON + digraph)
            \end{itemize}
        \end{itemize}
  	 \end{column}
  	
  	    \begin{column}[c]{0.5\textwidth} % each column can also be its own environment		
            \begin{figure}[hbtp]
            \includegraphics[scale=0.32]{konvertierung.png}
            \caption{Konvertierung}
            \end{figure}		
  	    \end{column}
  	\end{columns}
\end{frame}

\begin{frame}{Zusammenfassung}
  	\begin{itemize}
  	\setlength{\itemsep}{20pt}
    	\item Bugfixes:
    	\begin{columns}[c]
		    \begin{column}{0.5\textwidth} % each column can also be its own environment
            	\begin{itemize} \setlength{\itemsep}{5pt}
              		\item Schreiben von CSV Dateien auf Server
              		\item Parsen der CSV Dateien
              		\item Nullpointerexceptions
		        \end{itemize}
		    \end{column}
		    \begin{column}{0.4\textwidth} % each column can also be its own environment
                \begin{figure}[hbtp]
				    \includegraphics[scale=0.32]{nullpointerexception.png}
			    \end{figure}
		  	\end{column}
  	    \end{columns}
    	\item andere Verbesserungen / Erweiterungen:
    	\begin{itemize} \setlength{\itemsep}{5pt}
      		\item Testfälle für Random Forest nach digraph
      		\item Senden von CSV Dateien an den Server auch über ausführbare Java Klasse
      		\item Code aufgeräumt + Kommentare
      		\item ...
    	\end{itemize}
  	\end{itemize}
\end{frame}

\section{Evaluation}
\begin{frame}{Evaluation}
    	\begin{columns}[c]
		    \begin{column}{0.5\textwidth} % each column can also be its own environment
	            \begin{itemize} \setlength{\itemsep}{10pt}
              		\item Viele Features schon implementiert aber nicht genutzt
              		\item Neuer Klassifikator mit höherer Erkennungsrate \cite{5}
              		\item Einarbeitungsphase mühselig
              		\item CSV Format evtl. nicht optimal \\ $\rightarrow$ Datenbanknutzung?
	            \end{itemize}
			    \end{column}
		    \begin{column}{0.4\textwidth} % each column can also be its own environment
                \begin{figure}[hbtp]
				    \includegraphics[scale=0.32]{Evaluation.png}
				    \caption{Erkennungsraten nach \cite{5}}
			    \end{figure}
		  	\end{column}
  	    \end{columns}
\end{frame}

\section{Demo}
\begin{frame}{Live Demo}
    \begin{figure}[hbtp]
        \includegraphics[scale=0.35]{sbahn_track.png}
        \caption{Beispiel Aufzeichnung}
    \end{figure}	
\end{frame}

\begin{frame}{Live Demo}
  ösdlfsödfj
  
  
  topalov
  
  
  
  	\begin{itemize}
	 	\item \href{http://projects.mi.fu-berlin.de/gpseval/decisionTree/getLastTree }{getLastTree}
	 	\item \href{http://projects.mi.fu-berlin.de/gpseval/forest/getLastForestTrees }{getLastRandomForest}
	  	\item \href{http://sandbox.kidstrythisathome.com/erdos/}{Display Tree}
	  	\item \href{http://www.freeformatter.com/java-dotnet-escape.html}{Formatter}
	\end{itemize}
\end{frame}

\section{Future Work}
\begin{frame}{Future Work}
	\begin{itemize} \setlength{\itemsep}{20pt}
		\item Android App
		\begin{itemize} \setlength{\itemsep}{5pt}
			\item Refactoring, um den offiziellen Android Guidelines zu entsprechen
			\begin{itemize}
				\item Content Provider
				\item Sync Adapter
			\end{itemize}						
			\item neue Features
			\begin{itemize}
				\item Wetter Updates bei langen Aufzeichnungen oder gro\ss en Distanzen
				\item Weitere Wetter Kenndaten einbeziehen
				\item Wetter in Abh\"angigkeit von zur\"uckgelegter Entfernung aktualisieren
				\item Beschleunigungssensor: Varianz und DFT
				\item $\ldots$
			\end{itemize}
			\item Klassifikator Statistiken
			\item Beschleunigung des App-Starts
		\end{itemize}

		\item Server
		\begin{itemize} 
			\item Visualisierung der aufgenommenen Tracks
			\item Administrationsoberfl\"ache
		\end{itemize}
	\end{itemize}
\end{frame}

\section*{Quellennachweis}
\begin{frame}{Quellen}
\bibliographystyle{splncs}
	\begin{thebibliography}{1}
		\bibitem[1]{1}
		Autor: Gerard Biau,
		Titel: Analysis of a Random Forests Model, 
		Journal of Machine Learning Research 13 (2012) 1063-1095, 
		\href{http://jmlr.org/papers/volume13/biau12a/biau12a.pdf}{link}

		\bibitem[2]{2}
		openweathermap.org
		
		\bibitem[3]{3}
		Autor: Sebastian Barthel et al
		Titel: Dynamische Mobilitätserkennung
		Projektseminar Datenverwaltung Sommersemester 2013, Abschlussdokumentation

		\bibitem[4]{4}
		Autor: Yu Zheng et al
		Titel: Understanding Mobility Based on GPS Data,
		UbiComp'08
		
		\bibitem[5]{5}
		Autor: Serdar Tosun
		Titel: Erstellung verkehrsmittelspezifischer Navigationsgraphen mit Hilfe von GPS-Spuren,
		FU-Berlin Sommersemester 2013, Diplomarbeit
		
		\bibitem[6]{6}
		Autor: Sasank Reddy, Min Mun, Jeff Burke, Deborah Estrin, Mark Hansen, Mani Srivastava
		Titel: Using Mobile Phones to Determine Transportation Modes,
		ACM Transactions on Sensor Networks, Vol. 6, No. 2, pp. 1-27, 2010
		
		\bibitem[6]{6}
		Karten erstellt mit Google Maps: maps.google.com,
		Android Logo: android.com,
		Grails Logo: grails.org
	\end{thebibliography}
\end{frame}

\end{document}
