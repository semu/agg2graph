package gpseval.server

import static org.junit.Assert.*
import grails.converters.JSON
import org.junit.*

class GPSCaptureControllerTests {
	@Test
	void testSetCapture() {
		// send tracks
		def gpsCaptureController = new GPSCaptureController()
		def capture
		for (int i = 0; i < 400; i++) {
			capture += "abc\n"
		}
		gpsCaptureController.params.capture = capture
		gpsCaptureController.setCapture()
		
		// capture received
		assert GPSCapture.count() == 1
	}
	
	@Test
	void testSetCaptureWithoutParams() {
		// send tracks
		def gpsCaptureController = new GPSCaptureController()
		gpsCaptureController.setCapture()
		
		// capture received
		assert GPSCapture.count() == 0
	}
	
	@Test
	void testGetCapture() {
		// get tracks
		def gpsCaptureController = new GPSCaptureController()
		gpsCaptureController.getCaptures()
		
		// no captures received
		assert gpsCaptureController.response.json.toString() == "[]"
	}
}