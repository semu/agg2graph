package gpseval.server

import static org.junit.Assert.*
import org.junit.*

class DecisionTreeControllerTests {
	@Test
	void testSetTree() {
		// no tree in database
		DecisionTree.count() == 0
		
		// send tree
		def decisionTreeController = new DecisionTreeController()
		decisionTreeController.params.tree = "TREE"
		decisionTreeController.setTree()
		
		// tree received
		DecisionTree.count() == 1
	}
	
	@Test
	void testSetTreeWithoutParams() {
		// no tree in database
		DecisionTree.count() == 0
		
		// send tree
		def decisionTreeController = new DecisionTreeController()
		decisionTreeController.setTree()
		
		// no tree received
		DecisionTree.count() == 0
	}
	
	@Test
	void testGetLastTree() {
		// call controller action
		def decisionTreeController = new DecisionTreeController()
		decisionTreeController.getLastTree()
		
		// getting json from action
		def result = decisionTreeController.response.json
		
		// no gps tracks sent, so no trees available
		assert result.toString() == "{\"error\":\"No tree available.\"}"
		
		// send tree
		decisionTreeController = new DecisionTreeController()
		decisionTreeController.params.tree = "TREE"
		decisionTreeController.setTree()
		
		// call controller action
		decisionTreeController = new DecisionTreeController()
		decisionTreeController.getLastTree()
		result = decisionTreeController.response.json
		assert result.toString() == "{\"error\":\"No tree available.\"}"
	}
	
	@Test
	void testGetTreesWithoutTrees() {
		// call controller action
		def decisionTreeController = new DecisionTreeController()
		decisionTreeController.getTrees()
		
		// no gps tracks sent, so no trees available
		assert decisionTreeController.response.json.toString() == "[]"
	}
}