package gpseval.server

import grails.converters.JSON
import groovy.json.JsonOutput

/**
 * Controller for setting and getting GPS captures in JSON format.
 * 
 * @author Ferhat Beyaz
 * @author Damla Durmaz
 * @author Sebastian Barthel
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class GPSCaptureController {
	def ClassifierService
	def CsvService

	/**
	 * Saves a GPS capture in the database.
	 *
	 * @param GPS capture in CSV format
	 * @return Saved GPS capture entry from database or an error message (if send GPS capture is empty) as JSON
	 */
	def setCapture() {

		def captureString = params.capture

		if (!params.capture) {
			response.status = 500
			render ([error: "No CSV included."] as JSON)
			return
		}

		if (!captureString) {
			response.status = 500
			render ([error: "No CSV included."] as JSON)
			return
		}

		if (captureString.isEmpty()) {
			response.status = 500
			render ([error: "CSV should not be empty."] as JSON)
			return
		}

		// client sometimes sends empty files if send csv event is called more then once
		// therefore check, whether file has more then 283 character
		if (captureString.size() > 283) {
			println "[SET CAPTURE]"
			def capture = new GPSCapture()
			capture.capture = captureString
			capture.save()

			// update classifier
			ClassifierService.createClassifier()

			response.status = 200
			render capture as JSON
		} else {
			response.status = 200
			render ([error: "CSV has no GPS tracks"] as JSON)
		}
	}
	
	def index() {
		
	}

	def tmpfiles() {
		def list = []
		//		list << new Person(firstName: 'John', lastName:'Doe', age:50)
		//		list << new Person(firstName: 'Jane', lastName:'Smith', age:45)
		//		list << new Person(firstName: 'Sam', lastName:'Robinson', age:47)
		def tmpfilesDir = new File(CsvService.getCSVPath())

		tmpfilesDir.eachFile {
			if (it.directory) {
				it.eachFile { list += it.path }
			}
		}

		println "NUMBER OF TMPFILES: "+list.size()

		session.setAttribute("list", list)
		[ list:list ]
	}

	def deleteFiles() {
		def idx
		params.each { key, value ->
			if (key.startsWith('delete_')) {
				idx = Integer.parseInt(key-"delete_")
			} else if(key.equals('deleteAll')) {
				idx = -1
			}
		}
		def list = session.getAttribute("list")
		if (idx >= 0 && list.size() > idx) {
			// delete element
			if(new File(list.get(idx)).delete()) {
				println "FILE "+idx+" DELETED: "+list.get(idx)
			} else {
				println "ERROR WHILE DELETING FILE "+idx+": "+list.get(idx)
			}
		} else if(idx == -1 && list.size() > 0) {
			def boolSuccess = true
			// delete all elements
			list.each {
				if(!new File(it).delete()) {
					boolSuccess = false
				}
			}
			if(boolSuccess) {
				println "ALL FILES DELETED"
			} else {
				println "ERROR WHILE DELETING ALL FILES"
			}
		}
		redirect(action: "tmpfiles")
	}
}