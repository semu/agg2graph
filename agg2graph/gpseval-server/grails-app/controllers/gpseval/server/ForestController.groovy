package gpseval.server

import grails.converters.JSON


/**
 * Controller for getting random forest rendered as string.
 * 
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class ForestController {

	/**
	 * Shows a simple menu
	 * @return
	 */
	def index() {
		def html = "<h1>Receive Forests</h1>"
		html += "As JSON:<br /><a href=\"getForests\">getForests</a><br />"
		html += "Last Forest as simple String:<br /><a href=\"getLastForestTrees\">getLastForestTrees</a><br />"
		html += "<a href=\"../\"><-- Back</a><br />"
		render html
	}

	/**
	 * Render all created random forests as JSON
	 * @return
	 */
	def getForests() {
		println "[GET FORESTS]"
		render Forest.list() as JSON
	}
	
	/**
	 * Render the last created random forest as plain string
	 * @return
	 */
	def getLastForestTrees() {
		println "[GET LAST FOREST Trees]"
		def forest = Forest.last()
		if (forest != null) {
			def trees = forest.getTrees()
			// println trees
			render trees
		} else {
		    render "[]"
		}
	}
}
