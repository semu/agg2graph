package gpseval.server

import grails.converters.JSON
import de.fub.agg2graph.gpseval.evaluator.GPSEvaluator
import org.codehaus.groovy.grails.commons.ApplicationHolder
import groovy.xml.StreamingMarkupBuilder
import groovy.json.JsonOutput
import de.fub.agg2graph.pt.StopTree

/**
 * Controller for setting and getting decision trees in JSON format.
 *
 * @author Ferhat Beyaz
 * @author Damla Durmaz
 * @author Sebastian Barthel
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class DecisionTreeController {
	
	/**
	 * Gets last calculated tree. Trees are saved by {@link CreateTreeJob}.
	 *
	 * @return Decision tree or error message (if still no tree is calculated) as JSON
	 */
	def getLastTree() {
		println "[GET LAST TREE]"
		if (DecisionTree.last()) {
			render DecisionTree.last() as JSON
		} else {
			response.status = 500
			render ([error: "No tree available."] as JSON)
		}
	}
	
	/**
	 * Gets all calculated trees saved in the database. Trees are saved by {@link CreateTreeJob}.
	 *
	 * @return Decision trees as JSON
	 */
	def getTrees() {
		println "[GET TREES]"
		render DecisionTree.list() as JSON
	}
	
}