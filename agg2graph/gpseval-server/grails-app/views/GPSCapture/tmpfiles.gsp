<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<title>GPSCapture Controller</title>
</head>
<body>
	<div class="body">
		<h1>Current CSV Files on server</h1>
		<g:form name="fileForm" action="deleteFiles">
			<g:submitButton name="deleteAll" value="Delete All" ></g:submitButton>
			<ul style="width:90%;margin:1em auto auto auto;">
			<g:each in="${list}" var="file" status="i">
				
				<li style="margin-bottom:1em;">${file}<br /><g:submitButton name="delete_${i}" value="Delete" ></g:submitButton></li>
			</g:each>
			</ul>
		</g:form>
	</div>
</body>
</html>