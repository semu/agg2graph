package gpseval.server

import org.codehaus.groovy.grails.commons.ConfigurationHolder as expr
import grails.converters.JSON
import groovy.json.JsonOutput

/**
 * Quartz job for calculating decision trees from given GPS data.
 *
 * @see <a href="http://grails.org/plugin/quartz2">Quartz Job Reference</a>
 * @author Ferhat Beyaz
 * @author Damla Durmaz
 * @author Sebastian Barthel
 * @version 03.07.2013
 */
class CreateClassifierJob {
	
	/**
	 * Injection of the service, that contains the logic for calculating classifiers.
	 */
	def ClassifierService
	
	/**
	 * Calculation interval is a simple repeatInterval given in config.groovy.
	 */
	static triggers = {
		simple repeatInterval: expr.config.grails.scheduler.repeatInterval
	}
	
	
	/**
	 * Execution procedure of Quartz Job. Simply calls {@link ClassifierService.createClassifier()}.
	 * 
	 * @return void
	 */
	def execute() {
		println "[CALCULATING CLASSIFIERS " + new Date() + "]"
		
		// calculate new classifiers
		ClassifierService.createClassifier()
	}
}