package gpseval.server

import org.codehaus.groovy.grails.commons.ApplicationHolder

/**
 * Service with helper methods for working with CSV files send from client application.
 *
 * @author Ferhat Beyaz
 * @author Damla Durmaz
 * @author Sebastian Barthel
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class CsvService {
	
	// path for csv files on local server
	//def CSV_path = ApplicationHolder.application.parentContext.getResource("tmpfiles").file.absolutePath
	
	// path for csv files on tomcat (because of write permissions)
	def CSV_path = "/import/tomcat7/gpseval-tmpfiles"
	
	def getCSVPath() {
		return CSV_path;
	}
	
	/**
	 * Creates a CSV file with a given description and content at the given place (category/filename).
	 *
	 * @param category Modality mode, for example "Car", "Train", etc.
	 * @param filename Filename of the CSV track.
	 * @param description Every CSV file has a description in the first line.
	 * @param content Lines in the CSV document
	 * @return void
	 */
	def create(String category, String filename, String description, ArrayList content) {
		def path = CSV_path + "/" + category
		
		new File(path).mkdir()
		
		def file = new File(path + "/" + filename + ".csv")
		println file.absoluteFile
		println path
		println "FILE " +  file
		if (file.createNewFile()) {
			println file
			file.write(description + "\n")
			content.each { line ->
				file.append(line + "\n")
			}
		}
	}
	
	/**
	 * Convert android timestamp to groovy timestamp
	 *
	 * @param timestamp Android based timestamp
	 * @return Groovy Date Object
	 */
	def getDate(String timestamp) {
		String date = timestamp.split("T")[0]
		String time = timestamp.split("Z")[0].split("T")[1].tokenize(".")[0]
		return new Date().parse("yyyy-MM-dd hh-mm-ss", date + " " + time)
	}
}