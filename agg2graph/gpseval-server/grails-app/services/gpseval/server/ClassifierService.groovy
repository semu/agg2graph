package gpseval.server

import de.fub.agg2graph.gpseval.evaluator.GPSEvaluator
import org.codehaus.groovy.grails.commons.ApplicationHolder
import groovy.xml.StreamingMarkupBuilder
import grails.converters.JSON
import groovy.json.JsonOutput
import de.fub.agg2graph.pt.StopTree

/**
 * Service for creating files (preparation) and using Weka (execution).
 *
 * @author Ferhat Beyaz
 * @author Damla Durmaz
 * @author Sebastian Barthel
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class ClassifierService {
	
	/**
	 * Injection of the service, that contains the helper methods for working with CSV.
	 */
	def CsvService
	
	/**
	 * Calls {@link createFiles} and {@link useWeka}.
	 * 
	 * @return void
	 */
	def createClassifier() {
		println "[CALL createClassifier():" + new Date() + "]"
		createFiles()
		useWeka()
	}
	
	/**
	 * Prepares the process for classifier creation by configuring weka config file and
	 * creating all necessary files from GPS captures from database.
	 * 
	 * For each capture in the database, the algorithmm looks for parts. Those parts are
	 * given as comments in the CSV. Each part in such a GPS capture can have its own
	 * modality mode, so it gets its own CSV file in a temporary directory. The Weka config
	 * file gets those paths.
	 *
	 * @return void
	 */
	private def createFiles() {
		println "[CALL createFiles():" + new Date() + "]"
		
		// prepare folders
		def directory = new File(CsvService.getCSVPath()).canonicalPath
		new File(directory).mkdir()
		
		// for each capture in database, write csv files
		def captures = GPSCapture.list()
		captures.each { capture ->
			def lines = capture.capture.readLines()
			def description = lines[0]
			def content = lines[1..(lines.size()-1)]
			
			def part = []		// contains all lines from one category to next category
			def result = []		// contains all lines from part, which are in the needed timespan
			content.each { line ->
				if (line.contains("#")) {
					def annotation = line.subSequence((line.findIndexOf { it == "#" } + 1), line.size()).replaceAll("\\s", "")
					def category = annotation.split(";")[0]
					
					// get timestamp from part
					def partDate = CsvService.getDate(annotation.split(";")[1])
					
					def filename = partDate.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
					
					// put this line to part, too
					// part += line
					
					// only use elements that are 150 seconds earlier then timestamp
					// TODO: only use part elements or use elements 150 seconds ago, even if other parts?
					part.each { entry ->
//						def timestamp = entry.split(",")[8].substring(1, (entry.split(",")[8]).size() - 1)
//						def entryDate = CsvService.getDate(timestamp)
//						
//						def startDate
//						use (groovy.time.TimeCategory) {
//							startDate = partDate - 150.seconds
//						}
//						
//						// if capture is less then 30 minutes before capture end
//						if (entryDate >= startDate) {
//							result += entry
//						}
						
						result += entry
					}
					
					CsvService.create(category, filename, description, result)
					part = []
					result = []
				} else {
					part += line
				}
			}
		}
	}
	
	
	/**
	 * Uses Weka to calculate classifiers from the given CSV files created by {@link createFiles}.
	 * The calculated classifiers finally are saved in the database.
	 *
	 * @return void
	 */
	private def useWeka() {
		// calculating, if there are enough classes
		println "[CALCULATING NUMBER OF CLASSES " + new Date() + "]"
		def nrClasses = 0
		
		// def tmpfilesDir = new File(ApplicationHolder.application.parentContext.getResource("tmpfiles").file.absolutePath)
		def tmpfilesDir = new File(CsvService.getCSVPath())
		
		try {
			tmpfilesDir.eachFile {
				if (it.directory) {
					nrClasses++
				}
			}
		} catch (FileNotFoundException e) {
			nrClasses = 0
		}
		
		if (nrClasses < 2) {
			println "[NOT ENOUGH CLASSES TO BUILD DECISION TREE " + new Date() + "]"
			return
		}
		

		def configContent = new File(ApplicationHolder.application.parentContext.getResource("default_template.xml").file.absolutePath).text
		def root = new XmlSlurper().parseText(configContent)
		
		// cannot put class element, because of compilation problems
		new File(CsvService.getCSVPath()).eachFile {
			if (it.directory) {
				def featureName = it.name.capitalize()
				def featurePath = it.toString()
				
				root.classes.appendNode {
					abcdef {
						name(featureName)
						dataFolder(featurePath)
					}
				}
			}
		}
		
		def outputBuilder = new StreamingMarkupBuilder()
		String result = outputBuilder.bind{ mkp.yield root }
		
		// replace all "abcdef" with "class"
		result = (result =~ /abcdef/).replaceAll("class")
		
		// write new config file
		new File(ApplicationHolder.application.parentContext.getResource("config").file.absolutePath + "/default.xml").delete()
		new File(ApplicationHolder.application.parentContext.getResource("config").file.absolutePath + "/default.xml").write(result)
		
		// NOTE: change StopTree.java init method if using BVG.zip
		def pathToBvgZip = ApplicationHolder.application.parentContext.getResource("BVG.zip").file.absolutePath
		StopTree.setPath(pathToBvgZip)
		//println "[BVG PATH: "+pathToBvgZip+"]"
		
		// use WEKA to get decision tree
		GPSEvaluator app = new GPSEvaluator()
		String[] arg = new String[1]
		arg[0] = ApplicationHolder.application.parentContext.getResource("config").file.absolutePath
		app.start(arg)
		
		// save decision tree
		def tree = new DecisionTree()
		tree.tree = app.getTrees().getAt(0)
		tree.save()
		
		// save random forest
		def forest = new Forest()
		forest.trees = app.getForests().get(0).join(",")
		// println "[FORESTS: "+forest.trees+"]"
		forest.save()
	}
}