package gpseval.server

/**
 * Domain Model for decision trees. Decision trees are saved as strings in the J48-Format.
 * This format is given by the J48-Algorithm from WEKA. 
 * Every database entry also has a creation timestamp.
 *
 * @author Ferhat Beyaz
 * @author Damla Durmaz
 * @author Sebastian Barthel
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class DecisionTree {
	
	/**
	 * Trees are saved as Strings in the J48 format.
	 */
	String tree
	
	/**
	 * Every time a tree is saved in the database, the creation time is saved, too.
	 */
	Date dateCreated
	
	/**
	 * Constraints for trees in the database.
	 * A tree should not be blank and currently has a maxSize of 5000 characters.
	 */
	static constraints = {
		tree blank: false, maxSize: 5000
	}
}
