package gpseval.server

/**
 * Domain Model for GPS captures. GPS captures are saved as Strings, that contain the CSV data.
 * Every database entry also has a creation timestamp.
 *
 * @author Ferhat Beyaz
 * @author Damla Durmaz
 * @author Sebastian Barthel
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class GPSCapture {
	
	/**
	 * CSV data is saved as a String in the database.
	 */
	String capture
	
	
	/**
	 * Every time a GPS capture is saved in the database, the creation time is saved, too.
	 */
	Date dateCreated
	
	/**
	 * Constraints for GPS captures in the database.
	 * A GPS capture should not be blank and currently has a maxSize of 100000000 characters.
	 */
	static constraints = {
		capture blank: false, maxSize: 100000000
	}
}
