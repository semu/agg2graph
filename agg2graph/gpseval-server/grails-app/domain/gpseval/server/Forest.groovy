package gpseval.server

import java.util.Date;

/**
 * Domain Model for random forests which are saved as string with trees 
 * in J48-Format.
 * Every database entry also has a creation timestamp.
 * 
 * @author Martin Liesenberg
 * @author Daniel Neumann
 * @version 20.09.2014
 */
class Forest {

	/**
	 * Trees are saved in a list of J48 format trees as String.
	 */
	String trees
	
	/**
	 * Every time a tree is saved in the database, the 
	 * creation time is saved, too.
	 */
	Date dateCreated
	
	/**
	 * Constraints for random forest in the database.
	 * A random forest should not be blank and currently 
	 * has a maxSize of 10000 characters.
	 */
	static constraints = {
		trees blank: false, maxSize: 10000
	}
}
