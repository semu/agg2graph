package de.fub.agg2graph.evaluator.app;

import de.fub.agg2graph.evaluator.app.activity.MainActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.NotificationCompat;

/**
 * Send notifications to android in an easy way
 *
 */
public class EasyNotification {

    private static final int NOTIFICATION_ID = 0;

    private final Context context;
    private final NotificationManager notificationManager;

    private Resources resources;
	
	/**
	 * 
	 * @param context main application context
	 */
	public EasyNotification(Context context) {
		this.context = context;
        this.notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        this.resources = context.getResources();
	}
	
	/**
	 * Send a notification to android os
	 * @param status new transportation mode
	 * @param status_localized status in app-specific language
	 * @param icon to display
	 */
	public void send(String status, String status_localized, int icon) {

		// Prepare intent which is triggered when the notification is selected
		Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("fromNotification", true);
        intent.putExtra("status", status);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        String titleNormal = resources.getString(R.string.notificationTitleNormal) + ": " + status_localized;
        
        String text = resources.getString(R.string.notificationMessageNormal);

        String titleBig = resources.getString(R.string.notificationTitleBig) + ": " + status_localized;

		// Build notification, actions are just fake for the moment
		Notification notification = new NotificationCompat.Builder(context)
		        .setContentTitle(titleNormal)
		        .setContentText(text)
		        .setSmallIcon(icon)
                .setAutoCancel(true)
		        .setContentIntent(pIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(titleBig)
                        .bigText(text))
                .addAction(0, resources.getString(R.string.notificationStopTracking), pIntent)
                .addAction(0, resources.getString(R.string.notificationRecompute), pIntent)
                .build();

		notificationManager.notify(NOTIFICATION_ID, notification);
	}

    public void update(String message) {
        Notification notification = new NotificationCompat.Builder(context)
                .setContentText(message)
                .build();

        notificationManager.notify(NOTIFICATION_ID, notification);

    }
}
