package de.fub.agg2graph.evaluator.evaluator.features;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.PriorityQueue;

import de.fub.agg2graph.evaluator.app.GPSTracker;
import de.fub.agg2graph.evaluator.evaluator.BvgStops;

/**
 * Class to compute the features used in the classifiers singleton by type enum.
 */
public enum FeatureCalculator {

	FEATURE_CALCULATOR;

	private static final String TAG = FeatureCalculator.class.getSimpleName();
	private static final int TIME_COLUMN = FeatureManager.TIME_COLUMN;

	private final DateFormat dateFormat;
	private final Calendar calendar;

	private FeatureCalculator() {
		//dateFormat = new SimpleDateFormat("y-MM-d'T'k-m-s.S'Z'", Locale.GERMANY);
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		calendar = Calendar.getInstance();
	}

	/**
	 * Calculate the average value of the column of the last secondsIntoPast
	 * seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param column
	 *            of csv
	 * @return average value of column as {@link double}
	 */
	public double getAverageFeature(int secondsIntoPast, int column) {

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		Double result = Double.MIN_VALUE;
		int counter = 0;

		for (String[] l : lines) {

			if (result != Double.MIN_VALUE) {
				result += Double
						.parseDouble(l[column].replace("\"", "").trim());
			} else {
				result = Double.parseDouble(l[column].replace("\"", "").trim());
			}

			counter++;
		}

		return result != Double.MIN_VALUE ? result / counter : 0;
	}

	/**
	 * Calculate the max value of the column of the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param column
	 *            of csv
	 * @return max value of column as {@link double}
	 */
	public double getMaxFeature(int secondsIntoPast, int column) {

		Double result = Double.MIN_VALUE;

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		for (String[] l : lines) {

			String parseString = l[column].replace("\"", "").trim();

			double parseValue = 0.0;

			if (!parseString.equals("Infinit")) {
				parseValue = Double.parseDouble(parseString);
			}

			if (parseValue > result) {
				result = parseValue;
			}

		}

		return result != Double.MIN_VALUE ? result : 0;
	}

	/**
	 * Calculate the n-th max value of the column of the last secondsIntoPast
	 * seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param column
	 *            of csv
	 * @param n
	 *            get the n-th max
	 * @return n-th max value of column as {@link double}
	 */
	public double getMaxNFeature(int secondsIntoPast, int column, int n) {

		if (n < 0) {
			Log.e(TAG, "getMaxNFeature: n must be a value greater than 0");
			return 0.0;
		}

		List<String[]> lines = prepareCSVString(secondsIntoPast);
		PriorityQueue<Double> values = new PriorityQueue<Double>(n);

		for (String[] l : lines) {

			double value = Double.parseDouble(l[column].replace("\"", "")
					.trim());

			values.add(value);

			if (values.size() > n) {
				values.poll();
			}
		}

		return values.size() > 0 ? values.poll() : 0;
	}

	/**
	 * Calculate the average transportation distance of the last secondsIntoPast
	 * seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param latitudeColumn
	 *            index of latitude
	 * @param longitudeColumn
	 *            index of longitude
	 * @return average distance as {@link double}
	 */
	public double getAvgTransportationDistance(int secondsIntoPast,
			int longitudeColumn, int latitudeColumn) {

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		double distance = 0.0;
		int counter = 0;

		for (String[] l : lines) {
			double lon = Double.parseDouble(BvgStops
					.clearQuotesOnDouble(l[longitudeColumn]));
			double lat = Double.parseDouble(BvgStops
					.clearQuotesOnDouble(l[latitudeColumn]));
			double shortestDistance = BvgStops.getNearestStopDistance(lat, lon);

			distance += shortestDistance;
			counter++;
		}

		return counter > 0 ? distance / counter : 0;
	}

	/**
	 * Calculate the number of segments in the past secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param segmentColumn
	 *            index of segment
	 * @return number of segments as int
	 */
	public int getNumberOfSegments(int secondsIntoPast, int segmentColumn) {

		int result = 0;
		List<String[]> lines = prepareCSVString(secondsIntoPast);

		for (String[] l : lines) {
			result += Integer.parseInt(l[segmentColumn].replace("\"", "")
					.trim());
		}

		return result;
	}

	/**
	 * Calculate the average change of bearing over the last secondsIntoPast
	 * seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param bearingChangeThreshold
	 *            threshold to ignore values
	 * @param bearingColumn
	 *            index of bearing
	 * @return average change in bearing over the last secondsIntoPast seconds
	 */
	public double getAvgBearingChange(int secondsIntoPast,
			double bearingChangeThreshold, int bearingColumn) {

		int count = 0;
		double sumBearingChange = 0.0;
		double lastBearing = 0.0;
		double currentBearing;

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		for (String[] l : lines) {

			currentBearing = Double.parseDouble(l[bearingColumn].replace("\"",
					"").trim());

			double bearMax = Math.max(lastBearing, currentBearing);
			double bearMin = Math.min(lastBearing, currentBearing);

			double bearingChange = bearMax - bearMin;
			bearingChange = bearingChange > 180 ? 360 - bearingChange
					: bearingChange;

			if (bearingChange > bearingChangeThreshold) {
				sumBearingChange += bearingChange;
				count++;
			}

			lastBearing = currentBearing;
		}

		return count > 0 ? sumBearingChange / count : 0;
	}

	/**
	 * calculate the stop rate of the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param stopThreshold
	 *            threshold to consider movement as stopped
	 * @param distanceColumn
	 *            index of distance
	 * @param speedColumn
	 *            index of speed
	 * @return stopRate
	 */
	public double getStopRate(int secondsIntoPast, double stopThreshold,
			int speedColumn, int distanceColumn) {

		int counter = 0;
		double totalSegmentLength = 0.0;

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		for (String[] l : lines) {
			totalSegmentLength += Double.parseDouble(l[distanceColumn].replace(
					"\"", "").trim());

			float currentSpeed = Float.parseFloat(l[speedColumn].replace("\"",
					"").trim());

			if (currentSpeed <= stopThreshold) {
				counter++;
			}
		}

		return totalSegmentLength > 0 ? counter / totalSegmentLength : 0;
	}

	/**
	 * calculate the length of the track in the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param distanceColumn
	 *            index of distance
	 * @return track length
	 */
	public double getTrackLength(int secondsIntoPast, int distanceColumn) {

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		double length = 0.0;

		for (String[] l : lines) {
			length += Double.parseDouble(l[distanceColumn].replace("\"", "")
					.trim());
		}

		return length;
	}

	/**
	 * calculate the average acceleration in the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param speedColumn
	 *            index of speed
	 * @return average acceleration
	 */
	public double getAvgAcceleration(int secondsIntoPast, int speedColumn) {

		int counter = 0;
		double sumAcceleration = 0.0;

		double lastTime = 0.0;
		double lastSpeed = 0.0;

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		for (String[] l : lines) {
			double time = parseTime(l[TIME_COLUMN].replace("\"", "").trim());
			double speed = Double.parseDouble(l[speedColumn].replace("\"", "")
					.trim());

			double timeDiff = Math.max(0, (time - lastTime) / 1000);

			sumAcceleration += calculateAcceleration(speed, lastSpeed, timeDiff);
			counter++;

			lastSpeed = speed;
			lastTime = time;
		}

		return sumAcceleration > 0 ? sumAcceleration / counter : 0;
	}

	/**
	 * calculate the average magnitude of accelerometer values in the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param accelerometerColumn
	 *            index of accelerometer values
	 * @return average acceleration magnitude of all axis normalized by time difference
	 */
	public double getAvgAccelerometerMagnitude(int secondsIntoPast, int accelerometerColumn) {

		int counter = 0;
		double sumAccelerationMagnitude = 0;
		double lastTime = 0.0;

		List<String[]> lines = prepareCSVString(secondsIntoPast);
		Log.d(TAG, "LINES:\n"+lines.size());

		for (String[] l : lines) {
			double time = parseTime(l[TIME_COLUMN].replace("\"", "").trim());
			double timeDiff = Math.max(0, (time - lastTime) / 1000);

			if(timeDiff > 0) {
				double accelerationMagnitude = Double.parseDouble(l[accelerometerColumn].replace("\"", "")
						.trim());
				double normalizedAccelerationMagnitude = accelerationMagnitude / timeDiff;
				sumAccelerationMagnitude += normalizedAccelerationMagnitude;
				counter++;
			}

			lastTime = time;
		}

		return sumAccelerationMagnitude > 0 ? sumAccelerationMagnitude / counter : 0;
	}

	/**
	 * calculate the maximum acceleration in the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param speedColumn
	 *            index of speed
	 * @return max acceleration
	 */
	public double getMaxAcceleration(int secondsIntoPast, int speedColumn) {

		double result = Double.MIN_VALUE;

		double lastTime = 0.0;
		double lastSpeed = 0.0;

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		for (String[] l : lines) {
			double time = parseTime(l[TIME_COLUMN].replace("\"", "").trim());
			double speed = Double.parseDouble(l[speedColumn].replace("\"", "")
					.trim());

			double timeDiff = Math.max(0, (time - lastTime) / 1000);

			double acceleration = calculateAcceleration(speed, lastSpeed,
					timeDiff);

			if (acceleration > result) {
				result = acceleration;
			}

			lastSpeed = speed;
			lastTime = time;
		}

		return result;
	}

	/**
	 * calculate the nth max acceleration in the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param speedColumn
	 *            index of speed
	 * @param n
	 *            get the n-th max
	 * @return nth max acceleration
	 */
	public double getMaxNAcceleration(int secondsIntoPast, int speedColumn,
			int n) {

		if (n < 0) {
			Log.e(TAG, "getMaxNAcceleration: n must be a value greater than 0");
			return 0.0;
		}

		double lastTime = 0;
		double lastSpeed = 0;

		List<String[]> lines = prepareCSVString(secondsIntoPast);
		PriorityQueue<Double> values = new PriorityQueue<Double>(n);

		for (String[] l : lines) {

			double speed = Double.parseDouble(l[speedColumn].replace("\"", "")
					.trim());
			double time = Double.parseDouble(l[TIME_COLUMN].replace("\"", "")
					.trim());

			double timeDiff = Math.max(0, (time - lastTime) / 1000);

			double acceleration = calculateAcceleration(speed, lastSpeed,
					timeDiff);

			values.add(acceleration);

			if (values.size() > n) {
				values.poll();
			}
		}

		return values.size() > 0 ? values.poll() : 0;
	}

	/**
	 * calculate the variance of speed in the last secondsIntoPast seconds
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @param distanceColumn
	 *            index of distance
	 * @return variance of speed
	 */
	public double getVarianceOfSpeed(int secondsIntoPast, int distanceColumn) {

		double totalDistance = 0;
		double timeDiff = 0;

		List<String[]> lines = prepareCSVString(secondsIntoPast);

		if (lines.size() > 1) {
			double startTime = Double.parseDouble(lines.get(0)[TIME_COLUMN]
					.replace("\"", "").trim());
			double lastTime = Double
					.parseDouble(lines.get(lines.size() - 1)[TIME_COLUMN]
							.replace("\"", "").trim());
			timeDiff = lastTime - startTime;
		} else {
			Log.d(TAG,
					"size of value list not greater 1, varianceOfSpeed not useful");
		}

		for (String[] l : lines) {
			totalDistance += Double.parseDouble(l[distanceColumn].replace("\"",
					"").trim());
		}

		return timeDiff > 0 ? totalDistance / timeDiff : 0;
	}

    public double getWeather(int secondsIntoPast, int weatherColumn) {
        List<String[]> lines = prepareCSVString(secondsIntoPast);

        if (lines.size() >= 1) {
            return Double.parseDouble(lines.get(0)[weatherColumn]);
        } else {
            return 0.0;
        }
    }

	private double calculateAcceleration(double currentSpeed, double lastSpeed,
			double timeDiff) {
		return timeDiff > 0 ? (currentSpeed - lastSpeed) / timeDiff : 0;
	}

	/**
	 * helper function to check if timestamp is newer than secondsIntoPast
	 *
	 * @param lineTime
	 *            string encoding time of csv line
	 * @param secondsIntoPast
	 *            point to look back to
	 * @return boolean indicating if lineTime is within secondsIntoPast and now
	 */
	private boolean isInTimeRange(String lineTime, int secondsIntoPast) {

		double currentTime = calendar.getTime().getTime();
		double csvTime = parseTime(lineTime);
		double diff = currentTime - csvTime;

		return diff / 1000 < secondsIntoPast;

	}

	/**
	 * helper method to read current csv string and filter all values not needed
	 *
	 * @param secondsIntoPast
	 *            point to look back to
	 * @return list of records max secondsIntoPast in the past
	 */
	private List<String[]> prepareCSVString(int secondsIntoPast) {

		String csv = GPSTracker.getCurrentCsvString();
		String[] lines = csv.split("\n");

		List<String[]> result = new ArrayList<String[]>();

		for (int i = 1; i < lines.length; i++) {	// skip CSV header
			// skip user feedback
			if (lines[i].charAt(0) != '#') {

				// process the line
				String[] split = lines[i].split(",");

				// ignore user feedback entries with just a few columns
				if (split.length > 5) {
					String lineTime = split[TIME_COLUMN].replace("\"", "")
							.trim();
					if (isInTimeRange(lineTime, secondsIntoPast)) {
						result.add(split);
					}
				}
			}
		}

		return result;
	}

	/**
	 * helper function to parse time from string
	 *
	 * @param time
	 *            string to parse time from
	 * @return time as double
	 */
	private double parseTime(String time) {

		double parsedTime = 0;

		try {
			parsedTime = dateFormat.parse(time).getTime();
		} catch (ParseException e) {
			Log.e(TAG, "error parsing time from string: " + time);
			e.printStackTrace();
		}

		return parsedTime;
	}
}
