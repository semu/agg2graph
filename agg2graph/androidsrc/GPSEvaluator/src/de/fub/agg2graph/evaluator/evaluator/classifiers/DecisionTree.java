package de.fub.agg2graph.evaluator.evaluator.classifiers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.util.Log;

import de.fub.agg2graph.evaluator.evaluator.features.Feature;

/**
 * Representation of the decision tree in java
 *
 */
public class DecisionTree implements Classifiable {

    private static final String TAG = "DecisionTree";
	
	private final GenericTreeNode<Object> root;
	private final List<GenericTreeNode<Object>> nodeList;

    public DecisionTree(String graph) {
		root = new GenericTreeNode<Object>();
		root.number = 0;
		
		nodeList = new ArrayList<GenericTreeNode<Object>>();
		
		String[] lines = graph.split(System.getProperty("line.separator"));
		
		// collect nodes
		for (String line: lines) {
			if (line.startsWith("N")) {
				String label = line.split("\"")[1];
				int nodeNr = Integer.parseInt("" + parseStringToIntUntilWhitespaceOrOtherSymbol(line,1,'-'));
				int toNodeNr = -1;
				if (line.contains("->")) {
					toNodeNr = Integer.parseInt("" + parseStringToIntUntilWhitespaceOrOtherSymbol(line.split("->")[1].split(" ")[0],1,'-'));
				}
				
				boolean found = false;
				if (toNodeNr == -1) {
                    Log.d(TAG, "Node " + line);
					for (GenericTreeNode<Object> n: nodeList) {
						if (n.number == nodeNr) {
							found = true;
						}
					}
					
					if (!found) {
						GenericTreeNode<Object> newNode = new GenericTreeNode<Object>();
						newNode.data = label;
						newNode.number = nodeNr;
						nodeList.add(newNode);
					}
				}
			}
		}
		
		// collect edges and build tree
		for (String line: lines) {
			if (line.startsWith("N")) {
				String label = line.split("\"")[1];
				
				//numbers can be > 1
				int nodeNr = parseStringToIntUntilWhitespaceOrOtherSymbol(line,1,'-');
				int toNodeNr = -1;
				if (line.contains("->")) {
					toNodeNr = Integer.parseInt("" + parseStringToIntUntilWhitespaceOrOtherSymbol((line.split("->")[1].split(" ")[0]),1,'-'));
				}
				
				if (toNodeNr >= 0) {
					Log.d(TAG, "EDGE " + nodeNr + "->" + toNodeNr + " \"" + label + "\"");
					for (GenericTreeNode<Object> n: nodeList) {
						if (n.number == nodeNr) {
							for (GenericTreeNode<Object> m: nodeList) {
								if (m.number == toNodeNr) {
									m.edgeLabel = label;
									n.addChild(m);
								}
							}
						}
					}
					Log.d(TAG, nodeList.toString());
				}
			}
		}
		
	}
	
	/**
	 * Parse string from start until whitespace for an integer
	 * @param in
	 * @param start
	 * @return first {@link int}
	 */
	private int parseStringToIntUntilWhitespaceOrOtherSymbol(String in, int start, char symbol) {
		char[] chars = in.toCharArray();
		
		String result = "";
		
		for (int i = start; i < chars.length; i++) {
			if (chars[i] == ' ' || chars[i] == symbol) {
                break;
            }
			result += chars[i];
		}
		
		return Integer.parseInt(result);
	}
	
	/**
	 * Calculates the result for the given features of the decision tree
     * @param features pairs of feature name and value
	 * @return movement mode as {@link String}
	 */
	public String getResult(HashMap<Feature, Double> features) {
		double compareValue = 0;
		double edgeValue;
		String result = null;
		String[] split;
		String op;
		
		HashMap<String,Double> map = new HashMap<String,Double>();

		for(Feature f : features.keySet()) {
			map.put(f.getName(), features.get(f));
		}
		
		Log.d(TAG, "FeatureMap: " + map.toString());
		//Log.d(TAG, "DecisionTree: " + this.toString());
		
		if(nodeList.size() == 0) {
			return result;
		}
		
		//maxSpeed
		GenericTreeNode<Object> cursor = nodeList.get(0);
		GenericTreeNode<Object> parent = null;
		
		boolean firstRun = true;
		
		//just an special barrier to make sure while doesn't end in 1st if bracket
		boolean finish = false;
		//check edges for maxSpeed decision values
		while (!finish) {
			
			String label = cursor.data.toString();

			//if there is no whitespace in label this is an identify point like maxspeed, avgspeed, ...
 			if (firstRun) {

				compareValue = map.get(label);
				parent = cursor;
				cursor = cursor.getChildAt(0);
				
				firstRun=false;
			// if there is an edge label this value should be compared
			} else if (cursor.edgeLabel != null) {
				split = cursor.edgeLabel.split(" ");
				op = split[0];
				//0 => operator, 1 => value
				edgeValue = Double.parseDouble(split[1]);
				
				boolean op_res = false;
				
				if (op.equals("<")) {
					op_res = (compareValue < edgeValue);
				} else if (op.equals(">")) {
					op_res = (compareValue > edgeValue);
				} else if (op.equals("<=")) {
					op_res = (compareValue <= edgeValue);
				} else if (op.equals(">=")) {
					op_res = (compareValue >= edgeValue);
				} else {
					Log.d(TAG, "WARNING: No compare value found");
				}
				
				if (op_res) {
					cursor = nodeList.get(parent.getChildAt(0).number);
				} else {
					cursor = nodeList.get(parent.getChildAt(1).number);
				}
				
				if (cursor.children.size() == 0) {
                    finish = true;
                }

				Log.d(TAG, cursor.toString());
				firstRun = true;
			} 
			
			
		}
		result = cursor.getData().toString().split(" ")[0];
		
		return result;
	}

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(nodeList.size() * 150);

        for (GenericTreeNode<Object> gt : nodeList) {
            result.append(gt.toString());
        }

        return result.toString();
    }
}