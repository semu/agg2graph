package de.fub.agg2graph.evaluator.evaluator;

/**
 * Data structure for bvgstop entrys
 *
 */
public class BvgRow {

	private double stop_id;
	private String stop_code;
	private String stop_name;
	private String stop_desc;
	private double stop_lat;
	private double stop_lon;
	private double location_type;
	private double parent_station;
	
	public BvgRow(double stop_id, String stop_code, String stop_name,
			String stop_desc, double stop_lat, double stop_lon,
			double location_type, double parent_station) {
		super();
		this.stop_id = stop_id;
		this.stop_code = stop_code;
		this.stop_name = stop_name;
		this.stop_desc = stop_desc;
		this.stop_lat = stop_lat;
		this.stop_lon = stop_lon;
		this.location_type = location_type;
		this.parent_station = parent_station;
	}
	public double getStop_id() {
		return stop_id;
	}
	public void setStop_id(int stop_id) {
		this.stop_id = stop_id;
	}
	public String getStop_code() {
		return stop_code;
	}
	public void setStop_code(String stop_code) {
		this.stop_code = stop_code;
	}
	public String getStop_name() {
		return stop_name;
	}
	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}
	public String getStop_desc() {
		return stop_desc;
	}
	public void setStop_desc(String stop_desc) {
		this.stop_desc = stop_desc;
	}
	public double getStop_lat() {
		return stop_lat;
	}
	public void setStop_lat(double stop_lat) {
		this.stop_lat = stop_lat;
	}
	public double getStop_lon() {
		return stop_lon;
	}
	public void setStop_lon(double stop_lon) {
		this.stop_lon = stop_lon;
	}
	public double getLocation_type() {
		return location_type;
	}
	public void setLocation_type(double location_type) {
		this.location_type = location_type;
	}
	public double getParent_station() {
		return parent_station;
	}
	public void setParent_station(double parent_station) {
		this.parent_station = parent_station;
	}
	public String toString() {
		return stop_id + " " + stop_code + " " + stop_name + " " + stop_desc + " " + stop_lat + " " + stop_lon + " " + location_type + " " + parent_station;
	}
}
