package de.fub.agg2graph.evaluator.evaluator.classifiers;

/**
 * Classifiers used in the app.
 */
public enum Classifier {
    RANDOM_FOREST,
    DECISION_TREE
}
