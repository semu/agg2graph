package de.fub.agg2graph.evaluator.evaluator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;

/**
 * Enables poor optimized bvgstop access. Using this class it's possible to calculate the transportation distance of the given coordinates. Calculation in the past will be cached to reduce cpu usage.
 *
 */
public class BvgStops {
	
	public static final List<BvgRow> data = new ArrayList<BvgRow>();
	
	private static final HashMap<String, Double> cache = new HashMap<String,Double>();
	
	/**
	 * Read bvg stops file and save it to params
	 */
	public BvgStops(Context context) {
		
		//read text from file
        try {
        	
            
        	String file = "res/raw/bvgstops.txt"; // res/raw/test.txt also work.
        	InputStream is = this.getClass().getClassLoader().getResourceAsStream(file);
            
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            int lineNumber = 0;
            String[] split ;
            while ((line = br.readLine()) != null) {
                //ignore 1st line
            	if(lineNumber++ != 0 ) {
                	split = line.split(",");
                	if(split.length == 8)
                		data.add(new BvgRow(Double.parseDouble(clearQuotesOnDouble(split[0])), clearQuotes(split[1]), clearQuotes(split[2]), clearQuotes(split[3]), Double.parseDouble(clearQuotesOnDouble(split[4])), Double.parseDouble(clearQuotesOnDouble(split[5])),  Double.parseDouble(clearQuotesOnDouble(split[6])), Double.parseDouble(clearQuotesOnDouble(split[7]))));
                	else if( split.length == 7)
                		data.add(new BvgRow(Double.parseDouble(clearQuotesOnDouble(split[0])), clearQuotes(split[1]), clearQuotes(split[2]), clearQuotes(split[3]), Double.parseDouble(clearQuotesOnDouble(split[4])), Double.parseDouble(clearQuotesOnDouble(split[5])),  Double.parseDouble(clearQuotesOnDouble(split[6])), 0));
                	
                	
                	
                }
            }
        }catch (IOException e) {
        	e.printStackTrace();  
        }
	}
	
	/**
	 * Clear quotes
	 * @param in
	 * @return {@link String} without quotes
	 */
	private static String clearQuotes(String in) {
		if (in.length() == 0)
			return in;
		
		if(in.charAt(0) == '"')
			return in.substring(1, in.length()-1);
		else
			return in;
	}
	
	/**
	 * Clear quotes and creates a number if string is empty
	 * @param in
	 * @return unquoted {@link String} with the value "0" if it is empty 
	 */
	public static String clearQuotesOnDouble(String in) {
		if(in.equals(""))
			in = "0";
		
		return clearQuotes(in);
	}
	
	/**
	 * Calculate nearest stopDistance
	 * @param lat
	 * @param lng
	 * @return stop distance as {@double}
	 */
	public static double getNearestStopDistance(double lat, double lng) {
		
		double distance = Double.MAX_VALUE;
		double tmp_distance = -1;
		if(cache.containsKey(lat + "" + lng)) {
			return cache.get(lat + "" + lng);
		} else {

            for (BvgRow aData : data) {
                tmp_distance = distFrom(lat, lng, aData.getStop_lat(), aData.getStop_lon());

                if (tmp_distance < distance)
                    distance = tmp_distance;
            }
			cache.put(lat + "" + lng, distance);
			return distance;
		}
	}
	
	/**
	 * Calculate the distance of two points
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return distance of this two points in meters as {@link double}
	 */
	public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
		    double earthRadius = 3958.75;
		    double dLat = Math.toRadians(lat2-lat1);
		    double dLng = Math.toRadians(lng2-lng1);
		    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
		               Math.sin(dLng/2) * Math.sin(dLng/2);
		    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		    double dist = earthRadius * c;

		    int meterConversion = 1609;

		    return dist * meterConversion;
	    }
}
