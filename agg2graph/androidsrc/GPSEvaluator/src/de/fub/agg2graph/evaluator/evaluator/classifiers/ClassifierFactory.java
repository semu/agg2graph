package de.fub.agg2graph.evaluator.evaluator.classifiers;

import android.util.Log;

/**
 * Factory for classifier creation.
 */
public class ClassifierFactory {

    private static final String TAG = "ClassifierFactory";

    public Classifiable getClassifier(Classifier classifier, String representation) {
        if (classifier.equals(Classifier.DECISION_TREE)) {
            return createDecisionTree(representation);
        } else if (classifier.equals(Classifier.RANDOM_FOREST)) {
            return createRandomForest(representation);
        } else {
            Log.e(TAG, "Classifier " + classifier.name() + " not known.");
            return null;
        }
    }

    private Classifiable createDecisionTree(String decisionTree) {
        return new DecisionTree(decisionTree);
    }

    private Classifiable createRandomForest(String randomForest) {
        return new RandomForest(randomForest);
    }
}
