package de.fub.agg2graph.evaluator.evaluator.classifiers;

import java.util.HashMap;

import de.fub.agg2graph.evaluator.evaluator.features.Feature;

/**
 * Interface classifiers added to the app must implement.
 */
public interface Classifiable {

    /**
     * The method takes an instance to be classified represented as a set of
     * (feature value) tuples here encoded in a HashMap
     */
    public String getResult(HashMap<Feature, Double> features);

}
