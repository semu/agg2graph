package de.fub.agg2graph.evaluator.app;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.fub.agg2graph.evaluator.app.asynctasks.FetchWeather;
import de.fub.agg2graph.evaluator.app.model.Weather;
import de.fub.agg2graph.evaluator.evaluator.BvgStops;

/**
 * Main class that manages user tracking and save the results if requested to a
 * csv file
 *
 */
public class GPSTracker implements LocationListener, SensorEventListener {

	private static final String TAG = "GPSTracker";
	private static final long MIN_TIME = 5L;
	private static final float MIN_DISTANCE = 10;

	private final String currentProvider;
	private final Context context;

	private final DateFormat dateFormat;
	private final Calendar calendar;

	private static File recordFile; // reference to file, that is recorded
	private File fileDir; // directory, where recordFile is located
	private boolean externalFileDir = false;
	private static String csvValue; // DOM Builder
	private boolean active = false; // tracking switch

	private double lastLat;
	private double lastLon;
	private Date lastTime = null;
	private int pointCounter = 0;
	private Location lastLocation;
	private final LocationManager locationManager;

	private double[] linear_acceleration = { 0, 0, 0 };
	private double acceleration_sum = 0;

    private Weather mWeather;

    /**
	 * GPS tracking starts, when tracker is initialised
	 * 
	 * @param context of main activity
	 * @param provider initial provider of location data
	 */
	public GPSTracker(Context context, String provider) {
		this.context = context;
		fileDir = (externalFileDir) ? context.getExternalFilesDir(null)
				: context.getFilesDir();

		//dateFormat = new SimpleDateFormat("y-MM-d'T'k-m-s.S'Z'", Locale.GERMANY);
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		calendar = Calendar.getInstance();

		locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		lastLocation = locationManager
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		currentProvider = provider;
		
		if (lastLocation != null)
		{
          lastLat = lastLocation.getLatitude();
          lastLon = lastLocation.getLongitude();
		}
		Log.i(TAG, "initialising tracker");
		createNewFile();
		prepareCSVString();
		Log.i(TAG, "tracker initialised");

        // get current weather conditions
        new FetchWeather(this).execute(Double.toString(lastLat), Double.toString(lastLon));
	}

	public void changeExternalStorage(boolean external) {
		if (externalFileDir == external) {
			if (external && !isExternalStorageWritable()) {
				Log.e(TAG, "changing path to external storage not possible");
			}
		} else {
			externalFileDir = external;
			fileDir = (externalFileDir) ? context.getExternalFilesDir(null)
					: context.getFilesDir();
			createNewFile();
			Log.i(TAG, "storage path changed to: " + fileDir.getAbsolutePath());
		}
	}

	/**
	 * Creates a new file for gps tracking
	 */
	private void createNewFile() {
		// create new file with name "2012-11-16T10:54:20.723Z"
		calendar.setTime(new Date());
		String time = dateFormat.format(calendar.getTime());
		recordFile = new File(fileDir, time + ".csv");
		Log.i(TAG, "new csv file \"" + recordFile.getName() + "\" created");

	}

	/**
	 * Create the head of a csv file
	 */
	private void prepareCSVString() {
		StringBuilder sb = new StringBuilder(300);
		String valueSeparator = "\",\"";
		sb.append("\"");
		sb.append("Segment");
		sb.append(valueSeparator);
		sb.append("Punkt");
		sb.append(valueSeparator);
		sb.append("Breitengrad (°)");
		sb.append(valueSeparator);
		sb.append("Längengrad (°)");
		sb.append(valueSeparator);
		sb.append("Höhe (m)");
		sb.append(valueSeparator);
		sb.append("Peilung (°)");
		sb.append(valueSeparator);
		sb.append("Genauigkeit (m)");
		sb.append(valueSeparator);
		sb.append("Geschwindigkeit (m/s)");
		sb.append(valueSeparator);
		sb.append("Zeit");
		sb.append(valueSeparator);
		sb.append("Entfernung (n-1)");
		sb.append(valueSeparator);
		sb.append("Temperatur");
		sb.append(valueSeparator);
		sb.append("Trittfrequenz (Umdrehungen pro Minute)");
		sb.append(valueSeparator);
		sb.append("Herzfrequenz (Schläge pro Minute)");
		sb.append(valueSeparator);
		sb.append("Akkustatus");
		sb.append(valueSeparator);
		sb.append("Temperatur");
		sb.append(valueSeparator);
		sb.append("Beschleunigung");
		sb.append("\"\n");

		csvValue = sb.toString();
	}

	/**
	 * Save the current track and create a new empty string for tracking with a
	 * header
	 */
	public boolean saveAndRestart() {

		boolean success = stop();
		if (success) {
			Log.i(TAG, "saving track " + GPSTracker.recordFile.getName()
					+ " and starting new capture");
			createNewFile();
			prepareCSVString();
		} else {
			Log.e(TAG, "saving track " + GPSTracker.recordFile.getName()
					+ " NOT POSSIBLE!");
		}
		return success;
	}

	/* Checks if external storage is available for read and write */
	public static boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	/**
	 * Stops track
	 */
	public boolean stop() {
		if (externalFileDir && !isExternalStorageWritable()) {
			Log.e(TAG, "saving track " + GPSTracker.recordFile.getName()
					+ " to external storage not possible");
			return false;
		}

		try {
			OutputStream os = new FileOutputStream(recordFile);
			os.write(GPSTracker.csvValue.getBytes());
			os.close();

		} catch (IOException e) {
			Log.e(TAG, "IOException while writing csvValue to OutputStream");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * When location changes, new location is added to track segment
	 * 
	 * @param location
	 *            - current location
	 */
	@Override
	public void onLocationChanged(Location location) {

		if (active) {

			Log.i(TAG, "location changed");
			double lat = location.getLatitude();
			double lon = location.getLongitude();
			double speed;

			if (location.hasSpeed()) {
				speed = location.getSpeed();
			} else {
				if (lastTime != null) {
					/*
					 * distFrom calculation is not specific to BvgStops, just
					 * part of the class
					 */
					speed = (BvgStops.distFrom(lat, lon, lastLat, lastLon))
							/ ((calendar.getTime().getTime() - lastTime
									.getTime()) / 1000);
				} else {
					speed = 0;
				}
			}

			float distance = location.distanceTo(lastLocation);
			
			calendar.setTime(new Date());
			this.addTrackPoint(1, pointCounter++, lon, lat,
					location.getAltitude(), location.getBearing(),
					location.getAccuracy(), (float) speed, calendar.getTime(),
					distance, acceleration_sum);

			// reset values
			acceleration_sum = 0;
		}

		lastLat = location.getLatitude();
		lastLon = location.getLongitude();
		lastTime = calendar.getTime();
		lastLocation = location;
	}

	private void addTrackPoint(int segment, int point, double lon, double lat,
			double altitude, float bearing, float accuracy, float speed,
			Date time, float distance, double acceleration) {

		String start = "\"";
		String valueSeparator = "\",\"";
		String end = "\"\n";

		StringBuilder sb = new StringBuilder(300);
		sb.append(start);
		sb.append(Integer.toBinaryString(segment));
		sb.append(valueSeparator);
		sb.append(Integer.toString(point));
		sb.append(valueSeparator);
		sb.append(Double.toString(lat));
		sb.append(valueSeparator);
		sb.append(Double.toString(lon));
		sb.append(valueSeparator);
		sb.append(Double.toString(altitude));
		sb.append(valueSeparator);
		sb.append(Double.toString(bearing));
		sb.append(valueSeparator);
		sb.append(Double.toString(accuracy));
		sb.append(valueSeparator);
		sb.append(Double.toString(speed));
		sb.append(valueSeparator);
		sb.append(dateFormat.format(time));
		sb.append(valueSeparator);
		sb.append(Float.toString(distance));
		sb.append(valueSeparator);
		String weather_temperature = (mWeather != null)? Double.toString(mWeather.getTemperature()) : "";
        sb.append(weather_temperature);
		sb.append(valueSeparator);
		sb.append(valueSeparator);
		sb.append(valueSeparator);
		sb.append(valueSeparator);
		sb.append(valueSeparator);
		sb.append(Double.toString(acceleration));
		sb.append(end);

		csvValue = csvValue.concat(sb.toString());
	}

	/**
	 * When provider is disabled
	 */
	@Override
	public void onProviderDisabled(String provider) {

		if (provider.equals(LocationManager.GPS_PROVIDER)) {
			Log.i(TAG, "GPS provider disabled, switching to next best");

			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setSpeedAccuracy(Criteria.ACCURACY_HIGH);
			criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);

			String nextBestProvider = locationManager.getBestProvider(criteria,
					true);
			Log.i(TAG, "Next best is " + nextBestProvider);
			locationManager.requestLocationUpdates(nextBestProvider, MIN_TIME,
					MIN_DISTANCE, this);
		}
	}

	/**
	 * When provider is enabled
	 */
	@Override
	public void onProviderEnabled(String provider) {
		// change to GPS if other provider is active
		if (provider.equals(LocationManager.GPS_PROVIDER)) {
			if (!currentProvider.equals(provider)) {
				Log.i(TAG, "location updates now with GPS provider");
				locationManager.requestLocationUpdates(provider, MIN_TIME,
						MIN_DISTANCE, this);
			}
		} else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
			// only change to network provider, if it constitutes an improvement
			if (!currentProvider.equals(LocationManager.NETWORK_PROVIDER)
					&& !currentProvider.equals(LocationManager.GPS_PROVIDER)) {
				Log.i(TAG, "location updates now with network provider");
				locationManager.requestLocationUpdates(provider, MIN_TIME,
						MIN_DISTANCE, this);
			}
		}
	}

	/**
	 * When status changes
	 */
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		if (provider.equals(LocationManager.GPS_PROVIDER)) {
			if (status == LocationProvider.OUT_OF_SERVICE
					|| status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
				Log.i(TAG, "gps provider currently not available");
				locationManager.requestLocationUpdates(
						LocationManager.NETWORK_PROVIDER, MIN_TIME,
						MIN_DISTANCE, this);
			}
		} else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
			if (status == LocationProvider.OUT_OF_SERVICE
					|| status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
				Log.i(TAG, "network provider currently not available");
				locationManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE,
						this);
			}
		}
	}

	/**
	 * Enables tracking
	 */
	public void enable() {
		active = true;
	}

	/**
	 * Returns current record file
	 * 
	 * @return current record file
	 */
	public static File getCurrentRecordFile() {
		return recordFile;
	}

	/**
	 * Returns current csv value
	 * 
	 * @return current CSV value
	 */
	public static String getCurrentCsvString() {
		return csvValue;
	}

	/**
	 * If user change status a comment will be added to csv
	 * 
	 * @param statusChange
	 *            - new status of movement
	 */
	public static void addUsersStatusVetoToCsv(String statusChange) {
		DateFormat fmt = new SimpleDateFormat("y-MM-d'T'k-m-s.S'Z'",
				Locale.GERMANY);
		Calendar cal = Calendar.getInstance();

		String csvStr = "#" + statusChange + ";" + fmt.format(cal.getTime())
				+ "\n";
		csvValue += csvStr;

		Log.d(TAG, "user veto to csv: " + csvStr);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSensorChanged(SensorEvent event) {

		final double delta = 0.2;
		double[] tmp = { linear_acceleration[0], linear_acceleration[1],
				linear_acceleration[2] };

		linear_acceleration[0] = event.values[0];
		linear_acceleration[1] = event.values[1];
		linear_acceleration[2] = event.values[2];

		double magnitude_old = Math.sqrt(tmp[0] * tmp[0] + tmp[1] * tmp[1]
				+ tmp[2] * tmp[2]);
		double magnitude = Math.sqrt(linear_acceleration[0]
				* linear_acceleration[0] + linear_acceleration[1]
				* linear_acceleration[1] + linear_acceleration[2]
				* linear_acceleration[2]);
		double diff = Math.abs(magnitude - magnitude_old);

		// Log.d(TAG, "SensorChanged: X: " + linear_acceleration[0] +
		// " , Y: "
		// + linear_acceleration[1] + " , Z: "
		// + linear_acceleration[2]);

		if (diff > delta) {
			// Log.d(TAG, "SensorChanged: Mag: " + magnitude);
			acceleration_sum += magnitude;
		}
	}

    public Weather getWeather() {
        return mWeather;
    }

    public void setWeather(Weather weather) {
        mWeather = weather;
    }
}