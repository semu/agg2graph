package de.fub.agg2graph.evaluator.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import de.fub.agg2graph.evaluator.app.activity.MainActivity;

/**
 * Catch global push messages and handle it to present the correct visual presentation to the mainAcitvity view
 *
 */
public class PushResponseReceiver extends BroadcastReceiver {

	public static final String ACTION_RESP = "de.fub.agg2graph.gpseval.app.MESSAGE_PROCESSED";

	@Override
	public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(ACTION_RESP)) {
            String text = intent.getStringExtra("msg");
            Resources res = context.getResources();
            if (text.equals(res.getString(R.string.walking))) {
                MainActivity.editText.setText(res.getString(R.string.walking));
                MainActivity.editImg.setImageResource(R.drawable.walking);
            } else if (text.equals(res.getString(R.string.bike))) {
                MainActivity.editText.setText(res.getString(R.string.bike));
                MainActivity.editImg.setImageResource(R.drawable.bike);
            } else if (text.equals(res.getString(R.string.bus))) {
                MainActivity.editText.setText(res.getString(R.string.bus));
                MainActivity.editImg.setImageResource(R.drawable.bus);
            } else if (text.equals(res.getString(R.string.car))) {
                MainActivity.editText.setText(res.getString(R.string.car));
                MainActivity.editImg.setImageResource(R.drawable.car);
            } else if (text.equals(res.getString(R.string.train))) {
                MainActivity.editText.setText(res.getString(R.string.train));
                MainActivity.editImg.setImageResource(R.drawable.train);
            }
        }
	}

}
