package de.fub.agg2graph.evaluator.app.asynctasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
import de.fub.agg2graph.evaluator.app.Settings;
import de.fub.agg2graph.evaluator.app.activity.MainActivity;
import de.fub.agg2graph.evaluator.evaluator.classifiers.Classifier;

/**
 * Independent {@link AsyncTask} that updates the classifiers on the device
 *
 */
public class GetClassifiers extends AsyncTask<String, Integer, String> {

	private static final String TAG = GetClassifiers.class.getSimpleName();
	private static final String DECISION_TREE_URL = "decisionTree/getLastTree";
	private static final String RANDOM_FOREST_URL = "forest/getLastForestTrees";

	@Override
	protected String doInBackground(String... params) {

		StringBuilder result = new StringBuilder(400);
		String currentClassifier = MainActivity.settings.getString(
				MainActivity.CLASSIFIER_ENTRY, "");
		StringBuilder requestUrl = new StringBuilder(200);
		requestUrl.append(Settings.server_url);

		if (currentClassifier.equals(Classifier.DECISION_TREE.name())) {
			requestUrl.append(DECISION_TREE_URL);
		} else if (currentClassifier.equals(Classifier.RANDOM_FOREST.name())) {
			requestUrl.append(RANDOM_FOREST_URL);
		} else {
			Log.e(TAG,
					"no known classifier selected, falling back on decisionTree");
			requestUrl.append(DECISION_TREE_URL);
		}

		HttpURLConnection urlConnection = null;

		try {
			URL url = new URL(requestUrl.toString());
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.connect();

			int responseCode = urlConnection.getResponseCode();

			//String r = urlConnection.getResponseMessage();
			InputStreamReader streamReader = new InputStreamReader(
					urlConnection.getInputStream());
			BufferedReader br = new BufferedReader(streamReader);

			String line;
			while ((line = br.readLine()) != null) {
				result.append(line);
			}

			if (responseCode != 200) {
				Log.e(TAG, "ResponseCode not OK " + responseCode);
				return null;
			}

		} catch (ClientProtocolException e) {
			Log.e(TAG, "ClientProtocolException " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "IOException " + e.getMessage());
			e.printStackTrace();
		}

		if (urlConnection != null) {
			urlConnection.disconnect();
		}

		Log.d(TAG, "Result: " + result);
		return result.toString();
	}

	@Override
	protected void onPostExecute(String result) {

		super.onPostExecute(result);

		if (result == null) {
			result = "";

			// save this to our decision tree
			Log.d(TAG, "Classifier is null... no classifier saved");

		} else {
			String classifier = "";
			if (MainActivity.classifier == Classifier.DECISION_TREE) {
				try {
					JSONObject jObject = new JSONObject(result);
					classifier = jObject.getString("tree");
				} catch (JSONException e) {
					Log.e(TAG, "mal formatted JSON " + e.getMessage());
					e.printStackTrace();
				}
			}

			if (MainActivity.classifier.equals(Classifier.RANDOM_FOREST)) {
				classifier = result;
			}

			if (!classifier.equals("")) {
				Log.d(TAG, "Classifier received: " + classifier);
				MainActivity.getInstance().makeLongToast(
						"New Classifier received: " + classifier);
				result = classifier;

			} else {
				Log.e(TAG,
						"Something went wrong, reset to standard decision tree");
				result = "";
			}

		}

		if (!result.equals("")) {
			MainActivity.editor.putString(
					MainActivity.CURRENT_CLASSIFIER_ENTRY, result);
			MainActivity.editor.commit();
		}
	}
}
