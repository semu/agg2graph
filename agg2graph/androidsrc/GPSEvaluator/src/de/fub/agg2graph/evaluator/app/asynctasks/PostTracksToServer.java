package de.fub.agg2graph.evaluator.app.asynctasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import de.fub.agg2graph.evaluator.app.R;
import de.fub.agg2graph.evaluator.app.Settings;
import de.fub.agg2graph.evaluator.app.activity.MainActivity;

/**
 * {@link AsyncTask} that post all tracks of gpseval on the device to the
 * server. After a successful transfer all files will be deleted.
 *
 */
public class PostTracksToServer extends AsyncTask<File, Void, Void> {

	private static final String TAG = "PostTracksToServer";

	@Override
	protected Void doInBackground(File... params) {

		for (File param : params) {
			if (sendFileToServer(param)) {
				param.delete();
			}
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void v) {
		Toast toast = Toast.makeText(MainActivity.getInstance(),
				R.string.filesSendToServerOk, Toast.LENGTH_SHORT);
		toast.show();
	}

	/**
	 * Send the selected file to server
	 * 
	 * @param file
	 *            - selected gps track file
	 * @return
	 */
	private boolean sendFileToServer(File file) {

		String result;
		String serverUrl = Settings.server_url + "GPSCapture/setCapture";

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost request = new HttpPost(serverUrl);

		// read text from file
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
			br.close();
		} catch (IOException e) {
			Log.e(TAG, "Error while reading file");
			e.printStackTrace();
		}

		ArrayList<NameValuePair> postParameters;
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("capture", text.toString()));

		try {
			request.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "UTF-8 encoding not supported");
			e.printStackTrace();
		}

		Log.d(TAG, request.toString());
		System.out.println("Send plain string:\"" + text.toString() + "\"");

		try {
			HttpResponse response = httpclient.execute(request);
			int responseCode = response.getStatusLine().getStatusCode();
			result = EntityUtils.toString(response.getEntity());

			Log.d(TAG, "Server Response: " + result);

			if (responseCode == 200) {
				Log.d(TAG, "Server connection ok!");
				return true;
			} else {
				Log.d(TAG, "Server connection failed. Status code: "
						+ responseCode);
			}
		} catch (ClientProtocolException e) {
			Log.e(TAG, "ClientProtocolException while handling server response");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "IOException while handling server response");
			e.printStackTrace();
		}

		return false;
	}
}
