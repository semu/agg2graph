package de.fub.agg2graph.evaluator.app.asynctasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import de.fub.agg2graph.evaluator.app.GPSTracker;
import de.fub.agg2graph.evaluator.app.model.Weather;

/**
 * AsyncTask to fetch the weather from openweathermap.org based on the
 * supplied coordinates.
 */
public class FetchWeather extends AsyncTask<String, Void, Void> {

    private final String LOG_TAG = FetchWeather.class.getSimpleName();
    private final GPSTracker mGPSTracker;

    public FetchWeather(GPSTracker gpsTracker) {
        mGPSTracker = gpsTracker;
    }

    /**
     * Take the String representing the complete forecast in JSON Format and
     * pull out the data we need to construct our feature
     */
    private void getWeatherDataFromJson(String weatherJSONStr)
            throws JSONException {

        // Names of the JSON objects that need to be extracted.
        // Location information
        final String OWM_COORD = "coord";
        final String OWM_COORD_LAT = "lat";
        final String OWM_COORD_LONG = "lon";

        final String OWM_DATETIME = "dt";

        final String OWM_TEMPERATURE_OBJECT = "main";
        final String OWM_TEMPERATURE = "temp";
        final String OWM_PRESSURE = "pressure";
        final String OWM_HUMIDITY = "humidity";

        final String OWM_WIND = "wind";
        final String OWM_WINDSPEED = "speed";

        // Get the response JSON Object
        JSONObject weatherJSON = new JSONObject(weatherJSONStr);

        // Get the coordinates
        JSONObject coordJSON = weatherJSON.getJSONObject(OWM_COORD);
        double lat = coordJSON.getLong(OWM_COORD_LAT);
        double lon = coordJSON.getLong(OWM_COORD_LONG);

        // Get the dateTime
        long dateTime = weatherJSON.getLong(OWM_DATETIME);

        // Get the main object with temperature information
        JSONObject temperatureObject = weatherJSON.getJSONObject(OWM_TEMPERATURE_OBJECT);
        double pressure = temperatureObject.getDouble(OWM_PRESSURE);
        int humidity = temperatureObject.getInt(OWM_HUMIDITY);
        double temperature = temperatureObject.getDouble(OWM_TEMPERATURE);

        JSONObject windObject = weatherJSON.getJSONObject(OWM_WIND);
        double windSpeed = windObject.getDouble(OWM_WINDSPEED);

        Weather.Builder builder = new Weather.Builder();
        builder.pressure(pressure);
        builder.windSpeed(windSpeed);
        builder.temperature(temperature);
        builder.humidity(humidity);
        builder.latitude(lat);
        builder.longitude(lon);
        builder.dateTime(dateTime);


        mGPSTracker.setWeather(builder.build());
    }

    @Override
    protected Void doInBackground(String... params) {

        // If there's no lat, lon pair, there's nothing to look up.
        if (params.length == 0 || params.length != 2) {
            return null;
        }
        String lat = params[0];
        String lon = params[1];

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Raw JSON response as a string.
        String forecastJSONStr = null;

        String format = "json";
        String units = "metric";

        try {
            // Construct the URL for the OpenWeatherMap query
            // For details see: http://openweathermap.org/weather-data#current
            final String FORECAST_BASE_URL =
                    "http://api.openweathermap.org/data/2.5/weather?";
            final String FORMAT_PARAM = "mode";
            final String UNITS_PARAM = "units";

            Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                    .appendQueryParameter("lat", params[0])
                    .appendQueryParameter("lon", params[1])
                    .appendQueryParameter(FORMAT_PARAM, format)
                    .appendQueryParameter(UNITS_PARAM, units)
                    .build();

            Log.d(LOG_TAG, builtUri.toString());

            URL url = new URL(builtUri.toString());

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }

            forecastJSONStr = buffer.toString();

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        try {
            getWeatherDataFromJson(forecastJSONStr);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        // This will only happen if there was an error getting or parsing
        return null;
    }
}
