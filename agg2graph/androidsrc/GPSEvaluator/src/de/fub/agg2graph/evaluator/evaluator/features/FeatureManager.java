package de.fub.agg2graph.evaluator.evaluator.features;

import android.util.Log;

/**
 * Generic feature calculation wrapper.
 */
public enum FeatureManager {

	FEATURE_MANAGER;

	private static final String TAG = "FeatureManager";

	// index of value array of csv
	private static final int SEGMENT_COLUMN = 0;
	private static final int LONGITUDE_COLUMN = 2;
	private static final int LATITUDE_COLUMN = 3;
	private static final int ALTITUDE_COLUMN = 4;
	private static final int BEARING_COLUMN = 5;
	private static final int PRECISION_COLUMN = 6;
	private static final int SPEED_COLUMN = 7;
	public static final int TIME_COLUMN = 8;
	private static final int DISTANCE_COLUMN = 9;
    private static final int WEATHER_COLUMN = 10;
	public static final int ACCELEROMETER_COLUMN = 15;

	// predefined thresholds TODO make configurable
	private static final int BEARING_CHANGE_THRESHOLD = 10;
	private static final double STOP_THRESHOLD = 2.0;

	// predefined N for max_n_ features TODO make configurable
	private static final int N = 3;

	private int secondsIntoPast;
	private FeatureCalculator featureCalculator;

	public void setup() {
		featureCalculator = FeatureCalculator.FEATURE_CALCULATOR;
	}

	public void setSecondsIntoPast(int secondsIntoPast) {
		this.secondsIntoPast = secondsIntoPast;
	}

	public double calculateFeature(Feature feature) {
		try {
			if (feature.equals(Feature.AVG_SPEED)) {
				return featureCalculator.getAverageFeature(secondsIntoPast,
						SPEED_COLUMN);

			} else if (feature.equals(Feature.MAX_SPEED)) {
				return featureCalculator.getMaxFeature(secondsIntoPast,
						SPEED_COLUMN);

			} else if (feature.equals(Feature.MAX_N_SPEED)) {
				return featureCalculator.getMaxNFeature(secondsIntoPast,
						SPEED_COLUMN, N);

			} else if (feature.equals(Feature.AVG_TRANSPORTATION_DISTANCE)) {
				return featureCalculator.getAvgTransportationDistance(
						secondsIntoPast, LONGITUDE_COLUMN, LATITUDE_COLUMN);

			} else if (feature.equals(Feature.AVG_PRECISION)) {
				return featureCalculator.getAverageFeature(secondsIntoPast,
						PRECISION_COLUMN);

			} else if (feature.equals(Feature.MAX_PRECISION)) {
				return featureCalculator.getMaxFeature(secondsIntoPast,
						PRECISION_COLUMN);

			} else if (feature.equals(Feature.MAX_N_PRECISION)) {
				return featureCalculator.getMaxNFeature(secondsIntoPast,
						PRECISION_COLUMN, N);

			} else if (feature.equals(Feature.AVG_BEARING_CHANGE)) {
				return featureCalculator.getAvgBearingChange(secondsIntoPast,
						BEARING_CHANGE_THRESHOLD, BEARING_COLUMN);

			} else if (feature.equals(Feature.STOP_RATE)) {
				return featureCalculator.getStopRate(secondsIntoPast,
						STOP_THRESHOLD, SPEED_COLUMN, DISTANCE_COLUMN);

			} else if (feature.equals(Feature.NUMBER_OF_SEGMENTS)) {
				return featureCalculator.getNumberOfSegments(secondsIntoPast,
						SEGMENT_COLUMN);

			} else if (feature.equals(Feature.TRACK_LENGTH)) {
				return featureCalculator.getTrackLength(secondsIntoPast,
						DISTANCE_COLUMN);

			} else if (feature.equals(Feature.MAX_ACCELERATION)) {
				return featureCalculator.getMaxAcceleration(secondsIntoPast,
						SPEED_COLUMN);

			} else if (feature.equals(Feature.MAX_N_ACCELERATION)) {
				return featureCalculator.getMaxNAcceleration(secondsIntoPast,
						SPEED_COLUMN, N);

			} else if (feature.equals(Feature.AVG_ACCELERATION)) {
				return featureCalculator.getAvgAcceleration(secondsIntoPast,
						SPEED_COLUMN);

			} else if (feature.equals(Feature.VARIANCE_OF_SPEED)) {
				return featureCalculator.getVarianceOfSpeed(secondsIntoPast,
						SPEED_COLUMN);

			} else if (feature.equals(Feature.AVG_ACCELEROMETER)) {
				return featureCalculator.getAvgAccelerometerMagnitude(
						secondsIntoPast, ACCELEROMETER_COLUMN);

			} else if (feature.equals(Feature.WEATHER)) {
                return featureCalculator.getWeather(secondsIntoPast, WEATHER_COLUMN);
            } else {
				Log.e(TAG, "Unknown feature " + feature.name());
			}
		} catch (NumberFormatException e) {
			Log.e(TAG, e.getMessage());
		}

		return 0.0;
	}
}
