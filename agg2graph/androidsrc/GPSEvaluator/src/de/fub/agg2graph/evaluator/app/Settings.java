package de.fub.agg2graph.evaluator.app;

/**
 * Settings for this application
 *
 */
public class Settings {
	/**
	 * current server url
	 */
	public final static String server_url = "http://projects.mi.fu-berlin.de/gpseval/";
	
	/**
	 * Seconds to past that is used to calculate the correct movement mode
	 */
	public final static int seconds_to_past_on_calculation = 150; 
}
