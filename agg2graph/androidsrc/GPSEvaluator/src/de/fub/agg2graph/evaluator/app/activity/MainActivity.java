package de.fub.agg2graph.evaluator.app.activity;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import de.fub.agg2graph.evaluator.app.GPSTracker;
import de.fub.agg2graph.evaluator.app.PushResponseReceiver;
import de.fub.agg2graph.evaluator.app.R;
import de.fub.agg2graph.evaluator.app.asynctasks.GetClassifiers;
import de.fub.agg2graph.evaluator.app.asynctasks.PostTracksToServer;
import de.fub.agg2graph.evaluator.app.service.DynamicDetectionService;
import de.fub.agg2graph.evaluator.evaluator.BvgStops;
import de.fub.agg2graph.evaluator.evaluator.classifiers.Classifier;
import de.fub.agg2graph.evaluator.evaluator.features.Feature;

/**
 * Entry activity for this application. It starts all services and tasks that we
 * require.
 */
public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();
	private static final String PREFS_NAME = "Prefs";

	public static final String CLASSIFIER_ENTRY = "classifier";
	public static final String FEATURES_ENTRY = "features";
	public static final String CURRENT_CLASSIFIER_ENTRY = "currentClassifier";
	public static final String[] CLASS_NAMES = { "Walking", "Bike", "Bus", "Car", "Train" };

	public static final int time_to_past = de.fub.agg2graph.evaluator.app.Settings.seconds_to_past_on_calculation;

	private static GPSTracker gpsTracker;
	public static Classifier classifier = Classifier.DECISION_TREE;

	private static MainActivity instance;
	public static SharedPreferences.Editor editor;
	public static SharedPreferences settings;
	public static TextView editText;
	public static ImageView editImg;

	private final Context self = this;
	private PushResponseReceiver receiver;
	private boolean externalStorage = false;

	private AlertDialog gpsDialog;
	private AlertDialog classifierDialog;

	private SensorManager mSensorManager;
	private Sensor mSensor;

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		if (intent.getExtras() != null) {
			if (intent.getExtras().getBoolean("fromNotification")) {
				String status = intent.getExtras().getString("status");
				correctMobilityMode(status);
				Log.d(TAG, "BROADCAST CLICKED: "+status);
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		instance = this;

		// setup bvg data
		new BvgStops(this);
		if (BvgStops.data.size() > 0) {
			Log.d(TAG, "Bvg stops initialized");
		}

		// set session variables
		settings = getSharedPreferences(PREFS_NAME, 0);
		editor = settings.edit();

		checkSettings();

		Intent intent = new Intent(this, DynamicDetectionService.class);
		startService(intent);

		editText = (TextView) findViewById(R.id.statusText);
		editImg = (ImageView) findViewById(R.id.statusImg);

		receiver = new PushResponseReceiver();
		IntentFilter filter = new IntentFilter(PushResponseReceiver.ACTION_RESP);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		// register receiver
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
				filter);

		setupGPSCapture();

		final Button restartButton = (Button) findViewById(R.id.restartButton);
		restartButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				boolean success = gpsTracker.saveAndRestart();
				Resources res = getResources();

				String dir = (externalStorage) ? self.getExternalFilesDir(null)
						.toString() : self.getFilesDir().toString();
				String message = (success) ? res
						.getString(R.string.fileSavedTo) + ": " + dir : res
						.getString(R.string.fileNotSavedTo);

				Toast toast = Toast.makeText(self, message, Toast.LENGTH_SHORT);
				toast.show();
			}
		});

		final Button correctMobilityModeButton = (Button) findViewById(R.id.correctMobilityModeButton);
		correctMobilityModeButton
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Resources res = getResources();

						AlertDialog.Builder builder = new AlertDialog.Builder(
								self);
						builder.setTitle(res
								.getString(R.string.selectStatusAlert));

						final String[] choiceList = {
								res.getString(R.string.walking),
								res.getString(R.string.bike),
								res.getString(R.string.bus),
								res.getString(R.string.car),
								res.getString(R.string.train) };

						builder.setItems(choiceList,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int choice) {
										correctMobilityMode(CLASS_NAMES[choice]);
									}
								});
						AlertDialog alert = builder.create();
						alert.show();
					}
				});

		// check for network availability
		if (this.isNetworkAvailable()) {

			// get new decision tree
			new GetClassifiers().execute();

			// send csv data to server
//			new PostTracksToServer().execute(this.getFilesDir().listFiles());
//			if (GPSTracker.isExternalStorageWritable()) {
//				new PostTracksToServer().execute(this.getExternalFilesDir(null)
//						.listFiles());
//			}
		}

	}
	
	@Override
	public void onDestroy() {
		mSensorManager.unregisterListener(gpsTracker);
	}

	/**
	 * Prepare GPS access and activate it
	 */
	private void setupGPSCapture() {
		// enable GPS access
		Log.i(TAG, "checking gps setting");
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		boolean enabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// enable ACCELEROMETER
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor = mSensorManager
				.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

		// check if enabled and if not offer the user to go to the gps settings
		if (!enabled) {
			Log.i(TAG, "asking user to enable gps");
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					this);
			alertDialogBuilder
					.setMessage("GPS is disabled on your device. Enable it?")
					.setCancelable(false)
					.setPositiveButton("Enable GPS",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									Intent GPSSettingIntent = new Intent(
											android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
									startActivity(GPSSettingIntent);
									Log.i(TAG, "user accepted to enabled gps");
								}
							});
			alertDialogBuilder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
							Log.i(TAG, "user declined to enabled gps");
						}
					});

			gpsDialog = alertDialogBuilder.create();
			gpsDialog.show();
		} else {
			Log.i(TAG, "gps already enabled");
		}

		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setSpeedAccuracy(Criteria.ACCURACY_HIGH);
		criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);

		String provider = locationManager.getBestProvider(criteria, true);
		Location location = locationManager.getLastKnownLocation(provider);

		gpsTracker = new GPSTracker(this, provider);
		locationManager.requestLocationUpdates(provider, 5L, 10f, gpsTracker);
		mSensorManager.registerListener(gpsTracker, mSensor,
				SensorManager.SENSOR_DELAY_NORMAL);

		// initialize location fields
		Log.i(TAG, "initialize location fields");
		if (location != null) {
			Log.i(TAG, "provider \"" + provider + "\" has been selected");
			gpsTracker.onLocationChanged(location);
		}

		// start tracking
		gpsTracker.enable();
	}

	@Override
	protected void onResume() {
		super.onResume();

		IntentFilter filter = new IntentFilter(PushResponseReceiver.ACTION_RESP);
		filter.addCategory(Intent.CATEGORY_DEFAULT);

		LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
				filter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
		
		// save tracked data to CSV file
		gpsTracker.stop();
	}

	@Override
	protected void onStop() {
		//gpsTracker.saveAndRestart();

		if (gpsDialog != null) {
			gpsDialog.dismiss();
		}

		if (classifierDialog != null) {
			classifierDialog.dismiss();
		}

		super.onStop();
	}

	private void correctMobilityMode(String status) {
		
		// set status to users choice
		if (status.equals(CLASS_NAMES[0])) {
			MainActivity.editText.setText(R.string.walking);
			MainActivity.editImg.setImageResource(R.drawable.walking);
		} else if (status.equals(CLASS_NAMES[1])) {
			MainActivity.editText.setText(R.string.bike);
			MainActivity.editImg.setImageResource(R.drawable.bike);
		} else if (status.equals(CLASS_NAMES[2])) {
			MainActivity.editText.setText(R.string.bus);
			MainActivity.editImg.setImageResource(R.drawable.bus);
		} else if (status.equals(CLASS_NAMES[3])) {
			MainActivity.editText.setText(R.string.car);
			MainActivity.editImg.setImageResource(R.drawable.car);
		} else if (status.equals(CLASS_NAMES[4])) {
			MainActivity.editText.setText(R.string.train);
			MainActivity.editImg.setImageResource(R.drawable.train);
		}

		// set this to csv if user choice is not equals the current status
		if (!status.equals(DynamicDetectionService.getStatus())) {
			GPSTracker.addUsersStatusVetoToCsv(status);

			// add this status to our service
			DynamicDetectionService.setStatusByUser(status);
		}
	}

	/**
	 * Check if network is available
	 * 
	 * @return {@link boolean} availability
	 */
	boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null
		// otherwise check if we are connected
		return networkInfo != null && networkInfo.isConnected();
	}

	@Override
	/**
	 * Add all menu entries to settings list
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		// inflate the menu; this adds items to the action bar if it is present
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.settings, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Toast toast;
		switch (item.getItemId()) {

		case R.id.chooseClassifier:
			selectClassifier();
			return true;

		case R.id.clearFilesystem:

			// delete all files
			File[] files = this.getFilesDir().listFiles();
			for (File file : files) {
				file.delete();
			}
			if (GPSTracker.isExternalStorageWritable()) {
				for (File file : this.getExternalFilesDir(null).listFiles()) {
					file.delete();
				}
			}

			toast = Toast.makeText(this, R.string.filesDeletedOk,
					Toast.LENGTH_SHORT);
			toast.show();

			return true;

		case R.id.sendToServer:
			// check for network availability
			if (this.isNetworkAvailable()) {
				// send csv data to server
				new PostTracksToServer()
						.execute(this.getFilesDir().listFiles());
				if (GPSTracker.isExternalStorageWritable()) {
					new PostTracksToServer().execute(this.getExternalFilesDir(
							null).listFiles());
				}
				toast = Toast.makeText(this,
						R.string.sending,
						Toast.LENGTH_SHORT);
				toast.show();
			} else {
				toast = Toast.makeText(this,
						R.string.noInternetAvailableForPostTracks,
						Toast.LENGTH_SHORT);
				toast.show();
			}
			return true;

		case R.id.showClassifier:

			toast = Toast.makeText(this,
					settings.getString(CURRENT_CLASSIFIER_ENTRY, ""), Toast.LENGTH_LONG);
			toast.show();

			return true;

		case R.id.updateClassifier:
			// check for network availability
			if (this.isNetworkAvailable()) {
				new GetClassifiers().execute();
				toast = Toast.makeText(this, R.string.updateDecisionTreeOk,
						Toast.LENGTH_SHORT);
				toast.show();
			} else {
				toast = Toast.makeText(this, R.string.noInternetAvailable,
						Toast.LENGTH_SHORT);
				toast.show();
			}
			return true;

		case R.id.externalStorage:
			externalStorage = !item.isChecked();
			item.setChecked(externalStorage);
			gpsTracker.changeExternalStorage(externalStorage);
			// TODO toast message
			Log.d(TAG, "External Storage? " + externalStorage);
			return true;

		default:
			return super.onContextItemSelected(item);
		}

	}

	private void selectClassifier() {

		Classifier[] temp = Classifier.values();
		int numberOfClassifiers = temp.length;
		final CharSequence classifiers[] = new CharSequence[numberOfClassifiers];

		for (int i = 0; i < numberOfClassifiers; i++) {
			classifiers[i] = temp[i].toString();
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		Resources res = getResources();
		builder.setTitle(res.getString(R.string.selectClassifier));
		builder.setItems(classifiers, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Log.d(TAG,
						"Selected classifier " + classifiers[which].toString());
				editor = settings.edit();
				editor.putString(CLASSIFIER_ENTRY,
						classifiers[which].toString());
				editor.apply();
				classifier = Classifier.values()[which];
				
				dialog.dismiss();
				
				new GetClassifiers().execute();
			}
		});
		classifierDialog = builder.create();
		classifierDialog.show();
	}

	private void checkSettings() {

		// if no classifier is set yet assign precomputed tree
		String currentClassifier = settings.getString(CURRENT_CLASSIFIER_ENTRY,
				"");

		if (currentClassifier.equals("")) { // fallback on decisionTree
			String result = "digraph J48Tree {\n"
					+ "N0 [label=\"MaxSpeed\" ]\n"
					+ "N0->N1 [label=\"<= 9.151287\"]\n"
					+ "N1 [label=\"MaxSpeed\" ]\n"
					+ "N1->N2 [label=\"<= 4.653688\"]\n"
					+ "N2 [label=\"Walking (4.0)\" shape=box style=filled ]\n"
					+ "N1->N3 [label=\"> 4.653688\"]\n"
					+ "N3 [label=\"Bike (4.0)\" shape=box style=filled ]\n"
					+ "N0->N4 [label=\"> 9.151287\"]\n"
					+ "N4 [label=\"MaxSpeed\" ]\n"
					+ "N4->N5 [label=\"<= 17.785178\"]\n"
					+ "N5 [label=\"MaxSpeed\" ]\n"
					+ "N5->N6 [label=\"<= 15.154174\"]\n"
					+ "N6 [label=\"Bus (3.0/1.0)\" shape=box style=filled ]\n"
					+ "N5->N7 [label=\"> 15.154174\"]\n"
					+ "N7 [label=\"Car (2.0)\" shape=box style=filled ]\n"
					+ "N4->N8 [label=\"> 17.785178\"]\n"
					+ "N8 [label=\"AvgSpeed\" ]\n"
					+ "N8->N9 [label=\"<= 14.870121\"]\n"
					+ "N9 [label=\"Train (5.0/1.0)\" shape=box style=filled ]\n"
					+ "N8->N10 [label=\"> 14.870121\"]\n"
					+ "N10 [label=\"Car (2.0/1.0)\" shape=box style=filled ]\n"
					+ "}";

			editor = settings.edit();
			editor.putString(CURRENT_CLASSIFIER_ENTRY, result);
			editor.apply();
		}

		if (settings.getStringSet(FEATURES_ENTRY, null) == null) {
			Set<String> featureSet = new HashSet<String>();
			Feature[] features = Feature.values();
			for (int i = 0; i < features.length; i++) {
				featureSet.add(features[i].name());
			}

			editor = settings.edit();
			editor.putStringSet(FEATURES_ENTRY, featureSet);
			editor.apply();
		}

		if (settings.getString(CLASSIFIER_ENTRY, "").equals("")) {

			editor = settings.edit();
			editor.putString(CLASSIFIER_ENTRY, Classifier.DECISION_TREE.name());
			editor.apply();
		}
	}

	/**
	 * Create a long toast to MainActivity
	 * 
	 * @param msg
	 */
	public void makeLongToast(String msg) {
		Toast toast = Toast.makeText(self, msg, Toast.LENGTH_LONG);
		toast.show();
	}

	/**
	 *
	 * @return {@link MainActivity} current instance of main activity
	 */
	public static MainActivity getInstance() {
		return instance;
	}
}
