package de.fub.agg2graph.evaluator.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import android.app.IntentService;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import de.fub.agg2graph.evaluator.app.EasyNotification;
import de.fub.agg2graph.evaluator.app.PushResponseReceiver;
import de.fub.agg2graph.evaluator.app.R;
import de.fub.agg2graph.evaluator.app.activity.MainActivity;
import de.fub.agg2graph.evaluator.evaluator.classifiers.Classifiable;
import de.fub.agg2graph.evaluator.evaluator.classifiers.ClassifierFactory;
import de.fub.agg2graph.evaluator.evaluator.features.Feature;
import de.fub.agg2graph.evaluator.evaluator.features.FeatureManager;

/**
 * Main service of the application. The calculation of the classifier and
 * their consequences are processed here.
 *
 */
public class DynamicDetectionService extends IntentService {

	private static final String TAG = "DynamicDetectionService";

	private static boolean lockDynamicDetection;

	private static int timeHelper;

	private static String status;
	private static String lastDynamicStatusDetection;

	private FeatureManager featureManager;

	private EasyNotification notification;

	private Timer timer30;

	private boolean running;

	/**
	 * A constructor is required, and must call the super IntentService(String)
	 * constructor with a name for the worker thread.
	 */
	public DynamicDetectionService() {
		super("HelloIntentService");
	}

	@Override
	public void onCreate() {
		super.onCreate();

		timeHelper = -1;
		lockDynamicDetection = false;

		running = false;
		timer30 = new Timer();
		notification = new EasyNotification(getApplicationContext());
		featureManager = FeatureManager.FEATURE_MANAGER;
		featureManager.setup();
	}

	/**
	 * Main Task that executes every 15 seconds. It will skip a circle if
	 * another instance of this task still runs.
	 */
	private final TimerTask mainTask = new TimerTask() {
		public void run() {
			int icon = R.drawable.ic_launcher;

			// just start this new task is an old is not running!
			if (!running) {
				running = true;
				if (!lockDynamicDetection) {

					Log.d(TAG, "15s main Task");
					Resources res = getResources();

					String current_status = getCurrentStatus();
					if (current_status != null
							&& !current_status.equals(status)) {
						status = current_status;
						String status_localized = status;

						if (status.equals(MainActivity.CLASS_NAMES[0])) {
							status_localized = res.getString(R.string.walking);
							broadcastResult(status_localized);
							icon = R.drawable.walking;
							//MainActivity.editText.setText(R.string.walking);	// TODO: must run on UI-Thread
							//MainActivity.editImg.setImageResource(R.drawable.walking);
						} else if (status.equals(MainActivity.CLASS_NAMES[1])) {
							status_localized = res.getString(R.string.bike);
							icon = R.drawable.bike;
						} else if (status.equals(MainActivity.CLASS_NAMES[2])) {
							status_localized = res.getString(R.string.bus);
							icon = R.drawable.bus;
						} else if (status.equals(MainActivity.CLASS_NAMES[3])) {
							status_localized = res.getString(R.string.car);
							icon = R.drawable.car;
						} else if (status.equals(MainActivity.CLASS_NAMES[4])) {
							status_localized = res.getString(R.string.train);
							icon = R.drawable.train;
						}
						
						broadcastResult(status_localized);

						if (android.os.Build.VERSION.SDK_INT >= 16) {
							// only for api lv 16+
							notification.send(status, status_localized, icon);
						}

					}
					Log.d(TAG, "Classifier result: " + status);

				} else {
					String current_status = getCurrentStatus();

					// stop timeout on status change
					if (current_status == null
							|| !current_status
									.equals(lastDynamicStatusDetection)) {
						timeHelper = 0;
					} else {
						timeHelper = timeHelper - 5;
					}

					if (timeHelper <= 0) {
						lockDynamicDetection = false;
					}
				}
				running = false;
			} else {
				Log.d(TAG, "Skip main task as an old task is still running");
			}
		}
	};

	/**
	 * The IntentService calls this method from the default worker thread with
	 * the intent that started the service. When this method returns,
	 * IntentService stops the service, as appropriate.
	 */
	protected void onHandleIntent(Intent intent) {
		// Normally we would do some work here, like download a file.
		// For our sample, we just sleep for 5 seconds.
		synchronized (this) {
			timer30.scheduleAtFixedRate(mainTask, 0, 15000);
		}
	}

	/**
	 * Broadcast the given message
	 * 
	 * @param msg
	 */
	private void broadcastResult(String msg) {
		// processing done here
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(PushResponseReceiver.ACTION_RESP);
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		broadcastIntent.putExtra("msg", msg);
		sendBroadcast(broadcastIntent);
		
		Log.d(TAG, "BROADCAST: "+msg);
	}

	/**
	 * Sets manually a movement mode
	 * 
	 * @param changeStatusTo
	 *            value of values/status.xml
	 */
	public static void setStatusByUser(String changeStatusTo) {
		lastDynamicStatusDetection = status;
		status = changeStatusTo;
		lockDynamicDetection = true;
		timeHelper = 60 * 20; // 20 minutes
		Log.d(TAG, "Lock detection 5s task for 120s");
	}

	/**
	 * Get current status (mode of movement)
	 * 
	 * @return status as {@link String}
	 */
	public static String getStatus() {
		return status;
	}

	/**
	 * Recalculate the current mode of movement
	 * 
	 * @return status as {@link String}
	 */
	String getCurrentStatus() {

		// TODO update key and handling in GetDecisionTree
		String classifierString = MainActivity.settings.getString(
				MainActivity.CURRENT_CLASSIFIER_ENTRY, "");

		List<Feature> featureList = parseFeatures(MainActivity.settings
				.getStringSet(MainActivity.FEATURES_ENTRY, null));

		HashMap<Feature, Double> features = new HashMap<Feature, Double>(
				featureList.size());

		featureManager.setSecondsIntoPast(MainActivity.time_to_past);

		for (Feature f : featureList) {
			double value = featureManager.calculateFeature(f);
			Log.i(TAG, "Feature " + f.name() + " with value " + value);
			features.put(f, value);
		}
		//Log.i(TAG, "Classifierstring: " + classifierString);

		ClassifierFactory cf = new ClassifierFactory();

		Classifiable classifier = cf.getClassifier(MainActivity.classifier,
				classifierString);

		return classifier.getResult(features);
	}

	private List<Feature> parseFeatures(Set<String> features) {
		List<Feature> result = new ArrayList<Feature>();

		if (features != null) {
			for (String s : features) {
				Log.d(TAG, "Feature from StringSet: " + s);
				result.add(Feature.valueOf(s));
			}
		}

		return result;
	}
}
