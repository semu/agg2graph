package de.fub.agg2graph.evaluator.evaluator.classifiers;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fub.agg2graph.evaluator.evaluator.features.Feature;


/**
 * Random Forest classifier.
 * Can only be used to act as an evaluator for a given Random Forest, no
 * training.
 */
public class RandomForest implements Classifiable {

    private static final String TAG = "RandomForest";

    private static final Integer NUMBER_OF_CLASSES = 5;
    // transportation mode classes
    private static final String WALKING = "Walking";
    private static final String BUS = "Bus";
    private static final String BIKE = "Bike";
    private static final String CAR = "Car";
    private static final String TRAIN = "Train";

    private static final HashMap<String, Integer> RESULT_MAP =
            new HashMap<String, Integer>(NUMBER_OF_CLASSES);

    private final List<DecisionTree> forest;

    public RandomForest(String randomForest) {

        // TODO check string representation and adapt parsing
        String[] trees = randomForest.split(",");

        this.forest = new ArrayList<DecisionTree>(trees.length);
        for (String s : trees) {
            forest.add(new DecisionTree(s));
        }

        Log.i(TAG, "Built RandomForest: " + this.toString());

        RESULT_MAP.put(WALKING, 0);
        RESULT_MAP.put(BUS, 0);
        RESULT_MAP.put(BIKE, 0);
        RESULT_MAP.put(CAR, 0);
        RESULT_MAP.put(TRAIN, 0);
    }

    public List<DecisionTree> getForest() {
        return this.forest;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(forest.size() * 150);

        for (DecisionTree d : forest) {
            result.append(d.toString());
            result.append("\n");
        }

        return result.toString();
    }

    /**
     * Calculates the result for the given features of the random forest
     * @param features pairs of feature name and value
     * @return movement mode as {@link String}
     */
    public String getResult(HashMap<Feature, Double> features) {

        for (DecisionTree d : forest) {
            String res = d.getResult(features);
            if (RESULT_MAP.containsKey(res)) {
                RESULT_MAP.put(res, RESULT_MAP.get(res) + 1);
            } else {
                Log.e(TAG, "Predicted class not found: " + res);
            }
        }

        String result = "none";
        Integer lastMax = Integer.MIN_VALUE;
        for (Map.Entry<String, Integer> e : RESULT_MAP.entrySet()) {
            if (lastMax < e.getValue()) {
                result = e.getKey();
                Log.d(TAG, "new max: " + e.getValue() + " class: " + result);
            } else {
                Log.d(TAG, "current max: " + lastMax + " not updated: " + e.getValue());
            }
        }

        return result;
    }
}
