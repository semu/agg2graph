package de.fub.agg2graph.evaluator.evaluator.features;

/**
 * Features used in the app.
 */
public enum Feature {
    AVG_TRANSPORTATION_DISTANCE("AvgTransportationDistance"),
    MAX_SPEED("MaxSpeed"),
    AVG_SPEED("AvgSpeed"),
    MAX_N_SPEED(""),
    VARIANCE_OF_SPEED(""),
    STOP_RATE("StopRate"),
    AVG_BEARING_CHANGE("AvgBearingChange"),
    NUMBER_OF_SEGMENTS("Segments"),
    MAX_PRECISION("MaxPrecision"),
    AVG_PRECISION("AvgPrecision"),
    MAX_N_PRECISION(""),
    MAX_ACCELERATION("MaxAcceleration"),
    AVG_ACCELERATION("AvgAcceleration"),
    MAX_N_ACCELERATION("MaxNAcceleration"),
    WEATHER("AvgTemperature"),
    TRACK_LENGTH("TrackLength"),
    AVG_ACCELEROMETER("AvgAccelerometer");

    private String name;

    Feature(String name) {
    	this.name = name;
    }

    public String getName() {
    	return this.name;
    }
}
