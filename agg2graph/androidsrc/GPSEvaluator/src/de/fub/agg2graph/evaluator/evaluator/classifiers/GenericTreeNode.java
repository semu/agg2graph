package de.fub.agg2graph.evaluator.evaluator.classifiers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Generic Tree Node
 *
 * @param <T>
 */
public class GenericTreeNode<T> {
	public T data;
	public int number;
	public String edgeLabel;
	public List<GenericTreeNode<T>> children;
	
	public GenericTreeNode() {
		super();
		children = new ArrayList<GenericTreeNode<T>>();
	}
	
	public GenericTreeNode(T data) {
		this();
		setData(data);
	}
	
	List<GenericTreeNode<T>> getChildren() {
		return this.children;
	}
	
	int getNumberOfChildren() {
		return getChildren().size();
	}
	
	boolean hasChildren() {
		return (getNumberOfChildren() > 0);
	}
	
	public void setChildren(List<GenericTreeNode<T>> children) {
		this.children = children;
	}
	
	public void addChild(GenericTreeNode<T> child) {
		children.add(child);
	}
	
	public void addChildAt(int index, GenericTreeNode<T> child) throws IndexOutOfBoundsException {
		children.add(index, child);
	}
	
	public void removeChildren() {
		this.children = new ArrayList<GenericTreeNode<T>>();
	}
	
	public void removeChildAt(int index) throws IndexOutOfBoundsException {
		children.remove(index);
	}
	
	public GenericTreeNode<T> getChildAt(int index) throws IndexOutOfBoundsException {
		return children.get(index);
	}
	
	public T getData() {
		return this.data;
	}
	
	void setData(T data) {
		this.data = data;
	}
	
	GenericTreeNode<T> getNodeWithNumber(int number) {
		if (this.number == number) {
			return this;
		} else {
			if (this.hasChildren()) {
				for (GenericTreeNode<T> node: getChildren()) {
					return node.getNodeWithNumber(number);
				}
			} else {
				return null;
			}
			
		}
		return null;
	}
	
	public String toString() {
		return "Node " + number + " (" + data + ")";
	}
	
	public boolean equals(GenericTreeNode<T> node) {
		return node.getData().equals(getData());
	}
	
	public int hashCode() {
		return getData().hashCode();
	}
	
	public String toStringVerbose() {
		String stringRepresentation = toString() + ":[";
		
		for (GenericTreeNode<T> node : getChildren()) {
			stringRepresentation += node.getData().toString() + ", ";
		}
		
		//Pattern.DOTALL causes ^ and $ to match. Otherwise it won't. It's retarded.
		Pattern pattern = Pattern.compile(", $", Pattern.DOTALL);
		Matcher matcher = pattern.matcher(stringRepresentation);
		
		stringRepresentation = matcher.replaceFirst("");
		stringRepresentation += "]";
		
		return stringRepresentation;
	}
}