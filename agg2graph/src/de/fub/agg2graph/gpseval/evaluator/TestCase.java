/*
 * Copyright 2014 Damla Durmaz, Ferhat Beyaz, Sebastian Barthel, Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fub.agg2graph.gpseval.evaluator;

import de.fub.agg2graph.gpseval.Config;
import de.fub.agg2graph.gpseval.data.AggregatedData;
import de.fub.agg2graph.gpseval.data.DataLoader;
import de.fub.agg2graph.gpseval.data.filter.TrackFilter;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilter;
import de.fub.agg2graph.gpseval.WekaEval;
import de.fub.agg2graph.gpseval.features.Feature;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * A TestCase-instance is used to load GPS-data-files, run Weka with the loaded
 * data and return the results.
 * 
 * The GPS-data-files are loaded using a
 * {@link de.fub.agg2graph.gpseval.data.DataLoader DataLoader}-instance with
 * {@link de.fub.agg2graph.gpseval.data.filter.TrackFilter TrackFilter}s and
 * {@link de.fub.agg2graph.gpseval.data.filter.WaypointFilter WaypointFilter}s.
 * Weka is run using the {@link de.fub.agg2graph.gpseval.WekaEval WekaEval}
 * -class. All information needed for the test-case is loaded from a
 * {@link de.fub.agg2graph.gpseval.Config Config}-object. The results are stored
 * in a {@link de.fub.agg2graph.gpseval.WekaResult WekaResult}-object.
 * 
 */
public class TestCase {

	private Config mCfg;
	private Map<String, List<String>> mClassesFolderMapping;
	private List<Feature> mFeatures;
	private List<TrackFilter> mTrackFilters;
	private List<WaypointFilter> mWaypointFilters;
	private WekaEval mWekaEval;
	
	/**
	 * Create a test-case based on a {@link de.fub.agg2graph.gpseval.Config
	 * Config}.
	 * 
	 * @param cfg
	 */
	public TestCase(Config cfg) {
		mCfg = cfg;
		mFeatures = mCfg.getFeatures();
		mClassesFolderMapping = mCfg.getClassesFolderMapping();
		mTrackFilters = mCfg.getTrackFilters();
		mWaypointFilters = mCfg.getWaypointFilters();
	}
	
	/**
	 * Run the test-case: Load GPS-data-files, run Weka and return the results.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws de.fub.agg2graph.gpseval.TestCase.NoDataException
	 * @throws Exception
	 */
	public void run() throws Exception {
		// 1. Load GPS-tracks
		DataLoader gpsLoader = new DataLoader();
		gpsLoader.addFeatures(mFeatures);
		gpsLoader.addTrackFilters(mTrackFilters);
		gpsLoader.addWaypointFilters(mWaypointFilters);
		Map<String, List<AggregatedData>> gpsData = gpsLoader.loadData(mClassesFolderMapping);
		
		// 2. Run Weka
		mWekaEval = new WekaEval(gpsData, mFeatures);
		mWekaEval.setTrainingSetSize(mCfg.getTrainingSetSize());
		mWekaEval.run();
	}
	
	/**
	 * Returns the decision tree, computed in this test case
	 *
	 */
	public String getDecisionTree() {
		return mWekaEval.getDecisionTree().getDecisionTree();
	}
	
	public List<String> getRandomForest() {
		return mWekaEval.getRandomForest().getTrees();
	}
}