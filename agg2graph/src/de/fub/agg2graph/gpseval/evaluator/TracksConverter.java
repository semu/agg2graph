package de.fub.agg2graph.gpseval.evaluator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.bind.JAXBException;
import de.fub.agg2graph.io.GpxToCsvConverter;

/**
 * TracksConverter is used to call the method to transform gpx-files into 
 * csv-format, which is used by the {@link de.fub.agg2graph.gpseval}} project.
 * 
 * @see de.fub.agg2graph.io.GpxToCsvConverter.convertGpxFolderToCsv
 * 
 * @author Damla Durmaz
 * @author Ferhat Beyaz
 * @author Sebastian Barthel
 * @version 03.07.2013
 */
public class TracksConverter {
	
	/**
	 * Starts the transofrmation of gpx-files to csv-formatted files.
	 * 
	 * @param gpx-tracks-folder
	 *            The folder, which contains the gpx-files, which are to be transformed
	 * @param csv-tracks-folder
	 *            The result folder, where the transformed csv-files shall be stored
	 */
	public static void main(String[] args) throws IOException, JAXBException {
		if(args.length < 2) {
			System.out.println("Usage: java -cp <classpath> de.fub.agg2graph.gpseval.evaluator.TracksConverter <gpx-tracks-folder> <csv-tracks-folder>");
			return;
		}
		
		Path gpxFolder = Paths.get(args[0]);
		Path csvFolder = Paths.get(args[1]);
		
		if(!Files.exists(gpxFolder) || !Files.isDirectory(gpxFolder)) {
			System.out.println("<gpx-tracks-folder> not found");
			return;
		}
		
		if(!Files.exists(csvFolder) || !Files.isDirectory(csvFolder)) {
			System.out.println("<gpx-tracks-folder> not found");
			return;
		}
		
		GpxToCsvConverter.convertGpxFolderToCsv(gpxFolder.toAbsolutePath().toString(), csvFolder.toAbsolutePath().toString());
	}
	
}