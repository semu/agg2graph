package de.fub.agg2graph.gpseval.evaluator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Forest {
	private List<String> trees;

	public Forest(List<String> decisionTrees) {
		trees = decisionTrees;
	}
	
	public Forest(String wekaForestString) {
		trees = forestToGraphTrees(wekaForestString);
	}
	
	public List<String> getTrees() {
		return trees;
	}

	public void setTrees(List<String> trees) {
		this.trees = trees;
	}
	
	public List<String> forestToGraphTrees(String forestString) {
		List<String> trees = new ArrayList<>();

		Pattern p = Pattern.compile("\n+(.+)\n\nSize .+", Pattern.DOTALL);
		String[] splitted = forestString.split("==========");
		for(String split : splitted) {
			Matcher m = p.matcher(split);
			if(m.matches()) {
				String tree = treeToGraph(m.group(0));
				//System.out.println(tree);
				trees.add(tree);
			}
		}
		
		return trees;
	}
	
	public String treeToGraph(String forestTreeString){
		String tree = "[digraph J48Tree {\n";
		
		List<Integer> parentList = new ArrayList<>();
		parentList.add(0);
		int child = 0;
		String lastLeafPath = null;
		String[] lines = forestTreeString.split("\n");
		
		Pattern p_node = Pattern.compile("((?:\\|   )*)(.+) ([><][=]?.+)");
		Pattern p_leaf = Pattern.compile("((?:\\|   )*)(.+) ([><][=]?.+) : (.+)");
		for(String line : lines) {
			Matcher m = p_leaf.matcher(line);
			if(m.matches()) {
				String path = m.group(1);
				String label_parent = m.group(2);
				String label_edge = m.group(3);
				String label_child = m.group(4);
				int parent = parentList.get(parentList.size()-1);
				
				if(child == parent) {
					tree += "N"+parent+" [label=\""+label_parent+"\" ]\n";
					child++;
				}
				
				if(lastLeafPath != null && !lastLeafPath.equals(path)) {
					parentList.remove(parentList.size()-1);
					parent = parentList.get(parentList.size()-1);
				}
				
				tree += "N"+parent+"->N"+child+" [label=\""+label_edge+"\" ]\n";
				tree += "N"+child+" [label=\""+label_child+"\" ]\n";
				
				child++;
				lastLeafPath = path;
				
			} else if((m = p_node.matcher(line)).matches()) {
				String path = m.group(1);
				String label_parent = m.group(2);
				String label_edge = m.group(3);
				int parent = parentList.get(parentList.size()-1);
				
				if(child == parent) {
					tree += "N"+parent+" [label=\""+label_parent+"\" ]\n";
					child++;
				}
				
				if(lastLeafPath != null && !lastLeafPath.equals(path)) {
					parentList.remove(parentList.size()-1);
					parent = parentList.get(parentList.size()-1);
				}
				
				tree += "N"+parent+"->N"+child+" [label=\""+label_edge+"\" ]\n";
				parentList.add(child);
				
				lastLeafPath = null;
			}
		}
		
		tree += "}\n]\n";
		return tree;
	}
	
	@Override
	public String toString() {
		String forestString = "";
		for(String tree : trees) {
			forestString += tree + "\n";
		}
		return forestString;
	}
	
}
