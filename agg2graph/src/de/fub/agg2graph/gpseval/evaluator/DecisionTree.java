package de.fub.agg2graph.gpseval.evaluator;

/**
 * Domain Model for decision trees. Decision trees are saved as strings in the J48-Format.
 * This format is given by the J48-Algorithm from WEKA. 
 * Every database entry also has a creation timestamp.
 *
 * @author Damla Durmaz
 * @author Ferhat Beyaz
 * @author Sebastian Barthel
 * @version 03.07.2013
 */
public class DecisionTree {
	
	/**
	 * Trees are saved as Strings in the J48 format.
	 */
	private String mDecisionTree;
	
	/**
	 * Stores the decision tree in the class instance variables.
	 * 
	 * @param graph
	 * 			The decision tree in digraph format
	 */
	public DecisionTree(String graph) {
		this.mDecisionTree = graph;
	}
	
	/**
	 * Returns the current decision tree.
	 */
	public String getDecisionTree() {
		return mDecisionTree;
	}
	
	/**
	 * Sets a new decision tree.
	 * 
	 * @param graph
	 * 			The decision tree in digraph format
	 */
	public void setDecisionTree(String graph) {
		this.mDecisionTree = graph;
	}
}