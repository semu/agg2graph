package de.fub.agg2graph.gpseval.evaluator;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.fub.agg2graph.gpseval.Config;
import de.fub.agg2graph.gpseval.ConfigFile;

/**
 * This class contains wrappers for the gpseval project under 
 * @see @see de.fub.agg2graph.gpseval
 * 
 * <p>
 * Usage: java -cp &lt;classpath&gt; de.fub.agg2graph.gpseval.evaluator.GPSEvaluator
 * &lt;config-folder&gt;
 * </p>
 * 
 * <p>
 * The config-folder should contain one or many configuration-files (xml-files).
 * Each config-file describes a test-case. For example it defines where
 * GPS-data-files can be found and to which class they belong to. Moreover it
 * defines features that should be included in the feature set. You can set
 * paramters, e.g. to determine which filters should be used to limit the used
 * data (based on tracks and GPS-waypoints).
 * </p>
 * 
 * @see de.fub.agg2graph.gpseval.ConfigFile
 * 
 * @author Damla Durmaz
 * @author Ferhat Beyaz
 * @author Sebastian Barthel
 * @version 03.07.2013
 */
public class GPSEvaluator {
	
	private List<String> mTrees = new LinkedList<>();
	
	private List<List<String>> mForests = new ArrayList<>();
	
	/**
	 * The main entry point. The
	 * {@link de.fub.agg2graph.gpseval.evaluator.GPSEvaluator#start(String[] args) start}-method
	 * will be called.
	 * 
	 * @see de.fub.agg2graph.gpseval.evaluator.GPSEvaluator.start
	 * 
	 * @param args
	 *            the command line arguments
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		GPSEvaluator app = new GPSEvaluator();
		app.start(args);
		System.out.println(app.getTrees());
		System.out.println(app.getForests());
	}
	
	/**
	 * Read all config-files from the specified folder and run a
	 * {@link de.fub.agg2graph.gpseval.evaluator.TestCase TestCase} for each of them. Then
	 * provides resulting trees in 
	 * 
	 * @see de.fub.agg2graph.gpseval.evaluator.GPSEvaluator.getTrees 
	 * 
	 * @param args
	 *            The 0th-element must contain the path to the folder containing
	 *            the config-files.
	 * 
	 * @throws Exception
	 */
	public void start(String[] args) throws Exception {
		if(args.length < 1) {
			System.out.println("Usage: java -cp <classpath> de.fub.agg2graph.gpseval.evaluator.GPSEvaluator <config-folder>");
			return;
		}
		
		Path configFolder = Paths.get(args[0]);
		if(!Files.exists(configFolder) || !Files.isDirectory(configFolder)) {
			System.out.println("<config-folder> not found!");
			return;
		}
		
		List<String> configs = new LinkedList<>();
		
		// For each entry inside the config-folder
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(configFolder)) {
			for (Path entry: stream) {
				if(!Files.isDirectory(entry)) {
					// entry is a file (should be a config-file)
					configs.add(entry.toString());
				}
			}
		}
		
		// Run test for each config file
		Collections.sort(configs);
		int i = 0;
		for(String config : configs) {
			System.out.println("[" + ++i + "/" + configs.size() + "] Evaluating test case ...");
			Path entry = Paths.get(config);
			Config cfg = new ConfigFile(entry);
			if(cfg.getFeatures().isEmpty()) {
				System.out.println("No features found (" + entry + ")!");
				return;
			}
			TestCase test = new TestCase(cfg);
			test.run();
			mTrees.add(test.getDecisionTree());
			mForests.add(test.getRandomForest());
		}
	}
	
	/**
	 * Returns a list of decision trees, each of them representing one test case. Decision 
	 * trees are built in
	 * 
	 * @see de.fub.agg2graph.gpseval.evaluator.GPSEvaluator.start 
	 * 
	 * @see de.fub.agg2graph.gpseval.evaluator.getTrees 
	 * 
	 */
	public List<String> getTrees() {
		return mTrees;
	}
	
	public List<List<String>> getForests() {
		return mForests;
	}
}