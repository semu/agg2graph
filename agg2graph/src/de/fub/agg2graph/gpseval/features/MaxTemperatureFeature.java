package de.fub.agg2graph.gpseval.features;

import de.fub.agg2graph.gpseval.data.Waypoint;

/**
 * The MaxTemperatureFeature determines the maximum
 * temperature over the duration of the GPS-Track
 */
public class MaxTemperatureFeature extends Feature {

    private double mMaxTemperature = 0;

    @Override
    public void addWaypoint(Waypoint entry) {
        if (entry.getTemperature() > mMaxTemperature) {
            mMaxTemperature = entry.getTemperature();
        }
    }

    @Override
    public double getResult() {
        return mMaxTemperature;
    }

    @Override
    public void reset() {
        mMaxTemperature = 0;
    }
}
