/*
 * Copyright 2014 Serdar Tosun, Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpseval.features;

import de.fub.agg2graph.gpseval.data.Waypoint;
import de.fub.agg2graph.structs.GPSCalc;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MeanVelocityFeature extends Feature {

    private List<Double> list = new ArrayList<Double>(100);
    private Waypoint lastWaypoint;

    @Override
    public void reset() {
        list.clear();
    }

    @Override
    public void addWaypoint(Waypoint waypoint) {
        if (lastWaypoint != null
                && lastWaypoint.getTimestamp() != null
                && waypoint.getTimestamp() != null) {
            double velocity = 0;
            long seconds = Math.max(0, (waypoint.getTimestamp().getTime() - lastWaypoint.getTimestamp().getTime()) / 1000);
            if (seconds > 0) {
                velocity = GPSCalc.getDistVincentyFast(waypoint.getLat(), waypoint.getLon(), lastWaypoint.getLat(), lastWaypoint.getLon()) / seconds;
            }
            list.add(velocity);
        }
        lastWaypoint = waypoint;
    }

    @Override
    public double getResult() {
        Collections.sort(list);
        return list.isEmpty() ? 0 : list.get(list.size() / 2);
    }
}
