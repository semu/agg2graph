/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpseval.features;

import de.fub.agg2graph.gpseval.data.Waypoint;


/**
 * The AvgTemperatureFeature calculates the average temperature
 * over the duration of the GPS-Track
 */
public class AvgTemperatureFeature extends Feature {

    private int mCount = 0;
    private double mSumTemperature = 0;

    @Override
    public void addWaypoint(Waypoint entry) {
        if (entry.getTemperature() > 0) {
            mSumTemperature += entry.getTemperature();
            ++mCount;
        }
    }

    @Override
    public double getResult() {
        return mCount > 0 ? (mSumTemperature / mCount) : 0;
    }

    @Override
    public void reset() {
        mCount = 0;
        mSumTemperature = 0;
    }
}
