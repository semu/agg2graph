/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpseval.features;

import de.fub.agg2graph.gpseval.data.Waypoint;

/**
 * Feature that computes the average magnitude of
 * accelerometer-values.
 */
public class AvgAccelerometerFeature extends Feature {

	private int pointCount = 0;
	private Waypoint lastWaypoint = null;
	private double sumAccelerationMagnitude = 0;

	@Override
	public void reset() {
		pointCount = 0;
		lastWaypoint = null;
		sumAccelerationMagnitude = 0;
	}

	@Override
	public void addWaypoint(Waypoint waypoint) {
		if (lastWaypoint != null && waypoint != null
				&& waypoint.getTimestamp() != null
				&& lastWaypoint.getTimestamp() != null) {

			double timeDiff = (waypoint.getTimestamp().getTime() - lastWaypoint
					.getTimestamp().getTime()) / 1000d;

			if (timeDiff > 0) {
				double normalizedAccelerationMagnitude = (waypoint.getAcceleration()) / timeDiff;
				sumAccelerationMagnitude += normalizedAccelerationMagnitude;
				pointCount++;
			}
		}
		lastWaypoint = waypoint;
	}

	@Override
	public double getResult() {
		return pointCount == 0 ? 0 : sumAccelerationMagnitude / pointCount;
	}
}
