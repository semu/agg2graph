/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpseval.data.processing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import au.com.bytecode.opencsv.CSVWriter;
import de.fub.agg2graph.gpseval.data.Waypoint;
import de.fub.agg2graph.gpseval.data.file.MyTracksCSVFile;
import de.fub.agg2graph.io.FileExport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.MetadataType;
import de.fub.agg2graph.schemas.gpx.TrkType;
import de.fub.agg2graph.schemas.gpx.TrksegType;
import de.fub.agg2graph.schemas.gpx.WptType;

public class CSVFileProcessor {

	private MyTracksCSVFile csvFile = null;

	public CSVFileProcessor(MyTracksCSVFile csvFile) {
		super();
		this.csvFile = csvFile;
	}

	public void copyCSV(File f) {
		Iterator<Waypoint> csvIt = csvFile.iterator();
		try {
			CSVWriter csvOut = new CSVWriter(new FileWriter(f));
			String[] firstLine = { "Name", "Art der Aktivität", "Beschreibung" };
			csvOut.writeNext(firstLine);
			String[] secondLine = { "", csvFile.getActivityType(), csvFile.getDescription() };
			csvOut.writeNext(secondLine);
			csvOut.writeNext(new String[0]);
			String[] fourthLine = { "Segment", "Punkt", "Breitengrad (°)",
					"Längengrad (°)", "Höhe (m)", "Peilung (°)",
					"Genauigkeit (m)", "Geschwindigkeit (m/s)", "Zeit",
					"Leistung (W)", "Trittfrequenz (Umdrehungen pro Minute)",
					"Herzfrequenz (Schläge pro Minute)", "Akkustatus" };
			csvOut.writeNext(fourthLine);
			while (csvIt.hasNext()) {
				Waypoint wp = csvIt.next();
				csvOut.writeNext(wp.toData());
			}
			csvOut.flush();
			csvOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void convertCSVtoGPX(File f) throws DatatypeConfigurationException, JAXBException, IOException {
		Iterator<Waypoint> csvIt = csvFile.iterator();
		GpxType gpx = new GpxType();
		TrkType trk = new TrkType();
		int lastSeg = -1;
		TrksegType seg = null;
		while (csvIt.hasNext()) {
			Waypoint wp = csvIt.next();
			WptType wpt = new WptType();
			if (lastSeg != wp.getSegment()) {
				lastSeg = wp.getSegment();
				if (seg != null)
					trk.getTrkseg().add(seg);
				seg = new TrksegType();
			}
			wpt.setLat(BigDecimal.valueOf(wp.getLat()));
			wpt.setLon(BigDecimal.valueOf(wp.getLon()));
			wpt.setEle(BigDecimal.valueOf(wp.getAlt()));
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(wp.getTimestamp());
			XMLGregorianCalendar cal = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(c);
			wpt.setTime(cal);
			seg.getTrkpt().add(wpt);
		}
		if (seg != null)
			trk.getTrkseg().add(seg);
		gpx.getTrk().add(trk);
		gpx.setMetadata(new MetadataType());
		gpx.getMetadata().setDesc(csvFile.getDescription());
		gpx.getMetadata().setKeywords(csvFile.getActivityType());
		FileExport fe = new FileExport();
		fe.export(f, gpx);
	}

}
