/*
 * Copyright 2013 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpseval.data.processing;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import de.fub.agg2graph.gpseval.data.file.MyTracksCSVFile;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilter;
import de.fub.agg2graph.gpseval.data.filter.WaypointFilterFactory;

public class CSVFolderProcessor {

	private String trackDir = null;

	public CSVFolderProcessor(String trackDir) {
		super();
		this.trackDir = trackDir;
	}

	public void eraseSecretLocations(String copyDir, String homesList) {

		File tracks = new File(trackDir);
		File[] track = tracks.listFiles();
		String files = null;

		for (int i = 0; i < track.length; i++) {

			if (track[i].isFile()) {
				files = track[i].getName();
				if (files.toLowerCase().endsWith(".csv")) {
					MyTracksCSVFile mFile = new MyTracksCSVFile();
					mFile.setDataFile(track[i].toPath());
					WaypointFilter filter = WaypointFilterFactory.getFactory()
							.newWaypointFilter("RemoveHome");
					filter.setParam("homesList", homesList);
					filter.reset(); // apply params
					mFile.addWaypointFilter(filter);
					CSVFileProcessor mProc = new CSVFileProcessor(mFile);
					String prename = files.substring(0, files.toLowerCase()
							.indexOf(".csv"));
					mProc.copyCSV(new File(copyDir + prename + ".csv"));
				}
			}
		}

	}
	
	public void convertFolderToGpx(String copyDir) throws DatatypeConfigurationException, JAXBException, IOException {

		File tracks = new File(trackDir);
		File[] track = tracks.listFiles();
		String files = null;

		for (int i = 0; i < track.length; i++) {

			if (track[i].isFile()) {
				files = track[i].getName();
				if (files.toLowerCase().endsWith(".csv")) {
					MyTracksCSVFile mFile = new MyTracksCSVFile();
					mFile.setDataFile(track[i].toPath());
					CSVFileProcessor mProc = new CSVFileProcessor(mFile);
					String prename = files.substring(0, files.toLowerCase()
							.indexOf(".csv"));
					mProc.convertCSVtoGPX(new File(copyDir + prename + ".gpx"));
				}
			}
		}

	}

}
