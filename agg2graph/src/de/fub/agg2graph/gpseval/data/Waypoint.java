package de.fub.agg2graph.gpseval.data;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a single waypoint of a GPS-track.
 */
public class Waypoint {

    private static final Logger LOG = Logger.getLogger(Waypoint.class.getName());
    public static final String PROP_NAME_SPEED = "waypoint.speed";
    public static final String PROP_NAME_BEARING = "waypoint.bearing";
    public static final String PROP_NAME_SEGEMENTS = "waypoint.segments";
    public static final String PROP_NAME_PRECISION = "waypoint.precision";
    public static final String PROP_NAME_TIMESTAMP = "waypoint.timestemp";
    public static final String PROP_NAME_LATITUDE = "waypoint.latitude";
    public static final String PROP_NAME_LONGITUDE = "waypoint.longitude";
    public static final String PROP_NAME_DISTANCE = "waypoint.distance";
    public static final String PROP_NAME_TEMPERATURE = "waypoint.temperature";
    public static final String PROP_NAME_ACCELERATION = "waypoint.acceleration";

    private Collection<String> propertyList = getPropertyList();
    private final HashMap<String, String> propertyMap = new HashMap<String, String>();

    protected double speed;
    protected double bearing;
    protected int segment;
    protected int precision;
    protected Date timestamp;
    protected double lat;
    protected double lon;
	protected double alt;
    protected double distance;
    protected double temperature;
    protected double acceleration;

    public Waypoint() {
    }

    public Waypoint(Map<String, String> propertyMap) {
        this.propertyMap.putAll(propertyMap);
        init();
    }

    private void init() {
        for (Map.Entry<String, String> entry : this.propertyMap.entrySet()) {
            if (entry.getValue() != null) {
                try {
                    switch (entry.getKey()) {
                        case PROP_NAME_BEARING:
                            bearing = Double.parseDouble(entry.getValue().replaceFirst(",", "."));
                            break;
                        case PROP_NAME_LATITUDE:
                            lat = Double.parseDouble(entry.getValue().replaceFirst(",", "."));
                            break;
                        case PROP_NAME_LONGITUDE:
                            lon = Double.parseDouble(entry.getValue().replaceFirst(",", "."));
                            break;
                        case PROP_NAME_PRECISION:
                            precision = Integer.parseInt(entry.getValue());
                            break;
                        case PROP_NAME_SEGEMENTS:
                            segment = Integer.parseInt(entry.getValue());
                            break;
                        case PROP_NAME_SPEED:
                            speed = Double.parseDouble(entry.getValue().replaceFirst(",", "."));
                            break;
                        case PROP_NAME_TIMESTAMP:
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                            try {
                            	if(entry.getValue().contains("T")) {
                            		timestamp = sdf.parse(entry.getValue().replaceFirst("T", " "));
                            	} else {
                            		timestamp = sdf.parse(entry.getValue());
                            	}
                            } catch (ParseException ex) {
                                LOG.log(Level.SEVERE, "Error parsing Date: {0}", ex.getMessage());
                            }
                            break;
                        case PROP_NAME_DISTANCE:
                            distance = Double.parseDouble(entry.getValue().replaceFirst(",", "."));
                            break;
                        case PROP_NAME_TEMPERATURE:
                            temperature = Double.parseDouble(entry.getValue().replaceFirst(",", "."));
                            break;
                        case PROP_NAME_ACCELERATION:
                        	acceleration = Double.parseDouble(entry.getValue().replaceFirst(",", "."));
                        	break;
                        default:
                            LOG.info(MessageFormat.format("Property ({0}) not supported!", entry.getKey())); //NO18N
                    }
                } catch (IllegalArgumentException ex) {
                    // LOG.log(Level.SEVERE, ex.getMessage(), ex); // very slow if a large file does not contain a whole column 
                }
            }
        }
    }

    /**
     *
     * @param data
     * @deprecated use instead {@link Waypoint(Map<String, String> propertyMap)
     */
    @Deprecated
    public Waypoint(String[] data) {
    	try{
        speed = Double.parseDouble(data[7].replaceFirst(",", "."));
    	}
    	catch (NumberFormatException e){
    	speed = 0.0;
    	}
    	try {
        segment = Integer.parseInt(data[0]);
    	}
    	catch (NumberFormatException e){
    	segment = 0;
    	}
    	try {
        precision = Integer.parseInt(data[6]);
    	}
    	catch (NumberFormatException e){
    	precision = 0;
    	}
    	try {
        bearing = Double.parseDouble(data[5].replaceFirst(",", "."));
    	}
    	catch (NumberFormatException e){
    	bearing = 0;
    	}

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSX");
        try {
            timestamp = sdf.parse(data[8].replaceFirst("T", " "));
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error parsing Date: {0}", ex.getMessage());
        }
        lat = Double.parseDouble(data[2]);
        lon = Double.parseDouble(data[3]);
    	try {
        alt = Double.parseDouble(data[4]);
    	}
    	catch (NumberFormatException e){
    	alt = 0.0;
    	}

    }



	public String[] toData() {
		String[] data = new String[11];
		data[0] = String.valueOf(segment);
		data[1] = "";
		data[2] = String.valueOf(lat);
		data[3] = String.valueOf(lon);
		data[4] = String.valueOf(alt);
		data[5] = String.valueOf(bearing);
		data[6] = String.valueOf(precision);
		data[7] = String.valueOf(speed);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSX");
		data[8] = sdf.format(timestamp).replaceFirst(" ", "T").replaceFirst("\\+01", "Z");
        data[9] = String.valueOf(distance);
        data[10] = String.valueOf(temperature);
        data[11] = String.valueOf(acceleration);
		return data;
	}

    public double getSpeed() {
        return speed;
    }

    public double getBearing() {
        return bearing;
    }

    public int getSegment() {
        return segment;
    }

    public int getPrecision() {
        return precision;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public double getAlt() {
		return alt;
	}

    public double getDistance() {
        return distance;
    }

    public double getTemperature() {
        return temperature;
    }
    
    public double getAcceleration() {
    	return acceleration;
    }

	public void setAlt(double alt) {
		this.alt = alt;
	}

    public String putPropertyValue(String propertyName, String value) {
        return propertyMap.put(propertyName, value);
    }

    public String getPropertyValue(String propertyName) {
        return propertyMap.get(propertyName);
    }

    public static Collection<String> createPropertyList(Class<? extends Waypoint> clazz) {
        Collection<String> collection = new ArrayList<String>();
        Waypoint waypoint;
        try {
            waypoint = clazz.newInstance();
            collection = waypoint.getPropertyList();
        } catch (InstantiationException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return collection;
    }

    public Collection<String> getPropertyList() {
        if (propertyList == null) {
            propertyList = Arrays.asList(PROP_NAME_BEARING,
                    PROP_NAME_LATITUDE,
                    PROP_NAME_LONGITUDE,
                    PROP_NAME_PRECISION,
                    PROP_NAME_SEGEMENTS,
                    PROP_NAME_SPEED,
                    PROP_NAME_TIMESTAMP,
                    PROP_NAME_DISTANCE,
                    PROP_NAME_TEMPERATURE,
                    PROP_NAME_ACCELERATION);
        }
        return propertyList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.speed) ^ (Double.doubleToLongBits(this.speed) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.bearing) ^ (Double.doubleToLongBits(this.bearing) >>> 32));
        hash = 41 * hash + this.segment;
        hash = 41 * hash + this.precision;
        hash = 41 * hash + Objects.hashCode(this.timestamp);
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.lat) ^ (Double.doubleToLongBits(this.lat) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.lon) ^ (Double.doubleToLongBits(this.lon) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.distance) ^ (Double.doubleToLongBits(this.distance) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.temperature) ^ (Double.doubleToLongBits(this.temperature) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.acceleration) ^ (Double.doubleToLongBits(this.acceleration) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Waypoint other = (Waypoint) obj;
        if (Double.doubleToLongBits(this.speed) != Double.doubleToLongBits(other.speed)) {
            return false;
        }
        if (Double.doubleToLongBits(this.bearing) != Double.doubleToLongBits(other.bearing)) {
            return false;
        }
        if (this.segment != other.segment) {
            return false;
        }
        if (this.precision != other.precision) {
            return false;
        }
        if (!Objects.equals(this.timestamp, other.timestamp)) {
            return false;
        }
        if (Double.doubleToLongBits(this.lat) != Double.doubleToLongBits(other.lat)) {
            return false;
        }
        if (Double.doubleToLongBits(this.lon) != Double.doubleToLongBits(other.lon)) {
            return false;
        }
        if (Double.doubleToLongBits(this.distance) != Double.doubleToLongBits(other.distance)) {
            return false;
        }
        if (Double.doubleToLongBits(this.temperature) != Double.doubleToLongBits(other.temperature)) {
            return false;
        }
        if (Double.doubleToLongBits(this.acceleration) != Double.doubleToLongBits(other.acceleration)) {
        	return false;
        }
        return true;
    }
}
