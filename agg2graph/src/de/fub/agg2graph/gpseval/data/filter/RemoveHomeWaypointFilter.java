package de.fub.agg2graph.gpseval.data.filter;

import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Vector;

import de.fub.agg2graph.gpseval.data.SecretLocation;
import de.fub.agg2graph.gpseval.data.Waypoint;
import de.fub.agg2graph.gpseval.data.file.HomesFile;
import de.fub.agg2graph.structs.GPSCalc;


/**
 * A WaypointFilter used to remove waypoints near secret locations.
 *
 * It has one parameter "homesList" that specifies a CSV file of secret locations.
 */
public class RemoveHomeWaypointFilter extends WaypointFilter {

    private Vector<SecretLocation> homes = null;

    @Override
    public void reset() {
        homes = new Vector<SecretLocation>();
        String file = getParam("homesList", "test/gpseval/unit-test/homes.list");
        HomesFile homesFile = new HomesFile();
        homesFile.setDataFile(Paths.get(file));
        Iterator<SecretLocation> homesIt = homesFile.iterator();
        while (homesIt.hasNext()){
        	homes.add(homesIt.next());
        }
    }

    @Override
    public boolean filter(Waypoint gpsData) {
    	Iterator<SecretLocation> homeIt = homes.iterator();
    	while (homeIt.hasNext()){
    		SecretLocation home = homeIt.next();
    		double distance = GPSCalc.getDistance(home.mLat, home.mLon, gpsData.getLat(), gpsData.getLon());
    		if (distance < home.mDist) {
    			return false;
    		}
    	}
        
        return true;
    }
}