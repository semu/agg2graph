package de.fub.agg2graph.gpseval.data.file;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import au.com.bytecode.opencsv.CSVReader;
import de.fub.agg2graph.gpseval.data.Waypoint;

/**
 * MyTracksCSVFile reads GPS-data from a CSV-file exported by the Android-app
 * MyTracks.
 *
 * <p>
 * The first four lines of the file are skipped because they contain additional
 * information.
 * </p>
 *
 * <p>
 * The following lines contain comma-separated values enclosed by quotes. They
 * have the format: segments, number, latitude, longitude, height, bearing,
 * precision, speed, time, ununsed, ununsed, ununsed, ununsed.
 * </p>
 */
public class MyTracksCSVFile extends TrackFile {

	@Override
	protected Iterator<Waypoint> rawIterator() {
		return new GPSDataIterator();
	}

	/**
	 * Read CSV-file and return {@link de.fub.agg2graph.gpseval.data.Waypoint
	 * Waypoint}-objects for each data-line.
	 */
	private class GPSDataIterator implements Iterator<Waypoint> {

		private CSVReader mReader;
		private Waypoint mNextGpsData = null;

		public GPSDataIterator() {
			try {
				mReader = new CSVReader(
						new FileReader(getDataFile().toString()), ',', '"', 4);
			} catch (FileNotFoundException ex) {
				Logger.getLogger(MyTracksCSVFile.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}

		@Override
		public boolean hasNext() {
			mNextGpsData = null;

			try {
				String[] data = mReader.readNext();
				if (data != null) {
					mNextGpsData = new Waypoint(arrayToPropertyMap(data));
					return true;
				}

				return false;

			} catch (IOException ex) {
				Logger.getLogger(MyTracksCSVFile.class.getName()).log(
						Level.SEVERE, null, ex);
				return false;
			}
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Not supported.");
		}

		@Override
		public Waypoint next() {
			return mNextGpsData;
		}

		private HashMap<String, String> arrayToPropertyMap(String[] data) {
			HashMap<String, String> result = new HashMap<>(data.length);
			if (data.length > 0) {
				for (int i = 0; i < data.length; ++i) {
					switch (i) {
					case 0:
						result.put(Waypoint.PROP_NAME_SEGEMENTS, data[0]);
						break;
					case 2:
						result.put(Waypoint.PROP_NAME_LATITUDE, data[2]);
						break;
					case 3:
						result.put(Waypoint.PROP_NAME_LONGITUDE, data[3]);
						break;
					case 5:
						result.put(Waypoint.PROP_NAME_BEARING, data[5]);
						break;
					case 6:
						result.put(Waypoint.PROP_NAME_PRECISION, data[6]);
						break;
					case 7:
						result.put(Waypoint.PROP_NAME_SPEED, data[7]);
						break;
					case 8:
						result.put(Waypoint.PROP_NAME_TIMESTAMP, data[8]);
						break;
					case 9:
						result.put(Waypoint.PROP_NAME_DISTANCE, data[9]);
						break;
					case 10:
						result.put(Waypoint.PROP_NAME_TEMPERATURE, data[14]);
						break;
					case 11:
						result.put(Waypoint.PROP_NAME_ACCELERATION, data[15]);
						break;
					default:
						break;
					}
				}
			} else {
				Logger.getLogger(MyTracksCSVFile.class.getName()).log(
						Level.SEVERE, "malformatted data-line");
			}
			return result;
		}
	}

	public String getActivityType() {
		try(CSVReader reader = new CSVReader(new FileReader(getDataFile()
				.toString()), ',', '"', 1)) {
			String[] data = reader.readNext();
			return data[1];

		} catch (IOException ex) {
			Logger.getLogger(MyTracksCSVFile.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return null;
	}

	public String getDescription() {
		try(CSVReader reader = new CSVReader(new FileReader(getDataFile()
				.toString()), ',', '"', 1)) {
			String[] data = reader.readNext();
			return data[2];

		} catch (IOException ex) {
			Logger.getLogger(MyTracksCSVFile.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return null;
	}

}
