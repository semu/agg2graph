package de.fub.agg2graph.gpseval.data.file;

import au.com.bytecode.opencsv.CSVReader;
import de.fub.agg2graph.gpseval.data.SecretLocation;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * HomesFile reads secret location data from a CSV-file.
 * 
 * <p>
 * The following lines contain comma-separated values. They
 * have the format: latitude, longitude, distance.
 * </p>
 */
public class HomesFile {

	private Path mDataFile;

	/**
	 * Set the path for the GPS-data-file.
	 * 
	 * @param dataFile
	 */
	public void setDataFile(Path dataFile) {
		mDataFile = dataFile;
	}

	public Iterator<SecretLocation> iterator() {
		return new HomesIterator();
	}

	
	protected Iterator<SecretLocation> rawIterator() {
		return new HomesIterator();
	}

	/**
	 * Read CSV-file and return {@link de.fub.agg2graph.gpseval.data.Waypoint
	 * Waypoint}-objects for each data-line.
	 */
	private class HomesIterator implements Iterator<SecretLocation> {

		private CSVReader mReader;
		private SecretLocation mHome = null;

		public HomesIterator() {
			try {
				mReader = new CSVReader(
						new FileReader(mDataFile.toString()), ',', '"', 0);
			} catch (FileNotFoundException ex) {
				Logger.getLogger(HomesFile.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}

		@Override
		public boolean hasNext() {
			mHome = null;

			try {
				String[] data = mReader.readNext();
				if (data != null) {
					mHome = new SecretLocation(data);
					return true;
				}

				return false;

			} catch (IOException ex) {
				Logger.getLogger(HomesFile.class.getName()).log(
						Level.SEVERE, null, ex);
				return false;
			}
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Not supported.");
		}

		@Override
		public SecretLocation next() {
			return mHome;
		}
	}
}
