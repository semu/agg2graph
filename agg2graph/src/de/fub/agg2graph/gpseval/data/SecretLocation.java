package de.fub.agg2graph.gpseval.data;

/**
 * This class represents a secret location.
 */
public class SecretLocation {

	public double mLat;
	public double mLon;
	public double mDist;

	public SecretLocation(String[] data) {
		mLat = Double.parseDouble(data[0]);
		mLon = Double.parseDouble(data[1]);
		mDist = Double.parseDouble(data[2]);

	}

}
