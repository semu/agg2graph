/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.trace;

import java.io.File;
import java.net.URL;
import java.util.Iterator;
import java.util.Stack;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;


public class GpxLoader {
	private String _path;
	private StreamSource stream;
	private XMLEventReader  evRd;
	public GpxLoader(String path) throws XMLStreamException {
		_path = path;
		stream = new StreamSource( _path );
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		evRd = inputFactory.createXMLEventReader( stream );		
	}
	
	public GpxLoader(String path, boolean loadLocal) throws XMLStreamException {
		_path = path;
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		if(loadLocal){
			evRd = inputFactory.createXMLEventReader(this.getClass().getResourceAsStream(path));
		}
		else{
			stream = new StreamSource( _path );
			evRd = inputFactory.createXMLEventReader( stream );
		}
	}
	
	public Traces getTraces() throws XMLStreamException{
		Traces t = new Traces();
		
		Stack<String>   stck = new Stack<String>();
		Trace tmpTrace = new Trace();
		Integer traceVersionsId = tmpTrace.getVersionId();
		double lon=0, lat=0;
		
		while( evRd.hasNext() ) {
			XMLEvent ev = evRd.nextEvent();
			if( ev.isStartElement() ) {
				stck.push( ev.asStartElement().getName().getLocalPart() );
				
				/*if(ev.asStartElement().getName().getLocalPart() == "trk"){
					tmpTrace = t.addTrace("vId " + traceVersionsId + "",traceVersionsId);					
				}*/
				if(ev.asStartElement().getName().getLocalPart() == "trkseg"){
					tmpTrace = t.addTrace("vId " + traceVersionsId + "",traceVersionsId);					
				}
				
				//TODO: Namen mit importieren
				/*if(ev.asStartElement().getName().getLocalPart() == "name"){
					tmpTrace.setName(ev.asCharacters().getData());					
				}*/
				if(ev.asStartElement().getName().getLocalPart() == "trkpt"){
					Iterator<Attribute> iter = ev.asStartElement().getAttributes();
					while( iter.hasNext() ) {
			        	Attribute a = iter.next();
			        	if(a.getName().getLocalPart() == "lat")
		        			lat = Double.valueOf(a.getValue());
			        	else if(a.getName().getLocalPart() == "lon")
			        		lon = Double.valueOf(a.getValue());			        	
			        }
					tmpTrace.addPoint(lon, lat);
				}
			}			
			if(ev.isEndElement()) 
				stck.pop();
		}
		return t;
	}
	
	private static String buildXPathString( Stack<String> stck, String postfix ){
      StringBuffer sb = new StringBuffer();
      for( String s : stck ) sb.append( "/" ).append( s );
      sb.append( postfix );
      return sb.toString();
   }
}
