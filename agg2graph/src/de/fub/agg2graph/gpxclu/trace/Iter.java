/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.trace;

import java.util.Iterator;

public class Iter<E extends IterInterface<T>, T> implements Iterator<T>{
	private E _list;
	private int crtPt = 0;
	private boolean traversierung = true;
	
	Iter(E traces){
		_list = traces;
	}
	/**
	 * Gebe die Liste von hinten rum aus, wenn traversierung == false ist.
	 * @param traces
	 * @param traversierung
	 */
	Iter(E traces, boolean traversierung){
		_list = traces;
		this.traversierung = traversierung;
		if(!traversierung)
			crtPt = _list.size()-1;
	}
	
	@Override
	public boolean hasNext() {
		if(traversierung)
			return crtPt < _list.size();
		else
			return crtPt > -1;
	}

	@Override
	public T next() {
		if(traversierung)
			return _list.get(crtPt++);
		else
			return _list.get(crtPt--);
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
	
}
