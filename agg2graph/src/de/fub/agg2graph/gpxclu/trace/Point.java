/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.trace;

import java.io.Serializable;

public class Point implements Comparable<Point>, Serializable{
	private static final long serialVersionUID = -7518832057240045948L;
	private double _lat;
	private double _lon;
	private double _radLat;
	private double _radLon;
	
	private double _x;
	private double _y;
	
	public Point(double lon, double lat){
		setLat(lat);
		setLon(lon);
//		PtOpSphere.translateToXY(this);
	}
	public Point(double x, double y, boolean XY){
		setX(x);
		setY(y);
//		PtOpSphere.translateToLatLon(this);
	}
	public double getLat(){
		return _lat;
	}
	public double getLon(){
		return _lon;
	}
	public void setLat(double lat){
		_lat = lat;
		_radLat = Math.toRadians(lat);
	}
	public void setLon(double lon){
		_lon = lon;
		_radLon = Math.toRadians(lon);
	}
	public double getRadLat(){
		return _radLat;
	}
	public double getRadLon(){
		return _radLon;
	}
	
	@Override
	public int compareTo(Point o) {
		int tmp=0;
		if(_lat == o.getLat() && _lon == o.getLon())
			tmp = 0;
		else if(_lat > o.getLat() || _lon > o.getLon())
			tmp = -1;
		else if(_lat < o.getLat() || _lon < o.getLon())
			tmp = 1;
		return tmp;
	}
	public String toString(){
		return "(Lon: "+_lon+", Lat: "+_lat+")";
	}
	public double getX() {
		return _x;
	}
	public void setX(double _x) {
		this._x = _x;
	}
	public double getY() {
		return _y;
	}
	public void setY(double _y) {
		this._y = _y;
	}
}
