/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.trace;

import java.util.ArrayList;
import java.util.List;

import gnu.trove.TIntProcedure;

class PTIntProcedure implements TIntProcedure, Comparable<PTIntProcedure> {
	private Integer lastId = null;
	private Integer counter = 0;
	private Point intersection;
	private Integer traceId = null;
	private List<Integer> intersectList = new ArrayList<Integer>();
	
	
	public PTIntProcedure(Point intersection){		
		this.intersection = intersection;
	}
	
	public PTIntProcedure(int traceId){	
		this.traceId = traceId;
	}
	public boolean execute(int i) {
		intersectList.add(i);
		lastId = i;
		counter++;
		return true;
	}

	public Integer getId() {
		return lastId;
	}
	
	public List<Integer> getNearestNeighbours(){
		return intersectList;
	}
	public Integer getCountNearestNeighbours(){
		return counter;
	}
	public Point getIntersectionPoint(){
		return intersection;
	}
	public String toString(){
		if(traceId != null)
			return "(" + traceId + ", " + counter + ")";
		else
			return "(" + intersection + ", " + counter + ")";
		
	}

	@Override
	public int compareTo(PTIntProcedure o) {
		return counter.compareTo(o.getCountNearestNeighbours());
	}
}
