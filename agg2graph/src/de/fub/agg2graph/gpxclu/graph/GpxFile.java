/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.graph;


import java.io.Serializable;

import javax.xml.stream.XMLStreamException;

import de.fub.agg2graph.gpxclu.core.Debug;
import de.fub.agg2graph.gpxclu.trace.*;

/**
 * Diese Klasse hilft kapselen vom laden einer Datei 
 * @author Simon
 *
 */
public class GpxFile implements Serializable {
	private static final long serialVersionUID = -7474480092893827621L;
	private Traces traces;
	public String filename;
	
	/**
	 * Load File from Hard Drive
	 * @param file Path to file
	 */
	public GpxFile(String file){		
		  this(file,false);	  
	}
	/**
	 * Lade GPX Datei vom jar
	 * @param file Path in Jar-File
	 * @param loadLocal 
	 */
	public GpxFile(String file, boolean loadLocal){		
		  try {
			  filename = file;
			  if (filename.contains("\\")) {
			      filename = filename.substring(filename.lastIndexOf("\\"), filename.length());			      
			  }
			  if(filename == "")
				  filename = file;
			  GpxLoader l = new GpxLoader(file, loadLocal);
			  traces = l.getTraces();
			  Debug.syso("File Name=" + filename);
			  Debug.syso("Number of Traces: " + traces.countDisplayedTraces() + "");	
		  } catch (Exception e) {
			  Debug.syso("Die GPX Datei konnte nicht eingelesen werden.");
		  }
		  	  
	}
	public GpxFile(){
		this("/GpxFiles/berlin.gpx", true);
	}
	
	public Traces getTraces(){
		return traces;
	}
}
