/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.graph;


import processing.core.*;
import de.fub.agg2graph.gpxclu.trace.Point;
import de.fub.agg2graph.gpxclu.trace.PtOpSphere;
import de.fub.agg2graph.gpxclu.trace.Trace;
import de.fub.agg2graph.gpxclu.trace.Traces;

public class DrawTraces {
	private GpxFile gpx;
	
	private MainGraph g;
	
	private int lineColor, lineWeight=1;
	
	private boolean paintTraces = true;
	
	public DrawTraces(MainGraph g, GpxFile gpx){
		this.gpx = gpx;
		this.g = g;
		lineColor = g.color(0,0,0);//g.color(255,255,255);
	}
	public DrawTraces(MainGraph g){
		this.g = g;
		lineColor = g.color(0,0,0);//g.color(255,255,255);
	}
	
	
	
	public GpxFile getGpx() {
		return gpx;
	}
	public void setGpx(GpxFile gpx) {
		this.gpx = gpx;
	}
	
	private void drawArrow(Point p1, Point p2){
		g.stroke(lineColor);
		g.strokeWeight(lineWeight);
		g.line(g.lon(p1), g.lat(p1), g.lon(p2), g.lat(p2));
		//line(x(p1), y(p1), x(p2), y(p2));
		/*
		pushMatrix();
			  translate(lon(p2), lat(p2));
			  //translate(x(p2), y(p2));
			  Double x = Math.toRadians(PtOpSphere.cardinalDirection(p1, p2));
			  rotate(x.floatValue());
			  strokeWeight(1);
			  noFill();			  
			  beginShape();
			  vertex(-3,3);
			  vertex(0,0);			  
			  vertex(3,3);
			  endShape();
		popMatrix();
		*/

	}
	public void setColor(int c){
		//System.out.println("Trace Color von " + lineColor + " zu " + c);
		lineColor = c;
	}
	public void setLineWeight(int w){
		//System.out.println("Line Weight of Trace von " + lineWeight + " zu " + w);
		lineWeight = w;
	}
	public void draw(){
		if(paintTraces){
			draw(gpx.getTraces());
			drawStartAndEndPointFromTrace(gpx.getTraces());
		}
	}
	public void draw(Traces traces){
		//Alle Spuren zeichnen
		if(traces == null)
			return;
		
		for(Trace t : traces){		  
			if(t.getSubTraces().size()>0){
				this.draw(t.getSubTraces());
				continue;
			}
			draw(t);
		}
	}
	public void draw(Trace t){	  
		if(t.size() < 2)
			return;
		
		Point p1 = null;
		
		for(Point p2 : t){
			if(p1 == null){
				p1 = p2;
				continue;
			}
			drawArrow(p1,p2);
			p1=p2;
		}
	}
	public void drawStartAndEndPointFromTrace(Traces traces){
		for(Trace t : traces){		  
			if(t.getSubTraces().size()>0){
				this.drawStartAndEndPointFromTrace(t.getSubTraces());
				continue;
			}
			if(t.size() < 2)
				continue;
			
			Point p1 = t.get(0);
			Point pn = t.get(t.size()-1);
			int boxSize = 3;
			g.rectMode(g.CENTER);
			g.stroke(g.color(0,0,0,255));
			g.fill(g.color(255,0,0));
			g.rect(g.lon(p1), g.lat(p1), boxSize, boxSize);
			g.fill(g.color(0,255,0));
			g.rect(g.lon(pn), g.lat(pn), boxSize, boxSize);
			g.rectMode(g.CORNER);
		}
	}
	public boolean isPaintTraces() {
		return paintTraces;
	}
	public void setPaintTraces(boolean paintTraces) {
		this.paintTraces = paintTraces;
	}
	
}
