/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.graph;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.BoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.*;
import org.jfree.ui.ApplicationFrame;
import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import de.fub.agg2graph.gpxclu.cluster.Clusters;

public class FormClusterAnalyse extends JPanel implements ActionListener {
	private static final long serialVersionUID = -1549718750919505376L;
	
	private Clusters clusters;
	
	private JTextField txtIterStart = new JTextField("     0"), txtIterEnd = new JTextField("     0");
	private JTextField txtCluStart = new JTextField("     0"), txtCluEnd = new JTextField("     0");
	
	private int iterStart = 0, iterEnd=0;
	private int cluStart = 0, cluEnd=0;
	
	public FormClusterAnalyse(){
		super(new BorderLayout());
		setAutoscrolls(true);
		init();
	}
	
	public void init(){
		
		this.removeAll();
		
		JPanel toolbar = new JPanel();
		JButton item = new JButton("Erstelle Graphen");
		item.setActionCommand("drawGraph");
		item.addActionListener(this);
		toolbar.add(item);
		toolbar.add(new JLabel("Zeige Cluster von "));
		toolbar.add(txtCluStart);
		toolbar.add(new JLabel(" bis "));
		toolbar.add(txtCluEnd);
		toolbar.add(new JLabel("Zeige Iteration von "));
		toolbar.add(txtIterStart);
		toolbar.add(new JLabel(" bis "));
		toolbar.add(txtIterEnd);
		add(toolbar, BorderLayout.NORTH);
		
		if(clusters != null){
			JPanel charts = new JPanel();
			GridLayout gbc = new GridLayout(0, 1);
			charts.setLayout(gbc);
			
			//charts.add(createXYPlot(createDatasetClusterSize()), gbc);
			charts.add(BarChart("Veränderung der Spurenanzahl in den Clustern",createDatasetBarClusterSize()), gbc);
			charts.add(BoxAndWhisker("Verteilung der Frechet-Distanzen je Cluster", createDatesetDistributionCluster()), gbc);
			charts.add(BoxAndWhisker("Verteilung der Frechet-Distanzen", createDatesetDistribution()), gbc);
			
			add(charts);
		}
	}
	
	public ChartPanel BoxAndWhisker(String title, BoxAndWhiskerCategoryDataset dataset) {

		JFreeChart chart = ChartFactory.createBoxAndWhiskerChart(title, "", "Fréchet-Distanz", dataset, true);
		//chart.set
		ChartPanel chartPanel = new ChartPanel(chart, true);
		chart.setBackgroundPaint(Color.white);
		// chartPanel.setPreferredSize(new java.awt.Dimension(450, 270));
		return chartPanel;

	}
	/**
	 * Creates a dataset with all edge length or trace length.
	 * 
	 * @return dataset.
	 */
	private BoxAndWhiskerCategoryDataset createDatesetDistributionCluster() {

		final DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();
		//Iteration Nummer = iNo
		for (int iNo = iterStart; iNo < iterEnd; iNo++){
			for(int cId=cluStart; cId < cluEnd; cId++){
				List<Double> list = clusters.getAllFrechetDistanceByClusterId(iNo, cId);
				//for(int i=0; i < list.size(); i++)
				//	list.set(i, Math.pow(list.get(i),2));
				if(iNo == 0)
					dataset.add(list, "Startpartition", "C. "+(cId+1));
				else
					dataset.add(list, "Iteration "+iNo, "C. "+(cId+1));
			}
		}
		
		return dataset;
	}
	
	private BoxAndWhiskerCategoryDataset createDatesetDistribution() {

		final DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();
		//Iteration Nummer = iNo
		List<Double> list = new LinkedList<Double>();
		for (int iNo = iterStart; iNo < iterEnd; iNo++){			
			for(int cId=cluStart; cId < cluEnd; cId++){
				List<Double> l = clusters.getAllFrechetDistanceByClusterId(iNo, cId);
				for(Double v : l){
					//list.add(Math.pow(v,2));
					list.add(v);
				}
			}
			dataset.add(list, "Iteration "+(iNo+1), "Clusters");
		}
		
		return dataset;
	}
	
	public XYSeriesCollection createDatasetClusterSize(){
		
		XYSeries series[] = new XYSeries[clusters.list.get(0).getSize()];
		//int cntCluster = clusters.list.get(0).getSize();
		for(int i=cluStart; i < cluEnd; i++){
			series[i] = new XYSeries("Cluster "+(i+1)+"");
			for(int j=iterStart; j < iterEnd; j++){				
				series[i].add(j, clusters.list.get(j).getTraces(i).size());
			}
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		for(int i=0; i < series.length; i++){			
			dataset.addSeries(series[i]);
		}
		return dataset;
	}
	public DefaultCategoryDataset createDatasetBarClusterSize(){
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		//int amount[] = new int[clusters.list.get(0).getSize()];
		int start;
		if(iterStart == 0){
			start = 1;
		}
		else{
			start = iterStart;
		}
		for(int i=cluStart; i < cluEnd; i++){
			for(int j=start; j < iterEnd; j++){
				double deleta = (double) clusters.list.get(j-1).getTraces(i).size() - clusters.list.get(j).getTraces(i).size();
				dataset.addValue(deleta, "Iteration "+(j), "C. "+(i+1));
			}
		}
		
		return dataset;
	}
	public ChartPanel BarChart(String title, CategoryDataset dataset) {
		 // create the chart...
       final JFreeChart chart = ChartFactory.createBarChart(
           title,         // chart title
           "",               // domain axis label
           "Anzahl",                  // range axis label
           dataset,                  // data
           PlotOrientation.VERTICAL, // orientation
           true,                     // include legend
           true,                     // tooltips?
           false                     // URLs?
       );
       chart.setBackgroundPaint(Color.white);
       ChartPanel chartPanel = new ChartPanel(chart);
       return chartPanel;

   }
	public ChartPanel createXYPlot(XYSeriesCollection dataset){
		
		JFreeChart chart2 = ChartFactory.createXYLineChart(
				"Line Chart Demo",
				"Iterationsschritt",
				"Anzahl Spuren",
				dataset,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);
		chart2.setBackgroundPaint(Color.white);
		ChartPanel chartPanel2 = new ChartPanel(chart2);
		
		return chartPanel2;
	}
	
	public Clusters getClusters() {
		return clusters;
	}

	public void setClusters(Clusters clusters) {
		this.clusters = clusters;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String actionCommand = arg0.getActionCommand();
		if(actionCommand == "drawGraph"){
			iterStart = Integer.valueOf(txtIterStart.getText().trim());
			iterEnd = Integer.valueOf(txtIterEnd.getText().trim());
			cluStart = Integer.valueOf(txtCluStart.getText().trim());
			cluEnd = Integer.valueOf(txtCluEnd.getText().trim());
			
			if(iterEnd >= clusters.list.size())
				iterEnd = clusters.list.size()-1;
			
			if(!(iterStart >= 0 & iterStart < clusters.list.size()))
				iterStart = 0;
			
			if(cluEnd >= clusters.list.get(0).getSize())
				cluEnd = clusters.list.get(0).getSize()-1;
			if(!(cluStart >= 0 & cluStart < clusters.list.get(0).getSize()))
				cluStart = 0;
			
			init();
		}
	}
	
}
