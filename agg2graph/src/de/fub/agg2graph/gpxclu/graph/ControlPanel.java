/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.graph;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import de.fub.agg2graph.gpxclu.merg.Grid;

import de.fub.agg2graph.gpxclu.cluster.ClusterTraces;
import de.fub.agg2graph.gpxclu.cluster.Export;
import de.fub.agg2graph.gpxclu.cluster.KMeans;

import de.fub.agg2graph.gpxclu.core.Debug;

import de.fub.agg2graph.gpxclu.trace.Trace;
import de.fub.agg2graph.gpxclu.trace.Traces;
import de.fub.agg2graph.gpxclu.trace.TrcOp;

/**
 * Diese Klasse beschreibt die ganze GUI. JMenuBar und JTabbedPanel.
 * Die ActinoListner wird in der Klasse ControlPanelActionListener abgehandelt.
 * Da aber die Klasse ControlPanelActionListener auf viele Ressourcen von dieser Klasse 
 * Zugreifen muss sind einige dinge auf public gesetzt - sollte mal geändert werden...
 * 
 * Die in Tabes dargestellten Formalere werden in anderen Klassen organisiert:
 *  - MainGraph = Map
 *  - FormTraceAnalyse = Spuren 
 *  - FormClusterAnalyse = Cluster
 * 
 * @author Simon Könnecke
 *
 */
public class ControlPanel extends JPanel{
	private static final long serialVersionUID = 7468863540376385070L;
	
	private JFrame jf;
	
	private JTabbedPane tabbedPanel;
	
	private MainGraph graph;
	
	private JTree tree;
	
	private static int noOfFields = 13;
	public JTextField txtFields[] = new JTextField[noOfFields];
	private JLabel lblList[] = new JLabel[noOfFields];
	
	private JCheckBoxMenuItem chkList[] = new JCheckBoxMenuItem[100];
	
	public KMeans.mergeType mergType = KMeans.mergeType.TRACE;
	
	private GpxFile gpx;
	
	private JMenuBar menuBar;
	private JMenu menuCluster;
	public FormClusterAnalyse formClusterAnalyse = new FormClusterAnalyse();
	public FormTracesAnalyse formTracesAnalyse;
	
	/**
	 * Das ist der Action Handler für alle Aktionen die in diesem Programm abgefeuert werden
	 * TODO: Daher sind in dieser Class auch paar Attribute auf public gesetzt. Könnte mal geändert werden.
	 */
	private ControlPanelActionListener actionHandler = new ControlPanelActionListener(this);
	
	/**
	 * Hier werden alle Logs im Programm auf die GUI transportiert.
	 * Vielleicht nicht der aller schönste Ansatz aber einfach :)
	 */
	public JScrollPane infoPane = new JScrollPane(Debug.rootLogger.txt);
	
	  // constructor
	public ControlPanel(JFrame _jf, GpxFile _gpx) {
		gpx = _gpx;
		jf = _jf;
		
		setBackground(new Color(180, 180, 220));
	    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	    jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
        createMenuBar();
        jf.add(menuBar, BorderLayout.NORTH);
        
	    
	    tabbedPanel();
	    
	    addKeyListener(actionHandler);
	    tabbedPanel.addKeyListener(actionHandler);
	    jf.addKeyListener(actionHandler);
	    
	}
	private void tabbedPanel(){
		tabbedPanel = new JTabbedPane();
		
		//Graphen erstellen
	    graph = new MainGraph(gpx, 1000,700);
	    graph.init();
	    graph.setup(); 
	    graph.redraw();
	    
        tabbedPanel.addTab("Map", graph);
        tabbedPanel.setMnemonicAt(0, KeyEvent.VK_1);
        
        
        formTracesAnalyse = new FormTracesAnalyse(gpx);
        tabbedPanel.addTab("Spuren", new JScrollPane(formTracesAnalyse));
        tabbedPanel.setMnemonicAt(1, KeyEvent.VK_2);
        
        tabbedPanel.addTab("Cluster", formClusterAnalyse);
        tabbedPanel.setMnemonicAt(2, KeyEvent.VK_3);
        
        //Add the tabbed pane to this panel.
        add(tabbedPanel);
	}
	private void createMenuBar() {
		menuBar = new JMenuBar();
		JMenu menu;
		JMenuItem item;
		/**
		 * Menu Punkt: Datei
		 */
		menu = new JMenu("Datei");
	    
	    item = new JMenuItem("GPX-Datei öffnen");
        item.addActionListener(actionHandler);
        item.setActionCommand("loadFile");
        menu.add(item);
        
        menu.addSeparator();
        
        item = new JMenuItem("Erstelle Restore-Datei");
        item.setActionCommand("clusterExport2Ser");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        item = new JMenuItem("Öffne Restore-Datei");
        item.setActionCommand("clusterImportFromSer");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        menu.addSeparator();
        
        item = new JMenuItem("Rückgängig");
        item.setActionCommand("redo");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        menuBar.add(menu);
        
        /**
		 * Menu Punkt: Graph
		 */
        menu = new JMenu("Graph");
	    
	    item = new JMenuItem("Graph speichern");
        item.addActionListener(actionHandler);
        item.setActionCommand("saveGraphToFile");
        menu.add(item);
        item = new JMenuItem("Redraw Graph");
        item.addActionListener(actionHandler);
        item.setActionCommand("redraw");
        menu.add(item);
        
        item = new JMenuItem("Traces anzeigen/verbergen");
        item.addActionListener(actionHandler);
        item.setActionCommand("togglePaintTraces");
        menu.add(item);
        
        item = new JMenuItem("Zeige Traces oder Cluster an");
        item.addActionListener(actionHandler);
        item.setActionCommand("togglePaintModeGraph");
        menu.add(item);
        
        item = new JMenuItem("Spuren exportieren");
        item.addActionListener(actionHandler);
        item.setActionCommand("tracesExport");
        menu.add(item);
        
        menuBar.add(menu);
        
        /**
		 * Menu Punkt: Spuren Anzeigen
		 */
		menu = new JMenu("Spuren");
	    
		tree = new JTree(createNodes());
        JScrollPane treeView = new JScrollPane(tree);
        treeView.setPreferredSize(new Dimension(400, 600));
        menu.add(treeView);
        
        menuBar.add(menu);
		
        
        /**
		 * Menu Punkt: Spuren
		 */
        menu = new JMenu("Spur-Operation");
	    
        item = new JMenuItem("Fläche verkleinern");
        item.setActionCommand("resizePlain");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 0, "Fläche verkleinern [Grad]:", "0.0004");
        
        item = new JMenuItem("Durchtrenne Kante");
        item.setActionCommand("cutTraceByDistance");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 1, "bei einer Kantenlänge von [Meter]", "500");
        
        item = new JMenuItem("Durchtrenne Spure");
        item.setActionCommand("splitTraceAfterDistance");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 2, "nach einer Länge von [Meter]", "500");
        
        item = new JMenuItem("Vereinfache die Spuren");
        item.setActionCommand("simplifyTraces");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 3, "Vereinfachnungstoleranz [Meter]", "200");
        
        item = new JMenuItem("Teile die Spure");
        item.setActionCommand("splitTraceByDouglasPeucker");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 4, "bei einem XTA [Meter]", "200");
        
        item = new JMenuItem("Spure löschen");
        item.setActionCommand("deleteTraceWithADistancLess");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 12, "kleienr als [Meter]", "5");
        
        menuBar.add(menu);
        
        /**
		 * Menu Punkt: Kreuzungen
		 */
        menu = new JMenu("Kreuzungen");
	     
        item = new JMenuItem("Kreuzungen trennen");
        item.setActionCommand("splitByIntersection");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 7, "Anzahl der Iteration:", "0");
        
        //item = new JCheckBoxMenuItem("Kreuzungen anzeigen");
        item = new JMenuItem("Kreuzungen anzeigen/verbergen");
        item.setActionCommand("toggleIntersectionDisplay");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        
        
        menuBar.add(menu);
        
        /**
		 * Menu Punkt: Dichtegitter
		 */
        menu = new JMenu("Gitter");
        
        item = new JMenuItem("Dichtegitter");
        item.setActionCommand("tracesTogrid");
        item.addActionListener(actionHandler);
        menu.add(item);
        createTxtField(menu, 5, "Größe der Zelle [Meter]:", "10");
        
        item = new JMenuItem("Dichtegitter anzeigen/verbergen");
        item.setActionCommand("toggleDensityGridDisplay");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        item = new JMenuItem("Gitter anzeigen/verbergen");
        item.setActionCommand("toggleGridDisplay");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        item = new JMenuItem("Dichtefarbe anzeigen/verbergen");
        item.setActionCommand("toggleDensityColorDisplay");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        item = new JMenuItem("Dichtanzahl anzeigen/verbergen");
        item.setActionCommand("toggleDensityNumberDisplay");
        item.addActionListener(actionHandler);
        menu.add(item);
        
        menuBar.add(menu);
        
        
        /**
		 * Menu Punkt: Cluster
		 */
        createMenuCluster();
        menuBar.add(menuCluster);
        
        /**
		 * Menu Punkt: Log
		 */
        menu = new JMenu("Info");
        
        Debug.rootLogger.txt.setEditable(false);
        menu.add(infoPane);
        
        menuBar.add(menu);
        
		
	}
	
	private void createMenuCluster(){
		menuCluster  = new JMenu("Cluster");
		JMenuItem item;
		
		ButtonGroup mergType = new ButtonGroup();
		JRadioButtonMenuItem rbMenuItem = new JRadioButtonMenuItem("Spuren punktuell aggregieren");
		rbMenuItem.setSelected(true);
		rbMenuItem.setActionCommand("clusterTracesWithTraceMerg");
		rbMenuItem.addActionListener(actionHandler);
		mergType.add(rbMenuItem);
		menuCluster.add(rbMenuItem);

		rbMenuItem = new JRadioButtonMenuItem("Spuren mit einem Gitter aggregieren");
		rbMenuItem.setMnemonic(KeyEvent.VK_O);
		rbMenuItem.setActionCommand("clusterTracesWithTraceGrid");
		rbMenuItem.addActionListener(actionHandler);
		mergType.add(rbMenuItem);
		menuCluster.add(rbMenuItem);
		
		menuCluster.addSeparator();
		
		createTxtField(menuCluster, 10, "Anzahl der Iteration", "20");
		item = new JMenuItem("Cluster berechnen");
        item.setActionCommand("clusterTraces");
        item.addActionListener(actionHandler);
        menuCluster.add(item);
        createTxtField(menuCluster, 6, "Anzahl Cluster", "5");
        
        item = new JMenuItem("Cluster berechnen");
        item.setActionCommand("clusterTracesGrid");
        item.addActionListener(actionHandler);
        menuCluster.add(item);
        createTxtField(menuCluster, 8, "Spalte", "6");
        createTxtField(menuCluster, 9, "Zeile", "5");
        
        
        item = new JMenuItem("Cluster anzeigen/verbergen");
        item.setActionCommand("togglePaintModeGraph");
        item.addActionListener(actionHandler);
        menuCluster.add(item);
        
        menuCluster.add(new JSeparator());
        
        item = new JMenuItem("Speicher Cluster als .png");
        item.setActionCommand("saveClusterToFiles");
        item.addActionListener(actionHandler);
        menuCluster.add(item);
        
        menuCluster.add(new JSeparator());
        
        item = new JMenuItem("Exportiere letzte Iteration zu einer GPX-Datei");
        item.setActionCommand("clusterExport");
        item.addActionListener(actionHandler);
        menuCluster.add(item);
        
        menuCluster.add(new JSeparator());
        
        item = new JMenuItem("Zeige Iteration");
        item.setActionCommand("toggleClusterFirstOrSecond");
        item.addActionListener(actionHandler);
        menuCluster.add(item);
        createTxtField(menuCluster, 11, "Nummer:", "4");
        
       
        
        
    	for(int i=0; i < chkList.length; i++){
    		createCheckbox(menuCluster, i, (i+1)+". Cluster");
    	}
	        
        
	}
	private void createCheckbox(JMenu menu, int index, String label){
		final Dimension dim = new Dimension(150,15);
		chkList[index] = new JCheckBoxMenuItem(label, false);
		chkList[index].setPreferredSize(dim);
		chkList[index].setActionCommand("toggleCluster_"+index);
		chkList[index].addActionListener(actionHandler);
		chkList[index].setVisible(false);
		chkList[index].setSelected(true);
        menu.add(chkList[index]);
                
	}
	/**
	 * Die Funktion erstellt 
	 */
	public void setClusterCheckbox(){
		try{
	        Traces c = graph.getDrawCluster().getCluster().getCentroid();
	        if(graph.getDrawCluster().getCluster().getCentroid().size() > 0){
	        	for(int i=0; i < c.size(); i++){
	        		c.get(i).setDisplay(chkList[i].isSelected());
	        		chkList[i].setVisible(true);
	        	}
	        	for(int i=c.size(); i < chkList.length; i++){
	        		chkList[i].setVisible(false);
	        	}
	        }
        }
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void createTxtField(JMenu menu, int index, String label, String defaultValue){
		JPanel p = new JPanel();
		
		final Dimension dim = new Dimension(150,15);
		lblList[index] = new JLabel(label);
		lblList[index].setPreferredSize(dim);
		lblList[index].setMaximumSize(dim);
		p.add(lblList[index]);
        txtFields[index] = new JTextField(defaultValue, 10);
        txtFields[index].setPreferredSize(dim);
        txtFields[index].setMaximumSize(dim);
        lblList[index].setLabelFor(txtFields[index]);        
        p.add(txtFields[index]);
        p.setAlignmentX(10);        
        menu.add(p);        
	}
	
	private DefaultMutableTreeNode createNodes(){
		DefaultMutableTreeNode top = new DefaultMutableTreeNode("Alle Traces");
		createNodes(top, gpx.getTraces());
		return top;
	}
	private void createNodes(DefaultMutableTreeNode top, Traces traces){
		if(traces == null)
			return;
	     for(Trace t : traces){
	    	if(t.getSubTraces().size() > 0){
	    		DefaultMutableTreeNode cat = new DefaultMutableTreeNode(t);
	    		top.add(cat);
	    		createNodes(cat, t.getSubTraces());
	    	}
	    	else{
	    		top.add(new DefaultMutableTreeNode(t));
	    	}
	    }
	}
	public void reloadTree(){
		tree.setModel(new DefaultTreeModel(createNodes()));
		((DefaultTreeModel) tree.getModel()).reload();
		tree.revalidate();
		tree.repaint();
		tree.updateUI();
		tree.setVisible(false);
		tree.setVisible(true);
		//tree.setViewportView(taskDataTree);		
	}
	public void repaint(){
		try{
			reloadTree();
			graph.redraw();
			//formTracesAnalyse.repaint();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public JFrame getJf() {
		return jf;
	}
	public void setJf(JFrame jf) {
		this.jf = jf;
	}
	public MainGraph getGraph() {
		return graph;
	}
	public void setGraph(MainGraph graph) {
		this.graph = graph;
	}
	public GpxFile getGpx() {
		return gpx;
	}
	public void setGpx(GpxFile gpx) {
		this.gpx = gpx;
	}
	public KMeans.mergeType getMergType() {
		return mergType;
	}
	public void setMergType(KMeans.mergeType mergType) {
		this.mergType = mergType;
	}
	
	
	
}
