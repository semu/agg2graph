package de.fub.agg2graph.gpxclu.graph;

import java.awt.Cursor;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Semaphore;

import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JTextField;

import de.fub.agg2graph.gpxclu.cluster.ClusterTraces;
import de.fub.agg2graph.gpxclu.cluster.Clusters;
import de.fub.agg2graph.gpxclu.cluster.Export;
import de.fub.agg2graph.gpxclu.cluster.ExportStructur;
import de.fub.agg2graph.gpxclu.cluster.KMeans;
import de.fub.agg2graph.gpxclu.core.Debug;
import de.fub.agg2graph.gpxclu.merg.Grid;
import de.fub.agg2graph.gpxclu.trace.Trace;
import de.fub.agg2graph.gpxclu.trace.Traces;
import de.fub.agg2graph.gpxclu.trace.TrcOp;

public class ControlPanelActionListener implements
		ActionListener, Runnable, KeyListener {
	private static final long serialVersionUID = -1611804679006910374L;
	private ControlPanel parentForm;
	private GpxFile gpx;
	private JFrame jf;
	private MainGraph graph;
	private ActionEvent action;
	private Thread thread;
	private Semaphore mutex = new Semaphore(1);
	
	ControlPanelActionListener(ControlPanel frm){
		parentForm = frm;		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			mutex.acquire();
				action = arg0;
				gpx = parentForm.getGpx();
				jf = parentForm.getJf();
				graph = parentForm.getGraph();
				thread = new Thread(this);
				thread.start();
				jf.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				parentForm.infoPane.requestFocusInWindow();
				
				JScrollBar vertical = parentForm.infoPane.getVerticalScrollBar();
				vertical.setValue( vertical.getMaximum() );
		} catch (InterruptedException e) {
			Debug.syso("Action konnte nicht ausgeführt werden: " + action.getActionCommand());
		}
			
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_ESCAPE){
			if(thread != null && thread.isAlive()){
				Debug.syso("Interrupt by User");
				thread.interrupt();
				jf.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				mutex.release();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
	
	@Override
	public void run() {
		Debug.syso("----------------------");
		Debug.syso("-- Nächte Operation --");
		Debug.syso("----------------------");
		
		String actionCommand = action.getActionCommand();
		long cntDispTraces = gpx.getTraces().countDisplayedTraces();
		long cntDispPoints = gpx.getTraces().countPoints();
		
		if(actionCommand == "loadFile"){
			Debug.syso("Load new File");
			FileDialog dlg=null;

			dlg=new FileDialog(jf,"Select File to Load",FileDialog.LOAD);
			dlg.setFile("*.gpx");
			dlg.setVisible(true);
			String filename = dlg.getDirectory() + dlg.getFile();
			
			if (filename != null) {
				Debug.syso("Lade GPX-Datei: " + filename + ".");
				try{
					gpx = new GpxFile(filename, false);
					parentForm.reloadTree();
					graph.setGpxFile(gpx);
					Debug.syso(filename);
				}
				catch (Exception e) {
					Debug.syso("Fehler beim Laden der neuen GPX-Datei.");
				}
			}		        
		}
		else if(actionCommand == "clusterImportFromSer"){
			Debug.syso("Lade Clusters Restore-File");
			FileDialog dlg=null;

			dlg=new FileDialog(jf,"Select File to Load",FileDialog.LOAD);
			dlg.setFile("*.ser");
			dlg.setVisible(true);
			String filename = dlg.getDirectory() + dlg.getFile();
			
			if (filename != null) {
				Debug.syso("Lade SER-Datei: " + filename + ".");
				try{
					ExportStructur ex = Export.SerToClusters(filename);
					if(ex == null)
						throw new Exception();
					gpx = ex.gpx;
					graph.setGpxFile(gpx);
					graph.getDrawCluster().setClusters(ex.clusters);
					parentForm.formClusterAnalyse.setClusters(ex.clusters);
					graph.getDrawTraces().setGpx(gpx);
					parentForm.formTracesAnalyse.setGpx(gpx);
					Debug.rootLogger.txt.setText(ex.log);
					parentForm.repaint();
				}
				catch (Exception e) {
					Debug.syso("Fehler beim Laden der Clusters Restore-Datei.");
				}
				finally{
					Debug.syso("Import Restore-Datei abgeschlossen.");
				}
			}		        
		}
		
		else if(actionCommand == "saveGraphToFile"){
			Debug.syso("Load new File");
			FileDialog dlg=null;

			dlg=new FileDialog(jf,"Save Graph to File",FileDialog.SAVE);
			dlg.setFile("*.png");
			dlg.setVisible(true);
			String filename = dlg.getDirectory() + dlg.getFile();
			
			if (filename != null) {
				Debug.syso("Save Graph to: " + filename + ".");
				try{
					graph.save(filename);					
				}
				catch (Exception e) {
					Debug.syso("Fehler beim Speichern vom Graphen.");
				}
			}		        
		}
		else if(actionCommand == "saveClusterToFiles"){
			Debug.syso("Load new File");
			FileDialog dlg=null;

			dlg=new FileDialog(jf,"Choose Directoy for the Files",FileDialog.SAVE);
			dlg.setFile("test.png");
			dlg.setVisible(true);
			String dir = dlg.getDirectory();
			graph.getDrawCluster().saveToFile(dir);
			        
		}
		else if(actionCommand == "clusterExport"){
			Debug.syso("Export Cluster to GPX File");
			FileDialog dlg=null;
			dlg=new FileDialog(jf,"Choose Directoy for the File",FileDialog.SAVE);
			dlg.setFile("cluster.gpx");
			dlg.setVisible(true);
			String dir = dlg.getDirectory() + dlg.getFile();
			Export.ClustersToGpx(graph.getDrawCluster().getCluster(), dir);
			        
		}
		else if(actionCommand == "clusterExport2Ser"){
			Debug.syso("Export Cluster to Ser File");
			FileDialog dlg=null;
			dlg=new FileDialog(jf,"Choose Directoy for the File",FileDialog.SAVE);
			dlg.setFile("cluster.ser");
			dlg.setVisible(true);
			String dir = dlg.getDirectory() + dlg.getFile();
			Export.ClustersToSer(gpx, graph.getDrawCluster().getClusters(), dir);       
		}
		else if(actionCommand == "tracesExport"){
			Debug.syso("Export Cluster to GPX File");
			FileDialog dlg=null;
			dlg=new FileDialog(jf,"Choose Directoy for the File",FileDialog.SAVE);
			dlg.setFile("traces.gpx");
			dlg.setVisible(true);
			String dir = dlg.getDirectory() + dlg.getFile();
			Export.TraceToGpx(gpx.getTraces(), dir);
			        
		}		
		else if(actionCommand == "simplifyTraces"){
			double tol = Double.valueOf(parentForm.txtFields[3].getText());
			TrcOp.reduction(gpx.getTraces(), tol);
			Debug.syso("Reduce Points on Traces");
			parentForm.repaint();
		}
		else if(actionCommand == "splitTraceByDouglasPeucker"){
			double tol = Double.valueOf(parentForm.txtFields[4].getText());
			TrcOp.splitTraceByDouglasPeucker(gpx.getTraces(), tol);
			Debug.syso("Splite Trace with Douglas Peucker");
			parentForm.repaint();
		}			
		else if(actionCommand == "resizePlain"){
			double tol = Double.valueOf(parentForm.txtFields[0].getText());
			TrcOp.resizePlain(gpx.getTraces(), tol);
			Debug.syso("Resize Plain");
			parentForm.repaint();
		}
		else if(actionCommand == "cutTraceByDistance"){
			double tol = Double.valueOf(parentForm.txtFields[1].getText());
			TrcOp.splitTraceByDistance(gpx.getTraces(), tol);
			Debug.syso("Cut Trace by Distance");
			parentForm.repaint();
		}
		else if(actionCommand == "splitTraceAfterDistance"){
			double tol = Double.valueOf(parentForm.txtFields[2].getText());
			TrcOp.splitTraceAfterDistance(gpx.getTraces(), tol);
			Debug.syso("Split Trace after Distance");
			parentForm.repaint();
		}			
		else if(actionCommand == "splitByIntersection"){
			//double tol = Double.valueOf(txtFields[5].getText());
			int no = Integer.valueOf(parentForm.txtFields[7].getText());
			graph.setIntersections(TrcOp.getIntersections(gpx.getTraces(), 0.0015, no));
			parentForm.repaint();
	        Debug.syso("Repaint");
		}else if(actionCommand == "deleteTraceWithADistancLess"){
			
			double tol = Double.valueOf(parentForm.txtFields[12].getText());
			Debug.syso("Delete Trace with a distance less than " + tol);
			TrcOp.deleteTraceWithADistancLess(gpx.getTraces(), tol);
			parentForm.repaint();
		}
		else if(actionCommand == "clusterTraces"){
			Debug.syso("Cluster");
			Traces tmp = TrcOp.getTraces(gpx.getTraces());
			int k = Integer.valueOf(parentForm.txtFields[6].getText());
			int no = Integer.valueOf(parentForm.txtFields[10].getText());
			
			Debug.syso("Cluster mit der Startpartition von " + k + " zufälligen Spuren und der maxmimalen Iteration von " + no);
			ClusterTraces cltr = new KMeans(k, tmp, no, parentForm.mergType);
			cltr.run();
			graph.getDrawCluster().setClusters(cltr.getClusters());
			parentForm.formClusterAnalyse.setClusters(cltr.getClusters());
			
			graph.setPaintMode(MainGraph.paintModeOption.Cluster);
			//Config JMenuBar
			parentForm.setClusterCheckbox();
			parentForm.repaint();
		}
		else if(actionCommand == "clusterTracesWithTraceMerg"){
			Debug.syso("Spuren zusammenführen mit der TRACE Methode.");
			parentForm.mergType = KMeans.mergeType.TRACE;
		}
		else if(actionCommand == "clusterTracesWithTraceGrid"){
			Debug.syso("Spuren zusammenführen mit der GRID Methode.");
			parentForm.mergType = KMeans.mergeType.GRID;
		}
		else if(actionCommand == "clusterTracesGrid"){
			
			Traces tmp = TrcOp.getTraces(gpx.getTraces());
			int r = Integer.valueOf(parentForm.txtFields[8].getText());
			int c = Integer.valueOf(parentForm.txtFields[9].getText());
			int no = Integer.valueOf(parentForm.txtFields[10].getText());
			Debug.syso("Cluster mit der Startpartition eines Gitters (" + r + "," + c + ") und der maxmimalen Iteration von " + no);
			ClusterTraces cltr = new KMeans(r, c, tmp, no, parentForm.getMergType());
			cltr.run();
			graph.getDrawCluster().setClusters(cltr.getClusters());
			parentForm.formClusterAnalyse.setClusters(cltr.getClusters());
			graph.getDrawCluster().setPaintIteration((no)-1);
			parentForm.txtFields[11].setText(String.valueOf(no));
			graph.setPaintMode(MainGraph.paintModeOption.Cluster);
			//Config JMenuBar
			parentForm.setClusterCheckbox();
			parentForm.repaint();
		}
		else if(actionCommand == "tracesTogrid"){
			Debug.syso("Trace to Grid");
			int k = Integer.valueOf(parentForm.txtFields[5].getText());
			graph.setGrid(new Grid(gpx.getTraces(), k));				
			parentForm.repaint();
		}
		else if(actionCommand == "redo"){
			Debug.syso("Wiederrufe letzten Filter.");				
			TrcOp.redo(gpx);
			parentForm.repaint();
		}
		else if(actionCommand == "toggleIntersectionDisplay"){
			Debug.syso("Toggle Intersection Display");
			graph.setPaintIntersections(!graph.isPaintIntersections());
			graph.redraw();
		}
		else if(actionCommand == "toggleDensityGridDisplay"){
			Debug.syso("Toggle Density Grid Display");
			graph.setPaintGrid(!graph.isPaintGrid());
			graph.redraw();
		}
		else if(actionCommand == "toggleDensityColorDisplay"){
			Debug.syso("Toggle Density Color Display");
			graph.getGridGraph().setPaintDensity(!graph.getGridGraph().isPaintDensity());
			graph.redraw();
		}
		else if(actionCommand == "toggleDensityNumberDisplay"){
			Debug.syso("Toggle Density NumberDisplay");
			graph.getGridGraph().setPaintNumbers(!graph.getGridGraph().isPaintNumbers());
			graph.redraw();
		}
		else if(actionCommand == "toggleGridDisplay"){
			Debug.syso("Toggle Density Grid Grid Display");
			graph.getGridGraph().setPaintGrid(!graph.getGridGraph().isPaintGrid());
			graph.redraw();
		}
		else if(actionCommand == "togglePaintTraces"){
			Debug.syso("Toggle Density Grid Grid Display");
			graph.getDrawTraces().setPaintTraces(!graph.getDrawTraces().isPaintTraces());
			graph.redraw();
		}
		else if(actionCommand == "toggleClusterFirstOrSecond"){
			Debug.syso("Toggle between Iteration from Clustering");
			int k = Integer.valueOf(parentForm.txtFields[11].getText());
			k -= 1;//Der Index wird mit Null begonnen und nicht mit eins...
			if(k < 0){
				parentForm.txtFields[11].setText("0");
				k=0;
			}
			else if(k >= graph.getDrawCluster().getClusters().size()){
				parentForm.txtFields[11].setText(String.valueOf(graph.getDrawCluster().getClusters().size()));
				k = graph.getDrawCluster().getClusters().size()-1;
			}
			graph.getDrawCluster().setPaintIteration(k);
			parentForm.setClusterCheckbox();
			graph.redraw();
			
		}
		else if(actionCommand == "redraw"){
			Debug.syso("Karte wird neu gezeichnet.");
			graph.redraw();
		}
		else if(actionCommand == "togglePaintModeGraph"){
			
			if(graph.getPaintMode() == MainGraph.paintModeOption.Cluster){
				graph.setPaintMode(MainGraph.paintModeOption.Traces);
				Debug.syso("Wechsel Kartenansicht von Spuren.");
			}
			else{
				graph.setPaintMode(MainGraph.paintModeOption.Cluster);
				Debug.syso("Wechsel Kartenansicht von Clustern.");
			}
			
			graph.redraw();
		}
		else{
			if(actionCommand.startsWith("toggleCluster_")){
				int clusterId = Integer.valueOf(actionCommand.replace("toggleCluster_", ""));
				System.out.println("toggleCluster: " + clusterId);
				Trace c = graph.getDrawCluster().getCluster().getCentroid(clusterId);
				c.setDisplay(!c.isDisplay());
				graph.redraw();
			}
			else{
				System.out.println("Event: " + actionCommand);
			}
		}
		Debug.syso("Analyse: Es wurden vorher " + cntDispTraces + " Spuren mit insgesamt " + cntDispPoints + " Punkten angezeigt.");
		Debug.syso("Nach der Operation wurden " + gpx.getTraces().countDisplayedTraces() + " Spuren mit insgesamt " + gpx.getTraces().countPoints() + " Punkten angezeigt.");
		//Bahn freimachen für die nächste Aktion
		jf.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		mutex.release();
	}

	

}
