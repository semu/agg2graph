/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.merg;

import de.fub.agg2graph.gpxclu.trace.Traces;

public class TracesMergeGrid {
	private Traces t;
	
	private Grid g;
	
	public void set(Traces t) {
		this.t = t;		
	}

	public boolean run() {
		
		return true;
	}

	public Traces get() {
		return t;
	}
	
}
