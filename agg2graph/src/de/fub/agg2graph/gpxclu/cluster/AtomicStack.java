/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.cluster;

import java.util.Stack;
import java.util.concurrent.Semaphore;

public class AtomicStack {
	class Entry{
		public Integer centroidId, traceId;
		public Entry(Integer centroidId, Integer traceId){
			this.centroidId = centroidId;
			this.traceId = traceId;
		}
	}
	private Stack<Entry> stack;
	private Semaphore s;
	
	public AtomicStack(){
		stack = new Stack<Entry>();
		s = new Semaphore(1);
	}
	
	public void push(Integer centroidId, Integer traceId){
		s.acquireUninterruptibly();
		stack.add(new Entry(centroidId, traceId));
		s.release();
	}
	public int[] pop(){
		s.acquireUninterruptibly();
		Entry e = stack.pop();
		int[] t = new int[]{e.centroidId, e.traceId};
		s.release();
		return t;
	}
	public boolean empty(){
		s.acquireUninterruptibly();
		boolean t = stack.empty();
		s.release();
		return t;
	}
	
	
}
