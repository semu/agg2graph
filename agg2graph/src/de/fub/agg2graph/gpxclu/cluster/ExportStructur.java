package de.fub.agg2graph.gpxclu.cluster;
/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
import java.io.Serializable;

import de.fub.agg2graph.gpxclu.graph.GpxFile;

/**
 * 
 * @author Simon Könnecke
 *
 */
public class ExportStructur implements Serializable {
	private static final long serialVersionUID = 1686643463434137822L;
	public Clusters clusters;
	public GpxFile gpx;
	public String log;
}
