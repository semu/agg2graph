/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.cluster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.fub.agg2graph.gpxclu.trace.Trace;

public class Clusters implements Serializable{
	private static final long serialVersionUID = -9037595377060939452L;
	public ArrayList<Cluster> list = new ArrayList<Cluster>();
	public ArrayList<EpsilonTable> eplisonTable = new ArrayList<EpsilonTable>();
	public String log;
	public int size(){
		return list.size();
	}
	/**
	 * Liefert ein Liste aller Frechét Distanz vom Cluster cId von der Iteratation iNo
	 * @param iNo Iterationsnummer
	 * @param cId Clusternummer
	 * @return List aller Frechét Distanz vom Cluster ID in der Iteration iNo
	 */
	public List<Double> getAllFrechetDistanceByClusterId(int iNo, int cId){
		if(iNo < eplisonTable.size())
			return eplisonTable.get(iNo).get(cId);
		else{
			return new LinkedList<Double>();
		}
	}
	
	public List<Trace> getAllTraceIdsByClusterId(int iNo, int cId){
		if(iNo < eplisonTable.size())
			return list.get(iNo).getAllTraces(cId);
		else{
			return new LinkedList<Trace>();
		}
	}
	
}
