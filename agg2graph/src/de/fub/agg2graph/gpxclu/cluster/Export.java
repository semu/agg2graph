/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.cluster;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import de.fub.agg2graph.gpxclu.core.Debug;
import de.fub.agg2graph.gpxclu.graph.GpxFile;
import de.fub.agg2graph.gpxclu.trace.Point;
import de.fub.agg2graph.gpxclu.trace.Trace;
import de.fub.agg2graph.gpxclu.trace.Traces;

public class Export {
	public static void TraceToGpx(Traces t, String filename){
		try {
			File file = new File(filename);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write("<?xml version='1.0' encoding='UTF-8'?>\r\n"+
						"<gpx version=\"1.1\" creator=\"Cluster GPS Traces\" xmlns=\"http://www.topografix.com/GPX/1/1\"\r\n"+
							"\t  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n"+ 
							"\t  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\r\n");
			
			traceToFile(bw, t);
			bw.write("</gpx>\r\n");
			bw.close();
 
			System.out.println("Cluster stored to file: " + filename);
 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void traceToFile(BufferedWriter bw, Traces traces) throws IOException{
		for(Trace t : traces){
			if(t.getSubTraces().size() > 0){
				traceToFile(bw, t.getSubTraces());
				continue;
			}
			bw.write("\t<trk>\r\n");
			bw.write(traceToString(t));			
			bw.write("\t</trk>\r\n");				
		}
	}
	
	public static void ClustersToGpx(Cluster c, String filename){
		try {
			File file = new File(filename);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write("<?xml version='1.0' encoding='UTF-8'?>\r\n"+
						"<gpx version=\"1.1\" creator=\"Cluster GPS Traces\" xmlns=\"http://www.topografix.com/GPX/1/1\"\r\n"+
							"\t  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n"+ 
							"\t  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\r\n");
			
			for(int i=0; i<c.getSize(); i++){
				bw.write("\t<trk>\r\n");
				bw.write(traceToString(c.getCentroid(i)));
				Traces traces = c.getTraces(i);
				for(Trace t: traces){
					bw.write(traceToString(t));
				}
				bw.write("\t</trk>\r\n");				
			}
			bw.write("</gpx>\r\n");
			bw.close();
 
			System.out.println("Cluster stored to file: " + filename);
 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void ClustersToSer(GpxFile gpx, Clusters c, String filename){

	    try{
	      //use buffering
	      OutputStream file = new FileOutputStream(filename);
	      OutputStream buffer = new BufferedOutputStream( file );
	      ObjectOutput output = new ObjectOutputStream( buffer );
	      try{
	    	ExportStructur ex = new ExportStructur();
	    	ex.log = Debug.rootLogger.txt.getText();
	    	ex.clusters = c;
	    	ex.gpx = gpx;
	        output.writeObject(ex);
	      }
	      finally{
	        output.close();
	        Debug.syso("Export Clusters to " + filename + " success.");
	      }
	    }  
	    catch(IOException ex){
	    	Debug.syso("Export Clusters to " + filename + " failed.");
	    }
	}
	
	public static ExportStructur SerToClusters(String filename){
		ExportStructur recoveredClusters=null;
		try{
	      //use buffering
	      InputStream file = new FileInputStream(filename);
	      InputStream buffer = new BufferedInputStream( file );
	      ObjectInput input = new ObjectInputStream ( buffer );
	      try{
	        recoveredClusters = (ExportStructur)input.readObject();
	      }
	      finally{
	        input.close();
	        Debug.syso("Import Clusters to " + filename + " success.");
	        Debug.syso("#####################################################");
	        Debug.syso("###### Revovered Clusters Log START ################");
	        Debug.syso("#####################################################");
	        Debug.syso(recoveredClusters.log);
	        Debug.syso("#####################################################");
	        Debug.syso("###### Revovered Clusters Log END  ##################");
	        Debug.syso("#####################################################");
	      }
	    }
	    catch(ClassNotFoundException ex){
	    	Debug.syso("Import Clusters to " + filename + " failed (Class not found).");
	    }
	    catch(IOException ex){
	    	Debug.syso("Import Clusters to " + filename + " failed (IO Issues).");
	    }
		return recoveredClusters;
	}
	public static String traceToString(Trace t){
		String str = ("\t\t<trkseg>\r\n");
		for(Point p: t){
			str += ("\t\t\t<trkpt lat=\"" + p.getLat() + "\" lon=\"" + p.getLon() + "\"></trkpt>\r\n");
		}
		str += ("\t\t</trkseg>\r\n");
		return str;
	}
	
}