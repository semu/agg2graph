/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.cluster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.fub.agg2graph.gpxclu.trace.Trace;
import de.fub.agg2graph.gpxclu.trace.Traces;

public class Cluster implements Iterable<Traces>, Serializable{
	private static final long serialVersionUID = -4393397942416790527L;
	private Map<Integer, Traces> cluster;
	/**
	 * centroid sind die Zentren der Cluster
	 */	
	private ArrayList<Trace> centroid;
	private Traces centroidTraces;
	/**
	 * Anzahl der Cluster
	 */
	private int size = -1;
	
	public Cluster(int k){
		size = k;
		cluster = new HashMap<Integer, Traces>();
		centroid = new ArrayList<Trace>();
	}
	public void putTraces(Integer clusterId, Trace t){
		if(cluster.containsKey(clusterId)){
			cluster.get(clusterId).addTrace(t);
		}
		else{
			Traces tmp = new Traces();
			tmp.addTrace(t);
			cluster.put(clusterId, tmp);
		}
	}
	
	public Traces getTraces(Integer clusterId){
		return cluster.get(clusterId);
	}
	
	public void putCentroid(Integer clusterId, Trace t){
		centroid.add(clusterId, t);
	}
	public Trace getCentroid(Integer clusterId){
		return centroid.get(clusterId);
	}
	public Traces getCentroid(){
		if(centroidTraces == null || centroidTraces.size() != centroid.size()){
			centroidTraces = new Traces();
			for(Trace t : centroid){
				centroidTraces.addTrace(t);
			}
		}
		return centroidTraces;
	}
	@Override
	public Iterator<Traces> iterator() {
		Collection<Traces> c = cluster.values();
	    Iterator<Traces> itr = c.iterator();
		return itr;
	}
	
	public ArrayList<Trace> iteratorCentroid(){
		return centroid;
	}

	
	public int getSize() {
		if(size == -1)
			setSize(centroid.size());
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public List<Trace> getAllTraces(int cId) {
		Traces t = getTraces(cId);
		List<Trace> l = new ArrayList<Trace>(t.size());
		for(Trace tmp : t){
			l.add(tmp);
		}
		return l;
	}
	
}
