#!/bin/bash

for FILE in $(find . -name '*.java') 
do
  iconv -f ISO-8859-1 -t UTF-8 $FILE > cache.txt
  mv cache.txt $FILE
done
