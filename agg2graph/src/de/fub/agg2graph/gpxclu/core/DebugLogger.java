package de.fub.agg2graph.gpxclu.core;

import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Die Ausgabe in die GUI zu bringen wurde der nicht sehr schöne Ansatz hier gewählt.
 * Der Ansatz ist einfach eine Textfeld mit den Ausgaben zu befüllen.
 * @author Simon Könnecke
 *
 */
public class DebugLogger{
	//public List<String> msg = new LinkedList<String>();
	public JTextArea txt = new JTextArea("", 30, 40);
	
	public void add(String  str, boolean linebreak){
		//msg.add(str);
		if(linebreak)
			txt.setText(txt.getText() + str + "\r\n" );
		else
			txt.setText(txt.getText() + str);
		txt.repaint();
	}
}