/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.core;


import de.fub.agg2graph.gpxclu.graph.*;
import javax.swing.JFrame;


public class Main {
	public static void main(String[] args) {
		//PApplet.main(new String[] { "--present", "graph.MyProcessingSketch" });
		// threadsafe way to create a Swing GUI
	    
	    
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
    	    GpxFile gpx = new GpxFile();
    	    
	    	// create new JFrame
	  		JFrame jf = new JFrame("Clusteranalyse von GPS-Spuren");
	  	 
	  	    // this allows program to exit
	  	    jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  	 
	  	    // You add things to the contentPane in a JFrame
	  	    jf.getContentPane().add(new ControlPanel(jf, gpx));
	  	  
	  	    // keep window from being resized
	  	    jf.setResizable(true);
	  	 
	  	    // size frame
	  	    jf.pack();
	  	    
	  	    jf.setSize(1024, 800);
	  	 
	  	    // make frame visible
	  	    jf.setVisible(true);
	      }
	    }
	    );
		
	}
}
