/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.core;

import de.fub.agg2graph.gpxclu.cluster.Export;
import de.fub.agg2graph.gpxclu.cluster.FrechetDistance;
import de.fub.agg2graph.gpxclu.graph.GpxFile;
import de.fub.agg2graph.gpxclu.trace.Point;
import de.fub.agg2graph.gpxclu.trace.Trace;
import de.fub.agg2graph.gpxclu.trace.Traces;
import de.fub.agg2graph.gpxclu.trace.TrcOp;

public class TestDouglasPeucker {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GpxFile gpx;
		
		Debug.syso("Load new File");
		
		
		String filename = args[0];
		
		if (filename != null) {
			Debug.syso("Lade GPX-Datei: " + filename + ".");
			try{
				gpx = new GpxFile(filename, false);
				Trace s = new Trace();
				s.addPoint(10,10);
				s.addPoint(10,20);
				s.addPoint(20,10);
				s.addPoint(20,20);
				
				Trace s2 = new Trace();
				s2.addPoint(20,10);
				s2.addPoint(10,10);
				s2.addPoint(20,20);
				s2.addPoint(10,20);
				
				
				
				Trace t = gpx.getTraces().get(0);
				Trace t10 = TrcOp.DouglasPeuckerReduction(t, 10, Trace.getIncrementVersionId());
				Trace t200 = TrcOp.DouglasPeuckerReduction(t, 200, Trace.getIncrementVersionId());
				Trace t1500 = TrcOp.DouglasPeuckerReduction(t, 1500, Trace.getIncrementVersionId());
				
				FrechetDistance dist = new FrechetDistance();
				
				//double ds = dist.compareTo(s, s2);
			//	Debug.syso("S zu S2 " +  ds);
				
				
				
				export(t);
				
				Traces traces = new Traces();
				traces.addTrace(t10);
				Export.TraceToGpx(traces, filename + "10.gpx");
				export(t10);
				
				traces = new Traces();
				traces.addTrace(t200);
				Export.TraceToGpx(traces, filename + "200.gpx");
				export(t200);
				
				traces = new Traces();
				traces.addTrace(t1500);
				Export.TraceToGpx(traces, filename + "1500.gpx");
				export(t1500);
				
				
				Debug.syso("TR hat " +  t.size() + ", TR10 hat " + t10.size() + ", TR200 hat " + t200.size() + " und TR1500 hat " + t1500.size() + " Punkte.");
				
				double d1500 = dist.compareTo(t1500, t);
				Debug.syso("TR zu TR1500 " +  d1500);
				d1500 = dist.compareTo(t1500, t200);
				Debug.syso("TR200 zu TR1500 " +  d1500);
				double d200 = dist.compareTo(t, t200);
				Debug.syso("TR zu TR200 " +  d200);
				double d10 = dist.compareTo(t10, t);
				Debug.syso("TR zu TR10 " +  d10);
				double d = dist.compareTo(t, t);
				Debug.syso("TR zu TR " +  d);
				
			}
			catch (Exception e) {
				// TODO: handle exception
				
			}
			
			
		}
	}
	
	private static void export(Trace t){
		String o = "LINESTRING (";
		for(Point p : t){
			o += p.getX() + " " + p.getY() + ", ";
		}
		o = o.substring(0, o.length()-2);
		o += ")";
		System.out.println(o);
	}

}
