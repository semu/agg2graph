/*******************************************************************************
 * Copyright 2013 Simon Könnecke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.gpxclu.core;

public class Debug {
	public static final DebugLogger rootLogger = new DebugLogger();
	
	public static void syso(String str){
		rootLogger.add(str, true);
		System.out.println(str);
	}
	public static void sysoWithoutLn(String str){
		rootLogger.add(str, false);
		System.out.print(str);
	}
	
}



