/*******************************************************************************
   Copyright 2014 Johannes Mitlmeier, Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.ui.gui;

import java.awt.Desktop;
import java.awt.Dimension;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import de.fub.agg2graph.graph.RamerDouglasPeuckerFilter;
import de.fub.agg2graph.input.GPSCleaner;
import de.fub.agg2graph.input.GPSFilter;
import de.fub.agg2graph.input.GPXReader;
import de.fub.agg2graph.input.Globals;
import de.fub.agg2graph.management.MiniProfiler;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSSegment;
import de.fub.agg2graph.structs.frechet.FrechetDistance;
import de.fub.agg2graph.ui.FreeSpacePanel;
import de.fub.agg2graph.ui.gui.jmv.Layer;
import de.fub.agg2graph.ui.gui.jmv.TestUI;

public class CalcThread extends Thread {
	private String task;
	private final TestUI parent;

	public static final Map<String, Integer> levels;
	static {
		levels = new HashMap<String, Integer>();
		levels.put("input", 0);
		levels.put("filter", 1);
		levels.put("clean", 2);
		levels.put("agg", 3);
		levels.put("road", 4);
		levels.put("export", 5);
		levels.put("free",6);
	}

	public static String[] stepNames = new String[] { "Input", "Filter", "Clean",
			"Aggregation", "Road Gen", "Export", "Free Space"};

	public CalcThread(TestUI parent) {
		this.parent = parent;
	}

	public void setTask(String task) {
		this.task = task;
	}

	@Override
	public void run() {

		parent.setLoading(true);
		System.out.println(MiniProfiler.print(String.format("before step %s",
				task)));
		UIStepStorage stepStorage = parent.uiStepStorage;
		if (task.equals("input")) {
			stepStorage.clear(levels.get(task));
			stepStorage.inputSegmentList = new ArrayList<GPSSegment>();

			File folder = (File) parent.sourceFolderCombo.getSelectedItem();
			File[] files = folder.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".gpx");
				}
			});
			Arrays.sort(files);

			for (File file : files) {
				if (parent.deselectedTraceFiles.contains(file)) {
					continue;
				}
				System.out.println("processing file " + file);
				List<GPSSegment> segments = GPXReader.getSegments(file);
				if (segments == null) {
					System.out.println("Bad file: " + file);
					continue;
				}
				for (GPSSegment segment : segments) {
					parent.parseDim(segment);
					segment.addIDs("I" + stepStorage.inputSegmentList.size());
					stepStorage.rawLayer.addObject(segment);
					stepStorage.inputSegmentList.add(segment);
				}
			}

			System.out.println(String.format("Loaded %d gps segments",
					stepStorage.inputSegmentList.size()));

			parent.getMainPanel().showArea(parent.dataBoundingBox);
			parent.getLayerManager().setSize(new Dimension(1, 1));
			repaintEverything();
		} else if (task.equals("filter")) {
			stepStorage.clear(levels.get(task));

//			// Filter data
			GPSFilter gpsFilter = stepStorage.getGpsFilter();
			GPSSegment firstSegment = stepStorage.inputSegmentList.get(0);
			GPSSegment filteredSegment = gpsFilter.filter(firstSegment);
			stepStorage.inputSegmentList.remove(0);
			stepStorage.inputSegmentList.add(0, filteredSegment);

			repaintEverything();
		}  else if (task.equals("clean")) {
			stepStorage.clear(levels.get(task));
			stepStorage.cleanSegmentList = new ArrayList<GPSSegment>();

			// clean data
			GPSCleaner gpsCleaner = stepStorage.getGpsCleaner();
			RamerDouglasPeuckerFilter rdpf = stepStorage
					.getCleaningRamerDouglasPeuckerFilter();

			for (GPSSegment segment : stepStorage.inputSegmentList) {
				for (GPSSegment cleanSegment : gpsCleaner.clean(segment)) {
					// run through Douglas-Peucker here (slightly modified
					// perhaps to avoid too long edges)
					cleanSegment = rdpf.simplify(cleanSegment);
					stepStorage.cleanLayer.addObject(cleanSegment);
					stepStorage.cleanSegmentList.add(cleanSegment);
				}
			}
			repaintEverything();
		} else if (task.equals("agg")) {
			stepStorage.clear(levels.get(task));
			// use clean data if cleaning step has been performed
			if (stepStorage.levelReached >= 2) {
				for (int i = 0; i < stepStorage.cleanSegmentList.size(); i++) {
					if(i < 1)
						stepStorage.getAggContainer().addSegment(stepStorage.cleanSegmentList.get(i), true);
					else
						stepStorage.getAggContainer().addSegment(stepStorage.cleanSegmentList.get(i), false);
				}
			} else {
				for (int i = 0; i < stepStorage.inputSegmentList.size(); i++) {
					if(i < 1)
						stepStorage.getAggContainer().addSegment(stepStorage.inputSegmentList.get(i), true);
					else
						stepStorage.getAggContainer().addSegment(stepStorage.inputSegmentList.get(i), false);
				}
			}			
			repaintEverything();
		} else if (task.equals("road")) {
			stepStorage.clear(levels.get(task));
			stepStorage.getRoadNetwork().parse(stepStorage.getAggContainer(),
					stepStorage);
			repaintEverything();
		} else if (task.equals("export")) {
			stepStorage.clear(levels.get(task));
			if (stepStorage.getExporter().getTargetPrefix() == null) {
				stepStorage.getExporter()
						.setTargetPrefix(new File("osm-out"));
			}
			stepStorage.getExporter().export(stepStorage.getRoadNetwork());
			stepStorage.getExporter().export(parent.getLayerManager());
			if (stepStorage.isOpenOsmExportFile()
					&& (new File(stepStorage.getExporter().getTargetPrefix()+".xml")).exists()) {
				System.out.println("opening file "
						+ stepStorage.getExporter().getTargetPrefix()+".xml");
				try {
					Desktop.getDesktop().open(
							stepStorage.getExporter().getTargetPrefix());
				} catch (IOException e) {
					System.out.println(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}
		} else if (task.equals("free")) {
			System.out.println(String.format("Loaded %d gps segments",
					stepStorage.inputSegmentList.size()));
			//Get first Segment
			GPSSegment segment1 = stepStorage.inputSegmentList.get(0);
			List<GPSEdge> path1 = new ArrayList<GPSEdge>();
			for(int i = 0; i < segment1.size() - 1; i++) {
				path1.add(new GPSEdge(segment1.get(i), segment1.get(i+1)));
			}

			//Get second segment
			GPSSegment segment2 = stepStorage.inputSegmentList.get(1);
			List<GPSEdge> path2 = new ArrayList<GPSEdge>();
			for(int i = 0; i < segment2.size() - 1; i++) {
				path2.add(new GPSEdge(segment2.get(i), segment2.get(i+1)));
			}
			
			double epsilon = stepStorage.getFreeSpace().getFreeSpaceOptions().epsilon;
			FrechetDistance f = new FrechetDistance(path1, path2, epsilon);
			showFreeSpace(f);			
		}
		stepStorage.levelReached = levels.get(task);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				parent.tabbedPane.setSelectedIndex((levels.get(task) + 1)
						% levels.size());
			}
		});
		System.out.println(MiniProfiler.print(String.format("after step %s",
				task)));
		parent.setLoading(false);
	}

	private void repaintEverything() {
		parent.setPainting(true);
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				parent.getLayerManager().repaintAllLayers();
			}
		});
	}
	
	private void showFreeSpace(final FrechetDistance f) {
		javax.swing.SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
				show();
				
			}

			private void show() {
				
				JFrame freeSpaceView = new JFrame();
				freeSpaceView.setTitle("Free Space Diagram");
				freeSpaceView.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				
				FreeSpacePanel freeSpace = new FreeSpacePanel(freeSpaceView, f);
				freeSpace.setPreferredSize(new Dimension (600, 800));

				freeSpaceView.getContentPane().add(freeSpace);
				freeSpaceView.pack();
				freeSpaceView.setVisible(true);
			}
			
		});
	}
}
