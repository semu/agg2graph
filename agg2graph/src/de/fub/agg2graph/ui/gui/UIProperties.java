package de.fub.agg2graph.ui.gui;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class UIProperties {
	public int getLeft() {
		return left;
	}
	public void setLeft(int left) {
		this.left = left;
	}
	public int getTop() {
		return top;
	}
	public void setTop(int top) {
		this.top = top;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getSel() {
		return sel;
	}
	public void setSel(int sel) {
		this.sel = sel;
	}
	public UIProperties(int left, int top, int width, int height, int sel) {
		super();
		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
		this.sel = sel;
	}
	public UIProperties() {
		super();
	}
	public void initializeFromFile(){
		try {
			Properties uiProperties = new Properties();
			uiProperties.load(new FileInputStream("ui.properties"));
			left = Integer.parseInt(uiProperties.getProperty(
					"left", "50"));
			top = Integer.parseInt(uiProperties.getProperty("top",
					"50"));
			width = Integer.parseInt(uiProperties.getProperty(
					"width", "1000"));
			height = Integer.parseInt(uiProperties.getProperty(
					"height", "800"));
			sel = Integer.parseInt(uiProperties.getProperty(
					"input-selection", "0"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	

	int left = 50;
	int top = 40;
	int width = 1000;
	int height = 800;
	int sel = 0;

}
