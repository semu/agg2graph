/*******************************************************************************
   Copyright 2015 Johannes Mitlmeier, Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.ui.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Stroke;
import java.util.HashMap;
import java.util.Map;

public class RenderingOptions {
	public enum RenderingType {
		ALL, INTELLIGENT_ALL, POINTS, LINES, NONE
	};

	public enum LabelRenderingType {
		ALWAYS, INTELLIGENT, NEVER
	};

	public enum WeightRenderingType {
		ALWAYS, NEVER
	};

	
	public int zIndex = 0;
	public Color color = Color.BLACK;
	public float strokeBaseWidthFactor = 0.8F;
	public static final BasicStroke basicStroke = new BasicStroke(1.5f,
			BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
	public LabelRenderingType labelRenderingType = LabelRenderingType.NEVER;
	public WeightRenderingType weightRenderingType = WeightRenderingType.NEVER;
	public double opacity = 1;
	public RenderingType renderingType = RenderingType.INTELLIGENT_ALL;
	private Map<Float, Stroke> strokes = new HashMap<Float, Stroke>();

	@Override
	public String toString() {
		return "RenderingOptions [color=" + color + ", stroke=" + basicStroke
				+ ", opacity=" + opacity + "]";
	}

	public Stroke getStroke(float weightFactor) {
		// a little cache for the strokes
		if (strokes.get(weightFactor) != null) {
			return strokes.get(weightFactor);
		}
		BasicStroke newStroke = new BasicStroke(basicStroke.getLineWidth()
				* strokeBaseWidthFactor * weightFactor,
				basicStroke.getEndCap(), basicStroke.getLineJoin(),
				basicStroke.getMiterLimit(), basicStroke.getDashArray(),
				basicStroke.getDashPhase());
		// CompoundStroke finalStroke = new CompoundStroke(newStroke,
		// borderStroke, CompoundStroke.ADD);
		strokes.put(weightFactor, newStroke);
		return newStroke;
	}

	public RenderingOptions getCopy() {
		RenderingOptions result = new RenderingOptions();
		result.zIndex = zIndex;
		result.color = color;
		result.strokeBaseWidthFactor = strokeBaseWidthFactor;
		result.labelRenderingType = labelRenderingType;
		result.weightRenderingType = weightRenderingType;
		result.opacity = opacity;
		result.renderingType = renderingType;
		return result;
	}
}
