/*******************************************************************************
   Copyright 2013 Johannes Mitlmeier

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg;

import de.fub.agg2graph.agg.strategy.FrechetMatchIterativeMergeStrategy;
import de.fub.agg2graph.agg.strategy.GpxmergeMatchAttractionMergeStrategy;
import de.fub.agg2graph.agg.strategy.HausdorffMatchAttractionMergeStrategy;
import de.fub.agg2graph.agg.strategy.HausdorffMatchDefaultMergeStrategy;
import de.fub.agg2graph.agg.strategy.HausdorffMatchFrechetMergeStrategy;
import de.fub.agg2graph.agg.strategy.FrechetMatchAttractionMergeStrategy;
import de.fub.agg2graph.agg.strategy.GpxmergeMatchIterativeMergeStrategy;
import de.fub.agg2graph.agg.strategy.GpxmergeAggregationStrategy;
import de.fub.agg2graph.agg.strategy.GpxFrechetAggregationStrategy;
import de.fub.agg2graph.agg.strategy.HausdorffMatchIterativeMergeStrategy;
import de.fub.agg2graph.agg.strategy.OriginalDefaultAggregationStrategy;
import de.fub.agg2graph.agg.strategy.PathScoreMatchAttractionMergeStrategy;
import de.fub.agg2graph.agg.strategy.PathScoreMatchDefaultMergeStrategy;
import de.fub.agg2graph.agg.strategy.PathScoreMatchFrechetMergeStrategy;
import de.fub.agg2graph.agg.strategy.PathScoreMatchIterativeMergeStrategy;
import de.fub.agg2graph.agg.strategy.SecondAggregationStrategyAttraction;
import de.fub.agg2graph.agg.strategy.SecondAggregationStrategyIterative;
import de.fub.agg2graph.agg.strategy.TraClusIterativeAggregationStrategy;

/**
 * A factory that returns the {@link IAggregationStrategy} currently set via the
 * getObject method.
 * 
 * @author Johannes Mitlmeier
 * 
 */
public class AggregationStrategyFactory {
	
	/*
	 * Default class to return. Can be overwritten by calls to setClass.
	 */
	private static Class<? extends IAggregationStrategy> defaultDefaultClass = PathScoreMatchDefaultMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> defaultFrechetClass = PathScoreMatchFrechetMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> defaultAttractionClass = PathScoreMatchAttractionMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> defaultIterativeClass = PathScoreMatchIterativeMergeStrategy.class;
	
	private static Class<? extends IAggregationStrategy> gpxDefaultClass = GpxmergeAggregationStrategy.class;
	private static Class<? extends IAggregationStrategy> gpxFrechetClass = GpxFrechetAggregationStrategy.class;
	private static Class<? extends IAggregationStrategy> gpxAttractionClass = GpxmergeMatchAttractionMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> gpxIterativeClass = GpxmergeMatchIterativeMergeStrategy.class;
	
	private static Class<? extends IAggregationStrategy> hausdorffDefaultClass = HausdorffMatchDefaultMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> hausdorffFrechetClass = HausdorffMatchFrechetMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> hausdorffAttractionClass = HausdorffMatchAttractionMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> hausdorffIterativeClass = HausdorffMatchIterativeMergeStrategy.class;
	
	private static Class<? extends IAggregationStrategy> frechetAttractionClass = FrechetMatchAttractionMergeStrategy.class;
	private static Class<? extends IAggregationStrategy> frechetIterativeClass = FrechetMatchIterativeMergeStrategy.class;

	private static Class<? extends IAggregationStrategy> secondStrategyAttractionClass = SecondAggregationStrategyAttraction.class;
	private static Class<? extends IAggregationStrategy> secondStrategyIterativeClass = SecondAggregationStrategyIterative.class;

	private static Class<? extends IAggregationStrategy> originalDefaultStrategyClass = OriginalDefaultAggregationStrategy.class;
	
	private static Class<? extends IAggregationStrategy> traClusIterativeStrategyClass = TraClusIterativeAggregationStrategy.class;

	/*
	 * Default class to return. Can be overwritten by calls to setClass.
	 */
	private static Class<? extends IAggregationStrategy> factoryClass = PathScoreMatchDefaultMergeStrategy.class;

	public static void setClass(Class<? extends IAggregationStrategy> clazz) {
		factoryClass = clazz;
	}

	public static IAggregationStrategy getObject() {
		if (factoryClass == null) {
			return null;
		}
		try {
			return factoryClass.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static IAggregationStrategy getObject(String name) throws InstantiationException, IllegalAccessException {
		switch(name) {
		case "PathWCP":
			return defaultDefaultClass.newInstance();
		case "PathFrechet":
			return defaultFrechetClass.newInstance();
		case "PathAttraction":
			return defaultAttractionClass.newInstance();
		case "PathIterative":
			return defaultIterativeClass.newInstance();
		case "GPXMergeAggregation":
			return gpxDefaultClass.newInstance();
		case "GPXFrechetAggregation":
			return gpxFrechetClass.newInstance();
		case "EdgeAttraction":
			return gpxAttractionClass.newInstance();
		case "EdgeIterative":
			return gpxIterativeClass.newInstance();
		case "HausdorffWCP":
			return hausdorffDefaultClass.newInstance();
		case "HausdorffFrechet":
			return hausdorffFrechetClass.newInstance();
		case "HausdorffAttraction":
			return hausdorffAttractionClass.newInstance();
		case "HausdorffIterative":
			return hausdorffIterativeClass.newInstance();
		case "FrechetAttraction":
			return frechetAttractionClass.newInstance();
		case "FrechetIterative":
			return frechetIterativeClass.newInstance();
		case "ConformalAttraction":
			return secondStrategyAttractionClass.newInstance();
		case "ConformalIterative":
			return secondStrategyIterativeClass.newInstance();
		case "Original":
			return originalDefaultStrategyClass.newInstance();
		case "TraClus":
			return traClusIterativeStrategyClass.newInstance();
		default:
			return factoryClass.newInstance();
		}
	}
}
	
