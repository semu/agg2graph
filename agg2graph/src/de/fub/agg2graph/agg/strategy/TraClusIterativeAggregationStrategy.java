/*******************************************************************************
   Copyright 2014 Ferhat Beyaz, Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.awt.geom.Line2D;
import java.util.*;

import de.fub.agg2graph.traclus.*;
import de.fub.agg2graph.agg.*;
import de.fub.agg2graph.gpsk.algorithms.Normalizer;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.structs.*;

public class TraClusIterativeAggregationStrategy extends AbstractAggregationStrategy {
	// aggregation parameters
	public boolean usePartitioning = false;
	public boolean useNormalizer = true;
	public boolean useTraClusMatching = true;
	public boolean useTraClusMerging = true;
	
	public boolean allowMultipleCandidates = false;
	public boolean allowMultipleMatches = false;
	public boolean removeStartAndEndOfMatchings = true; // TODO bug in merger causes some shift problems
														// this variable with minLns=1 manually deletes start and and
	public boolean matchImmediately = false;
	public boolean cleanAfterMerge = false;
	
	// partitioning parameters
	public double w_perp = 1.0;
	public double w_par = 1.0;
	public double w_angle = 1.0;
	public double MDL_COST_ADVANTAGE = 2.0;
	
	// normalizing parameters
	public double normalizeDistance = 50.0;
	public double normalizeTolerance = 0.3;
	public boolean normalizeAllowShortEdges = true;
	
	// matching parameters
	public int maxLookahead = 7;
	public double maxPathDifference = 500;
	public double maxInitDistance = 100;
	
	// merging parameters
	public double minCloseNodeDistance = 0.00001;
	
	// traclus distance threshold
	public double maxTraClusDistance = 3;
	
	// rtree search configuration: 1.5, 1.5, 0.7, 0.7
	public double rTreeSearchFactor = 100;
	public double rTreeDimX = 1.5;
	public double rTreeDimY = 1.5;
	public double rTreeDimAngle = 0.7;
	public double rTreeDimLength = 0.7;
	
	// aggregation results in matches or no matches
	private enum State { NO_MATCH, IN_MATCH }
	private State currentAggregationState = State.NO_MATCH;
	private int partitionId = 0;
	
	// rtree saves edges
	private RTree<Vector<AggNode>> rTreeEdges;
	private GPSPoint fixedStartPoint;
	private GPSPoint fixedEndPoint;
	private List<AggNode> oldAggNodes = new ArrayList<AggNode>();
	
	// mercator transformation settings
	private int mercatorZoom = 12;
	
	// counter for statistics
	private boolean printStatistics = true;
	private int numberOfRTreeEdges;
	private int numberOfCandidateEdges;
	private int numberOfMatchings;
	private int numberOfSegments;
	private int numberOfSegmentPoints;
	private int overallLengthOfSegments;
	
	public TraClusIterativeAggregationStrategy() {
		if (useTraClusMerging) {
			MergeHandlerFactory.setClass(TraClusSweepMerge.class);
		} else {
			MergeHandlerFactory.setClass(WeightedClosestPointMerge.class);
		}
		
		traceDistance = TraceDistanceFactory.getObject();
		baseMergeHandler = MergeHandlerFactory.getObject();
		
		// rtree setup
		rTreeEdges = new RTree<Vector<AggNode>>(Integer.MAX_VALUE, 0, 6);
		fixedStartPoint = new GPSPoint();
		fixedEndPoint = new GPSPoint();
		fixedStartPoint.setLat(52.519172);
		fixedStartPoint.setLon(13.400184);
		fixedEndPoint.setX(52.518506);
		fixedEndPoint.setY(13.400849);
		
		// for statistics
		numberOfRTreeEdges = 0;
		numberOfCandidateEdges = 0;
		numberOfMatchings = 0;
		numberOfSegments = 0;
		numberOfSegmentPoints = 0;
		overallLengthOfSegments = 0;
	}
	
	/* AGGREGATION METHODS */
	
	@Override
	public void aggregate(GPSSegment segment, boolean isAgg) {
		System.out.println("New GPS-Segment (size = " + segment.size() + "): " + segment);
		if (segment.isEmpty()) return;
		
		// compute segment statistics
		if (printStatistics) {
			numberOfSegments++;
			numberOfSegmentPoints += segment.size();
			int currentPointIndex = 0;
			while (currentPointIndex < segment.size() - 1) {
				GPSPoint currentPoint = segment.get(currentPointIndex);
				GPSPoint nextPoint = segment.get(currentPointIndex + 1);
				overallLengthOfSegments += GPSCalc.getDistance(currentPoint.getLat(), currentPoint.getLon(), nextPoint.getLat(), nextPoint.getLon());
				currentPointIndex++;
			}
		}
		
		// reset all attributes
		lastNode = null;
		mergeHandler = null;
		matches = new ArrayList<IMergeHandler>();
		currentAggregationState = State.NO_MATCH;
		
		// partition segment
		if (usePartitioning) {
			segment = partition(segment, partitionId++);
			// System.out.println("\t\tGPS-Segment after partitioning: " + segment);
		}
		
		// normalize segment
		if (useNormalizer) {
			segment = normalize(segment);
			// System.out.println("\t\tNormalized GPS-Segment (size = " + segment.size() + ")");
		}
		
		// first segment should just be added
		if (aggContainer.getCachingStrategy() == null || aggContainer.getCachingStrategy().getNodeCount() == 0) {
			int i = 0;
			while (i < segment.size()) {
				AggNode node = new AggNode(segment.get(i), aggContainer);
				node.setID("A-" + segment.get(i).getID());
				addNodeToAgg(aggContainer, node);
				
				// add aggnode to rtree
				if (useTraClusMatching) {
					if (lastNode != null) {
						addEdgeToRTree(lastNode, node);
					}
				}
				
				lastNode = node;
				i++;
			}
			return;
		}
		
		// find matches between segment and the aggregate
		if (useTraClusMatching) {
			matchTraClus(segment);
		} else {
			matchDefault(segment);
		}
		
		// merge found matches
		merge();
		
		if (printStatistics) {
			System.out.println("Number Of Edges: " + numberOfRTreeEdges);
			System.out.println("Number Of Candidate Edges: " + numberOfCandidateEdges);
			System.out.println("Number Of Matchings: " + numberOfMatchings);
			System.out.println("Number Of Segments: " + numberOfSegments);
			System.out.println("Number Of Segment Points: " + numberOfSegmentPoints);
			System.out.println("Overall Network Length: " + overallLengthOfSegments);
		}
	}
	
	private GPSSegment normalize(GPSSegment segment) {
		// System.out.println("\tNormalizing:");
		
		// gpsk setup
		GPSkSegment gpskSegment = new GPSkSegment();
		for (GPSPoint p: segment) {
			gpskSegment.add(new GPSkPoint(p));
		}
		
		// normalizer setup
		Normalizer normalizer = new Normalizer();
		normalizer.setEps(normalizeDistance);
		normalizer.setMaxDistance(normalizeDistance + (normalizeDistance * normalizeTolerance));
		normalizer.setMinDistance(normalizeDistance - (normalizeDistance * normalizeTolerance));
		normalizer.setAllowShortEdges(normalizeAllowShortEdges);
		
		GPSSegment newSegment = new GPSSegment(normalizer.normalizeSegment(gpskSegment));
		int newId = 0;
		if (segment.get(0).getID() != null) {
			String[] idParts = segment.get(0).getID().split("-");
			for (GPSPoint point: newSegment) {
				if (!segment.contains(point)) {
					point.setID(idParts[0] + "-N*-" + newId++);
				}
			}
		}
		
		// System.out.println("Normalized GPS-Segment (size = " + newSegment.size() + "): " + newSegment);
		return newSegment;
	}
	
	private GPSSegment partition(GPSSegment segment, int partitionId) {
		// System.out.println("\tPartitioning:");
		// transform gps segment to traclus trajectory
		MercatorTransformation transformator = new MercatorTransformation();
		Trajectory trajectory = new Trajectory(partitionId);
		for (GPSPoint segmentPoint: segment) {
			Point trajectoryPoint = transformator.fromLatLngToPoint(segmentPoint.getLat(), segmentPoint.getLon(), mercatorZoom);
			trajectory.points.add(trajectoryPoint);
		}
		
		// initialize traclus
		ArrayList<Trajectory> trajectoryList = new ArrayList<>(1);
		trajectoryList.add(trajectory);
		TraClus traClus = new TraClus(trajectoryList, 2, 0, 0);
		
		// set parameters for partitioning
		traClus.w_perp = w_perp;
		traClus.w_par = w_par;
		traClus.w_angle = w_angle;
		traClus.MDL_COST_ADVANTAGE = MDL_COST_ADVANTAGE;
		
		// run partitioning
		traClus.findOptimalPartition(trajectory);
		
		// transform traclus partition back to gps segment
		GPSSegment partitionedSegment = new GPSSegment();
		for (Point traclusPartitionPoint: trajectory.partitionPoints) {
			// transform x and y coordinates to lat and lon
			Point transformedPoint = transformator.fromPointToLatLng(traclusPartitionPoint, mercatorZoom);
			
			// add point with those coordinates
			GPSPoint partitionedSegmentPoint = new GPSPoint();
			partitionedSegmentPoint.setID(UUID.randomUUID().toString());
			partitionedSegmentPoint.setLat(transformedPoint.coordinates[0]);
			partitionedSegmentPoint.setLon(transformedPoint.coordinates[1]);
			partitionedSegment.add(partitionedSegmentPoint);
		}
		
		return partitionedSegment;
	}
	
	private void matchTraClus(GPSSegment segment) {
		// System.out.println("\tMatching:");
		
		// adding nodes to aggcontainer after finding matchings
		List<Vector<AggNode>> edgesToAgg = new ArrayList<Vector<AggNode>>();
		
		// already matched edges should not be matched again
		List<Vector<AggNode>> matchedEdges = new ArrayList<Vector<AggNode>>();
		
		// matching should checked for all points in the segment
		int currentPointIndex = 0;
		while (currentPointIndex < segment.size() - 1) {
			// get current edge points
			GPSPoint currentPoint = segment.get(currentPointIndex);
			GPSPoint nextPoint = segment.get(currentPointIndex + 1);
			// System.out.println("\t\tCurrent Point: " + currentPoint + ", NextPoint: " + nextPoint);
			
			// get near edges
			List<Vector<AggNode>> nearEdges = getNearEdgesFromRTree(currentPoint, nextPoint);
			numberOfRTreeEdges += nearEdges.size();
			
			// if multiple matches per edge are not allowed, filter out already matched edges
			if (!allowMultipleMatches) {
				Iterator<Vector<AggNode>> nearEdgesIterator = nearEdges.iterator();
				while (nearEdgesIterator.hasNext()) {
					Vector<AggNode> nearEdge = nearEdgesIterator.next();
					for (Vector<AggNode> matchedEdge: matchedEdges) {
						// if it is already matched before, dont use it for future matches
						if (nearEdge.get(0).getID().equals(matchedEdge.get(0).getID()) &&
							nearEdge.get(1).getID().equals(matchedEdge.get(1).getID())) {
							nearEdgesIterator.remove();
						}
					}
				}
			}
			
			// filter out deleted old nodes
			Iterator<Vector<AggNode>> nearEdgesIterator = nearEdges.iterator();
			while (nearEdgesIterator.hasNext()) {
				Vector<AggNode> nearEdge = nearEdgesIterator.next();
				for (AggNode oldAggNode: oldAggNodes) {
					if (oldAggNode.getID().equals(nearEdge.get(0).getID()) || oldAggNode.getID().equals(nearEdge.get(1).getID())) {
						try { nearEdgesIterator.remove(); } catch (java.lang.IllegalStateException e) {}
					}
				}
			}
			
			// System.out.println("\t\tNear Edges (size = " + nearEdges.size() + "): " + nearEdges);
			
			// get candidate edges out of near edges (when traclus distance is under a given threshold)
			List<Vector<AggNode>> candidateEdges = new ArrayList<Vector<AggNode>>();
			Vector<AggNode> minEdge = null;
			double minDistance = Double.MAX_VALUE;
			for (Vector<AggNode> nearEdge: nearEdges) {
				// get agg nodes of near edge
				AggNode aggNodeStart = nearEdge.get(0);
				AggNode aggNodeEnd = nearEdge.get(1);
				
				// compute traclus distance
				double distance = computeDistanceBetweenEdgesTraClus(currentPoint, nextPoint, aggNodeStart, aggNodeEnd);
				// System.out.println("Distance " + currentPoint + ", " + nextPoint + " - " + aggNodeStart + ", " + aggNodeEnd + ": " + distance);
				
				// only add edge if its distance is under the threshold
				if (distance < maxTraClusDistance) {
					numberOfCandidateEdges++;
					candidateEdges.add(nearEdge);
					
					// save best edge
					if (distance < minDistance) {
						minDistance = distance;
						minEdge = nearEdge;
					}
				}
			}
			
			// if multiple candidates are not allowed, only take best edge
			if (!allowMultipleCandidates && (minEdge != null)) {
				candidateEdges.clear();
				candidateEdges.add(minEdge);
			}
			
			// System.out.println("\t\t\tMinDistance to edge " + minEdgeStart + " - " + minEdgeEnd + ": " + minDistance);
			
			// if no candidate edges are found, simply add edge into aggContainer, else match candidate edges
			if (candidateEdges.isEmpty()) {
				// System.out.println("\t\t\tEdge " + currentPoint + " - " + nextPoint + " has no candidate edges.");
				AggNode currentNode = new AggNode(currentPoint, aggContainer);
				currentNode.setID("A-" + currentPoint.getID());
				addNodeToAgg(aggContainer, currentNode);
				lastNode = currentNode;
				currentPointIndex++;
				
				// create next node from next point
				AggNode nextNode = new AggNode(nextPoint, aggContainer);
				nextNode.setID("A-" + nextPoint.getID());
				
				// if last edge, save last node
				if (currentPointIndex == segment.size() - 1) {
					addNodeToAgg(aggContainer, nextNode);
					lastNode = nextNode;
					currentPointIndex++;
				}
				
				// add edge into rtree
				Vector<AggNode> newEdge = new Vector<AggNode>();
				newEdge.add(currentNode);
				newEdge.add(nextNode);
				edgesToAgg.add(newEdge);
			} else {
				// System.out.println("\t\t\t" + candidateEdges.size() + " candidate edges under the threshold.");
				/*for (Vector<AggNode> candidateEdge: candidateEdges) {
					System.out.println("\t\t\t\t" + candidateEdge.get(0) + " - " + candidateEdge.get(1));
				}*/
				
				mergeHandler = baseMergeHandler.getCopy();
				mergeHandler.setAggContainer(aggContainer);
				
				// add candidate agg nodes to the merge handler
				IMergeHandler foundMergeHandler = null;
				for (Vector<AggNode> candidateEdge: candidateEdges) {
					// add intersecting edges to same merge handler, if traclus merging is selected
					if (useTraClusMerging) {
						// find intersecting matchings
						foundMergeHandler = getIntersectingMergeHandler(candidateEdge, currentPoint, nextPoint);
						if (foundMergeHandler != null) mergeHandler = foundMergeHandler;
						
						// merge handler should contain edges from gps trace and from aggregation
						Vector<ILocation> aggEdge = new Vector<ILocation>(2);
						aggEdge.add(candidateEdge.get(0));
						aggEdge.add(candidateEdge.get(1));
						
						Vector<ILocation> gpsEdge = new Vector<ILocation>(2);
						gpsEdge.add(currentPoint);
						gpsEdge.add(nextPoint);
						
						// edges will first have agg then gps edges
						((TraClusSweepMerge) mergeHandler).edges.add(aggEdge);
						((TraClusSweepMerge) mergeHandler).edges.add(gpsEdge);
					}
					
					// because of intersecting matchings, node may be found in the merge handler already
					List<AggNode> aggNodesInMergeHandler = mergeHandler.getAggNodes();
					if (!aggNodesInMergeHandler.contains(candidateEdge.get(0))) 
						mergeHandler.addAggNode(candidateEdge.get(0));
					if (!aggNodesInMergeHandler.contains(candidateEdge.get(1))) 
						mergeHandler.addAggNode(candidateEdge.get(1));
					mergeHandler.addGPSPoints(segment.subList(currentPointIndex, currentPointIndex + 2));
					matchedEdges.add(candidateEdge);
				}
				
				// add merge handler for this match
				if (foundMergeHandler == null) matches.add(mergeHandler);
				
				// after adding one sub-match this method is called and can handle that
				mergeHandler.processSubmatch();
				
				// connect matches
				// lastNode is the last non-matched node or the outNode of the last match
				if (lastNode != mergeHandler.getInNode()) {
					aggContainer.connect(lastNode, mergeHandler.getInNode());
					mergeHandler.setBeforeNode(lastNode);
				}
				
				// remember outgoing node (for later connection)
				lastNode = mergeHandler.getOutNode();
				currentPointIndex++;
			}
		}
		
		// add matches to agg container
		// System.out.println("\t\tAdding edges to agg container and rtree.");
		for (Vector<AggNode> edge: edgesToAgg) {
			AggNode currentNode = edge.get(0);
			AggNode nextNode = edge.get(1);
			
			// add edge to rtree
			// System.out.println("\t\t\tAdding edge from " + currentNode + " to " + nextNode + " to rtree.");
			addEdgeToRTree(currentNode, nextNode);
		}
	}
	
	private IMergeHandler getIntersectingMergeHandler(Vector<AggNode> candidateEdge, GPSPoint currentPoint, GPSPoint nextPoint) {
		IMergeHandler foundMergeHandler = null;
		for (IMergeHandler mergeHandler: matches) {
			if (!mergeHandler.isEmpty() && foundMergeHandler == null) {
				// get all edges from merge handler
				List<Vector<ILocation>> edgesInMergeHandler = ((TraClusSweepMerge) mergeHandler).edges;
				
				// if merging already contains 2 matches, dont use this merge handler
				// if (edgesInMergeHandler.size() > 2) continue;
				
				// get start and end from new matched edges
				ILocation aggStartNew = candidateEdge.get(0);
				ILocation aggEndNew = candidateEdge.get(1);
				ILocation segStartNew = currentPoint;
				ILocation segEndNew = nextPoint;
				
				// check, whether matchings intersect with each other
				for (int i = 0; i < edgesInMergeHandler.size(); i += 2) {
					// get start and end from edges from merge handler
					ILocation aggStart = edgesInMergeHandler.get(i).get(0);
					ILocation aggEnd = edgesInMergeHandler.get(i).get(1);
					ILocation segStart = edgesInMergeHandler.get(i + 1).get(0);
					ILocation segEnd = edgesInMergeHandler.get(i + 1).get(1);
					
					// check whether those matches intersect
					MercatorTransformation transformator = new MercatorTransformation();
					Point sStart1 = transformator.fromLatLngToPoint(segStart.getLat(), segStart.getLon(), 12);
					Point aStart1 = transformator.fromLatLngToPoint(aggStart.getLat(), aggStart.getLon(), 12);
					Point sEnd1 = transformator.fromLatLngToPoint(segEnd.getLat(), segEnd.getLon(), 12);
					Point aEnd1 = transformator.fromLatLngToPoint(aggEnd.getLat(), aggEnd.getLon(), 12);
					Point sStart2 = transformator.fromLatLngToPoint(segStartNew.getLat(), segStartNew.getLon(), 12);
					Point aStart2 = transformator.fromLatLngToPoint(aggStartNew.getLat(), aggStartNew.getLon(), 12);
					Point sEnd2 = transformator.fromLatLngToPoint(segEndNew.getLat(), segEndNew.getLon(), 12);
					Point aEnd2 = transformator.fromLatLngToPoint(aggEndNew.getLat(), aggEndNew.getLon(), 12);
					
					boolean edgesIntersect = Line2D.linesIntersect(
						sStart1.coordinates[0], sStart1.coordinates[1], 
						aStart1.coordinates[0], aStart1.coordinates[1], 
						sStart2.coordinates[0], sStart2.coordinates[1], 
						aStart2.coordinates[0], aStart2.coordinates[1]
					) || Line2D.linesIntersect(
						sEnd1.coordinates[0], sEnd1.coordinates[1], 
						aEnd1.coordinates[0], aEnd1.coordinates[1], 
						sEnd2.coordinates[0], sEnd2.coordinates[1], 
						aEnd2.coordinates[0], aEnd2.coordinates[1]
					);
					
					// remember intersecting merge handler
					if (edgesIntersect) {
						foundMergeHandler = mergeHandler;
						// System.out.println("\t\t\t\tMatchings intersect!");
					}
				}
			}
		}
		return foundMergeHandler;
	}
	
	private void matchDefault(GPSSegment segment) {
		// System.out.println("\tMatching:");
		
		// save last points to avoid no progress if last points are equal to each other
		BoundedQueue<ILocation> lastPoints = new BoundedQueue<ILocation>(5);
		
		// matching should checked for all points in the segment
		int currentPointIndex = 0;
		while (currentPointIndex < segment.size()) {
			GPSPoint currentPoint = segment.get(currentPointIndex);
			// System.out.println("\t\tCurrent Point (i = " + currentPointIndex + "): {" + currentPoint.getID() + "}");
			
			// if no progress is made (last two points are equal to current point), continue with next point
			if (lastPoints.size() > 2
				&& lastPoints.get(lastPoints.size() - 1).equals(currentPoint)
				&& lastPoints.get(lastPoints.size() - 2).equals(currentPoint)) {
				currentPointIndex++;
				continue;
			}
			
			// add current point to last observed points and save old aggregation state
			lastPoints.offer(currentPoint);
			State lastAggregationState = currentAggregationState;
			
			// get all close points, but none that are already in the current match
			Set<AggNode> nearPoints = aggContainer.getCachingStrategy().getCloseNodes(currentPoint, maxInitDistance);
			// System.out.println("\t\t\tNear Points: " + nearPoints);
			if (mergeHandler != null) {
				List<AggNode> nodes = mergeHandler.getAggNodes();
				for (int j = 0; j < nodes.size() - 1; j++) {
					nearPoints.remove(nodes.get(j));
				}
			}
			
			// check whether a match can be found
			boolean isMatch;
			if (nearPoints.size() == 0) { // if no near points are found, no points can be matched
				isMatch = false;
			} else { // else there are candidates for match
				List<List<AggNode>> candidatePaths = getPathsByDepth(nearPoints, 1, maxLookahead);
				
				// pick best path out of all candidate paths
				List<AggNode> bestPath = null;
				double bestPathDifference = Double.MAX_VALUE;
				int bestPathLength = 0;
				for (List<AggNode> candidatePath: candidatePaths) {
					// get difference of candidate path on aggregation and gps trace
					Object[] returnValues = traceDistance.getPathDifference(candidatePath, segment, currentPointIndex, mergeHandler);
					double candidatePathDifference = (Double) returnValues[0];
					int candidatePathLength = (int) Math.round(Double.valueOf(returnValues[1].toString()));
					
					// less path difference and longer paths are preferred for a match
					if (candidatePathDifference < bestPathDifference 
						|| (candidatePathDifference == bestPathDifference && candidatePathLength > bestPathLength)) {
						bestPath = candidatePath;
						bestPathDifference = candidatePathDifference;
						bestPathLength = candidatePathLength;
					}
				}
				
				// if best path is not good or too short, the match should not be done
				if (bestPathDifference >= maxPathDifference || bestPath == null) {
					isMatch = false;
					// System.out.println("\t\t\tMatching would be too bad.");
				} else if (bestPath.size() <= 1 && bestPathLength <= 1) {
					isMatch = false;
					// System.out.println("\t\t\tMatching would be too short.");
				} else {
					isMatch = true;
					// System.out.println("\t\t\tMatching found!");
				}
				
				// save whether a match can be done
				currentAggregationState = isMatch ? State.IN_MATCH : State.NO_MATCH;
				
				// if best path can be matched, create merge handler and add best path to its merge list
				if (isMatch) {
					// create a merge handler if the match starts here
					if (lastAggregationState == State.NO_MATCH) {
						mergeHandler = baseMergeHandler.getCopy();
						mergeHandler.setAggContainer(aggContainer);
					}
					
					// add best path and match on trace to the merge  handler
					mergeHandler.addAggNodes(bestPath);
					mergeHandler.addGPSPoints(segment.subList(currentPointIndex, currentPointIndex + bestPathLength));
					mergeHandler.setDistance(bestPathDifference);
					currentPointIndex = currentPointIndex + bestPathLength - 1;
				}
			}
			
			// finish matching or add point to the aggregation, if no valid match was found
			if (!isMatch && (lastAggregationState == State.IN_MATCH && (currentAggregationState == State.NO_MATCH || currentPointIndex == segment.size() - 1))) {
				// add merge handler for this match
				matches.add(mergeHandler);
				
				// after adding one sub-match this method is called and can handle that
				mergeHandler.processSubmatch();
				
				// connect to previous node
				// lastNode is the last non-matched node or the outNode of the last match
				aggContainer.connect(lastNode, mergeHandler.getInNode());
				mergeHandler.setBeforeNode(lastNode);
				
				// remember outgoing node (for later connection)
				lastNode = mergeHandler.getOutNode();
				// System.out.println("\t\t\tCurrent matching is finished.");
			} else if (!isMatch && lastAggregationState == State.NO_MATCH) {
				// if there is no close points or no valid match, add it to the aggregation
				AggNode node = new AggNode(currentPoint, aggContainer);
				node.setID("A-" + currentPoint.getID());
				addNodeToAgg(aggContainer, node);
				lastNode = node;
				currentPointIndex++;
				// System.out.println("\t\t\tNo close points or valid match.");
			}
		}
	}
	
	private void merge() {
		// System.out.println("\tMerging:");
		if (matches.size() == 0) {
			// System.out.println("\t\tNo matches found.");
		}
		
		// calculate number of matchings
		int matchingsCounter = 0;
		for (IMergeHandler match: matches) {
			if (!match.isEmpty() && useTraClusMerging) {
				matchingsCounter += ((TraClusSweepMerge) match).edges.size() / 2;
			}
		}
		
		numberOfMatchings += matchingsCounter;
		
		// every found match should be merged by an own merge handler
		for (IMergeHandler match: matches) {
			if (!match.isEmpty()) {
				// System.out.println("\t\t" + match);
				match.mergePoints();
				
				// for later cleanup
				if (useTraClusMerging && removeStartAndEndOfMatchings) {
					oldAggNodes.addAll(((TraClusSweepMerge) match).getOldAggNodes());
				}
			}
		}
		
		// remove all old agg nodes
		if (useTraClusMerging && removeStartAndEndOfMatchings) {
			for (AggNode node: oldAggNodes) {
				aggContainer.extractNode(node);
			}
		}
	}
	
	/* HELPER METHODS */
	
	private void addEdgeToRTree(AggNode startNode, AggNode endNode) {
		// calculate angle distance
		TraClus traclus = new TraClus(null, 2, 0, 0);
		MercatorTransformation transformator = new MercatorTransformation();
		Point s1 = transformator.fromLatLngToPoint(startNode.getLat(), startNode.getLon(), mercatorZoom);
		Point e1 = transformator.fromLatLngToPoint(endNode.getLat(), endNode.getLon(), mercatorZoom);
		Point s2 = transformator.fromLatLngToPoint(fixedStartPoint.getLat(), fixedStartPoint.getLon(), mercatorZoom);
		Point e2 = transformator.fromLatLngToPoint(fixedEndPoint.getLat(), fixedEndPoint.getLon(), mercatorZoom);
		double edgeLength1 = traclus.measureDistanceFromPointToPoint(s1, e1);
		double edgeLength2 = traclus.measureDistanceFromPointToPoint(s2, e2);
		
		double edgeAngle;
		if (edgeLength1 > edgeLength2) {
			edgeAngle = traclus.measureAngleDistance(s1, e1, s2, e2);
		} else {
			edgeAngle = traclus.measureAngleDistance(s2, e2, s1, e1);
		}
		
		float[] coords = {
			(float) s1.coordinates[0],
			(float) s1.coordinates[1],
			(float) e1.coordinates[0],
			(float) e1.coordinates[1],
			(float) edgeAngle,
			(float) edgeLength1
		};
		
		float[] dimensions = { 1, 1, 1, 1, 1, 1 };
		
		// add edge as vector
		Vector<AggNode> entry = new Vector<>();
		entry.add(startNode);
		entry.add(endNode);
		
		// save edge in rtree
		rTreeEdges.insert(coords, dimensions, entry);
	}
	
	private List<Vector<AggNode>> getNearEdgesFromRTree(ILocation currentPoint, ILocation nextPoint) {
		TraClus traclus = new TraClus(null, 2, 0, 0);
		MercatorTransformation transformator = new MercatorTransformation();
		Point s1 = transformator.fromLatLngToPoint(currentPoint.getLat(), currentPoint.getLon(), mercatorZoom);
		Point e1 = transformator.fromLatLngToPoint(nextPoint.getLat(), nextPoint.getLon(), mercatorZoom);
		Point s2 = transformator.fromLatLngToPoint(fixedStartPoint.getLat(), fixedStartPoint.getLon(), mercatorZoom);
		Point e2 = transformator.fromLatLngToPoint(fixedEndPoint.getLat(), fixedEndPoint.getLon(), mercatorZoom);
		double edgeLength1 = traclus.measureDistanceFromPointToPoint(s1, e1);
		double edgeLength2 = traclus.measureDistanceFromPointToPoint(s2, e2);
		
		double edgeAngle;
		if (edgeLength1 > edgeLength2) {
			edgeAngle = traclus.measureAngleDistance(s1, e1, s2, e2);
		} else {
			edgeAngle = traclus.measureAngleDistance(s2, e2, s1, e1);
		}
		
		float[] coords = {
			(float) s1.coordinates[0],
			(float) s1.coordinates[1],
			(float) e1.coordinates[0],
			(float) e1.coordinates[1],
			(float) edgeAngle,
			(float) edgeLength1
		};
		
		float[] dimensions = {
			(float) (rTreeSearchFactor * maxTraClusDistance * rTreeDimX),
			(float) (rTreeSearchFactor * maxTraClusDistance * rTreeDimY),
			(float) (rTreeSearchFactor * maxTraClusDistance * rTreeDimX),
			(float) (rTreeSearchFactor * maxTraClusDistance * rTreeDimY),
			(float) (rTreeSearchFactor * maxTraClusDistance * rTreeDimAngle),
			(float) (rTreeSearchFactor * maxTraClusDistance * rTreeDimLength)
		};
		
		// return edges in the near
		return rTreeEdges.search(coords, dimensions);
	}
	
	private double computeDistanceBetweenEdgesTraClus(ILocation startPoint1, ILocation endPoint1, ILocation startPoint2, ILocation endPoint2) {
		// traclus setup
		TraClus traclus = new TraClus(null, 2, 0, 2);
		traclus.w_perp = w_perp;
		traclus.w_par = w_par;
		traclus.w_angle = w_angle;
		
		MercatorTransformation transformator = new MercatorTransformation();
		Point s1 = transformator.fromLatLngToPoint(startPoint1.getLat(), startPoint1.getLon(), 12);
		Point e1 = transformator.fromLatLngToPoint(endPoint1.getLat(), endPoint1.getLon(), 12);
		Point s2 = transformator.fromLatLngToPoint(startPoint2.getLat(), startPoint2.getLon(), 12);
		Point e2 = transformator.fromLatLngToPoint(endPoint2.getLat(), endPoint2.getLon(), 12);
		return traclus.computeDistanceBetweenTwoLineSegments(s1, e1, s2, e2);
	}
	
	private List<List<AggNode>> getPathsByDepth(Set<AggNode> nearPoints, int minDepth, int maxDepth) {
		List<List<AggNode>> paths = new ArrayList<List<AggNode>>();
		for (AggNode matchStartNode: nearPoints) {
			List<AggNode> path = new ArrayList<AggNode>();
			path.add(matchStartNode);
			addPaths(paths, path, 1, minDepth, maxDepth);
		}
		return paths;
	}
	
	private void addPaths(List<List<AggNode>> paths, List<AggNode> path, int depth, int minDepth, int maxDepth) {
		if (depth > maxDepth) {
			return;
		}
		
		// add out nodes
		if (path.get(depth - 1).getOut() != null) {
			for (AggConnection outConn: path.get(depth - 1).getOut()) {
				AggNode outNode = outConn.getTo();
				path.add(outNode);
				if (depth >= minDepth) {
					ArrayList<AggNode> pathCopy = new ArrayList<AggNode>();
					pathCopy.addAll(path);
					paths.add(pathCopy);
				}
				addPaths(paths, path, depth + 1, minDepth, maxDepth);
				path.remove(path.size() - 1);
			}
		}
	}
}