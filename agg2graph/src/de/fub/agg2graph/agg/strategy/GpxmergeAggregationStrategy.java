/*******************************************************************************
   Copyright 2015 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggNode;
import de.fub.agg2graph.agg.AggregationStrategyFactory;
import de.fub.agg2graph.agg.IEdgeMergeHandler;
import de.fub.agg2graph.agg.IMergeHandler;
import de.fub.agg2graph.agg.MergeHandlerFactory;
import de.fub.agg2graph.agg.TraceDistanceFactory;
import de.fub.agg2graph.structs.ClassObjectEditor;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;

public class GpxmergeAggregationStrategy extends AbstractEdgeAggregationStrategy {
	private static final Logger logger = Logger
			.getLogger("agg2graph.agg.gpxmerge.strategy");

	public double maxInitDistance = 50;
	public double maxPathDifference = 0.00039;
//	public boolean connectMergesWithNonMatchings = true;
	
	
	/**
	 * Preferably use the {@link AggregationStrategyFactory} for creating
	 * instances of this class.
	 */
	public GpxmergeAggregationStrategy() {
		TraceDistanceFactory.setClass(GpxmergeAnglePerpDistance.class);
		traceDistance = TraceDistanceFactory.getObject();
		MergeHandlerFactory.setClass(GpxmergePriorityMerge.class);
		baseMergeHandler = (IEdgeMergeHandler) MergeHandlerFactory.getObject();
	}

	@Override
	public void aggregate(GPSSegment segment, boolean isAgg) {
		logger.setLevel(Level.ALL);

		mergeHandler = (IEdgeMergeHandler) baseMergeHandler.getCopy();

		matches = new ArrayList<IMergeHandler>();

		ArrayList<AggNode> irrelevantNodes = new ArrayList<AggNode>();
		
		// insert first segment without changes
		if (aggContainer.getCachingStrategy() == null
				|| aggContainer.getCachingStrategy().getNodeCount() == 0) {
			int i = 0;
			while (i < segment.size()) {
				AggNode node = new AggNode(segment.get(i), aggContainer);
				node.setID("A-" + segment.get(i).getID());
				addNodeToAgg(aggContainer, node);
				lastNode = node;
				i++;
			}
			return;
		}

		lastNode = null;
		int i = 0;
		// step 1: find starting point
		// get close edges, within x meters (merge candidates)
		Set<AggConnection> nearEdges = null;
		boolean matchedBefore = true;
		while (i < segment.size() - 1) {
			GPSPoint firstPoint = segment.get(i);
			GPSPoint secondPoint = segment.get(i + 1);
			GPSEdge currentEdge = new GPSEdge(firstPoint, secondPoint);
			nearEdges = aggContainer.getCachingStrategy().getCloseConnections(
					currentEdge, maxInitDistance);
			boolean addNode = true;
			logger.warning(nearEdges.size() + " near edges near edge " + currentEdge.toString());
			for(AggConnection edge:nearEdges){
				logger.fine(edge.toString());
			}
			if (nearEdges.size() == 0) {
			} else {
				mergeHandler.setAggContainer(aggContainer);
				Iterator<AggConnection> itNear = nearEdges.iterator();
				while (itNear.hasNext()) {
					AggConnection near = itNear.next();
					// nodes were set irrelevant to avoid matching one segment with itself	- matchings excluded here	
					if (near.getFrom().isRelevant()){
						Object[] distReturn = traceDistance.getPathDifference(
								near.toPointList(), currentEdge.toPointList(), 0,
								mergeHandler);
						double dist = (Double) distReturn[0];
						logger.finer(near.toString() +" and " + currentEdge + "have a distance of " + dist);
						logger.finest(near.toString() +" has latitudes " + near.getFrom().getLat() +" and " + near.getTo().getLat() );
						logger.finest(currentEdge.toString() +" has latitudes " + currentEdge.getFrom().getLat() +" and " + currentEdge.getTo().getLat() );
						if (dist < maxPathDifference) {
							addNode = false;
							mergeHandler.addAggNodes(near);
							mergeHandler.addGPSPoints(currentEdge);
							mergeHandler.setDistance(dist);
							if (!mergeHandler.isEmpty()) {

								logger.fine("MergeHandler includes Aggregation Nodes:");
								for(AggNode node:mergeHandler.getAggNodes()){
									logger.fine(node.toString());
								}
								logger.fine("MergeHandler includes GPS Points:");
								for(GPSPoint point:mergeHandler.getGpsPoints()){
									logger.fine(point.toString());
								}

								if (!matchedBefore){
									mergeHandler.setBeforeNode(lastNode);
								}

								mergeHandler.processSubmatch();
								// connect to previous node
								// remember outgoing node (for later connection)

								lastNode = null;

							}
							if (i == segment.size() - 2){
								mergeHandler.setAfterNode(null);
							}
							matchedBefore = true;
						}

					}

				}
			}
			if (addNode) {
				AggNode node = new AggNode(firstPoint, aggContainer);
				node.setID("A-" + firstPoint.getID());
				// nodes are set irrelevant to avoid matching one segment with itself		
				node.setRelevant(false);
				irrelevantNodes.add(node);
				addNodeToAgg(aggContainer, node);
				if (matchedBefore){
					mergeHandler.setAfterNode(node);
				}
				lastNode = node;
				if (i == segment.size() - 2){
					AggNode second = new AggNode(secondPoint, aggContainer);
					second.setID("A-" + secondPoint.getID());
					// nodes are set irrelevant to avoid matching one segment with itself		
					second.setRelevant(false);
					irrelevantNodes.add(second);
					addNodeToAgg(aggContainer, second);
				}
				matchedBefore = false;
			}
			i++;
		}
		// step 2 and 3 of 3: ghost points, merge everything
		mergeHandler.mergePoints();
		// nodes were set irrelevant to avoid matching one segment with itself		
		for (AggNode irrelevant : irrelevantNodes){
			irrelevant.setRelevant(true);
		}
	}
	
}
