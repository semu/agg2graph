/*******************************************************************************
   Copyright 2015 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.util.List;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggNode;
import de.fub.agg2graph.agg.IMergeHandler;
import de.fub.agg2graph.agg.ITraceDistance;
import de.fub.agg2graph.distances.PerpEdgeDistance;
import de.fub.agg2graph.structs.CartesianCalc;
import de.fub.agg2graph.structs.ClassObjectEditor;
import de.fub.agg2graph.structs.GPSCalc;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;

public class GpxmergePerpEdgeDistance implements ITraceDistance {

	
	private PerpEdgeDistance ped = null;

	
	/**
	 * Compute the difference of an edge of a single trajectory to an edge of the aggregation. 
	 * A difference of Double.MAX_Value indicates no crossing of perpendicular and edge. Assuming startIndex of 0 
	 * and edges as input.
	 * 
	 * @param aggPath
	 * @param tracePoints
	 * @param startIndex
	 * @param dmh
	 * @return Object[] { double bestValue, int bestValueLength }
	 */
	@Override
	public Object[] getPathDifference(List<AggNode> aggPath,
			List<GPSPoint> tracePoints, int startIndex, IMergeHandler dmh) {
		
		if (aggPath.size() == 2 && tracePoints.size() == 2) {
			return new Object[] {
					
					getPerpEdgeDistance(
							new GPSEdge(aggPath.get(0).getConnectionTo(aggPath.get(1))),
							new GPSEdge(tracePoints.get(startIndex),
									tracePoints.get(startIndex + 1))), 1 };
		}
		return new Object[] { Double.MAX_VALUE, 0 };
	}

	
	public double getPerpEdgeDistance(GPSEdge aggEdge,
			GPSEdge currentEdge) {
		

		return ped.calculate(aggEdge, currentEdge);
		
	}

	public GpxmergePerpEdgeDistance() {
		super();
		ped = new PerpEdgeDistance();
	}



	@Override
	public List<ClassObjectEditor> getSettings() {
		return null;
	}
}
