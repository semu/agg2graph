/*******************************************************************************
   Copyright 2015 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.fub.agg2graph.agg.AggNode;
import de.fub.agg2graph.agg.IMergeHandler;
import de.fub.agg2graph.agg.ITraceDistance;
import de.fub.agg2graph.distances.AnglePerpDistance;
import de.fub.agg2graph.distances.SimpleAngleLInfinityFrechetDistance;
import de.fub.agg2graph.distances.SubTrajectoryEdgeDistance;
import de.fub.agg2graph.structs.ClassObjectEditor;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;

public class GpxFrechetDistance implements ITraceDistance {

	public double weight = 0.000035;
	
	private SubTrajectoryEdgeDistance salifd = null;
	
	/**
	 * Compute the difference of an edge of a single trajectory to an edge of the aggregation. 
	 * A difference of Double.MAX_Value indicates no crossing of perpendicular and edge. Assuming startIndex of 0 
	 * and edges as input.
	 * 
	 * @param aggPath
	 * @param tracePoints
	 * @param startIndex
	 * @param dmh
	 * @return Object[] { double bestValue, int bestValueLength }
	 */
	@Override
	public Object[] getPathDifference(List<AggNode> aggPath,
			List<GPSPoint> tracePoints, int startIndex, IMergeHandler dmh) {
		
		if (aggPath.size() >= 2 && tracePoints.size() >= 2) {
			//convert Node-Lists to Edge-Lists
			List<GPSEdge> aggSub = new ArrayList<GPSEdge>();
			Iterator<AggNode> aggIt = aggPath.iterator();
			AggNode lastNode = null;
			while (aggIt.hasNext()){
				AggNode currentNode = aggIt.next();
				if (lastNode != null){
					GPSEdge aggEdge = new GPSEdge(lastNode, currentNode);
					aggSub.add(aggEdge);
				}
				lastNode = currentNode;
			}
			List<GPSEdge> traceSub = new ArrayList<GPSEdge>();
			Iterator<GPSPoint> traceIt = tracePoints.iterator();
			GPSPoint lastPoint = null;
			while (traceIt.hasNext()){
				GPSPoint currentPoint = traceIt.next();
				if (lastPoint != null){
					GPSEdge traceEdge = new GPSEdge(lastPoint, currentPoint);
					traceSub.add(traceEdge);
				}
				lastPoint = currentPoint;
			}
			
			return new Object[] {
					
					getSubTrajectoryEdgeDistance(
							aggSub,
							traceSub), 1 };
		}
		return new Object[] { Double.MAX_VALUE, 0 };
	}

	
	public double getSubTrajectoryEdgeDistance(List<GPSEdge> l1,
			List<GPSEdge> l2) {
		return salifd.calculate(l1, l2, weight);
		
	}

	public GpxFrechetDistance() {
		super();
		salifd = new SimpleAngleLInfinityFrechetDistance();
	}



	@Override
	public List<ClassObjectEditor> getSettings() {
		return null;
	}
}
