/*******************************************************************************
   Copyright 2015 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 ******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggContainer;
import de.fub.agg2graph.agg.AggNode;
import de.fub.agg2graph.agg.IMergeHandler;
import de.fub.agg2graph.agg.ISubTrajectoryMergeHandler;
import de.fub.agg2graph.graph.RamerDouglasPeuckerFilter;
import de.fub.agg2graph.input.Globals;
import de.fub.agg2graph.structs.ClassObjectEditor;
import de.fub.agg2graph.structs.GPSCalc;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.ILocation;
import de.fub.agg2graph.structs.SubTrajectoryMatch;
import de.fub.agg2graph.structs.SubTrajectoryMatchMap;
import de.fub.agg2graph.ui.gui.jmv.Layer;
import de.fub.agg2graph.ui.gui.RenderingOptions;
import de.fub.agg2graph.ui.gui.jmv.TestUI;

public class GpxFrechetPriorityMerge implements ISubTrajectoryMergeHandler {

	public boolean onlyMoveToActualCrossings = true;

	public int pathDepth = 3;

	public double maxConnectEdge = 20.0; 

	public double connectThreshold = 50.0; 

	public double angleThreshold = 75.0; 
	
	public boolean mergeConnections = true;

	public boolean avoidLongPerpendiculars = true;

	private static final Logger logger = Logger
			.getLogger("agg2graph.agg.default.merge");

	private ArrayList<SubTrajectoryMatchMap> matchMaps = null;
	// The TreeMap stores matchings stored in sets which might have the same matching distance
	private SubTrajectoryMatchMap matchMap = null;
	// in order to store a match until completion
	private SubTrajectoryMatch tempMatch = null;
	private SubTrajectoryMatch lastMatch = null;

	private AggNode inNode;
	private AggNode outNode;

	private AggContainer aggContainer;
	private RenderingOptions roMatchGPS;
	// cleaning stuff
	private RamerDouglasPeuckerFilter rdpf = new RamerDouglasPeuckerFilter(0,
			125);

	private double distance = 10;

	public GpxFrechetPriorityMerge() {
		// debugging
		logger.setLevel(Level.ALL);
		roMatchGPS = new RenderingOptions();
		roMatchGPS.color = Color.PINK;
		//logger.setLevel(Level.OFF);

	}

	public GpxFrechetPriorityMerge(AggContainer aggContainer) {
		this();

		this.aggContainer = aggContainer;
	}

	@Override
	public AggContainer getAggContainer() {
		return aggContainer;
	}

	@Override
	public void setAggContainer(AggContainer aggContainer) {
		this.aggContainer = aggContainer;
	}

	@Override
	public List<AggNode> getAggNodes() {
		if (tempMatch != null){
			if (tempMatch.getNodes() != null){
				return tempMatch.getNodes();
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.agg.IMergeHandler#addAggNode(de.fub.agg2graph.agg.AggNode)
	 * expecting exactly two AggNodes creating an AggConnection
	 */
	@Override
	public void addAggNode(AggNode aggNode) {
		if (tempMatch==null){
			tempMatch = new SubTrajectoryMatch();
		}
		if (tempMatch.getNodes() == null){
			tempMatch.setNodes(new ArrayList<AggNode>());
		}
		tempMatch.getNodes().add(aggNode);
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.agg.IMergeHandler#addAggNodes(java.util.List)
	 * expecting a list of exactly two AggNodes creating an AggConnection
	 */
	@Override
	public void addAggNodes(List<AggNode> aggNodes) {
		if (tempMatch==null){
			tempMatch = new SubTrajectoryMatch();
		}
		if (tempMatch.getNodes() == null){
			tempMatch.setNodes(new ArrayList<AggNode>());
		}
		tempMatch.getNodes().addAll(aggNodes);
	}

	@Override
	public List<GPSPoint> getGpsPoints() {
		if (tempMatch != null){
			if (tempMatch.getPoints() != null){
				return tempMatch.getPoints();
			}
		}
		return null;
	}

	@Override
	public void addGPSPoints(List<GPSPoint> gpsPoints) {
		if (tempMatch==null){
			tempMatch = new SubTrajectoryMatch();
		}
		if (tempMatch.getPoints() == null){
			tempMatch.setPoints(new ArrayList<GPSPoint>());
		}
		tempMatch.getPoints().addAll(gpsPoints);
	}

	@Override
	public void addGPSPoint(GPSPoint gpsPoint) {
		if (tempMatch==null){
			tempMatch = new SubTrajectoryMatch();
		}
		if (tempMatch.getPoints() == null){
			tempMatch.setPoints(new ArrayList<GPSPoint>());
		}
		tempMatch.getPoints().add(gpsPoint);
	}

	@Override
	public void processSubmatch() {
		if (matchMaps == null){
			matchMaps = new ArrayList<SubTrajectoryMatchMap>();
		}

		if (matchMap == null){
			matchMap = new SubTrajectoryMatchMap();
		}

		if (tempMatch.isComplete()){
			// add tempMatch to matchMap
			matchMap.storeSubTrajectoryMatch(tempMatch);
			lastMatch = tempMatch;
			tempMatch = null;
		}

	}

	@Override
	public double getDistance() {
		return distance;
	}

	@Override
	public void setDistance(double dist) {
		this.tempMatch.setDist(dist);
	}

	@Override
	public void mergePoints() {

		if (matchMap == null){
			return;
		}

		Iterator<SubTrajectoryMatchMap> matchIt = matchMaps.iterator();

		while (matchIt.hasNext()){
			matchMap = matchIt.next();

			HashMap<AggNode, GPSPoint> toBeMoved = new HashMap<AggNode, GPSPoint>();
			//			HashMap<AggNode, HashSet<EdgeMatch>> alreadyMovedMatches = new HashMap<AggNode, HashSet<EdgeMatch>>();
			TreeSet<GPSEdge> usedEdges = new TreeSet<GPSEdge>();
			TreeSet<GPSEdge> oldCons = new TreeSet<GPSEdge>();
			ArrayList<List<ILocation>> lines = new ArrayList<List<ILocation>>();


			for (TreeSet<SubTrajectoryMatch> set : matchMap.values()){

				for (SubTrajectoryMatch stm : set){

					//order agg, find connections. 

					List<AggConnection> cons = stm.getCons();

					List<GPSEdge> edges = stm.getEdges();

					//iterate agg and then ind edges

					Iterator<AggConnection> conIt = cons.iterator();

					while (conIt.hasNext()){
						AggConnection con = conIt.next();

						Iterator<GPSEdge> edgeIt = edges.iterator();

						while (edgeIt.hasNext()){
							GPSEdge edge = edgeIt.next();

							GPSEdge conClone = new GPSEdge(new GPSPoint(con.getFrom().getLat(), con.getFrom().getLon()), new GPSPoint(con.getTo().getLat(), con.getTo().getLon()));
							GPSPoint crossingFrom = conClone.getPerpCrossingFrom(edge);
							if (Math.abs(edge.getAngle()-conClone.getAngle()) < angleThreshold || !avoidLongPerpendiculars){
								if ((edge.pointWithinBounds(crossingFrom) || !onlyMoveToActualCrossings) && !toBeMoved.containsKey(con.getFrom()) && (GPSCalc.getDistance(con.getFrom(), crossingFrom) < connectThreshold || !avoidLongPerpendiculars)){
									GPSPoint newPoint = new GPSPoint();
									newPoint.setLat((crossingFrom.getLat() + con.getWeight() * con.getFrom().getLat()) / (con.getWeight()+1));
									newPoint.setLon((crossingFrom.getLon() + con.getWeight() * con.getFrom().getLon()) / (con.getWeight()+1));
									if (!toBeMoved.containsKey(con.getTo())) oldCons.add(conClone);
									usedEdges.add(edge);
									toBeMoved.put(con.getFrom(), newPoint);
									List<ILocation> line = new ArrayList<ILocation>(2);
									line.add(conClone.getFrom());
									line.add(crossingFrom);
									lines.add(line);
								}

								GPSPoint crossingTo = conClone.getPerpCrossingTo(edge);
								if ((edge.pointWithinBounds(crossingTo) || !onlyMoveToActualCrossings) && !toBeMoved.containsKey(con.getTo()) && (GPSCalc.getDistance(con.getTo(), crossingTo) < connectThreshold || !avoidLongPerpendiculars)){
									GPSPoint newPoint = new GPSPoint();
									newPoint.setLat((crossingTo.getLat() + con.getWeight() * con.getTo().getLat()) / (con.getWeight()+1));
									newPoint.setLon((crossingTo.getLon() + con.getWeight() * con.getTo().getLon()) / (con.getWeight()+1));
									if (!toBeMoved.containsKey(con.getFrom())) oldCons.add(conClone);
									usedEdges.add(edge);
									toBeMoved.put(con.getTo(), newPoint);
									List<ILocation> line = new ArrayList<ILocation>(2);
									line.add(conClone.getTo());
									line.add(crossingTo);
									lines.add(line);
								}
							}

						}


					}
				}
			}
			//actually move the nodes in the aggregation
			//HashSet<AggNode> alreadyMoved = new HashSet<AggNode>();
			Iterator<AggNode> toMove = toBeMoved.keySet().iterator();
			while(toMove.hasNext()){
				AggNode agg = toMove.next();
				GPSPoint pos = toBeMoved.get(agg);
				agg.setLat(pos.getLat());
				agg.setLon(pos.getLon());
				//				alreadyMoved.add(agg);
			}
			HashSet<AggConnection> alreadySet = new HashSet<AggConnection>();
			HashSet<AggNode> alreadyMoved = new HashSet<AggNode>(toBeMoved.keySet());

			for (Double dist : matchMap.keySet()){
				TreeSet<SubTrajectoryMatch> set = matchMap.get(dist);
				for (SubTrajectoryMatch stm : set){
					for (AggConnection con : stm.getCons()){
						if (alreadyMoved.contains(con.getTo()) && alreadyMoved.contains(con.getFrom())){
							if (!alreadySet.contains(con)){
								con.setWeight(con.getWeight()+1); 
								alreadySet.add(con);
							}
						}
					}
				}
			}

			//order alreadyMoved by connection order / connect first with beforeNode and last with afterNode

			LinkedList<AggNode> movedOrdered = new LinkedList<AggNode>();

			AggNode rootNode = null;
			int rootIndex = 0;
			boolean unconnectedAggregation = false;
			boolean canConnect = true;

			while (!alreadyMoved.isEmpty() && canConnect)
			{
				Iterator<AggNode> movedIt = alreadyMoved.iterator();

				if (movedIt.hasNext()){
					if (rootNode == null){
						rootNode = movedIt.next();
					}
					movedOrdered.add(rootIndex, rootNode);
					AggNode currentNode = rootNode;
					int currentIndex = rootIndex;
					boolean deadEnd = false;
					while (!deadEnd){
						deadEnd = true;
						for(AggConnection outCon : currentNode.getOut()){
							if (alreadyMoved.contains(outCon.getTo())){
								deadEnd = false;
								movedOrdered.add(currentIndex + 1, outCon.getTo());
								currentIndex++;
								currentNode = outCon.getTo();
							}
						}
					}
					deadEnd = false;
					currentNode = rootNode;
					currentIndex = rootIndex;
					while (!deadEnd){
						deadEnd = true;
						for(AggConnection inCon : currentNode.getIn()){
							if (alreadyMoved.contains(inCon.getFrom())){
								deadEnd = false;
								movedOrdered.add(currentIndex, inCon.getFrom());
								currentNode = inCon.getFrom();
							}
						}
					}
					alreadyMoved.removeAll(movedOrdered);
					if (!alreadyMoved.isEmpty()){
						unconnectedAggregation = true;
						AggNode agg = movedOrdered.getLast();
						Set<GPSPoint> indSet = matchMap.getIndForAgg(agg.getID());
						rootNode = null;
						rootNode = findConnectedAggregation(alreadyMoved, rootNode, indSet);
						agg = movedOrdered.getFirst();
						indSet = matchMap.getIndForAgg(agg.getID());
						rootNode = findConnectedAggregation(alreadyMoved, rootNode, indSet);
						if (rootNode != null){
							boolean bestBefore = true;
							double bestDistance = Double.MAX_VALUE;
							int bestIndex = 0;
							for (int i = 0; i < movedOrdered.size(); i++){
								agg = movedOrdered.get(i);
								double distRootOuttoOrderedStart = getSmallestDistanceStartToOut(rootNode, agg);
								if (distRootOuttoOrderedStart < bestDistance){
									bestBefore = true;
									bestDistance = distRootOuttoOrderedStart;
									bestIndex = i;
								}
								double distRootStarttoOrderedOut = getSmallestDistanceStartToOut(agg, rootNode);
								if (distRootStarttoOrderedOut < bestDistance){
									bestBefore = false;
									bestDistance = distRootStarttoOrderedOut;
									bestIndex = i+1;
								}
							}
							if (bestBefore){
								//traverse InNodes and check for bestIndex -1
								boolean found = true;
								while(found){
									found = false;
									AggNode ordered = movedOrdered.get(bestIndex);
									Iterator<AggConnection> inIt = ordered.getIn().iterator();
									while (inIt.hasNext() && bestIndex > 0){
										AggNode in = inIt.next().getFrom();
										if (in.equals(movedOrdered.get(bestIndex - 1))){
											bestIndex--;
											found = true;
										}
									}

								}
							}
							else{
								//traverse OutNodes and check for bestIndex +1
								boolean found = true;
								while(found){
									found = false;
									AggNode ordered = movedOrdered.get(bestIndex - 1);
									Iterator<AggConnection> outIt = ordered.getOut().iterator();
									while (outIt.hasNext() && bestIndex < movedOrdered.size() ){
										AggNode out = outIt.next().getTo();
										if (out.equals(movedOrdered.get(bestIndex))){
											bestIndex++;
											found = true;
										}
									}
								}
							}

							rootIndex = bestIndex;
						}
						else{
							logger.fine("not able to connect " + movedOrdered.getFirst().toString() + " and " + movedOrdered.getLast().toString() + " with " + alreadyMoved.toString());
							canConnect = false;
						}
					}

					if (alreadyMoved.isEmpty()){
						Iterator<AggNode> fromBeginning = movedOrdered.listIterator();
						while (fromBeginning.hasNext() && aggContainer.connect(matchMap.getBeforeNode(), fromBeginning.next(), maxConnectEdge, connectThreshold, angleThreshold) == null);							
						Iterator<AggNode> fromEnding = movedOrdered.descendingIterator();
						while (fromEnding.hasNext() && aggContainer.connect(fromEnding.next(), matchMap.getAfterNode(), maxConnectEdge, connectThreshold, angleThreshold) == null);
						if (unconnectedAggregation){
							Iterator<AggNode> aggIt = movedOrdered.iterator();
							AggNode before = null;
							while (aggIt.hasNext()){
								AggNode current = aggIt.next();
								if (before != null){
									if (before.getPathTo(current, pathDepth) == null && current.getPathTo(before, pathDepth) == null /*&& current.getPathFromTo(before, pathDepth) == null */){
										List<AggConnection> cons = current.getPathFromTo(before, pathDepth);
										if (cons.size()>0 && mergeConnections){
											// merge connections
											boolean found = true;
											while (found){
												found = false;
												Iterator<AggConnection> inIt = before.getIn().iterator();
												boolean unchanged = true;
												while (unchanged && inIt.hasNext()){
													AggConnection con = inIt.next();
													if (cons.contains(con)){
														AggNode mergeTarget = con.getFrom();
														before = aggContainer.mergeNodes(before, mergeTarget);
														unchanged = false;
														found = true;
													}
												}
											}
										}
										if (before.getPathTo(current, pathDepth) == null)
										{
											double connectWalk = 0.0;
											while (aggContainer.connect(before, current, maxConnectEdge, connectThreshold-connectWalk, angleThreshold) == null && connectWalk < connectThreshold){
												if (before.getIn().size()==1){
													AggConnection con = before.getIn().iterator().next();
													connectWalk += con.getLength();
													before = con.getFrom();
												}
												else{
													break;
												}
											}
										}
									}
								}
								before = current;
							}
						}
					}
				}
			}

			TestUI ui = (TestUI) Globals.get("ui");
			if (ui == null) {
				return;
			}
			Layer matchingLayer = ui.getLayerManager().getLayer("matching");
			Layer mergingLayer = ui.getLayerManager().getLayer("merging");

			for (GPSEdge edge : oldCons){
				ArrayList<ILocation> list = new ArrayList<ILocation>();
				list.add(edge.getFrom());
				list.add(edge.getTo());
				matchingLayer.addObject(list); 
			}
			for (GPSEdge edge : usedEdges){
				ArrayList<ILocation> list = new ArrayList<ILocation>();
				list.add(edge.getFrom());
				list.add(edge.getTo());
				matchingLayer.addObject(list); 
			}

			for ( List<ILocation> line : lines) {
				mergingLayer.addObject(line);
			}
		}

	}


	private double getSmallestDistanceStartToOut(AggNode outNode, AggNode startNode) {
		Iterator<AggConnection> itOut = outNode.getOut().iterator();
		double distance = GPSCalc.getDistanceTwoPoints(outNode, startNode);;
		while (itOut.hasNext()){
			AggConnection con = itOut.next();
			double tempDist = GPSCalc.getDistanceTwoPoints(con.getTo(), startNode);
			if (tempDist < distance){
				distance = tempDist;
			}
		}
		return distance;
	}

	private AggNode findConnectedAggregation(HashSet<AggNode> alreadyMoved,
			AggNode rootNode, Set<GPSPoint> indSet) {
		if (indSet != null){
			Iterator<GPSPoint> indIt = indSet.iterator();
			while (indIt.hasNext() && rootNode == null)
			{
				GPSPoint ind = indIt.next();
				Set<AggNode> aggSet = matchMap.getAggForInd(ind.getID());
				Iterator<AggNode> aggIt = aggSet.iterator();
				while (aggIt.hasNext() && rootNode == null){
					AggNode agg = aggIt.next();
					if (alreadyMoved.contains(agg)){
						rootNode = agg;
					}
				}
			}
		}
		return rootNode;
	}

	@Override
	public AggNode getInNode() {
		return inNode;
	}

	@Override
	public AggNode getOutNode() {
		return outNode;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (TreeSet<SubTrajectoryMatch> set : matchMap.values()){
			for (SubTrajectoryMatch match : set){
				sb.append(match.toString()).append(", ");
			}
		}

		return String.format("MergeHandler:\n\tMatches: %s", sb);
	}

	@Override
	public boolean isEmpty() {
		return !tempMatch.isComplete();
	}

	@Override
	public void setBeforeNode(AggNode lastNode) {
		if (matchMap == null) {
			matchMap = new SubTrajectoryMatchMap();
		}
		matchMap.setBeforeNode(lastNode);
		tempMatch.setBefore(lastNode);
	}

	@Override
	public void setAfterNode(AggNode lastNode) {
		if (matchMap!=null){
			matchMap.setAfterNode(lastNode);
			matchMaps.add(matchMap);
			matchMap = new SubTrajectoryMatchMap();
		}
		if (lastMatch != null){
			lastMatch.setAfter(lastNode);
			// DEBUG
			//			TreeSet<EdgeMatch> matchSet = matchMap.get(lastMatch.getDist());
			//			EdgeMatch test = matchSet.first();
			//			boolean set = test.getAfter() != null;
		}
	}

	@Override
	public void addAggNodes(AggConnection bestConn) {
		List<AggNode> agg = new ArrayList<AggNode>();
		agg.add(bestConn.getFrom());
		agg.add(bestConn.getTo());
		addAggNodes(agg);
	}

	@Override
	public void addGPSPoints(GPSEdge edge) {
		List<GPSPoint> tra = new ArrayList<GPSPoint>();
		tra.add(edge.getFrom());
		tra.add(edge.getTo());
		addGPSPoints(tra);
	}
	
	@Override
	public IMergeHandler getCopy() {
		GpxFrechetPriorityMerge object = new GpxFrechetPriorityMerge();
		object.matchMap = this.matchMap;
		object.onlyMoveToActualCrossings = this.onlyMoveToActualCrossings;
		object.pathDepth = this.pathDepth;
		object.maxConnectEdge = this.maxConnectEdge;
		object.connectThreshold = this.connectThreshold;
		object.angleThreshold = this.angleThreshold;
		object.mergeConnections = this.mergeConnections;
		object.avoidLongPerpendiculars = this.avoidLongPerpendiculars;
		object.matchMaps = this.matchMaps;
		object.inNode = this.inNode;
		object.outNode = this.outNode;
		return object;
	}

	@Override
	public List<ClassObjectEditor> getSettings() {
		List<ClassObjectEditor> result = new ArrayList<ClassObjectEditor>();
		result.add(new ClassObjectEditor(this, Arrays.asList(new String[] {
				"aggContainer", "distance", "rdpf" })));
		result.add(new ClassObjectEditor(this.rdpf));
		return result;
	}
}
