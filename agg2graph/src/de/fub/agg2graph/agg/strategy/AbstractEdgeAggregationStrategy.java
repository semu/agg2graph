/*******************************************************************************
   Copyright 2015 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.fub.agg2graph.agg.IEdgeMergeHandler;
import de.fub.agg2graph.structs.ClassObjectEditor;

public abstract class AbstractEdgeAggregationStrategy extends AbstractAggregationStrategy{
	protected IEdgeMergeHandler baseMergeHandler;
	protected IEdgeMergeHandler mergeHandler;
	
	@Override
	// it is necessary to copy this function so that it can access the more direct baseMergeHandler
	public List<ClassObjectEditor> getSettings() {
		List<ClassObjectEditor> result = new ArrayList<ClassObjectEditor>();
		result.add(new ClassObjectEditor(this, Arrays.asList(new String[] {
				"baseMergeHandler", "aggContainer" })));
		result.add(new ClassObjectEditor(getTraceDist()));
		result.add(new ClassObjectEditor(this.baseMergeHandler, Arrays
				.asList(new String[] { "distance", "rdpf", "aggContainer" })));
		return result;
	}
}
