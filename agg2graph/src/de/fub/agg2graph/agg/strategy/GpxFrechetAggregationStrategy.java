/*******************************************************************************
   Copyright 2015 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggNode;
import de.fub.agg2graph.agg.AggregationStrategyFactory;
import de.fub.agg2graph.agg.ISubTrajectoryMergeHandler;
import de.fub.agg2graph.agg.IMergeHandler;
import de.fub.agg2graph.agg.MergeHandlerFactory;
import de.fub.agg2graph.agg.TraceDistanceFactory;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;

public class GpxFrechetAggregationStrategy extends AbstractSubTrajectoryAggregationStrategy {
	private static final Logger logger = Logger
			.getLogger("agg2graph.agg.gpxmerge.strategy");

	public double maxInitDistance = 100;
	public double maxPathDifference = 0.0012;
	
	private HashSet<AggNode> matched = null; 
	
	/**
	 * Preferably use the {@link AggregationStrategyFactory} for creating
	 * instances of this class.
	 */
	public GpxFrechetAggregationStrategy() {
		TraceDistanceFactory.setClass(GpxFrechetDistance.class);
		traceDistance = TraceDistanceFactory.getObject();
		MergeHandlerFactory.setClass(GpxFrechetPriorityMerge.class);
		baseMergeHandler = (ISubTrajectoryMergeHandler) MergeHandlerFactory.getObject();
	}

	@Override
	public void aggregate(GPSSegment segment, boolean isAgg) {
		logger.setLevel(Level.ALL);

		mergeHandler = (ISubTrajectoryMergeHandler) baseMergeHandler.getCopy();

		matched = new HashSet<AggNode>();
		
//		matches = new ArrayList<IMergeHandler>();

		ArrayList<AggNode> irrelevantNodes = new ArrayList<AggNode>();
		
		// insert first segment without changes
		if (aggContainer.getCachingStrategy() == null
				|| aggContainer.getCachingStrategy().getNodeCount() == 0) {
			int i = 0;
			while (i < segment.size()) {
				AggNode node = new AggNode(segment.get(i), aggContainer);
				node.setID("A-" + segment.get(i).getID());
				addNodeToAgg(aggContainer, node);
				lastNode = node;
				i++;
			}
			return;
		}

		lastNode = null;
		int i = 0;
		// step 1: find starting point
		// get close edges, within x meters (merge candidates)
		Set<AggConnection> nearEdges = null;
		boolean matchedBefore = true;
		while (i < segment.size() - 1) {
			GPSPoint firstPoint = segment.get(i);
			GPSPoint secondPoint = segment.get(i + 1);
			GPSEdge currentEdge = new GPSEdge(firstPoint, secondPoint);
			nearEdges = aggContainer.getCachingStrategy().getCloseConnections(
					currentEdge, maxInitDistance);
			boolean addNode = true;
			logger.warning(nearEdges.size() + " near edges near edge " + currentEdge.toString());
			for(AggConnection edge:nearEdges){
				logger.fine(edge.toString());
			}
			if (nearEdges.size() == 0) {
			} else {
				mergeHandler.setAggContainer(aggContainer);
				Iterator<AggConnection> itNear = nearEdges.iterator();
				while (itNear.hasNext()) {
					AggConnection near = itNear.next();
					// nodes were set irrelevant to avoid matching one segment with itself	- matchings excluded here	
					if (near.getFrom().isRelevant()){
						Object[] distReturn = traceDistance.getPathDifference(
								near.toPointList(), currentEdge.toPointList(), 0,
								mergeHandler);
						double dist = (Double) distReturn[0];
						logger.finer(near.toString() +" and " + currentEdge + "have a distance of " + dist);
						logger.finest(near.toString() +" has latitudes " + near.getFrom().getLat() +" and " + near.getTo().getLat() );
						logger.finest(currentEdge.toString() +" has latitudes " + currentEdge.getFrom().getLat() +" and " + currentEdge.getTo().getLat() );
						if (dist < maxPathDifference) {
							boolean alreadyCatched = false;
							if (matched.contains(near.getFrom()) || matched.contains(near.getTo())) {
								alreadyCatched = true;
							}
							if (!alreadyCatched){
								List<AggNode> nodes = new ArrayList<AggNode>();
								nodes.add(near.getFrom());
								nodes.add(near.getTo());
								List<GPSPoint> points = new ArrayList<GPSPoint>();
								points.addAll(currentEdge.toPointList());
								int rootIndex = i;
								int currentIndexLeft = i;
								int currentIndexRight = i+1;
								boolean active = true;
								double lastDistance = dist;

								while (active){

									active = false;

									//left extension aggregation
									Iterator<AggConnection> conIt = nodes.get(0).getIn().iterator();
									boolean found = false;
									while(conIt.hasNext() && !found){
										AggNode testNode = conIt.next().getFrom();
										List<AggNode> testNodes = new ArrayList<AggNode>();
										testNodes.addAll(nodes);
										testNodes.add(0, testNode);
										Object[] testDistReturn = traceDistance.getPathDifference(
												testNodes, points, 0,
												mergeHandler);
										double testDist = (Double) testDistReturn[0];
										if (testDist < maxPathDifference){
											nodes = testNodes;
											found = true;
											active = true;
											lastDistance = testDist;
										}
									}

									//right extension aggregation
									conIt = nodes.get(nodes.size()-1).getOut().iterator();
									found = false;
									while(conIt.hasNext() && !found){
										AggNode testNode = conIt.next().getTo();
										List<AggNode> testNodes = new ArrayList<AggNode>();
										testNodes.addAll(nodes);
										testNodes.add(testNode);
										Object[] testDistReturn = traceDistance.getPathDifference(
												testNodes, points, 0,
												mergeHandler);
										double testDist = (Double) testDistReturn[0];
										if (testDist < maxPathDifference){
											nodes = testNodes;
											found = true;
											active = true;
											lastDistance = testDist;
										}
									}

									//left extension trajectory
									if (currentIndexLeft > 0){
										GPSPoint testPoint = segment.get(currentIndexLeft - 1);
										List<GPSPoint> testPoints = new ArrayList<GPSPoint>();
										testPoints.addAll(points);
										testPoints.add(0, testPoint);
										Object[] testDistReturn = traceDistance.getPathDifference(
												nodes, testPoints, 0,
												mergeHandler);
										double testDist = (Double) testDistReturn[0];
										if (testDist < maxPathDifference){
											points = testPoints;
											currentIndexLeft--;
											active = true;
											lastDistance = testDist;
										}
									}

									//right extension trajectory

									if (currentIndexRight < segment.size() - 1){
										GPSPoint testPoint = segment.get(currentIndexRight + 1);
										List<GPSPoint> testPoints = new ArrayList<GPSPoint>();
										testPoints.addAll(points);
										testPoints.add(testPoint);
										Object[] testDistReturn = traceDistance.getPathDifference(
												nodes, testPoints, 0,
												mergeHandler);
										double testDist = (Double) testDistReturn[0];
										if (testDist < maxPathDifference){
											points = testPoints;
											currentIndexRight++;
											active = true;
											lastDistance = testDist;
										}
									}
								}

								Iterator<AggNode> nodeIt = nodes.iterator();
								while (nodeIt.hasNext()){
									if (matched.contains(nodeIt.next())){
										alreadyCatched = true;
									}
								}

								matched.addAll(nodes);

								if (!alreadyCatched){
									mergeHandler.addAggNodes(nodes);
									mergeHandler.addGPSPoints(points);
									mergeHandler.setDistance(lastDistance);
									if (!mergeHandler.isEmpty()) {

										logger.fine("MergeHandler includes Aggregation Nodes:");
										for(AggNode node:mergeHandler.getAggNodes()){
											logger.fine(node.toString());
										}
										logger.fine("MergeHandler includes GPS Points:");
										for(GPSPoint point:mergeHandler.getGpsPoints()){
											logger.fine(point.toString());
										}

										if (!matchedBefore){
											mergeHandler.setBeforeNode(lastNode);
										}

										mergeHandler.processSubmatch();
										// connect to previous node
										// remember outgoing node (for later connection)

										lastNode = null;

									}
								}

							}
							addNode = false;
							if (i == segment.size() - 2){
								mergeHandler.setAfterNode(null);
							}
							matchedBefore = true;
						}

					}

				}
			}
			if (addNode) {
				AggNode node = new AggNode(firstPoint, aggContainer);
				node.setID("A-" + firstPoint.getID());
				// nodes are set irrelevant to avoid matching one segment with itself		
				node.setRelevant(false);
				irrelevantNodes.add(node);
				addNodeToAgg(aggContainer, node);
				if (matchedBefore){
					mergeHandler.setAfterNode(node);
				}
				lastNode = node;
				if (i == segment.size() - 2){
					AggNode second = new AggNode(secondPoint, aggContainer);
					second.setID("A-" + secondPoint.getID());
					// nodes are set irrelevant to avoid matching one segment with itself		
					second.setRelevant(false);
					irrelevantNodes.add(second);
					addNodeToAgg(aggContainer, second);
				}
				matchedBefore = false;
			}
			i++;
		}
		// step 2 and 3 of 3: ghost points, merge everything
		mergeHandler.mergePoints();
		// nodes were set irrelevant to avoid matching one segment with itself		
		for (AggNode irrelevant : irrelevantNodes){
			irrelevant.setRelevant(true);
		}
	}
}
