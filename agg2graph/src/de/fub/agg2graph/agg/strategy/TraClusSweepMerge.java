/*******************************************************************************
Copyright 2014  Ferhat Beyaz, Sebastian Müller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/
package de.fub.agg2graph.agg.strategy;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggContainer;
import de.fub.agg2graph.agg.AggNode;
import de.fub.agg2graph.agg.IMergeHandler;
import de.fub.agg2graph.agg.PointGhostPointPair;
import de.fub.agg2graph.input.Globals;
import de.fub.agg2graph.structs.ClassObjectEditor;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.ILocation;
import de.fub.agg2graph.traclus.LineSegmentId;
import de.fub.agg2graph.traclus.MercatorTransformation;
import de.fub.agg2graph.traclus.Point;
import de.fub.agg2graph.traclus.TraClus;
import de.fub.agg2graph.traclus.Trajectory;
import de.fub.agg2graph.ui.gui.jmv.Layer;
import de.fub.agg2graph.ui.gui.RenderingOptions;
import de.fub.agg2graph.ui.gui.jmv.TestUI;

public class TraClusSweepMerge implements IMergeHandler {
	
	public boolean mergeNotCrossingSweepLine = false; // merge should be done, even when sweepCrossesMultipleEdges is false
	public int minLns = 1;
	public List<Vector<ILocation>> edges; // first is edge from agg, then edge from gps, then second matched edge from agg, etc.
	
	// contains only matched points/nodes
	private List<AggNode> aggNodes = null;
	private List<GPSPoint> gpsPoints = null;
	
	// helper stuff
	private List<PointGhostPointPair> pointGhostPointPairs;
	private AggNode inNode;
	private AggNode outNode;
	private int mercatorZoom = 12;
	private TraClus traclus;
	private List<AggNode> oldAggNodes;
	
	private AggContainer aggContainer;
	private RenderingOptions roMatchGPS;
	
	private double distance = 40;
	
	public TraClusSweepMerge() {
		// debugging
		roMatchGPS = new RenderingOptions();
		roMatchGPS.color = Color.PINK;
		aggNodes = new ArrayList<AggNode>();
		gpsPoints = new ArrayList<GPSPoint>();
		oldAggNodes = new ArrayList<AggNode>();
		edges = new ArrayList<Vector<ILocation>>();
	}
	
	public TraClusSweepMerge(AggContainer aggContainer) {
		this();
		this.aggContainer = aggContainer;
	}
	
	public TraClusSweepMerge(AggContainer aggContainer, List<AggNode> aggNodes, List<GPSPoint> gpsPoints) {
		this(aggContainer);
		this.aggNodes = aggNodes;
		this.gpsPoints = gpsPoints;
	}
	
	@Override
	public void processSubmatch() {
		inNode = aggNodes.get(0);
	}
	
	@Override
	public void mergePoints() {
		showDebugInfo();
		
		// add nodes
		AggNode lastNode = null;
		AggConnection conn = null;
		for (AggNode node: getAggNodes()) {
			if (lastNode == null) {
				lastNode = node;
				continue;
			}
			
			// make sure that they are connected
			conn = lastNode.getConnectionTo(node);
			if (conn == null) continue;
			
			conn.tryToFill();
			lastNode = node;
		}
		
		aggContainer.removeLostNodes();
		
		/*
		 optional DEBUG output 
		// System.out.println("\t\tMerging agg nodes:");
		for (AggNode node: aggNodes) {
			// System.out.println("\t\t\t" + node.getID() + " (" + node.getLat() + ", " + node.getLon() + ")");
		}
		
		// System.out.println("\t\tMerging gps points:");
		for (GPSPoint point: gpsPoints) {
			// System.out.println("\t\t\t" + point.getID() + " (" + point.getLat() + ", " + point.getLon() + ")");
		}
		*/
		
		// use sweep algorithm to merge the matching trajectories
		mergeSweep(aggNodes, gpsPoints);
	}
	
	private void mergeSweep(List<AggNode> agg, List<GPSPoint> tra) {
		// sort agg nodes by occurence in aggregation
		agg = sort(agg);
		
		// transformator setup for x-y to lat-lon conversion
		MercatorTransformation transformator = new MercatorTransformation();
		
		// traclus setup for representative line calculation
		List<Trajectory> trajectoryList = new ArrayList<Trajectory>();
		traclus = new TraClus(trajectoryList, 2, 0, minLns);
		
		// transform trajectories into traclus data structure
		int dimension = 2;
		traclus.currComponentId = 1;
		traclus.componentIdArray = new ArrayList<Integer>(traclus.trajectoryPartitionsAsPoints.size());
		traclus.MIN_LINESEGMENT_LENGTH = 0.1;
		
		// add all edges as line segments to the same traclus cluster
		int lineSegmentId = 0;
		for (int i = 0; i < agg.size() - 1; i++) {
			Point startPoint = transformator.fromLatLngToPoint(agg.get(i).getLat(), agg.get(i).getLon(), mercatorZoom);
			Point endPoint = transformator.fromLatLngToPoint(agg.get(i + 1).getLat(), agg.get(i + 1).getLon(), mercatorZoom);
			
			Point trajectoryPartitionPoint = new Point(dimension * 2);
			for (int j = 0; j < dimension; j++) {
				trajectoryPartitionPoint.coordinates[j] = startPoint.coordinates[j];
				trajectoryPartitionPoint.coordinates[dimension + j] = endPoint.coordinates[j];
			}
			
			traclus.idArray.add(new LineSegmentId(lineSegmentId++, 0));
			traclus.trajectoryPartitionsAsPoints.add(trajectoryPartitionPoint);
			traclus.componentIdArray.add(0); // all belong to the same cluster component
		}
		
		for (int j = 0; j < tra.size() - 1; j++) {
			Point startPoint = transformator.fromLatLngToPoint(tra.get(j).getLat(), tra.get(j).getLon(), mercatorZoom);
			Point endPoint = transformator.fromLatLngToPoint(tra.get(j + 1).getLat(), tra.get(j + 1).getLon(), mercatorZoom);
			
			Point trajectoryPartitionPoint = new Point(dimension * 2);
			for (int i = 0; i < dimension; i++) {
				trajectoryPartitionPoint.coordinates[i] = startPoint.coordinates[i];
				trajectoryPartitionPoint.coordinates[dimension + i] = endPoint.coordinates[i];
			}
			
			traclus.idArray.add(new LineSegmentId(lineSegmentId++, 0));
			traclus.trajectoryPartitionsAsPoints.add(trajectoryPartitionPoint);
			traclus.componentIdArray.add(0);
		}
		
		// calculate representative with traclus
		traclus.constructCluster();
		
		// transform results back into needed format
		if (traclus.clusterList.size() > 0) {
			List<Point> representative = traclus.clusterList.get(0).points;
			List<GPSPoint> representativePoints = new ArrayList<GPSPoint>();
			for (Point p: representative) {
				Point pTransform = transformator.fromPointToLatLng(p, mercatorZoom);
				GPSPoint newPoint = new GPSPoint();
				newPoint.setLat(pTransform.coordinates[0]);
				newPoint.setLon(pTransform.coordinates[1]);
				representativePoints.add(newPoint);
			}
			
			// match points in representative line with agg
			matchRepresentativeLine(representativePoints, agg);
		} else {
			// System.out.println("\t\t\tNo merging, cause sweep line crosses no multiple edges");
		}
	}
	
	private List<AggNode> sort(List<AggNode> agg) {
		// get all connections
		List<AggConnection> cons = new ArrayList<AggConnection>();
		for (AggNode node: agg) {
			for (AggConnection con: node.getIn()) {
				if (!cons.contains(con)) {
					cons.add(con);
				}
			}
			for (AggConnection con: node.getOut()) {
				if (!cons.contains(con)) {
					cons.add(con);
				}
			}
		}
		
		// sort agg connections
		boolean isSorted = false;
		while (!isSorted) {
			isSorted = true;
			int index = 0;
			AggConnection con2Copy = null;
			for (AggConnection con1: cons) {
				for (AggConnection con2: cons) {
					if (con1 != con2) {
						if (con1.getFrom() == con2.getTo() && cons.indexOf(con2) > cons.indexOf(con1)) {
							index = cons.indexOf(con1);
							con2Copy = con2; 
							isSorted = false;
							break;
						}
					}
				}
				if (!isSorted) break;
			}
			
			if (!isSorted) {
				cons.remove(con2Copy);
				cons.add(index >= 1 ? index - 1: 0, con2Copy);
			}
		}
		
		// get sorted agg nodes from agg connections
		List<AggNode> oldAggNodes = agg;
		agg = new ArrayList<AggNode>();
		for (AggConnection con: cons) {
			if (oldAggNodes.contains(con.getFrom())) agg.add(con.getFrom());
			if (cons.indexOf(con) == cons.size() - 1) {
				if (oldAggNodes.contains(con.getTo())) {
					agg.add(con.getTo());
				}
			}
		}
		
		return agg;
	}
	
	private void matchRepresentativeLine(List<GPSPoint> representativePoints, List<AggNode> agg) {
		// match agg nodes
		// System.out.println("\t\tMerging agg nodes (size = " + agg.size() + ") with representative line (size = " + representativePoints.size() + ").");
		if (representativePoints.size() > 0) {
			// add both nodes for later cleanup
			oldAggNodes.add(agg.get(0));
			oldAggNodes.add(agg.get(agg.size() - 1));
			
			// merge start and end agg node
			mergePointPair(agg.get(0), representativePoints.get(0));
			representativePoints.remove(representativePoints.get(0));
			mergePointPair(agg.get(agg.size() - 1), representativePoints.get(representativePoints.size() - 1));
			representativePoints.remove(representativePoints.get(representativePoints.size() - 1));
			
			// merge remaining agg nodes
			for (int i = 1; i < agg.size() - 1; i++) {
				AggNode aggNode = agg.get(i);
				
				// search in representative line for nearest point
				double bestDistance = Double.MAX_VALUE;
				GPSPoint bestPoint = null;
				for (GPSPoint repPoint: representativePoints) {
					double repPointDistance = computeDistanceFromPointToPoint(aggNode, repPoint);
					if (repPointDistance < bestDistance) {
						bestDistance = repPointDistance;
						bestPoint = repPoint;
					}
				}
				
				// move agg node to calculated point
				if (bestPoint != null) {
					mergePointPair(aggNode, bestPoint);
					representativePoints.remove(bestPoint);
				}
			}
		}
		
		// match remaining points
		// System.out.println("\t\tMerging remaining points in representative line (size = " + representativePoints.size() + ").");
		int newId = 0;
		// representativePoints = new ArrayList<GPSPoint>();
		for (GPSPoint point: representativePoints) {
			// get sinTheta and cosTheta
			double sinTheta = traclus.lineSegmentClusters.get(0).sinTheta;
			double cosTheta = traclus.lineSegmentClusters.get(0).cosTheta;
			
			// sort the agg nodes and point by their x values
			double pointOrderingValue = traclus.getXRotation(point.getX(), point.getY(), cosTheta, sinTheta); 
			
			// partition points in agg into two half planes
			List<AggNode> halfPlane1 = new ArrayList<AggNode>();
			List<AggNode> halfPlane2 = new ArrayList<AggNode>();
			for (AggNode node: agg) {
				double nodeOrderingValue = traclus.getXRotation(node.getX(), node.getY(), cosTheta, sinTheta); 
				if (nodeOrderingValue <= pointOrderingValue) {
					halfPlane1.add(node);
				} else {
					halfPlane2.add(node);
				}
			}
			
			// search nearest nodes of each of those half planes
			AggNode nearestNodeHalfPlane1 = null;
			double distance = Double.MAX_VALUE;
			for (AggNode node: halfPlane1) {
				double currentDistance = computeDistanceFromPointToPoint(node, point);
				if (currentDistance < distance) {
					nearestNodeHalfPlane1 = node;
					distance = currentDistance;
				}
			}
			
			AggNode nearestNodeHalfPlane2 = null;
			distance = Double.MAX_VALUE;
			for (AggNode node: halfPlane2) {
				double currentDistance = computeDistanceFromPointToPoint(node, point);
				if (currentDistance < distance) {
					nearestNodeHalfPlane2 = node;
					distance = currentDistance;
				}
			}
			
			// position point between the half planes
			if (nearestNodeHalfPlane1 != null && nearestNodeHalfPlane2 != null) {
				AggNode newNode = new AggNode(point, aggContainer);
				newNode.setID(nearestNodeHalfPlane1.getID() + "-R*-" + newId);
				aggContainer.insertNodeBetween(newNode, nearestNodeHalfPlane1, nearestNodeHalfPlane2);
				agg.add(agg.indexOf(nearestNodeHalfPlane2), newNode);
				// System.out.println("\t\tAdding representative point " + newNode + " between " + nearestNodeHalfPlane1.getID() + " and " + nearestNodeHalfPlane2.getID());
			} else {
				System.out.println("This should not happen!!!");
			}
		}
	}
	
	private void mergePointPair(AggNode aggNode, GPSPoint gpsPoint) {
		/*System.out.println("\t\t\taggNode (weight = " + aggNode.getWeight() + "): " + aggNode.getLat() + "," + aggNode.getLon());
		System.out.println("\t\t\tgpsPoint (weight = " + gpsPoint.getWeight() + "): " + gpsPoint.getLat() + "," + gpsPoint.getLon());
		double factor = gpsPoint.getWeight() / (aggNode.getWeight() + gpsPoint.getWeight());
		Float64Vector newPos = GPSCalc.getDistanceTwoPointsFloat64(aggNode).plus(GPSCalc.getDistanceTwoPointsFloat64(aggNode, gpsPoint).times(factor));
		ILocation newPosCopy = new GPSPoint(newPos.getValue(0), newPos.getValue(1));
		aggContainer.moveNodeTo(aggNode, newPosCopy);*/
		aggContainer.moveNodeTo(aggNode, gpsPoint);
	}
	
	private double computeDistanceFromPointToPoint(ILocation p1, ILocation p2) {
		return Math.sqrt(((p2.getX() - p1.getX()) * (p2.getX() - p1.getX())) + ((p2.getY() - p1.getY()) * (p2.getY() - p1.getY())));
	}
	
	@Override
	public AggContainer getAggContainer() {
		return aggContainer;
	}
	
	@Override
	public void setAggContainer(AggContainer aggContainer) {
		this.aggContainer = aggContainer;
	}
	
	@Override
	public List<AggNode> getAggNodes() {
		return this.aggNodes;
	}
	
	@Override
	public void addAggNode(AggNode aggNode) {
		if (this.aggNodes.size() > 0 && this.aggNodes.get(this.aggNodes.size() - 1).equals(aggNode)) {
			this.aggNodes.remove(this.aggNodes.size() - 1);
		}
		this.aggNodes.add(aggNode);
	}
	
	@Override
	public void addAggNodes(List<AggNode> aggNodes) {
		int i = 0;
		while (aggNodes.size() > i
			&& this.aggNodes.size() > 0
			&& this.aggNodes.get(this.aggNodes.size() - 1).equals(aggNodes.get(i))) {
			this.aggNodes.remove(this.aggNodes.size() - 1);
			i++;
		}
		this.aggNodes.addAll(aggNodes);
	}
	
	@Override
	public List<GPSPoint> getGpsPoints() {
		return gpsPoints;
	}
	
	@Override
	public void addGPSPoints(List<GPSPoint> gpsPoints) {
		int i = 0;
		while (gpsPoints.size() > i
			&& this.gpsPoints.size() > 0
			&& this.gpsPoints.get(this.gpsPoints.size() - 1).equals(gpsPoints.get(i))) {
			this.gpsPoints.remove(this.gpsPoints.size() - 1);
			i++;
		}
		this.gpsPoints.addAll(gpsPoints);
	}
	
	@Override
	public void addGPSPoint(GPSPoint gpsPoint) {
		if (this.gpsPoints.size() > 0
			&& this.gpsPoints.get(this.gpsPoints.size() - 1).equals(gpsPoint)) {
			return;
		}
		this.gpsPoints.add(gpsPoint);
	}
	
	private void showDebugInfo() {
		TestUI ui = (TestUI) Globals.get("ui");
		if (ui == null) {
			return;
		}
		
		Layer matchingLayer = ui.getLayerManager().getLayer("matching");
		Layer mergingLayer = ui.getLayerManager().getLayer("merging");
		
		// clone the lists
		List<ILocation> aggNodesClone = new ArrayList<ILocation>(aggNodes.size());
		for (ILocation loc : aggNodes) {
			if (!loc.isRelevant()) {
				aggNodesClone.add(new GPSPoint(loc));
				matchingLayer.addObject(aggNodesClone);
				aggNodesClone = new ArrayList<ILocation>(aggNodes.size());
			} else {
				aggNodesClone.add(new GPSPoint(loc));
			}
		}
		
		if (aggNodesClone.size() > 0) {
			matchingLayer.addObject(aggNodesClone);
		}
		
		matchingLayer.addObject(gpsPoints);

		// for debugging highlight trace to agg with an arrow
		if (pointGhostPointPairs != null) {
			for (PointGhostPointPair pgpp: pointGhostPointPairs) {
				List<ILocation> line = new ArrayList<ILocation>(2);
				line.add(new GPSPoint(pgpp.point));
				line.add(new GPSPoint(pgpp.ghostPoint));
				mergingLayer.addObject(line);
			}
		}
	}
	
	@Override
	public double getDistance() {
		return distance;
	}
	
	@Override
	public void setDistance(double bestDifference) {
		this.distance = bestDifference;
	}
	
	@Override
	public AggNode getInNode() {
		return inNode;
	}
	
	@Override
	public AggNode getOutNode() {
		return outNode;
	}
	
	@Override
	public String toString() {
		return String.format("MergeHandler:\n\t\t\tGPS: %s\n\t\t\tAgg: %s", gpsPoints, aggNodes);
	}
	
	@Override
	public boolean isEmpty() {
		return gpsPoints.size() == 0 && gpsPoints.size() == 0;
	}
	
	@Override
	public void setBeforeNode(AggNode lastNode) {
		inNode = lastNode;
	}
	
	@Override
	public void addAggNodes(AggConnection bestConn) {
		List<AggNode> agg = new ArrayList<AggNode>();
		agg.add(bestConn.getFrom());
		agg.add(bestConn.getTo());
		addAggNodes(agg);
	}
	
	@Override
	public void addGPSPoints(GPSEdge edge) {
		List<GPSPoint> tra = new ArrayList<GPSPoint>();
		tra.add(edge.getFrom());
		tra.add(edge.getTo());
		addGPSPoints(tra);
	}
	
	@Override
	public IMergeHandler getCopy() {
		TraClusSweepMerge object = new TraClusSweepMerge();
		object.aggContainer = this.aggContainer;
		return object;
	}
	
	public List<AggNode> getOldAggNodes() {
		return oldAggNodes;
	}

	@Override
	public List<ClassObjectEditor> getSettings() {
		List<ClassObjectEditor> result = new ArrayList<ClassObjectEditor>();
		result.add(new ClassObjectEditor(this, Arrays.asList(new String[] {
			"aggContainer", "distance"
		})));
		return result;
	}
}