/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.structs;

import de.fub.agg2graph.schemas.gpx.LinkType;
import de.fub.agg2graph.schemas.gpx.WptType;
import java.util.Date;
import java.util.List;

/**
 *
 * reference implementation for importing data.
 * Probably you don't need so much data, so it is recommended, you write your
 * own class, that looks a bit like this one.
 * @author Christian Windolf christianwindolf@web.de
 */
public class GPXPoint extends GPSPoint {

    private String name;
    private String comment;
    private String desc;
    private String symbol;
    private String type;
    private String fix;
    //in meters
    private double height = Double.NaN;
    private Date time;
    private double declination = Double.NaN;
    private double geoidHeight = Double.NaN;
    private int amountOfSatellites;
    private int dgpsid;
    private double hdop = Double.NaN;
    private double vdop = Double.NaN;
    private double pdop = Double.NaN;
    private double ageOfData = Double.NaN;
    private List<Object> extensions;
    private List<LinkType> linkList;

    public GPXPoint(WptType wayPoint) {
        super(wayPoint.getLat().doubleValue(), wayPoint.getLon().doubleValue());
        name = wayPoint.getName();
        comment = wayPoint.getCmt();
        desc = wayPoint.getDesc();
        symbol = wayPoint.getSym();
        type = wayPoint.getType();
        fix = wayPoint.getFix();

        if (wayPoint.getEle() != null) {
            height = wayPoint.getEle().doubleValue();
        }
        if (wayPoint.getTime() != null) {
            time = wayPoint.getTime().toGregorianCalendar().getTime();
        }

        if (wayPoint.getMagvar() != null) {
            declination = wayPoint.getMagvar().doubleValue();
        }

        if (wayPoint.getGeoidheight() != null) {
            geoidHeight = wayPoint.getGeoidheight().doubleValue();
        }

        linkList = wayPoint.getLink();
        if (wayPoint.getSat() != null) {
            amountOfSatellites = wayPoint.getSat().intValue();
        }

        if (wayPoint.getHdop() != null) {
            hdop = wayPoint.getHdop().doubleValue();
        }

        if (wayPoint.getVdop() != null) {
            vdop = wayPoint.getVdop().doubleValue();
        }

        if (wayPoint.getPdop() != null) {
            pdop = wayPoint.getPdop().doubleValue();
        }

        if (wayPoint.getAgeofdgpsdata() != null) {
            ageOfData = wayPoint.getAgeofdgpsdata().doubleValue();
        }

        if (wayPoint.getDgpsid() != null) {
            dgpsid = wayPoint.getDgpsid().intValue();
        }
        if (extensions != null) {
            extensions = wayPoint.getExtensions().getAny();
        }

    }
    
    public GPXPoint(double lat, double lon){
        super(lat, lon);
    }

    /**
     * name of this element
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * Seconds between the last received DGPS-Signal and the position calculation
     * @return 
     */
    public double getAgeOfData() {
        return ageOfData;
    }

    /**
     * Amount of satellites that where used to determine the postion
     * @return 
     */
    public int getAmountOfSatellites() {
        return amountOfSatellites;
    }

    /**
     * comment about this element
     * @return 
     */
    public String getComment() {
        return comment;
    }

    /**
     * get the magnetic declination.
     * This means the angle between the magnetic and the geographic North Pole
     * @return 
     */
    public double getDeclination() {
        return declination;
    }

    /**
     * description about this element
     * @return 
     */
    public String getDesc() {
        return desc;
    }

    /**
     * ID of the used DPGS station
     * @return 
     */
    public int getDgpsid() {
        return dgpsid;
    }

    /**
     * list of additional information. You got to cast it yourself...
     * @return 
     */
    public List<Object> getExtensions() {
        return extensions;
    }

    /**
     * Kind position determining. GPX allows NONE, 2D, 3D, DGPS and PPS
     * @return 
     */
    public String getFix() {
        return fix;
    }

    /**
     * height in relation to geoid.
     * @return 
     */
    public double getGeoidHeight() {
        return geoidHeight;
    }

    /**
     * horizontal distribution
     * @return 
     */
    public double getHdop() {
        return hdop;
    }

    /**
     * height of this element
     * @return 
     */
    public double getHeight() {
        return height;
    }

    /**
     * List about additional informations
     * @return 
     */
    public List<LinkType> getLinkList() {
        return linkList;
    }

    /**
     * distribution
     * @return 
     */
    public double getPdop() {
        return pdop;
    }

    /**
     * a symbol for this element
     * @return 
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * date and time
     * @return 
     */
    public Date getTime() {
        return time;
    }

    /**
     * classification
     * @return 
     */
    public String getType() {
        return type;
    }

    /**
     * vertical distribution
     * @return 
     */
    public double getVdop() {
        return vdop;
    }

    public void setAgeOfData(double ageOfData) {
        this.ageOfData = ageOfData;
    }

    public void setAmountOfSatellites(int amountOfSatellites) {
        this.amountOfSatellites = amountOfSatellites;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setDeclination(double declination) {
        this.declination = declination;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setDgpsid(int dgpsid) {
        this.dgpsid = dgpsid;
    }

    public void setExtensions(List<Object> extensions) {
        this.extensions = extensions;
    }

    public void setFix(String fix) {
        this.fix = fix;
    }

    public void setGeoidHeight(double geoidHeight) {
        this.geoidHeight = geoidHeight;
    }

    public void setHdop(double hdop) {
        this.hdop = hdop;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setLinkList(List<LinkType> linkList) {
        this.linkList = linkList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPdop(double pdop) {
        this.pdop = pdop;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setVdop(double vdop) {
        this.vdop = vdop;
    }
    
    
}
