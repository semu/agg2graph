package de.fub.agg2graph.structs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggNode;

public class SubTrajectoryMatch implements Comparable<SubTrajectoryMatch>{

	public List<AggNode> getNodes() {
		return nodes;
	}
	public void setNodes(List<AggNode> nodes) {
		this.nodes = nodes;
	}
	public List<GPSPoint> getPoints() {
		return points;
	}
	public void setPoints(List<GPSPoint> points) {
		this.points = points;
	}

	private List<AggNode> nodes = null;
	private List<GPSPoint> points = null;
	private Double dist = null;
	private AggNode before = null;
	private AggNode after = null;
	

	public AggNode getBefore() {
		return before;
	}
	public void setBefore(AggNode before) {
		this.before = before;
	}
	public AggNode getAfter() {
		return after;
	}
	public void setAfter(AggNode after) {
		this.after = after;
	}
	public Double getDist() {
		return dist;
	}
	public void setDist(Double dist) {
		this.dist = dist;
	}
	public SubTrajectoryMatch(List<AggNode> nodes, List<GPSPoint> points, Double dist) {
		super();
		this.nodes = nodes;
		this.points = points;
		this.dist = dist;
	}
	public SubTrajectoryMatch() {
		super();
	}

	public boolean isComplete(){
	    if (nodes==null){
	    	return false;
	    }
	    if (points==null){
	    	return false;
	    }
	    if (dist==null){
	    	return false;
	    }
	    return true;
	}
	
	public List<AggConnection> getCons() {
		List<AggNode> aggs = nodes;
		List<AggConnection> cons = new ArrayList<AggConnection>();
		
		Iterator<AggNode> aggIt = aggs.iterator();
		
		AggNode lastNode = null;
		
		while (aggIt.hasNext()){
			AggNode node = aggIt.next();
			if (lastNode != null){
				AggConnection con = lastNode.getConnectionTo(node);
				if (con != null){
					cons.add(con);
				}
			}
			lastNode = node;
		}
		return cons;
	}
	
	public List<GPSEdge> getEdges() {
		List<GPSPoint> points = this.points;
		List<GPSEdge> edges = new ArrayList<GPSEdge>();
		
		Iterator<GPSPoint> pointIt = points.iterator();
		
		GPSPoint lastPoint = null;
		
		while (pointIt.hasNext()){
			GPSPoint point = pointIt.next();
			if (lastPoint != null){
				GPSEdge edge = new GPSEdge(lastPoint, point);
				edges.add(edge);
			}
			lastPoint = point;
		}
		return edges;
	}
	
	
	@Override
	public int compareTo(SubTrajectoryMatch o) {
		int doubleComp = this.getDist().compareTo(o.getDist());
		if (doubleComp != 0){
			return doubleComp;
		}
		else{
			int stringComp = this.toString().compareTo(o.toString());
			return stringComp;
		}
		
	}
	
	public String toString(){
		return "Match of AggNodes " + this.nodes.toString() + " and Trajectory " + this.points.toString() + " with distance " + this.dist.toString() + ".";
	}
	
}
