package de.fub.agg2graph.structs;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggNode;

public class EdgeMatch implements Comparable<EdgeMatch>{

	private AggConnection con = null;
	private GPSEdge ed = null;
	private Double dist = null;
	private AggNode before = null;
	private AggNode after = null;
	

	public AggNode getBefore() {
		return before;
	}
	public void setBefore(AggNode before) {
		this.before = before;
	}
	public AggNode getAfter() {
		return after;
	}
	public void setAfter(AggNode after) {
		this.after = after;
	}
	public AggConnection getCon() {
		return con;
	}
	public void setCon(AggConnection con) {
		this.con = con;
	}
	public GPSEdge getEd() {
		return ed;
	}
	public void setEd(GPSEdge ed) {
		this.ed = ed;
	}
	public Double getDist() {
		return dist;
	}
	public void setDist(Double dist) {
		this.dist = dist;
	}
	public EdgeMatch(AggConnection con, GPSEdge ed, Double dist) {
		super();
		this.con = con;
		this.ed = ed;
		this.dist = dist;
	}
	public EdgeMatch() {
		super();
	}

	public boolean isComplete(){
	    if (con==null){
	    	return false;
	    }
	    if (ed==null){
	    	return false;
	    }
	    if (dist==null){
	    	return false;
	    }
	    return true;
	}
	@Override
	public int compareTo(EdgeMatch o) {
		int doubleComp = this.getDist().compareTo(o.getDist());
		if (doubleComp != 0){
			return doubleComp;
		}
		else{
			int stringComp = this.toString().compareTo(o.toString());
			return stringComp;
		}
		
	}
	
	public String toString(){
		return "Match of AggConnection " + this.con.toString() + " and GPSEdge " + this.ed.toString() + " with distance " + this.dist.toString() + ".";
	}
	
}
