/*******************************************************************************
   Copyright 2014 Johannes Mitlmeier, Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.structs;

import java.util.Iterator;
import java.util.TreeSet;

import de.fub.agg2graph.agg.AggConnection;

/**
 * An edge in the gps data structures.
 * 
 * @author Johannes Mitlmeier
 * 
 */
public class GPSEdge extends AbstractEdge<GPSPoint> implements Comparable<GPSEdge> {
	
	private static boolean calculateCoordinates = true;
	
	public GPSEdge() {
		super();
	}

	public GPSEdge(GPSPoint from, GPSPoint to) {
		this.setFrom(from);
		this.setTo(to);
	}
	
	/**
	 * Constructor from GPSPoint and distance
	 * @param from
	 * @param to
	 * @param distance
	 */
	public GPSEdge(GPSPoint from, GPSPoint to, float distance) {
		super(from, to, distance);
	}
	
	/**
	 * Constructor from AggConnection
	 * @param aggConnection
	 */
	public GPSEdge(AggConnection aggConnection) {
		this.setFrom(aggConnection.getFrom());
		this.setTo(aggConnection.getTo());
	}
	
	/** TODO MARTINUS **/
	public GPSRegion getRegion() {
		return new GPSRegion(getFrom(), getTo());
	}

	public float getDistance() {
		if (distance == 0)
			this.distance = (float) getFrom().getDistanceTo(getTo());

		return distance;
	}

	public GPSPoint at(double t) {
		return new GPSPoint((1 - t) * from.getLat() + t * to.getLat(), (1 - t)
				* from.getLon() + t * to.getLon());
	}

	public double getNearestDistance(GPSEdge ed) {
		double gradient1 = this.getGradient();
		double gradient2 = ed.getGradient();
		double interception1 = this.getInterception();
		double interception2 = ed.getInterception();
		double sliceX = (interception1 - interception2)
				/ (gradient2 - gradient1);
		if ((this.getTo().getLon() < sliceX && sliceX < this.getFrom().getLon())
				|| (this.getTo().getLon() > sliceX && sliceX > this.getFrom()
						.getLon())) {
			return (0.0);
		} else {
			TreeSet<Double> distances = retrieveDistances(ed);
			return distances.first();
		}
	}

	public double getFarestDistance(GPSEdge ed) {
		TreeSet<Double> distances = retrieveDistances(ed);
		return distances.last();
	}

	public double getAverageDistance(GPSEdge ed) {
		TreeSet<Double> distances = retrieveDistances(ed);
		Iterator<Double> distIt = distances.iterator();
		double avg = 0.0;
		int cnt = 0;
		while (distIt.hasNext()) {
			avg = (avg * cnt + distIt.next()) / (cnt + 1);
			cnt++;
		}
		return avg;
	}

	public TreeSet<Double> retrieveDistances(GPSEdge ed) {
		TreeSet<Double> distances = retrievePerpDistances(ed);
		retrieveEuclideanPointToPointDistances(ed, distances);
		return distances;
	}

	private void retrieveEuclideanPointToPointDistances(GPSEdge ed,
			TreeSet<Double> distances) {
		retrieveEuclideanPointToPointDistances( ed, distances, true);
	}

	private void retrieveEuclideanPointToPointDistances(GPSEdge ed,
			TreeSet<Double> distances, boolean all) {
		double latDist = Math.abs(this.getTo().getLat() - ed.getTo().getLat());
		double longDist = Math.abs(this.getTo().getLon() - ed.getTo().getLon());
		double dist = Math.sqrt(latDist * latDist + longDist * longDist);
		distances.add(dist);
		if (all){
			latDist = Math.abs(this.getTo().getLat() - ed.getFrom().getLat());
			longDist = Math.abs(this.getTo().getLon() - ed.getFrom().getLon());
			dist = Math.sqrt(latDist * latDist + longDist * longDist);
			distances.add(dist);
			latDist = Math.abs(this.getFrom().getLat() - ed.getTo().getLat());
			longDist = Math.abs(this.getFrom().getLon() - ed.getTo().getLon());
			dist = Math.sqrt(latDist * latDist + longDist * longDist);
			distances.add(dist);
		}
		latDist = Math.abs(this.getFrom().getLat() - ed.getFrom().getLat());
		longDist = Math.abs(this.getFrom().getLon() - ed.getFrom().getLon());
		dist = Math.sqrt(latDist * latDist + longDist * longDist);
		distances.add(dist);
	}

	public TreeSet<Double> retrievePerpDistances(GPSEdge ed) {
		double gradient1;
		double gradient2;
		double interception1;
		double interception2;
		double sliceX;
		gradient1 = this.getGradient();
		gradient2 = ed.getGradient();
		interception1 = this.getInterception();
		interception2 = ed.getInterception();
		double interceptionFrom1Per2 = this.getFrom().getLat()
				- this.getFrom().getLon() * (1 / gradient2);
		double interceptionFrom2Per1 = ed.getFrom().getLat()
				- ed.getFrom().getLon() * (1 / gradient1);
		double interceptionTo1Per2 = this.getTo().getLat()
				- this.getTo().getLon() * (1 / gradient2);
		double interceptionTo2Per1 = ed.getTo().getLat() - ed.getTo().getLon()
				* (1 / gradient1);
		TreeSet<Double> distances = new TreeSet<Double>();
		sliceX = (interceptionFrom1Per2 - interception2)
				/ (gradient2 - (1 / gradient2));
		if ((ed.getTo().getLon() < sliceX && sliceX < ed.getFrom().getLon())
				|| (ed.getTo().getLon() > sliceX && sliceX > ed.getFrom()
						.getLon())) {
			double latSlice = interceptionFrom1Per2 + (1 / gradient2) * sliceX;
			double latDist = Math.abs(latSlice - this.getFrom().getLat());
			double longDist = Math.abs(sliceX - this.getFrom().getLon());
			double dist = Math.sqrt(latDist * latDist + longDist * longDist);
			distances.add(dist);
		}
		sliceX = (interceptionFrom2Per1 - interception1)
				/ (gradient1 - (1 / gradient1));
		if ((this.getTo().getLon() < sliceX && sliceX < this.getFrom().getLon())
				|| (this.getTo().getLon() > sliceX && sliceX > this.getFrom()
						.getLon())) {
			double latSlice = interceptionFrom2Per1 + (1 / gradient1) * sliceX;
			double latDist = Math.abs(latSlice - ed.getFrom().getLat());
			double longDist = Math.abs(sliceX - ed.getFrom().getLon());
			double dist = Math.sqrt(latDist * latDist + longDist * longDist);
			distances.add(dist);
		}
		sliceX = (interceptionTo1Per2 - interception2)
				/ (gradient2 - (1 / gradient2));
		if ((ed.getTo().getLon() < sliceX && sliceX < ed.getFrom().getLon())
				|| (ed.getTo().getLon() > sliceX && sliceX > ed.getFrom()
						.getLon())) {
			double latSlice = interceptionTo1Per2 + (1 / gradient2) * sliceX;
			double latDist = Math.abs(latSlice - this.getTo().getLat());
			double longDist = Math.abs(sliceX - this.getTo().getLon());
			double dist = Math.sqrt(latDist * latDist + longDist * longDist);
			distances.add(dist);
		}
		sliceX = (interceptionTo2Per1 - interception1)
				/ (gradient1 - (1 / gradient1));
		if ((this.getTo().getLon() < sliceX && sliceX < this.getFrom().getLon())
				|| (this.getTo().getLon() > sliceX && sliceX > this.getFrom()
						.getLon())) {
			double latSlice = interceptionTo2Per1 + (1 / gradient1) * sliceX;
			double latDist = Math.abs(latSlice - ed.getTo().getLat());
			double longDist = Math.abs(sliceX - ed.getTo().getLon());
			double dist = Math.sqrt(latDist * latDist + longDist * longDist);
			distances.add(dist);
		}
		return distances;
		
	}

	public double getPerpShiftDistance(GPSEdge ed) {
		double distFrom1Per2 = 0;
		double distFrom2Per1 = 0;
		double distTo1Per2 = 0;
		double distTo2Per1 = 0;
		double latSliceFrom1Per2 = 0;
		double latSliceFrom2Per1 = 0;
		double latSliceTo1Per2 = 0;
		double latSliceTo2Per1 = 0;
		double gradient1;
		double gradient2;
		double interception1;
		double interception2;
		double sliceX;
		gradient1 = this.getGradient();
		gradient2 = ed.getGradient();
		interception1 = this.getInterception();
		interception2 = ed.getInterception();
		double interceptionFrom1Per2 = this.getFrom().getLat()
				- this.getFrom().getLon() * (1 / gradient2);
		double interceptionFrom2Per1 = ed.getFrom().getLat()
				- ed.getFrom().getLon() * (1 / gradient1);
		double interceptionTo1Per2 = this.getTo().getLat()
				- this.getTo().getLon() * (1 / gradient2);
		double interceptionTo2Per1 = ed.getTo().getLat() - ed.getTo().getLon()
				* (1 / gradient1);
		double sliceXFrom1Per2 = (interceptionFrom1Per2 - interception2)
				/ (gradient2 - (1 / gradient2));
		if ((ed.getTo().getLon() < sliceXFrom1Per2 && sliceXFrom1Per2 < ed.getFrom().getLon())
				|| (ed.getTo().getLon() > sliceXFrom1Per2 && sliceXFrom1Per2 > ed.getFrom()
						.getLon())) {
			latSliceFrom1Per2 = interceptionFrom1Per2 + (1 / gradient2) * sliceXFrom1Per2;
			double latDist = Math.abs(latSliceFrom1Per2 - this.getFrom().getLat());
			double longDist = Math.abs(sliceXFrom1Per2 - this.getFrom().getLon());
			distFrom1Per2 = Math.sqrt(latDist * latDist + longDist * longDist);
		}
		double sliceXFrom2Per1 = (interceptionFrom2Per1 - interception1)
				/ (gradient1 - (1 / gradient1));
		if ((this.getTo().getLon() < sliceXFrom2Per1 && sliceXFrom2Per1 < this.getFrom().getLon())
				|| (this.getTo().getLon() > sliceXFrom2Per1 && sliceXFrom2Per1 > this.getFrom()
						.getLon())) {
			latSliceFrom2Per1 = interceptionFrom2Per1 + (1 / gradient1) * sliceXFrom2Per1;
			double latDist = Math.abs(latSliceFrom2Per1 - ed.getFrom().getLat());
			double longDist = Math.abs(sliceXFrom2Per1 - ed.getFrom().getLon());
			distFrom2Per1 = Math.sqrt(latDist * latDist + longDist * longDist);
		}
		sliceX = (interceptionTo1Per2 - interception2)
				/ (gradient2 - (1 / gradient2));
		if ((ed.getTo().getLon() < sliceX && sliceX < ed.getFrom().getLon())
				|| (ed.getTo().getLon() > sliceX && sliceX > ed.getFrom()
						.getLon())) {
			latSliceTo1Per2 = interceptionTo1Per2 + (1 / gradient2) * sliceX;
			double latDist = Math.abs(latSliceTo1Per2 - this.getTo().getLat());
			double longDist = Math.abs(sliceX - this.getTo().getLon());
			distTo1Per2 = Math.sqrt(latDist * latDist + longDist * longDist);
		}
		sliceX = (interceptionTo2Per1 - interception1)
				/ (gradient1 - (1 / gradient1));
		if ((this.getTo().getLon() < sliceX && sliceX < this.getFrom().getLon())
				|| (this.getTo().getLon() > sliceX && sliceX > this.getFrom()
						.getLon())) {
			latSliceTo2Per1 = interceptionTo2Per1 + (1 / gradient1) * sliceX;
			double latDist = Math.abs(latSliceTo2Per1 - ed.getTo().getLat());
			double longDist = Math.abs(sliceX - ed.getTo().getLon());
			distTo2Per1 = Math.sqrt(latDist * latDist + longDist * longDist);
		}
		
		double shiftDist = 0;
		int maxDist = 0;
		final int DISTFROM1PER2 = 1;
		final int DISTFROM2PER1 = 2;
		final int DISTTO1PER2 = 3;
		final int DISTTO2PER1 = 4;
		final double WEIGHT = 0.5;
		if (distFrom1Per2 > distFrom2Per1){
			if (distFrom1Per2 > distTo1Per2){
				if (distFrom1Per2 > distTo2Per1){
					maxDist = DISTFROM1PER2;
				}
				else{
					maxDist = DISTTO2PER1;
				}
			}
			else{
				if (distTo1Per2 > distTo2Per1){
					maxDist = DISTTO1PER2;
				}
				else{
					maxDist = DISTTO2PER1;
				}
			}
		}
		else{
			if (distFrom2Per1 > distTo1Per2){
				if (distFrom2Per1 > distTo2Per1){
					maxDist = DISTFROM2PER1;
				}
				else{
					maxDist = DISTTO2PER1;
				}
			}
			else{
				if (distTo1Per2 > distTo2Per1){
					maxDist = DISTTO1PER2;
				}
				else{
					maxDist = DISTTO2PER1;
				}
			}
		}
		if(maxDist == DISTFROM1PER2){
			double latDist = Math.abs(latSliceFrom1Per2 - ed.getFrom().getLat());
			double longDist = Math.abs(sliceXFrom1Per2 - ed.getFrom().getLon());
			shiftDist = Math.sqrt(latDist * latDist + longDist * longDist);
			return distFrom1Per2 + WEIGHT * Math.pow(( 2 * shiftDist / ed.getLength()),4);
		}
		else if(maxDist == DISTFROM2PER1){
			double latDist = Math.abs(latSliceFrom2Per1 - this.getFrom().getLat());
			double longDist = Math.abs(sliceXFrom2Per1 - this.getFrom().getLon());
			shiftDist = Math.sqrt(latDist * latDist + longDist * longDist);
			return distTo2Per1 + WEIGHT * Math.pow(( 2 * shiftDist / ed.getLength()),4);
		}
		else if(maxDist == DISTTO1PER2){
			double latDist = Math.abs(latSliceTo1Per2 - ed.getTo().getLat());
			double longDist = Math.abs(sliceX - ed.getTo().getLon());
			shiftDist = Math.sqrt(latDist * latDist + longDist * longDist);
			return distTo2Per1 + WEIGHT * Math.pow(( 2 * shiftDist / ed.getLength()),4);
		}
		else if(maxDist == DISTTO2PER1){
			double latDist = Math.abs(latSliceTo2Per1 - this.getTo().getLat());
			double longDist = Math.abs(sliceX - this.getTo().getLon());
			shiftDist = Math.sqrt(latDist * latDist + longDist * longDist);
			return distTo2Per1 + WEIGHT * Math.pow(( 2 * shiftDist / ed.getLength()),4);
		}
		return 0;
	}

	public double getGradient() {
		if (calculateCoordinates){
			return (this.getFrom().getLat() - this.getTo().getLat())
					/ (this.getFrom().getLon() - this.getTo().getLon());
		}
		return (this.getFrom().getY() - this.getTo().getY())
				/ (this.getFrom().getX() - this.getTo().getX());
	}

	public double getAngle() {
		double angle = Math.toDegrees(Math.atan((this.getEuDistance()
				* this.getEuDistance() + this.getLatDistance()
				* this.getLatDistance() - this.getLongDistance()
				* this.getLongDistance())
				/ (2 * this.getEuDistance() * this.getLatDistance())));
		angle = Math.toDegrees(Math.atan(this.getLatDistance()
				/ this.getLongDistance()));
		if (this.getTo().getLat() > this.getFrom().getLat()) {
			if (this.getTo().getLon() > this.getFrom().getLon()) {
				// correction on angle in order to have full 360 degrees: 90
				// minus angle
				angle = 90 - angle;
			} else {
				// 270 plus angle
				angle = 270 + angle;
			}
		} else {
			if (this.getTo().getLon() > this.getFrom().getLon()) {
				// 180 minus angle
				angle = 90 + angle;
			} else {
				// 180 plus angle
				angle = 270 - angle;
			}
		}
		return angle;
	}

	private double getLatDistance() {
		return Math.abs(this.getTo().getLat() - this.getFrom().getLat());
	}

	private double getLongDistance() {
		return Math.abs(this.getTo().getLon() - this.getFrom().getLon());
	}

	public double getInterception() {
		if (calculateCoordinates){
			return this.getFrom().getLat() - this.getFrom().getLon()
					* this.getGradient();
		}
		return this.getFrom().getY() - this.getFrom().getX()
				* this.getGradient();
	}

	public String toString() {
		return this.getFrom() + "->" + this.getTo();
	}

	public Double getEuDistance() {
		return this.getFrom().getDistanceTo(this.getTo());
	}

	public double getSquaredEulerDistance() {
		return this.getFrom().getSquaredDistanceTo(this.getTo());
	}
	
	public GPSEdge[] divide(GPSPoint middle) {
		GPSEdge[] ret = new GPSEdge[2];
		//TODO check whether middle is in line
		setTo(middle);
		ret[0] = this;
		ret[1] = new GPSEdge(middle, to);
		return ret;
	}

	@Override
	public int compareTo(GPSEdge o) {
		if (o == null)
			throw new NullPointerException();

		int r = getFrom().compareTo(o.getFrom());
		if (r == 0) {
			return getTo().compareTo(o.getTo());
		} else {
			return r;
		}
	}
	
	public double getLength(){
		return Math.sqrt(Math.pow((this.getFrom().getX() - this.getTo().getX()), 2)	+ Math.pow((this.getFrom().getY()-this.getTo().getY()), 2));
	}
	
	public Double getInverseGradient(){
		return -1 / this.getGradient();
	}
	
	public Double getPerpInterceptionFrom(){
		if (calculateCoordinates){
			return this.getFrom().getLat() - (this.getFrom().getLon() * this.getInverseGradient()) ;
		}

		return this.getFrom().getY() - (this.getFrom().getX() * this.getInverseGradient()) ;

	}

	public Double getPerpInterceptionTo(){
		if (calculateCoordinates){
			return this.getTo().getLat() - (this.getTo().getLon() * this.getInverseGradient()) ;
		}
		return this.getTo().getY() - (this.getTo().getX() * this.getInverseGradient()) ;

	}
	
	public GPSPoint getPerpCrossingFrom(GPSEdge ed2){
		if (calculateCoordinates){
			Double lon = (ed2.getInterception() - this.getPerpInterceptionFrom()) / (this.getInverseGradient() - ed2.getGradient());
			Double lat = lon * this.getInverseGradient() + this.getPerpInterceptionFrom();
			return new GPSPoint(lat, lon);
		}
		Double x = (ed2.getInterception() - this.getPerpInterceptionFrom()) / (this.getInverseGradient() - ed2.getGradient());
		Double y = x * this.getInverseGradient() + this.getPerpInterceptionFrom();
		GPSPoint crossing = new GPSPoint();
		crossing.setX(x);
		crossing.setY(y);
		return crossing;
		
	}
	
	public GPSPoint getPerpCrossingTo(GPSEdge ed2){
		if (calculateCoordinates){
			Double lon = (ed2.getInterception() - this.getPerpInterceptionTo()) / (this.getInverseGradient() - ed2.getGradient());
			Double lat = lon * this.getInverseGradient() + this.getPerpInterceptionTo();
			return new GPSPoint(lat, lon);
		}
		Double x = (ed2.getInterception() - this.getPerpInterceptionTo()) / (this.getInverseGradient() - ed2.getGradient());
		Double y = x * this.getInverseGradient() + this.getPerpInterceptionTo();
		GPSPoint crossing = new GPSPoint();
		crossing.setX(x);
		crossing.setY(y);
		return crossing;
	}
	
	public boolean pointWithinBounds(GPSPoint point){
		Double margin = 0.000000000000005;
		if (point.getLat() <= Math.max(this.getFrom().getLat(), this.getTo().getLat()) + margin){
			if (point.getLat() >= Math.min(this.getFrom().getLat(), this.getTo().getLat()) - margin){
				if (point.getLon() <= Math.max(this.getFrom().getLon(), this.getTo().getLon()) + margin){
					if (point.getLon() >= Math.min(this.getFrom().getLon(), this.getTo().getLon()) - margin){
						return true;
					}
				}
			}
		}
		return false;
	}

	public TreeSet<Double> retrieveWeightedPerpDistances(GPSEdge ed) {
		TreeSet<Double> distances = retrievePerpDistances(ed);

		if (distances.size()>= 1) {
			return distances;	
		}
		//for cases where perpendiculars and edges do not cross, calculate standard point to point distances.
		retrieveEuclideanPointToPointDistances(ed, distances, false);
		Iterator<Double> edIt = distances.iterator();
		Double high = 0.0;
		while(edIt.hasNext()){
			Double cur = edIt.next();
			if (cur > high){
				high = cur;
			}
		}
		
		return distances;
	}
	
}
