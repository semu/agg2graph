/*******************************************************************************
   Copyright 2015 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.structs;

import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import de.fub.agg2graph.agg.AggNode;

public class EdgeMatchMap extends TreeMap<Double, TreeSet<EdgeMatch>>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8417334641341928146L;

    // store matchings for finding connections
	private HashMap<String, Set<GPSPoint>> aggToInd = null;
	private HashMap<String, Set<AggNode>> indToAgg = null;

	private AggNode beforeNode = null;
	private AggNode afterNode = null;

	public void storeEdgeMatch(EdgeMatch em){
		TreeSet<EdgeMatch> matchSet = this.get(em.getDist());
		if (matchSet == null){
			matchSet = new TreeSet<EdgeMatch>();
			matchSet.add(em);
			this.put(em.getDist(), matchSet);
		}
		else {
			matchSet.add(em);
			this.put(em.getDist(), matchSet);
		}
		// store to with from and vice versa to increase ability to connect later
		AggNode agg = em.getCon().getFrom();
		GPSPoint ind = em.getEd().getFrom();
		
		storePoints(agg, ind);
		
		agg = em.getCon().getTo();
		ind = em.getEd().getTo();
		
		storePoints(agg, ind);

		agg = em.getCon().getTo();
		ind = em.getEd().getFrom();
		
		storePoints(agg, ind);
		
		agg = em.getCon().getFrom();
		ind = em.getEd().getTo();
		
		storePoints(agg, ind);
		
	}

	private void storePoints(AggNode agg, GPSPoint ind) {
		Set<GPSPoint> indList = aggToInd.get(agg.getID());
		if (indList == null){
			indList = new TreeSet<GPSPoint>();
		}
		indList.add(ind);
		aggToInd.put(agg.getID(), indList);
		Set<AggNode> aggList = indToAgg.get(ind.getID());
		if (aggList == null){
			aggList = new TreeSet<AggNode>();
		}
		aggList.add(agg);
		indToAgg.put(ind.getID(), aggList);
	}
	
	public AggNode getBeforeNode() {
		return beforeNode;
	}
	public void setBeforeNode(AggNode beforeNode) {
		this.beforeNode = beforeNode;
	}
	public AggNode getAfterNode() {
		return afterNode;
	}
	public void setAfterNode(AggNode afterNode) {
		this.afterNode = afterNode;
	}
	public EdgeMatchMap() {
		super();
		this.aggToInd = new HashMap<String, Set<GPSPoint>>();
		this.indToAgg = new HashMap<String, Set<AggNode>>();
	}

	public Set<GPSPoint> getIndForAgg(String agg) {
		return aggToInd.get(agg);
	}

	public Set<AggNode> getAggForInd(String ind) {
		return indToAgg.get(ind);
	}
	
}
