/*******************************************************************************
   Copyright 2014 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.structs.frechet;

import java.util.ArrayList;
import java.util.ListIterator;

import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSRegion;
import de.fub.agg2graph.structs.ILocation;

public interface ITrace extends Iterable<ILocation> {
	
//	ListIterator<AggConnection> connListIterator();
	ListIterator<GPSEdge> edgeListIterator(ILocation start);

//	ListIterator<AggConnection> connListIterator(ILocation start);
	ListIterator<GPSEdge> edgeListIterator();
	
//	ArrayList<AggConnection> conns();	
	ArrayList<GPSEdge> edges();
	
//	ArrayList<ILocation> connLocations();
	ArrayList<ILocation> edgeLocations();
	
//	ITrace connSubTrace(ILocation start, ILocation stop);
	ITrace edgeSubTrace(ILocation start, ILocation stop);
	
//	boolean connIsEmpty();
	boolean edgeIsEmpty();
	
	String name();
	GPSRegion getGPSRegion();
	
//	ILocation getConnFirstLocation();
	ILocation getEdgeFirstLocation();
	
//	ILocation getConnLastLocation();
	ILocation getEdgeLastLocation();
	
//	void insertConnLocation(int index, ILocation location);
	void insertEdgeLocation(int index, ILocation location);
}

