/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.matching;

import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import de.fub.agg2graph.distances.PointDistance;
import de.fub.agg2graph.structs.GPSPoint;

public class BruteForcePointMatching implements PointMatching{

	@Override
	public TreeMap<GPSPoint, GPSPoint> match(List<List<GPSPoint>> agg,
			List<GPSPoint> tra, PointDistance pd, double threshold) {
		TreeMap<GPSPoint, GPSPoint> matches = new TreeMap<GPSPoint, GPSPoint>();
		Iterator<List<GPSPoint>> itAgg= agg.iterator();
		while (itAgg.hasNext()){
			List<GPSPoint> curAgg = itAgg.next();
			Iterator<GPSPoint> itAT = curAgg.iterator();
			while (itAT.hasNext()){
				GPSPoint curAT = itAT.next();
				Iterator<GPSPoint> itTra= tra.iterator();
				while (itTra.hasNext()){
					GPSPoint curTra = itTra.next();
					if (threshold >= pd.calculate(curAT, curTra)){
						matches.put(curAT, curTra);
					}
				}
			}
		}
		return matches;
	}

}
