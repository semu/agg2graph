/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.matching;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.TreeMap;

import com.infomatiq.jsi.Point;
import com.infomatiq.jsi.Rectangle;
import com.infomatiq.jsi.SpatialIndex;
import com.infomatiq.jsi.rtree.RTree;

import de.fub.agg2graph.distances.PointDistance;
import de.fub.agg2graph.structs.GPSPoint;

public class RTreePointMatching implements PointMatching{

	@Override
	public TreeMap<GPSPoint, GPSPoint> match(List<List<GPSPoint>> agg,
			List<GPSPoint> tra, PointDistance pd, double threshold) {
		SpatialIndex si = new RTree();
		Properties prop = new Properties();
		prop.setProperty("MinNodeEntries", "10");
		prop.setProperty("MaxNodeEntries", "50");
		si.init(prop);
		Iterator<List<GPSPoint>> itAgg= agg.iterator();
		HashMap<Integer, GPSPoint> aggPoints = new HashMap<Integer, GPSPoint>();

		int c = 0;
		while (itAgg.hasNext()){
			List<GPSPoint> curAgg = itAgg.next();
			Iterator<GPSPoint> itAT = curAgg.iterator();
			while (itAT.hasNext()){
				c++;
				GPSPoint curAT = itAT.next();
				Rectangle rec = new Rectangle();
				rec.set((float) curAT.getX(), (float) curAT.getY(), (float) curAT.getX(), (float) curAT.getY());
				si.add(rec, c);
				aggPoints.put(c, curAT);
			}
		}
		Iterator<GPSPoint> itTra= tra.iterator();
		TreeMap<GPSPoint, GPSPoint> matches = new TreeMap<GPSPoint, GPSPoint>();
		while (itTra.hasNext()){
			GPSPoint curTra = itTra.next();
			Point p = new Point((float)curTra.getX(), (float)curTra.getY());
			AggIntProcedure proc = new AggIntProcedure();
			si.nearestN(p, proc, 65535, (float)threshold * 2);
			List<Integer> ids = proc.getIds();
			Iterator<Integer> idsIt = ids.iterator();
			while (idsIt.hasNext()){
				Integer id = idsIt.next();
				GPSPoint poAgg = aggPoints.get(id);
				if (pd.calculate(poAgg, curTra)<= threshold){
					matches.put(poAgg, curTra);
				}
			}
		}
		return matches;
	}

}
