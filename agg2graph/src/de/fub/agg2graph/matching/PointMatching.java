/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.matching;

import java.util.List;
import java.util.TreeMap;

import de.fub.agg2graph.distances.PointDistance;
import de.fub.agg2graph.structs.GPSPoint;

public interface PointMatching {
	public TreeMap<GPSPoint, GPSPoint> match (List<List<GPSPoint>> agg, List<GPSPoint> tra, PointDistance pd, double threshold);
}
