/**
 * *****************************************************************************
 * Copyright 2013 M.Klingen, C.Dresske
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.alternativevision.gpx.beans.GPX;
import org.alternativevision.gpx.beans.Track;
import org.alternativevision.gpx.beans.Waypoint;

/**
 *
 * @author Michael
 */
public class GPX2CSV {

    private SimpleDateFormat sdf = new SimpleDateFormat("y-MM-d'T'k-m-s.S'Z'", Locale.GERMANY);
    private GPX gpx;

    public GPX2CSV(GPX gpx) {
        this.gpx = gpx;
    }

    public boolean saveCSV(File outputfile) {
        boolean rtn = true;
        FileWriter fw = null;
        try {
            fw = new FileWriter(outputfile);
            String firstLine = "Name,Art der Aktivität,Beschreibung";
            fw.write(firstLine);
            String secondLine = ",TYPE,";
            fw.write(secondLine);
            fw.write("");
            String fourthLine = "Segment,Punkt,Breitengrad (°),Längengrad (°),Höhe (m),Peilung (°),Genauigkeit (m),Geschwindigkeit (m/s),Zeit,"
                    + "Leistung (W),Trittfrequenz (Umdrehungen pro Minute),Herzfrequenz (Schläge pro Minute),Akkustatus";
            fw.write(fourthLine);

            for (Track track : gpx.getTracks()) {
                for (Waypoint point : track.getTrackPoints()) {
                    String segment = "";
                    if (track.getNumber() != null) {
                        segment = track.getNumber() + "";
                    }
                    String punkt = "";
                    String lat = point.getLatitude() + "";
                    String lon = point.getLongitude() + "";
                    String elenvation = "";
                    if (point.getElevation() != null) {
                        elenvation = point.getElevation() + "";
                    }
                    String heading = "";
                    if (point.getMagneticDeclination() != null) {
                        heading = point.getMagneticDeclination() + "";
                    }
                    String accurany = "";
                    String speed = "";
                    String time = "";
                    if (point.getTime() != null) {
                        time = sdf.format(point.getTime()) + "";
                    }
                    String power = "";
                    String frequenz = "";
                    String heartrate = "";
                    String akkustatus = "";

                    fw.write(segment + ","
                            + punkt + ","
                            + lat + ","
                            + lon + ","
                            + lat + ","
                            + elenvation + ","
                            + heading + ","
                            + accurany + ","
                            + speed + ","
                            + time + ","
                            + power + ","
                            + frequenz + ","
                            + heartrate + ","
                            + akkustatus + "\n");
                }
                fw.flush();
            }
        } catch (Exception e) {
            rtn = false;
            Logger.getLogger(GPX2CSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    Logger.getLogger(GPX2CSV.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return rtn;
    }
}
