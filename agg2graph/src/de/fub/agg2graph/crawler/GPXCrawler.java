/**
 * *****************************************************************************
 * Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler;

import de.fub.agg2graph.crawler.OptionalParameters;
import de.fub.agg2graph.crawler.SourceHashMap;
import de.fub.agg2graph.crawler.Var;
import de.fub.agg2graph.crawler.loader.GPSiesFileLoader;
import de.fub.agg2graph.crawler.loader.StravaLoader;
import de.fub.agg2graph.crawler.loader.FileLoader;
import de.fub.agg2graph.crawler.loader.MapMyTracksFileLoader;
import de.fub.agg2graph.crawler.loader.OpenStreetMapFileLoader;

import java.util.Map;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class GPXCrawler {

    static {
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
    }
    private static final String USAGE =
            "Please select in the first place a datasource [-d <source>]. "
            + "Required paramters are listed belows as well as optional. Any combination is valid.";
    private static final String HEADER =
            "GPXCrawler.jar - An easy extendable lowcost gpx crawler. Copyright 2013 by FU Berlin.";
    private static final String FOOTER =
            "For more instructions, see our website at:  http://userpage.fu-berlin.de/~semu/";
    private static Map<Integer, String> sources;
    private static Options options;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        sources = new SourceHashMap();
        generateOptions();

        if (args.length == 0) {
            usage(options);
            return;
        }

        CommandLineParser parser = new BasicParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException pe) {
            usage(options);
            return;
        }
        new GPXCrawler(cmd);
    }

    private static void usage(Options options) {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp(80, USAGE, HEADER, options, FOOTER);
    }

    @SuppressWarnings("static-access")
	private static void generateOptions() {
        options = new Options();
        Option ds = OptionBuilder
                .withArgName("args")
                .isRequired(true)
                .hasArgs()
                .withLongOpt("datasource")
                .withDescription(sources.toString())
                .create(Var.DATASOURCE);
        options.addOption(ds);

        options.addOption(Var.USERNAME, "username", true, "username");
        options.addOption(Var.PASSWORD, "password", true, "password");
        options.addOption(Var.TRACK_TYPE, "tracktype", true, "tracktype (walking, jogging)");

        options.addOption(Var.KEY_API, "keyapi", true, "api key");
        Option bb = OptionBuilder
                .withArgName("values")
                .withValueSeparator(',')
                .hasArgs(4)
                .withLongOpt("boundingsbox")
                .withDescription("boundingbox 10.0,51.0,11.2,52.3")
                .create(Var.BOUNDING_BOX);
        options.addOption(bb);
        options.addOption(Var.FILEFORMAT, "fileformat", true, "outputformat (gpx)");

        options.addOption(Var.SLEEP_TIMER, "sleeptimer", true, "sleeptimer between downloading two tracks");
        options.addOption(Var.OFFSET, "offset", true, "offset of nr of tracks in downloadorder");
        options.addOption(Var.COUNT, "count", true, "max nr of tracks to download");
        options.addOption(Var.OUTPUT_DIRECTORY, "resultpath", true, "path to output directory");
        options.addOption(Var.FILEFORMAT, "fileformat", true, "filetype of outputdata");
    }

    private GPXCrawler(CommandLine cmd) {
        if (!cmd.hasOption(Var.DATASOURCE)) {
            usage(options);
            return;
        }
        OptionalParameters opts = new OptionalParameters(cmd);
        try {
            Datasource ds = getDataSource(cmd);
            if (ds == null) {
                usage(options);
                return;
            }
            switch (ds) {
                case GPSIES:
                    if (cmd.hasOption(Var.KEY_API) && cmd.hasOption(Var.BOUNDING_BOX)) {
                        System.out.println(ds.getText() + " ready");
                        FileLoader fileLoader = new GPSiesFileLoader(opts);
                        fileLoader.load();
                    } else {
                        System.out.println(ds.getText() + " needs at least the following paramters -d " + ds.getId() + " -k [KEY] -b [BOUNDINGBOX]");
                    }
                    break;
                case MAPMYTRACKS:
                    if (cmd.hasOption(Var.USERNAME) && cmd.hasOption(Var.PASSWORD)) {
                        System.out.println(ds.getText() + " ready");
                        FileLoader fileLoader = new MapMyTracksFileLoader(opts);
                        fileLoader.load();
                    } else {
                        System.out.println(ds.getText() + " needs at least the following paramters -d " + ds.getId() + " -u [USERNAME] -p [PASSWORD]");
                    }
                    break;
                case OSM:
                    if (cmd.hasOption(Var.BOUNDING_BOX)) {
                        System.out.println(ds.getText() + " ready");
                        FileLoader fileLoader = new OpenStreetMapFileLoader(opts);
                        fileLoader.load();
                    } else {
                        System.out.println(ds.getText() + " needs at least the following paramters -d " + ds.getId() + " -b [BOUNDINGBOX]");
                    }
                    break;
                case Strava:
                    if (cmd.hasOption(Var.USERNAME) && cmd.hasOption(Var.PASSWORD) && cmd.hasOption(Var.KEY_API)) {
                        System.out.println(ds.getText() + " ready");
                        FileLoader fileLoader = new StravaLoader(opts);
                        fileLoader.load();
                    } else {
                        System.out.println(ds.getText() + " needs at least the following paramters -d " + ds.getId() + " -k [KEY] -u [USERNAME] -p [PASSWORD]");
                    }
                    break;
//                case NEW:
//                    //irgendwas
//                    FileLoader fileLoader = new NewLoader(opts);
//                    fileLoader.load();
//                    break;
                default:
                    usage(options);
            }
        } catch (Exception e) {
            e.printStackTrace();
            usage(options);
        }
    }

    private Datasource getDataSource(CommandLine cmd) {
        Datasource ds = null;
        String dataSource = cmd.getOptionValue(Var.DATASOURCE);
        for (Datasource d : Datasource.values()) {
            if (dataSource.equals(d.getId() + "")) {
                ds = d;
                System.out.println("datasource found: " + d.getText());
                break;
            }
        }
        return ds;
    }
}
