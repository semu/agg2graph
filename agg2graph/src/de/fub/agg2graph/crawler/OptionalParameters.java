/**
 * *****************************************************************************
 * Copyright 2013 M.Klingen, C.Dresske
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler;

import java.io.File;
import org.apache.commons.cli.CommandLine;

/**
 *
 * @author Michael
 */
public class OptionalParameters {

    public String username = "";
    public String password = "";
    public TrackTypes trackType = TrackTypes.jogging;
    public String apiKey = "";
    public String boundingBox = "";
    private int sleepTimer = 0;//timeout between download
    private int offset = 0;//offset start download at ....
    private int count = 100;//limit number of max downloads since offset
    private File outputDirectory = new File("\\");//output directory
    private FileFormat fileFormat = FileFormat.GPX;//filetype

    public OptionalParameters(CommandLine cmd) {
        try {
            if (cmd.hasOption(Var.USERNAME)) {
                username = cmd.getOptionValue(Var.USERNAME);
            }
            if (cmd.hasOption(Var.PASSWORD)) {
                password = cmd.getOptionValue(Var.PASSWORD);
            }
            if (cmd.hasOption(Var.BOUNDING_BOX)) {
                String[] bb = cmd.getOptionValues(Var.BOUNDING_BOX);
                for (String b : bb) {
                    boundingBox += b;
                    boundingBox += ",";
                }
                boundingBox = boundingBox.substring(0, boundingBox.length() - 1);
            }
            if (cmd.hasOption(Var.TRACK_TYPE)) {
                try {
                    trackType = TrackTypes.valueOf(cmd.getOptionValue(Var.TRACK_TYPE));
                } catch (Exception e) {
                    throw new IllegalArgumentException("invalid trackType, possible values are: trekking, walking, jogging, skating, crossskating, biking, racingbike, mountainbiking, \n"
                            + "motorbiking, car, riding, canoeing, boating, climbing, flying, train, skiingAlpine, skiingNordic, \n"
                            + "wintersports, geocaching, miscellaneous ");
                }
            }
            if (cmd.hasOption(Var.KEY_API)) {
                apiKey = cmd.getOptionValue(Var.KEY_API);
            }
            if (cmd.hasOption(Var.SLEEP_TIMER)) {
                sleepTimer = Integer.parseInt(cmd.getOptionValue(Var.SLEEP_TIMER));
            }
            if (cmd.hasOption(Var.OFFSET)) {
                offset = Integer.parseInt(cmd.getOptionValue(Var.OFFSET));
            }
            if (cmd.hasOption(Var.COUNT)) {
                count = Integer.parseInt(cmd.getOptionValue(Var.COUNT));
            }
            if (cmd.hasOption(Var.OUTPUT_DIRECTORY)) {
                String path = cmd.getOptionValue(Var.OUTPUT_DIRECTORY);
                outputDirectory = new File(path);
                outputDirectory.mkdirs();
            }
            if (cmd.hasOption(Var.FILEFORMAT)) {
                String ff = cmd.getOptionValue(Var.FILEFORMAT);
                for (FileFormat f : FileFormat.values()) {
                    if (f.getText().equalsIgnoreCase(ff)) {
                        fileFormat = FileFormat.valueOf(ff.toUpperCase());
                    }
                }
            }
            if (offset > count) {
                throw new IllegalArgumentException("offset has to be <= count");
            }
        } catch (NumberFormatException e) {
        }
    }

    public int getSleepTimer() {
        return sleepTimer;
    }

    public int getOffset() {
        return offset;
    }

    public int getCount() {
        return count;
    }

    public File getOutputDirectory() {
        return outputDirectory;
    }

    public FileFormat getFileFormat() {
        return fileFormat;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public TrackTypes getTrackType() {
        return trackType;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getBoundingBox() {
        return boundingBox;
    }
}
