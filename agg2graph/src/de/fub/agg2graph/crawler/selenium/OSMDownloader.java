/**
 * *****************************************************************************
 * Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler.selenium;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 *
 * @author Michael
 */
public class OSMDownloader {

    private WebDriver driver;

    public OSMDownloader() {
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http.level").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.level").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.javascript.StrictErrorReporter.level").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.javascript.host.ActiveXObject.level").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.javascript.host.html.HTMLDocument.level").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.html.HtmlScript.level").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        driver = new HtmlUnitDriver();
    }

    public String getDownloadLink(String url) throws MalformedURLException {
        String rtn = "";
        //System.out.println(url);
        new URL(url); //throws exception if url is invalid
        driver.get(url);
        WebElement visibility = driver.findElement(By.xpath("//*[@id=\"content\"]/table/tbody/tr[8]/td[2]"));
        //System.out.println(visibility.getText());
        if (visibility.getText().contains("Identifiable")) {
            WebElement link = driver.findElement(By.xpath("//*[@id=\"content\"]/table/tbody/tr[1]/td[2]/a"));
            String downloadLink = link.getAttribute("href");
            rtn = downloadLink;
        }
        return rtn;
    }

    public String getTagsLink(String url) throws MalformedURLException {
    	new URL(url); //throws exception if url is invalid
        driver.get(url);
        WebElement visibility = driver.findElement(By.xpath("//*[@id=\"content\"]/table/tbody/tr[7]/td[2]"));
        return visibility.getText();
    }

    public void close() {
        driver.quit();
    }
}
