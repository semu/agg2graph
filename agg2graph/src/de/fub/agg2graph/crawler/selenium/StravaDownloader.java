/*******************************************************************************
Copyright 2013 M.Klingen, C.Dresske

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/

package de.fub.agg2graph.crawler.selenium;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Michael
 */
public class StravaDownloader extends Downloader {

    public StravaDownloader(String username, String password) {
        super(username, password);
        url = "https://www.strava.com/login";
    }

    @Override
    public List<String> getUsernames() {
        List<String> rtn = new ArrayList<String>();
        driver.get(url);
        WebElement usernameElement = driver.findElement(By.id("email"));
        usernameElement.sendKeys(username);
        WebElement passwordElement = driver.findElement(By.id("password"));
        passwordElement.sendKeys(password);

        WebElement button = driver.findElement(By.xpath("//*[@id='login_form']"));
        button.submit();

        try {
            WebElement error = driver
                    .findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div/p"));
            if (error != null) {
                throw new IllegalArgumentException(error.getText());
            }
        } catch (NoSuchElementException e) {
        }

        WebElement following = driver.findElement((By.xpath("/html/body/div[1]/header/nav/div/ul[2]/li[2]/div[1]/a")));
        following.click();
        driver.get(driver.getCurrentUrl() + "/follows");
        List<WebElement> users = driver.findElements(By.xpath("/html/body/div[1]/div/div/div[2]/div[1]/ul/li/h4/a"));

        for (WebElement name : users) {
            String user = name.getText().trim();
            rtn.add(user);
        }
        driver.quit();
        return rtn;
    }
}
