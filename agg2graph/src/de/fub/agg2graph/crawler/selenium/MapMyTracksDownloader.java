/*******************************************************************************
Copyright 2013 M.Klingen, C.Dresske

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/

package de.fub.agg2graph.crawler.selenium;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Michael
 */
public class MapMyTracksDownloader extends Downloader {

    public MapMyTracksDownloader(String username, String password) {
        super(username, password);
        url = "http://www.mapmytracks.com/login";
    }

    @Override
    public List<String> getUsernames() {
        List<String> rtn = new ArrayList<String>();
        driver.get(url);
        WebElement usernameElement = driver.findElement(By.id("un"));
        usernameElement.sendKeys(username);
        WebElement passwordElement = driver.findElement(By.id("pw"));
        passwordElement.sendKeys(password);

        WebElement button = driver.findElement(By.xpath("//div[@id='login']/p/button"));
        button.click();
        try {
            WebElement error = driver.findElement(By.xpath("//*[@id='content']/ul/li"));
            if (error != null) {
                throw new IllegalArgumentException(error.getText());
            }
        } catch (NoSuchElementException e) {
        }

        WebElement following = driver.findElement((By.id("following_count_link")));
        following.click();

        List<WebElement> users = driver.findElements(By.xpath("//div[@id='tracks-list']/table/tbody/tr[position()>1]/td/div/a"));

        for (WebElement name : users) {
            String user = name.getText().split("Member")[0].trim();
            rtn.add(user);
        }
        driver.quit();
        return rtn;
    }
}
