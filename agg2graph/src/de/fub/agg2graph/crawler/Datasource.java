/**
 * *****************************************************************************
 * Copyright 2013 M.Klingen, C.Dresske
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
*****************************************************************************
 */
package de.fub.agg2graph.crawler;

/**
 *
 * @author Michael
 */
public enum Datasource {

    Strava(0, "Strava"),
    GPSIES(1, "GPSies"),
    MAPMYTRACKS(2, "MapMyTracks"),
    OSM(3, "OpenStreetMap");
//    NEW(99, "NEW");
    private String name;
    private int id;

    private Datasource(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return name;
    }
}
