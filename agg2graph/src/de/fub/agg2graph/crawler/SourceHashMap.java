/*******************************************************************************
Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/ 
 package de.fub.agg2graph.crawler;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michael
 */
public final class SourceHashMap extends HashMap<Integer, String> {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5299410320594114793L;
	Map<Integer, String> map;

    public SourceHashMap() {
        map = new HashMap<Integer, String>();
        map.put(Datasource.Strava.getId(), Datasource.Strava + " -" + Var.USERNAME + " -" + Var.PASSWORD + " -" + Var.KEY_API);
        map.put(Datasource.GPSIES.getId(), Datasource.GPSIES + " -" + Var.KEY_API + " -" + Var.BOUNDING_BOX + " [ -" + Var.TRACK_TYPE + " -" + Var.OFFSET + " -" + Var.COUNT + " -" + Var.SLEEP_TIMER + " -" + Var.OUTPUT_DIRECTORY + " -" + Var.FILEFORMAT + "]");
        map.put(Datasource.MAPMYTRACKS.getId(), Datasource.MAPMYTRACKS + " -" + Var.USERNAME + " -" + Var.PASSWORD + " [ -" + Var.OFFSET + " -" + Var.COUNT + " -" + Var.SLEEP_TIMER + " -" + Var.OUTPUT_DIRECTORY + " -" + Var.FILEFORMAT + "]");
        map.put(Datasource.OSM.getId(), Datasource.OSM + " -" + Var.BOUNDING_BOX + " [ -" + Var.OFFSET + " -" + Var.COUNT + " -" + Var.SLEEP_TIMER + " -" + Var.OUTPUT_DIRECTORY + " -" + Var.FILEFORMAT + "]");
//        map.put(Datasource.NEW.getId(), Datasource.NEW + " -");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Integer i : keySet()) {
            sb.append(i);
            sb.append(" ");
            sb.append(get(i));
            sb.append("\n");
        }
        return sb.toString();
    }
}
