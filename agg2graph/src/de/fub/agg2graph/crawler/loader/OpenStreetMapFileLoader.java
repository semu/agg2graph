/**
 * *****************************************************************************
 * Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler.loader;

import de.fub.agg2graph.crawler.FileFormat;
import de.fub.agg2graph.crawler.GPX2CSV;
import de.fub.agg2graph.crawler.OptionalParameters;
import de.fub.agg2graph.crawler.TrackClassifier;
import de.fub.agg2graph.crawler.selenium.OSMDownloader;
import de.fub.agg2graph.crawler.xml.XMLParser;
import de.fub.agg2graph.crawler.xml.osm.OSMContentHandler;
import de.fub.agg2graph.crawler.xml.osm.OSMTrack;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.alternativevision.gpx.GPXParser;
import org.alternativevision.gpx.beans.GPX;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

/**
 *
 * @author Michael
 */
public class OpenStreetMapFileLoader extends FileLoader {

    private OptionalParameters ops;

    public OpenStreetMapFileLoader(OptionalParameters ops) {
        this.ops = ops;
    }

    @Override
    public void load() {
        int page = -1;
        int i = 0;
        boolean running = true;
        while (running) {
            if (i < ops.getOffset() || i > ops.getCount()) {
                running = false;
                continue;
            }
            page++;
            url = "http://api.openstreetmap.org/api/0.6/trackpoints?bbox=" + ops.getBoundingBox() + "&page=" + page;
            try {
                URL url2 = new URL(url);
                URLConnection conn2 = url2.openConnection();
                InputStream is = new BufferedInputStream(conn2.getInputStream());
                XMLParser xml = new XMLParser(new OSMContentHandler<OSMTrack>());
                List<OSMTrack> tracks = xml.parseOSMTracks(is).getAll();
                OSMDownloader downloader = new OSMDownloader();
                for (OSMTrack track : tracks) {
                    i++;
                    if (i <= ops.getOffset() || i > ops.getCount()) {
                        running = false;
                        continue;
                    }
                    try {
                        String link = downloader.getDownloadLink(track.getDownloadLink());

                        if (link.isEmpty()) {
                            continue;
                        }
                        String tags = downloader.getTagsLink(track.getDownloadLink());
                        URL myUrl = new URL(link);
                        System.out.println(myUrl);
                        URLConnection con = myUrl.openConnection();
                        InputStream inputStream = con.getInputStream();

                        String trackType = TrackClassifier.getInstance().getType(tags);
                        File path = new File(ops.getOutputDirectory() + "//" + ops.getFileFormat().getText() + "//" + trackType);
                        if (!path.exists()) {
                            path.mkdirs();
                        }

                        File f = new File(path.getAbsolutePath() + "//" + track.getName());
                        OutputStream outputStream = new FileOutputStream(f);
                        int read;
                        byte[] bytes = new byte[1024];
                        try {
                            BZip2CompressorInputStream bz = new BZip2CompressorInputStream(inputStream);
                            while ((read = bz.read(bytes)) != -1) {
                                outputStream.write(bytes, 0, read);
                            }
                            bz.close();
                        } catch (Exception ex) {
                            //lost in previous reading step!
                            outputStream.write("<?x".getBytes(), 0, 3);
                            outputStream.flush();
                            while ((read = inputStream.read(bytes)) != -1) {
                                outputStream.write(bytes, 0, read);
                            }
                        } finally {
                            outputStream.flush();
                            outputStream.close();
                        }
                        if (ops.getFileFormat() == FileFormat.CSV) {
                            FileInputStream fis = null;
                            try {
                                fis = new FileInputStream(f);
                                GPXParser parser = new GPXParser();
                                GPX gpx = parser.parseGPX(fis);
                                GPX2CSV gpx2csv = new GPX2CSV(gpx);
                                File out = new File(path.getAbsoluteFile() + "//" + track.getName().replace(FileFormat.GPX.getText(), FileFormat.CSV.getText()));
                                gpx2csv.saveCSV(out);
                            } catch (Exception ex) {
                                Logger.getLogger(OpenStreetMapFileLoader.class.getName()).log(Level.SEVERE, null, ex);
                            } finally {
                                if (fis != null) {
                                    fis.close();
                                }
                                f.delete();
                            }
                        }
                        i++;
                        Thread.sleep(ops.getSleepTimer());
                    } catch (MalformedURLException ex) {
                        //continue;
                    } catch (Exception ex) {
                        Logger.getLogger(GPSiesFileLoader.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                downloader.close();
            } catch (MalformedURLException ex) {
            } catch (IOException ex) {
                running = false;
                Logger.getLogger(GPSiesFileLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
