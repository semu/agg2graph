/**
 * *****************************************************************************
 * Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler.loader;

import de.fub.agg2graph.crawler.FileFormat;
import de.fub.agg2graph.crawler.GPX2CSV;
import de.fub.agg2graph.crawler.OptionalParameters;
import de.fub.agg2graph.crawler.loader.FileLoader;
import de.fub.agg2graph.crawler.xml.XMLParser;
import de.fub.agg2graph.crawler.xml.gpsies.GPSiesTrack;
import de.fub.agg2graph.crawler.xml.gpsies.GPSiesContentHandler;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.alternativevision.gpx.GPXParser;
import org.alternativevision.gpx.beans.GPX;

/**
 *
 * @author Michael
 */
public class GPSiesFileLoader extends FileLoader {

    private OptionalParameters ops;
    public GPSiesFileLoader(OptionalParameters ops) {
        this.ops = ops;
    }

    @Override
    public void load() {
        url = "http://www.gpsies.com/api.do?"
                + "key=" + ops.getApiKey()
                + "&BBOX=" + ops.getBoundingBox()
                + "&limit=" + (ops.getCount())
                + "&trackTypes=" + ops.getTrackType()
                + "&filetype=gpxTrk";
        try {
            URL url2 = new URL(url);
            URLConnection conn2 = url2.openConnection();
            InputStream is = new BufferedInputStream(conn2.getInputStream());

            XMLParser xml = new XMLParser(new GPSiesContentHandler<GPSiesTrack>());
            List<GPSiesTrack> tracks = xml.parseGPSiesTracks(is).getAll();
            int i = 0;
            for (GPSiesTrack track : tracks) {
                i++;
                if (i <= ops.getOffset()
                        || i > ops.getCount()
                        || track == null) {
                    continue;
                }
                try {
                    System.out.println(track.getDownloadlink());
                    URL myUrl = new URL(track.getDownloadlink());
                    URLConnection con = myUrl.openConnection();
                    InputStream inputStream = con.getInputStream();

                    GPXParser gPXParser = new GPXParser();
                    GPX gpx = gPXParser.parseGPX(inputStream);

                    File path = new File(ops.getOutputDirectory().getAbsoluteFile() + "//" + ops.getFileFormat().getText() + "//" + ops.getTrackType().name());
                    if (!path.exists()) {
                        path.mkdirs();
                    }
                    if (ops.getFileFormat() == FileFormat.GPX) {
                        File f = new File(path.getAbsolutePath() + "//" + track.getId() + i + "." + ops.getFileFormat().getText().toLowerCase());
                        gPXParser.writeGPX(gpx, new FileOutputStream(f));
                    } else if (ops.getFileFormat() == FileFormat.CSV) {
                        GPX2CSV gpx2csv = new GPX2CSV(gpx);
                        File out = new File(path.getAbsolutePath() + "//" + track.getId() + i + "." + ops.getFileFormat().getText().toLowerCase());
                        gpx2csv.saveCSV(out);
                    }
                    Thread.sleep(ops.getSleepTimer());
                } catch (Exception ex) {
                    Logger.getLogger(GPSiesFileLoader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(GPSiesFileLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
