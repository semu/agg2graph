/**
 * *****************************************************************************
 * Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler.loader;

import de.fub.agg2graph.crawler.FileFormat;
import de.fub.agg2graph.crawler.GPX2CSV;
import de.fub.agg2graph.crawler.OptionalParameters;
import de.fub.agg2graph.crawler.TrackClassifier;
import de.fub.agg2graph.crawler.selenium.Downloader;
import de.fub.agg2graph.crawler.selenium.MapMyTracksDownloader;
import de.fub.agg2graph.crawler.xml.mapmytracks.Activity;
import de.fub.agg2graph.crawler.xml.XMLParser;
import de.fub.agg2graph.crawler.xml.mapmytracks.ActivityContentHandler;
import de.fub.agg2graph.crawler.xml.mapmytracks.Points;
import de.fub.agg2graph.crawler.xml.mapmytracks.PointsContentHandler;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.alternativevision.gpx.GPXParser;
import org.alternativevision.gpx.beans.GPX;
import org.alternativevision.gpx.beans.Track;
import org.alternativevision.gpx.beans.Waypoint;
import org.openqa.selenium.internal.Base64Encoder;

/**
 *
 * @author Michael
 */
public class MapMyTracksFileLoader extends FileLoader {

    private OptionalParameters ops;

    public MapMyTracksFileLoader(OptionalParameters ops) {
        this.ops = ops;
    }

    @Override
    public void load() {
        Downloader downloader = new MapMyTracksDownloader(ops.getUsername(), ops.getPassword());
        List<String> usernames = downloader.getUsernames();
        int i = 0;

        for (String un : usernames) {
            if (i < ops.getOffset() || i > ops.getCount()) {
                continue;
            }
            try {
                url = "http://www.mapmytracks.com/api/";
                URL url2 = new URL(url);
                String encoding = new Base64Encoder().encode((ops.getUsername() + ":" + ops.getPassword()).getBytes());

                HttpURLConnection connection = (HttpURLConnection) url2.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setInstanceFollowRedirects(true);
                connection.setRequestProperty("Authorization", "Basic " + encoding);

                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes("request=get_activities&author=" + un);
                wr.flush();
                wr.close();

                InputStream content = (InputStream) connection.getInputStream();
                XMLParser xml = new XMLParser(new ActivityContentHandler<Activity>());
                List<Activity> activities = xml.parseActivities(content).getAll();
                //System.out.println(un + " has " + activities.size() + " tracks");
                for (Activity activity : activities) {
                    i++;
                    if (i <= ops.getOffset() || i > ops.getCount()) {
                        continue;
                    }
                    //System.out.println("  id: " + activity.getId());
                    boolean incomplete = true;
                    int fromTime = 0;
                    System.out.println("request=get_activity&activity_id=" + activity.getId());
                    while (incomplete) {
                        url2 = new URL(url);
                        encoding = new Base64Encoder().encode((ops.getUsername() + ":" + ops.getPassword()).getBytes());
                        connection = (HttpURLConnection) url2.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setDoOutput(true);
                        connection.setInstanceFollowRedirects(true);
                        connection.setRequestProperty("Authorization", "Basic " + encoding);
                        wr = new DataOutputStream(connection.getOutputStream());
                        wr.writeBytes("request=get_activity&activity_id=" + activity.getId() + "&from_time=" + fromTime);
                        wr.flush();
                        wr.close();
                        content = (InputStream) connection.getInputStream();
                        xml = new XMLParser(new PointsContentHandler<Points>());
                        Points points = (Points) xml.parsePoints(content).getAll().get(0);
                        if (points.isCompleted()) {
                            incomplete = false;
                        } else {
                            fromTime += 100;
                        }
                    }
                    Points points = (Points) xml.getPointsContentHandler().getAll().get(0);
                    //System.out.println("activits has type: "+activity.getType());
                    String trackType = TrackClassifier.getInstance().getType(activity.getType());
                    File path = new File(ops.getOutputDirectory()+"//" + ops.getFileFormat().getText() + "//" + trackType);
                    if (!path.exists()) {
                        path.mkdirs();
                    }
                    GPX gpx = new GPX();
                    HashSet<Track> tracks = new HashSet<Track>();
                    Track track = new Track();
                    ArrayList<Waypoint> waypoints = new ArrayList<Waypoint>();
                    for (Points.Point p : points.getPoints()) {
                        Waypoint wayPoint = new Waypoint();
                        wayPoint.setTime(p.getTime());
                        wayPoint.setLatitude(p.getLat());
                        wayPoint.setLongitude(p.getLon());
                        wayPoint.setElevation(p.getElevation());
                        waypoints.add(wayPoint);
                    }
                    track.setTrackPoints(waypoints);
                    tracks.add(track);
                    gpx.setTracks(tracks);
                    gpx.addTrack(track);

                    if (ops.getFileFormat() == FileFormat.GPX) {
                        GPXParser p = new GPXParser();
                        FileOutputStream out = new FileOutputStream(path.getAbsoluteFile() + "//" + activity.getId() + "." + ops.getFileFormat().getText());
                        p.writeGPX(gpx, out);
                        out.flush();
                        out.close();
                    }
                    if (ops.getFileFormat() == FileFormat.CSV) {
                        try {
                            GPX2CSV gpx2csv = new GPX2CSV(gpx);
                            File out = new File(path.getAbsoluteFile() + "//" + activity.getId() + "." + ops.getFileFormat().getText());
                            gpx2csv.saveCSV(out);
                        } catch (Exception ex) {
                            Logger.getLogger(OpenStreetMapFileLoader.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    Thread.sleep(ops.getSleepTimer());
                }
            } catch (Exception ex) {
                Logger.getLogger(MapMyTracksFileLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
