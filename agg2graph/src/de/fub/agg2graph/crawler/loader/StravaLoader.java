/*******************************************************************************
Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/

package de.fub.agg2graph.crawler.loader;

import de.fub.agg2graph.crawler.OptionalParameters;
import de.fub.agg2graph.crawler.loader.FileLoader;
import de.fub.agg2graph.crawler.selenium.Downloader;
import de.fub.agg2graph.crawler.selenium.StravaDownloader;

/**
 *
 * @author Michael
 */
public class StravaLoader extends FileLoader {

    private OptionalParameters ops;

    public StravaLoader(OptionalParameters opts) {
        this.ops = opts;
    }

    @Override
    public void load() {
        Downloader downloader = new StravaDownloader(ops.getUsername(), ops.getPassword());
        downloader.getUsernames();     
    }
}
