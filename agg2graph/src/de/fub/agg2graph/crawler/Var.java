/*******************************************************************************
Copyright 2013 M.Klingen, C.Dresske

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/ 
 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.crawler;

/**
 *
 * @author Michael
 */
public interface Var {

    public static final String DATASOURCE = "d";
    public static final String USERNAME = "u";
    public static final String PASSWORD = "p";
    public static final String TRACK_TYPE = "t";
    public static final String KEY_API = "k";
    public static final String BOUNDING_BOX = "b";
    public static final String SLEEP_TIMER = "s";//timeout between download
    public static final String OFFSET = "o";//offset start download at ....
    public static final String COUNT = "c";//limit number of max downloads since offset
    public static final String OUTPUT_DIRECTORY = "r";//output directory
    public static final String FILEFORMAT = "f";//filetype 
}
