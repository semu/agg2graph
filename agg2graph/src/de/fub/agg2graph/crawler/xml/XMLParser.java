/*******************************************************************************
Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/

package de.fub.agg2graph.crawler.xml;

import java.io.IOException;
import java.io.InputStream;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import de.fub.agg2graph.crawler.xml.gpsies.GPSiesContentHandler;
import de.fub.agg2graph.crawler.xml.mapmytracks.Activity;
import de.fub.agg2graph.crawler.xml.mapmytracks.ActivityContentHandler;
import de.fub.agg2graph.crawler.xml.mapmytracks.Points;
import de.fub.agg2graph.crawler.xml.mapmytracks.PointsContentHandler;
import de.fub.agg2graph.crawler.xml.osm.OSMContentHandler;
import de.fub.agg2graph.crawler.xml.osm.OSMTrack;
import de.fub.agg2graph.crawler.xml.gpsies.GPSiesTrack;

/**
 *
 * @author Michael
 */
public class XMLParser {

    private OSMContentHandler<OSMTrack> otch;
    private GPSiesContentHandler<GPSiesTrack> gtch;
    private ActivityContentHandler<Activity> ach;
    private PointsContentHandler<Points> pch;

    public XMLParser(OSMContentHandler<OSMTrack> otch) {
        this.otch = otch;
    }
    public XMLParser(GPSiesContentHandler<GPSiesTrack> gtch) {
        this.gtch = gtch;
    }
    public XMLParser(ActivityContentHandler<Activity> ach) {
        this.ach = ach;
    }
    public XMLParser(PointsContentHandler<Points> pch) {
        this.pch = pch;
    }

    public ActivityContentHandler<Activity> parseActivities(InputStream xml) {
        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(ach);
            InputSource is = new InputSource(xml);
            xmlReader.parse(is);
            return ach;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("nothing to parse");
    }
    public PointsContentHandler<Points> parsePoints(InputStream xml) {
        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(pch);
            InputSource is = new InputSource(xml);
            xmlReader.parse(is);
            return pch;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("nothing to parse");
    }
    
    public GPSiesContentHandler<GPSiesTrack> parseGPSiesTracks(InputStream xml) {
        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(gtch);
            InputSource is = new InputSource(xml);
            xmlReader.parse(is);
            return gtch;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("nothing to parse");
    }

    public OSMContentHandler<OSMTrack> parseOSMTracks(InputStream xml) {
        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(otch);
            InputSource is = new InputSource(xml);
            xmlReader.parse(is);
            return otch;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("nothing to parse");
    }

    public GPSiesContentHandler<GPSiesTrack> getGPSiesTrackContentHandler() {
        return gtch;
    }
    public OSMContentHandler<OSMTrack> getOSMTrackContentHandler() {
        return otch;
    }
    public ActivityContentHandler<Activity> getActivityContentHandler() {
        return ach;
    }
    public PointsContentHandler<Points> getPointsContentHandler() {
        return pch;
    }
}
