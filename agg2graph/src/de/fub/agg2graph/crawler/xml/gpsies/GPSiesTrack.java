/*******************************************************************************
Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/

package de.fub.agg2graph.crawler.xml.gpsies;

/**
 *
 * @author Michael
 */
public class GPSiesTrack {

    String downloadlink="";
    String id;
    String trackType;

    public GPSiesTrack() {
    }

    @Override
    public String toString() {
        return "Track - " + id + " " + trackType + " " + downloadlink;
    }

    public String getDownloadlink() {
        return downloadlink;
    }

    public void setDownloadlink(String downloadlink) {
        //System.out.println("set ddl "+downloadlink);
        this.downloadlink = downloadlink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrackType() {
        return trackType;
    }

    public void setTrackType(String trackType) {
        this.trackType = trackType;
    }
}
