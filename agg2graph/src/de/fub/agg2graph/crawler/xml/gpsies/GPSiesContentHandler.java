/**
 * *****************************************************************************
 * Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ****************************************************************************
 */
package de.fub.agg2graph.crawler.xml.gpsies;

import de.fub.agg2graph.crawler.xml.MyContentHandler;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author Michael
 */
public class GPSiesContentHandler<T> implements MyContentHandler<T> {

    public GPSiesContentHandler() {
    }
    private List<GPSiesTrack> allTracks = new ArrayList<GPSiesTrack>();
    private String currentValue;
    private GPSiesTrack track;
    private String trackType;
    private StringBuffer sb;

    public List<GPSiesTrack> getAll() {
        return (List<GPSiesTrack>) allTracks;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue = new String(ch, start, length);
        sb.append(currentValue);
    }

    // Methode wird aufgerufen wenn der Parser zu einem Start-Tag kommt
    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        sb = new StringBuffer();
        if (localName.equals("track")) {
            track = new GPSiesTrack();
            track.setTrackType(trackType);
        }
    }

    // Methode wird aufgerufen wenn der Parser zu einem End-Tag kommt
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        if (localName.equals("fileId")) {
            track.setId(sb.toString());
        }

        if (localName.equals("trackType")) {
            trackType = sb.toString();
        }
        if (localName.equals("downloadLink")) {
            // System.out.println("a ddl "+currentValue);
            track.setDownloadlink(sb.toString());
        }
        if (localName.startsWith("track")) {
            allTracks.add(track);
        }
    }

    @Override
    public void endDocument() throws SAXException {
    }

    @Override
    public void endPrefixMapping(String prefix) throws SAXException {
    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length)
            throws SAXException {
    }

    @Override
    public void processingInstruction(String target, String data)
            throws SAXException {
    }

    @Override
    public void setDocumentLocator(Locator locator) {
    }

    @Override
    public void skippedEntity(String name) throws SAXException {
    }

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void startPrefixMapping(String prefix, String uri)
            throws SAXException {
    }
}
