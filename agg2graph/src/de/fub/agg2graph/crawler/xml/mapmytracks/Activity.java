/*******************************************************************************
Copyright 2013 M.Klingen, C.Dresske

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/

package de.fub.agg2graph.crawler.xml.mapmytracks;

/**
 *
 * @author Michael
 */
public class Activity {

    long id=Long.MIN_VALUE;
    String type="";

    public Activity() {
    }

    public Activity(long id, String type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Activity - " + id + " " + type;
    }

    public long getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = Long.parseLong(id);
    }

    public String getType() 
    {
//        System.out.println("activity is :"+this.type);
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
