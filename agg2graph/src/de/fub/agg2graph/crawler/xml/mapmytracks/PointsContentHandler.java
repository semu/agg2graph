/*******************************************************************************
Copyright 2014 M.Klingen, C.Dresske, Sebastian Müller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
******************************************************************************/

package de.fub.agg2graph.crawler.xml.mapmytracks;

import de.fub.agg2graph.crawler.xml.MyContentHandler;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author Michael
 */
public class PointsContentHandler<T> implements MyContentHandler<T> {

    private List<Points> allPoints = new ArrayList<Points>();
    private String currentValue;

    public PointsContentHandler() {
        allPoints = new ArrayList<Points>();
        allPoints.add(new Points());
    }

    public List<Points> getAll() {
        return (List<Points>) allPoints;
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        currentValue = new String(ch, start, length);
    }

    // Methode wird aufgerufen wenn der Parser zu einem Start-Tag kommt
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes atts) throws SAXException {
    }

    // Methode wird aufgerufen wenn der Parser zu einem End-Tag kommt
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        //System.out.println(localName);
        if (localName.equals("complete")) {
            if (currentValue.equals("Yes")) {
                allPoints.get(0).setCompleted(true);
            }
        }

        if (localName.equals("points")) {
            String[] lines = currentValue.split(" ");
          //  System.out.println(lines.length + " zeilen mit punkten");
            String time, lat, lon, elevation;
            for (String line : lines) {
                String[] s = line.trim().split(",");
                if (s.length != 4) {
                    continue;
                }
                time = s[0];
                lat = s[1];
                lon = s[2];
                elevation = s[3];

                Points.Point p = new Points.Point(time, lat, lon, elevation);
                allPoints.get(0).setPoint(p);
            }
        }
    }

    @Override
    public void endDocument() throws SAXException {
    }

    @Override
    public void endPrefixMapping(String prefix) throws SAXException {
    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
    }

    @Override
    public void processingInstruction(String target, String data)
            throws SAXException {
    }

    @Override
    public void setDocumentLocator(Locator locator) {
    }

    @Override
    public void skippedEntity(String name) throws SAXException {
    }

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
    }
}
