/**
 * *****************************************************************************
 * Copyright 2013 M.Klingen, C.Dresske
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
*****************************************************************************
 */
package de.fub.agg2graph.crawler.xml.mapmytracks;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Michael
 */
public class Points {

    boolean completed = false;
    List<Point> points;

    public Points() {
        points = new ArrayList<Point>();
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoint(Point point) {
        points.add(point);
    }

    public static class Point {

        Date time;
        Double lat, lon, elevation;
        SimpleDateFormat sdf = new SimpleDateFormat();

        public Point(String time, String lat, String lon, String elevation) {
            try {
                this.lat = Double.parseDouble(lat);
                this.lon = Double.parseDouble(lon);
                this.elevation = Double.parseDouble(elevation);
                this.time = new Date(Long.parseLong(time) * 1000);
            } catch (Exception ex) {
            }
        }

        public Point() {
        }

        public Date getTime() {
            return time;
        }

        public double getLat() {
            return lat;
        }

        public double getLon() {
            return lon;
        }

        public double getElevation() {
            return elevation;
        }
    }
}
