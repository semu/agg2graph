/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.pipe;

import de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class Pipeline {

    public List<AbstractAlgorithm> steps = new LinkedList<>();

    public void addStep(AbstractAlgorithm aa) {
        steps.add(aa);
    }

    public GPSkEnvironment pipe(GPSkEnvironment input) throws Exception {
        GPSkEnvironment env = input;
        for (AbstractAlgorithm aa : steps) {
            aa.setInput(env);
            try {
                env = (GPSkEnvironment) aa.call();
            } catch (Exception e) {
                PipeException pe = new PipeException();
                pe.algoClass = aa.getClass();
                throw pe;
            }
        }
        return env;
    }

    public class PipeException extends Exception {

        /**
		 * 
		 */
		private static final long serialVersionUID = -8582107570058618636L;
		Class<? extends AbstractAlgorithm> algoClass;
        
        public Class<? extends AbstractAlgorithm> getAlgoClass(){
            return algoClass;
        }
    }
}
