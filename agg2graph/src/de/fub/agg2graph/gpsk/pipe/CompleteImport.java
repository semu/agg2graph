/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.pipe;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.algorithms.Cleaner;
import de.fub.agg2graph.gpsk.algorithms.CleaningOptions;
import de.fub.agg2graph.gpsk.algorithms.RDPFilter;
import de.fub.agg2graph.gpsk.algorithms.RDPOptions;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import static de.fub.agg2graph.gpsk.beans.EnvironmentBuilder.PROJECTION_OFF;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_SEGMENTS;
import de.fub.agg2graph.gpsk.data.BusEvent;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.data.MessageBus;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.io.ConvertedImport;
import java.io.File;
import javax.xml.bind.JAXBException;

import static de.fub.agg2graph.gpsk.data.MessageBus.GPSK;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class CompleteImport implements Runnable {
    private static Logger log = Logger.getLogger(CompleteImport.class);

    private File f;
    private boolean doAll;
    private String name;
    private ConvertedImport<GPSkEnvironment> ci;
    
    private final MessageBus mb = MessageBus.getInstance(GPSK);

    public CompleteImport(File f, boolean doAll, String name) {
        this.f = f;
        this.doAll = doAll;
    }

    @Override
    public void run() {
        EnvironmentManager em = EnvironmentManager.getInstance();
        EnvironmentBuilder builder = new EnvironmentBuilder();
        builder.setFocus(FOCUS_ON_SEGMENTS);
        builder.setProjection(PROJECTION_OFF);
        GPSkEnvironment env1 = null;
        try {
            ci = new ConvertedImport<>(new EnvironmentImport(builder));
            ci.setFile(f);
            env1 = ci.call();
        } catch (JAXBException ex) {
            BusEvent be = new BusEvent("Problem with the import of the file!", BusEvent.ERROR_MESSAGE, this);
            log.error("failed to read file", ex);
            mb.fireEvent(be);
            return;
        } catch (Exception e){
            BusEvent be = new BusEvent("A problem occurred while importing file. Probably caused by a programming error", BusEvent.ERROR_MESSAGE, this);
            mb.fireEvent(be);
            log.error("An exception occurred while reading file", e);
            return;
        }
        if(name == null || name.trim().equals("")) {
            em.put(f.getName().replace(".gpsk", ""), env1);
        } else {
            em.put(name, env1);
        }
        
        if(!doAll){
            BusEvent be = new BusEvent("parsed file " + f.getName(), BusEvent.INFO_MESAGE, this);
            mb.fireEvent(be);
            return;
        }
        Cleaner cleaner = new Cleaner(env1);
        Object o1 = null;
        try {
            o1 = KeyValueStore.loadOptions(CleaningOptions.class);
            
        } catch (Exception ex) {
            BusEvent be = new BusEvent("A problem occurred while reading cleaning options", BusEvent.ERROR_MESSAGE, this);
            mb.fireEvent(be);
            log.error("unable to instanciate options object", ex);
            return;
        }
        cleaner.setOptions(o1);
        GPSkEnvironment env2;
        try {
            env2 = cleaner.call();
            em.put("cleaned", env2);
        } catch (Exception ex) {
            BusEvent be = new BusEvent("A problem accurred while cleaning data!", BusEvent.ERROR_MESSAGE, this);
            mb.fireEvent(be);
            log.error("exception thrown while cleaning");
            return;
        }
        
        RDPFilter rdp = new RDPFilter(env2);
        Object o2 = null;
        try {
            o2 = KeyValueStore.loadOptions(RDPOptions.class);
        } catch (Exception ex) {
            BusEvent be = new BusEvent("unable to load options!", BusEvent.ERROR_MESSAGE, this);
            mb.fireEvent(be);
            log.error("unable to instanciate options object", ex);
        }
        rdp.setOptions(o2);
        
        GPSkEnvironment env3;
        try {
            env3 = rdp.call();
        } catch (Exception ex) {
            BusEvent be = new BusEvent("Exception thrown while smoothing data", BusEvent.ERROR_MESSAGE, this);
            mb.fireEvent(be);
            log.error("Exception thrown while smoothing data", ex);
            return;
        }
        em.put("smoothed", env3);
        BusEvent be = new BusEvent("read file, cleaned data and smoothed it!", BusEvent.INFO_MESAGE, this);
        
    }
}
