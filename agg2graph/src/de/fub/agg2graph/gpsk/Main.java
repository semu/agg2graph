/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.LayerManager;
import de.fub.agg2graph.gpsk.gui.MainWindow;

/**
 * Main Class that contains the main-method for the executable gpsk-jar file.
 *
 * 
 *
 * @author Christian Windolf
 */
public class Main {

    private static Logger log = Logger.getLogger(Main.class);
    /**
     * The actual version of this project
     */
    public static final String version = "0.2 SNAPSHOT";
    
    /**
     * A global static hook to access the GUI
     */
    public static MainWindow mainWindow = null;
    
    public static final int THREADS = 2; 
    
    /**
     * A pool thread that allows to execute tasks without creating new threads
     */
    public static ExecutorService executor = Executors.newFixedThreadPool(THREADS);

    public static void main(String[] args) {

        log.info("GPSk started - Version: " + version);

        //check that if configuration files could be found or can be created
        if (!ResourceManager.envOK()) {
            System.exit(1);
        }
        //layers will react to new data
        EnvironmentManager.getInstance().addObserver(LayerManager.getInstance());


        //start this app/creating the gui
        SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				mainWindow = new MainWindow();
			}
        });
        

       
       
    }
}
