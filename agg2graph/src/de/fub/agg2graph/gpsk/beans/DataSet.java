/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import java.util.List;

/**
 * A dataset of geographic data.
 * This interfaces purpose is that it used as some kind of a marker interface
 * for the dataset.
 * This interface forces an implementation mainly to provide information for the
 * GUI, so it can be used as a model.
 * 
 *
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public interface DataSet {
	/**
	 * title for this dataset
	 * @return 
	 */
    public String getTitle();
    
    /**
     * sets a new title on this dataset.
     * @param title
     */
    public void setTitle(String title);
    
    /**
     * what has been done to this dataset before it was created?
     * @return
     */
    public History getHistory();
    
    /**
     * the center point of this dataset.
     * may return null, if the dataset is empty or can't make a statement about
     * it's center point
     * @return may return null.
     */
    public GPSkPoint getCenter();
    
    /**
     * the bounds around the dataset.
     * if dataset is empty or can't make a statement about it's bounds, it returns
     * null
     * @return may return null
     */
    public double[] getBounds();
}
