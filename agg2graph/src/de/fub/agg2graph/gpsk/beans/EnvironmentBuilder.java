/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import java.util.HashMap;
import java.util.List;



import java.util.LinkedList;
import org.apache.log4j.Logger;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.*;
import de.fub.agg2graph.gpsk.logic.Projection;
import de.fub.agg2graph.gpsk.logic.ProjectionFactory;
import static java.lang.Double.isNaN;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.logging.Level;

/**
 *
 * Class that is designed to ease the creation of an GPSkEnvironment
 * wich grew quite compicated in the recent time
 * @author Christian Windolf christianwindolf@web.de
 */
public class EnvironmentBuilder {

    private static Logger log = Logger.getLogger(EnvironmentBuilder.class);
    public static int PROJECTION_OFF = 0;
    public static int PROJECTION_IF_THERE_IS_NONE = 1;
    public static int PROJECTION_ON = 2;
    private List<?> inputList = null;
    double minLat = Double.NaN;
    double minLon = Double.NaN;
    double maxLat = Double.NaN;
    double maxLon = Double.NaN;
    public HashMap<String, String> keyValue = new HashMap<>();
    private String title = null;
    private int focus = FOCUS_ON_SEGMENTS;
    private int projection = PROJECTION_IF_THERE_IS_NONE;
    private boolean inputFound = false;
    private List<GPSkPoint> pointList = new LinkedList<>();
    private List<GPSkSegment> segmentList = new LinkedList<>();
    private Class projectionClass;
    private Projection projectionLogic;
    
    private boolean center = false;

    /**
     * empty constructor. No data will be initialized
     */
    public EnvironmentBuilder() {
    }

    /**
     * with input list
     * @param inputList must be a list of GPSkPoint or GPSkSegment. Otherwise it will crash 
     */
    public EnvironmentBuilder(List<?> inputList) {
        this.inputList = inputList;
    }
    
    public EnvironmentBuilder center(){
        center = true;
        return this;
    }
    
    public EnvironmentBuilder center(boolean b){
        center = b;
        return this;
    }
    
    /**
     * sets the projection mode. Only {@link PROJECTION_ON}, {@link PROJECTION_OFF} 
     * and {@link PROJECTION_ON_IF_THERE_IS_NONE} are allowed values.
     * If projection is turned off, nothing will happen to the north and east values
     * of the points.
     * If it is on, if there is no eastNorth Value, that it will be created.
     * If it is turned ON, it will be overwritten.
     * @param p 
     */
    public void setProjection(int p){
        this.projection = p;
    }

    /**
     * set the inputList. the input list must be a pure list of either {@link GPSkPoint}
     * or {@link GPSkSegment}, otherwise it will crash.
     * @param inputList
     * @return 
     */
    public EnvironmentBuilder setList(List<?> inputList) {
        this.inputList = inputList;
        return this;
    }
    
    public EnvironmentBuilder useProjection(){
        projection = PROJECTION_ON;
        return this;
    }
    
    public EnvironmentBuilder useProjection(Class<? extends Projection> projectionClass){
        this.projectionClass = projectionClass;
        return this;
    }
    
    public EnvironmentBuilder useProjection(Projection p){
        this.projectionLogic = p;
        return this;
    }

   
    /**
     * set the bounds of the dataset.
     * @param minLat
     * @param minLon
     * @param maxLat
     * @param maxLon
     * @return 
     */
    public EnvironmentBuilder setBounds(
            double minLat,
            double minLon,
            double maxLat,
            double maxLon) {
        this.minLat = minLat;
        this.minLon = minLon;
        this.maxLat = maxLat;
        this.maxLon = maxLon;
        return this;
    }
    
    public EnvironmentBuilder setBounds(double[] bounds){
        this.minLat = bounds[0];
        this.minLon = bounds[1];
        this.maxLat = bounds[2];
        this.maxLon = bounds[3];
        return this;
    }
    
    public EnvironmentBuilder detectBounds(){
        try{
            double[] b = detectBounds((List<GPSkPoint>) inputList);
            setBounds(b);
        } catch(ClassCastException ex){
            double[] b = detectBounds2((List<GPSkSegment>) inputList);
            setBounds(b);
        } 
        
        return this;
    }
    
    private double[] detectBounds(List<GPSkPoint> points){
        double tmpMinLat = Double.MAX_VALUE;
        double tmpMinLon = Double.MAX_VALUE;
        double tmpMaxLat = Double.MIN_VALUE;
        double tmpMaxLon = Double.MIN_VALUE;
        for(GPSkPoint p : points){
            if(tmpMinLat > p.getLat()){
                tmpMinLat = p.getLat();
            }
            if(tmpMinLon > p.getLon()){
                tmpMinLon = p.getLon();
            }
            if(tmpMaxLat < p.getLat()){
                tmpMaxLat = p.getLat();
            }
            if(tmpMaxLon < p.getLon()){
                tmpMaxLon = p.getLon();
            }
        }
        return new double[]{
            tmpMinLat,
            tmpMinLon,
            tmpMaxLat,
            tmpMaxLon
        };
    }
    
    private double[] detectBounds2(List<GPSkSegment> segments){
        double tmpMinLat = Double.MAX_VALUE;
        double tmpMinLon = Double.MAX_VALUE;
        double tmpMaxLat = Double.MIN_VALUE;
        double tmpMaxLon = Double.MIN_VALUE;
        for(GPSkSegment seg : segments){
            double[] b = detectBounds(seg);
            if(b[0] < tmpMinLat){
                tmpMinLat = b[0];
            } 
            if(b[1] < tmpMinLon){
                tmpMinLon = b[1];
            }
            if(b[2] > tmpMaxLat){
                tmpMaxLat = b[2];
            }
            if(b[3] > tmpMaxLon){
                tmpMaxLon = b[3];
            }
            
        }
        return new double[]{
            tmpMinLat,
            tmpMinLon,
            tmpMaxLat,
            tmpMaxLon
        };
    }

    /**
     * give it a title for the meta inoformation. Otherwise it will be 'unknown'
     * @param title
     * @return 
     */
    public EnvironmentBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * this kind of information is only needed for the drawing layers
     * @param focus
     * @return 
     */
    public EnvironmentBuilder setFocus(int focus) {
        boolean valid = focus == FOCUS_ON_POINTS;
        valid = valid || focus == FOCUS_ON_SEGMENTS;
        valid = valid || focus == FOCUS_ON_BOTH;
        if (!valid) {
            throw new IllegalArgumentException("only "
                    + FOCUS_ON_BOTH + "(FOCUS_ON_BOTH), "
                    + FOCUS_ON_POINTS + "(FOCUS_ON_POINTS) or"
                    + FOCUS_ON_SEGMENTS + "(FOCUS_ON_SEGMENTS) are"
                    + "valid parameters");

        }
        this.focus = focus;
        return this;
    }
    
    

    /**
     * You have invoked the setters you wantet to?
     * Then start to build() an GPSkEnvironment.
     * @throws IllegalArgumentException if the input on the setters methods made
     * no sense
     */
    public GPSkEnvironment build() {
        if(!areBoundsSet() && projection != PROJECTION_OFF){
            throw new IllegalArgumentException("you have to set the bounds in order"
                    + "set up an index. Turn projection off, then build an environment,"
                    + "than call detectBounds() and resetOrigin() on the "
                    + "environment to work around this issue");
        }

        try {
            initProjection();
        } catch (NoSuchMethodException | InstantiationException | 
                IllegalAccessException | InvocationTargetException  ex) {
            
            log.error("unable to initialize Projection: ", ex);
        } 
        
        
        
        processInputList();
        GPSkEnvironment env = null;
        
        if (areBoundsSet()) {
            if (segmentList.isEmpty()) {
                env = new GPSkEnvironment(pointList, new double[]{
                            minLat,
                            minLon,
                            maxLat,
                            maxLon
                        });
                log.debug("created GPSkEnvironment without tracks but with bounds");
            } else if (!pointList.isEmpty()) {
                env = new GPSkEnvironment(pointList, segmentList, new double[]{
                            minLat,
                            minLon,
                            maxLat,
                            maxLon
                        });
                log.debug("created GPSkEnvironment with tracks and with bounds");
            }
        } else {
            if (segmentList.isEmpty()) {
                env = new GPSkEnvironment(pointList);
                log.debug("created GPSkEnvironment without tracks and bounds");
            } else if ((!pointList.isEmpty())) {
                env = new GPSkEnvironment(pointList, segmentList);
                log.debug("created GPSkEnvironment with tracks but without bounds");
            } else {
                env = new GPSkEnvironment();
                log.debug("created an empty GPSkEnvironment");
            }
        }
        if (env == null) {
            throw new IllegalArgumentException("it does not make sense, what you gave me as data");
        }
        GPSkEnvironment.MetaInformation mi;
        if (title != null) {
            mi = env.addMeta(title, focus);
        } else {
            mi = env.addMeta("default", focus);
        }
        if (keyValue != null) {
            mi.keyValue = keyValue;
        } else{
            mi.keyValue = new HashMap<>();
        }
        if(center){
            mi.center = true;
        }
        env.setProjection(projectionLogic);
        return env;

    }

    private void processInputList() {
        if (inputList == null) {
            return;
        }
        if (inputList.isEmpty()) {
            return;
        }

        boolean bounds = areBoundsSet();

        Object o = inputList.get(0);
        if (o instanceof GPSkSegment) {
            List<GPSkSegment> tmpSegList = (List<GPSkSegment>) inputList;
            for (GPSkSegment seg : tmpSegList) {
                segmentList.add(seg);
                for (GPSkPoint p : seg) {
                    setProjection(p);
                    pointList.add(p);
                }
            }
        } else if (o instanceof GPSkPoint) {
            List<GPSkPoint> tmpPointList = (List<GPSkPoint>) inputList;
            for (GPSkPoint p : tmpPointList) {
                setProjection(p);
                pointList.add(p);
            }
        }
    }

    private boolean areBoundsSet() {
        return !isNaN(minLat)
                && !isNaN(minLon)
                && !isNaN(maxLat)
                && !isNaN(maxLon);
    }
    
    private void initProjection() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        if(projectionLogic != null){
            return; // nothing to do...
        }
        if(projectionClass != null){
            projectionLogic = ProjectionFactory.getInstance().createProjection(minLat, minLon, maxLat, maxLon, projectionClass);
        } else if(projection == PROJECTION_IF_THERE_IS_NONE || projection == PROJECTION_ON){
            projectionLogic = ProjectionFactory.getInstance().createProjection(minLat, minLon, maxLat, maxLon);
            
        }
    }
    
    

    private void setProjection(GPSkPoint p) {
        
        if (projection == PROJECTION_OFF) {
            return;
        }
        if (projection == PROJECTION_IF_THERE_IS_NONE
                && (isNaN(p.getEast()) || isNaN(p.getNorth()))) {
            projectionLogic.setProjection(p);
            
        }
        if (projection == PROJECTION_ON) {
            projectionLogic.setProjection(p);
        }
    }
}
