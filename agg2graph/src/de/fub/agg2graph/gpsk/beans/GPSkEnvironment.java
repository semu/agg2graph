/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import com.infomatiq.jsi.Rectangle;
import com.infomatiq.jsi.rtree.RTree;

import de.fub.agg2graph.gpsk.data.IndexProcedure;
import de.fub.agg2graph.gpsk.logic.*;
import de.fub.agg2graph.gpsk.utils.SegmentUtils;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;
import gnu.trove.TIntProcedure;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;
import org.apache.log4j.Logger;

import static java.lang.Double.isNaN;
import static java.lang.Double.isInfinite;

/**
 *
 * <p>
 * Contains a list of segments and their points stored in a rtree. So neighbor
 * requests run very efficiently.
 * </p>
 * <p>
 * The input data should not be distributed over a large area. Because the use
 * framework can only handle planar coordinates, this class makes sure, that
 * each point/segment gets planar coordinates. The larger the input area is, the
 * lower is the precession
 * </p>
 *
 * @author Christian Windolf
 */
public class GPSkEnvironment extends Observable {

    private static Logger log = Logger.getLogger(GPSkEnvironment.class);
    private double minLat = Double.NaN, minLon = Double.NaN,
            maxLat = Double.NaN, maxLon = Double.NaN;
    protected final ArrayList<GPSkPoint> pointList;
    protected final ArrayList<GPSkSegment> segmentList;
    protected RTree[] pointTree = null;
    protected RTree[] segmentTree = null;
    private MetaInformation meta;
    private Projection projection;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public GPSkEnvironment(List<GPSkPoint> pointList,
            List<GPSkSegment> segmentList) {
        this.pointList = new ArrayList<>(pointList);
        this.segmentList = new ArrayList<>(segmentList);
    }

    public GPSkEnvironment(List<GPSkPoint> pointList) {
        this.pointList = new ArrayList<>(pointList);
        this.segmentList = new ArrayList<>(10);
    }

    public GPSkEnvironment() {
        this.pointList = new ArrayList<>(10);
        this.segmentList = new ArrayList<>(10);
    }

    public GPSkEnvironment(List<GPSkPoint> pointList,
            List<GPSkSegment> segmentList, double[] bounds) {
        if (bounds.length < 4) {
            throw new IllegalArgumentException("expected an array with length"
                    + "of 4, but it had a length of " + bounds.length);
        }
        this.pointList = new ArrayList<>(pointList);
        this.segmentList = new ArrayList<>(segmentList);

        minLat = bounds[0];
        minLon = bounds[1];
        maxLat = bounds[2];
        maxLon = bounds[3];
    }

    public GPSkEnvironment(List<GPSkPoint> pointList, double[] bounds) {
        this(pointList);
        if (bounds.length < 4) {
            throw new IllegalArgumentException("expected an array with length"
                    + " of 4, but it had a length of " + bounds.length);
        }
        minLat = bounds[0];
        minLon = bounds[1];
        maxLat = bounds[2];
        maxLon = bounds[3];
    }

    public void updateProjection() {
        if (projection == null) {
            try {
                projection = ProjectionFactory.getInstance().createProjection(
                        minLat, minLon, maxLat, maxLon);
            } catch (Exception ex) {
                log.error("unable to update projection.", ex);
                throw new IllegalStateException(
                        "you have to ensure, that a projection is set to update it!");
            }
        }
        for (GPSkPoint p : pointList) {
            projection.setProjection(p);
        }

    }

    public boolean activatePointIndex(int amount) {
        if (pointTree == null) {
            pointTree = new RTree[amount];
            for (int i = 0; i < amount; i++) {
                pointTree[i] = new RTree();
                pointTree[i].init(null);
                ListIterator<GPSkPoint> listIter = pointList.listIterator();
                while (listIter.hasNext()) {
                    int index = listIter.nextIndex();
                    GPSkPoint p = listIter.next();
                    // after this block we made sure, that the projection for this
                    // point
                    // has been initialized or this method returned
                    if (Double.isNaN(p.getEast()) || Double.isNaN(p.getNorth())) {
                        projection.setProjection(p);
                    }
                    pointTree[i].add(p.getRectangle(), index);
                }
            }
            setChanged();
            notifyObservers();
            pcs.firePropertyChange("point.index", null, pointTree);

            return true;

        } else {
            return true;
        }
    }

    public boolean activatePointIndex() {
        boolean b = activatePointIndex(1);

        return b;
    }

    public void setProjection(Projection projection) {
        pcs.firePropertyChange("projection", this.projection, projection);
        this.projection = projection;
    }

    public Projection getProjection() {
        return projection;
    }

    /**
     * Activates the index over segments. If there is already an index, nothing
     * will happen. It just returns true There won't be an index created. if
     * this environment doesn't contain any segments, it will do nothing and
     * return false. And print an error message via log4j.
     *
     * @return true, if an index already existed or was successfully created.
     * False if something went wrong.
     */
    public boolean activateSegmentIndex(int amount) {
        if (segmentTree != null) {
            return true;
        }
        if (segmentList.isEmpty()) {
            log.warn("tried to create an index on an empty data set");
            return false;
        }
        segmentTree = new RTree[amount];
        for (int i = 0; i < amount; i++) {
            segmentTree[i] = new RTree();
            segmentTree[i].init(null);
            ListIterator<GPSkSegment> listIter = segmentList.listIterator();
            while (listIter.hasNext()) {
                int index = listIter.nextIndex();
                GPSkSegment seg = listIter.next();
                segmentTree[i].add(seg.getRectangle(), index);
            }
        }
        pcs.firePropertyChange("segment.index", null, segmentTree);
        return true;
    }

    public boolean activateSegmentIndex() {
        boolean b = activateSegmentIndex(1);

        return b;

    }

    public boolean updatePointIndex() {
        RTree[] old = pointTree;
        deletePointIndex();
        setChanged();
        notifyObservers();
        pcs.firePropertyChange("point.index", old, pointTree);
        return activatePointIndex();
    }

    private void deletePointIndex() {
        pcs.firePropertyChange("point.index", pointTree, null);
        pointTree = null;
    }

    public boolean hasPointIndex() {
        return pointTree != null;
    }

    /**
     * adds a single point to the dataset and rtree. Note: this method does not
     * care, if the north/east value is not set up properly The new point wont
     * be added to index!
     *
     * @param lat
     * @param lon
     */
    public void addPoint(double lat, double lon) {
        GPSkPoint point;
        if (minLat != Double.NaN && minLon != Double.NaN) {
            point = new GPSkPoint(lat, lon, minLat, minLon);
        } else {
            point = new GPSkPoint(lat, lon);
        }
        pointList.add(point);
        notifyObservers(point);
        setChanged();
        pcs.fireIndexedPropertyChange("points", pointList.size() - 1, null, point);
    }

    /**
     * retrieve the list of points
     *
     * @return
     */
    public List<GPSkPoint> getPoints() {
        return pointList;
    }

    /**
     * retrieve the list of segments
     *
     * @return
     */
    public List<GPSkSegment> getSegments() {
        return segmentList;
    }

    /**
     * retrieve the latitude coordinate of the origin.
     *
     * @return
     */
    public double getOriginLat() {
        return minLat;
    }

    /**
     * retrieve the longitude coordinate of the origin.
     *
     * @return
     */
    public double getOriginLon() {
        return minLon;
    }

    /**
     * retrieve the origin as an array
     *
     * @return
     */
    public double[] getOrigin() {
        return new double[]{minLat, minLon};
    }

    /**
     * add new metainformation to this dataset
     *
     * @param name
     * @param type see also {@link MetaInformation.FOCUS_ON_POINTS},
     *            {@link MetaInformation.FOCUS_ON_SEGMENTS} and
     * {@link MetaInformation.FOCUS_ON_BOTH}
     * @return the newly created MetaInformation
     */
    public MetaInformation addMeta(String name, int type) {
        meta = new MetaInformation(name, type);
        return meta;
    }

    /**
     * get the metaInformation that belongs to this dataset
     *
     * @return
     */
    public MetaInformation getMeta() {
        return meta;
    }

    /**
     * retrieve the central point of this dataset
     *
     * @return
     */
    public GPSkPoint getCenter() {
        if (minLat == Double.NaN || minLon == Double.NaN) {
            throw new IllegalStateException("call detectBounds() first");
        }
        if (maxLat == Double.NaN || maxLon == Double.NaN) {
            throw new IllegalStateException("call detectBounds() first");
        }
        return new GPSkPoint((minLat + maxLat) / 2, (minLon + maxLon) / 2);

    }

    public void detectBounds() {
        double[] old = new double[]{minLat, minLon, maxLat, maxLon};
        double tmpMinLat = Double.MAX_VALUE;
        double tmpMinLon = Double.MAX_VALUE;
        double tmpMaxLat = Double.MIN_VALUE;
        double tmpMaxLon = Double.MIN_VALUE;

        for (GPSkPoint p : pointList) {
            double lat = p.getLat();
            double lon = p.getLon();
            if (tmpMinLat > lat) {
                tmpMinLat = lat;
            }
            if (tmpMinLon > lon) {
                tmpMinLon = lon;
            }
            if (tmpMaxLat < lat) {
                tmpMaxLat = lat;
            }
            if (tmpMaxLon < lon) {
                tmpMaxLon = lon;
            }
        }

        minLat = tmpMinLat;
        minLon = tmpMinLon;
        maxLat = tmpMaxLat;
        maxLon = tmpMaxLon;
        pcs.firePropertyChange("bounds", old, new double[]{tmpMinLat, tmpMinLon, tmpMaxLat, tmpMaxLon});
    }

    public boolean hasBounds() {
        return !Double.isNaN(minLat) && !Double.isNaN(minLon)
                && !Double.isNaN(maxLat) && !Double.isNaN(maxLon);
    }

    /**
     * returns an array with length of 4. if the values aren't defined, it just
     * returns the undefined values. So if you want to make sure, that this
     * method returns reasonable data, check it with {@link hasBounds()} first.
     * Or run {@link detectBounds()}.
     */
    public double[] getBounds() {
        return new double[]{minLat, minLon, maxLat, maxLon};
    }

    public void processPoints(IndexProcedure<GPSkPoint> ip, int tree) {
        ip.putData(pointList);
        for (Rectangle rect : ip.getRectangles()) {
            pointTree[tree].contains(rect, ip);
        }
    }

    public void processPoints(IndexProcedure<GPSkPoint> ip) {
        processPoints(ip, 0);
    }

    public void processSegments(IndexProcedure<GPSkSegment> ip, int tree) {
        ip.putData(segmentList);

        for (Rectangle rect : ip.getRectangles()) {
            segmentTree[tree].intersects(rect, ip);
        }
    }

    public void processSegments(IndexProcedure<GPSkSegment> ip) {
        processSegments(ip, 0);
    }

    public long getLength() {
        long length = 0;
        for (GPSkSegment seg : segmentList) {
            double l = SegmentUtils.length(seg);
            if (Double.isNaN(l)) {
                System.out.println(seg);
            }
            length += l;
        }
        return length;
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        pcs.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        pcs.removePropertyChangeListener(pcl);
    }

    /**
     * contains MetaInformation about this dataset.
     */
    public class MetaInformation {

        /**
         * variable to ease the use of the constructor. If the
         * {@link GPSkEnvironment} just contains points, use this variable when
         * invoking the constructor
         */
        public static final int FOCUS_ON_POINTS = 0;
        /**
         * variable to ease the use of the constructor. If the
         * {@link GPSkEnvironment} just contains segments, use this variable
         * then invoking the constructor.
         */
        public static final int FOCUS_ON_SEGMENTS = 1;
        /**
         * variable to ease the use of the constructor. If the
         * {@link GPSkEnvironment} points and segments, use this variable then
         * invoking the constructor.
         */
        public static final int FOCUS_ON_BOTH = 2;
        public static final String RAW_DATA_NAME = "raw data";
        public static final String CLEANED_DATA_NAME = "cleaned tracks";
        public static final String SMOOTHED_DATA_NAME = "smoothed tracks";
        public static final String OPTICS_RESULTS_NAME = "results of OPTICS";
        public static final String LAYER_VISIBLE = "layer.visible";
        public static final String TYPE = "type";
        /**
         * the name of the dataset
         */
        public String name;
        public boolean showPoints;
        public boolean showSegments;
        /**
         * a keyvalue store for this dataset
         */
        public HashMap<String, String> keyValue = new HashMap<>();
        public boolean center = false;

        /**
         * creates a new instance of MetaData for a dataset.
         *
         * @param name A nice name for the dataset.
         * @param type What is stored in this dataset? points or segments? See
         * also {@link FOCUS_ON_POINTS}, {@link FOCUS_ON_SEGMENTS} and
         * {@link FOCUS_ON_BOTH}
         */
        private MetaInformation(String name, int type) {
            this.name = name;
            switch (type) {
                case FOCUS_ON_BOTH:
                    showPoints = true;
                    showSegments = true;
                    break;
                case FOCUS_ON_POINTS:
                    showPoints = true;
                    showSegments = false;
                    break;
                case FOCUS_ON_SEGMENTS:
                    showPoints = false;
                    showSegments = true;
                    break;
                default:
                    showPoints = false;
                    showSegments = false;
            }
        }

        /**
         * copy the key value store of another hashset. Useful to keep track of
         * what is happening with the data
         *
         * @param kv
         */
        public void copyKeyValue(HashMap<String, String> kv) {
            for (Map.Entry<String, String> entry : kv.entrySet()) {
                keyValue.put(entry.getKey(), entry.getValue());
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final MetaInformation other = (MetaInformation) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            if (this.showPoints != other.showPoints) {
                return false;
            }
            if (this.showSegments != other.showSegments) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 47 * hash + Objects.hashCode(this.name);
            hash = 47 * hash + (this.showPoints ? 1 : 0);
            hash = 47 * hash + (this.showSegments ? 1 : 0);
            return hash;
        }

        public void setTitle(String title) {
            String old = name;
            name = title;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Environment with ");
        builder.append(pointList == null ? "ERROR. POINTLIST IS NULL" : String
                .valueOf(pointList.size()));
        builder.append(" points and ");
        builder.append(segmentList == null ? "ERROR. SEGMENTLIST IS NULL"
                : String.valueOf(segmentList.size()));
        builder.append(" segments. projection=");
        builder.append(projection == null ? "0" : "1").append(" ");
        builder.append("indexOnPoints=");
        builder.append(pointTree == null ? "0" : "1").append(" ");
        builder.append("indexOnSegments=");
        builder.append(segmentTree == null ? "0" : "1").append(" ");
        builder.append("bounds=");
        builder.append(hasBounds() ? "1" : "0");
        return builder.toString();
    }
}
