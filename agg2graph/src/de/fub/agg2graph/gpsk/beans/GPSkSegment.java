/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import com.infomatiq.jsi.Rectangle;
import data.Edge;
import data.Location;
import data.distance.FrechetDistance;
import de.fub.agg2graph.gpsk.data.Indexable;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;
//import geom.FrechetDistance;
import java.awt.geom.Point2D;
import java.util.*;
import org.apache.log4j.Logger;

/**
 *
 * An ordered list of points that represent a GPS-track. Meanwhile it keeps
 * track of the length of the segment and detects its origin.
 *
 * @author Christian Windolf
 */
public class GPSkSegment extends LinkedList<GPSkPoint> implements Indexable{

    private static Logger log = Logger.getLogger(GPSkSegment.class);
    private String title;

    /**
     * creates a new Segment out of agg2graph segment
     *
     * @param seg
     * @return
     */
    public static GPSkSegment createNewSegment(GPSSegment seg) {
        GPSkSegment kSegment = new GPSkSegment();
        Iterator<GPSPoint> it = seg.iterator();
        if (seg.getFirst() instanceof GPSkPoint) {
            while (it.hasNext()) {
                kSegment.add((GPSkPoint) it.next());
            }
            return kSegment;
        } else {
            throw new IllegalArgumentException("The segment must contain GPSkPoints. Use the other createNewSegment method insteast");
        }
    }

    public static GPSkSegment createNewSegment(GPSSegment seg, double originLatitude, double originLongitude) {
        GPSkSegment kSegment = new GPSkSegment();
        Iterator<GPSPoint> it = seg.iterator();
        while (it.hasNext()) {
            kSegment.add(new GPSkPoint(it.next(), new double[]{originLatitude, originLongitude}));
        }
        return kSegment;
    }

    public static GPSkSegment createNewSegment(GPSSegment seg, double[] origin) {
        return createNewSegment(seg, origin[0], origin[1]);
    }

    public GPSkSegment() {
        super();
    }

    public GPSkSegment(GPSkSegment s) {
        super();
        for (GPSkPoint p : s) {
            add(p.copy());
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getMinLatitude() {
        double minLat = Double.MAX_VALUE;
        for (GPSkPoint p : this) {
            if (p.getLat() < minLat) {
                minLat = p.getLat();
            }
        }
        return minLat;
    }

    public double getMinLongitude() {
        double minLon = Double.MAX_VALUE;
        for (GPSkPoint p : this) {
            if (p.getLon() < minLon) {
                minLon = p.getLon();
            }
        }
        return minLon;
    }

    public double getMaxLatitude() {
        double maxLat = Double.MIN_VALUE;
        for (GPSkPoint p : this) {
            if (p.getLat() > maxLat) {
                maxLat = p.getLat();
            }
        }
        return maxLat;
    }

    public double getMaxLongitude() {
        double maxLon = Double.MIN_VALUE;
        for (GPSkPoint p : this) {
            if (p.getLon() > maxLon) {
                maxLon = p.getLon();
            }
        }
        return maxLon;
    }

    public GPSkSegment reverse() {
        GPSkSegment seg = new GPSkSegment();
        for (int i = size() - 1; i >= 0; i--) {
            seg.add(get(i));
        }
        return seg;
    }

    public double calculateLength() {
        double length = 0;
        if (isEmpty()) {
            return 0;
        } else if (size() == 1) {
            return 0;
        }
        Iterator<GPSkPoint> it = iterator();
        GPSkPoint first = it.next();
        while (it.hasNext()) {
            GPSkPoint second = it.next();
            length += first.distanceTo(second);
            first = second;
        }

        return length;
    }

    /**
     *
     * @param seg2
     * @param eps
     * @return
     * @deprecated
     */
    public boolean isFrechet(GPSkSegment seg2, double eps) {
//        Point2D[] p = new Point2D[size()];
//        Iterator<GPSkPoint> iterator1 = iterator();
//        double origin_latitude;
//        double origin_longitude;
//        if (seg2.getMinLatitude() <= minLat) {
//            origin_latitude = seg2.getMinLatitude();
//        } else {
//            origin_latitude = minLat;
//        }
//        if (seg2.getMinLongitude() <= minLon) {
//            origin_longitude = seg2.getMinLongitude();
//        } else {
//            origin_longitude = minLon;
//        }
//
//
//
//        for (int i = 0; iterator1.hasNext(); i++) {
//            GPSkPoint point = iterator1.next();
//            double[] xy = point.getDistancesToOrigin(origin_latitude, origin_longitude);
//            p[i] = new Point2D.Double(xy[0], xy[1]);
//
//        }
//
//        Point2D[] q = new Point2D[seg2.size()];
//        Iterator<GPSkPoint> iterator2 = seg2.iterator();
//        for (int i = 0; iterator2.hasNext(); i++) {
//            GPSkPoint point = iterator2.next();
//            double[] xy = point.getDistancesToOrigin(origin_latitude, origin_longitude);
//            q[i] = new Point2D.Double(xy[0], xy[1]);
//        }
//        FrechetDistance fd = new FrechetDistance(p, q);
//
//        
//
//        boolean fre = fd.isFrechet(eps);
//
//        if (fd.isFrechet(eps)) {
//            return true;
//        }
//        iterator2 = seg2.iterator();
//        for (int i = q.length - 1; iterator2.hasNext(); i--) {
//            GPSkPoint point = iterator2.next();
//            double[] xy = point.getDistancesToOrigin(origin_latitude, origin_longitude);
//            q[i] = new Point2D.Double(xy[0], xy[1]);
//        }
//
//        fd = new FrechetDistance(p, q);
//        return fd.isFrechet(eps);

        double oLat = getFirst().getLat();
        double oLon = getFirst().getLon();

        List<Edge> list1 = new LinkedList<>();
        Iterator<GPSkPoint> it1 = iterator();
        GPSkPoint first = it1.next();
        while (it1.hasNext()) {
            GPSkPoint second = it1.next();
            Location l1 = new Location(
                    GPSkPoint.xCoordinate(oLat, oLon, first.getLon()),
                    GPSkPoint.yCoordinate(oLat, first.getLat()));
            Location l2 = new Location(
                    GPSkPoint.xCoordinate(oLat, oLon, second.getLon()),
                    GPSkPoint.yCoordinate(oLat, second.getLon()));
            Edge e = new Edge(l1, l2, (float) first.distanceTo(second));
            list1.add(e);
            first = second;
        }

        Vector<Edge> vector1 = new Vector<>(list1);

        List<Edge> list2 = new LinkedList<>();
        Iterator<GPSkPoint> it2 = seg2.iterator();
        first = it2.next();
        while (it2.hasNext()) {
            GPSkPoint second = it2.next();
            Location l1 = new Location(
                    GPSkPoint.xCoordinate(oLat, oLon, first.getLon()),
                    GPSkPoint.yCoordinate(oLat, first.getLat()));
            Location l2 = new Location(
                    GPSkPoint.xCoordinate(oLat, oLon, second.getLon()),
                    GPSkPoint.yCoordinate(oLat, second.getLon()));
            Edge e = new Edge(l1, l2, (float) first.distanceTo(second));
            list2.add(e);
            first = second;

        }

        Vector<Edge> vector2 = new Vector<>(list2);



        FrechetDistance fd = new FrechetDistance(vector1, vector2, eps);
        double d = fd.computeEpsilon();
        if (d <= eps) {
            return true;
        } else {
            List<Edge> list3 = new LinkedList<>();
            ListIterator<GPSkPoint> it3 = seg2.listIterator(seg2.size());
            first = it3.previous();
            while (it3.hasPrevious()) {
                GPSkPoint second = it3.previous();
                Location l1 = new Location(
                        GPSkPoint.xCoordinate(oLat, oLon, first.getLon()),
                        GPSkPoint.yCoordinate(oLat, first.getLat()));
                Location l2 = new Location(
                        GPSkPoint.xCoordinate(oLat, oLon, second.getLon()),
                        GPSkPoint.yCoordinate(oLat, second.getLon()));
                Edge e = new Edge(l1, l2, (float) first.distanceTo(second));
                list3.add(e);
                first = second;
            }
            Vector<Edge> vector3 = new Vector<>(list3);
            FrechetDistance fd2 = new FrechetDistance(vector1, vector3, eps);
            d = fd2.computeEpsilon();
            return (d <= eps);
        }




    }

    public GPSkSegment copy() {
        GPSkSegment s = new GPSkSegment();
        Iterator<GPSkPoint> it = iterator();
        while (it.hasNext()) {
            GPSkPoint p = it.next().copy();
            s.add(p);
        }
        return s;
    }

    /**
     * <p><b>This class was designed to provide additional methods for the gpsk
     * processing.</b></p> <p>It was not possible, to extend the GPSSegment
     * class. But some methods in the agg2graph framework require a GPSSegment
     * as input. This method create a <b>copy</b> of the segment, that is
     * processable by agg2graph methods</p>
     *
     * @return a copy, north-east informations will remain, but you have to do a
     * typecast to retrieve them: <br /> GPSkPoint p = (GPSkPoint)
     * gpsSegment.get(i);
     *
     */
    public GPSSegment agg2graphVersion() {
        GPSSegment segment = new GPSSegment();
        Iterator<GPSkPoint> it = iterator();
        while (it.hasNext()) {
            segment.add(it.next().copy());
        }
        return segment;
    }

    @Override
    public Rectangle getRectangle() {
        double[] projBounds = getProjectedBounds();

        Rectangle rect = new Rectangle((float) projBounds[0], 
        		(float) projBounds[1], 
        		(float) projBounds[2], 
        		(float) projBounds[3]);
        return rect;
    }
    
    public double[] getProjectedBounds(){
    	double minEast = Double.MAX_VALUE;
        double minNorth = Double.MAX_VALUE;

        double maxEast = Double.MIN_VALUE;
        double maxNorth = Double.MIN_VALUE;

        for (GPSkPoint p : this) {
            if (p.getEast() < minEast) {
                minEast = p.getEast();
            }
            if (p.getNorth() < minNorth) {
                minNorth = p.getNorth();
            }

            if (p.getEast() > maxEast) {
                maxEast = p.getEast();
            }
            if (p.getNorth() > maxNorth) {
                maxNorth = p.getNorth();
            }

        }
        return new double[]{minEast, minNorth, maxEast, maxNorth};

    	
    }

    @Override
    public String toString() {
        StringBuilder builder;
        if (title == null) {
            builder = new StringBuilder("GPSkSegment{points=");
        } else {
            builder = new StringBuilder(title);
            builder.append("{points=");
        }
        builder.append(size()).append(",length=").append(calculateLength());
        if (isEmpty()) {
            builder.append("}");
            return builder.toString();
        } else {
            builder.append(",first=[").append(getFirst().getLat()).append(',').append(getFirst().getLon());
            builder.append("],last=[").append(getLast().getLat()).append(',').append(getLast().getLon());
            builder.append("]}");
            return builder.toString();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final GPSkSegment other = (GPSkSegment) obj;
        if (size() != other.size()) {
            return false;
        }

        ListIterator<GPSkPoint> myIterator = listIterator();
        ListIterator<GPSkPoint> otherIterator = other.listIterator();
        while (myIterator.hasNext() && otherIterator.hasNext()) {
            GPSkPoint myPoint = myIterator.next();
            GPSkPoint otherPoint = otherIterator.next();
            if (!myPoint.equals(otherPoint)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }
}
