/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Every {@link DataSet} has a history about what happened before.
 * 
 * @author Christian Windolf
 *
 */
public class History {
	
	private final Object preferences;
	private final List<History> calculationSteps;

	/**
	 * @param preferences The preferences that were set, when this dataset was created
	 * @param cal if this dataset was processed by some other algorithms before, you should add them
	 */
	public History(Object preferences, List<History> calculationSteps) {
		this.preferences = preferences;
		this.calculationSteps = calculationSteps;
	}
	
	/**
	 * 
	 * @param preferences The preference that were set, when this dataset was created
	 * @param predecessor the predecessors.
	 */
	public History(Object preferences, History ... predecessor){
		this(preferences, Arrays.asList(predecessor));
		
	}
	
	/**
	 * if nothing happened with this dataset before, use this constructor
	 * @param preferences
	 */
	public History(Object preferences){
		this(preferences, new LinkedList<History>());
	}
	
	public Object getPreferences(){
		return preferences;
	}
	
	public boolean isForked(){
		return calculationSteps.size() <= 1;
	}
	
	public List<History> getAllCalculationSteps(){
		return calculationSteps;
	}
	
	public History getPredecessor(){
		if(calculationSteps.isEmpty()){
			return null;
		}
		return calculationSteps.get(0);
	}
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder("History::Preferences = ");
		builder.append(preferences);
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((calculationSteps == null) ? 0 : calculationSteps.hashCode());
		result = prime * result
				+ ((preferences == null) ? 0 : preferences.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		History other = (History) obj;
		if (calculationSteps == null) {
			if (other.calculationSteps != null)
				return false;
		} else if (!calculationSteps.equals(other.calculationSteps))
			return false;
		if (preferences == null) {
			if (other.preferences != null)
				return false;
		} else if (!preferences.equals(other.preferences))
			return false;
		return true;
	}
	
	

}
