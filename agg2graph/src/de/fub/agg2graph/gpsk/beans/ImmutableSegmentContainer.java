/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import java.util.LinkedList;
import java.util.List;

import de.fub.agg2graph.gpsk.data.Index;
import de.fub.agg2graph.gpsk.data.SegmentIndex;
import de.fub.agg2graph.gpsk.utils.SegmentCollection;

/**
 * @author Christian Windolf
 *
 */
public class ImmutableSegmentContainer extends ImmutablePointContainer implements SegmentIndex {
	protected List<GPSkSegment> segments;
	protected List<Index<GPSkSegment>> segmentIndexList = new LinkedList<>();

	/**
	 * @param points
	 * @param history
	 */
	public ImmutableSegmentContainer(List<GPSkSegment> segments, History history) {
		super(SegmentCollection.extractAllPoint(segments), history);
		this.segments = segments;
	}
	
	public List<GPSkSegment> getSegments(){
		return segments;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.SegmentIndex#getSegmentIndex()
	 */
	@Override
	public Index<GPSkSegment> getSegmentIndex() {
		synchronized(segmentIndexList){
			if(segmentIndexList.isEmpty()){
				return null;
			} 
			return segmentIndexList.remove(0);
		}
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.SegmentIndex#createSegmentIndex()
	 */
	@Override
	public Index<GPSkSegment> createSegmentIndex() {
		GPSkSegment[] array = new GPSkSegment[segments.size()];
		return new Index<GPSkSegment>(segments.toArray(array));
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.SegmentIndex#getOrCreateIndex()
	 */
	@Override
	public Index<GPSkSegment> getOrCreateIndex() {
		Index<GPSkSegment> index = getSegmentIndex();
		if(index == null){
			return createSegmentIndex();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.SegmentIndex#returnSegmentIndex(de.fub.agg2graph.gpsk.data.Index)
	 */
	@Override
	public void returnSegmentIndex(Index<GPSkSegment> index) {
		synchronized(this){
			segmentIndexList.add(index);
		}
	}

}
