/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.fub.agg2graph.gpsk.data.Index;
import de.fub.agg2graph.gpsk.data.PointIndex;
import de.fub.agg2graph.gpsk.logic.Projection;
import de.fub.agg2graph.gpsk.logic.ProjectionFactory;
import de.fub.agg2graph.gpsk.utils.PointCollection;

/**
 * @author Christian Windolf
 *
 */
public class ImmutablePointContainer implements DataSet, BoundedPropertyChange, PointIndex, ProjectionSupport {
	protected String title = "no title";
	protected History history;
	protected double[] bounds = null;
	protected GPSkPoint center = null;
	protected List<GPSkPoint> points;
	protected List<Index<GPSkPoint>> pointIndexList = new LinkedList<Index<GPSkPoint>>();
	
	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	
	protected Projection projection;

	/**
	 * 
	 */
	public ImmutablePointContainer(List<GPSkPoint> points, History history) {
		this.points = points;
		this.history = history;
		bounds = PointCollection.getBounds(points);
		double centerLat = (bounds[0] + bounds[2]) / 2d;
		double centerLon = (bounds[1] + bounds[3]) / 2d;
		center = new GPSkPoint(centerLat, centerLon);
		projection = ProjectionFactory.getInstance().createProjection(bounds);
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.DataSet#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.DataSet#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		String old = this.title;
		this.title = title;
		pcs.firePropertyChange("title", old, this.title);
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.DataSet#getHistory()
	 */
	@Override
	public History getHistory() {
		return history;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.DataSet#getCenter()
	 */
	@Override
	public GPSkPoint getCenter() {
		return center;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.DataSet#getBounds()
	 */
	@Override
	public double[] getBounds() {
		return bounds;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.BoundedPropertyChange#addPropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		pcs.addPropertyChangeListener(pcl);
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.BoundedPropertyChange#removePropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		pcs.removePropertyChangeListener(pcl);
		
	}

	
	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.PointIndex#getPointIndex()
	 */
	@Override
	public Index<GPSkPoint> getPointIndex() {
		synchronized(pointIndexList){
			if(pointIndexList.isEmpty()){
				return null;
			} else {
				return pointIndexList.remove(0);
			}
		}
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.PointIndex#getOrCreatePointIndex()
	 */
	@Override
	public Index<GPSkPoint> getOrCreatePointIndex() {
		Index<GPSkPoint> index = getPointIndex();
		if(index == null){
			return createPointIndex();
		} else{
			return index;
		}
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.PointIndex#createPointIndex()
	 */
	@Override
	public Index<GPSkPoint> createPointIndex() {
		GPSkPoint[] array = new GPSkPoint[points.size()];
		return new Index<>(points.toArray(array));
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.PointIndex#returnPointIndex(de.fub.agg2graph.gpsk.data.Index)
	 */
	@Override
	public void returnPointIndex(Index<GPSkPoint> index) {
		synchronized(pointIndexList){
			pointIndexList.add(index);
		}
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.ProjectionSupport#setProjection(de.fub.agg2graph.gpsk.logic.Projection)
	 */
	@Override
	public void setProjection(Projection p) {
		this.projection = p; 
		for(GPSkPoint point : points){
			projection.setProjection(point);
		}
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.ProjectionSupport#getProjection()
	 */
	@Override
	public Projection getProjection() {
		return projection;
	}
	
	/**
	 * <h3>WARING</h3>
	 * <b>The name of this class says, that the points are not mutable, but they are!</b> <br />
	 * The reason is, that the points should be still editable for classes that inherit from this class.
	 * For example {@link PointContainer}. 
	 * To return a complete copy of this dataset could be a solution, but it would just cost time.
	 * And most algorithms do not intend to do so, so everything is fine.
	 * If you need to manipulate this list for whatever, create a copy of it.
	 * 
	 * 
	 * @return points inside this Container
	 * @see Collections#copy(List, List)
	 */
	public List<GPSkPoint> getPoints(){
		return points;
	}
	
	@Override
	public String toString(){
		return "ImmutablePointContainer with " + points.size() + " points. Projection: " + projection.getClass().getSimpleName();
	}
	
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(bounds);
		result = prime * result + ((center == null) ? 0 : center.hashCode());
		result = prime * result + ((history == null) ? 0 : history.hashCode());
		result = prime * result + ((points == null) ? 0 : points.hashCode());
		result = prime * result
				+ ((projection == null) ? 0 : projection.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImmutablePointContainer other = (ImmutablePointContainer) obj;
		if (!Arrays.equals(bounds, other.bounds))
			return false;
		if (center == null) {
			if (other.center != null)
				return false;
		} else if (!center.equals(other.center))
			return false;
		if (history == null) {
			if (other.history != null)
				return false;
		} else if (!history.equals(other.history))
			return false;
		if (points == null) {
			if (other.points != null)
				return false;
		} else if (!points.equals(other.points))
			return false;
		if (projection == null) {
			if (other.projection != null)
				return false;
		} else if (!projection.equals(other.projection))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	
}
