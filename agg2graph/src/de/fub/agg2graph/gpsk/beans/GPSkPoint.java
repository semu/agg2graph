/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import static java.lang.Math.acos;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.tan;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

import java.util.Date;
import java.util.UUID;

import org.jdesktop.swingx.mapviewer.GeoPosition;

import com.infomatiq.jsi.Rectangle;
import com.infomatiq.jsi.rtree.RTree;

import de.fub.agg2graph.gpsk.data.Indexable;
import de.fub.agg2graph.structs.GPSPoint;


/**
 * <h3>Extends the {@link GPSPoint} Class from the agg2graph framework.</h3>
 * <p>It provides an GeoPostion Object with the same coordinates for further processing with
 * the map panel of from the swingX library.<br />
 * It also provides methods to calculate the distance to another point and the bearing to it.</p>
 * <p>The north/east coordinates are designed to enable the processing with frameworks, that only
 * know an eucledian distance. 
 * For example jsi with a rtree implementation or frechet distance.
 * If you have some points in a small area (for example Berlin, that consumes an area 
 * of about 40x40 kilometers), you can hand over some kind of origin point in the constructor
 * (I just recommend you, to take the furthermost southwestern point) and it automatically calculates
 * the distance to the north and the east.</p>
 * 
 *
 * @author Christian Windolf
 */
public class GPSkPoint extends GPSPoint implements Indexable{
    //variable used for distance calculation
    public static final double RADIUS = 6378388; //radius of the earth in meters
    
    /**
     * <p>calculate efficiently the distance between a given origin point and the latitude. </p>
     * <p>use this only at short range (less than 50km). the latitude coordinate is not needed. This method is
     * useful for converting sphere coordinates to planar coordinates.</p>
     * @param originLat latitude coordinate of the origin point
     * @param originLon longitude coordinate of the origin point
     * @param lon longitude coordinate of the point
     * @return x distance in meters
     * @deprecated 
     */
    public static double xCoordinate(double originLat, double originLon, double lon){
        
        double s = sin(toRadians(originLat));
        s *= s;
        double c = cos(toRadians(originLat));
        c *= c;
        double x = RADIUS * acos(s + (c * cos(toRadians(lon) - toRadians(originLon))));
        
        if(originLon < lon){
            return x;
        } else{
            return -x;
        }
        
    }
    
    /**
     * <p>calculates efficientliy the distance between a given point and the longitude. </p>
     * <p>Use this only at short range (less than 50km). The longitude coordinates are not needed.
     * This method is usefulk for converting sphere coordinates to planar coordinates</p>
     * @param originLat the latitude coordinate of the origin point
     * @param lat the latitude coordinate of the point of interest.
     * @return y distance in meters
     * @deprecated 
     */
    public static double yCoordinate(double originLat, double lat){
        return RADIUS * (toRadians(lat) - toRadians(originLat));
    }
    
    private final UUID uid;
    
    /**
     * The JXMapViewer expects a coordinate of the type GeoPosition. <nr />
     * So here it is, it contains exactly the same coordinates as this object.
     */
    public final GeoPosition gp;
    
    
    private  double north = Double.NaN, east = Double.NaN;
    
    private Date timestamp;
    
    /**
     * <h4>The simplest and lamest constructor of all</h4>
     * <p>As no origin point is expected, the north and east value can't be calculated and are therefore
     * zero</p>
     * @param lat latitude coordinate
     * @param lon longitude coordinate
     */
    public GPSkPoint(double lat, double lon){
        super(lat, lon);
        uid = UUID.randomUUID();
        setID(String.valueOf(uid));
        gp = new GeoPosition(lat, lon);
        
    }
    
    /**
     * <h4>Simple constructor with id</h4>
     * <p>As no origin point is expected, the north and east value can't be calculated and are therefore
     * zero</p>
     * @param id A name for the point. 
     * @param lat latitude coordinate
     * @param lon longitude coordinate
     */
    public GPSkPoint(String id, double lat, double lon){
        super(id, lat, lon);
        uid = UUID.randomUUID();
        gp = new GeoPosition(lat, lon);
        
        
    }
    
    /**
     * <h4>Very simple constructor for conversion</h4>
     * <p>As no origin point is expected, the north and east value can't be calculated and are therefore
     * zero</p>
     * @param p The point
     */
    public GPSkPoint(GPSPoint p){
        super(p.getID(), p.getLat(), p.getLon());
        uid = UUID.randomUUID();
        gp = new GeoPosition(p.getLat(), p.getLon());
        if(getID() == null){
            setID(String.valueOf(uid));
        }
        
        
    }
    
    /**
     * <h4>Very simple constructor for conversion with id</h4>
     * <p>As no origin point is expected, the north and east value can't be calculated and are therefore
     * zero</p>
     * @param id the new id for the point. The id of the input point p will be ignored
     * @param p the point
     */
    public GPSkPoint(String id, GPSPoint p){
        super(id, p.getLat(), p.getLon());
        uid = UUID.randomUUID();
        gp = new GeoPosition(p.getLat(), p.getLon());
        
    }
    
    /**
     * <h4>Constructor with an origin as input</h4>
     * @param p the point
     * @param origin the origin of the coordinate system
     * @deprecated
     */
    public GPSkPoint(GPSPoint p, double[] origin){
        super(p.getLat(), p.getLon());
        uid = UUID.randomUUID();
        gp = new GeoPosition(p.getLat(), p.getLon());
        north = yCoordinate(origin[0], p.getLat());
        east = xCoordinate(origin[0], origin[1], p.getLon());
        
    }
    
    /**
     * constructor used to copy a point
     * @param p 
     */
    protected GPSkPoint(GPSkPoint p){
        super(p.ID, p.latlon[0], p.latlon[1]);
        north = p.north;
        east = p.east;
        uid = UUID.randomUUID();
        gp = p.gp;
    }
    
    /**
     * <h4>Constructor with an id and origin</h4>
     * @param id
     * @param p
     * @param origin 
     */
    public GPSkPoint(String id, GPSPoint p, GPSPoint origin){
        super(id, p.getLat(), p.getLon());
        uid = UUID.randomUUID();
        gp = new GeoPosition(p.getLat(), p.getLon());
        north = yCoordinate(origin.getLat(), latlon[0]);
        east = xCoordinate(origin.getLat(), origin.getLon(), latlon[1]);
    }
    
    /**
     * <h4>constructor with an origin</h4>
     * @param lat
     * @param lon
     * @param origin 
     */
    public GPSkPoint(double lat, double lon, GPSkPoint origin){
        super(lat, lon);
        uid = UUID.randomUUID();
        gp = new GeoPosition(lat, lon);
        north = origin.distanceTo(lat, origin.getLon());
        east = origin.distanceTo(origin.getLat(), lon);
    }
    
    /**
     * <h4>constructor with an origin</h4>
     * @param lat
     * @param lon
     * @param originLat
     * @param originLon 
     * @deprecated("projection should not be initiated in the constructor")
     */
    public GPSkPoint(double lat, double lon, double originLat, double originLon){
        super(lat, lon);
        uid = UUID.randomUUID();
        gp = new GeoPosition(lat, lon);
        east = xCoordinate(originLat, originLon, lon);
        north = yCoordinate(originLat, originLon);
    }
    
 
            
          
    
    
    /**
     * calculates a new Rectangle for the  {@link RTree}.
     * 
     * @return values will be only be meaningful, if you used a constructor wiht an origin
     */
    @Override
    public Rectangle getRectangle(){
        if(Double.isNaN(east) || Double.isNaN(north)){
            throw new IllegalStateException
                    ("east and north variable must "
                    + "be initialized to create a rectangle!");
        }
        return new Rectangle((float) east, (float) north, (float) east, (float) north);
    }
    
    /**
     * 
     * @return 0, if this point was constructed without an origin. otherwise it will
     * return the north-south-distance to the origin.
     */
    public double getNorth(){
        return north;
    }
    
    /**
     * 
     * @return 0, if this point was constructed without an origin. otherwise it will return the east-west distance to the origin.
     */
    public double getEast(){
        return east;
    }
    

    /**
     * calculates the bearing to point2.
     * @param point2
     * @return A value between 0 and 360. Value will be in degrees, 0 means north, 90 east, 180 south and so on
     */
    public double bearingTo(GPSPoint point2){
        
        return (toDegrees(bearingToRadians(point2)) + 360) % 360;
    }
    
    public double bearingToRadians(GPSPoint point2){
        double deltaLon = toRadians(point2.getLon()) - toRadians(latlon[1]);
        
        double y = sin(deltaLon) * cos(toRadians(point2.getLat()));
        double x = cos(toRadians(latlon[0])) * sin(toRadians(point2.getLat()));
        x -= sin(toRadians(latlon[0])) * cos(toRadians(point2.getLat())) * cos(deltaLon);
        double bearing = atan2(y,x);
        return bearing;
    }
    
    /**
     * calculates the distance to point 2.
     * @param point2
     * @return distance in meters
     * @deprecated
     */
    public double distanceTo(GPSPoint point2){
        double lat1 = toRadians(latlon[0]);
        double lon1 = toRadians(latlon[1]);
        double lat2 = toRadians(point2.getLat());
        double lon2 = toRadians(point2.getLon());
        
        double distanceDegree = (sin(lat1) * sin(lat2))  
                + (cos(lat1) * cos(lat2) * cos(lon2 - lon1));
        
        return RADIUS * acos(distanceDegree);
    }
    
    /**
     * calculates the distance to position gp.
     * @param gp 
     * @return distance in meters
     * @deprecated 
     */
    public double distanceTo(GeoPosition gp){
        double lat1 = toRadians(latlon[0]);
        double lon1 = toRadians(latlon[1]);
        double lat2 = toRadians(gp.getLatitude());
        double lon2 = toRadians(gp.getLongitude());
        
        double distanceDegree = (sin(lat1) * sin(lat2))  
                + (cos(lat1) * cos(lat2) * cos(lon2 - lon1));
        
        return RADIUS * acos(distanceDegree);
        
    }
    
    /**
     * calculates the distance to a given point.
     * @param lat
     * @param lon
     * @return distance in meters
     * @deprecated 
     */
    public double distanceTo(double lat, double lon){
        double lat1 = toRadians(latlon[0]);
        double lon1 = toRadians(latlon[1]);
        double lat2 = toRadians(lat);
        double lon2 = toRadians(lon);
        
        double distanceDegree = (sin(lat1) * sin(lat2))  
                + (cos(lat1) * cos(lat2) * cos(lon2 - lon1));
        
        return RADIUS * acos(distanceDegree);
    }
    
    /**
     * calculate the distance to a different origin
     * @param lat
     * @param lon
     * @return 
     */
    public double[] getDistancesToOrigin(double lat, double lon){
        double lat1 = toRadians(lat);
        double lon1 = toRadians(lon);
        double lat2 = toRadians(latlon[0]);
        double lon2 = toRadians(latlon[1]);
        
        double s = sin(lat1);
        s *= s;
        double c = cos(lat1);
        c *= c;
        
        double eastDistanceDegree = s + (c * cos(lon2 - lon1));
                
        double[] eastNorth = new double[]{
            RADIUS * acos(eastDistanceDegree),
            RADIUS * (lat2 - lat1)
        };
        
        if(lon >= latlon[1]){
            eastNorth[0] = -eastNorth[0];
        }
        
        
        return eastNorth;
                
    }
    
    public double[] getDistancesToOrigin(GPSPoint origin){
        return getDistancesToOrigin(origin.getLat(), origin.getLon());
    }

    

    /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(east);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(north);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((timestamp == null) ? 0 : timestamp.hashCode());
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GPSkPoint other = (GPSkPoint) obj;
		if (Double.doubleToLongBits(east) != Double
				.doubleToLongBits(other.east))
			return false;
		if (Double.doubleToLongBits(north) != Double
				.doubleToLongBits(other.north))
			return false;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		return true;
	}

    

    

    @Override
    public String toString() {
        return "GPSkPoint{lat: " + latlon[0] + ", lon: " + latlon[1] +  '}';
    }
    
    /**
     * just for debugging/testing purpose
     * @return 
     */
    public String toExtendedString(){
        return toString() + " EAST=" + east + " NORTH=" + north;
    }
    
    public GPSkPoint copy(){
        return new GPSkPoint(this);
    }
    
    public GPSPoint dirDist(double bearing, double distance){
        double bearingAngle = toRadians(bearing);
        double distanceAngle = distance / RADIUS;
        
        double diffLon = sin(distanceAngle) * sin(bearingAngle);
        double diffLat = tan(diffLon) * (1/tan(bearingAngle));
        
        diffLon = toDegrees(asin(diffLon));
        diffLat = toDegrees(asin(diffLat));
        
        return new GPSPoint(latlon[0] + diffLat, latlon[1] + diffLon);
    }
    
    /**
     * @deprecated 
     */
    public void setOrigin(double lat, double lon){
        this.east = xCoordinate(lat, lon, latlon[1]);
        this.north = yCoordinate(lat, latlon[0]);
    }
    
    public void setNorth(double n){
        this.north = n;
    }
    
    public void setEast(double e){
        this.east = e;
    }
    

    

    
    
    
    
}
