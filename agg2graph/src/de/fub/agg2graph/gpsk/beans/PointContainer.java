/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.beans;

import java.util.List;

import de.fub.agg2graph.gpsk.data.Index;
import de.fub.agg2graph.gpsk.utils.PointCollection;
import de.fub.agg2graph.gpsk.utils.PointUtils;
import de.fub.agg2graph.gpsk.utils.SegmentCollection;

/**
 * @author Christian Windolf
 * 
 */
public class PointContainer extends ImmutablePointContainer {

	/**
	 * @param points
	 * @param history
	 */
	public PointContainer(List<GPSkPoint> points, History history) {
		super(points, history);
	}

	public void setPoints(List<GPSkPoint> points) {
		List<GPSkPoint> old = this.points;
		this.points = points;
		pcs.firePropertyChange("points", old, points);
		handlePointsChanged(true);
	}

	public boolean removePoint(GPSkPoint p) {
		boolean b = points.remove(p);
		if (b) {
			pcs.firePropertyChange("points", p, null);
			handlePointsChanged();
		}
		return b;
	}

	public void removePoint(int i) {
		try {
			GPSkPoint old = points.get(i);
			points.remove(i);
			pcs.firePropertyChange("points", old, null);
			handlePointsChanged();
		} catch (IndexOutOfBoundsException e) {
			// do nothing
		}
	}
	
	public void addPoint(GPSkPoint p){
		points.add(p);
		pcs.firePropertyChange("points", null, p);
		handlePointsChanged(!PointUtils.insideBounds(p, bounds));
	}

	private void handlePointsChanged(boolean updateBounds) {
		synchronized (pointIndexList) {
			pointIndexList.clear();
		}
		if(updateBounds){
			bounds = PointCollection.getBounds(points);
			double lat = (bounds[0] + bounds[2]) / 2d;
			double lon = (bounds[1] + bounds[3]) / 2d;
			center = new GPSkPoint(lat, lon);
		}
		
		
	}
	
	private void handlePointsChanged(){
		handlePointsChanged(false);
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.beans.ImmutablePointContainer#returnPointIndex(de.fub.agg2graph.gpsk.data.Index)
	 */
	@Override
	public void returnPointIndex(Index<GPSkPoint> index) {
		// do nothing, too dangerous to recycle the index
	}
	
	
}
