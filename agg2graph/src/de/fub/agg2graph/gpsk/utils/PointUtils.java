/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.utils;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.structs.GPSPoint;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;
import static java.lang.Math.asin;

/**
 *
 * @author doggy
 */
public class PointUtils {
    public static final double RADIUS = 6378388;
    
    /**
     * calculates the bearing to point2.
     * @param point2
     * @return A value between 0 and 360. Value will be in degrees, 0 means north, 90 east, 180 south and so on
     */
    public static double bearingTo(GPSPoint point1, GPSPoint point2) {
        return (toDegrees(bearingToRadians(point1, point2)) + 360) % 360;
    }
    
    public static double bearingToRadians(GPSPoint point1, GPSPoint point2) {
        double deltaLon = toRadians(point2.getLon()) - toRadians(point1.getLon());
        double y = sin(deltaLon) * cos(toRadians(point2.getLat()));
        double x = cos(toRadians(point1.getLat())) * sin(toRadians(point2.getLat()));
        x -= sin(toRadians(point1.getLat())) * cos(toRadians(point2.getLat())) * cos(deltaLon);
        double bearing = atan2(y,x);
        return bearing;
    }
    
    public static double[] destinationByBearingAndDistance(GPSPoint p, double bearing, double distance) {
        double sinDISTANCEradius = sin(distance/RADIUS);
        double cosDISTANCEradius = cos(distance/RADIUS);
        
        double sinLAT = sin(toRadians(p.getLat()));
        double cosLAT = cos(toRadians(p.getLat()));
        
        double lat1 = sinLAT * cosDISTANCEradius;
        double lat2 = cosLAT * sinDISTANCEradius * cos(bearing);
        double lat = asin(lat1 + lat2);
        lat = toDegrees(lat);
        
        double lon1 = sin(bearing)*sinDISTANCEradius * cosLAT;
        double lon2 = cosDISTANCEradius - (sinLAT * (lat1 + lat2));
        double lon = p.getLon() + toDegrees(atan2(lon1, lon2));
        return new double[]{lat, lon};
    }
    
    public static GPSkPoint midPointGeneration(GPSPoint p1, GPSPoint p2, double distance) {
        double bearing = bearingToRadians(p1, p2);
        double[] latlon = destinationByBearingAndDistance(p1, bearing, distance);
        return new GPSkPoint(latlon[0], latlon[1]);
    }
    
    public static boolean insideBounds(GPSkPoint p, double[] bounds) {
    	double lat = p.getLat();
    	double lon = p.getLon();
    	boolean lat_inside = lat >= bounds[0] && lat <= bounds[2];
    	boolean lon_inside = lon >= bounds[1] && lon <= bounds[3];
    	return lat_inside && lon_inside;
    }
}