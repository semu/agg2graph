/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.utils;

import java.util.ListIterator;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.logic.DistanceFactory;
import de.fub.agg2graph.gpsk.logic.PointDistance;

/**
 * @author Christian Windolf
 *
 */
public class SegmentUtils {
	
	private static PointDistance distance = DistanceFactory.getInstance().createNewInstance();

	public static double angle(GPSkSegment seg){
		return PointUtils.bearingTo(seg.getFirst(), seg.getLast());
	}
	
	/**
	 * calculates the length of a segment
	 * @param seg
	 * @return
	 */
	public static long length(GPSkSegment seg){
		if(seg == null){
			return 0;
		}
		if(seg.size() <= 1){
			return 0;
		}
		if(seg.size() == 2){
			return Math.round(distance.calculate(seg.getFirst(), seg.getLast()));
		}
		double length = 0;
		ListIterator<GPSkPoint> iter = seg.listIterator();
		GPSkPoint first = iter.next();
		GPSkPoint second = iter.next();
		do{
			double l = distance.calculate(first, second);
			if(Double.isNaN(l)){
				System.out.println("Distance is null: " + first + " ===== " + second);
			}
			length += l;
			first = second;
			second = iter.next();
			
		} while(iter.hasNext());
		return Math.round(length);
	}

}
