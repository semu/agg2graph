/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.utils;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;

import java.util.LinkedList;
import java.util.List;

import static de.fub.agg2graph.gpsk.utils.UtilConstants.*;

/**
 *
 * @author doggy
 */
public class SegmentCollection {

    public static double[] getBounds(List<GPSkSegment> list) {
        double[] result = new double[4];

        result[SOUTHWEST_LAT] = Double.MAX_VALUE;
        result[SOUTHWEST_LON] = Double.MAX_VALUE;

        result[NORTHEAST_LAT] = Double.MIN_VALUE;
        result[NORTHEAST_LON] = Double.MIN_VALUE;

        for (GPSkSegment seg : list) {
            double[] bounds = PointCollection.getBounds(seg);
            if (bounds[SOUTHWEST_LAT] < result[SOUTHWEST_LAT]) {
                result[SOUTHWEST_LAT] = bounds[SOUTHWEST_LAT];
            }
            if (bounds[SOUTHWEST_LON] < result[SOUTHWEST_LON]) {
                result[SOUTHWEST_LON] = bounds[SOUTHWEST_LON];
            }

            if (bounds[NORTHEAST_LAT] > result[NORTHEAST_LAT]) {
                result[NORTHEAST_LAT] = bounds[NORTHEAST_LAT];
            }
            if(bounds[NORTHEAST_LON] > result[NORTHEAST_LON]){
                result[NORTHEAST_LON] = bounds[NORTHEAST_LON];
            }
            
        }
        
        return result;
    }
    
    public static int getMaxPoints(List<GPSkSegment> list){
        int maxPoints = 0;
        for(GPSkSegment seg : list){
            if(maxPoints < seg.size()){
                maxPoints = seg.size();
            }
        }
        
        return maxPoints;
    }
    
    public static List<GPSkPoint> extractAllPoint(List<GPSkSegment> segments){
    	List<GPSkPoint> pointList = new LinkedList<>();
    	for(GPSkSegment seg : segments){
    		for(GPSkPoint p : seg){
    			pointList.add(p);
    		}
    	}
    	return pointList;
    }
}
