/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.utils;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import static de.fub.agg2graph.gpsk.utils.UtilConstants.*;

import java.util.Arrays;
import java.util.List;
/**
 *
 * @author doggy
 */
public class PointCollection {
    
    
    public static double[] getBounds(List<GPSkPoint> list){
        double[] result = new double[4];
        if(list == null){
        	Arrays.fill(result, Double.NaN);
        	return result;
        }
        if(list.isEmpty()){
        	Arrays.fill(result, Double.NaN);
        	return result;
        }
        
        result[SOUTHWEST_LAT] = Double.MAX_VALUE;
        result[SOUTHWEST_LON] = Double.MAX_VALUE;
        
        result[NORTHEAST_LAT] = Double.MIN_VALUE;
        result[NORTHEAST_LON] = Double.MIN_VALUE;
        
        for(GPSkPoint p : list){
            double lat = p.getLat();
            double lon = p.getLon();
            
            if(lat < result[SOUTHWEST_LAT]){
                result[SOUTHWEST_LAT] = lat;
            }
            if(lon < result[SOUTHWEST_LON]){
                result[SOUTHWEST_LON] = lon;
            }
            
            if(lat > result[NORTHEAST_LAT]){
                result[NORTHEAST_LAT] = lat;
            }
            if(lat > result[NORTHEAST_LON]){
                result[NORTHEAST_LON] = lon;
            }
        }
        return result;
        
    }
    
    public static boolean insideBounds(List<GPSkPoint> points, double[] bounds){
    	for(GPSkPoint p : points){
    		if(!PointUtils.insideBounds(p, bounds)){
    			return false;
    		}
    	}
    	return true;
    }
    
    
}
