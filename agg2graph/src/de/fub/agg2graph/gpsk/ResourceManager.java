/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

/**
 *
 * While developing, debugging and testing, all java classes are only compiled
 * but not packed into classes. When it is released, it will be a single jar, 
 * that contains all resources.
 * This class abstracts from this issue. It detects, if it is running in an IDE 
 * environment or as a jar.
 * All requests for resource files can be unified by this class.
 * @author Christian Windolf
 */
public class ResourceManager {
    private static Logger log = Logger.getLogger(ResourceManager.class);
    
    public static final File localFolder;
    static{
        
        String os = System.getProperty("os.name");
        File gpskFolder;
        if(os.toLowerCase().contains("windows")){
            String appdata = System.getenv("APPDATA");
            gpskFolder = new File(appdata, "gpsk");
            if(!gpskFolder.exists()){
                gpskFolder.mkdir();
                
            }
            
        } else { //so we're on a cool unixoide os
            String home = System.getProperty("user.home");
            gpskFolder = new File(home, ".gpsk");
            if(!gpskFolder.exists()){
                gpskFolder.mkdir();
            }
        }
        localFolder = gpskFolder;
    }
    
    private static enum ENV{
        JAR, FILESYSTEM;
    }
    
    private static ENV env = ENV.FILESYSTEM;
    /**
     * checks, that there may be all needed resources.
     * this does not garantee, that there will be all resources!
     * @return true, if everything seems OK. (only false can garantee, that 
     * nothings all right, true just says 'maybe')
     */
    public static boolean envOK(){
        URL url = ResourceManager.class.getResource("/");
        if(url != null){
            env = ENV.FILESYSTEM;
            File f = new File("res");
            if(f.exists() && f.isDirectory()){
                return true;
            } else {
                log.fatal("The directory " + f.getAbsolutePath() + 
                        " does not exist!");
                return false;
            }
        } else{
            env = ENV.JAR;
            return true;
        }
        
        
    }
    
    /**
     * independent from the development or productive environment, this method
     * returns the input stream of a resource.
     * @param path for example: "/icons/refresh.png"
     * @return null, if something went. It does not throw an exception. It just 
     * gives you warning via log4j!
     */
    public static InputStream getInputStream(String path){
        if(env == ENV.JAR){
            InputStream stream = ResourceManager.class.getResourceAsStream(path);
            if(stream == null){
                log.warn("The path " + path + " does not exist in the jar file");
            }
            return stream;
        } else {
            File f = new File("res");
            File f2 = new File(f.getAbsolutePath() + path);
            try {
                InputStream stream = new FileInputStream(f2);
                return stream;
            } catch (FileNotFoundException ex) {
                log.warn("The file " + f2.getAbsolutePath() + " does not exist");
                return null;
            }
        }
    }
    
    /**
     * return the URL of an resource object, indepenent from a productive 
     * environment (runnable jar) or development environment
     * @param path
     * @return null, if something went wrong. No exception will be thrown.
     * Warning or error via log4j instead.
     */
    public static URL getURL(String path){
        if(env == ENV.JAR){
            URL url = ResourceManager.class.getResource(path);
            if(url == null){
                log.warn("The path " + path + " does not exist in the jar file");
            }
            return url;
        } else {
            File f = new File("res");
            File f2 = new File(f.getAbsolutePath() + path);
            try {
                URL url = f2.toURI().toURL();
                return url;
            } catch (MalformedURLException ex) {
                log.error("something went wront while trying to extract the url", ex);
                return null;
            }
            
        }
    }
    
    public static File getLocalFolder(){
        return localFolder;
    }
    
}
