/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.io;

import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.io.Converter;
import de.fub.agg2graph.schemas.gpsk.KeyValue;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpsk.ObjectFactory;
import de.fub.agg2graph.schemas.gpsk.SetMeta;
import de.fub.agg2graph.schemas.gpx.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.*;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.boundary.Point;
import de.fub.agg2graph.gpsk.boundary.Segment;
import de.fub.agg2graph.gpsk.data.Filter;
import de.fub.agg2graph.gpsk.algorithms.optics.OPTICSPoint;
import de.fub.agg2graph.schemas.gpsk.PointMeta;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class EnvironmentImport implements Converter<GPSkEnvironment> {

    private double minLat = Double.NaN;
    private double minLon = Double.NaN;
    private double maxLat = Double.NaN;
    private double maxLon = Double.NaN;
    private EnvironmentBuilder builder = new EnvironmentBuilder();

    public EnvironmentImport() {
    }

    public EnvironmentImport(EnvironmentBuilder builder) {
        this.builder = builder;
    }

    @Override
    public GPSkEnvironment convert(GpxType input) {
        boolean bounds = false;

        MetadataType meta = input.getMetadata();
        if (meta != null) {
            bounds = processBounds(meta.getBounds());
            
            if (meta.getName() != null) {
                builder.setTitle(meta.getName());
            }
            processExtensions(meta.getExtensions());
        }
        List<WptType> wayPointList = input.getWpt();
        List<GPSkPoint> pointList = buildPoints(wayPointList);
        if (pointList != null) {
            builder.setList(pointList);
            builder.setFocus(FOCUS_ON_POINTS);
        } else {
            List<GPSkSegment> segments = buildSegmentsFromRoutes(input.getRte());
            segments.addAll(buildSegmentsFromTracks(input.getTrk()));
            builder.setList(segments);
            builder.setFocus(FOCUS_ON_SEGMENTS);
        }
        if(!bounds){
            builder.detectBounds();
        }
        builder.center();
        return builder.build();


    }

    /**
     *
     * @param points
     * @return null, if nothing interesting was found
     */
    private List<GPSkPoint> buildPoints(List<WptType> points) {
        if (points == null) {
            return null;
        }
        if (points.isEmpty()) {
            return null;
        }
        List<GPSkPoint> pointList = new LinkedList<>();
        for (WptType waypoint : points) {
            pointList.add(Point.fromGPX(waypoint));
        }
        return pointList;
    }

    /**
     *
     * @return an empty list, if nothing interesting was found
     */
    private List<GPSkSegment> buildSegmentsFromRoutes(List<RteType> routes) {
        List<GPSkSegment> list = new LinkedList<>();
        if (routes == null) {
            return list;
        }
        if (routes.isEmpty()) {
            return list;
        }
        for (RteType route : routes) {
            if (route != null) {
                GPSkSegment seg = Segment.fromGPX(route);
                list.add(seg);
            }
        }
        return list;
    }

    /**
     *
     * @param tracks
     * @return empty list, if nothing interesting was found
     */
    private List<GPSkSegment> buildSegmentsFromTracks(List<TrkType> tracks) {
        List<GPSkSegment> list = new LinkedList<>();
        if (tracks == null) {
            return list;
        }
        if (tracks.isEmpty()) {
            return list;
        }
        for (TrkType track : tracks) {
            list.addAll(Segment.fromGPX(track));
        }
        return list;
    }

   

    @Override
    public Class[] getClasses() {
        return new Class[]{ObjectFactory.class};
    }

    private void processExtensions(ExtensionsType extension) {
        if (extension == null) {
            return;
        }
        List<Object> any = extension.getAny();
        for (Object o : any) {
            if (o != null && o.getClass() == SetMeta.class) {
                SetMeta meta = (SetMeta) o;
                builder.keyValue = processKeyValue(meta);

            }
        }

    }

    private HashMap<String, String> processKeyValue(SetMeta meta) {
        List<KeyValue> list = meta.getKeyValue();
        if (list == null) {
            return new HashMap<>();
        }
        HashMap<String, String> map = new HashMap<>();
        for (KeyValue entry : list) {
            if (entry.getKey() != null && entry.getValue() != null) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map;
    }

   

    private boolean processBounds(BoundsType bounds) {
        if (bounds == null) {
            return false;
        }
        minLat = bounds.getMinlat().doubleValue();
        minLon = bounds.getMinlon().doubleValue();
        maxLat = bounds.getMaxlat().doubleValue();
        maxLon = bounds.getMaxlon().doubleValue();
        builder.setBounds(
                minLat,
                minLon,
                maxLat,
                maxLon);
        return true;
    }
}
