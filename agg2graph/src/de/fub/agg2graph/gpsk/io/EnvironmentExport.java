/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.io;



import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.boundary.GpxFactory;
import de.fub.agg2graph.gpsk.algorithms.optics.OPTICSPoint;
import de.fub.agg2graph.schemas.gpsk.KeyValue;
import de.fub.agg2graph.schemas.gpsk.PointMeta;
import de.fub.agg2graph.schemas.gpsk.SetMeta;
import de.fub.agg2graph.schemas.gpx.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class EnvironmentExport {
    
    private static Logger log = Logger.getLogger(EnvironmentExport.class);
    
    
    public static GpxType convertToGpx(GPSkEnvironment env){
        GpxType output = GpxFactory.createGpx();
        if(!env.hasBounds()){
            env.detectBounds();
        }
        MetadataType metadata = new MetadataType();
        metadata.setName(env.getMeta().name);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new Date());
        try {
            XMLGregorianCalendar xmlGC = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            metadata.setTime(xmlGC);
        } catch (DatatypeConfigurationException ex) {
            log.warn("unable to create xml date", ex);
        }
        BoundsType bounds = new BoundsType();
        if(env.hasBounds()){
            double[] b = env.getBounds();
            bounds.setMinlat(new BigDecimal(b[0]));
            bounds.setMinlon(new BigDecimal(b[1]));
            bounds.setMaxlat(new BigDecimal(b[2]));
            bounds.setMaxlon(new BigDecimal(b[3]));
            metadata.setBounds(bounds);
        }
        
        if(env.getSegments().isEmpty()){
            processPoints(output.getWpt(), env.getPoints());
        } else {
            processSegments(output.getRte(), env.getSegments());
        }
        Map<String, String> map = env.getMeta().keyValue;
        ExtensionsType et = new ExtensionsType();
        SetMeta sm = new SetMeta();
        List<KeyValue> kvList = sm.getKeyValue();
        for(Entry<String, String> e : map.entrySet()){
            KeyValue kv = new KeyValue();
            kv.setKey(e.getKey());
            kv.setValue(e.getValue());
            kvList.add(kv);
        }
        et.getAny().add(sm);
        return output;
        
        
    }
    
    private static void processPoints(List<WptType> waypointList, 
            List<GPSkPoint> pointList){
        for(GPSkPoint p : pointList){
            WptType waypoint = new WptType();
            waypoint.setLat(new BigDecimal(p.getLat()));
            waypoint.setLon(new BigDecimal(p.getLon()));
            if(p instanceof OPTICSPoint){
                ExtensionsType et = new ExtensionsType();
                PointMeta pm = new PointMeta();
                OPTICSPoint op = (OPTICSPoint) p;
                pm.setReachabilityDistance(op.getReachabilityDistance());
                et.getAny().add(pm);
                waypoint.setExtensions(et);
            }
            waypointList.add(waypoint);
        }
    }
    
    private static void processSegments(List<RteType> routeList, 
            List<GPSkSegment> segmentList){
        for(GPSkSegment seg : segmentList){
            RteType route = new RteType();
            processPoints(route.getRtept(), seg);
            routeList.add(route);
        }
    }
}
