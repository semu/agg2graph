/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.obs;

import de.fub.agg2graph.structs.GPSSegment;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Christian Windolf
 */
public class SplittedSegments extends Observable{
    
    private static final SplittedSegments singleton = new SplittedSegments();
    
    private List<GPSSegment> segments;
    
    public static SplittedSegments getInstance(){
        return singleton;
    }
    
    public SplittedSegments(){
        super();
        segments = null;
    }
    
    public void setSegments(List<GPSSegment> list){
        this.segments = list;
        setChanged();
        notifyObservers(segments);
    }
    
    public List<GPSSegment> getSegments(){
        return segments;
    }

}
