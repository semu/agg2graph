/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.obs;

import de.fub.agg2graph.gpsk.beans.ClusterCollection;
import de.fub.agg2graph.gpsk.beans.SegmentCluster;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class ClusterResults extends Observable {

    private static final ClusterResults singleton = new ClusterResults();
    private final HashMap<String, ClusterCollection> map =
            new HashMap<String, ClusterCollection>();

    public static ClusterResults getInstance() {
        return singleton;
    }

    public void putCollection(String key, ClusterCollection cc) {
        synchronized (this) {
            map.put(key, cc);
        }
        setChanged();
        notifyObservers(key);
    }

    public boolean removeCollection(String key) {
        if (map.containsKey(key)) {
            synchronized (this) {
                map.remove(key);
            }
            return true;
        } else {
            return false;
        }
    }

    public ClusterCollection getCollection(String key) {
        synchronized (this) {
            return map.get(key);
        }
    }
}
