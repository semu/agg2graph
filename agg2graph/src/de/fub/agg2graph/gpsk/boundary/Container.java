/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.boundary;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.jxpath.JXPathContext;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.RteType;
import de.fub.agg2graph.schemas.gpx.WptType;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class Container {
    public static List<GPSkPoint> extractPoints(GpxType gpx){
    	JXPathContext context = JXPathContext.newContext(gpx);
    	Iterator<WptType> iter = context.iterate("//wpt");
    	List<GPSkPoint> list = new LinkedList<>();
    	while(iter.hasNext()){
    		list.add(Point.fromGPX(iter.next()));
    	}
    	return list;
    }
    
    public static List<GPSkSegment> extractSegments(GpxType gpx){
    	JXPathContext context = JXPathContext.newContext(gpx);
    	Iterator<RteType> routeIterator = context.iterate("//rte");
    	List<GPSkSegment> list = new LinkedList<>();
    	while(routeIterator.hasNext()){
    		list.add(Segment.fromGPX(routeIterator.next()));
    	}
    	Iterator<RteType> trackIterator = context.iterate("//trkseg");
    	while(trackIterator.hasNext()){
    		list.add(Segment.fromGPX(trackIterator.next()));
    	}
    	return list;
    }
}
