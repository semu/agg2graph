/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.boundary;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.schemas.gpx.RteType;
import de.fub.agg2graph.schemas.gpx.TrkType;
import de.fub.agg2graph.schemas.gpx.TrksegType;
import de.fub.agg2graph.schemas.gpx.WptType;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class Segment {
    public static GPSkSegment fromGPX(RteType route){
        GPSkSegment seg = new GPSkSegment();
        for(WptType waypoint : route.getRtept()){
            seg.add(Point.fromGPX(waypoint));
        }
        return seg;
    }
    
    public static RteType toGPX(GPSkSegment seg){
        RteType rte = new RteType();
        List<WptType> list = rte.getRtept();
        for(GPSkPoint p : seg){
            list.add(Point.toGPX(p));
        }
        return rte;
    }
    
    public static List<GPSkSegment> fromGPX(TrkType track){
        List<GPSkSegment> segments = new LinkedList<>();
        List<TrksegType> trackSegments = track.getTrkseg();
        for(TrksegType s : trackSegments){
            GPSkSegment new_seg = new GPSkSegment();
            for(WptType waypoint : s.getTrkpt()){
                new_seg.add(Point.fromGPX(waypoint));
            }
            segments.add(new_seg);
        }
        return segments;
    }
    
    public static List<GPSkSegment> extractSegments(List<RteType> routes){
        List<GPSkSegment> segments = new LinkedList<>();
        for(RteType route : routes){
            segments.add(fromGPX(route));
        }
        return segments;
    }
    
    
}
