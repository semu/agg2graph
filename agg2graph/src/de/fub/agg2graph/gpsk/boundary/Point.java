/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.boundary;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.algorithms.optics.OPTICSPoint;
import de.fub.agg2graph.schemas.gpsk.PointMeta;
import de.fub.agg2graph.schemas.gpx.ExtensionsType;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.WptType;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class Point {
    public static WptType toGPX(GPSkPoint p){
        WptType waypoint = new WptType();
        waypoint.setLat(new BigDecimal(p.getLat()));
        waypoint.setLon(new BigDecimal(p.getLon()));
        if(p.getID() != null){
            waypoint.setName(p.getID());
        }
        if(p instanceof OPTICSPoint){
            List<Object> any = waypoint.getExtensions().getAny();
            PointMeta pm = new PointMeta();
            pm.setReachabilityDistance(((OPTICSPoint)p).getReachabilityDistance());
        }
        return waypoint;
    }
    
    public static GPSkPoint fromGPX(WptType wp){
        GPSkPoint p;
        if(wp.getName() != null && !wp.getName().trim().equals("")){
            p = new GPSkPoint(wp.getName(), wp.getLat().doubleValue(), wp.getLon().doubleValue());
        } else{
            p = new GPSkPoint(wp.getLat().doubleValue(), wp.getLon().doubleValue());
        }
        
        ExtensionsType et = wp.getExtensions();
        if(et != null){
            List<Object> any = et.getAny();
            for(Object o : any){
                if(o instanceof PointMeta){
                    PointMeta pm = (PointMeta) o;
                    OPTICSPoint op = new OPTICSPoint(p);
                    op.setReachabilityDistance(pm.getReachabilityDistance());
                    return op;
                }
            }
        }
        return p;
    }
    
    public static List<GPSkPoint> extractPoints(List<WptType> waypoints){
        List<GPSkPoint> pointList = new LinkedList<>();
        for(WptType waypoint : waypoints){
            pointList.add(fromGPX(waypoint));
        }
        
        return pointList;
    }
    
    public static List<GPSkPoint> extractPoints(GpxType gpx){
        return extractPoints(gpx.getWpt());
    }
}
