/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.boundary;

import java.util.List;

import de.fub.agg2graph.gpsk.beans.DataSet;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.beans.History;
import de.fub.agg2graph.gpsk.beans.ImmutablePointContainer;
import de.fub.agg2graph.gpsk.beans.ImmutableSegmentContainer;
import de.fub.agg2graph.gpsk.beans.PointContainer;
import de.fub.agg2graph.gpsk.io.SourceFile;
import de.fub.agg2graph.io.FileImport;
import de.fub.agg2graph.schemas.gpx.GpxType;

/**
 * <h3>When do I use this class? </h3>
 * <p>Suppose, you parsed in a gpx file via JAXB
 * ({@link FileImport}) 
 *  and received a {@link GpxType} object.</p> 
 * <p>This application mainly uses {@link DataSet} objects for it's algorithms,
 * how do I convert it into a {@link DataSet}?</p>
 * <p>Answer is simple: you are going to use this factory.
 * It looks into the {@link GpxType} object and tries to find a proper DataSet implementation to represent it.</p>
 * @author Christian Windolf
 *
 */
public class DataSetFactory {
	private boolean editable;
	/**
	 * Default contructor.
	 * Equivalent to {@link DataSetFactory#DataSetFactory(boolean) DataSetFactory(false)}.
	 */
	public DataSetFactory() {
		this(false);
	}
	
	/**
	 * contructor with the editable property. Take a look at {@link DataSetFactory#isEditable()} to see, what it does.
	 * @param editable
	 */
	public DataSetFactory(boolean editable){
		this.editable = editable;
		
	}

	/**
	 * The editable param defines, if this factory should try to find a DataSet, that can 
	 * represents editable or immutable data
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}
	
	/**
	 * create a new DataSet.
	 * It just looks, wich dataset should fit.
	 * The result of this method depends on the {@link DataSetFactory#setEditable(boolean) editable} property.
	 * @param gpx the gpxType object
	 * @param sourceInformation this parameter is added to the resulting DataSet. 
	 * The {@link DataSet}-interface demands, that there is a {@link History} object,
	 * that tells something about it's source. See also {@link DataSet#getHistory()} and
	 * {@link DataSetFactory#createDataSet(GpxType, Object) the other method} to see, how the history object
	 * should look like.
	 * @return a new DataSet
	 * @throws UnsupportedOperationException when you try to create a DataSet, that cannot be represented
	 * by a DataSet. For example there is no implementation for datasets with points and segments as well.
	 * Also an editable version of {@link ImmutableSegmentContainer} isn't implemented yet, so it also throws
	 * an exception, if put in a Gpx file like that.<br />
	 * Also empty datasets are not supported
	 */
	public DataSet createDataSet(GpxType gpx, History history){
		List<GPSkPoint> points = Container.extractPoints(gpx);
		if(!points.isEmpty()){
			if(!editable){
				return new ImmutablePointContainer(points, history);
			} else {
				return new PointContainer(points, history);
			}
		}
		List<GPSkSegment> segments = Container.extractSegments(gpx);
		if(!segments.isEmpty()){
			if(!editable){
				return new ImmutableSegmentContainer(segments, history);
			} else {
				throw new UnsupportedOperationException("we don't support editable segment containers :(");
			}
		}
		throw new UnsupportedOperationException("we don't support empty datasets :(");
	}
	
	/**
	 * See {@link DataSetFactory#createDataSet(GpxType, History)
	 * @param gpx
	 * @param sourceInformation the sourceInformation object should have getter-methods for its properties
	 * to allow introspection.
	 * The simplest example is to use an instance of {@link SourceFile} to provide information
	 * about where you got the GpxType from.
	 * @return
	 */
	public DataSet createDataSet(GpxType gpx, Object sourceInformation){
		return createDataSet(gpx, new History(sourceInformation));
	}

	/**
	 * @param editable the editable to set
	 * @see DataSetFactory#isEditable()
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	
	

}
