/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.eval;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.gui.LayerManager;
import de.fub.agg2graph.gpsk.gui.MapPanel;
import de.fub.agg2graph.gpsk.gui.TrackLayer;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

import static java.awt.event.ActionEvent.CTRL_MASK;
import java.awt.event.ActionListener;
import static java.awt.event.KeyEvent.VK_X;
import static java.awt.event.KeyEvent.VK_D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javax.swing.*;
import static javax.swing.KeyStroke.getKeyStroke;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.TileFactory;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_SEGMENTS;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class TrackCollection extends AbstractAction {

    private static Logger log = Logger.getLogger(TrackCollection.class);
    private static int ACTIVE = 1, INACTIVE = 0;
    private int status = INACTIVE;
    private List<GPSkSegment> segments = new LinkedList<>();
    private TrackLayer tl;
    private Switch s = new Switch();
    private EnvCreator envCreator = new EnvCreator();

    public TrackCollection() {
        super("create tracks (on)");
        putValue(ACCELERATOR_KEY, getKeyStroke(VK_X, CTRL_MASK));
        tl = new TrackLayer("marked tracks");
        LayerManager.getInstance().addLayer(tl);

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (status == INACTIVE) {
            ActionMap am = Main.mainWindow.getMapPanel().getActionMap();

            am.put("switch", s);
            InputMap im = Main.mainWindow.getMapPanel().getInputMap();
            im.put(getKeyStroke(VK_D, CTRL_MASK), "switch");
            
            
            status = ACTIVE;
            s.setEnabled(true);
            log.debug("turning trackcollector on");

        } else {

            //trim segments
            if (segments != null) {
                if (!segments.isEmpty()) {
                    ListIterator<GPSkSegment> it = segments.listIterator();
                    while (it.hasNext()) {
                        GPSkSegment s = it.next();
                        if (s.size() <= 1) {
                            it.remove();
                        }
                    }
                }
                envCreator.setVisible(true);
            } else{
                status = INACTIVE;
                s.setEnabled(false);
            }
        }

    }

    private class Switch extends AbstractAction {

        private int status = INACTIVE;
        private Handler handler;

        @Override
        public void actionPerformed(ActionEvent ae) {

            if (status == ACTIVE) {
                if (handler != null) {
                    JXMapViewer jmv = (JXMapViewer) ae.getSource();
                    jmv.removeMouseListener(handler);

                    handler = null;
                    status = INACTIVE;
                    log.debug("turning mouse listener off");
                }
            } else {
                JXMapViewer jmv = (JXMapViewer) ae.getSource();
                handler = new Handler();

                segments.add(handler.seg);

                jmv.addMouseListener(handler);
                status = ACTIVE;
                log.debug("turning mouse listener off");
            }
        }
    }

    private class Handler implements MouseListener {

        private final GPSkSegment seg = new GPSkSegment();

        @Override
        public void mouseClicked(MouseEvent me) {

            JXMapViewer jmv = (JXMapViewer) me.getSource();
            TileFactory tf = jmv.getTileFactory();
            GeoPosition gp = jmv.convertPointToGeoPosition(me.getPoint());


            seg.add(new GPSkPoint(gp.getLatitude(), gp.getLongitude()));
            tl.setData(segments);
            jmv.repaint();

        }

        @Override
        public void mousePressed(MouseEvent me) {
            //NOP
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            //NOP
        }

        @Override
        public void mouseEntered(MouseEvent me) {
            //NOP
        }

        @Override
        public void mouseExited(MouseEvent me) {
            //NOP
        }
    }

    private class EnvCreator extends JDialog {

        private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyy 'at' kk:mm:ss");
        private final JLabel keyLabel = new JLabel("key");
        private final JTextField keyField = new JTextField("customSet");
        private final JButton abortButton = new JButton("ABORT");
        private final JButton okButton = new JButton("OK");

        private EnvCreator() {
            super(Main.mainWindow, "put it into environment");
            setLayout(new GridLayout(2, 2));
            add(keyLabel);
            add(keyField);
            abortButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    setVisible(false);
                    status = ACTIVE;
                }
            });

            okButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    setVisible(false);
                    status = INACTIVE;
                    List<GPSSegment> sl = new LinkedList<>();
                    
                    segments = null;
                    EnvironmentBuilder builder = new EnvironmentBuilder(segments);
                    builder.setTitle("custom tracjs").setFocus(FOCUS_ON_SEGMENTS);
                    builder.keyValue.put("time", format.format(new Date()));
                    
                    EnvironmentManager.getInstance().put(keyField.getText().trim(), 
                            builder.build());
                    if (tl != null) {
                        LayerManager.getInstance().removeLayer(tl);
                    }

                    status = INACTIVE;
                    s.setEnabled(false);
                    log.debug("turning trackcollector off");
                }
            });
            add(abortButton);
            add(okButton);

        }
    }
}
