/*
 * Copyright (C) 2014 Christian Windolf, Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk;

import static java.io.File.separator;

import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Observer;
import java.util.Set;

import org.apache.log4j.Logger;

import de.fub.agg2graph.gpsk.annotations.Prefix;

/**
 * <p>A global persistent key-value-store for options...</p>
 *
 * <p>It stores all keyValues in a file '~/.gpsk/kvstore.hsh'</p>
 *
 * @author Christian Windolf
 */
public class KeyValueStore {

    private static Logger log = Logger.getLogger(KeyValueStore.class);
    private static HashMap<String, String> store;
    private static String path;
    
    private static HashMap<Class, List<Observer>> listenerMap = new HashMap<>();
    
    /**
     * interested objects can listen to this objects. If something changes, the keyValue-store will pronounce these changes
     * through this object.
     */
    public static PropertyChangeSupport pChange = new PropertyChangeSupport(new Object());

    /**
     * <p>initializes the key value store.</p> <p> If the file for the key-value
     * store is available, it loads it, otherwise it just creates a new HashMap
     * for the store</p> <p> If it is running on a Windows machine, it loads the
     * data from %APPDATA%\gpsk\kvstore.hsh <br /> On a unixoide machine it
     * loads it from ~/.gpsk/kvstore.hsh
     *
     */
    static {
        File gpskFolder = ResourceManager.localFolder;

        File kvStore = new File(gpskFolder.getAbsolutePath() + separator + "kvstore.hsh");
        path = kvStore.getAbsolutePath();


        loadStore();
        Runtime.getRuntime().addShutdownHook(new Thread(new Savior()));

    }

    private static void loadStore() {
        File f = new File(path);
        store = new HashMap<String, String>();

        if (f.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(f));

                String line = br.readLine();
                while (line != null) {
                    if (line.startsWith("#")) {
                        //ignore
                    } else {
                        String[] kv = line.split(":");
                        if (kv.length != 2) {
                            log.warn("File " + f + " seems to be corrupted. Maybe you should delete this file");
                        } else {
                            store.put(kv[0], kv[1]);
                        }

                    }
                    line = br.readLine();
                }
                br.close();
            } catch (FileNotFoundException ex) {
                log.warn("File " + f + " not found. Some preferences won't be stored persistent.");
            } catch (IOException ex) {
                log.warn("Unable to read file " + f + " but the program will still run. It just won't save some preferences persistent");

            }
        }
    }

    /**
     * returns the value for a given key. If no entry is found, the kv-pair
     * <key,value> will be added to the store. Then def will be returned
     *
     * @param key the key
     * @param def the default value for this key.
     * @return if store has a value for the key, the value will be returned.
     * Otherwise, the input param def will be returned.
     */
    public static String getValue(String key, String def) {
        String result = store.get(key);
        if (result != null) {
            return result;
        }
        store.put(key, def);
        return def;
    }

    /**
     * retrieved the value for a key from the store.
     *
     * @return null, if no proper value is found.
     */
    public static String getValue(String key) {
        return store.get(key);
    }

    /**
     * loads a boolean value from the keyvalue store. Note: All values are
     * stored as Strings, so if there is an invalid String, it just returns
     * false.
     *
     * @param key
     * @return false, if the String does not say "true"
     */
    public static boolean getBoolean(String key) {
        return Boolean.parseBoolean(store.get(key));
    }

    /**
     * loads a boolean value from the key value store. Note: All values are
     * stored as Strings, so if there is an invalid String, it just returns
     * false.
     *
     * @param key
     * @param def A default value, if there is no data for this key.
     * @return false, if the String does not say "true"
     */
    public static boolean getBoolean(String key, boolean def) {
        String result = store.get(key);
        if (result != null) {
            return Boolean.parseBoolean(result);
        }
        store.put(key, String.valueOf(def));
        return def;
    }

    /**
     * returns the double value for this key. This method saves you typing
     * <code>Double.parseDouble(String x)</code> but does not handle a
     * NumberFormatException.
     *
     * @param key
     * @return The parsed double value.
     * @throws NumberFormatException if the value is not a number
     * @throws NullPointerException if the key does not exist
     */
    public static double getDouble(String key) {
        return Double.parseDouble(key);
    }

    /**
     * Like {@link KeyValueStore#getDouble(String)}, but if the key is not present, it
     * creates a new entry with the given default value and returns it
     *
     * @param key A key for the value
     * @param def The default value
     * @return the parsed double value or, if for this value was nothing found, the default value is returned and stored
     * @throws NumberFormatException if the value is not a number
     */
    public static double getDouble(String key, double def) {
        String result = store.get(key);
        if (result != null) {
            return Double.parseDouble(result);
        }
        store.put(key, String.valueOf(def));
        return def;
    }

    /**
     * Retrieves the entry for the key and parses it to integer.
     *
     * @param key
     * @return the parsed value
     * @throws NumberFormatException if the value was not a number
     * @throws NullPointerException if this key has no entry
     */
    public static int getInt(String key) {
        return Integer.parseInt(store.get(key));
    }

    /**
     * Like {@link KeyValueStore#getInt(String)}, but you can also define a default value,
     * if there is no value for that key.
     *
     * @param key the key.
     * @param def the default value, that will be returned and stored, if there
     * is just a null value for this key.
     * @return the value, if present or the default value.
     * @throws NumberFormatException if for this key there is no valid String
     * (NaN)
     */
    public static int getInt(String key, int def) {
        String result = store.get(key);
        if (result != null) {
            return Integer.parseInt(result);
        }
        store.put(key, String.valueOf(def));
        return def;
    }

    /**
     * Just like {@link KeyValueStore#getInt(String, int)}, but for long-values
     *
     * @param key
     * @param def
     * @return
     * @throws NumberFormatException
     */
    public static long getLong(String key, long def) {
        String result = store.get(key);
        if (result != null) {
            return Long.parseLong(result);
        }
        store.put(key, String.valueOf(def));
        return def;
    }

    /**
     * puts a new item into the store.
     *
     * @param key A key for the value. If there is already an entry for this
     * key, it will be overwritten
     * @param value the actual value.
     */
    public static void putValue(String key, String value) {

        store.put(key, value);
    }

    /**
     * see {@link KeyValueStore#putValue(String, String) putString()}. Takes care of converting
     * it to String.
     *
     * @param key
     * @param value
     */
    public static void putValue(String key, boolean value) {
        store.put(key, String.valueOf(value));
    }

    /**
     * see {@link #putValue(String, String) putValue()}. Just takes also care of
     * converting the double to String
     *
     * @param key
     * @param value
     */
    public static void putValue(String key, double value) {
        store.put(key, String.valueOf(value));
    }

    /**
     * see {@link #putValue(String, String) putValue()}. Just takes also care of
     * converting the int to String
     *
     * @param key
     * @param value
     */
    public static void putValue(String key, int value) {
        store.put(key, String.valueOf(value));
    }

    /**
     * see {@link #putValue(String, String) putValue()}. Just takes also care of
     * converting the long to String
     *
     * @param key
     * @param value
     */
    public static void putValue(String key, long value) {
        store.put(key, String.valueOf(value));
    }

    
    /**
     * This runnable class makes sure, that the data in the key value 
     * store will be stored in a file when the application is shut down
     */
    private static class Savior implements Runnable {

        private Logger log = Logger.getLogger(Savior.class);

        @Override
        public void run() {
            try {
                File f = new File(path);
                if (!f.exists()) {
                    f.createNewFile();
                }
                BufferedWriter out = new BufferedWriter(new FileWriter(f));
                out.write("#Key Value Store for gpsk Version " + Main.version + ".");
                out.newLine();
                Set<String> keySet = store.keySet();
                Iterator<String> keyIt = keySet.iterator();
                while (keyIt.hasNext()) {
                    String key = keyIt.next();
                    out.write(key);
                    out.write(":");
                    out.write(store.get(key));
                    out.newLine();
                }
                out.flush();
                out.close();
            } catch (IOException ex) {
                log.warn("Unable to write into file " + path, ex);

            }
        }
    }

    /**
     * This method creats a bean of the desired class.
     * It will use its {@link Prefix} annotation to determine, wich prefix its keys has.
     * After that it fills all public members with values from the store or the
     * default value in the
     * @param c the class, that has public primitive (or String) fields for its values
     * @return An instance of c. If there could be any values found for this class, those will be stored in the fields. Otherwise, it will initiated with default values
     * @throws Exception 
     */
    public static Object loadOptions(Class c) throws Exception {
        String prefix;
        try {
            Prefix p = (Prefix) c.getAnnotation(Prefix.class);

            prefix = p.value();

        } catch (NullPointerException | ClassCastException ex) {
            prefix = c.getSimpleName();
            ex.printStackTrace();//DEBUG
        }

        Object o = c.getConstructor().newInstance();
        for (Field f : c.getFields()) {
            loadField(prefix, f, o);
        }
        return o;






    }

    /**
     * A helping method for the {@link #loadOptions(Class)}-method.
     * It processes a single field of a class 
     * @param prefix
     * @param f
     * @param o
     * @throws Exception
     */
    private static void loadField(String prefix, Field f, Object o) throws Exception {
        Class type = f.getType();
        String key = prefix + '.' + f.getName();
        if (type.equals(Integer.TYPE)) {
            int x = f.getInt(o);
            x = getInt(key, x);
            f.setInt(o, x);
            return;
        }
        if (type.equals(Long.TYPE)) {
            long x = f.getLong(o);
            x = getLong(key, x);
            f.setLong(o, x);
            return;
        }
        if (type.equals(Double.TYPE)) {
            double x = f.getDouble(o);
            x = getDouble(key, x);
            f.setDouble(o, x);
            return;
        }
        if (type.equals(Boolean.TYPE)) {
            boolean x = f.getBoolean(o);
            x = getBoolean(key, x);
            f.setBoolean(o, x);
            return;
        }

        if (type.equals(String.class)) {
            String x = (String) f.get(o);
            x = getValue(key, x);
            f.set(o, x);
            return;
        }
    }

    /**
     * save the values of this object in the keyvalue store.
     * It can only process public and primitive values.
     * @param o
     * @throws Exception
     */
    public static void saveOptions(Object o) throws Exception {
        Object old = loadOptions(o.getClass());
        
        String prefix;
        try {
            Prefix p = (Prefix) o.getClass().getAnnotation(Prefix.class);
            prefix = p.value();

        } catch (Exception ex) {
            log.warn("I have to use the class name as prefix", ex);
            prefix = o.getClass().getSimpleName();
        }
        for (Field f : o.getClass().getFields()) {
            saveFields(prefix, f, o);
        }
        pChange.firePropertyChange(o.getClass().getName(), old, o);
        

    }

    private static void saveFields(String prefix, Field f, Object o) throws Exception {
        Class type = f.getType();
        String key = prefix + "." + f.getName();
        if (type.equals(Integer.TYPE)) {
            int x = f.getInt(o);
            putValue(key, x);
            return;
        }
        if (type.equals(Long.TYPE)) {
            long x = f.getLong(o);
            putValue(key, x);
            return;
        }
        if (type.equals(Double.TYPE)) {
            double x = f.getDouble(o);
            putValue(key, x);
            return;
        }
        if (type.equals(Boolean.TYPE)) {
            boolean x = f.getBoolean(o);
            putValue(key, x);
            return;
        }
        if (type.equals(String.class)) {
            String x = (String) f.get(o);
            putValue(key, x);
            return;
        }
        
    }
    
   
}
