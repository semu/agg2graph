/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import static de.fub.agg2graph.gpsk.beans.GPSkPoint.RADIUS;
import de.fub.agg2graph.structs.GPSPoint;

import static java.lang.Math.*;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class SimpleProjection extends Projection{

    public SimpleProjection(double minLat, double minLon, double maxLat, double maxLon) {
        super(minLat, minLon, maxLat, maxLon);
    }

    public SimpleProjection(GPSPoint southWest, GPSPoint northEast) {
        super(southWest, northEast);
    }

    public SimpleProjection(double[] bounds) {
        super(bounds);
    }

    
    @Override
    public void setProjection(GPSkPoint p) {
        double s = sin(toRadians(minLat));
        s *= s;
        double c = sin(toRadians(minLat));
        c *= c;
        double x = RADIUS * acos(s + (c * cos(toRadians(p.getLon()) - toRadians(minLon))));
        if(minLon > p.getLon()){
            p.setEast(-x);
        } else {
            p.setEast(x);
        }
        double y = RADIUS * (toRadians(p.getLat()) - toRadians(minLat));
        p.setNorth(y);
    }

    @Override
    public GPSPoint toLatLon(double x, double y) {
        double lat = x * 113100;
        double lon = cos(toRadians(minLat)) * x * 113100;
        return new GPSkPoint(lat, lon);
        
    }
    
}
