/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class FrechetFactory {
    private static final FrechetFactory singleton = new FrechetFactory();
    
    public static FrechetFactory getInstance(){
        return singleton;
    }
    
    private Class<? extends FrechetDistance> distanceClass = WindolfFrechet.class;
    
    
    public Class<? extends FrechetDistance> getDefaultClass(){
    	return distanceClass;
    }
    
    private FrechetFactory(){
        
    }
    
    public FrechetDistance createNewInstance(Class<? extends FrechetDistance> c) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        Constructor<? extends FrechetDistance> constructor = c.getConstructor();
        FrechetDistance fd = constructor.newInstance();
        return fd;
        
    }
    
    public FrechetDistance createNewInstance() {
        return new WindolfFrechet();
    }
    
    public FrechetDistance createNewInstance(
            Class<? extends FrechetDistance> c,
            double eps) 
            throws 
            NoSuchMethodException, 
            InstantiationException, 
            IllegalAccessException, 
            IllegalArgumentException, 
            InvocationTargetException{
        Constructor<? extends FrechetDistance> constructor = c.getConstructor(Double.TYPE);
        FrechetDistance fd = constructor.newInstance(eps);
        return fd;
        
    }
    
    public FrechetDistance createNewInstance(
            double eps) {
        return new WindolfFrechet(eps);
    }
    
    
}
