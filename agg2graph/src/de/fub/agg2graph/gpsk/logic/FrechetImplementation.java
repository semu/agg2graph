/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.logic;

/**
 *
 * @author doggy
 */
public enum FrechetImplementation {
    JTS(JTSDistance.class), 
    JENS(JensFrechet.class),
    HD(HausdorffDistance.class),
    WIN(WindolfFrechet.class);
    
    private FrechetImplementation(Class c){
        this.implementation = c;
    }
    
    private final Class implementation;
    public Class getImplementation(){
        return implementation;
    }
    
}
