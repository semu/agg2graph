/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import data.Edge;
import data.Location;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.util.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class JensFrechet extends FrechetDistance {

    private static Logger log = Logger.getLogger(JensFrechet.class);
    private final PointDistance distanceFunction;

    public JensFrechet() {
        super();
        PointDistance pd;
        try {
            pd = DistanceFactory.getInstance().createNewInstance();
        } catch (Exception e) {
            pd = new TrigonometricDistance();
            log.warn("unable to use the correct distance function. using the trigonmetric distance instead");
        }
        distanceFunction = pd;
    }
    
    public JensFrechet(double eps){
        super(eps);
        PointDistance pd = null;
        try{
            pd = DistanceFactory.getInstance().createNewInstance();
        } catch (Exception ex) {
            pd = new TrigonometricDistance();
            log.warn("unable to use the correct distance function. using the trigonmetric distance instead");
        } finally{
            distanceFunction = pd;
        }
        
    }

    @Override
    public boolean isInFrechetDistance(GPSkSegment seg1, GPSkSegment seg2, double eps) {
        List<Edge> list1 = new LinkedList<>();
        Iterator<GPSkPoint> it1 = seg1.iterator();
        GPSkPoint first = it1.next();
        while (it1.hasNext()) {
            GPSkPoint second = it1.next();
            Location l1 = new Location(first.getEast(), first.getNorth());
            Location l2 = new Location(second.getEast(), second.getNorth());
            Edge e = new Edge(l1, l2,
                    (float) distanceFunction.calculate(first, second));
            list1.add(e);
            first = second;
        }
        Vector<Edge> vector1 = new Vector<>(list1);

        List<Edge> list2 = new LinkedList<>();
        Iterator<GPSkPoint> it2 = seg2.iterator();
        first = it2.next();
        while (it2.hasNext()) {
            GPSkPoint second = it2.next();
            Location l1 = new Location(
                    first.getEast(),
                    first.getNorth());
            Location l2 = new Location(second.getEast(), second.getNorth());

            Edge e = new Edge(l1, l2, (float) distanceFunction.calculate(first, second));
            list2.add(e);
            first = second;
        }
        Vector<Edge> vector2 = new Vector<>(list2);

        data.distance.FrechetDistance fd = new data.distance.FrechetDistance(vector1, vector2, eps);
        double d = fd.computeEpsilon();
        return d <= eps;
    }
}
