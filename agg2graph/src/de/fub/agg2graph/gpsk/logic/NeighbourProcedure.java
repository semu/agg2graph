/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import gnu.trove.TIntProcedure;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public abstract class NeighbourProcedure<T> implements TIntProcedure{
    
    protected final T center;
    protected final List<T> results = new LinkedList<>();
    protected final double eps;
    protected final ArrayList<T> data;
    
    public NeighbourProcedure(T center, double eps, ArrayList<T> data){
        this.center = center;
        this.eps = eps;
        this.data = data;
    }
    
    public List<T> getResult(){
        return results;
    }
    
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append("{center=").append(center).append(";eps=").append(eps);
        builder.append(",datasetSize=").append(results.size()).append(';');
        builder.append("resultsSize=").append(results.size()).append('}');
        return builder.toString();
    }
}
