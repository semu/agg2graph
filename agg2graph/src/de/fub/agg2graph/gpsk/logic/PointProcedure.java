/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.log4j.Logger;


/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class PointProcedure extends NeighbourProcedure<GPSkPoint> {
    private static Logger log = Logger.getLogger(PointProcedure.class);
    private PointDistance pd;
    

    public PointProcedure(GPSkPoint center, double eps, ArrayList<GPSkPoint> data) {
        super(center, eps, data);
        
        try {
            pd = DistanceFactory.getInstance().createNewInstance();
        } catch (Exception ex) {
            log.warn("error occurred while creating a distance measurement object.");
            pd = new TrigonometricDistance();
            
        } 
    }

    
    @Override
    public boolean execute(int i) {
        GPSkPoint p = data.get(i);
        
        
        double d = pd.calculate(p, center);
        if(d <= eps && !p.equals(center)){
            results.add(p);
        }
        return true;
        
    }
    
}
