/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.structs.GPSPoint;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import org.jscience.geography.coordinates.LatLong;
import org.jscience.geography.coordinates.UTM;

import static org.jscience.geography.coordinates.UTM.*;
import static org.jscience.geography.coordinates.crs.ReferenceEllipsoid.WGS84;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class UTMProjection extends Projection{
    protected final char latitudeZone;
    protected final int longitudeZone;

    public UTMProjection(double minLat, double minLon, double maxLat, double maxLon) {
        super(minLat, minLon, maxLat, maxLon);
        LatLong southWest = LatLong.valueOf(minLat, minLon, NonSI.DEGREE_ANGLE);
        LatLong northEast = LatLong.valueOf(maxLat, maxLon, NonSI.DEGREE_ANGLE);
        if(isNorthPolar(southWest) || isNorthPolar(northEast)){
            throw new IllegalArgumentException("The latitude must be lower than 84°");
        } else if(isSouthPolar(southWest) || isSouthPolar(northEast)){
            throw new IllegalArgumentException("The UTM projection does not support latitudes below 80° south");
        }
        char tmpSouthLatZone = getLatitudeZone(southWest);
        char tmpNorthLatZone = getLatitudeZone(northEast);
        int tmpWestLonZone = getLongitudeZone(southWest);
        int tmpEastLonZone = getLongitudeZone(northEast);
        
        if(tmpSouthLatZone != tmpNorthLatZone){
            throw new IllegalArgumentException("sorry, but the designated area consists of more than two zones<" +
            		minLat + "," + minLon + "|" + maxLat + "," + maxLon + ">");
        }
        
        if(tmpWestLonZone != tmpEastLonZone){
            throw new IllegalArgumentException("sorry, but the designated bounds need more than one UTM zone <" 
        + minLat + "," + minLon + "|" + maxLat + "," + maxLon + ">");
        }
        
        latitudeZone = tmpSouthLatZone;
        longitudeZone = tmpEastLonZone;
    }

    public UTMProjection(GPSPoint southWest, GPSPoint northEast) {
        this(southWest.getLat(), southWest.getLon(), northEast.getLat(), northEast.getLon());
    }

    public UTMProjection(double[] bounds) {
        this(bounds[0], bounds[1], bounds[2], bounds[3]);
    }

    
    @Override
    public void setProjection(GPSkPoint p) {
        LatLong coordinates = LatLong.valueOf(p.getLat(), p.getLon(), NonSI.DEGREE_ANGLE);
        UTM utm = UTM.latLongToUtm(coordinates, WGS84);
        p.setEast(utm.eastingValue(SI.METER));
        p.setNorth(utm.northingValue(SI.METER));
        
    }

    @Override
    public GPSPoint toLatLon(double x, double y) {
        UTM utm = UTM.valueOf(longitudeZone, latitudeZone, x, y, SI.METER);
        LatLong latLong = UTM.utmToLatLong(utm, WGS84);
        GPSkPoint p = new GPSkPoint(latLong.latitudeValue(NonSI.DEGREE_ANGLE),
                latLong.longitudeValue(NonSI.DEGREE_ANGLE));
        p.setEast(x);
        p.setNorth(y);
        return p;
        
    }
    
    @Override
    public String toString(){
    	StringBuilder builder = new StringBuilder("UTM_Projection{");
    	builder.append("latZone=").append(latitudeZone).append(',');
    	builder.append("longitudeZone=").append(longitudeZone).append('}');
    	return builder.toString();
    }
    
}
