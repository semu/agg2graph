/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import java.util.ArrayList;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class PointProcedureFactory {
    
    private PointProcedureFactory() {
    }
    
    public static PointProcedureFactory getInstance() {
        return PointProcedureFactoryHolder.INSTANCE;
    }
    
    private static class PointProcedureFactoryHolder {

        private static final PointProcedureFactory INSTANCE = new PointProcedureFactory();
    }
    
    public NeighbourProcedure<GPSkPoint> createNewInstance(GPSkPoint center, double eps, ArrayList<GPSkPoint> data){
        return new PointProcedure(center, eps, data);
    }
}
