/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.utils.SegmentCollection;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author doggy
 */
public class NumberedAggregation extends ClusterAggregation {
    ProjectionFactory factory = ProjectionFactory.getInstance();
    PointDistance pd = DistanceFactory.getInstance().createNewInstance();

    public NumberedAggregation() {
        
    }
    

    private static Logger log = Logger.getLogger(NumberedAggregation.class);
    
    

    @Override
    public GPSkSegment aggregate(List<GPSkSegment> list) {
    	double[] bounds = SegmentCollection.getBounds(list);
        GPSkPoint first = list.get(0).getFirst();
        GPSkPoint last = list.get(0).getLast();
        ListIterator<GPSkSegment> li1 = list.listIterator();
        while(li1.hasNext()){
            GPSkSegment seg = li1.next();
            GPSkPoint tmpFirst = seg.getFirst();
            GPSkPoint tmpLast = seg.getLast();
            
            double d1 = pd.calculate(first, tmpFirst);
            double d2 = pd.calculate(first, tmpLast);
            if(d2 < d1){
                li1.set(seg.reverse());
            }
        }
        
        
        Projection projection = factory.createProjection(bounds);;
        
            
        ListIterator[] iterArray = new ListIterator[list.size()];
        ListIterator<GPSkSegment> mainIterator = list.listIterator();
        while(mainIterator.hasNext()){
        	int index = mainIterator.nextIndex();
        	GPSkSegment seg = mainIterator.next();
        	iterArray[index] = seg.listIterator();
        }
        int maxSize = SegmentCollection.getMaxPoints(list);
        GPSkSegment newSegment = new GPSkSegment();
        for(int i = 0; i < maxSize; i++){
            double east = 0;
            double north = 0;
            int counter = 0;
            for(ListIterator li : iterArray){
                if(li.hasNext()){
                    GPSkPoint p = (GPSkPoint) li.next();
                    east += p.getEast();
                    north += p.getNorth();
                    counter++;
                }
            }
            east /= (double) counter;
            north /= (double) counter;
            GPSkPoint point = (GPSkPoint) projection.toLatLon(east, north);
            newSegment.add(point);
            
        }
        
        return newSegment;
    }
    

}
