/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class DefaultSegmentProcedure extends NeighbourProcedure<GPSkSegment>{
    private static Logger log = Logger.getLogger(DefaultSegmentProcedure.class);
    
    private FrechetDistance fd;
    
    List<Future<GPSkSegment>> tmpResults = new LinkedList<>();

    public DefaultSegmentProcedure(GPSkSegment center, double eps, ArrayList<GPSkSegment> data) {
        super(center, eps, data);
        try{
            fd = FrechetFactory.getInstance().createNewInstance(eps);
        } catch(Exception ex){
            fd = new JensFrechet();
        }
    }

    
    
    @Override
    public boolean execute(int i) {
        GPSkSegment seg = data.get(i);
        
        if(seg.equals(center)){
            return true;
        }
        if(fd.isInFrechetDistance(seg, center)) {
        	results.add(seg);
        }
        return true;
    }

    
    
    
    

    
    
    
}
