/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.boundary.GpxFactory;
import de.fub.agg2graph.gpsk.boundary.Segment;
import de.fub.agg2graph.io.FileExport;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.MetadataType;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class ExceptionCatchingProcedure extends DefaultSegmentProcedure{
    private static Logger log = Logger.getLogger(ExceptionCatchingProcedure.class);

    public ExceptionCatchingProcedure(GPSkSegment center, double eps, ArrayList<GPSkSegment> data) {
        super(center, eps, data);
    }
    
    public boolean execute(int i){
        try{
            return super.execute(i);
        } catch(Exception ex){
            System.out.println("exception caught!");
            ex.printStackTrace();
            log.error("exception caught hile using frechet distance.", ex);
            return true;
        }
        
    }
    
    
}
