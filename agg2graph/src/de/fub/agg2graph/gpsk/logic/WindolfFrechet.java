/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.util.Arrays;
import java.util.ListIterator;

import net.windolf.frechet.DiscreteFrechet;
import net.windolf.frechet.Point;
import net.windolf.frechet.PolygonalLine;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class WindolfFrechet extends FrechetDistance {

    private static Logger log = Logger.getLogger(WindolfFrechet.class);
    DiscreteFrechet df = net.windolf.frechet.FrechetFactory.getInstance().getDiscreteFrechet();
    
    public WindolfFrechet() {
    }
    
    public WindolfFrechet(double eps) {
        super(eps);
    }
    
    @Override
    public boolean isInFrechetDistance(GPSkSegment seg1, GPSkSegment seg2, double eps) {
        Point[] array1 = new Point[seg1.size()];
        ListIterator<GPSkPoint> pointIterator1 = seg1.listIterator();
        while (pointIterator1.hasNext()) {
            int index = pointIterator1.nextIndex();
            GPSkPoint point = pointIterator1.next();
            
            array1[index] = new Point(point.getEast(), point.getNorth());
        }
        
        Point[] array2 = new Point[seg2.size()];
        ListIterator<GPSkPoint> pointIterator2 = seg2.listIterator();
        while (pointIterator2.hasNext()) {
            int index = pointIterator2.nextIndex();
            GPSkPoint point = pointIterator2.next();
            array2[index] = new Point(point.getEast(), point.getNorth());
        }
        PolygonalLine pl1 = null;
        try {
            pl1 = new PolygonalLine(array1);
        } catch (Exception e) {
            log.warn("exception thrown", e);
            log.warn(Arrays.toString(array1));
            return false;
        }
        
        PolygonalLine pl2 = null;
        try {
            pl2 = new PolygonalLine(array2);
        } catch (Exception e) {
            log.warn("exception thrown", e);
            log.warn(Arrays.toString(array2));
            return false;
        }
        
        
        return df.isFrechet(pl2, pl1, eps);
        
    }
}
