/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic.geom;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.ListIterator;

import org.apache.log4j.Logger;

/**
 * @author Christian Windolf
 * 
 */
public class Polygon {
	private static Logger log = Logger.getLogger(Polygon.class);

	private final Point[] points;
	private final Edge[] edges;

	/**
	 * 
	 * @param points
	 *            fill them in CLOCKWISE
	 */
	public Polygon(Point... points) {
		if (points == null) {
			throw new IllegalArgumentException(
					"input is null, this can't be a polygone.");
		}
		if (points.length < 3) {
			throw new IllegalArgumentException(
					"A polygone must have at least three points to surround an area!");
		}
		this.points = points;
		edges = new Edge[points.length];
		for (int i = 0; i < edges.length - 1; i++) {
			edges[i] = new Edge(points[i], points[i + 1]);
		}
		edges[edges.length - 1] = new Edge(points[edges.length - 1], points[0]);
		checkNoDuplicatePoints();
		checkNoEdgesAreCrossing();
	}

	/**
	 * returns that point, that has the lowest height.
	 * 
	 * @return the point on the bottom or null, if this polygone is empty
	 */
	public Point getBottomPoint() {

		Point min = new Point(0, Double.MAX_VALUE);
		for (Point p : points) {
			if (p.y < min.y) {
				min = p;
			}
		}
		return min;
	}

	public Point getLeftMostPoint() {
		Point min = new Point(Double.MAX_VALUE, 0);
		for (Point p : points) {
			if (p.x < min.x) {
				min = p;
			}
		}
		return min;
	}

	public Point getRightMostPoint() {
		Point max = new Point(Double.MIN_VALUE, 0);
		for (Point p : points) {
			if (p.x > max.x) {
				max = p;
			}
		}
		return max;
	}

	public Point getTopPoint() {
		Point max = new Point(0, Double.MIN_VALUE);
		for (Point p : points) {
			if (p.y > max.y) {
				max = p;
			}
		}
		return max;
	}

	public Point[] getPoints() {
		return points;
	}

	public Edge[] getEdges() {
		return edges;
	}

	/**
	 * checks, if the polygone contains the point p. It uses the ray-casting
	 * algorithm.
	 * 
	 * @param p
	 * @return
	 * @see <a href="http://en.wikipedia.org/wiki/Point_in_polygon">short
	 *      description on wikipedia</a> <br />
	 *      <a href=
	 *      "http://design.osu.edu/carlson/history/PDFs/ten-hidden-surface.pdf"
	 *      >complete paper</a>
	 */
	public boolean contains(Point p) {
		Point left = getLeftMostPoint();
		Point right = getRightMostPoint();
		if (p.x < left.x) {
			return false;
		}
		if (p.x > right.x) {
			return false;
		}

		Point top = getTopPoint();
		Point bottom = getBottomPoint();

		if (p.y < bottom.y) {
			return false;
		}
		if (p.y > top.y) {
			return false;
		}

		Edge e1 = new Edge(p.x, p.y, p.x + (Math.abs(left.x - right.x)), p.y);
		int counter = 0;
		for (Edge e2 : edges) {
			try {
				Point r = e1.crosses(e2);
				if (r != null) {
					counter++;
				}
			} catch (CollinearException e) {
				if (e2.y1 == e1.y1) {
					counter++;
				}
			}
		}
		return counter % 2 == 1;

	}

	/**
	 * checks, that no point is contained doubled...
	 */
	private void checkNoDuplicatePoints() {
		for (int i = 0; i < points.length; i++) {
			Point p1 = points[i];
			for (int j = i + 1; i < points.length; i++) {
				Point p2 = points[j];
				if (p1.equals(p2)) {
					throw new IllegalArgumentException(
							"at least one point is contained doubled in this polygone");
				}
			}
		}
	}

	private void checkNoEdgesAreCrossing() {
		for (int i = 0; i < edges.length - 1; i++) {
			Edge e1 = edges[i];
			Edge e2 = edges[i + 1];
			try {
				Point p = e1.crosses(e2);
			} catch (CollinearException e) {
				log.warn("there are two edges in one line. Will cause more computation time");
			}
			int lastIndex;
			if(i == 0){
				lastIndex = edges.length - 1;
			} else {
				lastIndex = edges.length;
			}
			for (int j = i + 2; j < lastIndex; j++) {
				Edge e3 = edges[j];
				Point p1 = new Point(e3.x1, e3.y1);
				Point p2 = new Point(e3.x2, e3.y2);
				if (e1.pointLiesOnIt(p1) || e1.pointLiesOnIt(p2)) {
					throw new IllegalArgumentException("Illegal state");
				}

				try {
					Point cross = e1.crosses(e3);
					if (cross != null) {
						throw new IllegalArgumentException(
								"two edges of this polygone cross each other");
					}
				} catch (CollinearException e) {
					// all fine
				}
			}
			
		}
	}

	public static class Builder {
		private final LinkedList<Point> points = new LinkedList<Point>();

		private Point last1 = null;
		private Point last2 = null;

		public Builder add(Point p) {
			if (last1 != null && last2 != null) {
				Edge e1 = new Edge(last1, last2);
				Edge e2 = new Edge(last2, p);
				try {
					e1.crosses(e2);
					last1 = last2;

				} catch (CollinearException e) {
					points.removeLast();

				} finally {
					last2 = p;
					points.add(p);
				}
				return this;
			}
			last1 = last2;
			last2 = p;
			points.add(p);
			return this;
		}

		public Builder add(double x, double y) {
			return add(new Point(x, y));
		}

		public Polygon build() {
			if (points.isEmpty()) {
				return null;
			}
			Point[] pointArray = new Point[points.size()];
			ListIterator<Point> li = points.listIterator();
			while (li.hasNext()) {
				int index = li.nextIndex();
				Point p = li.next();
				pointArray[index] = p;
			}

			return new Polygon(pointArray);
		}
	}

	public static class RectangleBuilder {
		private double x1, y1;
		private double x2, y2;

		public RectangleBuilder(double x1, double y1, double x2, double y2) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		}

		public RectangleBuilder() {

		}

		public void setX1(double x1) {
			this.x1 = x1;
		}

		public void setY1(double y1) {
			this.y1 = y1;
		}

		public void setX2(double x2) {
			this.x2 = x2;
		}

		public void setY2(double y2) {
			this.y2 = y2;
		}

		public void setLeftUp(Point p) {
			this.x1 = p.x;
			this.y1 = p.y;
		}
		
		public void setLeftUp(double x1, double y1){
			this.y1 = y1;
			this.x1 = x1;
		}
		
		public void setRightDown(double x2, double y2){
			this.y2 = y2;
			this.x2 = x2;
		}
		
		public void setRightDown(Point p){
			setRightDown(p.x, p.y);
		}
		
		public Polygon build(){
			return new Polygon(new Point(x1, y1), new Point(x2, y1), new Point(x2, y2), new Point(x1, y2));
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("Polygon{");
		builder.append("(").append(points[0].x).append(",").append(points[0].y)
				.append(")");
		for (int i = 1; i < points.length; i++) {
			builder.append(",");
			builder.append("(").append(points[i].x).append(",")
					.append(points[i].y).append(")");
		}
		builder.append("}");
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(points);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Polygon other = (Polygon) obj;
		if (!Arrays.equals(points, other.points))
			return false;
		return true;
	}
	
	

}
