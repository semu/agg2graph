/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic.geom;

import static java.lang.Double.doubleToLongBits;

/**
 * represents an edge between two points
 * 
 * @author Christian Windolf
 * 
 */
public class Edge {

	protected final double x1;
	protected final double y1;
	protected final double x2;
	protected final double y2;

	private final double v_x;
	private final double v_y;

	/**
	 * 
	 */
	public Edge(double x1, double y1, double x2, double y2) {
		if (Double.isNaN(x1) || Double.isNaN(y1) || Double.isNaN(x2)
				|| Double.isInfinite(y2)) {
			throw new IllegalArgumentException(
					"one of the input coordinates is no a number");
		}
		if (Double.isInfinite(x1) || Double.isInfinite(y1)
				|| Double.isInfinite(x2) || Double.isInfinite(y2)) {
			throw new IllegalArgumentException(
					"one of the input coordinates has an infinite value");
		}
		if (x1 == x2 && y1 == y2) {
			throw new IllegalArgumentException(
					"you are creating a point, not an edge!");
		}
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.v_x = x2 - x1;
		this.v_y = y2 - y1;
	}

	public Edge(Point p1, Point p2) {
		this(p1.x, p1.y, p2.x, p2.y);
	}

	/**
	 * resolves by the algorithm of Gauß crossing point between these two edge.
	 * <code>
	 * |x1P|      |x2P|    |x1Q|     |x2Q|
	 * |   | + r* |   |  = |   | + s*|   |
	 * |y1P|      |y2P|    |y1Q|     |y2Q|
	 * </code> The first vector consists of (x1,y1) and the second vector is the
	 * vector from (x1,y1) to (x2,y2). if there can be found a r between 0 and 1
	 * AND and s between 0 and 1, the edge crosses
	 * 
	 * @param other
	 * @return
	 * @throws CollinearException
	 */
	public Point crosses(Edge other) throws CollinearException {
		if (this.equals(other)) {
			throw new CollinearException("yes, of course I cross myself...");
		}

		/*
		 * if they are collinear, checkCollinear will throw an exception that
		 * will be rethrown by the throws declaration in method signature
		 */
		checkCollinear(other);
		if(x1 < other.x1 && x1 < other.x2 && x2 < other.x1 && x2 < other.x2){
			return null;
		}
		if(x1 > other.x1 && x1 > other.x2 && x2 > other.x1 && x2 > other.x2){
			return null;
		}
		if(y1 < other.y1 && y1 < other.y2 && y2 < other.y1 && y2 < other.y2){
			return null;
		}
		if(y1 > other.y1 && y1 > other.y2 && y2 > other.y1 && y2 > other.y2){
			return null;
		}
		
		
		if (x2 == other.x1 && y2 == other.y2) {
			return new Point(x2, y2);// save computation time on special cases
		}

		// now they definetely are not collinear...

		// resolve s...

		double s;
		if (v_x == 0) {
			// since we hace checked, that they are not collinear,
			// the condition v_x == 0 && other.v_x == 0 can't be true.
			// Threrefor we can savely divide by other.v_x
			s = (other.x1 - x1) / other.v_x;
			s = -s;
		} else {
			double frac1 = v_y / v_x;
			double frac2 = (other.v_x * frac1) - other.v_y;
			// frac2 can only reach 0, if the cross product is 0.
			// that would mean, that the two edge are collinear, but that
			// possibility
			// was elimnated before
			double rightSide = other.y1 - y1 - (frac1 * (other.x1 - x1));
			s = rightSide / frac2;
		}

		if (s < 0) {
			return null;
		} else if (s > 1) {
			return null;
		}

		// resolve r...

		double r;
		if (s == 0) {
			r = (other.x1 - x1) / v_x;
		} else if (v_x != 0) {
			double rightSide = (other.x1 - x1) - (s * other.v_x);
			r = rightSide / v_x;
		} else {
			double rightSide = (other.y1 - y1) - (s * other.v_y);
			r = rightSide / v_y;
		}

		if (r < 0) {
			return null;
		} else if (r > 1) {
			return null;
		}

		Point result = new Point(x1 + r * v_x, y1 + r * v_y);
		return result;

	}

	private void checkCollinear(Edge other) throws CollinearException {

		if (v_x == 0 && other.v_x == 0) {
			throw new CollinearException("edges are collinear!");
		} else if (v_y == 0 && other.v_y == 0) {
			throw new CollinearException("edges are collinear!");
		}

		double r_x = other.v_x / v_x;
		double r_y = other.v_y / v_y;

		if (doubleToLongBits(r_x) == doubleToLongBits(r_y)) {
			throw new CollinearException("edges are collinear!");
		}
	}

	public boolean pointLiesOnIt(Point p) {
		if (p.x == x1 && p.y == y1) {
			return true;
		}
		if (p.x == x2 && p.y == y2) {
			return true;
		}

		double r_x;
		double r_y;

		if (v_x == 0) {
			if (x1 == p.x) {
				r_x = 0;
			} else {
				return false;
			}
		} else {
			r_x = (p.x - x1) / v_x;
		}

		if (v_y == 0) {
			if (y1 == p.y) {
				r_y = 0;
			} else {
				return false;
			}
		} else {
			r_y = (p.y - y1) / v_y;
		}

		return (Math.abs(r_x - r_y) < 0.00001);
	}

	public boolean isEndPoint(Point p) {
		if (p.x == x1 && p.y == y1) {
			return true;
		} else if (p.x == x2 && p.y == y2) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass()) {
			return false;
		}

		Edge other = (Edge) obj;
		if (doubleToLongBits(x1) != doubleToLongBits(other.x1)) {
			return false;
		}
		if (doubleToLongBits(y1) != doubleToLongBits(other.y1)) {
			return false;
		}
		if (doubleToLongBits(x2) != doubleToLongBits(other.x2)) {
			return false;
		}
		if (doubleToLongBits(y2) != doubleToLongBits(other.y2)) {
			return false;
		}
		return true;

	}

	@Override
	public String toString() {
		return "Edge{(" + x1 + ',' + y1 + "),(" + x2 + ',' + y2 + ")}";
	}

}
