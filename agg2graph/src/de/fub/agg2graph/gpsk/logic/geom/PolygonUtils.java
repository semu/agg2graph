/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic.geom;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;

import de.fub.agg2graph.gpsk.logic.geom.Polygon.Builder;

/**
 * @author Christian Windolf
 *
 */
public class PolygonUtils {
	
	private static final GeometryFactory gFactory = new GeometryFactory();
	/**
	 * 
	 */
	public PolygonUtils() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * checks, if somehow two polygons overlap each other
	 * @param pg1
	 * @param pg2
	 * @return
	 */
	public static boolean overlap(Polygon pg1, Polygon pg2){
		if(pg1.equals(pg2)){
			return true;
		}
		Point left1 = pg1.getLeftMostPoint();
		Point right1 = pg2.getRightMostPoint();
		if(left1.getX() > right1.getX()){
			return false;
		}
		Point left2 = pg2.getLeftMostPoint();
		Point right2 = pg1.getRightMostPoint();
		if(left2.getX() > right2.getX()){
			return false;
		}
		
		Point bottom1 = pg1.getBottomPoint();
		Point top1 = pg2.getTopPoint();
		if(top1.getY() < bottom1.getY()){
			return false;
		}
		
		Point top2 = pg2.getTopPoint();
		Point bottom2 = pg1.getBottomPoint();
		if(top2.getY() < bottom2.getY()){
			return false;
		}
		for(Edge e1 : pg1.getEdges()){
			for(Edge e2 : pg2.getEdges()){
				if(e1.pointLiesOnIt(new Point(e2.x1, e2.y1)) || e1.pointLiesOnIt(new Point(e2.x2, e2.y2))){
					return true;
				}
				try {
					if(e1.crosses(e2) != null){
						return true;
					}
				} catch (CollinearException e) {
					//does not matter
				}
			}
		}
		for(Point p : pg1.getPoints()){
			if(pg2.contains(p)){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * 
	 * @param pg1
	 * @param pg2
	 * @return
	 */
	public static Polygon merge(Polygon pg1, Polygon pg2){
		Point[] points1 = pg1.getPoints();
		Coordinate coords1[] = new Coordinate[points1.length + 1];
		
		for(int i = 0; i < points1.length; i++){
			coords1[i] = new Coordinate(points1[i].getX(), points1[i].getY());
		}
		coords1[coords1.length - 1] = coords1[0];
		
		Point[] points2 = pg2.getPoints();
		Coordinate coords2[] = new Coordinate[points2.length + 1];
		
		for(int i = 0; i < points2.length; i++){
			coords2[i] = new Coordinate(points2[i].getX(), points2[i].getY());
		}
		coords2[coords2.length - 1] = coords2[0];
		LinearRing ring1 = gFactory.createLinearRing(coords1);
		LinearRing ring2 = gFactory.createLinearRing(coords2);
		
		com.vividsolutions.jts.geom.Polygon polygon1 = gFactory.createPolygon(ring1, null);
		com.vividsolutions.jts.geom.Polygon polygon2 = gFactory.createPolygon(ring2, null);
		
		Geometry g = polygon1.union(polygon2);
		Polygon.Builder builder = new Polygon.Builder();
		Coordinate[] resultCoords = g.getCoordinates();
		for(int i = 0; i < resultCoords.length - 1; i++){
			Coordinate c = resultCoords[i];
			builder.add(c.x, c.y);
		}
		return builder.build();
		
		
	}

}
