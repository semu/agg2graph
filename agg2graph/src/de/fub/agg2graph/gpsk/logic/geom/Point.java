/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic.geom;

import static java.lang.Double.doubleToLongBits;
import static java.lang.Double.isInfinite;
import static java.lang.Double.isNaN;

/**
 * @author Christian Windolf
 *
 */
public class Point {
	
	protected final double x;
	protected final double y;

	/**
	 * 
	 */
	public Point(double x, double y) {
		if(isNaN(x)){
			throw new IllegalArgumentException("x coordinate is not a number!");
		}
		if(isNaN(y)){
			throw new IllegalArgumentException("y coordinate is not a number!");
		}
		if(isInfinite(x)){
			throw new IllegalArgumentException("x coordinate is infinite");
		}
		if(isInfinite(y)){
			throw new IllegalArgumentException("y coordinate is infinite");
		}
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString(){
		return "Point{" + x + ',' + y + '}';
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}
	
	
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}

}
