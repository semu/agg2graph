/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 
 * @author Christian Windolf, christianwindolf@web.de
 */
public class ProjectionFactory {

	private ProjectionFactory() {
	}

	public static ProjectionFactory getInstance() {
		return ProjectionFactoryHolder.INSTANCE;
	}

	private static class ProjectionFactoryHolder {

		private static final ProjectionFactory INSTANCE = new ProjectionFactory();
	}

	private Class<? extends Projection> projectionClass = UTMProjection.class;

	public Projection createProjection(double minLat, double minLon,
			double maxLat, double maxLon, Class<? extends Projection> pc) {
		try {
			Constructor<? extends Projection> constructor = pc.getConstructor(
					Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE);

			return (Projection) constructor.newInstance(minLat, minLon, maxLat,
					maxLon);
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException e) {
			return null;
		}
	}

	public Projection createProjection(double minLat, double minLon,
			double maxLat, double maxLon)  {
		return createProjection(minLat, minLon, maxLat, maxLon, projectionClass);
	}

	public Projection createProjection(double[] bounds, Class<? extends Projection> pc){
		Constructor<? extends Projection> constructor;
		try {
			constructor = pc.getConstructor(double[].class);
			return (Projection) constructor.newInstance(bounds);
		} catch (NoSuchMethodException | SecurityException e) {
			return null;
		} catch (InstantiationException | IllegalAccessException e) {
			return null;
		}  catch (IllegalArgumentException e) {
			return null;
		} catch (InvocationTargetException e) {
			return null;
		}
	}

	public Projection createProjection(double[] bounds)
			{
		return createProjection(bounds, projectionClass);
	}

	public Projection createProjection(GPSkPoint southWest,
			GPSkPoint northEast, Class<? extends Projection> pc) {
		Constructor<? extends Projection> constructor;
		try {
			constructor = pc.getConstructor(GPSkPoint.class,
					GPSkPoint.class);
			return (Projection) constructor.newInstance(southWest, northEast);
		} catch (NoSuchMethodException | SecurityException |IllegalAccessException 
				| InstantiationException | IllegalArgumentException | InvocationTargetException e) {
			return null;
		}
	}

	public Projection createProjection(GPSkPoint southWest, GPSkPoint northEast){
		return createProjection(southWest, northEast, projectionClass);
	}
}
