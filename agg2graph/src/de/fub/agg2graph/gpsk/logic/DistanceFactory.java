/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class DistanceFactory {
    private static final DistanceFactory singleton = new DistanceFactory();
    
    Class<? extends PointDistance> prefClass = TrigonometricDistance.class;
    
    public static DistanceFactory getInstance(){
        return singleton;
    }
    
    public PointDistance createNewInstance(){
        return new TrigonometricDistance();
    }
    
    public PointDistance createNewInstance(Class<? extends PointDistance> c) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        Constructor constructor = c.getConstructor();
        PointDistance pd = (PointDistance) constructor.newInstance();
        return pd;
        
    }
}
