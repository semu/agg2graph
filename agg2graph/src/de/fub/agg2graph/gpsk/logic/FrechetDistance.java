/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkSegment;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public abstract class FrechetDistance {
    protected final double eps;
    
    public FrechetDistance(){
        eps = Double.NaN;
    }
    
    public FrechetDistance(double eps){
        this.eps = eps;
    }
    
    /**
     * Checks if two segments are with the frechet distance.
     * Note: most implementations expect, that a projection is set on the segments.
     * @param seg1
     * @param seg2
     * @return 
     */
    public boolean isInFrechetDistance(GPSkSegment seg1, GPSkSegment seg2){
        if(Double.isNaN(eps)){
            throw new IllegalArgumentException("don't construct an object without initializing an eps value");
            
        }
        else {
            return isInFrechetDistance(seg1, seg2, eps);
        }
    }
    
    public double getEps(){
    	return eps;
    }
    
    public abstract boolean isInFrechetDistance(GPSkSegment seg1, GPSkSegment seg2, double eps);
}
