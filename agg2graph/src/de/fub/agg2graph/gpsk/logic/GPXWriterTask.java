/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.schemas.gpx.GpxType;
import java.io.File;

import static de.fub.agg2graph.gpsk.io.EnvironmentExport.convertToGpx;
import de.fub.agg2graph.io.FileExport;
import de.fub.agg2graph.schemas.gpsk.ObjectFactory;
import java.io.IOException;
import java.util.logging.Level;
import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class GPXWriterTask implements Runnable{
    private static Logger log = Logger.getLogger(ObjectFactory.class);
    private String input;
    private File path;
    
    private FileExport fe;
    
    public GPXWriterTask(String input, File path){
        this();
        this.input = input;
        this.path = path;
        
    }
    
    public GPXWriterTask(){
        try {
            fe = new FileExport(ObjectFactory.class);
        } catch (JAXBException ex) {
            log.error("it will be unpossible to save any data!", ex);
        }
    }
    
    public void setInput(String input){
        this.input = input;
    }
    
    public void setPath(File path){
        this.path = path;
    }

    @Override
    public void run() {
        EnvironmentManager em = EnvironmentManager.getInstance();
        GPSkEnvironment env = em.getEnv(input);
        
        GpxType gpx = convertToGpx(env);
        try {
            fe.export(path, gpx);
        } catch (JAXBException | NullPointerException | IOException ex) {
            log.error("unable to save file", ex);
        } 
    }
    
    
    
    
}
