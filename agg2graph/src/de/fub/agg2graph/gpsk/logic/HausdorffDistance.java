/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.utils.PointUtils;
import de.fub.agg2graph.gpsk.utils.SegmentUtils;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class HausdorffDistance extends FrechetDistance{
    private PointDistance pd = DistanceFactory.getInstance().createNewInstance();

    public HausdorffDistance() {
    }

    public HausdorffDistance(double eps) {
        super(eps);
    }
    
    

    @Override
    public boolean isInFrechetDistance(GPSkSegment seg1, GPSkSegment seg2, double eps) {
        double maxA = Double.MIN_VALUE;
        for(GPSkPoint p : seg2){
            double d = minDist(seg1, p);
            if(d > maxA){
                maxA = d;
            }
        }
        double maxB = Double.MIN_VALUE;
        for(GPSkPoint p : seg1) {
            double d = minDist(seg2, p);
            if(d > maxB){
                maxB = d;
            }
        }
        double distance = Math.max(maxA, maxB); 
        double angle1 = SegmentUtils.angle(seg1);
        double angle2 = SegmentUtils.angle(seg2);
        double diffAngle = (angle1 - angle2 + 360) % 360;
        return distance + (distance * (diffAngle / 10)) < eps; 
        
    }
    
    
    protected double minDist(GPSkSegment k, GPSkPoint x){
        double minDist = Double.MAX_VALUE;
        for(GPSkPoint p : k){
            double d = pd.calculate(p, x);
            if(d < minDist){
                minDist = d;
            }
        }
        return minDist;
    }
}
