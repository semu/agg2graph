/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class SimpleAggregation extends ClusterAggregation{
    private PointDistance pd = DistanceFactory.getInstance().createNewInstance();
            

    @Override
    public GPSkSegment aggregate(List<GPSkSegment> list) {
        GPSkSegment first = list.remove(0);
        List<GPSkPoint> sideOne = new LinkedList<>();
        sideOne.add(first.getFirst());
        List<GPSkPoint> sideTwo = new LinkedList<>();
        sideTwo.add(first.getLast());
        for(GPSkSegment seg : list){
            if(pd.calculate(first.getFirst(), seg.getFirst()) < pd.calculate(first.getFirst(), seg.getLast())){
                sideOne.add(seg.getFirst());
                sideTwo.add(seg.getLast());
            } else {
                sideOne.add(seg.getLast());
                sideTwo.add(seg.getFirst());
            }
        }
        double lat = 0;
        double lon = 0;
        for(GPSkPoint p : sideOne){
            lat += p.getLat();
            lon += p.getLon();
        }
        
        GPSkPoint one = new GPSkPoint(lat / (double) sideOne.size(), lon / (double) sideOne.size());
        lat = 0;
        lon = 0;
        for(GPSkPoint p : sideTwo){
            lat += p.getLat();
            lon += p.getLon();
        }
        
        GPSkPoint two = new GPSkPoint(lat/(double) sideTwo.size(), lon / (double) sideTwo.size());
        GPSkSegment aggregated = new GPSkSegment();
        aggregated.add(one);
        aggregated.add(two);
        return aggregated;
        
        
    }
    
}
