/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.structs.GPSPoint;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public abstract class Projection {
    
    public double minLat;
    public double minLon;
    public double maxLat;
    public double maxLon;
    
    public Projection(double minLat, double minLon, double maxLat, double maxLon){
        this.minLat = minLat;
        this.minLon = minLon;
        this.maxLat = maxLat;
        this.maxLon = maxLon;
    }
    
    public Projection(GPSPoint southWest, GPSPoint northEast){
        this.minLat = southWest.getLat();
        this.minLon = southWest.getLon();
        this.maxLat = northEast.getLat();
        this.maxLon = northEast.getLon();
    }
    
    public Projection(double[] bounds){
        this.minLat = bounds[0];
        this.minLon = bounds[1];
        this.maxLat = bounds[2];
        this.maxLon = bounds[3];
    }
    
    public double[] getBounds(){
        return new double[]{
            minLat,
            minLon,
            maxLat,
            maxLon
        };
    }
    
    public abstract void setProjection(GPSkPoint p);
    
    public abstract GPSPoint toLatLon(double x, double y);

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(maxLat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(maxLon);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minLat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minLon);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projection other = (Projection) obj;
		if (Double.doubleToLongBits(maxLat) != Double
				.doubleToLongBits(other.maxLat))
			return false;
		if (Double.doubleToLongBits(maxLon) != Double
				.doubleToLongBits(other.maxLon))
			return false;
		if (Double.doubleToLongBits(minLat) != Double
				.doubleToLongBits(other.minLat))
			return false;
		if (Double.doubleToLongBits(minLon) != Double
				.doubleToLongBits(other.minLon))
			return false;
		return true;
	}
    
    
    
}
