/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.KeyValueStore;
import org.apache.log4j.Logger;
import de.fub.agg2graph.input.CleaningOptions;

import static de.fub.agg2graph.gpsk.KeyValueStore.getValue;
import static de.fub.agg2graph.gpsk.KeyValueStore.getBoolean;
import static de.fub.agg2graph.gpsk.KeyValueStore.getDouble;
import static de.fub.agg2graph.gpsk.KeyValueStore.getInt;
import static de.fub.agg2graph.gpsk.KeyValueStore.getLong;
import static de.fub.agg2graph.gpsk.KeyValueStore.putValue;




/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class CleaningOptionsIO {

    private static Logger log = Logger.getLogger(CleaningOptionsIO.class);
    private static final String prefix = "cleaning";
    
    private static final String FILTER_BY_SEGMENT_LENGTH_KEY = prefix + ".filterBySegmentLength";
    private static final String MIN_SEGMENT_LENGTH_KEY = prefix + ".minSegmentLength";
    private static final String MAX_SEGMENT_LENGTH_KEY = prefix + ".maxSegmentLength";
    
    private static final String FILTER_BY_EDGE_LENGTH_KEY = prefix + ".filterByEdgeLength";
    private static final String MIN_EDGE_LENGTH_KEY = prefix + ".minEdgeLength";
    private static final String MAX_EDGE_LENGTH_KEY = prefix + ".maxEdgeLength";
    
    private static final String FILTER_BY_EDGE_LENGTH_INCREASE_FACTOR_KEY = prefix + 
            ".filterByEdgeLengthIncreaseFactor";
    private static final String MIN_EDGE_INCREASE_FACTOR_KEY = prefix + ".minEdgeIncreaseFactor";
    private static final String MIN_EDGE_LENGTH_AFTER_INCREASE_KEY = prefix + ".minEdgeLengthAfterIncrease";
    
    private static final String FILTER_ZIGZAG_KEY = prefix + ".filterZigZag";
    private static final String MAX_ZIGZAG_ANGLE_KEY = prefix + ".maxZigZagAngle";
    
    private static final String FILTER_FAKE_CIRCLE_KEY = prefix + ".filterFakeCircle";
    private static final String MAX_FAKE_CIRLCE_ANGLE_KEY = prefix + ".maxFakeCircleAngle";
    
    private static final String FILTER_OUTLIERS_KEY = prefix + ".filterOutliers";
    private static final String MAX_NUM_OUTLIERS_KEY = prefix + ".maxNumOutliers";

    public static CleaningOptions load() {
        CleaningOptions default_options = new CleaningOptions();

        CleaningOptions return_options = new CleaningOptions();

        return_options.filterBySegmentLength = getBoolean(FILTER_BY_SEGMENT_LENGTH_KEY, 
                default_options.filterBySegmentLength);

        try {
            return_options.minSegmentLength = getLong(MIN_SEGMENT_LENGTH_KEY, 
                    default_options.minSegmentLength);
        } catch (NumberFormatException ex){
            log.warn("The key value store has an illegal entry for " + MIN_SEGMENT_LENGTH_KEY);
        }
        
        try{
            return_options.maxSegmentLength = getLong(MAX_SEGMENT_LENGTH_KEY, 
                    default_options.maxSegmentLength);
        } catch (NumberFormatException ex){
            log.warn("The key value store has an illegal entry for " + MAX_SEGMENT_LENGTH_KEY);
        }
        
        return_options.filterByEdgeLength = getBoolean(FILTER_BY_EDGE_LENGTH_KEY, default_options.filterByEdgeLength);
        
        try{
            return_options.minEdgeLength = getDouble(MIN_EDGE_LENGTH_KEY, default_options.minEdgeLength);
            
        } catch(NumberFormatException ex){
            log.warn("The key value store has an illegal entry for " + MIN_EDGE_LENGTH_KEY);
        }
        
        try{
            return_options.maxEdgeLength = getDouble(MAX_EDGE_LENGTH_KEY, default_options.maxEdgeLength);
        } catch (NumberFormatException ex){
            log.warn("The key value store has an illegal entry for " + MAX_EDGE_LENGTH_KEY);
        }
        
        return_options.filterByEdgeLengthIncrease = getBoolean(FILTER_BY_EDGE_LENGTH_INCREASE_FACTOR_KEY, default_options.filterByEdgeLengthIncrease);
        try{
            return_options.minEdgeLengthIncreaseFactor = getDouble(MIN_EDGE_INCREASE_FACTOR_KEY, default_options.minEdgeLengthIncreaseFactor);
        } catch (NumberFormatException ex){
            log.warn("The key value store has an illegal entry for " + MIN_EDGE_INCREASE_FACTOR_KEY);
        }
        
        try{
            return_options.minEdgeLengthAfterIncrease = getDouble(MIN_EDGE_LENGTH_AFTER_INCREASE_KEY, default_options.minEdgeLengthAfterIncrease);                    
        } catch (NumberFormatException ex){
           log.warn("The key value store has an illegal entry for " + MIN_EDGE_LENGTH_AFTER_INCREASE_KEY); 
        }
        
        return_options.filterZigzag = getBoolean(FILTER_ZIGZAG_KEY, default_options.filterZigzag);
        
        try{
            return_options.maxZigzagAngle = getDouble(MAX_ZIGZAG_ANGLE_KEY, default_options.maxZigzagAngle);
        } catch (NumberFormatException ex){
            log.warn("The key value store has an illegal entry for " + MAX_ZIGZAG_ANGLE_KEY);
        }
        
        return_options.filterFakeCircle = getBoolean(FILTER_FAKE_CIRCLE_KEY, default_options.filterFakeCircle);
        
        try{
            return_options.maxFakeCircleAngle = getDouble(MAX_FAKE_CIRLCE_ANGLE_KEY, default_options.maxFakeCircleAngle);
        } catch(NumberFormatException ex){
            log.warn("The key value store has an illegal entry for " + MAX_FAKE_CIRLCE_ANGLE_KEY);
        }
        
        return return_options;
    }
    
    public static void store(CleaningOptions co){
        putValue(FILTER_BY_SEGMENT_LENGTH_KEY, co.filterBySegmentLength);
        putValue(MIN_SEGMENT_LENGTH_KEY, co.minSegmentLength);
        putValue(MAX_SEGMENT_LENGTH_KEY, co.maxSegmentLength);
        
        putValue(FILTER_BY_EDGE_LENGTH_KEY, co.filterByEdgeLength);
        putValue(MIN_EDGE_LENGTH_KEY, co.minEdgeLength);
        putValue(MAX_EDGE_LENGTH_KEY, co.maxEdgeLength);
        
        putValue(FILTER_BY_EDGE_LENGTH_INCREASE_FACTOR_KEY, co.filterByEdgeLengthIncrease);
        putValue(MIN_EDGE_INCREASE_FACTOR_KEY, co.minEdgeLengthIncreaseFactor);
        putValue(MIN_EDGE_LENGTH_AFTER_INCREASE_KEY, co.minEdgeLengthAfterIncrease);
        
        putValue(FILTER_ZIGZAG_KEY, co.filterZigzag);
        putValue(MAX_ZIGZAG_ANGLE_KEY, co.maxZigzagAngle);
    }
}
