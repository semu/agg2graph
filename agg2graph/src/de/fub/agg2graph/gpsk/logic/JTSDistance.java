/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.awt.geom.Point2D;
import java.util.ListIterator;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class JTSDistance extends FrechetDistance {
    
    public JTSDistance(){
        super();
    }
    
    public JTSDistance(double eps){
        super(eps);
    }

    @Override
    public boolean isInFrechetDistance(GPSkSegment seg1, GPSkSegment seg2, double eps) {
        Point2D[] p = new Point2D[seg1.size()];
        for(ListIterator<GPSkPoint> it = seg1.listIterator(); it.hasNext(); ){
            int index = it.nextIndex();
            GPSkPoint point = it.next();
            p[index] = new Point2D.Double(point.getEast(), point.getNorth());
        }
        
        Point2D[] q = new Point2D[seg2.size()];
        for(ListIterator<GPSkPoint> it = seg2.listIterator(); it.hasNext();){
            int index = it.nextIndex();
            GPSkPoint point = it.next();
            q[index] = new Point2D.Double(point.getEast(), point.getNorth());
        }
        
        geom.FrechetDistance fd = new geom.FrechetDistance(p, p);
        return fd.isFrechet(eps);
        
    }
    
}
