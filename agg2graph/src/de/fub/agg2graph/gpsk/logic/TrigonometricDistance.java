/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;

import static java.lang.Math.*;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class TrigonometricDistance extends PointDistance {
	public static final double RADIUS = 6378388;
	
	@Override
	public double calculate(GPSkPoint p1, GPSkPoint p2) {
		if (Double.doubleToLongBits(p1.getLat()) == Double.doubleToLongBits(p2.getLat())
			&& Double.doubleToLongBits(p1.getLon()) == Double.doubleToLongBits(p2.getLon())) {
			return 0;
		}
		
		double lat1 = toRadians(p1.getLat());
		double lon1 = toRadians(p1.getLon());
		double lat2 = toRadians(p2.getLat());
		double lon2 = toRadians(p2.getLon());
		
		double distanceDegree = (sin(lat1) * sin(lat2)) + (cos(lat1) * cos(lat2) * cos(lon2 - lon1));
		
		return RADIUS * acos(distanceDegree);
	}
}