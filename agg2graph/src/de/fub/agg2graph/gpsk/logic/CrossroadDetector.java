/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.logic;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.algorithms.optics.OPTICSPoint;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.mapviewer.GeoPosition;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_POINTS;
import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.TYPE;
import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.LAYER_VISIBLE;
import de.fub.agg2graph.structs.GPSPoint;
import java.util.HashMap;

/**
 *
 * @author Christian Windolf
 */
public class CrossroadDetector implements Runnable {

    private static Logger log = Logger.getLogger(CrossroadDetector.class);
    public static final String RANGEKEY = "crossrad.range";
    public static final String RELKEY = "crossroad.relative";
    public static final int DEFAULT_RANGE = 10;
    public static final double DEFAULT_REL = 0.5;
    private final int range;
    private final double rel;
    GPSkEnvironment env;
    private final List<OPTICSPoint> results = new LinkedList<>();
    private final List<OPTICSPoint> pointsList;
    
    private final String input;
    private final String output;

    public CrossroadDetector(String input, String output){
        EnvironmentManager em = EnvironmentManager.getInstance();
        range = KeyValueStore.getInt(RANGEKEY, DEFAULT_RANGE);
        rel = KeyValueStore.getDouble(RELKEY, DEFAULT_REL);
        env = em.getEnv(input);
        if(env == null){
            throw new IllegalArgumentException("for key " + input + " there is no data");
        }
        List<GPSkPoint> list = env.getPoints();
        pointsList = new ArrayList<>(list.size());
        //convert it
        for(GPSkPoint p : list){
            
            pointsList.add((OPTICSPoint) p);
        }
        this.input = input;
        this.output = output;
    }
    
    

    @Override
    public void run() {
        if (log.isDebugEnabled()) {
            log.debug("Detecting crossroads. range=" + range + " | rel=" + rel);
        }
        

        for (int i = range; i < pointsList.size() - range - 1; i++) {
            double avg = getAverage(i);
            OPTICSPoint p = pointsList.get(i);
            if (avg == -1) {
                //skip it
            } else {
                if (p.getReachabilityDistance()< avg * rel) {
                    results.add(p);
                }
            }

        }

        log.info("Found " + results.size() + " crossroads");
        EnvironmentBuilder builder = new EnvironmentBuilder(results);
        builder.setTitle("detected crossroads");
        builder.setFocus(FOCUS_ON_POINTS);
        
        HashMap<String, String> map = new HashMap<>();
        map.put("amound of crossroads", String.valueOf(results.size()));
        map.put(TYPE, "crossroads");
        map.put(LAYER_VISIBLE, "true");
        map.put("color", "yellow");
        
        builder.keyValue = map;
        builder.useProjection();
        
        builder.setBounds(env.getBounds());
        EnvironmentManager.getInstance().put(output, builder.build());
        log.debug("putted a new environment");
        
    }

    private double getAverage(int index) {
        double sum = 0;
        for (int i = index - range; i < index + range; i++) {
            if (i != index) {
                if (pointsList.get(i).isUndefined()) {
                    return -1;
                }
                sum += pointsList.get(i).getReachabilityDistance();
            }
        }
        return sum / (double) (2 * range);

    }

    private double getAverage(int offset, int skip) {
        double sum = 0;
        for (int i = offset; i < offset + (range * 2) + 1; i++) {
            if (i == skip) {
                i++;
            } else { //skip over it!
                if (pointsList.get(i).isUndefined()) {
                    return -1;
                } else {
                    sum += pointsList.get(i).getReachabilityDistance();
                }
            }
        }
        return sum / (double) (range * 2);
    }
}
