/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import com.infomatiq.jsi.Rectangle;

/**
 * <h3>When is this interface used?</h3>
 * 
 * <p>This interface is used, when you have a spatial {@link Index} over a dataset.
 * To be able use the spatial index via {@link Index#process(IndexProcessor)} you need to implement
 * this interface or look for an implementation.
 * The basic idea is, that the rtree only supports requests over an rectangle.
 * But what if it should look like a circle?
 * Or you have other conditions how you like filter the results.
 * Then you can use this interface.</p>
 * <p>There are two different ways, how requests can be done one the spatial index.
 * One method returns only objects that are fully contained by the {@link IndexProcessor#getRequestedArea() rectangle}.
 * This is recommended if you want to process points
 * The other method is that all objects are processed, that intersect somehow the rectangle.
 * This is recommended for Segments</p>
 * <p><b>NOTE</b> If both, {@link IndexProcessor#useContains()} and {@link IndexProcessor#useIntersect()}
 * return false, nothing will happen.
 * @author Christian Windolf
 *
 */
public interface IndexProcessor<T> {

	/**
	 * tell the {@link Index}, wich area it should process.
	 * <p>the index will use the projected values. That means, every point should have a valid projection</p>
	 * @return
	 */
	public Rectangle getRequestedArea();
	
	/**
	 * for every element that is found in the given {@link IndexProcessor#getRequestedArea() rectangle},
	 * this method will be invoked. Do with it whatever you want...
	 * @param t a value inside the {@link IndexProcessor#getRequestedArea() rectangle}
	 * @see Rectangle
	 */
	public void processValue(T t);
	
	
	
	/**
	 * 
	 * @return use the intersection request on the tree?
	 * @see IndexProcessor more information 
	 */
	public boolean useIntersect();
	
	/**
	 * 
	 * @return use the contains request on the tree?
	 * @see IndexProcessor more information
	 */
	public boolean useContains();
}
