/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import java.util.*;

/**
 *
 * All {@link de.fub.agg2graph.gpsk.beans.GPSkEnvironment} instances should be
 * stored here. It is just an Observable HashMap
 *
 * @author Christian Windolf
 */
public class EnvironmentManager extends Observable {

    /**
     * recommended key to store the raw data from the file
     */
    public static final String RAW_DATA = "rawData";
    /**
     * recommended key to the cleaned data.
     */
    public static final String CLEANED_DATA = "cleanData";
    /**
     * recommended key to the results of RDPfilter
     */
    public static final String SMOOTHED_DATA = "smoothedData";
    /**
     * @deprecated default key, to store results of OPTICS. But it could be
     * useful, to be able to handle more than one instance, so this variable is
     * deprecated
     */
    public static final String OPTICS_RESULTS = "opticsResults";
    public static final String MARKED_CROSSROADS = "markedCrossroads";
    public static final String COMPUTED_CROSSROADS = "computedCrossroads";
    private static final EnvironmentManager singleton = new EnvironmentManager();

    /**
     *
     * @return singleton instance
     */
    public static EnvironmentManager getInstance() {
        return singleton;
    }
    private HashMap<String, GPSkEnvironment> map = new HashMap<String, GPSkEnvironment>();

    private EnvironmentManager() {
    }

    /**
     * put a new {@link de.fub.agg2graph.gpsk.beans.GPSkEnvironment} into the
     * hashmap or overwrite one
     *
     * @param key
     * @param env
     */
    public void put(String key, GPSkEnvironment env) {
        synchronized (this) {
            map.put(key, env);
        }
        setChanged();
        notifyObservers(key);

    }

    public Set<String> keys() {
        synchronized (this) {
            return map.keySet();
        }
    }

    public GPSkEnvironment.MetaInformation getMeta(String key) {
        synchronized (this) {
            GPSkEnvironment env = map.get(key);
            if(env == null) return null;
            return env.getMeta();
        }
    }

    public GPSkEnvironment getEnv(String key) {
        synchronized (this) {
            return map.get(key);
        }
    }

    public Collection<GPSkEnvironment> getValues() {
        return map.values();
    }
    
    public void remove(String key){
        map.remove(key);
        setChanged();
        notifyObservers(key);
    }
    
    public boolean contains(String key){
        return map.containsKey(key);
    }
}
