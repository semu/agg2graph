/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.infomatiq.jsi.Rectangle;


import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.logic.FrechetDistance;
import de.fub.agg2graph.gpsk.logic.FrechetFactory;

/**
 * @author Christian Windolf
 *
 */
public class DefaultSegmentProcedure implements IndexProcedure<GPSkSegment>{
	public ArrayList<GPSkSegment> data;
	public final List<GPSkSegment> results = new LinkedList<>();
	public final GPSkSegment center;
	public final FrechetDistance fd;
	public final double eps;
	

	/**
	 * 
	 */
	public DefaultSegmentProcedure(GPSkSegment center, double eps) {
		this.fd = FrechetFactory.getInstance().createNewInstance(eps);
		this.center = center;
		this.eps = eps;
	}

	/* (non-Javadoc)
	 * @see gnu.trove.TIntProcedure#execute(int)
	 */
	@Override
	public boolean execute(int arg0) {
		
		GPSkSegment seg = data.get(arg0);
		
		if(center.equals(seg)){
			return true;
		}
		if(fd.isInFrechetDistance(seg, center)){
			results.add(seg);
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#getRectangles()
	 */
	@Override
	public List<Rectangle> getRectangles() {
		double[] projBounds = center.getProjectedBounds();
		
		Rectangle rect = new Rectangle(
				(float) (projBounds[0] - eps),
				(float) (projBounds[1] + eps),
				(float) (projBounds[2] + eps),
				(float) (projBounds[3] - eps));
		
		List<Rectangle> list = new LinkedList<>();
		list.add(rect);
		return list;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#getResults()
	 */
	@Override
	public List<GPSkSegment> getResults() {
		return results;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#putData(java.util.ArrayList)
	 */
	@Override
	public void putData(ArrayList<GPSkSegment> data) {
		this.data = data;
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#startNewRectangle()
	 */
	@Override
	public void startNewRectangle() {
		// do nothing
		
	}

}
