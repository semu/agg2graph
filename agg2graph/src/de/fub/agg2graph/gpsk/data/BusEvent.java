/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

/**
 *
 * @author Christian Windolf
 */
public class BusEvent {
    public static final int INFO_MESAGE = 1;
    public static final int ERROR_MESSAGE = 2;
    
    public final String message;
    public final int type;
    public final Object source;
    
    public BusEvent(String message, int type, Object source){
        this.message = message;
        this.type = type;
        this.source = source;
    }
    
    public BusEvent(Exception ex, Object source){
        StringBuilder builder = new StringBuilder("An");
        builder.append(ex.getClass().getSimpleName()).append(" occurred. ");
        builder.append("Cause: ").append(ex.getCause().getMessage());
        message = builder.toString();
        type = ERROR_MESSAGE;
        this.source = source;
        
    }
    
}
