/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import java.util.LinkedList;
import java.util.List;

import com.infomatiq.jsi.Rectangle;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.logic.DistanceFactory;
import de.fub.agg2graph.gpsk.logic.PointDistance;


public class CircleProcessor implements IndexProcessor<GPSkPoint> {
	private final GPSkPoint center;
	private final PointDistance pointDistance = DistanceFactory.getInstance().createNewInstance();
	private final List<GPSkPoint> results = new LinkedList<>();
	private final double eps;

	/**
	 * Creates a new CircleProcessor
	 */
	public CircleProcessor(GPSkPoint center, double eps) {
		this.center = center;
		this.eps = eps;
		
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcessor#getRequestedArea()
	 */
	@Override
	public Rectangle getRequestedArea() {
		return center.getRectangle();
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcessor#processValue(java.lang.Object)
	 */
	@Override
	public void processValue(GPSkPoint t) {
		double distance = pointDistance.calculate(t, center);
		if(distance <= eps){
			results.add(t);
		}
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcessor#useIntersect()
	 */
	@Override
	public boolean useIntersect() {
		return false;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcessor#useContains()
	 */
	@Override
	public boolean useContains() {
		return true;
	}

	/**
	 * @return the center
	 */
	public GPSkPoint getCenter() {
		return center;
	}

	/**
	 * @return the results
	 */
	public List<GPSkPoint> getResults() {
		return results;
	}

	/**
	 * @return the eps
	 */
	public double getEps() {
		return eps;
	}
	
	

}
