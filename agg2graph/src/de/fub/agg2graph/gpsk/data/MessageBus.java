/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf
 */
public class MessageBus {
    public static final String GPSK = "gpsk";
    private static final HashMap<String, MessageBus> map = new HashMap<>();
    
    static{
        map.put(GPSK, new MessageBus());
    }
    
    public static MessageBus getInstance(String key){
        MessageBus mb = map.get(key);
        if(mb != null){
            return mb;
        } else {
            mb = new MessageBus();
            map.put(key, mb);
            return mb;
        }
    }

    private static final int maxBufferSize = 50;
    private final LinkedList<BusEvent> messageBuffer = new LinkedList<>();
    private final List<BusListener> listeners = new LinkedList<>();

    public void addBusListener(BusListener bl) {
        listeners.add(bl);
    }

    public void fireEvent(BusEvent be) {
        synchronized (messageBuffer) {
            messageBuffer.add(be);
            if (messageBuffer.size() >= maxBufferSize) {
                messageBuffer.removeFirst();
            }
        }
        synchronized(listeners){
            for(BusListener bl : listeners){
                bl.messageReceived(be);
            }
        }
    }

    public void removeBusListner(BusListener bl) {
        synchronized(listeners){
            listeners.remove(bl);
        }
    }

    
}
