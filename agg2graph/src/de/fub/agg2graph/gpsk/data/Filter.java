/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.TYPE;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public interface Filter<T> {
    
    
    public boolean filter(T t);
    
    public class DefaultFilter<T> implements Filter<T>{

        @Override
        public boolean filter(T t) {
            if(t == null){
                return false;
            }
            return true;
        }
        
    }
    
    public class Segment implements Filter<GPSkEnvironment>{
        @Override
        public boolean filter(GPSkEnvironment t) {
            if(t.getMeta().showSegments){
                return true;
            } else{
                return false;
            }
            
        }
    }
    
    public class Type implements Filter<GPSkEnvironment>{
        private final String type;
        public Type(String type){
            this.type = type;
        }

        @Override
        public boolean filter(GPSkEnvironment t) {
            String ty = t.getMeta().keyValue.get(TYPE);
            if(ty == null){
                return false;
            }
            return ty.equals(type);
            
        }
    }
}
