/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.infomatiq.jsi.Rectangle;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.logic.DistanceFactory;
import de.fub.agg2graph.gpsk.logic.PointDistance;

/**
 * @author Christian Windolf
 *
 */
public class CircleProcedure implements IndexProcedure<GPSkPoint>{
	private final PointDistance pointDistance = DistanceFactory.getInstance().createNewInstance();
	private final GPSkPoint center;
	private List<GPSkPoint> points = new LinkedList<>();
	private ArrayList<GPSkPoint> data;
	private final double radius;

	/**
	 * 
	 */
	public CircleProcedure(GPSkPoint center, double radius) {
		this.center = center;
		this.radius = radius;
	}

	/* (non-Javadoc)
	 * @see gnu.trove.TIntProcedure#execute(int)
	 */
	@Override
	public boolean execute(int arg0) {
            if(data.get(arg0).equals(center)){
                return true;
            }
		if(pointDistance.calculate(data.get(arg0), center) <= radius){
			points.add(data.get(arg0));
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#getRectangle()
	 */
	@Override
	public List<Rectangle> getRectangles() {
		List<Rectangle> list = new LinkedList<Rectangle>();
		Rectangle rect = new Rectangle(
				(float) (center.getEast() - radius),
				(float) (center.getNorth() + radius ),
				(float) (center.getEast() + radius),
				(float) (center.getNorth() - radius));
		list.add(rect);
		return list;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#getResults()
	 */
	@Override
	public List<GPSkPoint> getResults() {
		return points;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#putData(java.util.ArrayList)
	 */
	@Override
	public void putData(ArrayList<GPSkPoint> data) {
		this.data = data;
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#startNewRectangle()
	 */
	@Override
	public void startNewRectangle() {
		//do nothing
		
	}
	
	@Override
	public String toString(){
		return "CircleProcedure with " + points.size() + " points";
	}

}
