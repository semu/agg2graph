/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.infomatiq.jsi.Rectangle;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;

/**
 * @author Christian Windolf
 *
 */
public class RectangleProcedure implements IndexProcedure<GPSkPoint> {
	
	private final Rectangle rect;
	private final List<GPSkPoint> results = new LinkedList<>();
	private ArrayList<GPSkPoint> data;

	/**
	 * 
	 */
	public RectangleProcedure(double x1, double y1, double x2, double y2) {
		this((float) x1, (float) y1, (float) x2, (float) y2);
	}
	
	public RectangleProcedure(float x1, float y1, float x2, float y2){
		this.rect = new Rectangle(x1, y1, x2, y2);
	}

	/* (non-Javadoc)
	 * @see gnu.trove.TIntProcedure#execute(int)
	 */
	@Override
	public boolean execute(int arg0) {
		results.add(data.get(arg0));
		return true;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#getRectangle()
	 */
	@Override
	public List<Rectangle> getRectangles() {
		List<Rectangle> list = new LinkedList<>();
		list.add(rect);
		return list;
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#getResults()
	 */
	@Override
	public List<GPSkPoint> getResults() {
		return results;
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#putData(java.util.ArrayList)
	 */
	@Override
	public void putData(ArrayList<GPSkPoint> data) {
		this.data = data;
		
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.data.IndexProcedure#startNewRectangle()
	 */
	@Override
	public void startNewRectangle() {
		//do nothing
		
	}

}
