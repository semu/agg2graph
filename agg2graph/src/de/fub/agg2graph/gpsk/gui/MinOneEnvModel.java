/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import java.util.LinkedList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class MinOneEnvModel implements AvailabilitySupport, ListDataListener{
    private static Logger log = Logger.getLogger(MinOneEnvModel.class);
    
    private DropDownModel ddm;
    public MinOneEnvModel(DropDownModel ddm){
        this.ddm = ddm;
        ddm.addListDataListener(this);
    }
    
    @Override
    public boolean isAvailable(){
        return ddm.getSize() != 0;
    }
    
    private LinkedList<AvailabilityListener> listeners = new LinkedList<>();

    @Override
    public void addAvailabilityListener(AvailabilityListener al) {
        listeners.add(al);
        
    }

    @Override
    public void removeAvailabilityListener(AvailabilityListener al) {
        listeners.remove(al);
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
        react(e);
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
        react(e);
        
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
        react(e);
    }
    
    private void react(ListDataEvent e){
        Object o = e.getSource();
        if(!(o instanceof DropDownModel)){
            log.warn("notified about an Event that I can't process");
        } else {
            DropDownModel ddm = (DropDownModel) o;
            boolean b = ddm.getSize() != 0;
            for(AvailabilityListener al : listeners){
                al.updateAvailability(b, this);
            }
        }
    }
    
}
