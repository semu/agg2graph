/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JTabbedPane;

import static java.awt.BorderLayout.PAGE_END;
import static java.awt.BorderLayout.CENTER;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JPanel;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class OptionsDialog extends JDialog{
    private static Logger log = Logger.getLogger(OptionsDialog.class);
    
    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.VERTICAL);
    
    private List<OptionsModel> models = new LinkedList<>();
    
    public OptionsDialog(Class ... optionClasses){
        super(Main.mainWindow, "Options");
        setLayout(new BorderLayout());
        for(Class c : optionClasses){
            try {
                OptionsModel om = new OptionsModel(c);
                KeyValueStore.pChange.addPropertyChangeListener(c.getName(), om);
                JPanel panel = new OptionsPanel(om);
                models.add(om);
                tabbedPane.addTab(c.getSimpleName(), panel);
                
            } catch (Exception ex) {
                log.error("unable to create a real options dialog :(", ex);
            }
            
        }
        add(tabbedPane, CENTER);
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton abort = new JButton("abort");
        abort.addActionListener(new AbortHandler());
        buttonPanel.add(abort);
        
        JButton ok = new JButton("OK");
        ok.addActionListener(new OKHandler());
        buttonPanel.add(ok);
        
        add(buttonPanel, PAGE_END);
        pack();
        
        
        
        
    }
    
    private OptionsDialog outer(){
        return this;
    }
    
    private class AbortHandler implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            for(OptionsModel om : models){
                try {
                    Object o = KeyValueStore.loadOptions(om.getOptionsClass());
                    PropertyChangeEvent pce = new PropertyChangeEvent(outer(), o.getClass().getName(), om.getOptions(), o);
                    om.propertyChange(pce);
                    outer().setVisible(false);
                } catch (Exception ex) {
                    
                }
            }
        }
        
    }
    
    private class OKHandler implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            for(OptionsModel om : models){
                try {
                    KeyValueStore.saveOptions(om.getOptions());
                    outer().setVisible(false);
                } catch (Exception ex) {
                    log.error("unable to save options");
                }
            }
        }
        
    }
}
