/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.tilefactory;

import org.jdesktop.swingx.mapviewer.Tile;
import org.jdesktop.swingx.mapviewer.TileFactory;
import org.jdesktop.swingx.mapviewer.TileFactoryInfo;

/**
 *
 * @author Christian Windolf
 */
public class GoogleMaps extends TileFactoryInfo{

    private final int totalMapzone = 17;
    public GoogleMaps(){
        super(
                0, //min zoom level
                15, //max allowed zoom level
                17, //total mapzone
                256, //tile size
                true,
                true, //orientation normal
           
         "http://mt" + (int) (Math.random() * 3 + 0.5)
                + ".google.com/vt/v=w2.106&hl=de", //base URL
                "x",
                "y",
                "z");
    }
    
    @Override
    public String getTileUrl(int x, int y, int z){
        int zoom = totalMapzone - z;
        return baseURL + "&x=" + x + "&y=" + y + "&z=" + zoom;
    }
    
}
