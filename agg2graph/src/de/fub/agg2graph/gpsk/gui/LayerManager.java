/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.*;
import javax.swing.JMenu;
import javax.swing.JToolBar;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.painter.Painter;

/**
 *
 * @author Christian Windolf
 */
public class LayerManager extends JToolBar implements Painter<JXMapViewer>, Observer {

    private static final LayerManager singleton = new LayerManager();
    private static Logger log = Logger.getLogger(LayerManager.class);

    public static LayerManager getInstance() {
        return singleton;
    }
    private final HashSet<String> keys = new HashSet<String>();

    public LayerManager() {
        super("Layers");
    }
    private final List<Layer> layers = new LinkedList<Layer>();

    @Override
    public void paint(Graphics2D g, JXMapViewer t, int width, int height) {
        g = (Graphics2D) g.create();
        Rectangle rect = t.getViewportBounds();
        g.translate(-rect.x, -rect.y);
        synchronized (this) {
            for (Layer l : layers) {
                if (l.visible()) {
                    l.paint(g, t, width, height);
                }
            }
        }
    }

    public void addLayer(Layer l) {
        synchronized (this) {
            layers.add(l);
        }
    }

    public boolean removeLayer(Layer l) {
        synchronized (this) {
            return layers.remove(l);
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o1 == null) {
            return;
        }
        if (!(o1 instanceof String)) {
            return;
        }
        String key = (String) o1;
        //the layer itself will take care of this issue
        if (keys.contains(key)) {
            return;
        }


        EnvironmentLayer tl = new EnvironmentLayer(key);
        o.addObserver(tl);
        synchronized (this) {
            layers.add(tl);
        }
        keys.add(key);
        add(tl);


        log.debug("new layer: " + key);
        repaint();

        if (!(o instanceof EnvironmentManager)) {
            return;
        }
        EnvironmentManager em = (EnvironmentManager) o;
        em.addObserver(tl);


    }
}
