/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.structs.GPSSegment;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.TileFactory;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class TrackLayer extends DataLayer {

    private List<GPSkSegment> list;

    public TrackLayer(String title) {
        super(title);
        setEnabled(false);
    }

    public TrackLayer(String title, Color c) {
        super(title, true, c);
        setEnabled(false);
    }

    public TrackLayer(String title, Color c, List<GPSkSegment> list) {
        super(title, true, c);
        this.list = list;
    }

    public TrackLayer(String title, List<GPSkSegment> list) {
        super(title);
        this.list = list;
    }

    public void setData(List<GPSkSegment> seg) {
        synchronized (this) {
            list = seg;
        }
        setEnabled(seg != null);


    }

    @Override
    public void paint(Graphics2D g2d, JXMapViewer mapViewer, int width, int height) {
        if (list == null) {
            return;
        }
        g2d.setColor(c);
        int zoom = mapViewer.getZoom();
        TileFactory fac = mapViewer.getTileFactory();
        synchronized (this) {

            for (GPSkSegment segment : list) {

                if (!segment.isEmpty()) {
                    Iterator<GPSkPoint> iter = segment.iterator();

                    GPSkPoint p1 = iter.next();
                    Point2D p2d1 = fac.geoToPixel(p1.gp, zoom);

                    while (iter.hasNext()) {
                        GPSkPoint p2 = iter.next();
                        Point2D p2d2 = fac.geoToPixel(p2.gp, zoom);
                        g2d.drawLine((int) p2d1.getX(), (int) p2d1.getY(), (int) p2d2.getX(), (int) p2d2.getY());
                        p1 = p2;
                        p2d1 = p2d2;
                    }
                }
            }
        }
    }
}
