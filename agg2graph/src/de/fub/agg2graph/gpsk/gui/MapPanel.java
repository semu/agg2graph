/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import static java.io.File.separator;

import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.TileCache;

import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.tilefactory.OpenStreetMap;
import de.fub.agg2graph.structs.GPSSegment;

/**
 *
 * @author Christian Windolf
 */
public class MapPanel extends JXMapViewer implements Observer {

    private static Logger log = Logger.getLogger(MapPanel.class);

    static {
        log.setLevel(Level.WARN);
    }
    private static final int xSpace = 200, ySpace = 200;
    private final int xResolution, yResolution;
    private List<GPSSegment> segments = null;
    private static final GeoPosition startPostion = new GeoPosition(52.455614, 13.297034);
    private final LayerManager layers = LayerManager.getInstance();
    public final ActionListener repaintIt = new MapRefresher();

    public MapPanel() {
        super();
        GraphicsEnvironment gEnv =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gDevice = gEnv.getDefaultScreenDevice();
        DisplayMode dMode = gDevice.getDisplayMode();
        xResolution = dMode.getWidth() - xSpace;
        yResolution = dMode.getHeight() - ySpace;
        setPreferredSize(new Dimension(xResolution, yResolution));


        DefaultTileFactory tf = new DefaultTileFactory(new OpenStreetMap());
        tf.setTileCache(new DiskTileCache());
        setTileFactory(tf);
        setZoom(1);
        setOverlayPainter(layers);

        setCenterPosition(startPostion);

    }

    public List<GPSSegment> getSegments() {
        return segments;
    }

    /**
     * Expects, that this method gets invoked by an {@link EnvironmentManager}.
     * By that, it expects, that the first parameter is an instance of
     * {@link EnvironmentManager} and the second is an instance of String. If it
     * is not so, the method will just return and does nothing.
     *
     * @param o The environment manager
     * @param o1 the key of the dataset that has changed. It will only react, if
     * this key equivalent to the RAW_DATA key in the {@link EnvironmentManager}
     * class.
     */
    @Override
    public void update(Observable o, Object o1) {
        if (o == null || o1 == null) {
            return;
        }
        if (!(o instanceof EnvironmentManager)) {
            return;
        }
        if (!(o1 instanceof String)) {
            return;
        }
        String key = (String) o1;

        EnvironmentManager em = (EnvironmentManager) o;
        if(em.getEnv(key) == null){
        	return;
        }
        if(em.getMeta(key).center){
            setCenterPosition(em.getEnv(key).getCenter().gp);
        }
        

        repaint();
    }

    public class MapRefresher implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            repaint();
        }
    }

    /**
     * Class is mostly copied from a board entry on java.net
     *
     * @see <a href="http://www.java.net/node/665066">link</a>
     */
    private class DiskTileCache extends TileCache {

        private final File cacheDir;
        //cache size: 250MB
        private final long maxCacheSize = 250 * 1024 * 1024;
        private final int maxAge = 30;

        private DiskTileCache(File f) {
            this.cacheDir = f;
            if (!cacheDir.isDirectory()) {
                cacheDir.mkdirs();
            }
        }

        private DiskTileCache() {
            File localFolder = ResourceManager.localFolder;
            cacheDir = new File(localFolder.getAbsolutePath() + separator + "tiles");
            if (!cacheDir.isDirectory()) {
                cacheDir.mkdirs();
            }
        }

        @Override
        public void put(URI pURI, byte[] pImgBytes, BufferedImage pImage) {
            super.put(pURI, pImgBytes, pImage);
            synchronized (this) {
                putImageData(pURI.toString(), pImgBytes);
            }
        }

        @Override
        public BufferedImage get(URI pURI) throws IOException {
            BufferedImage lImage = super.get(pURI);
            synchronized (this) {
                if (lImage == null && containsImageURI(pURI)) {
                    byte[] lImageBytes = getImageData(pURI);
                    lImage = ImageIO.read(new ByteArrayInputStream(lImageBytes));
                    super.put(pURI, lImageBytes, lImage);
                    return lImage;
                }
            }
            return lImage;
        }

        /**
         * Test if file exists based on given URI
         */
        private boolean containsImageURI(URI pFullImageURI) {
            File lFile = new File(cacheDir, toMD5Hash(pFullImageURI.toString()));
            return lFile.isFile() && !isTooOld(lFile, maxAge);
        }

        public byte[] getImageData(URI pFullImageURI) throws IOException {
            // return byte array with compressed image contents
            if (containsImageURI(pFullImageURI)) {
                File lFile = new File(cacheDir, toMD5Hash(pFullImageURI.toString()));
                log.debug("Load cached file: " + lFile);
                return copyFile(lFile);
            }
            return null;
        }

        public void putImageData(String pFullImageUrlName, byte[] pData) {

            if (log.isDebugEnabled()) {
                log.debug("Caching: " + pFullImageUrlName + ", size: " + pData.length);
            }

            File lFile = new File(cacheDir, toMD5Hash(pFullImageUrlName));
            try {
                copyFile(pData, lFile);
            } catch (IOException e) {
                log.warn("Error saving file " + lFile, e);
            }
        }

        /**
         * Copy byte array to geven file
         */
        public void copyFile(byte[] inFile, File outFile) throws IOException {

            FileOutputStream lStream = null;
            try {
                lStream = new FileOutputStream(outFile);
                BufferedOutputStream os = new BufferedOutputStream(lStream);
                os.write(inFile, 0, inFile.length);
                os.close();
            } finally {
                if (lStream != null) {
                    try {
                        lStream.close();
                    } catch (Exception e) {
                        //do nothing
                    }
                }
            }
        }

        /**
         * copy file to byte array
         */
        public byte[] copyFile(File pInFile) throws IOException {

            FileInputStream lStream = null;
            try {
                lStream = new FileInputStream(pInFile);
                ByteArrayOutputStream lOutFile = new ByteArrayOutputStream();
                BufferedInputStream is = new BufferedInputStream(lStream);
                BufferedOutputStream os = new BufferedOutputStream(lOutFile);

                //Read and write until done
                byte[] buf = new byte[4096];
                int len;

                //Buffercopy
                while ((len = is.read(buf)) >= 0) {
                    os.write(buf, 0, len);
                }

                is.close();
                os.close();
                return lOutFile.toByteArray();

            } finally {
                if (lStream != null) {
                    try {
                        lStream.close();
                    } catch (IOException e) {
                        //do nothing
                    }
                }
            }
        }

        /**
         * Test if given file is older then maxAge
         *
         * @return Return true if older
         */
        public boolean isTooOld(File pFile, int pMaxAgeInDays) {

            long lNow = System.currentTimeMillis();
            long lHistory = lNow - 1000L * 60 * 60 * 24 * pMaxAgeInDays;
            return (pFile.lastModified() < lHistory);
        }

        /**
         * Encode string MD5, results in HEX
         */
        public String toMD5Hash(String pKey) {

            String lRestring = "";
            try {
                MessageDigest lMessageDigest = MessageDigest.getInstance("MD5");
                lMessageDigest.update(pKey.getBytes());
                byte lResult[] = lMessageDigest.digest();
                lRestring = toHexCode(lResult);
            } catch (NoSuchAlgorithmException e) {
                log.error("No such algorithm", e);
            }
            return lRestring;
        }

        /**
         * Convert byte array to hex code equivalent
         */
        private String toHexCode(byte pByteArray[]) {

            StringBuffer lBuffer = new StringBuffer(pByteArray.length * 2);
            for (byte lHash : pByteArray) {
                if ((lHash & 0xff) < 16) {
                    lBuffer.append("0");
                }
                lBuffer.append(Long.toString(lHash & 0xff, 16));
            }

            return lBuffer.toString().toUpperCase();
        }
    }
}
