/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 *
 * @author Christian Windolf
 */
public abstract class SmartField extends JTextField {

    protected final JLabel descriptionLabel;

    public SmartField(String text, JLabel label) {
        super(text);
        this.descriptionLabel = label;
    }

    public boolean check() {
        if (checkValue()) {
            setBackground(Color.WHITE);


            return true;
        } else {
            Container comp = getParent();
            while (!(comp instanceof Component)) {
                comp = comp.getParent();
            }
            setBackground(Color.YELLOW);
            showMessageDialog((Component) comp, getErrorMessage(), "Error", ERROR_MESSAGE);
            return false;
        }

    }

    protected String getErrorMessage() {
        return "The input in the field " + descriptionLabel.getText() + " is invalid!";
    }

    protected abstract boolean checkValue();
}
