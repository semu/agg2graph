/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.components;

import de.fub.agg2graph.gpsk.gui.PreferenceWindow;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author Christian Windolf
 */
public class ApproveButton extends JButton implements ActionListener{
    
    private final PreferenceWindow parent;
    
    public ApproveButton(PreferenceWindow parent){
        this(parent, "OK");
    }
    
    public ApproveButton(PreferenceWindow parent, String text){
        super(text);
        this.parent = parent;
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        parent.setVisible(!parent.save());
    }
    
}
