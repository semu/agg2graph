/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.components;

import javax.swing.JLabel;

import static java.lang.String.valueOf;
import static java.lang.Double.parseDouble;

/**
 *
 * @author Christian Windolf
 */
public class DoubleField extends SmartField {

    private final boolean checkBounds;
    private final double minValue;
    private final double maxValue;
    private boolean isNumber = true;
    private boolean isInBounds = true;

    public DoubleField(double value, JLabel descLabel) {
        super(valueOf(value), descLabel);
        checkBounds = false;
        minValue = Double.MIN_VALUE;
        maxValue = Double.MAX_VALUE;
    }

    public DoubleField(double value, JLabel descLabel, double minV, double maxV) {
        super(valueOf(value), descLabel);
        checkBounds = true;
        minValue = minV;
        maxValue = maxV;
    }

    @Override
    protected boolean checkValue() {
        try {
            double x = parseDouble(getText());
            isNumber = true;
            if (checkBounds) {
                if (x < minValue) {
                    isInBounds = false;
                    return false;
                } else if (x > maxValue) {
                    isInBounds = false;
                    return false;
                } else {
                    isInBounds = true;
                    return true;
                }
            } else {
                return true;
            }
        } catch (NumberFormatException ex) {
            isNumber = false;
            return false;
        }
    }
    
    public double getDouble(){
        return parseDouble(getText());
    }

    @Override
    protected String getErrorMessage() {
        if (!isNumber) {
            return "The field " + descriptionLabel.getText() + " accepts only numeric values";
        } else {
            return "The value in the field " + descriptionLabel.getText() + " is out of bounds! <br />"
                    + "must be a value between " + minValue + " and " + maxValue;
        }

    }
}
