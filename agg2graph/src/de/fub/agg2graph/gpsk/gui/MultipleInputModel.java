/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.algorithms.MultipleInputAlgorithm;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.data.Filter;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class MultipleInputModel {
    private static Logger log = Logger.getLogger(MultipleInputModel.class);
    
    private List<DropDownModel> modelList;

    private MultipleInputAlgorithm exampleInstance;

    public MultipleInputModel(Class c) {
        try {
            Constructor constructor = c.getConstructor();
            exampleInstance = (MultipleInputAlgorithm) constructor.newInstance();
            int size = exampleInstance.getAmountOfInputs();
            modelList = new ArrayList<> (size);
            for(int i = 0; i < size; i++){
                Filter f = exampleInstance.getFilter(i);
                DropDownModel ddm = new DropDownModel(f);
                EnvironmentManager.getInstance().addObserver(ddm);
                modelList.add(i, ddm);
            }
        } catch (Exception ex) {
            log.error("For the class " + c + " I cannot create an instance :(", ex);
        }

    }

    public DropDownModel getModelAt(int i) {
        return modelList.get(i);
    }

    public int getAmountOfInputs() {
        return modelList.size();
    }
    
    public String getNameAt(int i){
        return exampleInstance.getName(i);
    }
    
    public String getName(){
        return exampleInstance.getClass().getSimpleName();
    }
}
