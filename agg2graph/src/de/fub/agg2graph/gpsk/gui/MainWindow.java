/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.algorithms.CDEvaluation;
import de.fub.agg2graph.gpsk.gui.actions.MInputAction;
import de.fub.agg2graph.gpsk.gui.actions.OpenGPXAction;
import de.fub.agg2graph.gpsk.gui.actions.FindCrossroadsAction;
import de.fub.agg2graph.gpsk.gui.actions.DataManagerAction;
import de.fub.agg2graph.gpsk.gui.actions.AboutAction;
import de.fub.agg2graph.gpsk.gui.actions.RunAlgorithmAction;
import de.fub.agg2graph.gpsk.algorithms.BoundaryCRRemover;
import de.fub.agg2graph.gpsk.algorithms.CleaningOptions;
import de.fub.agg2graph.gpsk.algorithms.DBSCAN;
import de.fub.agg2graph.gpsk.algorithms.DBSCANEval;
import de.fub.agg2graph.gpsk.algorithms.EvalScript;
import de.fub.agg2graph.gpsk.algorithms.Evaluator2;
import de.fub.agg2graph.gpsk.algorithms.NormalizeOptions;
import de.fub.agg2graph.gpsk.algorithms.Normalizer;
import de.fub.agg2graph.gpsk.algorithms.PolygonCrossroad;
import de.fub.agg2graph.gpsk.algorithms.RDPOptions;
import de.fub.agg2graph.gpsk.algorithms.Splitter;
import de.fub.agg2graph.gpsk.algorithms.SplitterOptions;
import de.fub.agg2graph.gpsk.algorithms.optics.SteepClustering;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.data.MessageBus;
import de.fub.agg2graph.gpsk.eval.TrackCollection;
import de.fub.agg2graph.gpsk.gui.actions.*;

import de.fub.agg2graph.gpsk.gui.optics.RunOPTICSAction;
import de.fub.agg2graph.gpsk.gui.optics.ShowResultsAction;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.*;


import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.PAGE_START;
import static java.awt.BorderLayout.SOUTH;

/**
 * Main Window for the GPSk-application
 *
 * @author Christian Windolf
 */
public class MainWindow extends JFrame{

    private final Action openGPX = new OpenGPXAction();
    private final Action saveGPX = new SaveGPXAction();
    private final Action aboutAction = new AboutAction();
    private final Action showOPTICSOptionsAction = new OPTICSOptions();
    private final Action opticsAction = new RunOPTICSAction();
    private final FindCrossroadsAction findCrossraodsAction = new FindCrossroadsAction();
    private final ShowResultsAction opticsResultsAction = new ShowResultsAction();
    
    private final LayerManager layerManager = LayerManager.getInstance();
    
    private final JDialog opticsOptions = new de.fub.agg2graph.gpsk.gui.optics.Options();
    
    private final MapPanel mapPanel = new MapPanel();
    
    private final StatusBar statusBar = new StatusBar();
    public MainWindow() {
        super("GPSk");
        setLayout(new BorderLayout());
        setJMenuBar(new MenuBar());
        add(mapPanel, CENTER);
        EnvironmentManager em = EnvironmentManager.getInstance();
        em.addObserver(mapPanel);
        layerManager.add(opticsResultsAction);
        add(layerManager, PAGE_START);
        add(statusBar, SOUTH);
        pack();
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        em.addObserver(opticsResultsAction);
        em.addObserver(findCrossraodsAction);
        
        MessageBus.getInstance(MessageBus.GPSK).addBusListener(statusBar);
        
    }



    protected class MenuBar extends JMenuBar {

        //File Menu Begin
        private final JMenu fileMenu = new JMenu("File");
        private final JMenuItem openFile = new JMenuItem(openGPX);
        private final JMenuItem saveFile = new JMenuItem(saveGPX);
        //File Menu End
        
        
        
        //Tools menu Begin
        private final JMenu toolsMenu = new JMenu("Tools");
        private final JMenu crossroadMenu = new JMenu("Crossroads");
        private final JMenu opticsMenu = new JMenu("OPTICS");
        private final JMenuItem opticsOptionsItem = new JMenuItem(showOPTICSOptionsAction);
        private final JMenuItem runOPTICSMenuItem = new JMenuItem(opticsAction);
       
//        private final JMenuItem runDBSCANItem = new JMenuItem(new DBSCANAction());
        private final JMenuItem findCrossroadsItem = new JMenuItem(findCrossraodsAction);
        
        private final JMenuItem openDataManager = new JMenuItem(new DataManagerAction());
        private final JMenuItem runNormalizer = new JMenuItem(new RunAlgorithmAction(Normalizer.class));
        private final JMenuItem runPGCrossroad = new JMenuItem(new RunAlgorithmAction(PolygonCrossroad.class));
        private final JMenuItem runDBSCAN = new JMenuItem(new RunAlgorithmAction(DBSCAN.class));
        private final JMenuItem runSplitter = new JMenuItem(new MInputAction(Splitter.class));
        private final JMenuItem runCrossroad2 = new JMenuItem(new RunAlgorithmAction(SteepClustering.class));
        private final JMenuItem evalCrossRoads = new JMenuItem(new MInputAction(CDEvaluation.class));
        private final JMenuItem bcr = new JMenuItem(new RunAlgorithmAction(BoundaryCRRemover.class));
        private final JMenuItem eval2 = new JMenuItem(new MInputAction(Evaluator2.class));
        private final JMenuItem evalScript = new JMenuItem(new MInputAction(EvalScript.class));
        private final JMenuItem DBSCANeval = new JMenuItem(new RunAlgorithmAction(DBSCANEval.class));
        private final JMenuItem showOptions = new JMenuItem(new ShowDialogAction(
                new OptionsDialog(SplitterOptions.class, CleaningOptions.class, RDPOptions.class, NormalizeOptions.class), "Options"));
        //tools Menu End
        
        //Help Menu Begin
        private final JMenu helpMenu = new JMenu("Help");
        private final JMenuItem aboutItem = new JMenuItem(aboutAction);
        //Help Menu End

        MenuBar() {
            super();
            add(fileMenu);
            fileMenu.add(openFile);
            fileMenu.add(saveFile);
            add(toolsMenu);
            toolsMenu.add(crossroadMenu);
            crossroadMenu.add(findCrossroadsItem);
            toolsMenu.add(opticsMenu);
            opticsMenu.add(opticsOptionsItem);
            opticsMenu.add(runOPTICSMenuItem);
            toolsMenu.add(openDataManager);
            toolsMenu.add(runNormalizer);
            toolsMenu.add(runPGCrossroad);
            toolsMenu.add(runDBSCAN);
            toolsMenu.add(runSplitter);
            toolsMenu.add(runCrossroad2);
            toolsMenu.add(showOptions);
            toolsMenu.add(evalCrossRoads);
            toolsMenu.add(bcr);
            toolsMenu.add(evalScript);
            toolsMenu.add(DBSCANeval);
            toolsMenu.add(eval2);
            
            
            add(helpMenu);
            helpMenu.add(aboutItem);
        }
    }

    public MapPanel getMapPanel() {
        return mapPanel;
    }
    
    public StatusBar getStatusBar(){
        return statusBar;
    }
    

    
    
    
    private class OPTICSOptions extends AbstractAction{
        
        OPTICSOptions(){
            super("OPTICS options");
            putValue(SHORT_DESCRIPTION, "set up input parameters for OPTICS");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            opticsOptions.setVisible(true);
        }
        
    }
    
   
    
    
}
