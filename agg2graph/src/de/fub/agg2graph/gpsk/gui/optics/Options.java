/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.optics;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.gui.components.AbortButton;
import de.fub.agg2graph.gpsk.gui.components.ApproveButton;
import de.fub.agg2graph.gpsk.gui.PreferenceWindow;
import java.awt.GridLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import static de.fub.agg2graph.gpsk.algorithms.optics.Algorithm.DEFAULT_EPS;
import static de.fub.agg2graph.gpsk.algorithms.optics.Algorithm.DEFAULT_MINPTS;
import static de.fub.agg2graph.gpsk.algorithms.optics.Algorithm.EPSKEY;
import static de.fub.agg2graph.gpsk.algorithms.optics.Algorithm.MINPTSKEY;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;


import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author Christian Windolf
 */
public class Options extends JDialog implements PreferenceWindow {

    private final JLabel epsLabel = new JLabel("epsilon");
    private final JTextField epsField;
    private final String epsToolTipText = "Maximum allowed reachability distance."
            + "<br /> Reduce this to save CPU time. "
            + "<br /> Value in meters";
    private final JLabel minPtsLabel = new JLabel("minimum Points");
    private final JTextField minPtsField;
    private final String minPtsToolTipText = " Minimum amount of points that"
            + "must be within the epsilon distance";
    
    private final JButton cancelButton;
    private final JButton okButton;

    public Options() {
        super(Main.mainWindow, "OPTICS Options");
        setLayout(new GridLayout(3, 2, 10, 10));
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        String epsString = KeyValueStore.getValue(EPSKEY, valueOf(DEFAULT_EPS));
        String mintPtsString = KeyValueStore.getValue(MINPTSKEY, valueOf(DEFAULT_MINPTS));

        add(epsLabel);
        epsField = new JTextField(epsString);
        add(epsField);
        epsLabel.setToolTipText(epsToolTipText);
        epsField.setToolTipText(epsToolTipText);

        add(minPtsLabel);
        minPtsField = new JTextField(mintPtsString);
        add(minPtsField);
        minPtsLabel.setToolTipText(minPtsToolTipText);
        minPtsField.setToolTipText(minPtsToolTipText);
        
        cancelButton = new AbortButton(this);
        add(cancelButton);
        okButton = new ApproveButton(this);
        add(okButton);
        pack();
    }

    @Override
    public boolean save() {
        try {
            double eps = parseDouble(epsField.getText());
            
            if(eps <= 0){
                showMessageDialog(this, "The epsilon field must contain a value greater than zero!");
                return false;
            }
            
        } catch (NumberFormatException ex) {
            showMessageDialog(this, "You did not type a double into the epsilon field");
            return false;
        }
        
        try{
            int minPts = parseInt(minPtsField.getText());
            if(minPts <= 0){
                showMessageDialog(this, "The value in the minimum points field must be greater than zero");
                return false;
            }
        } catch (NumberFormatException ex){
            showMessageDialog(this, "The value in the minPts-Field is not a number!");
            return false;
        }
        KeyValueStore.putValue(EPSKEY, epsField.getText());
        KeyValueStore.putValue(MINPTSKEY, minPtsField.getText());
        return true;
    }
}