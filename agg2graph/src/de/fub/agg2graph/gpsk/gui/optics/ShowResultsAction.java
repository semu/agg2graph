/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.optics;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.PreferenceWindow;
import de.fub.agg2graph.gpsk.gui.components.AbortButton;
import de.fub.agg2graph.gpsk.gui.components.ApproveButton;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.*;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JDialog;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.TYPE;
import javax.swing.*;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class ShowResultsAction extends AbstractAction implements Observer {

    private final HashMap<String, ResultViewer> viewer = new HashMap<>();
    MutableComboBoxModel<ResultViewer> model = new DefaultComboBoxModel<>();

    public ShowResultsAction() {
        super("show OPTICS results");
        EnvironmentManager em = EnvironmentManager.getInstance();
        setEnabled(false);

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        new SourceChooser();

    }

    @Override
    public void update(Observable o, Object o1) {
        if (o == null) {
            return;
        }
        if (!(o instanceof EnvironmentManager)) {
            return;
        }
        if (o1 == null) {
            return;
        }
        if (!(o1 instanceof String)) {
            return;
        }

        EnvironmentManager em = (EnvironmentManager) o;
        String key = (String) o1;

        
        if (em.contains(key)) {
            if ("optics".equals(em.getMeta(key).keyValue.get(TYPE))) {
                model.addElement(new ResultViewer(key));
                setEnabled(true);
            }
        } else {
            ResultViewer rv = viewer.get(key);
            if(rv != null){
                model.removeElement(rv);
            }
            viewer.remove(key);
            if(viewer.isEmpty()){
                setEnabled(false);
            }
        }
    }

    private class SourceChooser extends JDialog implements PreferenceWindow {

        JComboBox<ResultViewer> dropDown;
        

        SourceChooser() {
            super(Main.mainWindow, "select results");
            
            setLayout(new GridLayout(1, 3));
            dropDown = new JComboBox<>(model);
            add(dropDown);
            add(new ApproveButton(this));
            add(new AbortButton(this));
            pack();
            setVisible(true);
        }

        @Override
        public boolean save() {
            ((ResultViewer)dropDown.getSelectedItem()).setVisible(true);
            return true;
        }
    }
    
   
    
    
}
