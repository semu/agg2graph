/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.optics;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.ResourceManager;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;

import de.fub.agg2graph.gpsk.algorithms.optics.OPTICSPoint;
import de.fub.agg2graph.structs.GPSPoint;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;


import org.apache.log4j.Logger;
import org.jdesktop.swingx.mapviewer.GeoPosition;


import java.util.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import static javax.swing.JScrollPane.VERTICAL_SCROLLBAR_NEVER;
import static javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS;
import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.NORTH;



/**
 *
 * @author Christian Windolf
 */
public class ResultViewer extends JDialog {

    private static Logger log = Logger.getLogger(ResultViewer.class);
    private static final int MAX_HEIGHT = 500;
    private static final int UNDEFINED_HEIGHT = 600;
    private final List<DiagramBar> output;
    private final double epsilon;
    private final int minPts;
    private final JPanel canvasPanel;
    private final JScrollPane scrollPane;
    private final Action repaintAction;
    private static Icon onIcon = null;
    private static Icon offIcon = null;

    private final double MAX_DISTANCE_TO_CROSSROAD = 50d;
    private final GPSkEnvironment env;
    private final GPSkEnvironment.MetaInformation metaInformation;
    protected final String key;

    public ResultViewer(String key) {
        super(Main.mainWindow, "OPTICS results");
        this.key = key;
        env = EnvironmentManager.getInstance().getEnv(key);
        metaInformation = env.getMeta();


        setLayout(new BorderLayout());
//        setTitle("OPTICS: results");

        this.epsilon = Double.parseDouble(metaInformation.keyValue.get("eps"));
        this.minPts = Integer.parseInt(metaInformation.keyValue.get("minPts"));
        

        repaintAction = new RepaintAction();


        

        output = new ArrayList<DiagramBar>(env.getPoints().size());
        Iterator<GPSkPoint> outputIterator = env.getPoints().iterator();
        int index = 0;
        while (outputIterator.hasNext()) {
            GPSkPoint p = outputIterator.next();
            DiagramBar bar = new DiagramBar((OPTICSPoint) p);
            output.add(index++, bar);
        }

        canvasPanel = new CanvasPanel();
        scrollPane = new JScrollPane(canvasPanel, VERTICAL_SCROLLBAR_NEVER, HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);

        add(scrollPane, CENTER);

        pack();
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        repaint();
    }
    
    @Override
    public String toString(){
        return metaInformation.name + " (" + key + ")";
    }



    private class CanvasPanel extends JPanel {

        DecimalFormat format = new DecimalFormat(",##0.0##");
        private final int width;

        CanvasPanel() {
            width = output.size();
            setPreferredSize(new Dimension(width + 250, 800));
            setBackground(Color.WHITE);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponents(g);
            Graphics2D g2d = (Graphics2D) g;
            Font scaleFont = new Font("SansSerif", Font.PLAIN, 20);
            g2d.setFont(scaleFont);
            FontMetrics scaleFontMetrics = g2d.getFontMetrics(scaleFont);
            Graphics2D g2d2 = (Graphics2D) g2d.create(200, 150, width, 600);
            paintDiagramm(g2d2);

            int undefinedStringWidth = scaleFontMetrics.stringWidth("UNDEFINED");
            g2d.drawString("UNDEFINED", 20, 150);

            double fithEpsilon = epsilon / 5d;
            for (int i = 0; i <= 5; i++) {
                double value = i * fithEpsilon;
                String scaleString = format.format(value);
                int leftEdge = (20 + undefinedStringWidth) - scaleFontMetrics.stringWidth(scaleString);
                g2d.drawString(scaleString, leftEdge, 750 - (i * 100));
            }

        }

        private void paintDiagramm(Graphics2D g2d) {

            g2d.setColor(Color.RED);
            for (int i = 0; i < output.size(); i++) {

                int height = output.get(i).height;
                g2d.setColor(output.get(i).getColor());

                g2d.fillRect(i, 600 - height, 1, height);
            }
            g2d.setColor(Color.BLACK);
            for (int i = 0; i < 6; i++) {
                g2d.drawLine(0, i * 100, g2d.getClipBounds().width, i * 100);
            }

        }
    }

    private class RepaintAction extends AbstractAction {

        private Logger log = Logger.getLogger(RepaintAction.class);

        RepaintAction() {
            super("Repaint Diagram");
            putValue(SHORT_DESCRIPTION, "repaints the diagram. You'll see: you'll need it");
            URL url = ResourceManager.getURL("/icons/refresh.png");
            try {
                Icon icon = new ImageIcon(ImageIO.read(url));
                putValue(LARGE_ICON_KEY, icon);
            } catch (IOException ex) {
                log.warn("failed to load refreshing icon. Nothing serious, GUI just does not look so pretty");

            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            repaint();
        }
    }

    private class Toolbar extends JPanel {

        Toolbar(Action... actions) {
            super(new FlowLayout(FlowLayout.LEFT));
            for (int i = 0; i < actions.length; i++) {
                add(new JButton(actions[i]));
            }
        }
    }

    private class DiagramBar {

        public final int height;
        private Color color = Color.RED;

        public DiagramBar(OPTICSPoint p) {
            if (p.isUndefined()) {
                height = UNDEFINED_HEIGHT;
            } else {
                double rel = p.getReachabilityDistance()/ epsilon;
                height = (int) ((double) MAX_HEIGHT * rel);
            }

        }

        public void setColor(Color color) {
            synchronized (this) {
                this.color = color;
            }
        }

        public Color getColor() {
            synchronized (this) {
                return color;
            }
        }
    }

    /**
     * only for test purpose
     *
     * @param args
     */
    public static void main(String[] args) {
        List<OPTICSPoint> output = new LinkedList<OPTICSPoint>();
        output.add(createPoint(Double.POSITIVE_INFINITY));
        output.add(createPoint(Double.POSITIVE_INFINITY));
        output.add(createPoint(Double.POSITIVE_INFINITY));
        output.add(createPoint(48d));
        output.add(createPoint(44d));
        output.add(createPoint(44d));
        output.add(createPoint(45d));
        output.add(createPoint(40d));
        output.add(createPoint(28d));
        output.add(createPoint(38d));
        output.add(createPoint(42d));
        output.add(createPoint(40d));
        output.add(createPoint(35d));
        output.add(createPoint(34d));
        output.add(createPoint(33d));
        output.add(createPoint(32d));
        output.add(createPoint(31d));
        output.add(createPoint(25d));
        output.add(createPoint(24d));
        output.add(createPoint(12d));
        output.add(createPoint(13d));
        output.add(createPoint(14d));
        output.add(createPoint(15d));
        output.add(createPoint(16d));
        output.add(createPoint(17d));
        output.add(createPoint(Double.POSITIVE_INFINITY));
        output.add(createPoint(Double.POSITIVE_INFINITY));
        output.add(createPoint(Double.POSITIVE_INFINITY));
        output.add(createPoint(18d));
//        new ResultViewer(new OPTICSResults(output, 30, 50));
    }

    /**
     * debugging/test purpose
     *
     * @param rd
     * @return
     */
    private static OPTICSPoint createPoint(double rd) {
        OPTICSPoint p = new OPTICSPoint(new GPSPoint(53d, 13d));
        p.setReachabilityDistance(rd);
        return p;
    }

    
}
