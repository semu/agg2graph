/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.optics;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.PreferenceWindow;
import de.fub.agg2graph.gpsk.gui.components.AbortButton;
import de.fub.agg2graph.gpsk.gui.components.ApproveButton;
import de.fub.agg2graph.gpsk.algorithms.optics.Algorithm;
import de.fub.agg2graph.gpsk.algorithms.optics.OPTICSPoint;

import de.fub.agg2graph.structs.GPSSegment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.*;
import javax.swing.*;

import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.ERROR_MESSAGE;


import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.TYPE;
import static de.fub.agg2graph.gpsk.gui.StatusBar.OPTICS_RUNNING;

/**
 *
 * @author Christian Windolf
 */
public class RunOPTICSAction extends AbstractAction {
    
    public RunOPTICSAction(){
        super("run OPTICS");
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            Dialog d = new Dialog();
        } catch (IllegalArgumentException ex) {
            showMessageDialog(Main.mainWindow, "Load a file before starting to cluster", "OPTICS",
                    ERROR_MESSAGE);

        }
    }


    private class Dialog extends JDialog implements PreferenceWindow {

        private final JLabel inputLabel = new JLabel("input");
        private final InputList inputList;
        private final JLabel outputLabel = new JLabel("output");
        private final JTextField outputField = new JTextField("optics");
        private final JButton okButton;
        private final JButton abortButton;

        Dialog() throws IllegalArgumentException {
            super(Main.mainWindow, "OPTICS");
            setLayout(new GridLayout(2, 3));
            add(inputLabel);
            add(outputLabel);
            abortButton = new AbortButton(this);
            add(abortButton);
            inputList = InputList.build();
            add(inputList);
            add(outputField);
            okButton = new ApproveButton(this);
            add(okButton);
            pack();
            setVisible(true);

        }

        @Override
        public boolean save() {
            Algorithm runner = new Algorithm(
                    (String) inputList.getSelectedItem(), outputField.getText());
            GPSkEnvironment env = EnvironmentManager.getInstance().getEnv((String) inputList.getSelectedItem());
            
            Main.executor.execute(runner);
            
            return true;
        }
    }

    private static class InputList extends JComboBox {

        private InputList(String[] layers) {
            super(layers);
        }

        private static InputList build() {
            EnvironmentManager em = EnvironmentManager.getInstance();
            Set<String> keys = em.keys();
            if (keys.size() == 0) {
                throw new IllegalArgumentException("no input tracks");
            }
            int countOfOPTICS = 0;
            Iterator<String> iter = keys.iterator();
            List<String> keyList = new LinkedList<>();
            while (iter.hasNext()) {
                String key = iter.next();

                if (!"optics".equals(em.getMeta(key).keyValue.get(TYPE))) {
                    keyList.add(key);
                }
            }


            String[] array = new String[keyList.size()];
            Iterator<String> it = keyList.iterator();
            for (int i = 0; i < array.length && it.hasNext(); i++) {
                array[i] = it.next();
            }
            return new InputList(array);
            
        }
    }
}
