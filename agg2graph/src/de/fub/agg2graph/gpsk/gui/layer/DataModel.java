/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.layer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;

/**
 * @author Christian Windolf
 *
 */
public class DataModel implements  Observer{

	private final HashMap<String, EnvController> controllers = new HashMap<>();
	private final ControllerFactory factory = ControllerFactory.getInstance();

	/**
	 * 
	 */
	public DataModel() {
	}

	
	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {
		if(!(o instanceof EnvironmentManager)){
			return;
		}
		EnvironmentManager em = (EnvironmentManager) o;
		if(!(arg instanceof String)){
			return;
		}
		String key = (String) arg;
		
		EnvController old = controllers.get(key);
		if(old != null){
			old.destroy();
		}
		
		GPSkEnvironment env = em.getEnv(key);
		EnvController c = factory.createController(env);
		controllers.put(key, c);
		
	}

}
