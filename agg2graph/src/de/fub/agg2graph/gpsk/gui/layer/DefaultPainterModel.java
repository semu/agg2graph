/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.layer;

import de.fub.agg2graph.gpsk.beans.BoundedPropertyChange;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class DefaultPainterModel implements PainterModel, ColorChangeable, TransparencyChangeable, BoundedPropertyChange{
    private List<PainterChangedListener> listener = new LinkedList<>();
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    

    private DefaultPainter painter;
    
    
    private boolean displayable = true;
    
    public DefaultPainterModel(GPSkEnvironment env){
        painter = new DefaultPainter(env.getSegments(), env.getPoints());
    }

    @Override
    public Painter getPainter() {
        return painter;
    }

    @Override
    public void setPainter(Painter painter) {
        //do nothing
    }

    @Override
    public String getTitle() {
//        return title;
    	return null;
    }

    @Override
    public void setTitle(String title) {
        
    }

    @Override
    public void setDisplayable(boolean displayable) {
        this.displayable = displayable;
        for(PainterChangedListener pcl : listener){
            pcl.visibilityChanged(this, displayable);
        }
    }

    @Override
    public boolean isDisplayable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addDisplayableListener(PainterChangedListener dl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeDisplayableListener(PainterChangedListener dl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Color getColor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setColor(Color c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float getTransparency() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTransparency(float x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        pcs.addPropertyChangeListener(pcl);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        pcs.removePropertyChangeListener(pcl);
    }
    
}
