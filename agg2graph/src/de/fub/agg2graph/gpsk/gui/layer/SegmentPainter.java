/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.layer;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;
import org.jdesktop.swingx.mapviewer.TileFactory;

import static java.lang.Math.round;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class SegmentPainter extends PainterUtility<List<GPSkSegment>>{

    public SegmentPainter() {
    }

    public SegmentPainter(int thickness, Color c, float transparency) {
        super(thickness, c, transparency);
    }

    public SegmentPainter(int thickness) {
        super(thickness);
    }

    public SegmentPainter(Color color) {
        super(color);
    }

    public SegmentPainter(float transparency) {
        super(transparency);
    }

    public SegmentPainter(int thickness, Color c) {
        super(thickness, c);
    }

    public SegmentPainter(Color c, float transparency) {
        super(c, transparency);
    }

    
    @Override
    public void paint(PaintingArguments pa, List<GPSkSegment> data) {
        pa.g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, transparency));
        pa.g2d.setColor(color);
        pa.g2d.setStroke(new BasicStroke(thickness));
        int zoom = pa.mapViewer.getZoom();
        TileFactory fac = pa.mapViewer.getTileFactory();
        for(GPSkSegment seg : data){
            if(seg.size() > 1) {
                Iterator<GPSkPoint> iter = seg.iterator();
                GPSkPoint first = iter.next();
                Point2D p1 = fac.geoToPixel(first.gp, zoom);
                do{
                    GPSkPoint second = iter.next();
                    Point2D p2 = fac.geoToPixel(second.gp, zoom);
                    pa.g2d.drawLine(
                            (int) round(p1.getX()),
                            (int) round(p1.getY()),
                            (int) round(p2.getX()), 
                            (int) round(p2.getY()));
                    first = second;
                    p1 = p2;
                } while(iter.hasNext());
            }
        }
    }
    
}
