/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.layer;


import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.awt.Color;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class DefaultPainter implements Painter, ColorChangeable, TransparencyChangeable{
    private static Logger log = Logger.getLogger(DefaultPainter.class);
    
    private List<GPSkPoint> points;
    private List<GPSkSegment> segments;
    
    private boolean showPoints = false;
    private boolean showSegments = true;
    
    private SegmentPainter sp;
    private PointPainter pp;
    
    public DefaultPainter(List<GPSkSegment> segments, List<GPSkPoint> points){
        this.segments = segments;
        this.points = points;
        sp = new SegmentPainter(Color.red, 1.0f);
        pp = new PointPainter(Color.red, 1.0f);
    }

    @Override
    public void paint(PaintingArguments pa) {
        log.debug("zoom: " + pa.mapViewer.getZoom());
        if(showSegments){
            sp.paint(pa, segments);
        } else if(showPoints){
            pp.paint(pa, points);
        }
        
    }
    
    public void setShowPoints(boolean showPoints){
        this.showPoints = showPoints;
    }
    
    public void setShowSegments(boolean showSegments){
        this.showSegments = showSegments;
    }

    @Override
    public Color getColor() {
        return pp.getColor();
    }

    @Override
    public void setColor(Color c) {
        pp.setColor(c);
        sp.setColor(c);
    }

    @Override
    public float getTransparency() {
        return pp.getTransparency();
    }

    @Override
    public void setTransparency(float x) {
        pp.setTransparency(x);
        sp.setTransparency(x);
    }
    
    public void setSegments(List<GPSkSegment> segments){
        this.segments = segments;
    }
    
    public void setPoints(List<GPSkPoint> points){
        this.points = points;
    }
    
}
