/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.layer;

import java.awt.Graphics2D;
import org.jdesktop.swingx.JXMapViewer;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class PaintingArguments {
    protected JXMapViewer mapViewer;
    protected Graphics2D g2d;
    
    protected int width;
    protected int height;
    
    public PaintingArguments(){
        
    }
    
    public PaintingArguments(JXMapViewer mapViewer, Graphics2D g2d, int width, int height){
        this.mapViewer = mapViewer;
        this.g2d = g2d;
        this.width = width;
        this.height = height;
    }
    
    public PaintingArguments setMapViewer(JXMapViewer mapViewer){
        this.mapViewer = mapViewer;
        return this;
    }
    
    public PaintingArguments setGraphics(Graphics2D g2d){
        this.g2d = g2d;
        return this;
        
    }
    
    public PaintingArguments setWidth(int width){
        this.width = width;
        return this;
    }
    
    public PaintingArguments setHeight(int height){
        this.height = height;
        return this;
    }
    
    public PaintingArguments setBounds(int width, int height){
        this.width = width;
        this.height = height;
        return this;
    }
}
