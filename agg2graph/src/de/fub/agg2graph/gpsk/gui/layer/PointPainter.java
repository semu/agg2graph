/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.layer;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.List;
import org.jdesktop.swingx.mapviewer.TileFactory;

import static java.lang.Math.round;
import javolution.UtilTestSuite;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class PointPainter extends PainterUtility<List<GPSkPoint>>{

    public PointPainter() {
    }

    public PointPainter(int thickness, Color c, float transparency) {
        super(thickness, c, transparency);
    }

    public PointPainter(int thickness) {
        super(thickness);
    }

    public PointPainter(Color color) {
        super(color);
    }

    public PointPainter(float transparency) {
        super(transparency);
    }

    public PointPainter(int thickness, Color c) {
        super(thickness, c);
    }

    public PointPainter(Color c, float transparency) {
        super(c, transparency);
    }
    
    @Override
    public void paint(PaintingArguments pa, List<GPSkPoint> data) {    
        pa.g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, transparency));
        pa.g2d.setColor(color);
        TileFactory fac = pa.mapViewer.getTileFactory();
        int zoom = pa.mapViewer.getZoom();
        for(GPSkPoint p : data){
            Point2D point = fac.geoToPixel(p.gp, thickness);
            pa.g2d.fillOval((int) round(point.getX()),
                    (int) point.getY(), 
                    thickness, 
                    thickness);
        }
    }
    
    
}
