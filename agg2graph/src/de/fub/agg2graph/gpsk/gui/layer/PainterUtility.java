/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.layer;

import java.awt.Color;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public abstract class PainterUtility<T> {
    protected int thickness = 1;
    protected Color color = Color.RED;
    protected float transparency = 1.0f;
    
    public PainterUtility(){
    }
    
    public PainterUtility(int thickness, Color c, float transparency){
        this.thickness = thickness;
        this.color = c;
        this.transparency = transparency;
    }
    
    public PainterUtility(int thickness){
        this.thickness = thickness;
    }
    
    public PainterUtility(Color color){
        this.color = color;
    }
    
    public PainterUtility(float transparency){
        this.transparency = transparency;
    }
    
    public PainterUtility(int thickness, Color c){
        this.thickness = thickness;
        this.color = c;
    }
    
    public PainterUtility(Color c, float transparency){
        this.color = c;
        this.transparency = transparency;
    }

    public int getThickness() {
        return thickness;
    }

    public Color getColor() {
        return color;
    }

    public float getTransparency() {
        return transparency;
    }

    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setTransparency(float transparency) {
        this.transparency = transparency;
    }
    
    public abstract void paint(PaintingArguments pa, T data);
    
}
