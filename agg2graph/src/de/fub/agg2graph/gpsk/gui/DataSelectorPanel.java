/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.beans.KeyValuePair;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.actions.DataManagerAction;

import java.awt.dnd.DragSource;
import java.awt.dnd.DropTarget;
import java.util.*;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import static javax.swing.ListSelectionModel.SINGLE_SELECTION;
import static javax.swing.event.ListDataEvent.*;
/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class DataSelectorPanel extends JPanel {

    protected JList<KeyValuePair> dataList;
    private KVObserver observer = new KVObserver();

    public DataSelectorPanel() {

        dataList = new JList<>();
        EnvironmentManager.getInstance().addObserver(observer);
        dataList.setModel(observer);
        dataList.setFixedCellWidth(400);
        dataList.setSelectionMode(SINGLE_SELECTION);
        dataList.setDragEnabled(true);
        
        DragSource ds = new DragSource();
//        ds.createDefaultDragGestureRecognizer(c, actions, dgl)
        


        add(dataList);

    }

    private class KVObserver implements ListModel<KeyValuePair>, Observer {

        private EnvironmentManager em = EnvironmentManager.getInstance();
        private List<KeyValuePair> values = new LinkedList<>();
        private List<ListDataListener> listeners = new LinkedList<>();

        private KVObserver() {

            Set<String> keys = em.keys();
            for (String s : keys) {
                String title = em.getMeta(s).name;
                values.add(new KeyValuePair(s, title));
            }
        }

        @Override
        public int getSize() {
            return values.size();
        }

        @Override
        public KeyValuePair getElementAt(int i) {
            return values.get(i);

        }

        @Override
        public void addListDataListener(ListDataListener ll) {
            listeners.add(ll);
        }

        @Override
        public void removeListDataListener(ListDataListener ll) {
            listeners.remove(ll);
        }

        @Override
        public void update(Observable o, Object o1) {
            if (o == null) {
                return;
            }
            if (!(o instanceof EnvironmentManager)) {
                return;
            }
            if (o1 == null) {
                return;
            }
            if (!(o1 instanceof String)) {
                return;
            }

            EnvironmentManager em = (EnvironmentManager) o;
            String key = (String) o1;
            int in = containsKey(key);
            if (em.getEnv(key) == null) {
                for (ListIterator<KeyValuePair> it = values.listIterator();
                        it.hasNext();) {
                    int index = it.nextIndex();
                    KeyValuePair kt = it.next();
                    if (kt.key.equals(key)) {
                        it.remove();
                        for (ListDataListener ldl : listeners) {
                            ldl.intervalRemoved(new ListDataEvent(key, INTERVAL_REMOVED, index, index));
                        }
                    }
                }
            } else if (in >= 0) {
                for (ListDataListener ldl : listeners) {
                    ldl.contentsChanged(new ListDataEvent(key, CONTENTS_CHANGED,
                            in, in));
                }
            } else {
                String title;
                try {
                    title = em.getMeta(key).name;
                } catch (NullPointerException ex) {
                    title = "unknown";
                }
                KeyValuePair kt = new KeyValuePair(key, title);
                values.add(kt);
                for (ListDataListener ldl : listeners) {
                    ldl.intervalAdded(new ListDataEvent(key, INTERVAL_ADDED,
                            values.size() - 1, values.size() - 1));
                }

            }


        }

        private int containsKey(String key) {
            for (ListIterator<KeyValuePair> it = values.listIterator();
                    it.hasNext();) {
                int index = it.nextIndex();
                if (it.next().key.equals(key)) {
                    return index;
                }
            }
            return -1;
        }
    }
}