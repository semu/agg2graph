/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class DataItem {
    
    private List<GPSkPoint> points = new LinkedList<GPSkPoint>();
    private List<GPSkSegment> segments = new LinkedList<GPSkSegment>();
    
    private boolean visible = true;
    
    private Color color = Color.RED;
    
    public DataItem(List<?> data, boolean visible, Color c){
        if(!data.isEmpty()){
            Object first = data.get(0);
            if(first instanceof GPSkPoint){
                points = (List<GPSkPoint>) data;
            } else if(first instanceof GPSkSegment){
                segments = (List<GPSkSegment>) data;
            }
        }
    }
    
    public DataItem(List<?> data, boolean visible){
        this(data, visible, Color.RED);
    }
    
    public DataItem(List<?> data, Color c){
        this(data, true, c);
    }

    public DataItem(List<?> data){
        this(data, true, Color.RED);
    }
    public Color getColor() {
        return color;
    }

    public List<GPSkPoint> getPoints() {
        return points;
    }

    public List<GPSkSegment> getSegments() {
        return segments;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setPoints(List<GPSkPoint> points) {
        this.points = points;
    }

    public void setSegments(List<GPSkSegment> segments) {
        this.segments = segments;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    
    
    
    
}
