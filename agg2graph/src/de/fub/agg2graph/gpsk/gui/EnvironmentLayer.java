/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import java.awt.Color;
import javax.swing.JCheckBox;
import org.apache.log4j.Logger;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.LAYER_VISIBLE;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.TileFactory;
/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class EnvironmentLayer extends DataLayer implements Observer{
    
    private static Logger log = Logger.getLogger(DataLayer.class);
    private final String key;
    private final EnvironmentManager em;
    
    private GPSkEnvironment env;
    private GPSkEnvironment.MetaInformation metaInformation;
    
    public EnvironmentLayer(String key) {
        this(key, Color.RED);
        String c = metaInformation.keyValue.get("color");
        if (c == null) {
            return;
        }
        
        this.c = ss.stringToColor(c);

    }
    
    public EnvironmentLayer(String key, Color c) {
        this(key, c, true);
        String b = metaInformation.keyValue.get(LAYER_VISIBLE);
        setSelected(Boolean.parseBoolean(b));
    }
    

    
    public EnvironmentLayer(String key, Color c, boolean selected) {
        super(key);
        em = EnvironmentManager.getInstance();
        env = em.getEnv(key);
        if (env == null) {
            throw new IllegalArgumentException("tracklayer created for non existing track!");
        }
        this.key = key;
        metaInformation = em.getMeta(key);
        if (metaInformation == null) {
            setText(key);
        } else {
            setText(metaInformation.name + " (" + key + ')');
        }
        setToolTipText("right click to change the color");
        this.c = c;
        setSelected(selected);
        
        
    }
    
    @Override
    public void paint(Graphics2D g2d, JXMapViewer mapViewer, int width, int height) {
        
        
        List<GPSkSegment> segments = env.getSegments();
        if (segments == null) {
            return;
        }
        g2d.setColor(c);
        TileFactory fac = mapViewer.getTileFactory();
        int zoom = mapViewer.getZoom();
        if (metaInformation.showSegments) {
            //DEBUG

            for (GPSkSegment segment : segments) {
                
                Iterator<GPSkPoint> iter = segment.iterator();
                GPSkPoint p1 = iter.next();
                Point2D p2d1 = fac.geoToPixel(p1.gp, zoom);
                
                while (iter.hasNext()) {
                    GPSkPoint p2 = iter.next();
                    Point2D p2d2 = fac.geoToPixel(p2.gp, zoom);
                    g2d.drawLine((int) p2d1.getX(), (int) p2d1.getY(), (int) p2d2.getX(), (int) p2d2.getY());
                    p1 = p2;
                    p2d1 = p2d2;
                }
            }
        }
        if (metaInformation.showPoints) {
            Iterator<GPSkPoint> iter = env.getPoints().iterator();
            while (iter.hasNext()) {
                Point2D p2d = fac.geoToPixel(iter.next().gp, zoom);
                g2d.fillOval((int) p2d.getX(), (int) p2d.getY(), 6, 6);
            }
        }
        
        
    }
    
    @Override
    public void update(Observable o, Object o1) {
        if (o1 == null) {
            return;
        }
        if (!(o1 instanceof String)) {
            return;
        }
        
        String updatedKey = (String) o1;
        if (updatedKey.equals(key)) {
            this.env = em.getEnv(key);
            this.metaInformation = em.getMeta(key);
        }
    }
    
    
}
