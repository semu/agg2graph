/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class ExtensionFilter extends FileFilter {
    private final String extension;
    private final String desc;
    
    public ExtensionFilter(){
        this("GPX");
    }
    
    public ExtensionFilter(String ex){
        extension = "." + ex.toLowerCase();
        desc = ex.toUpperCase() + "-Files (*." + ex.toLowerCase() +")";
    }

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        if (f.getName().toLowerCase().endsWith(extension)) {
            return true;
        }
        return false;
    }

    @Override
    public String getDescription() {
        return desc;
    }
}
