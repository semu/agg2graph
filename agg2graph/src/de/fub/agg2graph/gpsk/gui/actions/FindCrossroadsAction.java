/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.PreferenceWindow;
import de.fub.agg2graph.gpsk.gui.components.AbortButton;
import de.fub.agg2graph.gpsk.gui.components.ApproveButton;
import de.fub.agg2graph.gpsk.gui.components.DoubleField;
import de.fub.agg2graph.gpsk.gui.components.IntegerField;
import de.fub.agg2graph.gpsk.logic.CrossroadDetector;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.*;
import javax.swing.*;
import org.apache.log4j.Logger;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.TYPE;
import de.fub.agg2graph.gpsk.data.Filter;
import de.fub.agg2graph.gpsk.gui.DropDownModel;
import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 *
 * @author Christian Windolf
 */
public class FindCrossroadsAction extends AbstractAction implements Observer {

    private static Logger log = Logger.getLogger(FindCrossroadsAction.class);
    
    private HashSet<String> opticsKeys = new HashSet<>();

    public FindCrossroadsAction() {
        super("Find crossroads");
        putValue(SHORT_DESCRIPTION, "find crossroads with the help of OPTICS results");
        setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        new Options();
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o == null) {
            return;
        }
        if (!(o instanceof EnvironmentManager)) {
            return;
        }
        if (o1 == null) {
            return;
        }
        if (!(o1 instanceof String)) {
            return;
        }
        EnvironmentManager em = (EnvironmentManager) o;
        String key = (String) o1;
        if (em.contains(key)) {
            String type = em.getMeta(key).keyValue.get(TYPE);
            if ("optics".equals(type)) {
                setEnabled(true);
                opticsKeys.add(key);
            }
        } else {
            opticsKeys.remove(key);
            if(opticsKeys.isEmpty()){
                setEnabled(true);
            }
        }
    }

    private class Options extends JDialog implements PreferenceWindow {

        private final JLabel rangeLabel = new JLabel("Range");
        private final IntegerField rangeField;
        private final JLabel relLabel = new JLabel("proportion");
        private final DoubleField relField;
        private final JButton okButton;
        private final JButton cancelButton;
        private final JComboBox<String> inputChooser;
        private final JTextField outputChooser;
        

        public Options() {
            super(Main.mainWindow, "Crossroad detection options");
            setLayout(new GridLayout(4, 2));

            rangeField = new IntegerField(KeyValueStore.getInt(CrossroadDetector.RANGEKEY, CrossroadDetector.DEFAULT_RANGE),
                    rangeLabel, 1, 100);

            relField = new DoubleField(KeyValueStore.getDouble(CrossroadDetector.RELKEY, CrossroadDetector.DEFAULT_REL),
                    relLabel, 0, 1);

            add(rangeLabel);
            add(rangeField);
            add(relLabel);
            add(relField);

           
            inputChooser = new JComboBox<>(new DropDownModel(new Filter.DefaultFilter<GPSkEnvironment>()));

            outputChooser = new JTextField("computedCrossroads");

            add(inputChooser);
            add(outputChooser);

            okButton = new ApproveButton(this);
            cancelButton = new AbortButton(this);

            add(okButton);
            add(cancelButton);

            setDefaultCloseOperation(HIDE_ON_CLOSE);

            pack();
            setVisible(true);

        }

        @Override
        public boolean save() {
            if (!rangeField.check()) {
                return false;
            }
            if (!relField.check()) {
                return false;
            }

            KeyValueStore.putValue(CrossroadDetector.RANGEKEY, rangeField.getInt());
            KeyValueStore.putValue(CrossroadDetector.RELKEY, relField.getDouble());

            CrossroadDetector cd;
            String key = (String) inputChooser.getSelectedItem();
            
            try {
                cd = new CrossroadDetector(key, outputChooser.getText());
                Main.executor.execute(cd);
            } catch (IllegalArgumentException ex) {
                log.warn(ex);
                showMessageDialog(Main.mainWindow, "For the key " + key
                        + " no data exists!", "ERROR", ERROR_MESSAGE);
            }

            return true;
        }
    }
}
