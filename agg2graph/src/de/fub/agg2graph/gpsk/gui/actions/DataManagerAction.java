/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.KeyValuePair;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.DataInfoPanel;
import de.fub.agg2graph.gpsk.gui.DataManager;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.Map.Entry;
import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;




import javax.swing.event.*;
import javax.swing.table.TableModel;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class DataManagerAction extends AbstractAction {
    private DataManager manager = new DataManager();
    public DataManagerAction(){
        super("Open data manager");
        manager.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        manager.setVisible(true);
    }
   
    
}
