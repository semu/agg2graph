/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm;
import de.fub.agg2graph.gpsk.algorithms.DefaultAlgorithmRunner;
import de.fub.agg2graph.gpsk.algorithms.MultipleInputAlgorithm;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.BusEvent;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.data.MessageBus;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import javax.swing.AbstractAction;

import static de.fub.agg2graph.gpsk.data.MessageBus.GPSK;
import de.fub.agg2graph.gpsk.gui.AllAvailableModel;
import de.fub.agg2graph.gpsk.gui.AvailabilityListener;
import de.fub.agg2graph.gpsk.gui.MinOneEnvModel;
import de.fub.agg2graph.gpsk.gui.MultipleInputModel;
import de.fub.agg2graph.gpsk.gui.MultipleInputPanel;
import de.fub.agg2graph.gpsk.gui.OptionsModel;
import de.fub.agg2graph.gpsk.gui.OptionsPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import org.apache.log4j.Logger;

import static java.awt.BorderLayout.PAGE_END;
import static java.awt.BorderLayout.PAGE_START;
import static java.awt.BorderLayout.CENTER;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JButton;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class MInputAction extends AbstractAction implements AvailabilityListener {
    private static Logger log = Logger.getLogger(MInputAction.class);

    private Class<? extends MultipleInputAlgorithm> algorithmClass;
    private Class optionsClass;
    private OptionsModel om = null;
    private MultipleInputModel mim = null;
    
    private Dialog dialog;

    public MInputAction(Class<? extends MultipleInputAlgorithm> algorithmClass) {
        super(algorithmClass.getSimpleName());
        this.algorithmClass = algorithmClass;
        Options options = (Options) algorithmClass.getAnnotation(Options.class);
        if (options != null) {
            optionsClass = options.value();
        }
        try {
            om = new OptionsModel(optionsClass);
            mim = new MultipleInputModel(algorithmClass);
            dialog = new Dialog(algorithmClass.getSimpleName());
            
        } catch (Exception ex) {
            log.error("unable to create an optionsModel", ex);
            BusEvent be = new BusEvent(ex, null);
            MessageBus.getInstance(GPSK).fireEvent(be);
        }
        AllAvailableModel aam = new AllAvailableModel();
        aam.addAvailabilityListener(this);
        for(int i = 0; i < mim.getAmountOfInputs(); i++){
            MinOneEnvModel moem = new MinOneEnvModel(mim.getModelAt(i));
            moem.addAvailabilityListener(aam);
            aam.addSource(moem);
        }
        setEnabled(aam.isAvailable());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        dialog.setVisible(true);
    }

    @Override
    public void updateAvailability(boolean available, Object o) {
        setEnabled(available);
    }
    
    private class Dialog extends JDialog{
        private final OptionsPanel op = new OptionsPanel(om);
        private final MultipleInputPanel ioPanel = new MultipleInputPanel(mim);
        
        public Dialog(String name){
            super(Main.mainWindow, name);
            setDefaultCloseOperation(HIDE_ON_CLOSE);
            setLayout(new BorderLayout());
            
            JPanel optionsPanel = new OptionsPanel(om);
            
            add(ioPanel, PAGE_START);
            add(optionsPanel, CENTER);
            JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
            JButton abort = new JButton("abort");
            abort.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                }
            });
            buttonPanel.add(abort);
            
            JButton ok = new JButton("OK");
            ok.addActionListener(new Handler());
            buttonPanel.add(ok);
            add(buttonPanel, PAGE_END);
            
            pack();
        }
    }

    private class Handler implements ActionListener {
        private EnvironmentManager em = EnvironmentManager.getInstance();

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                KeyValueStore.saveOptions(om.getOptions());
                List<String> inputList = new LinkedList<>();
                for(int i = 0; i < mim.getAmountOfInputs(); i++){
                    inputList.add((String)mim.getModelAt(i).getSelectedItem());
                }
                DefaultAlgorithmRunner dar = new DefaultAlgorithmRunner(
                        inputList, 
                        dialog.ioPanel.getOutput(), 
                        (Class<? extends MultipleInputAlgorithm<GPSkEnvironment>>)algorithmClass);
                Main.executor.execute(dar);
            } catch (Exception ex) {
                log.error("unable to save options", ex);
            } finally{
                dialog.setVisible(false);
            }
        }
    }
}
