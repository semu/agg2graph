/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.algorithms.DefaultAlgorithmRunner;
import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.gui.DropDownModel;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;



import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.OptionsModel;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

import org.apache.log4j.Logger;


import de.fub.agg2graph.gpsk.gui.OptionsPanel;
import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.PAGE_START;
import static java.awt.BorderLayout.PAGE_END;
import static de.fub.agg2graph.gpsk.algorithms.AlgorithmInputFilter.getFilter;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.gui.AvailabilityListener;
import de.fub.agg2graph.gpsk.gui.MinOneEnvModel;
import java.util.logging.Level;
import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class RunAlgorithmAction extends AbstractAction implements AvailabilityListener{

    private static Logger log = Logger.getLogger(RunAlgorithmAction.class);
    private Class algorithmClass;
    private RunDialog rd;
    private OptionsModel om;
    private final DropDownModel ddm;

    public RunAlgorithmAction(Class algorithmClass) {
        super(algorithmClass.getSimpleName());
        this.algorithmClass = algorithmClass;
        Options options = (Options) algorithmClass.getAnnotation(Options.class);
        
        ddm = new DropDownModel(getFilter(algorithmClass));
        MinOneEnvModel moem = new MinOneEnvModel(ddm);
        moem.addAvailabilityListener(this);
        setEnabled(moem.isAvailable());
        try {
            om = new OptionsModel(options.value());
            
        } catch (Exception ex) {
            log.fatal("unable to create action! ", ex);
        }
        Description desc = (Description) algorithmClass.getAnnotation(Description.class);
        if (desc != null) {
            putValue(SHORT_DESCRIPTION, desc.value());
        }
        rd = new RunDialog();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        rd.setVisible(true);


    }

    @Override
    public void updateAvailability(boolean available, Object o) {
        setEnabled(available);
    }

    private class RunDialog extends JDialog {

        private final JLabel inputLabel = new JLabel("input: ");
        private final JComboBox<String> inputList;
        
        private final JLabel outputLabel = new JLabel("output: ");
        private final JTextField outputField =
                new JTextField(algorithmClass.getSimpleName());
        private final JButton cancelButton = new JButton("cancel");
        private final JButton okButton = new JButton("OK");
        private final JPanel optionsPanel = new OptionsPanel(om);

        RunDialog() {
            super(Main.mainWindow,
                    algorithmClass.getSimpleName() + " Options");

            JPanel ioPanel = new JPanel(new GridLayout(2, 2));

            setLayout(new BorderLayout());
            
            EnvironmentManager.getInstance().addObserver(ddm);
            inputList = new JComboBox<String>(ddm);

            ioPanel.add(inputLabel);
            ioPanel.add(outputLabel);

            ioPanel.add(inputList);
            ioPanel.add(outputField);

            add(ioPanel, PAGE_START);

            JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

            cancelButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    setVisible(false);
                }
            });

            okButton.addActionListener(new Executor());

            buttonPanel.add(cancelButton);
            buttonPanel.add(okButton);
            add(buttonPanel, PAGE_END);

            add(optionsPanel, CENTER);
            pack();

        }
    }

    private class Executor implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            int length = om.getAmountOfItems();
            boolean e = true;
            for (int i = 0; i < length; i++) {
                String error = om.getErrorMessage(i);
                if (error != null) {
                    e = false;
                    showMessageDialog(rd, error, "Error", ERROR_MESSAGE);
                }
            }
            if (e) {
                rd.setVisible(false);
                try {
                    KeyValueStore.saveOptions(om.getOptions());
                } catch (Exception ex) {
                    showMessageDialog(rd, "failed to store options. It is probably a programming error", "Error", ERROR_MESSAGE);
                }
                DefaultAlgorithmRunner dar = new DefaultAlgorithmRunner(
                        (String) rd.inputList.getSelectedItem(),
                        rd.outputField.getText(),
                        algorithmClass);
                Main.executor.execute(dar);
            }
        }
    }
}
