/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.BusEvent;
import de.fub.agg2graph.gpsk.obs.InputFile;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.UIManager;

import static java.awt.event.ActionEvent.CTRL_MASK;
import static java.awt.event.KeyEvent.VK_O;
import java.io.File;
import javax.swing.JFileChooser;
import static javax.swing.KeyStroke.getKeyStroke;
import javax.swing.filechooser.FileFilter;


import static de.fub.agg2graph.gpsk.gui.StatusBar.OPENING_FILE;
import static de.fub.agg2graph.gpsk.data.EnvironmentManager.RAW_DATA;
import de.fub.agg2graph.gpsk.data.MessageBus;
import de.fub.agg2graph.gpsk.io.EnvironmentImport;
import de.fub.agg2graph.gpsk.pipe.CompleteImport;
import de.fub.agg2graph.io.ConvertedImport;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.*;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import static java.awt.BorderLayout.PAGE_START;
import static java.awt.BorderLayout.CENTER;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 *
 * @author Christian Windolf
 */
public class OpenGPXAction extends AbstractAction {

    private static final String folderKey = "OpenGPXAction.folder";
    private JFileChooser fChooser = new JFileChooser();
    private JDialog dialog;
    private JPanel northPanel;
    private JCheckBox doNotClean = new JCheckBox("Don't clean tracks", false);
    private OutputField outputField = new OutputField();
    private static final String desc = "opens a GPX-file";
    private final InputFile inputFile = InputFile.getInstance();

    public OpenGPXAction() {
        super("open", UIManager.getIcon("Tree.openIcon"));
        putValue(ACCELERATOR_KEY, getKeyStroke(VK_O, CTRL_MASK));
        putValue(SHORT_DESCRIPTION, desc);
        fChooser.setFileFilter(new ExtensionFilter());
        fChooser.setFileHidingEnabled(false);
        fChooser.addChoosableFileFilter(new AcceptAllFilter());
        File def = new File(KeyValueStore.getValue(folderKey, new File(".").getAbsolutePath()));
        fChooser.setCurrentDirectory(def);
        dialog = new JDialog(Main.mainWindow, "open GPX files");
        dialog.setLayout(new BorderLayout());
        northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        northPanel.add(doNotClean);
        northPanel.add(outputField);
        doNotClean.addActionListener(outputField);
        fChooser.addPropertyChangeListener(outputField);
        fChooser.setApproveButtonText("open");
        fChooser.addActionListener(new Handler());
        dialog.add(northPanel, PAGE_START);
        dialog.add(fChooser, CENTER);
        dialog.pack();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        dialog.setVisible(true);

//        int result = fChooser.showOpenDialog(Main.mainWindow);
//        if(result == APPROVE_OPTION){
//            File file = fChooser.getSelectedFile();
//            KeyValueStore.putValue(folderKey, file.getParent());
//            inputFile.setFile(file);
//            Main.mainWindow.getStatusBar().setStatus(OPENING_FILE, RAW_DATA);
//        }
    }

    private class OutputField extends JTextField implements ActionListener, PropertyChangeListener {
        
        private OutputField(){
            super(20);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setEnabled(doNotClean.isSelected());

        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Object o = evt.getNewValue();
            if (o instanceof File) {
                File f = (File) o;
                outputField.setText(f.getName().replace(".gpx", ""));
            }

        }
    }

    private class Handler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals(JFileChooser.APPROVE_SELECTION)) {
                File f = fChooser.getSelectedFile();
                KeyValueStore.putValue(folderKey,
                        fChooser.getCurrentDirectory().getAbsolutePath());
                CompleteImport ci = new CompleteImport(f, !doNotClean.isSelected(), outputField.getText());
                Main.executor.execute(ci);
                dialog.setVisible(false);

            } else if (e.getActionCommand().equals(JFileChooser.CANCEL_SELECTION)){
                dialog.setVisible(false);
            }

        }
    }
}
