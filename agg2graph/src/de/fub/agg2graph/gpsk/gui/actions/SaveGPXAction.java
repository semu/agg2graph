/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.PAGE_START;
import static java.awt.event.ActionEvent.CTRL_MASK;
import static java.awt.event.KeyEvent.VK_S;
import static javax.swing.JOptionPane.NO_OPTION;
import static javax.swing.JOptionPane.YES_NO_OPTION;
import static javax.swing.JOptionPane.showConfirmDialog;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.UIManager;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.DropDownModel;
import de.fub.agg2graph.gpsk.logic.GPXWriterTask;

/**
 * 
 * @author Christian Windolf christianwindolf@web.de
 */
public class SaveGPXAction extends AbstractAction {

	private static final Icon saveIcon;
	private static final Icon abortIcon;
	static {
		saveIcon = UIManager.getIcon("FileView.floppyDriveIcon");
		abortIcon = UIManager.getIcon("to be done");
	}

	private static String folderkey = "SaveGPXAction.folder";

	private String input;
	private final JFileChooser fc;

	private GPXWriterTask writerTask = new GPXWriterTask();

	private JComboBox<String> list;

	private DropDownModel dd = new DropDownModel();

	private String key = null;

	JDialog dialog = new JDialog(Main.mainWindow, "Save GPX");

	public SaveGPXAction() {
		super("save", saveIcon);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_S, CTRL_MASK));

		File f = new File(KeyValueStore.getValue(folderkey,
				new File(".").getAbsolutePath()));
		fc = new JFileChooser(f);
		fc.setFileHidingEnabled(false);
		fc.setFileFilter(new ExtensionFilter("GPX"));
		fc.addChoosableFileFilter(new ExtensionFilter("GPK"));
		fc.addChoosableFileFilter(new AcceptAllFilter());
		fc.setApproveButtonText("save");
		EnvironmentManager.getInstance().addObserver(dd);
		list = new JComboBox<>(dd);
		dialog.setLayout(new BorderLayout());
		dialog.add(list, PAGE_START);

		dialog.add(fc, CENTER);
		// fc.setAccessory(list);

		fc.addActionListener(new Handler());

		dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

		dialog.pack();
	}

	public SaveGPXAction(String key) {
		this();
		dialog.remove(list);

	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		dialog.setVisible(true);

		// int result = fc.showSaveDialog(Main.mainWindow);
		// if(result == APPROVE_OPTION){
		// File f = fc.getSelectedFile();
		// KeyValueStore.putValue(folderkey,
		// f.getParentFile().getAbsolutePath());
		// writerTask.setPath(f);
		// writerTask.setInput((String) list.getSelectedItem());
		// Main.executor.execute(writerTask);
		// }
	}

	private class Handler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			String command = ae.getActionCommand();
			if (command.equals(JFileChooser.APPROVE_SELECTION)) {
				File f = fc.getSelectedFile();
				if (f.exists()) {
					int result = showConfirmDialog(dialog,
							"The file " + f.getName()
									+ " already exists! Overwrite it?",
							"File already exists", YES_NO_OPTION);
					if (result == NO_OPTION) {
						return;
					}

				}
				KeyValueStore.putValue(folderkey, fc.getCurrentDirectory()
						.getAbsolutePath());
				writerTask.setPath(f);
				if (key == null) {
					writerTask.setInput((String) list.getSelectedItem());
				} else {
					writerTask.setInput(key);
				}
				Main.executor.execute(writerTask);
				dialog.setVisible(false);

			} else if (command.equals(JFileChooser.CANCEL_SELECTION)) {
				dialog.setVisible(false);
			}
		}

	}

}
