/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui.actions;

import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class RemoveEnvironmentAction extends AbstractAction{
    private String key;
    
    private final EnvironmentManager em = EnvironmentManager.getInstance();
    
    public RemoveEnvironmentAction(){
        super("remove");
        putValue(SHORT_DESCRIPTION, "removes this environment from the application. "
                + "Has no influence on the data on your hard drive");
        setEnabled(false);
    }
    
    public RemoveEnvironmentAction(String key){
        this();
        this.key = key;
        if(key != null){
            setEnabled(true);
        }
    }
    
    public void setKey(String key){
        this.key = key;
        if(key != null){
            setEnabled(true);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        em.remove(key);
        
    }
}
