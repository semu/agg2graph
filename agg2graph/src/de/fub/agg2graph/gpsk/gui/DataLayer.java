/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.structs.GPSSegment;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JCheckBoxMenuItem;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.TileFactory;


import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.text.html.StyleSheet;

/**
 *
 * @author Christian Windolf
 */
public abstract class DataLayer extends JCheckBox implements Layer {
    
    
    
    protected Color c = Color.RED;
    protected final JPopupMenu context;
    protected final StyleSheet ss = new StyleSheet();
    
    public DataLayer(String title, boolean selected, Color c){
        super(title, selected);
        context = new ContextMenu();
        addMouseListener(new PopupListener());
        this.c = c;
    }
    
    public DataLayer(String title, boolean selected){
        super(title, selected);
        context = new ContextMenu();
        addMouseListener(new PopupListener());
        c = Color.RED;
    }
    
    public DataLayer(String title){
        this(title, true);
    }
    
    
    
    
    @Override
    public boolean visible() {
        return isSelected();
    }
    
    @Override
    public void setVisible(boolean b) {
        //ignore
    }
    
    protected class ContextMenu extends JPopupMenu {
        
        private final ColorButton yellow = new ColorButton("yellow");
        private final ColorButton blue = new ColorButton("blue");
        private final JMenuItem red = new ColorButton("red");
        private final JMenuItem black = new ColorButton("black");
        private final JMenuItem white = new ColorButton("white");
        private final ButtonGroup group = new ButtonGroup();
        private final ActionListener al = new ColorSetter();
        
        public ContextMenu() {
            add(yellow);
            group.add(yellow);
            yellow.addActionListener(al);
            if(c.equals(Color.YELLOW)){
                yellow.setSelected(true);
            }
            
            add(blue);
            group.add(blue);
            blue.addActionListener(al);
            if(c.equals(Color.BLUE)){
                blue.setSelected(true);
            }
            
            add(red);
            group.add(red);
            red.addActionListener(al);
            if(c.equals(Color.RED)){
                red.setSelected(true);
            }
            
            add(black);
            group.add(black);
            black.addActionListener(al);
            if(c.equals(Color.BLACK)){
                black.setSelected(true);
            }
            
            add(white);
            group.add(white);
            white.addActionListener(al);
            if(c.equals(Color.WHITE)){
                white.setSelected(true);
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ContextMenu other = (ContextMenu) obj;
            if (!Objects.equals(this.yellow, other.yellow)) {
                return false;
            }
            if (!Objects.equals(this.blue, other.blue)) {
                return false;
            }
            if (!Objects.equals(this.red, other.red)) {
                return false;
            }
            if (!Objects.equals(this.black, other.black)) {
                return false;
            }
            if (!Objects.equals(this.white, other.white)) {
                return false;
            }
            if (!Objects.equals(this.group, other.group)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 17 * hash + Objects.hashCode(this.yellow);
            hash = 17 * hash + Objects.hashCode(this.blue);
            hash = 17 * hash + Objects.hashCode(this.red);
            hash = 17 * hash + Objects.hashCode(this.black);
            hash = 17 * hash + Objects.hashCode(this.white);
            hash = 17 * hash + Objects.hashCode(this.group);
            return hash;
        }
        
        
    }
    
    private class ColorButton extends JRadioButtonMenuItem {
        
        private final Color color;
        
        public ColorButton(String color) {
            super(color);
            this.color = ss.stringToColor(color);
        }
    }
    
    private class ColorSetter implements ActionListener {
        
        @Override
        public void actionPerformed(ActionEvent ae) {
            ColorButton cd = (ColorButton) ae.getSource();
            synchronized (this) {
                c = cd.color;
            }
        }
    }
    
    private class PopupListener extends MouseAdapter {
        
        @Override
        public void mousePressed(MouseEvent e) {
            handle(e);
        }
        
        @Override
        public void mouseReleased(MouseEvent e) {
            handle(e);
        }
        
        private void handle(MouseEvent e) {
            if (e.isPopupTrigger()) {
                context.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataLayer other = (DataLayer) obj;
        return true;
    }

    
    
    
    
}
