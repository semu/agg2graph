/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class AllAvailableModel implements AvailabilitySupport, AvailabilityListener {

    private List<AvailabilityListener> listeners = new LinkedList<>();
    private List<AvailabilitySupport> sourceEvents = new LinkedList<>();
    private boolean available = true;

    public AllAvailableModel() {
    }

    public void addSource(AvailabilitySupport... as) {
        for (int i = 0; i < as.length; i++) {
            sourceEvents.add(as[i]);
            if (available) {
                if (!as[i].isAvailable()) {
                    available = false;
                }
            }
        }
    }

    @Override
    public void addAvailabilityListener(AvailabilityListener al) {
        listeners.add(al);
    }

    @Override
    public void removeAvailabilityListener(AvailabilityListener al) {
        listeners.remove(al);
    }

    @Override
    public boolean isAvailable() {
        return available;

    }

    @Override
    public void updateAvailability(boolean a, Object o) {

        boolean cache = available;
        available = true;
        for (AvailabilitySupport as : sourceEvents) {

            available &= as.isAvailable();
        }

        if (cache != available) {

            for (AvailabilityListener al : listeners) {
                al.updateAvailability(available, this);
            }
        }
    }
}
