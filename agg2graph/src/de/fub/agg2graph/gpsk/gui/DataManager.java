/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.Main;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JDialog;
import javax.swing.JScrollPane;

import static javax.swing.JScrollPane.*;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class DataManager extends JDialog {

    private final DataSelectorPanel selector = new DataSelectorPanel();
    private final JScrollPane scrollPane1 = new JScrollPane(selector,
            VERTICAL_SCROLLBAR_ALWAYS,
            HORIZONTAL_SCROLLBAR_NEVER);
    private final DataInfoPanel dataInfo = new DataInfoPanel();
    private final JScrollPane scrollPane2 = new JScrollPane(dataInfo,
            VERTICAL_SCROLLBAR_ALWAYS,
            HORIZONTAL_SCROLLBAR_NEVER);

    public DataManager() {
        super(Main.mainWindow, "Data Manager");
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.LEADING));
        scrollPane1.setPreferredSize(new Dimension(400, 600));
        scrollPane2.setPreferredSize(new Dimension(400, 600));
        add(scrollPane1);
        add(scrollPane2);

        selector.dataList.addListSelectionListener(dataInfo.tableContent);
        selector.dataList.addListSelectionListener(dataInfo.am);
        pack();
    }
}