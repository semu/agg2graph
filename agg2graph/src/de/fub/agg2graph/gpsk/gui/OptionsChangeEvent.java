/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

/**
 *
 * @author Christian Windolf
 */
public class OptionsChangeEvent {
    public static final int OK = 1;
    public static final int ERROR = 2;
    
    public final String value;
    public final int status;
    public final Object source;
    
    public OptionsChangeEvent(String value, int status, Object source){
        this.value = value;
        this.status = status;
        this.source = source;
    }
    
    public OptionsChangeEvent(String value, int status){
        this(value, status, null);
    }
    
    public OptionsChangeEvent(String value){
        this(value, OK);
    }
}
