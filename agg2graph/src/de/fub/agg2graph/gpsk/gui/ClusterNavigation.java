/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.ResourceManager;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class ClusterNavigation extends JToolBar implements PropertyChangeListener{
    
    private final JButton prevButton;
    private final JButton nextButton;
    
    public ClusterNavigation(PaintingInfo pi){
        super("DBSCAN clusters", VERTICAL);
        setLayout(new BorderLayout());
        Icon prevIcon = null;
        try {
            InputStream stream = ResourceManager.getInputStream("/icons/Back24.gif");
            prevIcon = new ImageIcon(ImageIO.read(stream));
            
        } catch (IOException ex) {
            
        }
        if(prevIcon == null){
            prevButton = new JButton("prev");
        } else {
            prevButton = new JButton(prevIcon);
        }
        
        Icon nextIcon = null;
        try{
            InputStream stream = ResourceManager.getInputStream("/icons/Forward24.gif");
            nextIcon = new ImageIcon(ImageIO.read(stream));
        } catch (IOException ex){
            
        }
        
        if(nextIcon == null){
            nextButton = new JButton("next");
        } else {
            nextButton = new JButton(nextIcon);
        }
        
        JPanel navPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        navPanel.add(prevButton);
        navPanel.add(nextButton);
        
        
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    private class NavigationHandler implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
    }
    
}
