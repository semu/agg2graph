/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.data.BusEvent;
import de.fub.agg2graph.gpsk.data.BusListener;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.algorithms.optics.Algorithm;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class StatusBar extends JPanel implements BusListener {

    public static final String OPENING_FILE = "opening file";
    public static final String CLEANING_FILE = "cleaning file";
    public static final String OPTICS_RUNNING = "OPTICS running";
    private static final int maxMessages = 4;
    
    private final Icon errorIcon = UIManager.getIcon("OptionPane.errorIcon");
    private final Icon infoIcon = UIManager.getIcon("OptionPane.informationIcon");
    private LinkedList<JLabel> eventLabels = new LinkedList<>();

    public StatusBar() {
        super(new FlowLayout(FlowLayout.LEFT));
    }

    @Override
    public void messageReceived(BusEvent be) {
        final JLabel label;
        if (be.type == BusEvent.ERROR_MESSAGE) {
            label = new JLabel(be.message, errorIcon, SwingConstants.LEFT);

        } else {
            label = new JLabel(be.message, infoIcon, SwingConstants.LEFT);
        }

        label.setToolTipText(be.source.toString());
        label.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(5,5,5,5), new LineBorder(Color.BLACK)));
        



        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (eventLabels.size() >= maxMessages) {
                    JLabel first = eventLabels.removeFirst();
                    remove(first);
                }
                add(label);
                revalidate();
                getParent().revalidate();

            }
        });        
    }
}
