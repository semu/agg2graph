/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.algorithms.NormalizeOptions;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.*;

import static de.fub.agg2graph.gpsk.gui.OptionsChangeEvent.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.apache.log4j.Logger;


import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 *
 * @author Christian Windolf
 */
public class OptionsPanel extends JPanel implements OptionsChangeListener {

    private static Logger log = Logger.getLogger(OptionsPanel.class);
    private OptionsModel model;
    private ItemPanel[] panels;

    public OptionsPanel(OptionsModel om) {
        super(new GridBagLayout());
        this.model = om;
        om.addOptionsChangeListener(this);
        panels = new ItemPanel[om.getAmountOfItems()];
        for (int i = 0; i < panels.length; i++) {
            panels[i] = new ItemPanel(i);
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = i;
            gbc.insets = new Insets(5, 5, 5, 5);
            add(panels[i], gbc);
        }
    }

    @Override
    public void optionsChanged(int i, OptionsChangeEvent oce) {
        if (oce.status == ERROR) {
            panels[i].right.setBackground(Color.YELLOW);
        } else {
            panels[i].right.setBackground(Color.WHITE);
        }
    }
    
    public OptionsModel getOptionsModel(){
        return model;
    }

    private class ItemPanel extends JPanel {

        private final int index;
        private final JComponent left;
        private final JComponent right;

        private ItemPanel(int i) {
            super(new GridLayout(1, 2));
            setPreferredSize(new Dimension(200, 20));
            if (model.getType(i).equals(Boolean.TYPE)) {
                left = new JLabel();
                JCheckBox cb =
                        new JCheckBox(model.getLabel(i), model.getBoolean(i));
                cb.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        JCheckBox source = (JCheckBox) ae.getSource();
                        try {
                            model.setValue(String.valueOf(source.isSelected()), index);
                        } catch (Exception ex) {
                            log.error("unable to save the value of a checkbox :(",
                                    ex);
                        }
                    }
                });
                right = cb;


            } else {
                left = new JLabel(model.getLabel(i));
                JTextField field = new JTextField(model.getFieldValue(i));
                field.getDocument().addDocumentListener(new ChangeListener((i)));
                right = field;
            }
            index = i;
            add(left);
            add(right);
            setToolTipText(model.getToolTip(i));
        }
    }

    private class ChangeListener implements DocumentListener {

        private final int index;

        ChangeListener(int i) {
            this.index = i;
        }

        @Override
        public void insertUpdate(DocumentEvent de) {
            handleUpdate(de);
        }

        @Override
        public void removeUpdate(DocumentEvent de) {
            handleUpdate(de);
        }

        @Override
        public void changedUpdate(DocumentEvent de) {
            handleUpdate(de);
        }

        private void handleUpdate(DocumentEvent de) {
            Document doc = de.getDocument();
            try {
                String text = doc.getText(0, doc.getLength());
                model.setValue(text, index);
                
                
            } catch (BadLocationException ex) {
                log.error("unable to retrieve text!", ex);
            } catch (Exception ex) {
                
            }
        }
    }

    public static void main(String[] args) throws Exception {
        OptionsModel om = new OptionsModel(NormalizeOptions.class);
        OptionsPanel op = new OptionsPanel(om);
        JFrame frame = new JFrame("Test Test Test");
        frame.add(op);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
