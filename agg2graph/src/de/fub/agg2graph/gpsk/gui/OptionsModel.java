/*
 * Copyright (C) 2013 Christian Windolf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.annotations.Max;
import de.fub.agg2graph.gpsk.annotations.Min;
import de.fub.agg2graph.gpsk.data.BusEvent;
import de.fub.agg2graph.gpsk.data.MessageBus;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Field;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.*;
import org.apache.log4j.Logger;

import static java.lang.Integer.parseInt;
import static java.lang.Double.parseDouble;
import static java.lang.Long.parseLong;

import static java.lang.Double.isNaN;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Christian Windolf
 */
public class OptionsModel implements PropertyChangeListener {

    private static Logger log = Logger.getLogger(OptionsModel.class);
    Class optionClass;
    Object optionsObject;
    private List<OptionsChangeListener> listeners = new LinkedList<>();
    private Item[] items;
    private Field[] fields;
    private String[] errors;

    public OptionsModel(Class optionClass) throws Exception {
        this.optionClass = optionClass;
        fields = optionClass.getFields();
        items = new Item[fields.length];
        errors = new String[fields.length];

        for (int i = 0; i < fields.length; i++) {
            items[i] = new Item(fields[i]);
        }

        optionsObject = KeyValueStore.loadOptions(optionClass);
    }

    public OptionsModel() {
        fields = new Field[0];
        items = new Item[0];
        errors = new String[0];
    }

    public int getAmountOfItems() {
        return items.length;
    }
    
    public Class getOptionsClass(){
        return optionClass;
    }

    /**
     * returns a descriptive label for this attribute
     *
     * @param i index of the attribute
     * @return
     */
    public String getLabel(int i) {
        return items[i].label;
    }

    /**
     * returns the value of the field
     *
     * @param i index of the attribute
     * @return
     */
    public String getFieldValue(int i) {
        return items[i].getText();
    }

    /**
     * a desciprtive tooltip for this field
     *
     * @param i
     * @return
     */
    public String getToolTip(int i) {
        return items[i].tooltip;
    }

    public Object getOptions() {
        return optionsObject;
    }

    public Class getType(int i) {
        return fields[i].getType();
    }

    /**
     *
     * @param i
     * @return null, if everything is alright
     */
    public String getErrorMessage(int i) {
        return errors[i];
    }

    public String setValue(String v, int i) throws Exception {
        errors[i] = items[i].setValue(v);
        
        if (errors[i] == null) {
            OptionsChangeEvent oce = new OptionsChangeEvent(
                    v,
                    OptionsChangeEvent.OK,
                    this);
            for (OptionsChangeListener ocl : listeners) {
                ocl.optionsChanged(i, oce);
            }
        }
        return errors[i];
    }

    public boolean getBoolean(int i) {
        return Boolean.parseBoolean(items[i].getText());
    }

    private OptionsModel outer() {
        return this;
    }

    public void addOptionsChangeListener(OptionsChangeListener ocl) {
        listeners.add(ocl);
    }

    public void removeOptionsChangeListener(OptionsChangeListener ocl) {
        listeners.remove(ocl);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(optionsObject.equals(evt.getNewValue())){
            return;
        }
        optionsObject = evt.getNewValue();
        for (int i = 0; i < fields.length; i++) {
            try {
                String v = String.valueOf(fields[i].get(optionsObject));
                OptionsChangeEvent oce = new OptionsChangeEvent(v, i, this);
                for (OptionsChangeListener ocl : listeners) {
                    ocl.optionsChanged(i, oce);
                }

            } catch (IllegalArgumentException | IllegalAccessException ex) {
                log.error("oh no! An Exception occurred!", ex);
            }
        }
        
    }

    private class Item {

        private Field field;
        private Class type;
        private String label;
        private String tooltip;
        double minV = Double.NaN;
        double maxV = Double.NaN;
        String error = null;

        private Item(Field f) {
            this.field = f;
            type = f.getType();

            Min min = (Min) field.getAnnotation(Min.class);
            if (min != null) {
                minV = min.value();
            }
            Max max = (Max) field.getAnnotation(Max.class);
            if (max != null) {
                maxV = max.value();
            }

            label = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
            Description desc = (Description) field.getAnnotation(Description.class);
            if (desc != null) {
                tooltip = desc.value();
            } else {
                tooltip = "Description is not available";
            }
        }

        public String getText() {
            try {
                return String.valueOf(field.get(optionsObject));
            } catch (IllegalArgumentException ex) {
                BusEvent be = new BusEvent(
                        "Illegal Argument Exception occurred when retrieving value from "
                        + optionClass.getSimpleName()
                        + ". Message: "
                        + ex.getMessage(),
                        BusEvent.ERROR_MESSAGE,
                        outer());
                MessageBus.getInstance(MessageBus.GPSK).fireEvent(be);

                return "Internal error";
            } catch (IllegalAccessException ex) {
                BusEvent be = new BusEvent(
                        "IllegalAccessException orccurred when retrieving value from "
                        + optionClass.getSimpleName()
                        + ". Message_ "
                        + ex.getMessage(),
                        BusEvent.ERROR_MESSAGE,
                        outer());
                return "Internal error";
            }
        }

        public String setValue(String v) throws Exception {
            if (type.equals(Integer.TYPE)) {
                try {
                    int x = parseInt(v);
                    if (!isNaN(minV)) {
                        if (x < minV) {
                            error = "the minimum value of " + label
                                    + "  is " + minV;
                            return error;
                        }
                    }
                    if (!isNaN(maxV)) {
                        if (x > maxV) {
                            error = "the maximum value of " + label
                                    + " is " + maxV;
                            return error;
                        }
                    }
                    field.setInt(optionsObject, x);
                    error = null;
                    field.set(optionsObject, x);
                    return null;
                } catch (NumberFormatException ex) {
                    error = label + " is not a number";
                    return error;
                }
            }

            if (type.equals(Long.TYPE)) {
                try {
                    long x = parseLong(v);
                    if (!isNaN(minV)) {
                        if (x < minV) {
                            error = "the minimum value of " + label
                                    + "  is " + minV;
                            return error;
                        }
                    }
                    if (!isNaN(maxV)) {
                        if (x > maxV) {
                            error = "the maximum value of " + label
                                    + " is " + maxV;
                            return error;
                        }
                    }
                    error = null;
                    field.set(optionsObject, x);
                    return null;
                } catch (NumberFormatException ex) {
                    return label + " is not a number";
                }
            }
            if (type.equals(Double.TYPE)) {
                
                try {
                    double x = parseDouble(v);
                    if (!isNaN(minV)) {
                        if (x < minV) {
                            error = "the minimum value of " + label
                                    + "  is " + minV;
                            return error;
                        }
                    }
                    if (!isNaN(maxV)) {
                        if (x > maxV) {
                            error = "the maximum value of " + label
                                    + " is " + maxV;
                            return error;
                        }
                    }
                    error = null;
                    field.set(optionsObject, x);
                    return null;

                } catch (NumberFormatException ex) {
                    error = label + " is not a number";
                    return error;
                }
            }
            if (type.equals(String.class)) {
                field.set(optionsObject, v);
                error = null;
                return null;

            } else {
                BusEvent be = new BusEvent(
                        "argument must be string, int, double, boolean or long",
                        BusEvent.ERROR_MESSAGE,
                        outer());
                MessageBus.getInstance(MessageBus.GPSK).fireEvent(be);
                throw new IllegalArgumentException("argument must be string, int, double, boolean or long");
            }
        }
    }
}
