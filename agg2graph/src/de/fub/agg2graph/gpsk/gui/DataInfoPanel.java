/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.KeyValuePair;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.gui.actions.RemoveEnvironmentAction;
import de.fub.agg2graph.gpsk.gui.actions.SaveGPXAction;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.swing.*;
import org.apache.log4j.Logger;


import static de.fub.agg2graph.gpsk.utils.UtilConstants.*;

import static java.awt.BorderLayout.PAGE_START;
import static java.awt.BorderLayout.CENTER;




/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class DataInfoPanel extends JPanel {
    private static Logger log = Logger.getLogger(DataInfoPanel.class);
    
    private NumberFormat nf = new DecimalFormat("#.##");


    private final JTable table = new JTable();
    
    private EnvironmentManager em = EnvironmentManager.getInstance();
    
    private GPSkEnvironment env = null;
    
    private final SaveGPXAction sga = new SaveGPXAction();
    private final RemoveEnvironmentAction rea = new RemoveEnvironmentAction();
    private final JButton saveButton;
    private final JButton removeButton;
    protected final ActionManager am = new ActionManager();
    
    protected final TableContent tableContent = new TableContent();

    public DataInfoPanel() {
        super(new BorderLayout());
        table.setModel(tableContent);
        add(table, PAGE_START);
        JPanel buttonPanel = new JPanel(new GridLayout(1,2));
        saveButton = new JButton(sga);
        removeButton = new JButton(rea);
        buttonPanel.add(removeButton);
        buttonPanel.add(saveButton);
        add(buttonPanel, CENTER);

    }
    
    
    
    /**
     * takes care of the content in the table.
     * It reacts to changes in the selection list and in the selected environment
     */
    private class TableContent implements TableModel, ListSelectionListener, Observer{
        
        
        private final List<KeyValuePair> rows = new LinkedList<>();
        private final List<TableModelListener> listeners = new LinkedList<>();

        @Override
        public int getRowCount() {
            return rows.size();
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int i) {
            if(i == 0){
                return "keys";
            } else {
                return "values";
            }
            
        }

        @Override
        public Class<?> getColumnClass(int i) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if(env.hasBounds()){
            	return rowIndex >= 9 && columnIndex == 1;
            } else {
            	return rowIndex >= 7 && columnIndex == 1;
            }
            
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            KeyValuePair pair = rows.get(rowIndex);
            if(columnIndex == 0){
                return pair.key;
            } else {
                return pair.value;
            }
            
        }

        @Override
        public void setValueAt(Object o, int rowIndex, int columnIndex) {
            ListIterator<KeyValuePair> it = rows.listIterator(rowIndex);
            KeyValuePair oldPair = it.next();
            KeyValuePair newPair;
            if(o instanceof String){
                newPair = new KeyValuePair(oldPair.key, (String) o);
            } else {
                newPair = new KeyValuePair(oldPair.key, o.toString());
            }
            it.set(newPair);
            update(rowIndex);
            
        }

        @Override
        public void addTableModelListener(TableModelListener tl) {
            listeners.add(tl);
        }

        @Override
        public void removeTableModelListener(TableModelListener tl) {
            listeners.remove(tl);
        }

        @Override
        public void valueChanged(ListSelectionEvent lse) {
            if(env != null){
                env.deleteObserver(this);
            }
            Object src = lse.getSource();
            JList<KeyValuePair> list = (JList<KeyValuePair>) src;
            KeyValuePair kt = list.getSelectedValue();
            if(kt == null){
                resetEntries();
                env = null;
                return;
            }
            env = em.getEnv(kt.key);
            if(!checkExistence(env)){
                env = null;
                return;
            }
            rows.clear();
            rows.add(new KeyValuePair("key", kt.key));
            rows.add(new KeyValuePair("title", env.getMeta().name));
            rows.add(new KeyValuePair("# segments", 
                    String.valueOf(env.getSegments().size())));
            rows.add(new KeyValuePair("# points",
                    String.valueOf(env.getPoints().size())));
            rows.add(new KeyValuePair("Has Bounds",
                    String.valueOf(env.hasBounds())));
            if(env.hasBounds()){
            	rows.add(new KeyValuePair("bounds.southWest",
            			String.valueOf(env.getBounds()[SOUTHWEST_LAT]) + "," 
            			+ String.valueOf(env.getBounds()[SOUTHWEST_LON])));
            	rows.add(new KeyValuePair("bounds.northEast",
            			String.valueOf(env.getBounds()[NORTHEAST_LAT]) + ","
            			+ String.valueOf(env.getBounds()[NORTHEAST_LON])));
            }
            rows.add(new KeyValuePair("Has Index(RTree)",
                    String.valueOf(env.hasPointIndex())));
            System.out.println("Length: " + env.getLength());
            rows.add(new KeyValuePair("length",
            		nf.format(env.getLength() ) + "m" ));
            
            Set<Entry<String, String>> set = env.getMeta().keyValue.entrySet();
            Iterator<Entry<String, String>> it = set.iterator();
            while(it.hasNext()){
                Entry<String, String> entry = it.next();
                rows.add(new KeyValuePair(
                        entry.getKey(),
                        entry.getValue()));
            }
            env.addObserver(this);
            update();
            
            
        }
        
        private void resetEntries(){
            rows.clear();
            KeyValuePair kv1 = new KeyValuePair("key", "not selected");
            KeyValuePair kv2 = new KeyValuePair("title", "not selected");
            rows.add(kv1);
            rows.add(kv2);
            update();
        }
        
        private boolean checkExistence(GPSkEnvironment env){
            if(env == null){
                rows.clear();
                KeyValuePair kv1 = new KeyValuePair("key", "does not exist");
                KeyValuePair kv2 = new KeyValuePair("title", "does not exist");
                rows.add(kv1);
                rows.add(kv2);
                update();
                return false;
            }
            return true;
            
        }
        
        private void update(){
            for(TableModelListener tml : listeners){
                tml.tableChanged(new TableModelEvent(this));
            }
        }
        
        private void update(int row){
            for(TableModelListener tml : listeners){
                tml.tableChanged(new TableModelEvent(this, row));
            }
        }
        
        private void update(int firstRow, int lastRow){
            for(TableModelListener tml : listeners){
                tml.tableChanged(new TableModelEvent(this, firstRow, lastRow));
            }
        }

        @Override
        public void update(Observable o, Object o1) {
            log.warn("update method is not implemented yet");
            
        }
    }
    
    private class ActionManager implements ListSelectionListener{

        @Override
        public void valueChanged(ListSelectionEvent lse) {
            JList<KeyValuePair> list = (JList<KeyValuePair>) lse.getSource();
            if(list.getSelectedValue() == null){
//                sga.setInput(null);
                rea.setKey(null);
                return;
            }
            String key = list.getSelectedValue().key;
//            sga.setInput(key);
            rea.setKey(key);
            saveButton.setAction(new SaveGPXAction(key));
        }
        
    }
}