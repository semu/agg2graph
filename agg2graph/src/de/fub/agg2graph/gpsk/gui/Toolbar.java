/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import javax.measure.quantity.Length;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JToolBar;

/**
 *
 * @author Christian Windolf
 */
public class Toolbar extends JToolBar{
    
    
    public Toolbar(){
        this(new Action[0]);
    }
    
    public Toolbar(Action ... actions){
        super("GPSk toolbar");
        for(int i = 0; i < actions.length; i++){
            add(new JButton(actions[i]));
        }
    }
}
