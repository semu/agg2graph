/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.gui;

import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.data.Filter;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.MutableComboBoxModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.apache.log4j.Logger;

import static javax.swing.event.ListDataEvent.*;

/**
 * 
 * @author Christian Windolf christianwindolf@web.de
 */
public class DropDownModel implements MutableComboBoxModel<String>, Observer {

	private static Logger log = Logger.getLogger(DropDownModel.class);
	private final EnvironmentManager em;
	private final List<String> keys;
	private final Filter<GPSkEnvironment> f;
	private final List<ListDataListener> listeners = new LinkedList<ListDataListener>();
	private int index = 0;

	public DropDownModel(Filter f) {
		this.f = f;
		em = EnvironmentManager.getInstance();
		keys = new LinkedList<>();
		for (String k : em.keys()) {
			GPSkEnvironment env = em.getEnv(k);
			if (f.filter(env)) {
				keys.add(k);

			}
		}
	}

	public DropDownModel() {
		this(new Filter.DefaultFilter<String>());
	}

	@Override
	public void addElement(String e) {
		GPSkEnvironment env = em.getEnv(e);
		for (String k : keys) {
			if (k.equals(e)) {
				return;
			}
		}
		if (f.filter(env)) {
			keys.add(e);
			update(keys.size() - 1, INTERVAL_ADDED);
		}
	}

	@Override
	public void removeElement(Object o) {
		int i = keys.indexOf(o);
		if (i != -1) {
			keys.remove(o);
			update(i, INTERVAL_REMOVED);
		}
	}

	@Override
	public void insertElementAt(String e, int i) {
		GPSkEnvironment env = em.getEnv(e);
		for (String k : keys) {
			if (k.equals(e)) {
				return;
			}
		}
		if (f.filter(env)) {
			keys.add(i, e);
			update(i, INTERVAL_ADDED);
		}
	}

	@Override
	public void removeElementAt(int i) {
		if (i < 0 || i >= keys.size()) {
			return;
		}
		keys.remove(i);
		update(i, INTERVAL_REMOVED);
	}

	@Override
	public void setSelectedItem(Object o) {
		int i = 0;
		for (String k : keys) {
			if (k.equals(o)) {
				index = i;
				return;
			}
			i++;
		}
		log.warn("tried to select an item that does not exist");
	}

	@Override
	public Object getSelectedItem() {
		if (keys.isEmpty())
			return null;
		try {
			return keys.get(index);
		} catch (IndexOutOfBoundsException ex) {
			index = 0;
			return keys.isEmpty() ? null : keys.get(index);

		}
	}

	@Override
	public int getSize() {
		return keys.size();
	}

	@Override
	public String getElementAt(int i) {
		if (i < 0 || i >= keys.size()) {
			throw new IndexOutOfBoundsException("Size of list is: "
					+ keys.size() + " requested index was: " + i);
		}
		return keys.get(i);
	}

	@Override
	public void addListDataListener(ListDataListener ll) {
		listeners.add(ll);
	}

	@Override
	public void removeListDataListener(ListDataListener ll) {
		listeners.remove(ll);
	}

	private void update(int index, int type) {
		update(index, index, type);
	}

	private void update(int index0, int index1, int type) {
		for (ListDataListener ldl : listeners) {
			ListDataEvent lde = new ListDataEvent(this, type, index0, index1);
			switch (type) {
			case CONTENTS_CHANGED:
				ldl.contentsChanged(lde);
				break;
			case INTERVAL_ADDED:
				ldl.intervalAdded(lde);
				break;
			case INTERVAL_REMOVED:
				ldl.intervalRemoved(lde);
				break;
			}
		}
	}

	@Override
	public void update(Observable o, Object o1) {
		if (o == null)
			return;
		if (!(o instanceof EnvironmentManager))
			return;
		if (o1 == null)
			return;
		if (!(o1 instanceof String))
			return;

		EnvironmentManager em = (EnvironmentManager) o;
		String key = (String) o1;
		GPSkEnvironment env = em.getEnv(key);
		if (env == null) {
			removeElement(key);
			return;
		}
		addElement(key);
	}
}
