/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import de.fub.agg2graph.gpsk.algorithms.optics.SteepClustering;
import de.fub.agg2graph.gpsk.algorithms.optics.SteepOptions;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.Filter;

/**
 * @author Christian Windolf
 *
 */
@Options(ESOptions.class)
public class EvalScript extends MultipleInputAlgorithm<GPSkEnvironment>{
	
	private NumberFormat nf = new DecimalFormat("#.###");

	
	
	/**
	 * @param names
	 * @param filter
	 */
	public EvalScript() {
		super(new String[]{
				"map data",
				"optics"
		}, new Filter[]{
				new Filter.DefaultFilter<GPSkEnvironment>(),
				new Filter.DefaultFilter<GPSkEnvironment>()
		});
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public GPSkEnvironment call() throws Exception {

		GPSkEnvironment map = (GPSkEnvironment) input[0];
		GPSkEnvironment optics = (GPSkEnvironment) input[1];
		ESOptions o = (ESOptions) options;
		File f = new File(o.outputFile);
		BufferedWriter bw = new BufferedWriter(new FileWriter(f));
		for(double xi = o.minXi; xi <= o.maxXi; xi += o.tickXi){
			SteepClustering sc = new SteepClustering();
			sc.setInput(optics);
			SteepOptions so = new SteepOptions();
			so.minPts = 15;
			so.xi = xi;
			sc.setOptions(so);
			GPSkEnvironment steep = sc.call();
			
			BoundaryCRRemover bcr = new BoundaryCRRemover();
			bcr.setOptions(new BCROptions());
			bcr.setInput(steep);
			GPSkEnvironment bc = bcr.call();
			
			CDEvaluation cde = new CDEvaluation();
			cde.setInput(bc, 1);
			cde.setInput(map, 0);
			cde.setOptions(new CDEvalOptions());
			cde.call();
			
			
			
			bw.newLine();
			bw.write(nf.format(xi).replace(",",".") + ", " + 
			nf.format(cde.detectionRate).replace(",", ".") + ", " + 
					nf.format(cde.errorRate).replace(",", ".") + "," + bc.getPoints().size() );
			
		}
		bw.flush();
		bw.close();
		return null;
	}

}
