/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.lang.reflect.Field;

import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.annotations.Max;
import de.fub.agg2graph.gpsk.annotations.Min;
import de.fub.agg2graph.gpsk.annotations.Prefix;

/**
 * A reimplementation of the {@link de.fub.agg2graph.input.CleaningOptions} from the original agg2graph project.
 * This class also contains annotations for the gui builders
 * @author Christian Windolf christianwindolf@web.de
 * 
 */
@Prefix(value = "clean")
public class CleaningOptions {
    // number of edges allowed per segment
    @Description(value = "make sure, the amount of edges in a segment is within bounds")
    public boolean filterBySegmentLength = false;
    
    @Description(value = "minimum amount of edges in a segment")
    @Min(value = 1)
    public long minSegmentLength = 1;
    
    @Description(value = "maximum amount of edges in a segment")
    @Min(value = 1)
    public long maxSegmentLength = Long.MAX_VALUE;
    // min/max distance between two points (meters)
    
    @Description(value = "filter out edges, that are too long or too short")
    public boolean filterByEdgeLength = true;
    
    @Description(value = "minimum length of an edge")
    @Min
    public double minEdgeLength = 0.1;
    
    @Description(value = "maximum length of an edge")
    @Min
    public double maxEdgeLength = 500;
    // length change between two consecutive edges
    public boolean filterByEdgeLengthIncrease = true;
    @Min
    public double minEdgeLengthIncreaseFactor = 10;
    @Min
    public double minEdgeLengthAfterIncrease = 30;
    // zigzag
    @Description(value = "remove zigzag patterns")
    public boolean filterZigzag = true;
    @Min
    @Max(value = 360)
    public double maxZigzagAngle = 30;
    // fake circle
    public boolean filterFakeCircle = true;
    @Min
    @Max(value = 360)
    public double maxFakeCircleAngle = 50;
    // outliers
    public boolean filterOutliers = true;
    @Min
    public int maxNumOutliers = 2;
    
    /**
     * converts the values in this object to an instance of the original {@link de.fub.agg2graph.input.CleaningOptions} class.
     * @return
     */
    public  de.fub.agg2graph.input.CleaningOptions convertToAgg2Graph(){
        de.fub.agg2graph.input.CleaningOptions co = 
                new de.fub.agg2graph.input.CleaningOptions();
        co.filterBySegmentLength = filterBySegmentLength;
        co.minSegmentLength = minSegmentLength;
        co.maxSegmentLength = maxSegmentLength;
        
        co.filterByEdgeLength = filterByEdgeLength;
        co.minEdgeLength = minEdgeLength;
        co.maxEdgeLength = maxEdgeLength;
        
        co.filterByEdgeLengthIncrease = filterByEdgeLengthIncrease;
        co.minEdgeLengthIncreaseFactor = minEdgeLengthIncreaseFactor;
        co.minEdgeLengthAfterIncrease = minEdgeLengthAfterIncrease;
        
        co.filterZigzag = filterZigzag;
        co.maxZigzagAngle = maxZigzagAngle;
        
        co.filterFakeCircle = filterFakeCircle;
        co.maxFakeCircleAngle = maxFakeCircleAngle;
        
        return co;
        
        
    }
    
    @Override
    public String toString(){
        Field[] fields = CleaningOptions.class.getFields();
        StringBuilder builder = new StringBuilder("Cleaning Options\n");
        for(Field f : fields){
            try {
                builder.append(f.getName()).append(": ").append(f.get(this)).append("\n");
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                builder.append(f.getName() + ": no information available");
            } 
            
        }
        return builder.toString();
    }
}
