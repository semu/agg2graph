/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import de.fub.agg2graph.gpsk.Main;

/**
 * Distributes  SIMD-tasks over several threads 
 *
 * @author Christian Windolf, christianwindolf@web.de
 * 
 * 
 */
public class ConcurrentDataRunner<T> implements ConcurrentDataFilter<T> {
    private ConcurrentDataFilter<T> filter;
//    public ConcurrentDataRunner(Concurrent)
    
    public ConcurrentDataRunner(ConcurrentDataFilter<T> filter){
        this.filter = filter;
    }

    /**
     * Distributes the tasks over several threads and merges them, when they are finished.
     */
    @Override
    public List<T> filter(List<T> t) throws Exception{
        int size = t.size();
        if(size <= 10){
            return filter.filter(t);
        } else {
            Task t1 = new Task(t.subList(0, size/2));
            Task t2 = new Task(t.subList(size/2, size));
            Future<List<T>> f = Main.executor.submit(t2);
            List<T> results = t1.call();
            results.addAll(f.get());
            return results;
        }
        
    }
    
    private class Task implements Callable<List<T>>{
        
        List<T> input;
        public Task(List<T> input){
            this.input = input;
        }

        @Override
        public List<T> call() throws Exception {
            return filter.filter(input);
            
        }
        
    }

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.algorithms.ConcurrentDataFilter#setID(int, int)
	 */
	@Override
	public void setID(int id, int total) {
		
	}
}
