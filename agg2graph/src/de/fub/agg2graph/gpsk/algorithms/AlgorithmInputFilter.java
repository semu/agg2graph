/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.HashMap;

import de.fub.agg2graph.gpsk.data.Filter;

/**
 * this class just has map from an algorithm to filters. It is important for GUI-builders in this application
 * @author Christian Windolf christianwindolf@web.de
 */
public class AlgorithmInputFilter {
    private static HashMap<Class, Filter> classToFilter = new HashMap<>();
    
    static{
        classToFilter.put(Normalizer.class, new Filter.Segment());
    }
    
    /**
     * Get a {@link Filter} for the input data
     * @param c
     * @return
     */
    public static Filter getFilter(Class c){
        Filter f = classToFilter.get(c);
        if(f == null){
            return new Filter.DefaultFilter<Object>();
        } else {
            return f;
        }
    }
}
