/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.logic.PointDistance;
import de.fub.agg2graph.gpsk.logic.TrigonometricDistance;
import de.fub.agg2graph.gpsk.utils.PointCollection;
import java.util.List;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public class GPSkCluster extends Cluster{
    private PointDistance pd = new TrigonometricDistance();
    
    protected double diameter;
    protected GPSkPoint centroid;

    public GPSkCluster(List<OPTICSPoint> list) {
        super((List<OPTICSObject>)(List<?>)list);
        double[] bounds = PointCollection.getBounds((List<GPSkPoint>)(List<?>) list);
        GPSkPoint southWest = new GPSkPoint(bounds[0], bounds[1]);
        GPSkPoint northEast = new GPSkPoint(bounds[2], bounds[3]);
        diameter = pd.calculate(southWest, northEast);
        
        double lat_sum = 0;
        double lon_sum = 0;
        
        for(OPTICSPoint p : list){
            lat_sum += p.getLat();
            lon_sum += p.getLon();
        }
        
        double lat = lat_sum / list.size();
        double lon = lon_sum / list.size();
        
        centroid = new GPSkPoint(lat, lon);
    }
    
    public GPSkCluster(Cluster c){
        this((List<OPTICSPoint>) (List<?>) c.getList());
    }

    public double getDiameter() {
        return diameter;
    }

    public GPSkPoint getCentroid() {
        return centroid;
    }
    
    
    
    
}
