/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Christian Windolf
 *
 */
public class Cluster {

    protected final List<OPTICSObject> list;
    protected final int startIndex;
    protected final int endIndex;

    /**
     *
     */
    public Cluster(List<OPTICSObject> list) {
        this.list = list;
        startIndex = list.get(0).getIndex();
        endIndex = startIndex + list.size() - 1;
    }

    public List<OPTICSObject> getList() {
        return list;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.startIndex;
        hash = 59 * hash + this.endIndex;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cluster other = (Cluster) obj;
        if (this.startIndex != other.startIndex) {
            return false;
        }
        if (this.endIndex != other.endIndex) {
            return false;
        }
        return true;
    }
    
    public boolean contains(Cluster c){
    	return c.startIndex >= this.startIndex && c.endIndex <= this.endIndex;
    }
    
    public boolean isMergable(Cluster c, int minPts){
    	if(c.endIndex < this.startIndex || this.endIndex < c.startIndex){
    		return false;
    	}
    	return Math.abs(c.endIndex - this.endIndex) <= minPts 
    			&& Math.abs(c.startIndex - c.startIndex) <= minPts;
    }
    
    public Cluster merge(Cluster c){
    	if(c.endIndex < this.startIndex || this.endIndex < c.startIndex){
    		throw new IllegalArgumentException("These clusters do not overlap each other! (" 
    				+ this.startIndex + "," + this.endIndex + ") ("
    				+ c.startIndex + "," + c.startIndex);
    	}
    	List<OPTICSObject> resultList = new LinkedList<>();
    	
    	if(this.startIndex <= c.startIndex){
    		resultList.addAll(list);
    		if(this.endIndex >= c.endIndex){
    			return new Cluster(resultList);
    		} else {
    			int endDiff = c.endIndex - this.endIndex;
    			ListIterator<OPTICSObject> it = c.list.listIterator(c.list.size() - endDiff);
    			while(it.hasNext()){
    				resultList.add(it.next());
    			}
    			return new Cluster(resultList);
    		}
    	} else{
    		/*
    		 * the start index of the other is cluster is before this cluster.
    		 * 
    		 */
    		resultList.addAll(c.list);
    		if(this.endIndex <= c.endIndex){
    			return new Cluster(resultList);
    			
    		} else {
    			int endDiff = this.endIndex - c.endIndex;
    			ListIterator<OPTICSObject> it = this.list.listIterator(this.list.size() - endDiff);
    			while(it.hasNext()){
    				resultList.add(it.next());
    			}
    			return new Cluster(resultList);
    		}
    		
    	}
    	
    }
    
    
    
    
    @Override
    public String toString(){
        return "Cluster{" + startIndex + "," + endIndex + "}";
    }
    
}
