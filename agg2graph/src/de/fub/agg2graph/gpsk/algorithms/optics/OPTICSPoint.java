/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.structs.AbstractLocation;
import de.fub.agg2graph.structs.GPSPoint;
import java.util.Comparator;

/**
 *
 * @author Christian Windolf
 */
public class OPTICSPoint extends GPSkPoint implements OPTICSObject{
    private static final double UNDEFINED = Double.POSITIVE_INFINITY;
    
    private double reachabilityDistance;
    
    private boolean processed;
    
    protected int index = -1;
    
    protected boolean up = false;
    
    protected boolean down = false;
    
    public OPTICSPoint(GPSPoint point){
        super(point.getID(), point.getLat(), point.getLon());
        reachabilityDistance = UNDEFINED;
        processed = false;
    }
    
    public OPTICSPoint(GPSPoint point, String id){
        super(id, point.getLat(), point.getLon());
        reachabilityDistance = UNDEFINED;
        processed = false;
    }
    
    public OPTICSPoint(GPSPoint point, long id){
        super(String.valueOf(id), point.getLat(), point.getLon());
        reachabilityDistance = UNDEFINED;
        processed = false;
    }
    
    public OPTICSPoint(double lat, double lon, String id){
        super(id, lat, lon);
        processed = false;
        reachabilityDistance = UNDEFINED;
    }
    
    public OPTICSPoint(double lat, double lon){
        super(lat, lon);
        processed = false;
        reachabilityDistance = UNDEFINED;
    }
    
    public OPTICSPoint(GPSkPoint p, double originLat, double originLon){
        super(p, new double[]{originLat, originLon});
        reachabilityDistance = UNDEFINED;
        processed = false;
        
    }
    
    public OPTICSPoint(GPSkPoint p){
        super(p);
        reachabilityDistance = UNDEFINED;
        processed = false;
    }
    
    @Override
    public void setReachabilityDistance(double distance){
        this.reachabilityDistance = distance;
    }
    
    @Override
    public double getReachabilityDistance(){
        return reachabilityDistance;
    }
    
    public void setUndefined(){
        reachabilityDistance = UNDEFINED;
    }
    
    @Override
    public boolean isUndefined(){
        return reachabilityDistance == UNDEFINED;   
    }
    
    public void markProcessed(){
        processed = true;
    }
    
    public boolean isProcessed(){
        return processed;
    }

    @Override
    public int compareTo(AbstractLocation al) {
    	OPTICSPoint p = (OPTICSPoint) al;
        if(this.reachabilityDistance < p.reachabilityDistance){
            return - 1;
        } else if (this.reachabilityDistance == p.reachabilityDistance){
            return 0;
        } else {
            return 1;
        }
    }
    
    @Override
    public String toString(){
        StringBuffer buffer = new StringBuffer("OPTICS Point: ");
        buffer.append(ID).append(" lat: ").append(latlon[0]).append(" lon: ");
        buffer.append(latlon[1]).append(" rDist: ");
        if(reachabilityDistance == UNDEFINED){
            buffer.append("UNDEFINED");
        } else{
            buffer.append(reachabilityDistance);
        }
        return new String(buffer);
    }



    @Override
    public boolean isUpPoint() {
        return up;
    }

    @Override
    public boolean isDownPoint() {
        return down;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public void setUpPoint(boolean b) {
        this.up = b;
    }

    @Override
    public void setDownPoint(boolean b) {
        this.down = b;
    }
}
