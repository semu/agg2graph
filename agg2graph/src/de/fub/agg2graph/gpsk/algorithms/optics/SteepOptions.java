/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.annotations.Max;
import de.fub.agg2graph.gpsk.annotations.Min;
import de.fub.agg2graph.gpsk.annotations.Prefix;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@Prefix("steep")
public class SteepOptions {
    
    @Min(0)
    @Description("Should be the same value as used for the OPTICS computation.")
    public int minPts = 5;
    
    @Min(0)
    @Max(1)
    @Description("How steep should a dent be in the rd-diagramm? The lower the value, the less clusters will be found")
    public double xi = 0.01;
    
    
    @Min(0)
    @Description("maximum diameter for crossroads. A high value causes less crossroads to be found")
    public double maxDiameter = 100;
    
    
    @Override
    public String toString(){
    	return "Steep Options{minPts=" + minPts + ", xi=" + xi + "}";
    }


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(maxDiameter);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + minPts;
		temp = Double.doubleToLongBits(xi);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SteepOptions other = (SteepOptions) obj;
		if (Double.doubleToLongBits(maxDiameter) != Double
				.doubleToLongBits(other.maxDiameter))
			return false;
		if (minPts != other.minPts)
			return false;
		if (Double.doubleToLongBits(xi) != Double.doubleToLongBits(other.xi))
			return false;
		return true;
	}


	
}
