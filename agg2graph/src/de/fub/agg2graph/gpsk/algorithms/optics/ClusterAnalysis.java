/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * The ClusterAnalysis class is neither an algorithm nor an options class. It's
 * purpose is to support the SteepClustering-algorithm
 * 
 * @author Christian Windolf
 * 
 */
public class ClusterAnalysis {

	protected ArrayList<OPTICSObject> list;
	protected final double xi;
	protected final int minPts;
	protected List<UpSteepArea> usaSet = new LinkedList<>();
	protected List<DownSteepArea> dsaSet = new LinkedList<>();
	protected Set<Cluster> clusterSet = new HashSet<>();

	public static List<Set<Cluster>> extractClusters(List<OPTICSObject> input,
			double xi, int minPts) {
		Iterator<OPTICSObject> iterator = input.iterator();
		List<Set<Cluster>> resultList = new LinkedList<>();
		while (iterator.hasNext()) {
			OPTICSObject oo = iterator.next();
			if (!Double.isInfinite(oo.getReachabilityDistance())) {
				List<OPTICSObject> list = new LinkedList<OPTICSObject>();
				list.add(oo);
				while (iterator.hasNext()
						&& !Double.isInfinite(oo.getReachabilityDistance())) {
					oo = iterator.next();
					if (!Double.isInfinite(oo.getReachabilityDistance())) {
						list.add(oo);
					}
				}
				if (list.size() > minPts) {
					ClusterAnalysis ca = new ClusterAnalysis(list, xi, minPts);
					ca.detectSteepPointsAndSetIndizes();
					ca.detectClusters();
					resultList.add(ca.clusterSet);
				}
			}
		}

		return resultList;
	}

	/**
	 * <p>
	 * This constructor creates a copy of the input list. Since the list may be
	 * created by a sublist()-call, there might be unexpected behavior, if this
	 * class can't work on this class as it likes to.
	 * </p>
	 * 
	 * @param list
	 *            it must not contain objects with a reachability distance that
	 *            is
	 *            <ul>
	 *            <li>negative</li>
	 *            <li>infinite</li>
	 *            <li>not a number (Double.NaN)</li>
	 *            </ul>
	 * @param so
	 *            the options that should be used while cluster detection
	 */
	public ClusterAnalysis(List<OPTICSObject> list, double xi, int minPts) {
		this.xi = xi;
		this.minPts = minPts;
		if (list.size() <= minPts) {
			throw new IllegalArgumentException(
					"list must have at least minPts elements");
		}
		for (OPTICSObject oo : list) {
			double r = oo.getReachabilityDistance();
			if (Double.isInfinite(r)) {
				throw new IllegalArgumentException(
						"rechabilityDistance is infinite. This class is not designed for data like that");
			}
			if (Double.isNaN(r)) {
				throw new IllegalArgumentException(
						"reachabilityDistance is NaN.");
			}
			if (r < 0) {
				throw new IllegalArgumentException(
						"reachabilityDistance is negative. This can't be a valid result of OPTICS");
			}
		}
		this.list = new ArrayList<>(list);
	}

	public void detectSteepPointsAndSetIndizes() {
		ListIterator<OPTICSObject> iterator = list.listIterator();
		int index = iterator.nextIndex();
		OPTICSObject first = iterator.next();
		first.setIndex(index);
		while (iterator.hasNext()) {
			index = iterator.nextIndex();
			OPTICSObject second = iterator.next();
			second.setIndex(index);
			if (first.getReachabilityDistance() <= (1 - xi)
					* second.getReachabilityDistance()) {
				first.setUpPoint(true);
			} else if (first.getReachabilityDistance() * (1 - xi) >= second
					.getReachabilityDistance()) {
				first.setDownPoint(true);
			}
			first = second;
		}
	}

	public void detectClusters() {
		/*
		 * the current index...
		 */
		int index = 0;

		/*
		 * maximum in between. It is the maximum between the last end point of
		 * the last area (no matter what kind of steep area it is. Up or Down
		 * does not matter)
		 */
		double global_mib = 0;

		while (index < list.size()) {
			OPTICSObject p = list.get(index);

			global_mib = Math.max(global_mib, p.getReachabilityDistance());

			if (p.isDownPoint()) {
				DownSteepArea dsa = new DownSteepArea(index);
				updateAndFilter(global_mib);
				dsa.setMib(0);
				dsaSet.add(dsa);
				index = dsa.endPoint + 1;
				global_mib = list.get(index).getReachabilityDistance();
			} else if (p.isUpPoint()) {
				UpSteepArea usa = new UpSteepArea(index);
				updateAndFilter(global_mib);
				index = usa.endPoint + 1;
				createClusters(usa);
			} else {
				index++;
			}
			list.get(list.size() - 1).setUpPoint(true);

		}
	}

	private void updateAndFilter(double global_mib) {
		Iterator<DownSteepArea> dsaIterator = dsaSet.iterator();
		while (dsaIterator.hasNext()) {
			DownSteepArea dsa = dsaIterator.next();
			if (list.get(dsa.startPoint).getReachabilityDistance() * (1 - xi) <= global_mib) {
				dsaIterator.remove();

			} else {
				dsa.setMib(Math.max(dsa.getMib(), global_mib));
			}
		}
	}

	private void createClusters(UpSteepArea usa) {
		for (DownSteepArea dsa : dsaSet) {
			int reachStart_index = dsa.startPoint;
			double reachStart_rd = list.get(reachStart_index)
					.getReachabilityDistance();
			int reachEnd_index = usa.endPoint;
			double reachEnd_rd = list.get(reachEnd_index)
					.getReachabilityDistance();
			if (reachEnd_rd * (1 - xi) >= dsa.getMib()) {

				if (Math.abs(reachEnd_rd - reachStart_rd) <= (xi * reachEnd_rd)) {
					if (reachEnd_index - dsa.endPoint > minPts) {
						clusterSet.add(new Cluster(list.subList(dsa.startPoint,
								reachEnd_index + 1)));
					}
				} else if (reachStart_rd > reachEnd_rd) {

					int start = findStartPoint(dsa, usa);
					if (reachEnd_index - start > minPts) {
						clusterSet.add(new Cluster(list.subList(start,
								reachEnd_index + 1)));
					}
				} else {
					// start of cluster
					int soc = dsa.startPoint;
					// end of cluster
					int eoc = findEndPoint(dsa, usa);
					if (eoc - soc > minPts) {
						clusterSet.add(new Cluster(list.subList(soc, eoc + 1)));
					}

				}
			}
		}
	}

	private int findStartPoint(DownSteepArea dsa, UpSteepArea usa) {
		int index = dsa.startPoint;
		double reachEnd = list.get(usa.endPoint).getReachabilityDistance();
		double diff = Math.abs(list.get(index).getReachabilityDistance()
				- reachEnd);
		while (index < dsa.endPoint) {
			index++;
			double diff2 = Math.abs(list.get(index).getReachabilityDistance()
					- reachEnd);
			if (diff2 > diff) {
				return index - 1;
			}
			diff = diff2;
		}
		return index;
	}

	private int findEndPoint(DownSteepArea dsa, UpSteepArea usa) {
		int index = usa.startPoint;
		double reachStart = list.get(dsa.startPoint).getReachabilityDistance();
		double diff = (reachStart - list.get(index).getReachabilityDistance());
		while (index < usa.endPoint) {
			index++;
			double diff2 = Math.abs(list.get(index).getReachabilityDistance()
					- reachStart);
			if (diff2 > diff) {
				return index - 1;
			}
			diff = diff2;
		}
		return index;
	}

	protected class UpSteepArea {

		int startPoint;
		int endPoint;
		double mib;

		UpSteepArea(int startPoint) {
			this.startPoint = startPoint;
			extend();
		}

		void extend() {
			int index = startPoint + 1;
			int tmp_end = startPoint;
			int nonSteepPoints = 0;
			while (nonSteepPoints < minPts && index < list.size()) {
				if (list.get(index).isUpPoint()) {
					nonSteepPoints = 0;
					tmp_end = index;
				} else if (list.get(index - 1).getReachabilityDistance() <= list
						.get(index).getReachabilityDistance()) {
					nonSteepPoints++;
				} else {
					break;
				}
				index++;
			}
			endPoint = tmp_end;
		}

		/**
		 * @return the mib
		 */
		public double getMib() {
			return mib;
		}

		/**
		 * @param mib
		 *            the mib to set
		 */
		public void setMib(double mib) {
			this.mib = mib;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + endPoint;
			result = prime * result + startPoint;
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			UpSteepArea other = (UpSteepArea) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (endPoint != other.endPoint) {
				return false;
			}
			if (startPoint != other.startPoint) {
				return false;
			}
			return true;
		}

		private ClusterAnalysis getOuterType() {
			return ClusterAnalysis.this;
		}

		@Override
		public String toString() {
			return "Steep Up Area (start=" + startPoint + "|end=" + endPoint
					+ "|mib=" + mib + ")";
		}
	}

	private class DownSteepArea {

		int startPoint;
		int endPoint;
		double mib;

		DownSteepArea(int startPoint) {
			this.startPoint = startPoint;
			extend();
		}

		void extend() {
			int index = startPoint + 1;
			int tmp_end = startPoint;
			int nonSteepPoints = 0;
			while (nonSteepPoints < minPts && index < list.size()) {
				if (list.get(index).isDownPoint()) {
					nonSteepPoints = 0;
					tmp_end = index;
				} else {
					double rd_before = list.get(index - 1)
							.getReachabilityDistance();
					double rd_this = list.get(index).getReachabilityDistance();
					if (rd_before >= rd_this) {
						nonSteepPoints++;
					} else {
						break;
					}
				}
				index++;
			}
			endPoint = tmp_end;

		}

		/**
		 * mib = middle in between
		 * 
		 * @return
		 */
		double getMib() {
			return mib;
		}

		void setMib(double mib) {
			this.mib = mib;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + endPoint;
			long temp;
			temp = Double.doubleToLongBits(mib);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + startPoint;

			return result;

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			ClusterAnalysis.DownSteepArea other = (ClusterAnalysis.DownSteepArea) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (endPoint != other.endPoint) {
				return false;
			}
			if (Double.doubleToLongBits(mib) != Double
					.doubleToLongBits(other.mib)) {
				return false;
			}
			if (startPoint != other.startPoint) {
				return false;
			}
			return true;
		}

		private ClusterAnalysis getOuterType() {
			return ClusterAnalysis.this;
		}

		public String toString() {
			return "Steep Down Area (start=" + startPoint + "|end=" + endPoint
					+ "|mib=" + mib + ")";
		}
	}
}
