/*
 * Copyright 2013 Christian Windolf.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.data.CircleProcedure;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.data.IndexProcedure;

import java.util.LinkedList;
import java.util.List;


import java.util.PriorityQueue;
import org.apache.log4j.Logger;


import java.util.*;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.TYPE;
import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.LAYER_VISIBLE;
import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_POINTS;
import static java.lang.Math.max;
import java.util.Map.Entry;
import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 *
 * @author Christian Windolf
 */
public class Algorithm extends Observable implements Runnable{

    private static Logger log = Logger.getLogger(Algorithm.class);
    public static final String EPSKEY = "optics.eps";
    public static final String MINPTSKEY = "optics.minPts";
    public static final double UNDEFINED = Double.POSITIVE_INFINITY;
    public static final double DEFAULT_EPS = 30;
    public static final int DEFAULT_MINPTS = 10;
    
    private final String input;
    private final String output;
    
    private final EnvironmentManager em;
    private final GPSkEnvironment env;
    
    public Algorithm(String input, String output){
        this.input = input;
        this.output = output;
        em = EnvironmentManager.getInstance();
        env = em.getEnv(input);
        if(env == null){
            throw new NullPointerException("There is no data with the key " + input + "!");
        }
    }


    public void run() {
        EnvironmentManager em = EnvironmentManager.getInstance();
        GPSkEnvironment env = em.getEnv(input);
        if(env == null){
            showMessageDialog(Main.mainWindow, 
                    "The environment manager does not have any entry like " + input);
            return;
        }

        
        long milliseconds_at_start = System.currentTimeMillis();
        
        double eps = 0;
        int minPts = 0;
        try {
            eps = KeyValueStore.getDouble(EPSKEY, DEFAULT_EPS);
            minPts = KeyValueStore.getInt(MINPTSKEY, DEFAULT_MINPTS);

        } catch (NumberFormatException ex) {
            showMessageDialog(Main.mainWindow, "Invalid values for epsilon and minPts. Please reconfigure the OPTICS settings",
                    "Error", ERROR_MESSAGE);
            return;
        }
        if(!env.hasBounds()){
            env.detectBounds();
        }
        
        //set reachability distance undefined...
        Neighbourhood nb = Neighbourhood.build(env, eps, minPts);
        if(!nb.activatePointIndex()){
            log.error("failed to create index");
            return;
        }
        List<OPTICSPoint> outputList = new LinkedList<>();

        List<OPTICSPoint> pointsList = (List<OPTICSPoint>) (List<?>) nb.getPoints();
        
        for (int i = 0; i < pointsList.size(); i++) {
            OPTICSPoint oPoint = pointsList.get(i);
            
            //for each unprocessed point p...
            //so we need to jump over all processed points...
//            System.out.println("===============================================");
//            System.out.println("\tPoint " + oPoint.getID() + " - index: " + i);
            if (oPoint.isProcessed()) {
//                System.out.println("\tis already processed and ignored");
//                System.out.println("===============================================");
//                i++;
            } else {
                setChanged();
                notifyObservers(new StatusUpdate(pointsList.size(), i));

                List<OPTICSPoint> neighbours = nb.getNeighbours(oPoint);
                
//                System.out.println("\thas " + neighbours.size() + " neighbours");
                //add method automatically marks the point as processed
                outputList.add(oPoint);
                oPoint.markProcessed();
//                System.out.println("\tputted to output");

                PriorityQueue<OPTICSPoint> seeds = new PriorityQueue<>();
                double coredist = nb.calcCoreDist(oPoint);
//                System.out.println("core distance: " + coredist);
                if (coredist != UNDEFINED) {
//                    System.out.println("updating");
                    update(seeds, oPoint, neighbours, nb);

//                    System.out.println("\tSeeds size: " + seeds.size());

                    while (!seeds.isEmpty()) {
//                        System.out.println("===============================================");
                        OPTICSPoint o = seeds.poll();
//                        System.out.println("\tchecking " + o + " from prioriy queue");
                        List<OPTICSPoint> n2 = nb.getNeighbours(o);
                        if (!o.isProcessed()) {
                            outputList.add(o);
                            o.markProcessed();

//                        System.out.println("putted to output");
                            if (nb.calcCoreDist(o, n2) != UNDEFINED) {
//                            System.out.println("updated");

                                update(seeds, o, n2, nb);
//                            System.out.println("\tSeeds.size(): " + seeds.size());
                            }
                        }
                    }
                } else {
//                    System.out.println("\tcoredistance is undefined");
//                    System.out.println("===============================================");
                }
            }
        }
        long milliseconds_at_end = System.currentTimeMillis();
        long seconds = (milliseconds_at_end - milliseconds_at_start) / 1000;
        long milliseconds = (milliseconds_at_end - milliseconds_at_start) % 1000;
        log.info("OPTICS calculated " + outputList.size() + " points in " + seconds + " seconds and "
                + milliseconds + " milliseconds");
        EnvironmentBuilder builder = new EnvironmentBuilder(outputList);
        builder.setBounds(nb.getBounds());
        builder.setTitle("OPTICS results").setFocus(FOCUS_ON_POINTS);
        for(Entry<String, String> entry : env.getMeta().keyValue.entrySet()){
            builder.keyValue.put(entry.getKey(), entry.getValue());
        }
        builder.keyValue.put("eps", String.valueOf(eps));
        builder.keyValue.put("minPts", String.valueOf(minPts));
        builder.keyValue.put("computation time (milliseconds)", String.valueOf(
                milliseconds_at_end - milliseconds_at_start));
        builder.keyValue.put(TYPE, "optics");
        builder.keyValue.put(LAYER_VISIBLE, "false");
        
        em.put(output, builder.build());
    }

    public static void update(PriorityQueue<OPTICSPoint> queue,
            OPTICSPoint p,
            List<OPTICSPoint> neighbours,
            Neighbourhood nb) {

        double coredist = nb.calcCoreDist(p, neighbours);
        for (OPTICSPoint n : neighbours) {
            
            if (!n.isProcessed()) {
                double d = p.distanceTo(n);
                double new_reachdist = max(coredist, d);

                if (n.getReachabilityDistance() == UNDEFINED) {
//                    System.out.println("new reachD: " + new_reachdist);
                    n.setReachabilityDistance(new_reachdist);
                    if (n.isProcessed()) {
                        throw new IllegalStateException("Processed Point in queue!");
                    }
                    queue.offer(n);
                } else {

                    if (new_reachdist < n.getReachabilityDistance()) {
                        int x = queue.size();

//                        System.out.println("update readD: " + new_reachdist);
                        n.setReachabilityDistance(new_reachdist);
                        //java priority queue does not provide an update method
                        //this an ugly workaround...
                        if (n.isProcessed()) {
                            throw new IllegalStateException("Processed Point in queue!");
                        }
                        queue.remove(n);
                        queue.offer(n);
//
//                        if (x != queue.size()) {
//                            System.out.println("ALERT! Different size after updating. Size before update: " + x + " and after: " + queue.size());
//                        } else {
//                            System.out.println("went well. Queue size: " + queue.size());
//                        }
                    }
                }
            }

        }
    }

    private static class Neighbourhood extends GPSkEnvironment {

        
        private final double eps;
        private final int minPts;

        Neighbourhood(ArrayList<GPSkPoint> pointList, double[] bounds, double eps, int minPts) {
            super(pointList, bounds);
            this.eps = eps;
            this.minPts = minPts;
        }
        
        public static Neighbourhood build(GPSkEnvironment env, double eps, int minPts){
            ArrayList<OPTICSPoint> list = new ArrayList<OPTICSPoint>(env.getPoints().size());
            Iterator<GPSkPoint> iter = env.getPoints().iterator();
            
            
            if(!env.hasBounds()){
                env.detectBounds();
            }
            double bounds[] = env.getBounds();
            
            while(iter.hasNext()){
                GPSkPoint p = iter.next();
                list.add(new OPTICSPoint(p, bounds[0], bounds[1]));
            }
            return new Neighbourhood((ArrayList<GPSkPoint>) (List<?>) list, 
                    bounds, eps, minPts);
        }
        
        double calcCoreDist(OPTICSPoint point){
        	IndexProcedure<GPSkPoint> ip = new CircleProcedure(point, eps);
        	processPoints(ip);
            List<GPSkPoint> neighbours = ip.getResults();
            return calcCoreDist(point, (List<OPTICSPoint>) (List<?>) neighbours);
        }

        double calcCoreDist(OPTICSPoint point, List<OPTICSPoint> neighbours) {
            
            double[] distances = new double[minPts];
            Arrays.fill(distances, Double.MAX_VALUE);
            for(GPSkPoint p : neighbours){
                if(p.getLat() != point.getLat() || p.getLon() != point.getLon()){
                    double d = p.distanceTo(point);
                    if(distances[minPts - 1] > d && d < eps){
                        distances[minPts - 1] = d;
                        Arrays.sort(distances);
                    }
                }
            }
            
            if(distances[minPts - 1] == Double.MAX_VALUE){
                return UNDEFINED;
            } else {
                return distances[minPts - 1];
            }
        }
        
        
        
        List<OPTICSPoint> getNeighbours(OPTICSPoint p){
        	IndexProcedure<GPSkPoint> ip = new CircleProcedure(p, eps);
        	processPoints(ip);
            List<GPSkPoint> neighbours = ip.getResults();
            return (List<OPTICSPoint>) (List<?>) neighbours;
            
        }
    }
    
    public static class StatusUpdate{
        public final int allPoints;
        public final int calculatedPoints;
        
        public StatusUpdate(int allPoints, int calculatedPoints){
            this.allPoints = allPoints;
            this.calculatedPoints = calculatedPoints;
        }
        
        @Override
        public String toString(){
            StringBuilder sb = new StringBuilder("All Points: ");
            sb.append(allPoints).append(" | Already calculated points ").append(calculatedPoints);
            return new String(sb);
            
        }
    }
}
