/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm;
import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@Options(OPTICSoptions.class)
@Description("A pre processing step to detect crossroads")
public class OPTICS extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment>{
    private static Logger log = Logger.getLogger(OPTICS.class);

    public OPTICS(GPSkEnvironment input) {
        super(input);
    }

    public OPTICS() {
    }

    
    @Override
    public GPSkEnvironment call() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
