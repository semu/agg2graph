/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms.optics;

import de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.logic.PointDistance;
import de.fub.agg2graph.gpsk.logic.TrigonometricDistance;
import de.fub.agg2graph.gpsk.utils.PointCollection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * 
 * @author Christian Windolf, christianwindolf@web.de
 */
@Options(SteepOptions.class)
public class SteepClustering extends
		AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> {

	private static Logger log = Logger.getLogger(SteepClustering.class);
	
	

	public SteepClustering(GPSkEnvironment input) {
		super(input);
	}

	public SteepClustering() {
		super();
	}

	@Override
	public GPSkEnvironment call() throws Exception {

		SteepOptions so = (SteepOptions) options;
		log.info("steep clustering with xi=" + so.xi + "; minPts= " + so.minPts);
		List<OPTICSPoint> pointList = (List<OPTICSPoint>) (List<?>) input
				.getPoints();
		if (pointList.size() <= so.minPts) {
			throw new IllegalArgumentException(
					"not enough points in the data set");
		}
		
		
		List<GPSkPoint> points = new LinkedList<>();
		List<Set<Cluster>> clusters = ClusterAnalysis.extractClusters((List<OPTICSObject>) (List<?>) input.getPoints(), 
				so.xi, so.minPts);
		for(Set<Cluster> set : clusters){
			points.addAll(extractPoint(set));
		}
		EnvironmentBuilder builder = new EnvironmentBuilder(points);
		builder.setTitle("crossroads by steep clustering");
		
		builder.setBounds(input.hasBounds() ? input.getBounds() : PointCollection.getBounds(points));
		builder.setFocus(GPSkEnvironment.MetaInformation.FOCUS_ON_POINTS);
                builder.keyValue.put("type", "crossroads");
		return builder.build();
		
		
		

		
	}
	
	private List<GPSkPoint> extractPoint(Set<Cluster> clusterSet){
		List<GPSkPoint> points = new LinkedList<>();
		List<Cluster> list = new ArrayList<>(clusterSet);
		
		
		for(int i = list.size() -1 ; i >= 0; i--){
			boolean shouldBeAdded = true;
			for(int j = i -1; j >= 0; j--){
				if(list.get(i).contains(list.get(j))){
					shouldBeAdded = false;
					break;
				}
			}
			if(shouldBeAdded){
				GPSkCluster gc = new GPSkCluster(list.get(i));
				points.add(gc.centroid);
			}
		}
		return points;
	}
	
	
}
