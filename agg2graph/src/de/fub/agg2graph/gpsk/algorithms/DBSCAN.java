/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_SEGMENTS;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.apache.log4j.Logger;

import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.data.DefaultSegmentProcedure;
import de.fub.agg2graph.gpsk.data.IndexProcedure;
import de.fub.agg2graph.gpsk.logic.AggregationFactory;
import de.fub.agg2graph.gpsk.logic.ClusterAggregation;
import de.fub.agg2graph.gpsk.logic.FrechetFactory;

/**
 * <h3>Implementation of the DBSCAN algorithm</h3>
 * <p>
 * This class does not only implement the algorithm on GPS-traces. It also
 * aggregates them.
 * </p>
 * <p>
 * As distance function it just uses, what the {@link FrechetFactory} returns.
 * </p>
 * <p>
 * For the aggregation it uses, what the {@link AggregationFactory} returns.
 * </p>
 * <p>
 * The options can be set up via the {@link DBSCANoptions} class.
 * 
 * @author Christian Windolf christianwindolf@web.de
 * @see <a
 *      href="http://www.dbs.ifi.lmu.de/Publikationen/Papers/KDD-96.final.frame.pdf">DBSCAN
 *      Paper</a>
 */
@Options(DBSCANoptions.class)
@Description("extracts cluster from a dataset and aggregates them")
public class DBSCAN extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> {
	private static Logger log = Logger.getLogger(DBSCAN.class);

	public DBSCAN() {
	}

	public DBSCAN(GPSkEnvironment input) {
		super(input);
	}

	@Override
	public GPSkEnvironment call() throws Exception {
		DBSCANoptions o = (DBSCANoptions) options;
		List<Cluster> clusters = new LinkedList<>();
		List<GPSkSegment> segmentList = new LinkedList<>();
		for (GPSkSegment seg : input.getSegments()) {
			segmentList.add(new Segment(seg));
		}
		EnvironmentBuilder builder = new EnvironmentBuilder(segmentList);
		builder.setBounds(input.getBounds()).useProjection(
				input.getProjection());

		GPSkEnvironment modified = builder.build();
		if (!modified.activateSegmentIndex()) {
			log.error("failed to create an index!");
			throw new IllegalStateException("I cannot work without an index!");
		}

		List<GPSkSegment> segmentList2 = modified.getSegments();
		for (ListIterator<GPSkSegment> it = segmentList2.listIterator(); it
				.hasNext();) {
			Segment s = (Segment) it.next();
			if (!s.visited) {
				s.visited = true;
				IndexProcedure<GPSkSegment> ip = new SegmentProcedure(s, o.eps);
				modified.processSegments(ip);
				List<GPSkSegment> n = ip.getResults();

				if (n.size() < o.minPts) {
					s.noise = true;
				} else {
					Cluster c = new Cluster();
					expandCluster(s, n, c, modified);
					clusters.add(c);
				}
			}
		}
		log.info("detected " + clusters.size() + " clusters");
		ClusterAggregation ca = AggregationFactory.getInstance()
				.createNewAggregation();
		List<GPSkSegment> aggregated = new LinkedList<>();
		for (Cluster c : clusters) {
			aggregated.add(ca.aggregate(c));
		}
		EnvironmentBuilder builder2 = new EnvironmentBuilder(aggregated);
		builder2.setTitle("DBSCAN results");
		builder2.setFocus(FOCUS_ON_SEGMENTS);
		builder2.setBounds(input.getBounds());
		return builder2.build();
	}

	protected void expandCluster(Segment s, List<GPSkSegment> n, Cluster c,
			GPSkEnvironment modified) {
		DBSCANoptions o = (DBSCANoptions) options;
		c.add(s);
		s.clusterMember = true;
		for (ListIterator<GPSkSegment> it = n.listIterator(); it.hasNext();) {
			Segment seg = (Segment) it.next();
			if (!seg.visited) {
				seg.visited = true;
				IndexProcedure<GPSkSegment> ip = new SegmentProcedure(seg, o.eps);
				modified.processSegments(ip);
				List<GPSkSegment> n2 = ip.getResults();
				if (n2.size() >= o.minPts) {
					for (GPSkSegment s2 : n2) {
						if (!n.contains(s2)) {
							it.add(s2);
						}
					}
				}
			}
			if (!seg.clusterMember) {
				c.add(seg);
				seg.clusterMember = true;
			}
		}
	}

	protected class Segment extends GPSkSegment {
		private Segment(GPSkSegment s) {
			super(s);
		}

		private boolean visited = false;

		private boolean noise = false;

		private boolean clusterMember = false;
	}

	protected class Cluster extends LinkedList<GPSkSegment> {
	}

	protected class SegmentProcedure extends DefaultSegmentProcedure {

		/**
		 * @param center
		 * @param eps
		 */
		public SegmentProcedure(GPSkSegment center, double eps) {
			super(center, eps);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.fub.agg2graph.gpsk.data.DefaultSegmentProcedure#execute(int)
		 */
		@Override
		public boolean execute(int arg0) {
			Segment seg = (Segment) data.get(arg0);
			if (seg.clusterMember) {
				return true;
			} else {
				return super.execute(arg0);
			}
		}

	}

}
