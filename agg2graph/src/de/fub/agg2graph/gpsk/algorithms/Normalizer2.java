/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.beans.ClusterCollection;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.beans.SegmentCluster;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
@Description(value = "All segments are going to have the same length")
public class Normalizer2 extends AbstractAlgorithm<ClusterCollection, GPSkEnvironment>{

    @Override
    public GPSkEnvironment call() throws Exception {
        
        
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    protected GPSkSegment mergeSegments(SegmentCluster sc){
        ListIterator<GPSkSegment> it = sc.listIterator();
        int maxLengthIndex = 0;
        double maxLength = 0;
        while(it.hasNext()){
            GPSkSegment seg = it.next();
            double length = seg.calculateLength();
            if(length >= maxLength){
                maxLength = length;
                maxLengthIndex = it.previousIndex();
            }
            
        }
        GPSkSegment longest = sc.get(maxLengthIndex);
        throw new UnsupportedOperationException();
    }
    
}
