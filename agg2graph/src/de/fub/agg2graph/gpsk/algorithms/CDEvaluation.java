/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.data.CircleProcedure;
import de.fub.agg2graph.gpsk.data.Filter;
import de.fub.agg2graph.gpsk.logic.ProjectionFactory;
import de.fub.agg2graph.gpsk.utils.PointCollection;
import de.fub.agg2graph.gpsk.utils.PointUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@Options(CDEvalOptions.class)
public class CDEvaluation extends MultipleInputAlgorithm<GPSkEnvironment> {
    private static Logger log = Logger.getLogger(CDEvaluation.class);

    public CDEvaluation() {
        super(new String[]{"map data", "crossroads"},
                new Filter[]{new Filter.DefaultFilter<GPSkEnvironment>(), new Filter.DefaultFilter<GPSkEnvironment>()});

    }

    protected double errorRate;
    protected double detectionRate;
    
    @Override
    public GPSkEnvironment call() throws Exception {
        errorRate = calculateErrorRate();
        log.info("Error Rate: " + errorRate);
        detectionRate = calculateDetectionRate();
        log.info("detection Rate: " + detectionRate);
        return null;
    }

    /**
     * for each point in the map data set a close point in the data set is
     * searched
     *
     * @return value between 0 and 1, how many real existing crossroads can be
     * found in a calculated data set?
     */
    private double calculateDetectionRate() {
        GPSkEnvironment data = (GPSkEnvironment) input[1];
        data.activatePointIndex();
        GPSkEnvironment mapData = (GPSkEnvironment) input[0];

        double radius = ((CDEvalOptions) options).maxDistance;
        int count = 0;
        int sum = 0;
        for (GPSkPoint p : mapData.getPoints()) {
            if (PointUtils.insideBounds(p, data.getBounds())) {
                CircleProcedure cp = new CircleProcedure(p, radius);
                data.processPoints(cp);
                
                if (!cp.getResults().isEmpty()) {
                    count++;
                }
                sum++;
            }
        }
        
        
        return (double) count / (double) sum;
    }
    
    private double calculateErrorRate(){
        GPSkEnvironment data = (GPSkEnvironment) input[1];
        GPSkEnvironment mapData = (GPSkEnvironment) input[0];
        double[] bounds = PointCollection.getBounds(mapData.getPoints());
        mapData.setProjection(ProjectionFactory.getInstance().createProjection(bounds));
        mapData.activatePointIndex();
        double radius = ((CDEvalOptions) options).maxDistance;
        int count = 0;
        int sum = 0;
        for(GPSkPoint p : data.getPoints()){
            CircleProcedure cp = new CircleProcedure(p, radius);
            mapData.processPoints(cp);
            if(cp.getResults().isEmpty()){
                count++;
            }
            sum++;
        }
        
        return (double) count /(double) sum;
    }
    
    
}
