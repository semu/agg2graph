/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.log4j.Logger;

import de.fub.agg2graph.gpsk.algorithms.AbstractFilter.ThreadInformation;

/**
 * @author Christian Windolf
 * 
 */
public class FilterFactory {
	private static Logger log = Logger.getLogger(FilterFactory.class);

	private static final FilterFactory singleton = new FilterFactory();
	
	public static FilterFactory getInstance(){
		return singleton;
	}

	/**
	 * 
	 */
	private FilterFactory() {
		// TODO Auto-generated constructor stub
	}

	public <T> AbstractFilter<T> createNewFilter(ThreadInformation ti,
			List<T> data, Class<? extends AbstractFilter<T>> clazz, Object outer) {
		try {
			Constructor<? extends AbstractFilter<T>> constructor;
			
			if (outer == null) {
				constructor = clazz.getConstructor(data.getClass(),
						ThreadInformation.class);
				return (AbstractFilter<T>) constructor.newInstance(data, ti);
				
			} else {
				
				constructor = clazz.getConstructor(outer.getClass(), List.class, ThreadInformation.class);
				return (AbstractFilter<T>) constructor.newInstance(outer, data, ti);
			}
		} catch (NoSuchMethodException | SecurityException e) {
			log.error("exception thrown while trying to construct a " + clazz.getSimpleName()+ " object", e);
			return null;
		} catch (InstantiationException e) {
			log.error("unable to create a " + clazz.getSimpleName() + " object", e);
			return null;
		} catch (IllegalAccessException e) {
			log.error("unable to create a " + clazz.getSimpleName() + " object", e);
			return null;
		} catch (IllegalArgumentException e) {
			log.error("unable to create a " + clazz.getSimpleName() + " object", e);
			return null;
		} catch (InvocationTargetException e) {
			log.error("unable to create a " + clazz.getSimpleName() + " object", e);
			return null;
		}
		
	}

	public <T> AbstractFilter<T> createNewFilter(ThreadInformation ti,
			List<T> data, Class<? extends AbstractFilter<T>> clazz) {
		return createNewFilter(ti, data, clazz, null);
	}

}
