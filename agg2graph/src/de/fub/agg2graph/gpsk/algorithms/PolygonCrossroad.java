/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.data.RectangleProcedure;
import de.fub.agg2graph.gpsk.logic.Projection;
import de.fub.agg2graph.gpsk.logic.geom.Point;
import de.fub.agg2graph.gpsk.logic.geom.Polygon;
import de.fub.agg2graph.gpsk.logic.geom.PolygonUtils;
import de.fub.agg2graph.gpsk.utils.PointUtils;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_SEGMENTS;

/**
 * @author Christian Windolf
 * 
 */
@Options(PGOptions.class)
public class PolygonCrossroad extends
		AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> {

	private HashSet<GPSkPoint> set = new HashSet<>();
	private Projection proj;

	/**
	 * 
	 */
	public PolygonCrossroad() {
		super();
	}

	/**
	 * @param input
	 */
	public PolygonCrossroad(GPSkEnvironment input) {
		super(input);
		proj = input.getProjection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm#setInput(java.lang
	 * .Object)
	 */
	@Override
	public void setInput(GPSkEnvironment input) {
		this.proj = input.getProjection();
		super.setInput(input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm#call()
	 */
	@Override
	public GPSkEnvironment call() throws Exception {
		proj = input.getProjection();
		input.activatePointIndex();
		set.clear();
		List<Crossroad> rectangles = new LinkedList<>();
		for (GPSkPoint p : input.getPoints()) {
			if (!set.contains(p)) {
				Crossroad cr = new Crossroad(p);
				cr.firstStep();
				while (cr.extend())
					;
				rectangles.add(cr);
			}
		}
		Crossroad cr = processCrossroads(rectangles);
		List<GPSkSegment> segList = new LinkedList<>();
		while(cr != null){
			segList.add(convertFromPolygon(cr.pg));
			cr = processCrossroads(rectangles);
		}
		
		
		

		EnvironmentBuilder builder = new EnvironmentBuilder(segList);
		builder.detectBounds();
		builder.useProjection();
		builder.setFocus(FOCUS_ON_SEGMENTS);

		return builder.build();

	}

	private GPSkSegment convertFromPolygon(Polygon p) {
		GPSkSegment seg = new GPSkSegment();
		for (Point point : p.getPoints()) {
			GPSkPoint gPoint = (GPSkPoint) proj.toLatLon(point.getX(),
					point.getY());

			seg.add(gPoint);
		}
		GPSkPoint last = seg.getFirst();
		seg.add(last);
		return seg;
	}
	
	private Crossroad processCrossroads(List<Crossroad> src){
		if(src.isEmpty()){
			return null;
		}
		Crossroad first = src.remove(0);
		if(src.isEmpty()){
			return first;
		}
		ListIterator<Crossroad> lit = src.listIterator();
		while(lit.hasNext()){
			Crossroad cr = lit.next();
			if(PolygonUtils.overlap(first.pg, cr.pg)){
				first = new Crossroad(first, cr);
				lit.remove();
				lit = src.listIterator();
			}
			
		}
		return first;
	}

	protected class Crossroad {
		private List<GPSkPoint> points;
		private Polygon pg;

		private final int NORTHWEST = 0;
		private final int NORTHEAST = 1;
		private final int SOUTHEAST = 2;
		private final int SOUTHWEST = 3;

		/**
		 * Instanciates a new Crossroad-polygone with just one crossroad.
		 * 
		 * @param point
		 *            the crossroad
		 */
		protected Crossroad(GPSkPoint point) {
			double east = point.getEast();
			double north = point.getNorth();
			if (Double.isNaN(east) || Double.isInfinite(east)) {
				throw new IllegalArgumentException(
						"Easting value must be a number!");
			}
			if (Double.isNaN(north) || Double.isInfinite(north)) {
				throw new IllegalArgumentException(
						"Northing value must be a number!");
			}

			this.points = new LinkedList<>();
			this.points.add(point);
			double minRectValue = ((PGOptions) options).startingRectSize;
			Polygon.RectangleBuilder rectBuilder = new Polygon.RectangleBuilder();
			rectBuilder.setLeftUp(east - minRectValue, north + minRectValue);
			rectBuilder.setRightDown(east + minRectValue, north - minRectValue);
			pg = rectBuilder.build();
			set.add(point);
		}

		protected Crossroad(Crossroad cr1, Crossroad cr2) {
			this.points = new LinkedList<>();
			points.addAll(cr1.getContainingPoints());
			points.addAll(cr2.getContainingPoints());
			pg = PolygonUtils.merge(cr1.pg, cr2.pg);

		}

		protected void firstStep() {
			Point[] pointsArray = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(
					pointsArray[0].getX(), pointsArray[0].getY(),
					pointsArray[2].getX(), pointsArray[2].getY());
			input.processPoints(rp);
			List<GPSkPoint> pointList = rp.getResults();

			for (GPSkPoint point : pointList) {
				if (!set.contains(point)) {
					points.add(point);
					set.add(point);
				}
			}
		}

		protected HashSet<GPSkPoint> checkBorder(int side) {
			double extensionSize = ((PGOptions) options).extensionSize;
			HashSet<GPSkPoint> results = new HashSet<>();

			if (side == NORTHWEST) {
				List<GPSkPoint> list = getNorthSide(extensionSize);
				list.addAll(getNorthWestEdge(extensionSize));
				list.addAll(getWestSide(extensionSize));

				for (GPSkPoint p : list) {
					if (!set.contains(p)) {
						results.add(p);
					}
				}

			}
			if (side == NORTHEAST) {

				List<GPSkPoint> list = getNorthSide(extensionSize);
				list.addAll(getNorthEastEdge(extensionSize));
				list.addAll(getEastSide(extensionSize));

				for (GPSkPoint p : list) {
					if (!set.contains(p)) {
						results.add(p);
					}
				}

			}

			if (side == SOUTHEAST) {
				List<GPSkPoint> list = getSouthSide(extensionSize);
				list.addAll(getSouthEastEdge(extensionSize));
				list.addAll(getEastSide(extensionSize));

				for (GPSkPoint p : list) {
					if (!set.contains(p)) {
						results.add(p);
					}
				}
			}

			if (side == SOUTHWEST) {
				List<GPSkPoint> list = getSouthSide(extensionSize);
				list.addAll(getSouthWestEdge(extensionSize));
				list.addAll(getWestSide(extensionSize));
				for (GPSkPoint p : list) {
					if (!set.contains(p)) {
						results.add(p);
					}
				}
			}
			return results;
		}

		private List<GPSkPoint> getNorthSide(double extensionSize) {
			Point[] points = pg.getPoints();

			RectangleProcedure rp = new RectangleProcedure(points[0].getX(),
					points[0].getY() + extensionSize, points[1].getX(),
					points[1].getY());
			input.processPoints(rp);
			return rp.getResults();
		}

		private List<GPSkPoint> getSouthSide(double extensionSize) {
			Point[] points = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(points[3].getX(),
					points[3].getY(), points[2].getX(), points[2].getY()
							- extensionSize);
			input.processPoints(rp);
			return rp.getResults();
		}

		private List<GPSkPoint> getWestSide(double extensionSize) {
			Point[] points = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(points[0].getX()
					- extensionSize, points[0].getY(), points[3].getX(),
					points[3].getY());
			input.processPoints(rp);
			return rp.getResults();
		}

		private List<GPSkPoint> getEastSide(double extensionSize) {
			Point[] points = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(points[1].getX(),
					points[1].getY(), points[2].getX() + extensionSize,
					points[2].getY());
			input.processPoints(rp);
			return rp.getResults();
		}

		private List<GPSkPoint> getNorthWestEdge(double extensionSize) {
			Point[] points = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(points[0].getX()
					- extensionSize, points[0].getY() + extensionSize,
					points[0].getX(), points[0].getY());
			input.processPoints(rp);
			return rp.getResults();
		}

		private List<GPSkPoint> getNorthEastEdge(double extensionSize) {
			Point[] points = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(points[1].getX(),
					points[1].getY() + extensionSize, points[1].getX()
							+ extensionSize, points[1].getY());
			input.processPoints(rp);
			return rp.getResults();
		}

		private List<GPSkPoint> getSouthEastEdge(double extensionSize) {
			Point[] points = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(points[2].getX(),
					points[2].getY(), points[2].getX() + extensionSize,
					points[2].getY() - extensionSize);
			input.processPoints(rp);
			return rp.getResults();
		}

		private List<GPSkPoint> getSouthWestEdge(double extensionSize) {
			Point[] points = pg.getPoints();
			RectangleProcedure rp = new RectangleProcedure(points[3].getX()
					- extensionSize, points[3].getY(), points[3].getX(),
					points[3].getY() - extensionSize);
			input.processPoints(rp);
			return rp.getResults();
		}

		protected boolean extend() {
			HashSet<GPSkPoint> nw = checkBorder(NORTHWEST);
			HashSet<GPSkPoint> ne = checkBorder(NORTHEAST);
			HashSet<GPSkPoint> se = checkBorder(SOUTHEAST);
			HashSet<GPSkPoint> sw = checkBorder(SOUTHWEST);

			int side = NORTHWEST;

			HashSet<GPSkPoint> maxSized = nw;
			if (maxSized.size() < ne.size()) {
				side = NORTHEAST;
				maxSized = ne;
			}
			if (maxSized.size() < se.size()) {
				side = SOUTHEAST;
				maxSized = se;
			}
			if (maxSized.size() < sw.size()) {
				side = SOUTHWEST;
				maxSized = sw;
			}
			if (maxSized.isEmpty()) {
				return false;
			}

			List<GPSkPoint> copy = new LinkedList<>(maxSized);
			copy.addAll(points);
			Polygon pg2 = extendPolygon(side,
					((PGOptions) options).extensionSize);
			if (isValid(copy, pg2)) {
				points.addAll(maxSized);
				pg = pg2;
				return true;
			}
			return false;

		}

		private Polygon extendPolygon(int side, double extensionSize) {
			Polygon.RectangleBuilder rectBuilder = new Polygon.RectangleBuilder();
			Point[] points = pg.getPoints();
			if (side == NORTHWEST) {
				rectBuilder.setLeftUp(points[0].getX() - extensionSize,
						points[0].getY() + extensionSize);
				rectBuilder.setRightDown(pg.getPoints()[2]);
			}
			if (side == NORTHEAST) {
				rectBuilder.setLeftUp(points[0].getX(), points[0].getY()
						+ extensionSize);
				rectBuilder.setRightDown(points[2].getX() + extensionSize,
						points[2].getY());
			}

			if (side == SOUTHEAST) {
				rectBuilder.setLeftUp(points[0]);
				rectBuilder.setRightDown(points[2].getX() + extensionSize,
						points[2].getY() - extensionSize);

			}
			if (side == SOUTHWEST) {
				rectBuilder.setLeftUp(points[0].getX() - extensionSize,
						points[0].getY());
				rectBuilder.setRightDown(points[2].getX(), points[2].getY()
						- extensionSize);

			}
			return rectBuilder.build();
		}

		/**
		 * checks if the rectangle is in a valid state. if it has less than four
		 * points, it is. If not, at least three quadrants of the rectangle must
		 * contain a forth of all points.
		 * 
		 * @return
		 */
		protected boolean isValid(List<GPSkPoint> pointList, Polygon polygone) {
			int size = pointList.size();
			if (size < 4) {
				return true;
			}

			/*
			 * constraint: must be at least three at the end of this method. If
			 * not, this rectangle is not valid
			 */
			int validQuadrants = 0;

			double x1, x2, y1, y2;

			Point[] points = polygone.getPoints();
			Polygon.RectangleBuilder first4Builder = new Polygon.RectangleBuilder();
			x1 = points[0].getX();
			y1 = points[0].getY();
			first4Builder.setLeftUp(points[0]);
			x2 = (points[0].getX() + points[2].getX()) / 2d;
			y2 = (points[0].getY() + points[2].getY()) / 2d;
			first4Builder.setX2(x2);
			first4Builder.setY2(y2);
			Polygon p1 = first4Builder.build();
			int quadrantCounter = 0; // counts the amount of points in a
										// quadrant.
			ListIterator<GPSkPoint> listIterator = pointList.listIterator();
			while (listIterator.hasNext()) {
				GPSkPoint p = listIterator.next();
				Point point = new Point(p.getEast(), p.getNorth());
				if (p1.contains(point)) {
					listIterator.remove();
					quadrantCounter++;
				}
			}
			if (quadrantIsValid(size, quadrantCounter)) {
				validQuadrants++;
			}

			Polygon.RectangleBuilder second4Builder = new Polygon.RectangleBuilder();
			x1 = x2;
			second4Builder.setX1(x1);
			second4Builder.setY1(y1);
			x2 = points[1].getX();
			// y2 = y2 ... first and second qudrand have the same bottom and top
			// line
			second4Builder.setX2(x2);
			second4Builder.setY2(y2);
			Polygon p2 = second4Builder.build();
			listIterator = pointList.listIterator();
			quadrantCounter = 0;
			while (listIterator.hasNext()) {
				GPSkPoint p = listIterator.next();
				Point point = new Point(p.getEast(), p.getNorth());
				if (p2.contains(point)) {
					listIterator.remove();
					quadrantCounter++;
				}
			}
			if (quadrantIsValid(size, quadrantCounter)) {
				validQuadrants++;
			}

			Polygon.RectangleBuilder third4Builder = new Polygon.RectangleBuilder();
			third4Builder.setX1(x1);
			y1 = y2;
			third4Builder.setY1(y1);
			third4Builder.setRightDown(points[2]);
			Polygon p3 = third4Builder.build();
			listIterator = pointList.listIterator();
			quadrantCounter = 0;
			while (listIterator.hasNext()) {
				GPSkPoint p = listIterator.next();
				Point point = new Point(p.getEast(), p.getNorth());
				if (p3.contains(point)) {
					listIterator.remove();
					quadrantCounter++;
				}
			}
			if (quadrantIsValid(size, quadrantCounter)) {
				validQuadrants++;
			}
			if (validQuadrants == 3) {
				return true;
			}

			Polygon.RectangleBuilder forth4Builder = new Polygon.RectangleBuilder();
			forth4Builder.setX1(points[0].getX());
			forth4Builder.setY1(y1);
			x2 = x1;
			forth4Builder.setX2(x2);
			forth4Builder.setY2(points[3].getY());
			Polygon p4 = forth4Builder.build();
			quadrantCounter = 0;
			listIterator = pointList.listIterator();
			while (listIterator.hasNext()) {
				GPSkPoint p = listIterator.next();
				Point point = new Point(p.getEast(), p.getNorth());
				if (p4.contains(point)) {
					listIterator.remove();
					quadrantCounter++;
				}
			}

			if (quadrantIsValid(size, quadrantCounter)) {
				validQuadrants++;
			}

			return validQuadrants >= 3;

		}

		private boolean quadrantIsValid(int size, int quadrantCounter) {
			if (quadrantCounter % 4 == 3) {
				return quadrantCounter >= Math.round((double) size / 4d);
			} else {
				return quadrantCounter >= size / 4;
			}
		}

		public Polygon getPolygon() {
			return pg;
		}

		public List<GPSkPoint> getContainingPoints() {
			return points;
		}

		// private void printProjection(GPSkPoint p){
		// System.out.println("Point{" + p.getEast() + "," + p.getNorth() +
		// "}");
		// }
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder(
					"PolygonCrossroad{#points=");
			if (points == null) {
				builder.append("null}");
			} else {
				builder.append(points.size()).append('}');
			}

			builder.append(" Polygon{");

			if (pg == null) {
				builder.append("null}");
			} else {

				Point left = pg.getLeftMostPoint();
				builder.append('(').append(left.getX()).append(',');
				Point up = pg.getTopPoint();
				builder.append(up.getY()).append("),(");
				Point right = pg.getRightMostPoint();
				builder.append(right.getX()).append(',');
				Point bottom = pg.getBottomPoint();
				builder.append(bottom.getY()).append("), #points=");
				builder.append(pg.getPoints().length).append("}");
			}
			return builder.toString();
		}
	}

}
