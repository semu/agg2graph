/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.fub.agg2graph.gpsk.annotations.Options;

/**
 * An algorithm with just one input and output should inherit this class.
 * The inheriting class should also have an {@link Options}-annotation, to tell its parent (this class),
 * how the options look like
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public abstract class AbstractAlgorithm<Input, Output> implements Callable<Output> {
    private static Logger log = Logger.getLogger(AbstractAlgorithm.class);
    public Input input;
    protected Object options;

    /**
     * like {@link AbstractAlgorithm#AbstractAlgorithm()}, but you can also pass the input data.
     * That means, the algorithm is ready to go.
     * @param input
     */
    public AbstractAlgorithm(Input input){
        this();
        this.input = input;
    }
    
    /**
     * Creates a new algorithm object and stores the default options object.
     * If any exception occurs, it will be catched and an error message will be logged via log4j.
     * That means of course, that later, at execution time, there can be a lot of exceptions thrown,
     * especially NullPointerExceptions
     */
    public AbstractAlgorithm(){
        Options o = (Options) getClass().getAnnotation(Options.class);
        if(o == null){
            log.warn(getClass() + " has no correct annotation about Options class");
        }
        Class optionsClass = o.value();
        try {
            Constructor optionsConstructor = optionsClass.getConstructor();
            this.options = optionsConstructor.newInstance();
        } catch (NoSuchMethodException ex) {
            log.error(optionsClass.getSimpleName() + " has no default constructor", ex);
        } catch (SecurityException | IllegalAccessException ex) {
            log.error(optionsClass.getSimpleName() + " has no public default constructor", ex);
        } catch (InstantiationException | InvocationTargetException ex){
            log.error("failed to invoke default constructor on " + optionsClass.getSimpleName(), ex);
        }
    }
    
    
    /**
     * if there was no input data passed by the constructor, you can do it here
     * @param input
     */
    public void setInput(Input input){
        this.input = input;
    }
    
    /**
     * sets options, if the default options are not the ones, that are wanted.
     * If something is wrong with the options, the error will be just logged.
     * @param options
     */
    public void setOptions(Object options){
        Class c = getClass();
        Options o = (Options) c.getAnnotation(Options.class);
        if(o == null){
            log.warn("The class " + getClass() + " has no options Class annotation"
                    + " Therefor I connot check if the options are valid");
            this.options = options;
        } else{
            if(o.value().isInstance(options)){
                this.options = options;
            } else {
                log.error("something tried to set options, that are not valid for this algorithm");
                throw new IllegalArgumentException("options object was not instance of the correct class!");
            }
        }
    }
    
    /**
     * Execute the algorithm
     */
    @Override
    public abstract Output call() throws Exception;
    
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append(": options=").append(options == null ? "0" : "1");
        builder.append(" input=").append(options == null ?  "0" : "1");
        return builder.toString();
        
    }
    
}
