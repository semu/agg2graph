/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.data.Filter;

/**
 * Maintains tasks for algorithms with Multiple Input.
 * 
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
public abstract class MultipleInputAlgorithm<Output> implements Callable<Output> {
    private static Logger log = Logger.getLogger(MultipleInputAlgorithm.class);
    
    /**
     * Stores the input
     */
    public Object[] input;
    
    /**
     * Variable names for the input.
     * This information is mostly used by the GUI
     */
    public String[] names;
    
    /**
     * stores filters for the input.
     * @see Filter
     */
    public Filter<Object>[] filter;
    public Object options;
    
    
    /**
     * sets up names and filters.
     * @param names
     * @param filter
     */
    public MultipleInputAlgorithm(String[] names, Filter<Object>[] filter){
        this.names = names;
        this.filter = filter;
        Options options = getClass().getAnnotation(Options.class);
        input = new Object[names.length];
        if(options != null){
            try {
                Constructor<Options> c = options.value().getConstructor();
                this.options = c.newInstance();
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | InvocationTargetException   ex) {
                log.warn("unable to initialize default options. Should not be a big deal, but it could cause NUllpointer exceptions later", ex);
                return;
            } 
        } else {
            log.warn(getClass().getSimpleName() + " seems to have no Options annotation");
        }
    }
    
    
    
    public void setInput(Object o, int index){
        
        input[index] = o;
    }
    
    public String getName(int index) {
        return names[index];
    }
    
    public Filter getFilter(int index){
        return filter[index];
    }
    
    public void setOptions(Object options){
        this.options = options;
    }
    
    public int getAmountOfInputs(){
        return input.length;
    }
    
    
}
