/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import java.util.ListIterator;


import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_SEGMENTS;
import de.fub.agg2graph.gpsk.logic.DistanceFactory;
import de.fub.agg2graph.gpsk.logic.PointDistance;
import de.fub.agg2graph.gpsk.logic.TrigonometricDistance;
import de.fub.agg2graph.gpsk.utils.PointUtils;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
@Description("Normalize all edge lengths")
@Options(NormalizeOptions.class)
public class Normalizer extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> {
	
    private static int TOO_SHORT = -1, OK = 0, TOO_LONG = 1;

    private PointDistance pd;
    protected double minDistance;
	protected double maxDistance;
    protected double eps;
    protected boolean allowShortEdges;
    
    public boolean isAllowShortEdges() {
		return allowShortEdges;
	}
    
	public void setAllowShortEdges(boolean allowShortEdges) {
		this.allowShortEdges = allowShortEdges;
	}
	
	public double getMinDistance() {
		return minDistance;
	}
    
	public void setMinDistance(double minDistance) {
		this.minDistance = minDistance;
	}
	
	public double getMaxDistance() {
		return maxDistance;
	}
	
	public void setMaxDistance(double maxDistance) {
		this.maxDistance = maxDistance;
	}
	
	public double getEps() {
		return eps;
	}
	
	public void setEps(double eps) {
		this.eps = eps;
	}
	
    public Normalizer(GPSkEnvironment env) {
        super(env);
        try {
            pd = DistanceFactory.getInstance().createNewInstance();
        } catch (Exception e) {
            pd = new TrigonometricDistance();
        }
    }
    
    public Normalizer() {
        super();
        try {
            pd = DistanceFactory.getInstance().createNewInstance();
        } catch (Exception e) {
            pd = new TrigonometricDistance();
        }
    }
    
    @Override
    public GPSkEnvironment call() throws Exception {
        // "importing" options
        NormalizeOptions o = (NormalizeOptions) options;
        minDistance = o.distance - (o.distance * o.tolerance); //calculatring a minimum distance between points
        maxDistance = o.distance + (o.distance * o.tolerance);
        eps = o.distance;
        List<GPSkSegment> segments = new LinkedList<>();
        for (GPSkSegment seg : input.getSegments()) {
            GPSkSegment newSeg = normalizeSegment(seg);
            if (newSeg != null) {
                segments.add(newSeg);
            }
        }
        
        EnvironmentBuilder builder = new EnvironmentBuilder(segments);
        builder.setTitle("normalized tracks");
        builder.setBounds(input.getBounds());
        builder.setFocus(FOCUS_ON_SEGMENTS);
        
        return builder.build();
    }
    
    public GPSkSegment normalizeSegment(GPSkSegment seg) {
    	if (seg.size() <= 1) {
            return null;
        }
    	
        ListIterator<GPSkPoint> it = seg.listIterator();
        GPSkPoint first = it.next();
        GPSkSegment newSegment = new GPSkSegment();
        newSegment.add(first);
        while (it.hasNext()) {
            GPSkPoint second = it.next();
            
            // generate mid points if distance is okay
            int distance = distanceOK(first, second);
            if (distance == OK) {
                newSegment.add(second);
            } else if (distance == TOO_SHORT) {
            	if (allowShortEdges) {
            		newSegment.add(second);
            	} else {
            		if (it.hasNext()) {
                    	// do nothing
                    } else {
                        newSegment.add(second);
                    }
            	}
            } else {
                GPSkPoint newPoint = PointUtils.midPointGeneration(first, second, eps);
                newSegment.add(newPoint);
                it.previous();
            }
            
            first = newSegment.getLast();
        }
        
        return newSegment;
    }
    
    private int distanceOK(GPSkPoint p1, GPSkPoint p2) {
        double distance = pd.calculate(p1, p2);
        if (distance <= maxDistance && distance >= minDistance) {
            return OK;
        } else if (distance <= minDistance) {
            return TOO_SHORT;
        } else {
            return TOO_LONG;
        }
    }
}