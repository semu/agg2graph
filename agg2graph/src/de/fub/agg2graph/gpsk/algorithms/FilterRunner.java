/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Future;


import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.algorithms.AbstractFilter.ThreadInformation;
import static de.fub.agg2graph.gpsk.Main.THREADS;

/**
 * @author Christian Windolf
 * 
 */
public class FilterRunner<T> {

	
	private final List<T> inputList;
	private final FilterFactory ff = FilterFactory.getInstance();

	private final List<AbstractFilter<T>> filterList = new LinkedList<>();

	/**
	 * 
	 */
	public FilterRunner(List<T> inputList,
			Class<? extends AbstractFilter<T>> clazz) {
		this.inputList = inputList;
		if (inputList.size() <= 10) {
			ThreadInformation ti = new ThreadInformation(0, 1, 0,
					inputList.size(), inputList.size());
			AbstractFilter<T> af = ff.<T> createNewFilter(ti, inputList, clazz);
			filterList.add(af);
		} else {
			int frac = inputList.size() / THREADS;
			for (int i = 0; i < THREADS - 1; i++) {

				int fromIndex = frac * i;
				int toIndex = frac * (i + 1);
				List<T> subList = inputList.subList(fromIndex, toIndex);
				ThreadInformation ti = new ThreadInformation(i, THREADS,
						fromIndex, toIndex, inputList.size());
				AbstractFilter<T> af = ff.<T> createNewFilter(ti, subList,
						clazz);
				filterList.add(af);
			}

			ThreadInformation ti = new ThreadInformation(THREADS - 1, THREADS,
					frac * (THREADS - 1), inputList.size(), inputList.size());
			AbstractFilter<T> af = ff.<T> createNewFilter(ti,
					inputList.subList(frac * (THREADS - 1), inputList.size()),
					clazz);
			filterList.add(af);
		}
	}
	
	public FilterRunner(List<T> data, Class clazz, Object outer){
		this.inputList = data;
		if(inputList.size() <= 10){
			ThreadInformation ti = new ThreadInformation(0, 1, 0, inputList.size(), inputList.size());
			AbstractFilter af = ff.<T>createNewFilter(ti, data, clazz, outer);
			filterList.add(af);
		} else {
			int frac = inputList.size() / THREADS;
			for(int i = 0; i < THREADS - 1; i++){
				int fromIndex = frac * i;
				int toIndex = frac * (i + 1);
				List<T> subList = inputList.subList(fromIndex, toIndex);
				ThreadInformation ti = new ThreadInformation(i, THREADS,
						fromIndex, toIndex, inputList.size());
				AbstractFilter<T> af = ff.<T>createNewFilter(ti, data, clazz, outer);
				filterList.add(af);
			}
			ThreadInformation ti = new ThreadInformation(THREADS - 1, THREADS,
					frac * (THREADS - 1), inputList.size(), inputList.size());
			AbstractFilter<T> af = ff.<T> createNewFilter(ti, inputList.subList(frac * (THREADS - 1), inputList.size()), clazz, outer);
			filterList.add(af);
		}
	}
	
	

	public List<T> filter() {
		if (filterList.size() >= 2) {
			ListIterator<AbstractFilter<T>> listIter = filterList
					.listIterator(1);
			List<Future<List<T>>> futureList = new LinkedList<>();
			while (listIter.hasNext()) {
				Future<List<T>> future = Main.executor.submit(listIter.next());
			}
			AbstractFilter<T> af = filterList.get(0);
			try {
				List<T> results = af.call();
				for(Future<List<T>> f : futureList){
					results.addAll(f.get());
				}
				return results;
			} catch (Exception e) {

				return null;
			}
		} else {
			AbstractFilter<T> af = filterList.get(0);
			try {
				return af.call();
			} catch (Exception e) {
				return null;
			}
		}

	}

}
