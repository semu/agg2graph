/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.Main;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.graph.RamerDouglasPeuckerFilter;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@Options(RDPOptions.class)
public class RDPFilter extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> {
    private static Logger log = Logger.getLogger(RDPFilter.class);

    RamerDouglasPeuckerFilter rdp;

    public RDPFilter(GPSkEnvironment input) {
        super(input);
    }

    public RDPFilter() {
    }

    @Override
    public GPSkEnvironment call() throws Exception {
        RDPOptions o = (RDPOptions) options;
        log.debug("RDP called with " + o);
        rdp = new RamerDouglasPeuckerFilter(o.eps, o.maxSegmentLength);
        
        FilterRunner<GPSkSegment> fr = new FilterRunner<>(input.getSegments(), ListRunner.class, this);
        List<GPSkSegment> results = fr.filter();
        EnvironmentBuilder builder = new EnvironmentBuilder(results);
        builder.setTitle("smoothed").setBounds(input.getBounds());
        return builder.build();
        
    }

    

    protected class ListRunner extends AbstractFilter<GPSkSegment> {

        /**
		 * @param data
		 * @param ti
		 */
		public ListRunner(
				List<GPSkSegment> data,
				de.fub.agg2graph.gpsk.algorithms.AbstractFilter.ThreadInformation ti) {
			super(data, ti);
			// TODO Auto-generated constructor stub
		}

		@Override
        public List<GPSkSegment> call() throws Exception {
            List<GPSkSegment> results = new LinkedList<>();
            for (GPSkSegment seg : data) {
                GPSSegment s1 = rdp.simplify(seg.copy().agg2graphVersion());
                
                GPSkSegment s2 = new GPSkSegment();
                for(GPSPoint p : s1){
                    s2.add(new GPSkPoint(p));
                }
                results.add(s2);
            }
            return results;
        }
    }
}
