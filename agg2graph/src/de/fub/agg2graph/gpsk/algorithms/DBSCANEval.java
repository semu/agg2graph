/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@Options(DBSCANevalOptions.class)
public class DBSCANEval extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment>{

    public DBSCANEval(GPSkEnvironment input) {
        super(input);
    }

    public DBSCANEval() {
    }

    
    @Override
    public GPSkEnvironment call() throws Exception {
        DBSCANevalOptions deo = (DBSCANevalOptions) options;
        File f = new File(deo.file);
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        for(int minPts = deo.startMinPts; minPts <= deo.endMinPts; minPts += deo.minPtsTick){
            DBSCAN dbscan = new DBSCAN();
            DBSCANoptions dbo = new DBSCANoptions();
            dbo.eps = deo.eps;
            dbo.minPts = minPts;
            dbscan.setOptions(dbo);
            dbscan.setInput(input);
            GPSkEnvironment result = dbscan.call();
            bw.write(deo.eps + ", " + minPts + ", " + result.getLength() + ", " + result.getSegments().size());
            bw.newLine();
        }
        bw.flush();
        bw.close();
        
        return null;
    }
    
    
}
