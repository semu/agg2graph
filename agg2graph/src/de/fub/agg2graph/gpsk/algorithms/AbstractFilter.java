/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author Christian Windolf
 *
 */
public abstract class AbstractFilter<T> implements Callable<List<T>> {
	public final ThreadInformation ti;
	public List<T> data;

	/**
	 * 
	 */
	public AbstractFilter(List<T> data, ThreadInformation ti) {
		this.data = data;
		this.ti = ti;
	}
	
	

	
	
	public static class ThreadInformation{
		public final int ID;
		public final int totalThreads;
		public final int startingIndex;
		public final int stoppingIndex;
		public final int size;
		
		public ThreadInformation(int ID, int totalThreads, int startingIndex, int stoppingIndex, int size){
			this.ID = ID;
			this.totalThreads = totalThreads;
			this.startingIndex = startingIndex;
			this.stoppingIndex = stoppingIndex;
			this.size = size;
		}
	}
	
	

}
