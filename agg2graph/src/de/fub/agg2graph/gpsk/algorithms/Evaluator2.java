/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.infomatiq.jsi.Rectangle;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.data.Filter;
import de.fub.agg2graph.gpsk.data.IndexProcedure;
import de.fub.agg2graph.gpsk.data.RectangleProcedure;
import de.fub.agg2graph.gpsk.logic.Projection;
import de.fub.agg2graph.gpsk.logic.ProjectionFactory;
import de.fub.agg2graph.gpsk.utils.PointCollection;
import de.fub.agg2graph.gpsk.utils.PointUtils;

/**
 * @author Christian Windolf
 * 
 */
@Options(Evaluator2Options.class)
public class Evaluator2 extends MultipleInputAlgorithm<GPSkEnvironment> {
	private static Logger log = Logger.getLogger(Evaluator2.class);
	Projection projection;

	/**
	 * @param names
	 * @param filter
	 */
	public Evaluator2() {
		super(new String[] { "map data", "data" }, new Filter[] {
				new Filter.DefaultFilter<GPSkEnvironment>(),
				new Filter.DefaultFilter<GPSkEnvironment>() });

		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public GPSkEnvironment call() throws Exception {
		GPSkEnvironment map = (GPSkEnvironment) input[0];
		GPSkEnvironment data = (GPSkEnvironment) input[1];
		projection = ProjectionFactory.getInstance().createProjection(
				map.getBounds());
		map.setProjection(projection);
		map.activatePointIndex();
		map.getBounds();

		data.setProjection(projection);
		data.activateSegmentIndex();

		
		
		double detectionRate = calculateDetectionRate();
		double errorRate = calculateErrorRate();
		log.info("Detection Rate: " + detectionRate);
		log.info("Error Rate: " + errorRate);
		return null;
	}

	private double calculateErrorRate() {
		GPSkEnvironment map = (GPSkEnvironment) input[0];
		GPSkEnvironment data = (GPSkEnvironment) input[1];

		List<GPSkPoint> mapPoint = map.getPoints();
		List<GPSkSegment> crossroads = data.getSegments();

		int counter = 0;
		for (GPSkSegment crossroad : crossroads) {
			Rectangle rect = crossroad.getRectangle();
			RectangleProcedure rr = new RectangleProcedure(rect.minX,
					rect.minY, rect.maxX, rect.maxY);
			map.processPoints(rr);
			if (rr.getResults().isEmpty()) {
				counter++;
			}

		}

		return (double) crossroads.size() / (double) counter;
	}

	private double calculateDetectionRate() {
		GPSkEnvironment map = (GPSkEnvironment) input[0];
		GPSkEnvironment data = (GPSkEnvironment) input[1];

		List<GPSkPoint> mapPoints = map.getPoints();
		List<GPSkSegment> crossroads = data.getSegments();

		double counterForDetectedCrossroads = 0;
		double counterForCrossroadsInsideBounds = 0;

		for (GPSkPoint mapPoint : mapPoints) {
			if (PointUtils.insideBounds(mapPoint, data.getBounds())) {
				counterForCrossroadsInsideBounds ++;
				double x = mapPoint.getEast();
				double y = mapPoint.getNorth();
				final GPSkPoint finalPoint = mapPoint;
				final Rectangle r = new Rectangle((float) (x - 75),
						(float) y + 75, (float) x + 75, (float) y + 75);
				IndexProcedure<GPSkSegment> ip = new IndexProcedure<GPSkSegment>() {
					private ArrayList<GPSkSegment> list;
					private List<GPSkSegment> results = Collections.EMPTY_LIST;

					@Override
					public boolean execute(int arg0) {
						GPSkSegment seg = list.get(arg0);
						if (finalPoint.getLat() <= seg.get(0).getLat()
								&& finalPoint.getLon() >= seg.get(0).getLon()
								&& finalPoint.getLat() >= seg.get(2).getLat()
								&& finalPoint.getLon() <= seg.get(2).getLon()) {
							results = Collections.singletonList(seg);
						}
						return true;
					}

					@Override
					public List<Rectangle> getRectangles() {

						return Collections.singletonList(r);
					}

					@Override
					public List<GPSkSegment> getResults() {
						return results;
					}

					@Override
					public void putData(ArrayList<GPSkSegment> data) {
						this.list = data;

					}

					@Override
					public void startNewRectangle() {

					}

				};
				data.processSegments(ip);
				if(ip.getResults().isEmpty()){
					counterForDetectedCrossroads++;
				}

			}
		}
		return (double) counterForDetectedCrossroads / (double) counterForCrossroadsInsideBounds;
	}
}
