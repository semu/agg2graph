/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fub.agg2graph.gpsk.algorithms;

/**
 * @author Christian Windolf
 * <p>In this package, all algorithms should be stored.
 * The state of May 24th '13 is, that they are not stored.</p>
 * <p>Each algorithm should inherit the {@link de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm AbstractAlgorithm-class}.
 * </p>
 * <p>Except for the splitting algorithm, that has more than one input.</p>
 * <p>An algorithm class should store it's options in a different class.
 * It should be annotated with an {@link de.fub.agg2graph.gpsk.annotations.Options Options} annotation
 * to tell tools (GUI-Builders, algorithm runners, {@link de.fub.agg2graph.gpsk.KeyValueStore KeyValueStore}),
 *    how the options look like.
 */
