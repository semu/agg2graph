/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.data.CircleProcedure;
import de.fub.agg2graph.gpsk.data.Filter;
import de.fub.agg2graph.gpsk.logic.DistanceFactory;
import de.fub.agg2graph.gpsk.logic.PointDistance;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import static de.fub.agg2graph.gpsk.beans.GPSkEnvironment.MetaInformation.FOCUS_ON_SEGMENTS;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@Options(SplitterOptions.class)
public class Splitter extends MultipleInputAlgorithm<GPSkEnvironment> {

    public static final int CROSSROAD = 0;
    public static final int TRACKS = 1;
    GPSkEnvironment crossroads;
    GPSkEnvironment tracks;
    private PointDistance pd;

    public Splitter() {
        super(
                new String[]{"crossroads", "tracks"},
                new Filter[]{new Filter.Type("crossroads"), new Filter.DefaultFilter<GPSkEnvironment>()});
        pd = DistanceFactory.getInstance().createNewInstance();
    }

    @Override
    public GPSkEnvironment call() throws Exception {
        SplitterOptions so = (SplitterOptions) options;
        crossroads = (GPSkEnvironment) input[CROSSROAD];
        tracks = (GPSkEnvironment) input[TRACKS];
        
        crossroads.detectBounds();
        crossroads.updateProjection();
        crossroads.activatePointIndex();
        SegmentSplitter filter2 = new SegmentSplitter(so.eps);
//		ConcurrentDataRunner<GPSkSegment> runner = new ConcurrentDataRunner<>(filter2);
        List<GPSkSegment> resultList = filter2.filter(tracks.getSegments());
        EnvironmentBuilder builder = new EnvironmentBuilder(resultList);
        builder.setTitle("splitted").setBounds(tracks.getBounds());
        builder.useProjection();
        return builder.build();
    }

    private class SegmentSplitter implements ConcurrentDataFilter<GPSkSegment> {

        private final double eps;
        private int id;
        
        

        private SegmentSplitter(double eps) {
            this.eps = eps;
            

        }

        @Override
        public List<GPSkSegment> filter(List<GPSkSegment> t) throws Exception {
            List<GPSkSegment> results = new LinkedList<>();
            for (GPSkSegment seg : t) {
                SegmentProcessor sp = new SegmentProcessor(seg, eps, id);
                List<GPSkSegment> l = sp.getSegments();
                ListIterator<GPSkSegment> li = l.listIterator();
                while (li.hasNext()) {
                    GPSkSegment s = li.next();
                    s = trim(s);
                    if (s == null) {
                        li.remove();
                    }
                }
                results.addAll(l);
            }
            return results;
        }

        private GPSkSegment trim(GPSkSegment seg) {
            if(seg.isEmpty()){
                return null;
            }
            ListIterator<GPSkPoint> li = seg.listIterator();
            
            GPSkPoint first = li.next();
            while (li.hasNext()) {
                GPSkPoint second = li.next();
                if (first.equals(second)) {
                    li.remove();
                }
            }
            if (seg.size() <= 1) {
                return null;
            } else {
                return seg;
            }

        }

		/* (non-Javadoc)
		 * @see de.fub.agg2graph.gpsk.algorithms.ConcurrentDataFilter#setID(int, int)
		 */
		@Override
		public void setID(int id, int total) {	
			this.id = id;
			
		}
    }

    private class SegmentProcessor {

        double eps;
        private GPSkSegment segment;
        List<GPSkSegment> results = new LinkedList<>();
        GPSkSegment recentSeg;
        int id;

        private SegmentProcessor(GPSkSegment seg, double eps, int id) {
            this.segment = seg;
            this.eps = eps;
            this.id = id;
        }

        public List<GPSkSegment> getSegments() {
            recentSeg = new GPSkSegment();
            results.add(recentSeg);
            for (GPSkPoint p : segment) {
                GPSkPoint n = getNearest(p);
                if (n == null) {
                    recentSeg.add(p);

                } else if (recentSeg.isEmpty() ) {
                    // do nothing...
                } else if(recentSeg.getLast().equals(n)){
                    // do nothing
                }
                else {
                    recentSeg.add(n);
                    results.add(recentSeg);
                    GPSkPoint cachedPoint = recentSeg.getLast();
                    recentSeg = new GPSkSegment();
                    recentSeg.add(cachedPoint);
                }
            }
            return results;
        }

        private GPSkPoint getNearest(GPSkPoint p) {
        	CircleProcedure cp = new CircleProcedure(p, eps);
        	crossroads.processPoints(cp, id);
            List<GPSkPoint> nb = cp.getResults();
            double min = Double.MAX_VALUE;
            int minIndex = 0;
            if (nb.isEmpty()) {
                return null;
            }
            for (ListIterator<GPSkPoint> li = nb.listIterator(); li.hasNext();) {
                int index = li.nextIndex();
                GPSkPoint p2 = li.next();
                double d = pd.calculate(p2, p);
                if (d < min) {
                    min = d;
                    minIndex = index;
                }
            }
            return nb.get(minIndex);
        }
    }
}
