/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.List;

/**
 *
 * If you have something like a SIMD-task (same instruction, multiple data),
 * your algorithm should implement this interface.
 * The {@link ConcurrentDataRunner} then will use multiple threads to process that data.
 * @author Christian Windolf, christianwindolf@web.de
 * 
 */
public interface ConcurrentDataFilter<T> {
	
	/**
	 * Executes the operation, that is implemented.
	 * The execution will run in multiple threads but it will also use the thread from which
	 * this method has been called. 
	 * @param t List of input data
	 * @return 
	 * @throws Exception
	 */
    public List<T> filter(List<T> t) throws Exception;
    
    public void setID(int id, int total);
    
}
