/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.LinkedList;
import java.util.List;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.logic.PointDistance;
import de.fub.agg2graph.gpsk.logic.ProjectionFactory;
import de.fub.agg2graph.gpsk.logic.TrigonometricDistance;

/**
 * @author Christian Windolf
 *
 */
@Options(BCROptions.class)
public class BoundaryCRRemover extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment>{
	PointDistance pd = new TrigonometricDistance();
	

	/**
	 * 
	 */
	public BoundaryCRRemover() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 */
	public BoundaryCRRemover(GPSkEnvironment input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see de.fub.agg2graph.gpsk.algorithms.AbstractAlgorithm#call()
	 */
	@Override
	public GPSkEnvironment call() throws Exception {
		input.detectBounds();
		input.setProjection(ProjectionFactory.getInstance().createProjection(input.getBounds()));
		List<GPSkPoint> points = input.getPoints();
		List<GPSkPoint> remainingCrossroads = new LinkedList<>();
		for(GPSkPoint p : points){
			if(!pointCloseToBounds(p)){
				remainingCrossroads.add(p);
			}
		}
		EnvironmentBuilder builder = new EnvironmentBuilder(remainingCrossroads);
		builder.detectBounds();
		builder.setFocus(GPSkEnvironment.MetaInformation.FOCUS_ON_POINTS);
		builder.keyValue.put("type", "crossroads");
		builder.setTitle("Crossroads");
		return builder.build();
		
	}
	
	private boolean pointCloseToBounds(GPSkPoint point){
		double eps = ((BCROptions) options).eps;
		double[] bounds = input.getBounds();
		//check to north/south
		GPSkPoint southern = new GPSkPoint(bounds[2], point.getLon());
		GPSkPoint northern = new GPSkPoint(bounds[0], point.getLon());
		if(pd.calculate(point, southern) <= eps){
			return true;
		}
		if(pd.calculate(point, northern) <= eps){
			return true;
		}
		
		GPSkPoint western = new GPSkPoint(point.getLat(), bounds[1]);
		GPSkPoint eastern = new GPSkPoint(point.getLat(), bounds[3]);
		
		if(pd.calculate(point, western) <= eps){
			return true;
		}
		if(pd.calculate(point, eastern) <= eps){
			return true;
		}
		return false;
		
		
	}

	

}
