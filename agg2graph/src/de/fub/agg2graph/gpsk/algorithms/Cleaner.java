/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import java.util.LinkedList;
import java.util.List;

import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.EnvironmentBuilder;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.input.GPSCleaner;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;

/**
 * This algorithm just takes the input and processes it through the {@link GPSCleaner}.
 * That requires conversions to the agg2graph format and backwards conversion for return values.
 * @author Christian Windolf christianwindolf@web.de
 */
@Options(CleaningOptions.class)
public class Cleaner extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> {
    
    private GPSCleaner cleaner;
    
    /**
     * create a new instance of the cleaner with default options.  
     * 
     * @see CleaningOptions
     */
    public Cleaner() {
    }

    
    /**
     * like {@link Cleaner#Cleaner()} but with input data.
     * @param input
     */
    public Cleaner(GPSkEnvironment input) {
        super(input);
    }



    /**
     * execute the cleaning of the segments
     */
    @Override
    public GPSkEnvironment call() throws Exception {
        
        List<GPSkSegment> segments = input.getSegments();
        ConcurrentDataRunner<GPSkSegment> cdr = new ConcurrentDataRunner<>(new ListRunner());
        List<GPSkSegment> list = cdr.filter(segments);
        EnvironmentBuilder builder = new EnvironmentBuilder(list);
        builder.setTitle("cleaned tracks").setBounds(input.getBounds());
        return builder.build();
        
    }

    
    
    private class ListRunner implements ConcurrentDataFilter<GPSkSegment>{

        @Override
        public List<GPSkSegment> filter(List<GPSkSegment> t) throws Exception {
            GPSCleaner cleaner = new GPSCleaner(((CleaningOptions) options).convertToAgg2Graph());
            List<GPSkSegment> cleaned = new LinkedList<>();
            for (GPSkSegment s : t) {
                List<GPSSegment> c = cleaner.clean(s.agg2graphVersion());
                for (GPSSegment s2 : c) {
                    GPSkSegment s3 = new GPSkSegment();
                    for (GPSPoint p : s2) {
                        s3.add(new GPSkPoint(p));
                    }
                    cleaned.add(s3);
                }
            }
            return cleaned;
        }

		/* (non-Javadoc)
		 * @see de.fub.agg2graph.gpsk.algorithms.ConcurrentDataFilter#setID(int, int)
		 */
		@Override
		public void setID(int id, int total) {
			
		}
        
    }
}
