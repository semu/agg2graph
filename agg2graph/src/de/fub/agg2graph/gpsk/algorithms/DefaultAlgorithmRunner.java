/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import static de.fub.agg2graph.gpsk.data.BusEvent.ERROR_MESSAGE;
import static de.fub.agg2graph.gpsk.data.MessageBus.GPSK;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.fub.agg2graph.gpsk.KeyValueStore;
import de.fub.agg2graph.gpsk.annotations.Options;
import de.fub.agg2graph.gpsk.beans.GPSkEnvironment;
import de.fub.agg2graph.gpsk.data.BusEvent;
import de.fub.agg2graph.gpsk.data.EnvironmentManager;
import de.fub.agg2graph.gpsk.data.MessageBus;

/**
 * <p>
 * This class is reponsable to retrieve data from the {@link EnvironmentManager}
 * , execute an arbitrary algorithm, that inherits either
 * {@link AbstractAlgorithm} or {@link MultipleInputAlgorithm}.
 * </p>
 * After execution, it stores the results again in the
 * {@link EnvironmentManager}. If an exception is thrown during execution, it
 * sends messages via log4j and the {@link MessageBus}.
 * 
 * @author Christian Windolf christianwindolf@web.de
 */
public class DefaultAlgorithmRunner implements Runnable {

	private static Logger log = Logger.getLogger(DefaultAlgorithmRunner.class);
	private AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> algorithm;
	private MultipleInputAlgorithm<GPSkEnvironment> algorithm2;
	private EnvironmentManager em = EnvironmentManager.getInstance();
	private List<String> inputList;
	private String output;

	/**
	 * 
	 * @param input
	 *            The key, under which the data can be found in the
	 *            {@link EnvironmentManager}.
	 * @param output
	 *            The key that should be used, to store the data after execution
	 *            in the {@link EnvironmentManager}
	 * @param algorithm
	 *            Instance of the algorithm.
	 */
	public DefaultAlgorithmRunner(String input, String output,
			AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment> algorithm) {
		this.inputList = new LinkedList<>();
		this.inputList.add(input);
		this.output = output;
		this.algorithm = algorithm;
	}

	/**
	 * Same as
	 * {@link DefaultAlgorithmRunner#DefaultAlgorithmRunner(String, String, AbstractAlgorithm)}
	 * , but it creates an instance of the algorithm by itself. If an exception
	 * occurs, it will be just printed out via log4j.
	 * 
	 * @param input
	 * @param output
	 * @param c
	 */
	public DefaultAlgorithmRunner(
			String input,
			String output,
			Class<? extends AbstractAlgorithm<GPSkEnvironment, GPSkEnvironment>> c) {
		this.inputList = new LinkedList<>();
		inputList.add(input);
		this.output = output;
		try {

			this.algorithm = c.newInstance();
		} catch (InstantiationException ex) {
			log.error("unable to run algorithm " + c.getSimpleName(), ex);
		} catch (SecurityException | IllegalAccessException ex) {
			log.error("unable to run algorithm " + c.getSimpleName(), ex);
		}
	}

	/**
	 * Constructor for an algorithm with multiple input.
	 * 
	 * @param inputList
	 * @param output
	 * @param c
	 */
	public DefaultAlgorithmRunner(List<String> inputList, String output,
			Class<? extends MultipleInputAlgorithm<GPSkEnvironment>> c) {

		this.inputList = inputList;
		this.output = output;
		try {

			algorithm2 = c.newInstance();
		} catch (Exception ex) {
			log.error("unable to run algorithm " + c.getSimpleName(), ex);
		}
	}

	/**
	 * Executes the algorithm.
	 * Messages for start and end of the execution are sent via {@link MessageBus}.
	 * If an error occurs, there will be message over log4j and {@link MessageBus}. 
	 */
	@Override
	public void run() {
		BusEvent be1;
		if (algorithm2 == null) {
			be1 = new BusEvent("starting "
					+ algorithm.getClass().getSimpleName(),
					BusEvent.INFO_MESAGE, this);
		} else {
			be1 = new BusEvent("starting "
					+ algorithm2.getClass().getSimpleName(),
					BusEvent.INFO_MESAGE, this);
		}
		MessageBus.getInstance(GPSK).fireEvent(be1);
		List<GPSkEnvironment> envList = new ArrayList<>(inputList.size());
		for (int i = 0; i < inputList.size(); i++) {
			GPSkEnvironment env = em.getEnv(inputList.get(i));
			envList.add(env);
			if (env == null) {
				BusEvent be = new BusEvent(
						"There is no environment for the key "
								+ inputList.get(i)
								+ ". Can't execute algorithm", ERROR_MESSAGE,
						this);
				MessageBus.getInstance(GPSK).fireEvent(be);
				log.error("for the key " + inputList.get(i)
						+ " I found no environment");
				return;
			}
		}

		Options o = getOptions();
		if (o == null) {
			BusEvent be = new BusEvent("The algorithm class "
					+ algorithm.getClass().getSimpleName()
					+ " has no options class annotation", ERROR_MESSAGE, this);
			MessageBus.getInstance(GPSK).fireEvent(be);
		} else {
			Class c = o.value();
			Object options;
			try {
				options = KeyValueStore.loadOptions(c);

			} catch (Exception ex) {
				log.warn("unable to load options", ex);
				options = null;
			}
			Callable<GPSkEnvironment> ca = handleAlgorithm(options, envList);
			try {
                            long start_millies = System.currentTimeMillis();
				GPSkEnvironment result = ca.call();
                                
                                long end_millies = System.currentTimeMillis();
                                log.info("calculated " + ca.getClass().getSimpleName() + " in " + (end_millies - start_millies) + "ms");
                                if(result != null){
				em.put(output, result);
                                }
				BusEvent be2 = new BusEvent("finished "
						+ ca.getClass().getSimpleName(), BusEvent.INFO_MESAGE,
						this);
				MessageBus.getInstance(GPSK).fireEvent(be2);
			} catch (Exception ex) {
				log.error("algorithm " + ca.getClass().getSimpleName()
						+ " just crashed", ex);
				BusEvent be = new BusEvent("algorithm "
						+ ca.getClass().getSimpleName() + " just crashed. An "
						+ ex.getClass().getSimpleName() + " occurred",
						ERROR_MESSAGE, this);
				MessageBus.getInstance(GPSK).fireEvent(be);
				BusEvent b2 = new BusEvent(ex.getMessage(), ERROR_MESSAGE, this);
				MessageBus.getInstance(GPSK).fireEvent(b2);
			}
		}

	}

	private Callable<GPSkEnvironment> handleAlgorithm(Object options,
			List<GPSkEnvironment> list) {
		if (algorithm2 == null) {
			algorithm.setOptions(options);

			algorithm.setInput(list.get(0));
			return algorithm;
		} else {

			algorithm2.setOptions(options);
			ListIterator<GPSkEnvironment> li = list.listIterator();
			while (li.hasNext()) {
				int index = li.nextIndex();
				GPSkEnvironment env = li.next();

				System.out.println("index: " + index);
				algorithm2.setInput(env, index);
			}
			return algorithm2;
		}
	}

	private Options getOptions() {
		if (algorithm == null) {
			Options o = (Options) algorithm2.getClass().getAnnotation(
					Options.class);
			return o;
		} else {
			Options o = (Options) algorithm.getClass().getAnnotation(
					Options.class);
			return o;
		}
	}
	
        @Override
	public String toString(){
		StringBuilder builder = new StringBuilder("AlgorithmExection in Thread ");
		builder.append(Thread.currentThread().getName());
		builder.append(". Algorithm: ");
		if(algorithm != null){
			builder.append(algorithm.getClass().getSimpleName());
		} else if(algorithm2 != null){
			builder.append(algorithm2.getClass().getSimpleName());
		} else {
			builder.append("null");
		}
		return builder.toString();
	}
}
