/*
 * Copyright 2013 Christian Windolf, christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.gpsk.algorithms;

import de.fub.agg2graph.gpsk.annotations.Description;
import de.fub.agg2graph.gpsk.annotations.Min;
import de.fub.agg2graph.gpsk.annotations.Prefix;

/**
 *
 * @author Christian Windolf, christianwindolf@web.de
 */
@Prefix(value = "splitter")
public class SplitterOptions {
    @Min
    @Description(value = "the radius around a crossroad")
    public double eps;
    
    @Override
    public String toString(){
        return "Options: eps=" + eps;
    }
}
