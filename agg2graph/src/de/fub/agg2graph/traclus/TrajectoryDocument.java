package de.fub.agg2graph.traclus;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class TrajectoryDocument {
	private List<Trajectory> trajectoryList;
	
	public List<Trajectory> openDocument(String inputFileName) {
		// read all lines of document
		Path filePath = new File(inputFileName).toPath();
		List<String> lines;
		try {
			lines = Files.readAllLines(filePath, Charset.defaultCharset());
		} catch (IOException e) {
			System.out.println("Unable to open input file");
			return null;
		}
		
		// get dimension and number of trajectories
		int dimension = Integer.parseInt(lines.remove(0).trim());
		@SuppressWarnings("unused")
		int numberOfTrajectories = Integer.parseInt(lines.remove(0).trim());
		
		// trajectory id, number of points, coordinate of a point ...
		trajectoryList = new ArrayList<Trajectory>();
		for (String line : lines) {
			String[] lineArray = line.split(" ");
			
			// get trajectory id and point number
			int trajectoryId = Integer.parseInt(lineArray[0]);
			int numberOfTrajectoryPoints = Integer.parseInt(lineArray[1]);
			
			// construct trajectory
			Trajectory trajectory = new Trajectory(trajectoryId);
			trajectoryList.add(trajectory);
			
			// add all points of line to trajectory
			int offset = 2;
			for (int j = 0; j < numberOfTrajectoryPoints; j++) {
				Point point = new Point(dimension);
				
				// get all point coordinates
				for (int k = 0; k < dimension; k++) {
					point.coordinates[k] = Double.parseDouble(lineArray[offset]);
					offset++;
				}
				
				trajectory.points.add(point);
			}
		}
		
		return trajectoryList;
	}
	
	public boolean generateClusterFile(String outputFilename, double epsilon, int minLns) {
		long t_all_1 = System.currentTimeMillis();
		TraClus traclus = new TraClus(trajectoryList, 2, epsilon, minLns);
		
		// trajectory partition
		long t_partition_1 = System.currentTimeMillis();
		if (!traclus.partition()) {
			System.out.println("Unable to partition a trajectory.");
			return false;
		}
		long t_partition_2 = System.currentTimeMillis();
		
		// density-based clustering & cluster construction
		long t_group_1 = System.currentTimeMillis();
		List<Cluster> clusterList = traclus.group();
		if (clusterList == null) {
			System.out.println("Unable to group trajectory partitions.");
			return false;
		}
		long t_group_2 = System.currentTimeMillis();
		
		long t_all_2 = System.currentTimeMillis();
		System.out.println("Cluster Time: " + (t_all_2 - t_all_1) + "ms");
		System.out.println("Partition Time: " + (t_partition_2 - t_partition_1) + "ms");
		System.out.println("Group Time: " + (t_group_2 - t_group_1) + "ms\n");
		System.out.println("Cluster Ratio: " + traclus.clusterRatio);
		System.out.println("Cluster List Size: " + clusterList.size());
		
		// write generated clusters into file
		FileWriter fw;
		try {
			fw = new FileWriter(new File(outputFilename).getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(clusterList.size() + "\n");
			for (Cluster cluster: clusterList) {
				bw.write(cluster.id + " " +  cluster.points.size() + " ");
				for (int i = 0; i < cluster.points.size(); i++) {
					double roundCoord0 = Math.round(cluster.points.get(i).coordinates[0] * 100)/100.0;
					double roundCoord1 = Math.round(cluster.points.get(i).coordinates[1] * 100)/100.0;
					bw.write(roundCoord0 + " " + roundCoord1 + " ");
				}
				bw.write("\n");
			}
			bw.close();
		} catch (IOException e) {
			System.out.println("Could not create output file.");
			return false;
		}
		
		return true;
	}
}