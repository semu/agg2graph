package de.fub.agg2graph.traclus;

import java.util.ArrayList;
import java.util.List;

public class Cluster {
	public final int id; // the identifier of this cluster
	public int numberOfTrajectories; // number of trajectories belonging to this cluster
	public List<Point> points; // the array of the cluster points

	Cluster(int id) {
		this.id = id;
		points = new ArrayList<Point>();
	}
}