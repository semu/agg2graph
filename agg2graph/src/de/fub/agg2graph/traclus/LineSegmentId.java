package de.fub.agg2graph.traclus;

public class LineSegmentId {
	final int trajectoryId;
	final int order;
	
	public LineSegmentId(int trajectoryId, int order) {
		this.trajectoryId = trajectoryId;
		this.order = order;
	}
}