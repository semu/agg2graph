/*******************************************************************************
   Copyright 2014 Sebastian Müller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/
package de.fub.agg2graph.traclus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.*;
import java.util.*;
import java.io.*;

import org.apache.commons.io.FileUtils;

import de.fub.agg2graph.agg.*;
import de.fub.agg2graph.agg.strategy.*;
import de.fub.agg2graph.agg.tiling.CachingStrategyFactory;
import de.fub.agg2graph.agg.tiling.ICachingStrategy;
import de.fub.agg2graph.gpsk.beans.GPSkPoint;
import de.fub.agg2graph.gpsk.beans.GPSkSegment;
import de.fub.agg2graph.gpsk.utils.SegmentUtils;
import de.fub.agg2graph.graph.RamerDouglasPeuckerFilter;
import de.fub.agg2graph.input.*;
import de.fub.agg2graph.osm.IExporter;
import de.fub.agg2graph.osm.OsmExporter;
import de.fub.agg2graph.osm.UniversalExporter;
import de.fub.agg2graph.roadgen.*;
import de.fub.agg2graph.structs.*;

public class Benchmark {
	public Properties properties = new Properties();
	
	public List<GPSSegment> input(String directory) {
		// get .gpx files from directory
		File folder = new File(directory);
		File[] files = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".gpx");
			}
		});
		
		// load all segments from .gpx files
		List<GPSSegment> allFileSegments = new ArrayList<GPSSegment>();
		for (File file : files) {
			List<GPSSegment> segments = GPXReader.getSegments(file);
			if (segments == null) {
				System.out.println("Bad file: " + file);
				continue;
			} else {
				// iterate over file segments and add every segment to allFileSegments
				for (GPSSegment segment : segments) {
					allFileSegments.add(segment);
				}
			}
		}
		
		return allFileSegments;
	}
	
	public List<GPSSegment> clean(List<GPSSegment> segments) {
		// configure cleaner
		CleaningOptions cleaningOptions = new CleaningOptions();
		
		// number of edges allowed per segment
		cleaningOptions.filterBySegmentLength = Boolean.parseBoolean(properties.getProperty("cleaning.filterBySegmentLength"));
		cleaningOptions.minSegmentLength = Long.parseLong(properties.getProperty("cleaning.minSegmentLength"));
		cleaningOptions.maxSegmentLength = Long.parseLong(properties.getProperty("cleaning.maxSegmentLength"));
		
		// min and max distance between two points (meter)
		cleaningOptions.filterByEdgeLength = Boolean.parseBoolean(properties.getProperty("cleaning.filterByEdgeLength"));
		cleaningOptions.minEdgeLength = Double.parseDouble(properties.getProperty("cleaning.minEdgeLength"));
		cleaningOptions.maxEdgeLength = Double.parseDouble(properties.getProperty("cleaning.maxEdgeLength"));
		
		// length change between two consecutive edges
		cleaningOptions.filterByEdgeLengthIncrease = Boolean.parseBoolean(properties.getProperty("cleaning.filterByEdgeLengthIncrease"));
		cleaningOptions.minEdgeLengthIncreaseFactor = Double.parseDouble(properties.getProperty("cleaning.minEdgeLengthIncreaseFactor"));
		cleaningOptions.minEdgeLengthAfterIncrease = Double.parseDouble(properties.getProperty("cleaning.minEdgeLengthIncreaseFactor"));
		
		// zigzag
		cleaningOptions.filterZigzag = Boolean.parseBoolean(properties.getProperty("cleaning.filterZigzag"));
		cleaningOptions.maxZigzagAngle = Double.parseDouble(properties.getProperty("cleaning.maxZigzagAngle"));
		
		// fake circle
		cleaningOptions.filterFakeCircle = Boolean.parseBoolean(properties.getProperty("cleaning.filterFakeCircle"));
		cleaningOptions.maxFakeCircleAngle = Double.parseDouble(properties.getProperty("cleaning.maxFakeCircleAngle"));
/*		
		// outliers
		cleaningOptions.filterOutliers = Boolean.parseBoolean(properties.getProperty("cleaning.filterOutliers"));
		cleaningOptions.maxNumOutliers = Integer.parseInt(properties.getProperty("cleaning.maxNumOutliers"));
		
		// TODO k-Anonymity: At most one value must be true
		cleaningOptions.useMin = Boolean.parseBoolean(properties.getProperty("cleaning.useMin"));
		cleaningOptions.useMean = Boolean.parseBoolean(properties.getProperty("cleaning.useMean"));
		cleaningOptions.onlySame = Boolean.parseBoolean(properties.getProperty("cleaning.onlySame"));
		*/
		// ramer dougles peucker filter epsilon
		double rpdfEpsilon = Double.parseDouble(properties.getProperty("cleaning.rpdfEpsilon"));
		
		// initialise cleaner and rdpf
		GPSCleaner gpsCleaner = new GPSCleaner(cleaningOptions);
		RamerDouglasPeuckerFilter rdpf = new RamerDouglasPeuckerFilter(rpdfEpsilon);
		
		// clean all segments
		List<GPSSegment> allCleanSegments = new ArrayList<GPSSegment>();
		for (GPSSegment segment : segments) {
			for (GPSSegment cleanSegment : gpsCleaner.clean(segment)) {
				cleanSegment = rdpf.simplify(cleanSegment);
				allCleanSegments.add(cleanSegment);
			}
		}
		return segments;
	}
	
	public AggContainer agg(List<GPSSegment> segments) {
		// configure strategy
		Class<? extends IAggregationStrategy> strategy = null;
		switch (properties.getProperty("agg.strategy")) {
			case "PathScoreMatchDefaultMergeStrategy":
				strategy = PathScoreMatchDefaultMergeStrategy.class;
				break;
			case "PathScoreMatchAttractionMergeStrategy":
				strategy = PathScoreMatchAttractionMergeStrategy.class;
				break;
			case "PathScoreMatchFrechetMergeStrategy":
				strategy = PathScoreMatchFrechetMergeStrategy.class;
				break;
			case "PathScoreMatchIterativeMergeStrategy":
				strategy = PathScoreMatchIterativeMergeStrategy.class;
				break;
			case "FrechetMatchAttractionMergeStrategy":
				strategy = FrechetMatchAttractionMergeStrategy.class;
				break;
			case "FrechetMatchIterativeMergeStrategy":
				strategy = FrechetMatchIterativeMergeStrategy.class;
				break;
			case "GpxmergeMatchAttractionMergeStrategy":
				strategy = GpxmergeMatchAttractionMergeStrategy.class;
				break;
			case "GpxmergeMatchIterativeMergeStrategy":
				strategy = GpxmergeMatchIterativeMergeStrategy.class;
				break;
			case "HausdorffMatchAttractionMergeStrategy":
				strategy = HausdorffMatchAttractionMergeStrategy.class;
				break;
			case "HausdorffMatchDefaultMergeStrategy":
				strategy = HausdorffMatchDefaultMergeStrategy.class;
				break;
			case "HausdorffMatchFrechetMergeStrategy":
				strategy = HausdorffMatchFrechetMergeStrategy.class;
				break;
			case "HausdorffMatchIterativeMergeStrategy":
				strategy = HausdorffMatchIterativeMergeStrategy.class;
				break;
			case "OriginalDefaultAggregationStrategy":
				strategy = OriginalDefaultAggregationStrategy.class;
				break;
			case "TraClusIterativeAggregationStrategy":
				strategy = TraClusIterativeAggregationStrategy.class;
				break;
		}
		
		// strategy configuration
		/*
		int maxLookahead = Integer.parseInt(properties.getProperty("agg.strategy.maxLookahead"));
		double maxPathDifference = Double.parseDouble(properties.getProperty("agg.strategy.maxPathDifference"));
		double maxInitDistance = Double.parseDouble(properties.getProperty("agg.strategy.maxInitDistance"));
		*/
		
		AggregationStrategyFactory.setClass(strategy);
		IAggregationStrategy aggStrategy = AggregationStrategyFactory.getObject();
		/*((AbstractAggregationStrategy) aggStrategy).setMaxLookahead(maxLookahead);
		((AbstractAggregationStrategy) aggStrategy).setMaxPathDifference(maxPathDifference);
		((AbstractAggregationStrategy) aggStrategy).setMaxInitDistance(maxInitDistance);*/
		
		// configure matcher and merger
		ITraceDistance distance;
		List<ClassObjectEditor> mergeSettingsList;
		ClassObjectEditor mergeSettings;
		switch (strategy.getSimpleName()) {
			case "PathScoreMatchDefaultMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((PathScoreDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((PathScoreDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((PathScoreDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((PathScoreDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((PathScoreDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((PathScoreDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				((PathScoreDistance) distance).maxAngle = Double.parseDouble(properties.getProperty("agg.matcher.maxAngle"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "PathScoreMatchAttractionMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((PathScoreDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((PathScoreDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((PathScoreDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((PathScoreDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((PathScoreDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((PathScoreDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				((PathScoreDistance) distance).maxAngle = Double.parseDouble(properties.getProperty("agg.matcher.maxAngle"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "PathScoreMatchFrechetMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((PathScoreDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((PathScoreDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((PathScoreDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((PathScoreDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((PathScoreDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((PathScoreDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				((PathScoreDistance) distance).maxAngle = Double.parseDouble(properties.getProperty("agg.matcher.maxAngle"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
						case "criticalEdges":
							editableObject.value = new HashSet<GPSEdge>();	// TODO: add critical edges to configuration file
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "PathScoreMatchIterativeMergeStrategy": 
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((PathScoreDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((PathScoreDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((PathScoreDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((PathScoreDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((PathScoreDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((PathScoreDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				((PathScoreDistance) distance).maxAngle = Double.parseDouble(properties.getProperty("agg.matcher.maxAngle"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
						case "criticalEdges":
							editableObject.value = new HashSet<GPSEdge>();
							break;
						case "delta":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.delta"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "FrechetMatchAttractionMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((FreeSpaceMatch) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((FreeSpaceMatch) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "FrechetMatchIterativeMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((FreeSpaceMatch) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((FreeSpaceMatch) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
						case "delta":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.delta"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "GpxmergeMatchAttractionMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((GpxmergeTraceDistance) distance).angleFactor = Double.parseDouble(properties.getProperty("agg.matcher.angleFactor"));
				((GpxmergeTraceDistance) distance).maxAngle = Double.parseDouble(properties.getProperty("agg.matcher.maxAngle"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "GpxmergeMatchIterativeMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((GpxmergeTraceDistance) distance).angleFactor = Double.parseDouble(properties.getProperty("agg.matcher.angleFactor"));
				((GpxmergeTraceDistance) distance).maxAngle = Double.parseDouble(properties.getProperty("agg.matcher.maxAngle"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
						case "delta":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.delta"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "HausdorffMatchAttractionMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((HausdorffTraceDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((HausdorffTraceDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((HausdorffTraceDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((HausdorffTraceDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((HausdorffTraceDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((HausdorffTraceDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "HausdorffMatchDefaultMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((HausdorffTraceDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((HausdorffTraceDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((HausdorffTraceDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((HausdorffTraceDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((HausdorffTraceDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((HausdorffTraceDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "HausdorffMatchFrechetMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((HausdorffTraceDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((HausdorffTraceDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((HausdorffTraceDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((HausdorffTraceDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((HausdorffTraceDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((HausdorffTraceDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
						case "criticalEdges":
							editableObject.value = new HashSet<GPSEdge>();
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "HausdorffMatchIterativeMergeStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((HausdorffTraceDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((HausdorffTraceDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((HausdorffTraceDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((HausdorffTraceDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((HausdorffTraceDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((HausdorffTraceDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
						case "delta":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.delta"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "OriginalDefaultAggregationStrategy":
				// configure matcher
				distance = aggStrategy.getTraceDist();
				((OriginalDefaultTraceDistance) distance).aggReflectionFactor = Double.parseDouble(properties.getProperty("agg.matcher.aggReflectionFactor"));
				((OriginalDefaultTraceDistance) distance).maxOutliners = Integer.parseInt(properties.getProperty("agg.matcher.maxOutliners"));
				((OriginalDefaultTraceDistance) distance).maxDistance = Double.parseDouble(properties.getProperty("agg.matcher.maxDistance"));
				((OriginalDefaultTraceDistance) distance).maxLookahead = Integer.parseInt(properties.getProperty("agg.matcher.maxLookahead"));
				((OriginalDefaultTraceDistance) distance).maxPathDifference = Double.parseDouble(properties.getProperty("agg.matcher.maxPathDifference"));
				((OriginalDefaultTraceDistance) distance).minLengthFirstSegment = Integer.parseInt(properties.getProperty("agg.matcher.minLengthFirstSegment"));
				((OriginalDefaultTraceDistance) distance).maxAngle = Double.parseDouble(properties.getProperty("agg.matcher.maxAngle"));
				
				// configure merger
				mergeSettingsList = aggStrategy.getSettings();
				mergeSettings = mergeSettingsList.get(mergeSettingsList.size() - 1);
				for (EditableObject editableObject : mergeSettings.getEditableObjects()) {
					switch (editableObject.name) {
						case "maxLookahead":
							editableObject.value = Integer.parseInt(properties.getProperty("agg.merger.maxLookahead"));
							break;
						case "minContinuationAngle":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.minContinuationAngle"));
							break;
						case "maxPointGhostDist":
							editableObject.value = Double.parseDouble(properties.getProperty("agg.merger.maxPointGhostDist"));
							break;
					}
					System.out.println(editableObject.name + " has value " + editableObject.value + ".");
				}
				break;
			case "TraClusIterativeAggregationStrategy":
				// configure global parameters
				((TraClusIterativeAggregationStrategy) aggStrategy).usePartitioning = Boolean.parseBoolean(properties.getProperty("agg.traclus.usePartitioning"));
				((TraClusIterativeAggregationStrategy) aggStrategy).useNormalizer = Boolean.parseBoolean(properties.getProperty("agg.traclus.useNormalizer"));
				((TraClusIterativeAggregationStrategy) aggStrategy).useTraClusMatching = Boolean.parseBoolean(properties.getProperty("agg.traclus.useTraClusMatching"));
				((TraClusIterativeAggregationStrategy) aggStrategy).useTraClusMerging = Boolean.parseBoolean(properties.getProperty("agg.traclus.useTraClusMerging"));
				
				// configure partitioning
				((TraClusIterativeAggregationStrategy) aggStrategy).w_perp = Double.parseDouble(properties.getProperty("agg.traclus.w_perp"));
				((TraClusIterativeAggregationStrategy) aggStrategy).w_par = Double.parseDouble(properties.getProperty("agg.traclus.w_par"));
				((TraClusIterativeAggregationStrategy) aggStrategy).w_angle = Double.parseDouble(properties.getProperty("agg.traclus.w_angle"));
				((TraClusIterativeAggregationStrategy) aggStrategy).MDL_COST_ADVANTAGE = Double.parseDouble(properties.getProperty("agg.traclus.MDL_COST_ADVANTAGE"));
				
				// configure normalizing
				((TraClusIterativeAggregationStrategy) aggStrategy).normalizeDistance = Double.parseDouble(properties.getProperty("agg.traclus.normalizeDistance"));
				((TraClusIterativeAggregationStrategy) aggStrategy).normalizeTolerance = Double.parseDouble(properties.getProperty("agg.traclus.normalizeTolerance"));
				((TraClusIterativeAggregationStrategy) aggStrategy).normalizeAllowShortEdges = Boolean.parseBoolean(properties.getProperty("agg.traclus.normalizeAllowShortEdges"));
				
				// configure matching
				((TraClusIterativeAggregationStrategy) aggStrategy).maxLookahead = Integer.parseInt(properties.getProperty("agg.strategy.maxLookahead"));
				((TraClusIterativeAggregationStrategy) aggStrategy).maxPathDifference = Double.parseDouble(properties.getProperty("agg.strategy.maxPathDifference"));
				((TraClusIterativeAggregationStrategy) aggStrategy).maxInitDistance = Double.parseDouble(properties.getProperty("agg.strategy.maxInitDistance"));
				
				// configure rtree and traclus distance parameters
				((TraClusIterativeAggregationStrategy) aggStrategy).allowMultipleMatches = Boolean.parseBoolean(properties.getProperty("agg.traclus.allowMultipleMatches"));
				((TraClusIterativeAggregationStrategy) aggStrategy).maxTraClusDistance = Double.parseDouble(properties.getProperty("agg.traclus.maxTraClusDistance"));
				((TraClusIterativeAggregationStrategy) aggStrategy).rTreeSearchFactor = Double.parseDouble(properties.getProperty("agg.traclus.rTreeSearchFactor"));
				((TraClusIterativeAggregationStrategy) aggStrategy).rTreeDimX = Double.parseDouble(properties.getProperty("agg.traclus.rTreeDimX"));
				((TraClusIterativeAggregationStrategy) aggStrategy).rTreeDimY = Double.parseDouble(properties.getProperty("agg.traclus.rTreeDimY"));
				((TraClusIterativeAggregationStrategy) aggStrategy).rTreeDimAngle = Double.parseDouble(properties.getProperty("agg.traclus.rTreeDimAngle"));
				((TraClusIterativeAggregationStrategy) aggStrategy).rTreeDimLength = Double.parseDouble(properties.getProperty("agg.traclus.rTreeDimLength"));
				break;
		}
		
		ICachingStrategy cacheStrategy = CachingStrategyFactory.getObject();
		String aggFilename = "test/agg/berlin/";
		AggContainer aggContainer = AggContainer.createContainer(new File(aggFilename), aggStrategy, cacheStrategy);
		for (int i = 0; i < segments.size(); i++) {
			if (i == 0) {
				aggContainer.addSegment(segments.get(i), true);
			} else {
				aggContainer.addSegment(segments.get(i), false);
			}
		}
		
		return aggContainer;
	}
	
	public RoadNetwork road(AggContainer aggContainer) {
		Class<? extends IAggFilter> filter = DefaultAggFilter.class;
		AggFilterFactory.setClass(filter);
		DefaultAggFilter aggFilter = (DefaultAggFilter) AggFilterFactory.getObject();
		aggFilter.minEdgeWeight = Double.parseDouble(properties.getProperty("road.minEdgeWeight"));
		
		Class<? extends IRoadTypeClassifier> classifier = DefaultRoadTypeClassifier.class;
		RoadTypeClassifierFactory.setClass(classifier);
		DefaultRoadTypeClassifier roadTypeClassifier = (DefaultRoadTypeClassifier) RoadTypeClassifierFactory.getObject();
		roadTypeClassifier.minWeightPrimary = Double.parseDouble(properties.getProperty("road.minWeightPrimary"));
		roadTypeClassifier.minWeightSecondary = Double.parseDouble(properties.getProperty("road.minWeightSecondary"));
		roadTypeClassifier.minWidthPrimary = Double.parseDouble(properties.getProperty("road.minWidthPrimary"));
		roadTypeClassifier.minWidthSecondary = Double.parseDouble(properties.getProperty("road.minWidthSecondary"));
		
		Class<? extends IRoadNetworkFilter> networkFilter = DefaultRoadNetworkFilter.class;
		RoadNetworkFilterFactory.setClass(networkFilter);
		DefaultRoadNetworkFilter roadNetworkFilter = (DefaultRoadNetworkFilter) RoadNetworkFilterFactory.getObject();
		roadNetworkFilter.removeBorderRoads = Boolean.parseBoolean(properties.getProperty("road.removeBorderRoads"));
		roadNetworkFilter.minBorderRoadLength = Double.parseDouble(properties.getProperty("road.minBorderRoadLength"));
		roadNetworkFilter.removeIsolatedRoads = Boolean.parseBoolean(properties.getProperty("road.removeIsolatedRoads"));
		roadNetworkFilter.minIsolatedRoadLength = Double.parseDouble(properties.getProperty("road.minIsolatedRoadLength"));
		
		Class<? extends IRoadObjectMerger> objectMerger = DefaultRoadObjectMerger.class;
		RoadObjectMergerFactory.setClass(objectMerger);
		DefaultRoadObjectMerger roadObjectMerger = (DefaultRoadObjectMerger) RoadObjectMergerFactory.getObject();
		roadObjectMerger.maxIntersectionMergeDistance = Double.parseDouble(properties.getProperty("road.maxIntersectionMergeDistance"));
		roadObjectMerger.maxRoadMergeDistance = Double.parseDouble(properties.getProperty("road.maxRoadMergeDistance"));
		
		RoadNetwork roadNetwork = new RoadNetwork();
		roadNetwork.aggFilter = aggFilter;
		roadNetwork.roadTypeClassifier = roadTypeClassifier;
		roadNetwork.parse(aggContainer);
		return roadNetwork;
	}
	
	public File osm(RoadNetwork roadNetwork) {
		File targetFile = new File(properties.getProperty("output"));
		IExporter exporter = new UniversalExporter();
		
		// configure ramer douglas peucker filter
		double rpdfEpsilon = Double.parseDouble(properties.getProperty("output.rpdfEpsilon"));
		double maxSegmentLength = Double.parseDouble(properties.getProperty("output.maxSegmentLength"));
		((OsmExporter) exporter).rdpf = new RamerDouglasPeuckerFilter(rpdfEpsilon);
		((OsmExporter) exporter).rdpf.setMaxEdgeLength(maxSegmentLength);
		
		// configure xml document
		((OsmExporter) exporter).uid = properties.getProperty("output.uid");
		((OsmExporter) exporter).user = properties.getProperty("output.user");
		((OsmExporter) exporter).osmNodeStartId = Integer.parseInt(properties.getProperty("output.osmNodeStartId"));
		((OsmExporter) exporter).osmWayStartId = Integer.parseInt(properties.getProperty("output.osmWayStartId"));
		
		// export osm file
		exporter.setTargetPrefix(targetFile);
		exporter.export(roadNetwork);
		return targetFile;
	}
	
	public static void timeCostExperiment(int numberOfExperiments) {
		// load experiment data into benchmark objects
		Benchmark exp_timecost_traclus = new Benchmark();
		Benchmark exp_timecost_traclus_rtree = new Benchmark();
		Benchmark exp_timecost_default = new Benchmark();
		Benchmark exp_timecost_default_normalize = new Benchmark();
		
		try {
			exp_timecost_traclus.properties.load(new FileInputStream("traclus_experiments/exp_timecost_traclus.properties"));
			exp_timecost_traclus_rtree.properties.load(new FileInputStream("traclus_experiments/exp_timecost_traclus_rtree.properties"));
			exp_timecost_default.properties.load(new FileInputStream("traclus_experiments/exp_timecost_default.properties"));
			exp_timecost_default_normalize.properties.load(new FileInputStream("traclus_experiments/exp_timecost_default_normalize.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// clean segments
		/*
		List<GPSSegment> cleanedSegments1 = exp_timecost_traclus.clean(exp_timecost_traclus.input(exp_timecost_traclus.properties.getProperty("input")));
		List<GPSSegment> cleanedSegments2 = exp_timecost_traclus_rtree.clean(exp_timecost_traclus_rtree.input(exp_timecost_traclus_rtree.properties.getProperty("input")));
		List<GPSSegment> cleanedSegments3 = exp_timecost_default.clean(exp_timecost_default.input(exp_timecost_default.properties.getProperty("input")));
		*/
		List<GPSSegment> cleanedSegments4 = exp_timecost_default_normalize.clean(exp_timecost_default_normalize.input(exp_timecost_default_normalize.properties.getProperty("input")));
		
		// experiments
		long s, e;
		List<Long> performanceTraClus = new ArrayList<Long>();
		List<Long> performanceTraClusRtree = new ArrayList<Long>();
		List<Long> performanceDefault = new ArrayList<Long>();
		List<Long> performanceDefaultNormalize = new ArrayList<Long>();
		for (int i = 0; i < numberOfExperiments; i++) {
			// performance of traclus
			System.out.println("# Experiment TraClus " + i + " started.");
			s = System.currentTimeMillis();
			// exp_timecost_traclus.agg(cleanedSegments1);
			e = System.currentTimeMillis();
			performanceTraClus.add((e - s));
			
			// performance of traclus with rtree
			System.out.println("# Experiment TraClus RTree " + i + " started.");
			s = System.currentTimeMillis();
			// exp_timecost_traclus_rtree.agg(cleanedSegments2);
			e = System.currentTimeMillis();
			performanceTraClusRtree.add((e - s));
			
			// performance of default algorithm (normalizing off)
			System.out.println("# Experiment Default " + i + " started.");
			s = System.currentTimeMillis();
			// exp_timecost_default.agg(cleanedSegments3);
			e = System.currentTimeMillis();
			performanceDefault.add((e - s));
			
			// performance of default algorithm (normalizing on)
			System.out.println("# Experiment Default Normalize " + i + " started.");
			s = System.currentTimeMillis();
			exp_timecost_default_normalize.agg(cleanedSegments4);
			e = System.currentTimeMillis();
			performanceDefaultNormalize.add((e - s));
			
			// TODO performance of default algoritm with rtree
		}
		
		// sumarize information
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		File resultFile = new File("traclus_experiments/results.txt");
		try {
			FileUtils.writeStringToFile(resultFile, "# Experiment with " + numberOfExperiments + " iterations on " + dateFormat.format(new Date()) + ".\n", true);
		} catch (IOException ex) { ex.printStackTrace(); }
		long sum = 0;
		for (Long measure: performanceTraClus) sum += measure;
		try {
			FileUtils.writeStringToFile(resultFile, "Average time TraClus: " + (sum / numberOfExperiments) + "ms\n", true);
		} catch (IOException ex) { ex.printStackTrace(); }
		System.out.println("Average time TraClus: " + (sum / numberOfExperiments) + "ms");
		
		sum = 0;
		for (Long measure: performanceTraClusRtree) sum += measure;
		try {
			FileUtils.writeStringToFile(resultFile, "Average time TraClus RTree: " + (sum / numberOfExperiments) + "ms\n", true);
		} catch (IOException ex) { ex.printStackTrace(); }
		System.out.println("Average time TraClus RTree: " + (sum / numberOfExperiments) + "ms");
		
		sum = 0;
		for (Long measure: performanceDefault) sum += measure;
		try {
			FileUtils.writeStringToFile(resultFile, "Average time Default: " + (sum / numberOfExperiments) + "ms\n", true);
		} catch (IOException ex) { ex.printStackTrace(); }
		System.out.println("Average time Default: " + (sum / numberOfExperiments) + "ms");
		
		sum = 0;
		for (Long measure: performanceDefaultNormalize) sum += measure;
		try {
			FileUtils.writeStringToFile(resultFile, "Average time Default Normalize: " + (sum / numberOfExperiments) + "ms\n", true);
		} catch (IOException ex) { ex.printStackTrace(); }
		System.out.println("Average time Default Normalize: " + (sum / numberOfExperiments) + "ms");
		
		// input data stats
		List<GPSSegment> segments = exp_timecost_traclus.input(exp_timecost_traclus.properties.getProperty("input"));
		// GPSPoint currentPoint, nextPoint;
		long numberOfPoints = 0;
		double overallNetworkLength = 0.0;
		for (GPSSegment segment: segments) {
			numberOfPoints += segment.size();
			GPSkSegment gpskSegment = new GPSkSegment();
			for (GPSPoint point: segment) {
				GPSkPoint gpskPoint = new GPSkPoint(point);
				gpskSegment.add(gpskPoint);
			}
			overallNetworkLength += SegmentUtils.length(gpskSegment);
		}
		
		System.out.println("Number of Trajectories in Input File: " + segments.size());
		System.out.println("Number of Points in Input Data: " + numberOfPoints);
		System.out.println("Overall Trajectory Length in Input Data: " + overallNetworkLength + "m");
	}
	
	public static void main(String[] args) {
		// set log level
		Logger log = LogManager.getLogManager().getLogger("");
		for (Handler h : log.getHandlers()) {
		    h.setLevel(Level.WARNING);
		}
		
		// start experiments
		timeCostExperiment(2);
		
		// TODO time cost and network stats (#points, overall network length, etc.) for different partitioning weights (MDL_COST_ADV)
		// TODO time cost and network stats for different normalization distances
		// TODO time cost and network stats for different rtree dimensions
	}
}