package de.fub.agg2graph.traclus;

public final class MercatorTransformation {
	private final int TILE_SIZE = 256;
	private Point _pixelOrigin;
	private double _pixelsPerLonDegree;
	private double _pixelsPerLonRadian;
	
	public MercatorTransformation() {
		this._pixelOrigin = new Point(2);
		this._pixelOrigin.coordinates[0] = TILE_SIZE / 2.0;
		this._pixelOrigin.coordinates[1] = TILE_SIZE / 2.0;
		this._pixelsPerLonDegree = TILE_SIZE / 360.0;
		this._pixelsPerLonRadian = TILE_SIZE / (2 * Math.PI);
	}

	double bound(double val, double valMin, double valMax) {
		double res;
		res = Math.max(val, valMin);
		res = Math.min(val, valMax);
		return res;
	}

	double degreesToRadians(double deg) {
		return deg * (Math.PI / 180);
	}

	double radiansToDegrees(double rad) {
		return rad / (Math.PI / 180);
	}

	
	public double fromLatToY(double lat, int zoom) {
		int numTiles = 1 << zoom;
		double y;
		// Truncating to 0.9999 effectively limits latitude to 89.189. This is
		// about a third of a tile past the edge of the world tile.
		double siny = bound(Math.sin(degreesToRadians(lat)), -0.9999, 0.9999);
		y = _pixelOrigin.coordinates[1] + 0.5 * Math.log((1 + siny) / (1 - siny))
				* -_pixelsPerLonRadian;
		y = y * numTiles;
		return y;
	}
	
	public double fromLonToX(double lng, int zoom) {
		int numTiles = 1 << zoom;
		double x;
		x = _pixelOrigin.coordinates[0] + lng * _pixelsPerLonDegree;
		x = x * numTiles;
		return x;
	}
	
	public Point fromLatLngToPoint(double lat, double lng, int zoom) {
		int numTiles = 1 << zoom;
		Point point = new Point(2);
		point.coordinates[0] = _pixelOrigin.coordinates[0] + lng * _pixelsPerLonDegree;
		point.coordinates[0] = point.coordinates[0] * numTiles;

		// Truncating to 0.9999 effectively limits latitude to 89.189. This is
		// about a third of a tile past the edge of the world tile.
		double siny = bound(Math.sin(degreesToRadians(lat)), -0.9999, 0.9999);
		point.coordinates[1] = _pixelOrigin.coordinates[1] + 0.5 * Math.log((1 + siny) / (1 - siny))
				* -_pixelsPerLonRadian;
		point.coordinates[1] = point.coordinates[1] * numTiles;
		return point;
	}

	public double fromYToLat(double y, int zoom) {
		int numTiles = 1 << zoom;
		y = y / numTiles;
		
		double latRadians = (y - _pixelOrigin.coordinates[1]) / -_pixelsPerLonRadian;
		double lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians))
				- Math.PI / 2);
		return lat;
	}

	public double fromXToLon(double x, int zoom) {
		int numTiles = 1 << zoom;
		x = x / numTiles;
		
		double lng = (x - _pixelOrigin.coordinates[0]) / _pixelsPerLonDegree;
		
		return lng;
	}

	public Point fromPointToLatLng(Point point, int zoom) {
		int numTiles = 1 << zoom;
		point.coordinates[0] = point.coordinates[0] / numTiles;
		point.coordinates[1] = point.coordinates[1] / numTiles;
		
		double lng = (point.coordinates[0] - _pixelOrigin.coordinates[0]) / _pixelsPerLonDegree;
		double latRadians = (point.coordinates[1] - _pixelOrigin.coordinates[1]) / -_pixelsPerLonRadian;
		double lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians))
				- Math.PI / 2);
		
		Point tmpPoint = new Point(2);
		tmpPoint.coordinates[0] = lat;
		tmpPoint.coordinates[1] = lng;
		return tmpPoint;
	}

	public static void main(String[] args) {
		MercatorTransformation gmap2 = new MercatorTransformation();
		Point point1 = gmap2.fromLatLngToPoint(41.850033, -87.6500523, 15);
		System.out.println(point1.coordinates[0] + "   " + point1.coordinates[1]);
		Point point2 = gmap2.fromPointToLatLng(point1, 15);
		System.out.println(point2.coordinates[0] + "   " + point2.coordinates[1]);
	}
}