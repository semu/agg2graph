package de.fub.agg2graph.traclus;

import java.util.Vector;

public class Trajectory {
	public final int id;					// identifier of this trajectory
	public Vector<Point> points;			// trajectory points
	public Vector<Point> partitionPoints;	// partition points
	
	public Trajectory(int id) {
		this.id = id;
		this.points = new Vector<Point>();
		this.partitionPoints = new Vector<Point>();
	}
	
	@Override
	public String toString() {
		return "Trajectory [id=" + id + ", points=" + points + "]";
	}
}