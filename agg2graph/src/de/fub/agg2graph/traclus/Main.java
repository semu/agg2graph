package de.fub.agg2graph.traclus;

public class Main {
	public static void main(String args[]) {
		if (args.length != 4){
			System.out.println("USAGE: traclus <input file> <output file> [<eps> <MinLns>]");
			System.out.println("\t- <input file>: a trajectory file");
			System.out.println("\t- <output file>: a cluster file");
			System.out.println("\t- <eps>: the parameter epsilon (double)");
			System.out.println("\t- <MinLns>: the parameter MinLns (integer)");
			System.exit(1);
		}
		
		TrajectoryDocument document = new TrajectoryDocument();
		
		// exit if document could not be parsed
		if (document.openDocument(args[0]) == null) {
			System.out.println("Cannot parse trajectory file.");
			System.exit(1);
		}
		
		// generate cluster file
		if (!document.generateClusterFile(args[1], Double.parseDouble(args[2]), Integer.parseInt(args[3]))) {
			System.out.println("Cannot generate cluster file.");
			System.exit(1);
		}
		
		System.out.println("Clustering has been completed sucessfully.");
	}
}
