package de.fub.agg2graph.traclus;

import java.util.ArrayList;
import java.util.List;

public class LineSegmentCluster {
	final int id;
	boolean enabled;
	public double cosTheta;
	public double sinTheta;
	public Point avgDirectionVector;
	List<CandidateClusterPoint> candidatePoints;
	List<Point> clusterPoints;
	List<Integer> trajectoryIds;
	int numberOfLineSegments;

	LineSegmentCluster(int id, int dimension) {
		this.id = id;
		this.enabled = false;
		this.cosTheta = 0.0;
		this.sinTheta = 0.0;
		this.avgDirectionVector = new Point(dimension);
		this.candidatePoints = new ArrayList<CandidateClusterPoint>();
		this.trajectoryIds = new ArrayList<Integer>();
		this.clusterPoints = new ArrayList<Point>();
		this.numberOfLineSegments = 0;
	}
}