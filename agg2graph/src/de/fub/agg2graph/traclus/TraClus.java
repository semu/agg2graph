package de.fub.agg2graph.traclus;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TraClus {
	private static final int UNCLASSIFIED = -2;
	private static final int NOISE = -1;
	
	public double MDL_COST_ADVANTAGE = 25;
	public double MIN_LINESEGMENT_LENGTH = 0.001; // 50.0
	
	public double w_perp;
	public double w_par;
	public double w_angle;
	
	public boolean sweepCrossesMultipleEdges;
	
	private int dimension;
	private double epsilon;
	private int minLns;
	
	public List<Trajectory> trajectoryList;
	public List<Cluster> clusterList;
	public List<LineSegmentId> idArray;
	public List<Point> trajectoryPartitionsAsPoints;
	public List<Integer> componentIdArray;
	public List<LineSegmentCluster> lineSegmentClusters;
	
	private Point startPoint1;
	private Point startPoint2;
	private Point endPoint1;
	private Point endPoint2;
	
	private Point projectionPoint;
	private double coefficient;
	public int currComponentId;
	
	public double clusterRatio;
	
	public TraClus(List<Trajectory> trajectoryList, int dimension, double epsilon, int minLns) {
		this.trajectoryList = trajectoryList;
		this.dimension = dimension;
		this.epsilon = epsilon;
		this.minLns = minLns;
		
		this.w_perp = 1;
		this.w_par = 1;
		this.w_angle = 1;
		
		this.sweepCrossesMultipleEdges = false;
		
		this.idArray = new ArrayList<LineSegmentId>();
		this.trajectoryPartitionsAsPoints = new ArrayList<Point>();
		
		this.startPoint1 = new Point(dimension);
		this.startPoint2 = new Point(dimension);
		this.endPoint1 = new Point(dimension);
		this.endPoint2 = new Point(dimension);
	}
	
	public TraClus(List<Trajectory> trajectoryList, int dimension, double epsilon, int minLns, double w_perp, double w_par, double w_angle) {
		this.trajectoryList = trajectoryList;
		this.dimension = dimension;
		this.epsilon = epsilon;
		this.minLns = minLns;
		
		this.w_perp = w_perp;
		this.w_par = w_par;
		this.w_angle = w_angle;
		
		this.idArray = new ArrayList<LineSegmentId>();
		this.trajectoryPartitionsAsPoints = new ArrayList<Point>();
		
		this.startPoint1 = new Point(dimension);
		this.startPoint2 = new Point(dimension);
		this.endPoint1 = new Point(dimension);
		this.endPoint2 = new Point(dimension);
	}
	
	public boolean partition() {
		// find optimal partition for every trajectory
		for (Trajectory trajectory: trajectoryList) {
			findOptimalPartition(trajectory);
		}
		
		// store components into index for group phase
		if (!storeClusterComponentIntoIndex()) {
			return false;
		}
		
		return true;
	}
	
	public List<Cluster> group() {
		// density-based clustering
		if (!performDBSCAN()) {
			// System.out.println("Unable to perform DBSCAN.");
			return null;
		}
		
		// cluster construction
		if (!constructCluster()) {
			// System.out.println("Unable to construct a cluster.");
			return null;
		}
		
		return clusterList;
	}
	
	/* PARTITION */
	
	public void findOptimalPartition(Trajectory trajectory) {
		// add start point to partition
		trajectory.partitionPoints.add(trajectory.points.elementAt(0));
		
		// partitioning algorithm
		int numberOfPoints = trajectory.points.size();
		double mdl_nopar, mdl_par;
		int startIndex = 0, length;
		for (;;) {
			mdl_nopar = mdl_par = 0;
			for (length = 1; startIndex + length < numberOfPoints; length++) {
				// compute the total length of the trajectory
				// MDL_nopar = L(H)
				mdl_nopar += computeLH(trajectory, startIndex + length - 1, startIndex + length);
				
				// compute the sum of (1) the length of a cluster component and
				//                    (2) the perpendicular and angle distances
				// MLD_par = L(H) + L(D|H)
				mdl_par = 	computeLH(trajectory, startIndex, startIndex + length) + 
							computeLDH(trajectory, startIndex, startIndex + length);
				
				// create partition, if its MDL cost is less then MDL of no partition
				if (mdl_nopar == mdl_par) continue;
				if (mdl_nopar < mdl_par + MDL_COST_ADVANTAGE) {
					// System.out.println("\t\tCreating partition for trajectory " + trajectory.id + " at previous position " + (startIndex + length - 1));
					trajectory.partitionPoints.add(trajectory.points.get(startIndex + length - 1));
					startIndex = startIndex + length - 1;
					length = 0;
					break;
				}
			}
			
			// if we reach the end of a trajectory
			if (startIndex + length >= numberOfPoints) {
				break;
			}
		}
		
		// add the end point to partition
		trajectory.partitionPoints.add(trajectory.points.elementAt(trajectory.points.size() - 1));
	}
	
	public double computeLH(Trajectory trajectory, int startPointIndex, int endPointIndex) {
		Point lineSegmentStart = trajectory.points.elementAt(startPointIndex);
		Point lineSegmentEnd = trajectory.points.elementAt(endPointIndex);
		
		double distance = measureDistanceFromPointToPoint(lineSegmentStart, lineSegmentEnd);
		if (distance < 1.0) { // to take logarithm
			distance = 1.0;
		}
		
		return log2(distance);
	}
	
	public double computeLDH(Trajectory trajectory, int startPointIndex, int endPointIndex) {
		Point clusterComponentStart = trajectory.points.elementAt(startPointIndex);
		Point clusterComponentEnd = trajectory.points.elementAt(endPointIndex);
		
		double ldh = 0.0;
		for (int i = startPointIndex; i < endPointIndex; i++) {
			Point lineSegmentStart = trajectory.points.elementAt(i);
			Point lineSegmentEnd = trajectory.points.elementAt(i + 1);
			
			double perpendicularDistance = measurePerpendicularDistance(clusterComponentStart, clusterComponentEnd, lineSegmentStart, lineSegmentEnd);
			double angleDistance = measureAngleDistance(clusterComponentStart, clusterComponentEnd, lineSegmentStart, lineSegmentEnd);
			
			// to take logarithm
			if (perpendicularDistance < 1.0) {
				perpendicularDistance = 1.0;
			}
			
			// to take logarithm
			if (angleDistance < 1.0) {
				angleDistance = 1.0;
			}
			
			ldh += (int) Math.ceil(log2(perpendicularDistance)) + (int) Math.ceil(log2(angleDistance));
		}
		
		return ldh;
	}
	
	private boolean storeClusterComponentIntoIndex() {
		for (Trajectory trajectory: trajectoryList) {
			for (int i = 0; i < trajectory.partitionPoints.size() - 1; i++) {
				// convert an n-dimensional trajectory partition into a 2n-dimensional point
				// i.e., the first n-dimension: the start point
				//       the last n-dimension: the end point
				Point startPoint = trajectory.partitionPoints.get(i);
				Point endPoint = trajectory.partitionPoints.get(i + 1);
				
				if (measureDistanceFromPointToPoint(startPoint, endPoint) < MIN_LINESEGMENT_LENGTH) {
					continue;
				}
				
				Point trajectoryPartitionPoint = new Point(dimension * 2);
				for (int j = 0; j < dimension; j++) {
					trajectoryPartitionPoint.coordinates[j] = startPoint.coordinates[j];
					trajectoryPartitionPoint.coordinates[dimension + j] = endPoint.coordinates[j];
				}
				
				idArray.add(new LineSegmentId(trajectory.id, i));
				trajectoryPartitionsAsPoints.add(trajectoryPartitionPoint);
			}
		}
		
		return true;
	}
	
	/* DENSITY-BASED CLUSTERING */
	
	private boolean performDBSCAN() {
		componentIdArray = new ArrayList<Integer>(trajectoryPartitionsAsPoints.size());
		for (int i = 0; i < trajectoryPartitionsAsPoints.size(); i++) {
			componentIdArray.add(UNCLASSIFIED);
		}
		
		currComponentId = 0;
		for (int i = 0; i < trajectoryPartitionsAsPoints.size(); i++) {
			if (componentIdArray.get(i) == UNCLASSIFIED) {
				if (expandDenseComponent(i, currComponentId, epsilon, minLns)) {
					currComponentId++;
				}
			}
		}
		
		return true;
	}
	
	private boolean expandDenseComponent(int index, int componentId, double epsilon, int minDensity) {
		extractStartAndEndPoints(index, startPoint1, endPoint1);
		Set<Integer> seeds = computeEPSNeighborhood(startPoint1, endPoint1, epsilon);
		if (seeds.size() < minDensity) { // not a core line segment
			componentIdArray.set(index, NOISE);
			return false;
		} else {
			Iterator<Integer> seedsIterator = seeds.iterator();
			while (seedsIterator.hasNext()) {
			  componentIdArray.set(seedsIterator.next(), componentId);
			}
			
			seeds.remove(index);
			
			int currIndex;
			while (!seeds.isEmpty()) {
				Iterator<Integer> iterator = seeds.iterator();
				currIndex = iterator.next();
				
				extractStartAndEndPoints(currIndex, startPoint1, endPoint1);
				Set<Integer> seedResult = computeEPSNeighborhood(startPoint1, endPoint1, epsilon);
				
				if (seedResult.size() >= minDensity) {
					Iterator<Integer> seedResultIterator = seedResult.iterator();
					while (seedResultIterator.hasNext()) {
						int iter = seedResultIterator.next();
						int componentValue = componentIdArray.get(iter);
						if (componentValue == UNCLASSIFIED || componentValue == NOISE) {
							if (componentValue == UNCLASSIFIED) {
								seeds.add(iter);
							}
							componentIdArray.set(iter, componentId);
						}
					}
				}
				
				seeds.remove(currIndex);
			}
			
			return true;
		}
	}
	
	private Set<Integer> computeEPSNeighborhood(Point startPoint, Point endPoint, double epsilon) {
		Set<Integer> result = new TreeSet<Integer>();
		for (int j = 0; j < trajectoryPartitionsAsPoints.size(); j++) {
			extractStartAndEndPoints(j, startPoint2, endPoint2);
			
			double distance = computeDistanceBetweenTwoLineSegments(startPoint, endPoint, startPoint2, endPoint2);
			
			// if the distance is below the threshold, this line segment belongs to the eps-neighborhood
			if (distance <= epsilon) {
				result.add(j);
			}
		}
		
		return result;
	}
	
	public double computeDistanceBetweenTwoLineSegments(Point startPoint1, Point endPoint1, Point startPoint2, Point endPoint2) {
		double perpendicularDistance, parallelDistance, angleDistance;
		
		// length of the first and second trajectory partition
		double length1 = measureDistanceFromPointToPoint(startPoint1, endPoint1);
		double length2 = measureDistanceFromPointToPoint(startPoint2, endPoint2);
		
		// compute the perpendicular distance and the parallel distance
		double perDistance1, perDistance2, parDistance1, parDistance2;
		if (length1 > length2) {
			perDistance1 = measureDistanceFromPointToLineSegment(startPoint1, endPoint1, startPoint2);
			if (coefficient < 0.5) {
				parDistance1 = measureDistanceFromPointToPoint(startPoint1, projectionPoint);
			} else {
				parDistance1 = measureDistanceFromPointToPoint(endPoint1, projectionPoint);
			}
			
			perDistance2 = measureDistanceFromPointToLineSegment(startPoint1, endPoint1, endPoint2);
			if (coefficient < 0.5) {
				parDistance2 = measureDistanceFromPointToPoint(startPoint1, projectionPoint);
			} else {
				parDistance2 = measureDistanceFromPointToPoint(endPoint1, projectionPoint);
			}
		} else {
			perDistance1 = measureDistanceFromPointToLineSegment(startPoint2, endPoint2, startPoint1);
			if (coefficient < 0.5) {
				parDistance1 = measureDistanceFromPointToPoint(startPoint2, projectionPoint);
			} else {
				parDistance1 = measureDistanceFromPointToPoint(endPoint2, projectionPoint);
			}
			
			perDistance2 = measureDistanceFromPointToLineSegment(startPoint2, endPoint2, endPoint1);
			if (coefficient < 0.5) {
				parDistance2 = measureDistanceFromPointToPoint(startPoint2, projectionPoint);
			} else {
				parDistance2 = measureDistanceFromPointToPoint(endPoint2, projectionPoint);
			}
		}
		
		// compute the perpendicular distance; take (d1^2 + d2^2) / (d1 + d2)
		if (!(perDistance1 == 0.0 && perDistance2 == 0.0))  {
			perpendicularDistance = ((pow2(perDistance1) + pow2(perDistance2)) / (perDistance1 + perDistance2));
		} else {
			perpendicularDistance = 0.0;
		}
		
		// compute the parallel distance; take the minimum
		parallelDistance = (parDistance1 < parDistance2) ? parDistance1 : parDistance2;
		
		// compute the angle distance
		// measureAngleDistance() assumes that the first line segment is longer than the second one
		if (length1 > length2) {
			angleDistance = measureAngleDistance(startPoint1, endPoint1, startPoint2, endPoint2);
		} else {
			angleDistance = measureAngleDistance(startPoint2, endPoint2, startPoint1, endPoint1);
		}
		
		// System.out.println(perpendicularDistance + ", " + parallelDistance + ", " + angleDistance);
		return (w_perp * perpendicularDistance + w_par * parallelDistance + w_angle * angleDistance);
	}
	
	private void extractStartAndEndPoints(int index, Point startPoint, Point endPoint) {
		// compose the start and end points of the line segment
		for (int i = 0; i < dimension; i++) {
			startPoint.coordinates[i] = trajectoryPartitionsAsPoints.get(index).coordinates[i];
			endPoint.coordinates[i] = trajectoryPartitionsAsPoints.get(index).coordinates[dimension + i];
		}
	}
	
	/* CLUSTER CONSTRUCTION */
	
	public boolean constructCluster() {
		if (!constructLineSegmentCluster()) {
			return false;
		}
		
		if (!storeLineSegmentCluster()) {
			return false;
		}
		
		return true;
	}
	
	private boolean constructLineSegmentCluster() {
		lineSegmentClusters = new ArrayList<LineSegmentCluster>(currComponentId);
		
		// initialize the list of line segment clusters
		for (int i = 0; i < currComponentId; i++) {
			lineSegmentClusters.add(new LineSegmentCluster(i, dimension));
		}
		
		// accumulate the direction vector of a line segment
		for (int i = 0; i < trajectoryPartitionsAsPoints.size(); i++) {
			int componentId = componentIdArray.get(i);
			// System.out.println("\t\t\t\tEdge " + i + ": " + trajectoryPartitionsAsPoints.get(i));
			if (componentId >= 0) {
				for (int j = 0; j < dimension; j++) {
					double difference = trajectoryPartitionsAsPoints.get(i).coordinates[dimension + j] - trajectoryPartitionsAsPoints.get(i).coordinates[j];
					double currSum = lineSegmentClusters.get(componentId).avgDirectionVector.coordinates[j] + difference;
					lineSegmentClusters.get(componentId).avgDirectionVector.coordinates[j] = currSum;
				}
				lineSegmentClusters.get(componentId).numberOfLineSegments++;
			}
		}
		
		// System.out.println("\t\t\t\tAvg Direction Vector: " + lineSegmentClusters.get(0).avgDirectionVector);
		
		// compute the average direction vector of a line segment cluster
		double vectorLength1, vectorLength2, innerProduct;
		double cosTheta, sinTheta;
		
		Point vector = new Point(2);
		vector.coordinates[0] = 1.0;
		vector.coordinates[1] = 0.0;
		
		for (int i = 0; i < currComponentId; i++) {
			LineSegmentCluster clusterEntry = lineSegmentClusters.get(i);
			
			for (int j = 0; j < dimension; j++) {
				clusterEntry.avgDirectionVector.coordinates[j] = clusterEntry.avgDirectionVector.coordinates[j] / clusterEntry.numberOfLineSegments;
			}
			
			vectorLength1 = computeVectorLength(clusterEntry.avgDirectionVector);
			vectorLength2 = 1.0;
			
			innerProduct = computeInnerProduct(clusterEntry.avgDirectionVector, vector);
			cosTheta = innerProduct / (vectorLength1 * vectorLength2);
			if (cosTheta > 1.0) {
				cosTheta = 1.0; 
			}
			if (cosTheta < -1.0) {
				cosTheta = -1.0;
			}
			sinTheta = Math.sqrt(1 - pow2(cosTheta));
			
			if (clusterEntry.avgDirectionVector.coordinates[1] < 0) {
				sinTheta = -sinTheta;
			}
			
			clusterEntry.cosTheta = cosTheta;
			clusterEntry.sinTheta = sinTheta;
			// System.out.println("\t\t\t\tsinTheta = " + sinTheta + ", cosTheta = " + cosTheta);
		}
		
		// summarize the information about line segment clusters
		// the structure for summarization is as follows
		// [lineSegmentClusterId, nClusterPoints, clusterPointArray, nTrajectories, { trajectoryId, ... }]
		for (int i = 0; i < trajectoryPartitionsAsPoints.size(); i++) {
			if (componentIdArray.get(i) >= 0) { // if the componentId < 0, it is NOISE
				registerAndUpdateLineSegmentCluster(componentIdArray.get(i), i);
			}
		}
		
		Set<Integer> trajectories = new TreeSet<Integer>();
		for (int i = 0; i < currComponentId; i++) {
			LineSegmentCluster clusterEntry = lineSegmentClusters.get(i);

			// line segment cluster must have trajectories more than the minimum threshold
			if (clusterEntry.trajectoryIds.size() >= minLns) {
				clusterEntry.enabled = true;
				
				// DEBUG: count the number of trajectories that belong to clusters
				for (int j = 0; j < (int) clusterEntry.trajectoryIds.size(); j++) {
					trajectories.add(clusterEntry.trajectoryIds.get(j));
				}
				
				computeRepresentativeLines(clusterEntry);
			} else {
				clusterEntry.candidatePoints.clear();
				clusterEntry.clusterPoints.clear();
				clusterEntry.trajectoryIds.clear();
			}
		}
		
		// DEBUG: compute the ratio of trajectories that belong to clusters
		clusterRatio = ((double) trajectories.size()) / trajectoryList.size();
		
		return true;
	}
	
	private void computeRepresentativeLines(LineSegmentCluster clusterEntry) {
		Set<Integer> lineSegments = new HashSet<Integer>();
		double prevOrderingValue = 0.0;
		int nClusterPoints = 0;
		lineSegments.clear();
		
		// sweep the line segments in a line segment cluster
		Set<Integer> insertionList = new HashSet<Integer>();
		Set<Integer> deletionList = new HashSet<Integer>();
		Iterator<CandidateClusterPoint> iter = clusterEntry.candidatePoints.iterator();
		Iterator<CandidateClusterPoint> iterTmp = clusterEntry.candidatePoints.iterator();
		iterTmp.next();
		
		// System.out.println("\t\t\t\tCandidate Points: " + clusterEntry.candidatePoints.size());
		while (iter.hasNext()) {
			insertionList.clear();
			deletionList.clear();
			
			CandidateClusterPoint candidatePoint, nextCandidatePoint = null;
			do {
				candidatePoint = iter.next();
				// System.out.println("\t\t\t\tCurrent " + candidatePoint + ", Next " + nextCandidatePoint);
				
				// check whether this line segment has begun or not
				if (!lineSegments.contains(candidatePoint.id)) {	// if there is no matched element,
					insertionList.add(candidatePoint.id);			// this line segment begins at this point
					lineSegments.add(candidatePoint.id);
					// System.out.println("\t\t\t\t\tNo matching element found in line segments.");
				} else {											// if there is a matched element,
					deletionList.add(candidatePoint.id);			// this line segment ends at this point
					// System.out.println("\t\t\t\t\tMatching element found in line segments.");
				}
				
				// check whether the next line segment begins or ends at the same point
				if (iter.hasNext()) {
					nextCandidatePoint = iterTmp.next();
				} else {
					break;
				}
			} while (candidatePoint.orderingValue == nextCandidatePoint.orderingValue);
			
			// check if a line segment is connected to another line segment in the same trajectory
			// if so, delete one of the line segments to remove duplicates
			for (Integer i: insertionList) {
				for (Integer d: deletionList) {
					if (i == d) {
						lineSegments.remove(d);
						deletionList.remove(d);
						break;
					}
				}
				for (Integer d: deletionList) {
					if (idArray.get(i).trajectoryId == idArray.get(d).trajectoryId) {
						lineSegments.remove(d);
						deletionList.remove(d);
						break;
					}
				}
			}
			
			// if the current density exceeds a given threshold
			if (lineSegments.size() >= minLns) {
				if (Math.abs(candidatePoint.orderingValue - prevOrderingValue) > (MIN_LINESEGMENT_LENGTH / 1.414)) {
					computeAndRegisterClusterPoint(clusterEntry, candidatePoint.orderingValue, lineSegments);
					prevOrderingValue = candidatePoint.orderingValue;
					nClusterPoints++;
				} else {
					// System.out.println("\t\t\t\t\tCandidate point is too near. Not saving!");
				}
			} else {
				// System.out.println("\t\t\t\t\tToo few line segments. Not saving!");
			}
			
			// delete the line segment that is not connected to another line segment
			for (Integer d: deletionList) {
				lineSegments.remove(d);
			}
		}
		
		if (nClusterPoints < 2) {
			// there is no representative trend in this line segment cluster
			clusterEntry.enabled = false;
			clusterEntry.candidatePoints.clear();
			clusterEntry.clusterPoints.clear();
			clusterEntry.trajectoryIds.clear();
		}
	}
	
	private void computeAndRegisterClusterPoint(LineSegmentCluster clusterEntry, double currValue, Set<Integer> lineSegments) {
		// System.out.println("\t\t\t\t\tSweep for component " + clusterEntry.id + " with " + lineSegments.size() + " edges.");
		Point avgPoint = new Point(dimension);
		
		// hot fix: filter out duplicate sweep points
		List<Point> sweepPoints = new ArrayList<Point>();
		for (Integer j: lineSegments) {
			Point sweepPoint = getSweepPointOfLineSegment(clusterEntry, currValue, j);
			
			// is it contained?
			boolean contained = false;
			for (Point p: sweepPoints) {
				if (p.coordinates[0] == sweepPoint.coordinates[0] &&
					p.coordinates[1] == sweepPoint.coordinates[1]) {
					contained = true;
				}
			}
			
			if (!contained) {
				sweepPoints.add(sweepPoint);
			}
		}
		
		if (sweepPoints.size() > 1) {
			sweepCrossesMultipleEdges = true;
		}
		
		// compute the average of all the sweep points
		for (Point sweepPoint: sweepPoints) {
			for (int i = 0; i < dimension; i++) {
				avgPoint.coordinates[i] = avgPoint.coordinates[i] + (sweepPoint.coordinates[i] / sweepPoints.size());
			}
		}
		
		/*
		for (Integer j: lineSegments) {
			// get the sweep point of each line segment
			// this point is parallel to the current value of the sweeping direction
			Point sweepPoint = getSweepPointOfLineSegment(clusterEntry, currValue, j);
			System.out.println("\t\t\t\t\tSweep " + sweepPoint);
			
			// compute the average of all the sweep points
			for (int i = 0; i < dimension; i++) {
				avgPoint.coordinates[i] = avgPoint.coordinates[i] + (sweepPoint.coordinates[i] / lineSegments.size());
			}
		}*/
		
		// this program code works only for 2-dimensional data
		double origX, origY;
		origX = getXRevRotation(avgPoint.coordinates[0], avgPoint.coordinates[1], clusterEntry.cosTheta, clusterEntry.sinTheta);
		origY = getYRevRotation(avgPoint.coordinates[0], avgPoint.coordinates[1], clusterEntry.cosTheta, clusterEntry.sinTheta);
		avgPoint.coordinates[0] = origX;
		avgPoint.coordinates[1] = origY;
		
		// System.out.println("\t\t\t\t\tSaving " + avgPoint + " in representative line.");
		
		// register the obtained cluster point (i.e., the average of all the sweep points)
		clusterEntry.clusterPoints.add(avgPoint);
	}
	
	private Point getSweepPointOfLineSegment(LineSegmentCluster clusterEntry, double currX, Integer lineSegmentId) {
		// get line segment as point with 4 dimensions
		Point lineSegmentPoint = trajectoryPartitionsAsPoints.get(lineSegmentId); // 2n-dimensional point
		// System.out.println("\t\t\t\t\tLine Segment: " + lineSegmentPoint);
		
		// this program code only works for 2-dimensional data
		double newStartX = getXRotation(lineSegmentPoint.coordinates[0], lineSegmentPoint.coordinates[1], clusterEntry.cosTheta, clusterEntry.sinTheta);
		double newEndX   = getXRotation(lineSegmentPoint.coordinates[2], lineSegmentPoint.coordinates[3], clusterEntry.cosTheta, clusterEntry.sinTheta);
		double newStartY = getYRotation(lineSegmentPoint.coordinates[0], lineSegmentPoint.coordinates[1], clusterEntry.cosTheta, clusterEntry.sinTheta);
		double newEndY   = getYRotation(lineSegmentPoint.coordinates[2], lineSegmentPoint.coordinates[3], clusterEntry.cosTheta, clusterEntry.sinTheta);
		double coefficient = (currX - newStartX) / (newEndX - newStartX);
		
		// build sweep point
		Point sweepPoint = new Point(dimension);
		sweepPoint.coordinates[0] = currX;
		sweepPoint.coordinates[1] = newStartY + coefficient * (newEndY - newStartY);
		return sweepPoint;
	}
	
	private void registerAndUpdateLineSegmentCluster(Integer componentId, int lineSegmentId) {
		LineSegmentCluster clusterEntry = lineSegmentClusters.get(componentId);
		
		// the start and end values of the first dimension (e.g., the x value in the 2-dimension)
		// NOTE: this program code works only for the 2-dimensional data
		Point aLineSegment = trajectoryPartitionsAsPoints.get(lineSegmentId);
		double orderingValue1 = getXRotation(aLineSegment.coordinates[0], aLineSegment.coordinates[1], clusterEntry.cosTheta, clusterEntry.sinTheta);
		double orderingValue2 = getXRotation(aLineSegment.coordinates[2], aLineSegment.coordinates[3], clusterEntry.cosTheta, clusterEntry.sinTheta);
		
		// sort the line segment points by the coordinate of the first dimension
		// simply use the insertion sort algorithm
		int i, j;
		CandidateClusterPoint existingCandidatePoint;
		for (i = 0; i < clusterEntry.candidatePoints.size(); i++) {
			existingCandidatePoint = clusterEntry.candidatePoints.get(i);
			if (!(existingCandidatePoint.orderingValue < orderingValue1)) {
				break;
			}
		}
		
		CandidateClusterPoint newCandidatePoint = new CandidateClusterPoint(lineSegmentId, orderingValue1, true);
		if (i == 0) {
			clusterEntry.candidatePoints.add(0, newCandidatePoint);
		} else if (i >= clusterEntry.candidatePoints.size()) {
			clusterEntry.candidatePoints.add(newCandidatePoint);
		} else {
			clusterEntry.candidatePoints.add(i, newCandidatePoint);
		}
		
		for (j = 0; j < clusterEntry.candidatePoints.size(); j++) {
			existingCandidatePoint = clusterEntry.candidatePoints.get(j);
			if (!(existingCandidatePoint.orderingValue < orderingValue2)) {
				break;
			}
		}
		
		newCandidatePoint = new CandidateClusterPoint(lineSegmentId, orderingValue2, false);
		if (j == 0) {
			clusterEntry.candidatePoints.add(0, newCandidatePoint);
		} else if (j >= clusterEntry.candidatePoints.size()) {
			clusterEntry.candidatePoints.add(newCandidatePoint);
		} else {
			clusterEntry.candidatePoints.add(j, newCandidatePoint);
		}
		
		// store the identifier of the trajectories that belong to this line segment cluster
		int trajectoryId = idArray.get(lineSegmentId).trajectoryId;
		if (clusterEntry.trajectoryIds.indexOf(trajectoryId) == -1) {
			clusterEntry.trajectoryIds.add(trajectoryId);
		}
	}
	
	private boolean storeLineSegmentCluster() {
		int currClusterId = 0;
		clusterList = new ArrayList<Cluster>();
		
		for (int i = 0; i < currComponentId; i++) {
			if (lineSegmentClusters.get(i).enabled) {
				// store the clusters finally identified
				Cluster cluster = new Cluster(currClusterId);
				clusterList.add(cluster);
				
				for (int j = 0; j < lineSegmentClusters.get(i).clusterPoints.size(); j++) {
					cluster.points.add(lineSegmentClusters.get(i).clusterPoints.get(j));
				}
				
				cluster.numberOfTrajectories = lineSegmentClusters.get(i).trajectoryIds.size();
				currClusterId++; // increase the number of final clusters
			}
		}
		
		return true;
	}
	
	/* DISTANCE METHODS */
	
	public double measurePerpendicularDistance(Point s1, Point e1, Point s2, Point e2) {
		// we assume that the first line segment is longer than the second one
		double distance1; // the distance from a start point to the cluster component
		double distance2; // the distance from an end point to the cluster component
		
		distance1 = measureDistanceFromPointToLineSegment(s1, e1, s2);
		distance2 = measureDistanceFromPointToLineSegment(s1, e1, e2);
		
		// if the first line segment is exactly the same as the second one, the perpendicular distance should be zero
		if (distance1 == 0.0 && distance2 == 0.0) {
			return 0.0;
		}
		
		// return (d1^2 + d2^2) / (d1 + d2) as the perpendicular distance
		return ((pow2(distance1) + pow2(distance2)) / (distance1 + distance2));
	}
	
	public double measureAngleDistance(Point s1, Point e1, Point s2, Point e2) {
		// construct two vectors representing the cluster component and a line segment, respectively
		Point vector1 = new Point(2);
		Point vector2 = new Point(2);
		for (int i = 0; i < dimension; i++) {
			vector1.coordinates[i] = e1.coordinates[i] - s1.coordinates[i];
			vector2.coordinates[i] = e2.coordinates[i] - s2.coordinates[i];
		}
		
		// we assume that the first line segment is longer than the second one
		// i.e., vectorLength1 >= vectorLength2
		double vectorLength1 = computeVectorLength(vector1);
		double vectorLength2 = computeVectorLength(vector2);
		
		// if one of two vectors is a point, the angle distance becomes zero
		if (vectorLength1 == 0.0 || vectorLength2 == 0.0) return 0.0;
		
		// compute the inner product of the two vectors
		double innerProduct = computeInnerProduct(vector1, vector2);
		
		// compute the angle between two vectors by using the inner product
		double cosTheta = innerProduct / (vectorLength1 * vectorLength2);
		
		// compensate the computation error (e.g., 1.00001)
		// cos(theta) should be in the range [-1.0, 1.0]
		if (cosTheta > 1.0) cosTheta = 1.0;
		if (cosTheta < -1.0) cosTheta = -1.0;
		if (0 <= cosTheta && cosTheta <= 90) {
			double sinTheta = Math.sqrt(1 - pow2(cosTheta));
			return (vectorLength2 * sinTheta);
		} else if (90 <= cosTheta && cosTheta <= 180) {
			return vectorLength2;
		} else {
			return Double.MAX_VALUE;
		}
	}
	
	public double measureDistanceFromPointToPoint(Point point1, Point point2) {
		double squareSum = 0.0;
		
		for (int i = 0; i < dimension; i++) {
			squareSum += pow2(point2.coordinates[i] - point1.coordinates[i]);
		}
		
		return Math.sqrt(squareSum);
	}
	
	public double measureDistanceFromPointToLineSegment(Point s, Point e, Point p) {
		Point vector1 = new Point(2);
		Point vector2 = new Point(2);
		
		for (int i = 0; i < dimension; i++) {
			// construct two vectors as follows
			// 1. the vector connecting the start point of the cluster component and a given point
			// 2. the vector representing the cluster component
			
			vector1.coordinates[i] = p.coordinates[i] - s.coordinates[i];
			vector2.coordinates[i] = e.coordinates[i] - s.coordinates[i];
		}
		
		// coefficient (0 <= b <= 1)
		coefficient = computeInnerProduct(vector1, vector2) / computeInnerProduct(vector2, vector2);
		
		// projection on the cluster component from a given point
		projectionPoint = new Point(2);
		for (int i = 0; i < dimension; i++) {
			projectionPoint.coordinates[i] = s.coordinates[i] + coefficient * vector2.coordinates[i];
		}
		
		// return the distance between the projection point and the given point
		return measureDistanceFromPointToPoint(p, projectionPoint);
	}
	
	/* HELPER METHODS */
	
	private double computeVectorLength (Point vector) {
		double squareSum = 0.0;
		
		for (int i = 0; i < dimension; i++) {
			squareSum += pow2(vector.coordinates[i]);
		}
		
		return Math.sqrt(squareSum);
	}
	
	private double computeInnerProduct(Point vector1, Point vector2) {
		double innerProduct = 0.0;
		
		for (int i = 0; i < dimension; i++) {
			innerProduct += (vector1.coordinates[i] * vector2.coordinates[i]);
		}
		
		return innerProduct;
	}
	
	public double getXRotation(double _x, double _y, double _cos, double _sin) {
		return ((_x)*(_cos) + (_y)*(_sin));
	}
	
	private double getYRotation(double _x, double _y, double _cos, double _sin) {
		return (-(_x)*(_sin) + (_y)*(_cos));
	}
	
	private double getXRevRotation(double _x, double _y, double _cos, double _sin) {
		return ((_x)*(_cos) - (_y)*(_sin));
	}
	
	private double getYRevRotation(double _x, double _y, double _cos, double _sin) {
		return ((_x)*(_sin) + (_y)*(_cos));
	}
	
	private double log2(double a) {
		return Math.log(a)/Math.log(2);
	}
	
	private double pow2(final double a) {
		// faster then Math.pow see
		// - http://dhruba.name/2012/09/01/performance-pattern-multiplication-is-20x-faster-than-math-pow/
		// - http://stackoverflow.com/questions/6321170/is-there-any-advantage-to-using-powx-2-instead-of-xx-with-x-double
		return a*a;
	}
}