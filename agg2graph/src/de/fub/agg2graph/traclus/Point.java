package de.fub.agg2graph.traclus;

public class Point {
	public double[] coordinates; // point coordinates
	
	public Point(int dimensions) {
		this.coordinates = new double[dimensions];
	}

	@Override
	public String toString() {
		String resultString = "Point [";
		for (double coordinate: coordinates) {
			resultString += coordinate + ", ";
		}
		return resultString + "]";
	}
}