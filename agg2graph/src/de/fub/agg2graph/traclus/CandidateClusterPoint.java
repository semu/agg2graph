package de.fub.agg2graph.traclus;

public class CandidateClusterPoint {
	public final int id;
	public final double orderingValue;
	public final boolean isStartPoint;

	CandidateClusterPoint(int id, double orderingValue, boolean isStartPoint) {
		this.id = id;
		this.orderingValue = orderingValue;
		this.isStartPoint = isStartPoint;
	}

	@Override
	public String toString() {
		return "CCPoint [" + id + ", " + orderingValue + ", " + isStartPoint + "]";
	}
}