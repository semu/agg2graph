/*
 * Copyright (C) 2014 Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.input;

import java.io.File;
import java.io.IOException;
import java.util.List;

import de.fub.agg2graph.structs.GPSSegment;

public class ChangeDirector {
	
	public static List<GPSSegment> read(File file) {
		List<GPSSegment> segment = GPXReader.getSegments(file);
		
		return segment;
	}
	
	public static void write(File file, List<GPSSegment> segment) throws IOException {
		GPSSegment reverse = new GPSSegment();
		for(int i = segment.get(0).size()-1; i >= 0; i--) {
			reverse.add(segment.get(0).get(i));
		}
		
		GPXWriter.writeSegment(file, reverse);
	}
	
	public static void main(String[] args) throws IOException {
		String name = "konstanzerstrasse3.gpx";
		String directory = "Scen2 - Konstanzstrasse_Enge_Kreuzungen/";
		File f = new File("test/input/"+directory+name);
		
		List<GPSSegment> segment = read(f);
		write(f, segment);
		
	}
}
