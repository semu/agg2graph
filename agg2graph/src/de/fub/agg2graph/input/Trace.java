/*
 * Copyright (C) 2014 Sebastian Müller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.input;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.ListIterator;

import de.fub.agg2graph.agg.AggConnection;
import de.fub.agg2graph.agg.AggContainer;
import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSRegion;
import de.fub.agg2graph.structs.ILocation;
import de.fub.agg2graph.structs.frechet.ITrace;

public class Trace implements ITrace {

	AggContainer aggContainer;
	protected ArrayList<ILocation> locations = new ArrayList<ILocation>();
	protected ArrayList<AggConnection> conns = new ArrayList<AggConnection>();
	protected ArrayList<GPSEdge> edges = new ArrayList<GPSEdge>();
	protected String name;

	long id;
	static long idGenerator = 0;
	public boolean connsNeedsUpdate = true;
	public boolean edgesNeedsUpdate = true;

	public Trace() {
		this.id = ++idGenerator;
	}

	@Override
	public Iterator<ILocation> iterator() {
		return locations.iterator();
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public GPSRegion getGPSRegion() {
		// TODO Auto-generated method stub
		return null;
	}

	protected void updateEdges() {
		if (!edgesNeedsUpdate)
			return;
		edges.clear();

		ILocation from = null;
		for (ILocation l : locations) {
			if (from == null) {
				from = l;
				continue;
			}

			ILocation to = l;
			GPSEdge edge = new GPSEdge();
			edge.setFrom((GPSPoint) from);
			edge.setTo((GPSPoint) to);
			edges.add(edge);
			from = to;
		}
		edgesNeedsUpdate = false;
	}
	
	@Override
	public ListIterator<GPSEdge> edgeListIterator(ILocation start) {
		if (edgesNeedsUpdate)
			updateEdges();

		ListIterator<GPSEdge> it = edgeListIterator();
		while (it.hasNext()) {
			if (it.next().getFrom().compareTo((GPSPoint) start) == 0) {
				it.previous();
				return it;
			}
		}
		return Collections.emptyListIterator();
	}

	@Override
	public ListIterator<GPSEdge> edgeListIterator() {
		if (edgesNeedsUpdate)
			updateEdges();

		return edges().listIterator();
	}

	@Override
	public ArrayList<GPSEdge> edges() {
		return edges;
	}

	@Override
	public ArrayList<ILocation> edgeLocations() {
		return locations;
	}

	@Override
	public ITrace edgeSubTrace(ILocation start, ILocation stop) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean edgeIsEmpty() {
		return locations == null || locations.size() < 2;
	}

	@Override
	public ILocation getEdgeFirstLocation() {
		return locations.get(0);
	}

	@Override
	public ILocation getEdgeLastLocation() {
		return locations.get(locations.size() - 1);
	}

	@Override
	public void insertEdgeLocation(int index, ILocation location) {
		locations.add(index, location);
		edgesNeedsUpdate = true;
	}
}
