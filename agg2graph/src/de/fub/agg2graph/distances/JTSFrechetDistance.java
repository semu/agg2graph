/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import geom.FrechetDistance;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.ListIterator;

import de.fub.agg2graph.structs.GPSPoint;

public class JTSFrechetDistance implements TrajectoryDistance{

	public double calculate (List<GPSPoint> l1, List<GPSPoint> l2, PointDistance pd){
        Point2D[] p = new Point2D[l1.size()];
        for(ListIterator<GPSPoint> it = l1.listIterator(); it.hasNext(); ){
            int index = it.nextIndex();
            GPSPoint point = it.next();
            p[index] = new Point2D.Double(point.getX(), point.getY());
        }
        
        Point2D[] q = new Point2D[l2.size()];
        for(ListIterator<GPSPoint> it = l2.listIterator(); it.hasNext();){
            int index = it.nextIndex();
            GPSPoint point = it.next();
            q[index] = new Point2D.Double(point.getX(), point.getY());
        }
        
        FrechetDistance fd = new FrechetDistance(p, q);
//        System.out.println(p.length + ":" + q.length);
        return fd.computeFrechetDistance();
	}
}
