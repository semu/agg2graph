/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import java.util.Iterator;
import java.util.List;

import de.fub.agg2graph.structs.GPSEdge;

public class AngleHausdorffDistance implements TrajectoryEdgeDistance{

	public double calculate (List<GPSEdge> l1, List<GPSEdge> l2, EdgeDistance ed){
		Iterator<GPSEdge> it1 = l1.iterator();
		Iterator<GPSEdge> it2 = l2.iterator();
		double sup1 = 0.0;
		while(it1.hasNext()){
			GPSEdge p1 = it1.next();
			double inf = Double.MAX_VALUE;
			while(it2.hasNext()){
				GPSEdge p2 = it2.next();
				double dist = ed.calculate(p1,p2);
				if (dist < inf){
					inf = dist;
				}
			}
			if (inf > sup1){
				sup1 = inf;
			}
		}
		return sup1;
	}
	
}
