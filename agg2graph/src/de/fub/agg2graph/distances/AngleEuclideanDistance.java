/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;

public class AngleEuclideanDistance implements EdgeDistance{

	static double OMEGA = 0.05;
	
	public double calculate (double lat1, double lon1, double lat2, double lon2, double lat3, double lon3, double lat4, double lon4){
        GPSPoint p1 = new GPSPoint(lat1, lon1);
        GPSPoint p2 = new GPSPoint(lat2, lon2);
        GPSPoint p3 = new GPSPoint(lat3, lon3);
        GPSPoint p4 = new GPSPoint(lat4, lon4);
		return calculate(p1, p2, p3, p4);
	}
	
	public double calculate (GPSPoint p1, GPSPoint p2, GPSPoint p3, GPSPoint p4){
		return calculate(new GPSEdge(p1, p2), new GPSEdge(p3,p4));
	}
	
	public double calculate (GPSEdge e1, GPSEdge e2){
		PointDistance pd = new EuclideanDistance();
		double w = OMEGA*(pd.calculate(e1.getFrom(),e1.getTo())+pd.calculate(e2.getFrom(),e2.getTo()))%2;
		
		return Math.sqrt(Math.pow((e1.getFrom().getX() - e2.getFrom().getX()), 2)
				+ Math.pow((e1.getFrom().getY() - e2.getFrom().getY()), 2)
				+ w * Math.pow((e1.getAngle() - e2.getAngle()), 2));
		
	}
	
}
