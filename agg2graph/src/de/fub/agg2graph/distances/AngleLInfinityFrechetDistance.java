/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import java.util.List;
import java.util.ListIterator;

import de.fub.agg2graph.structs.GPSEdge;
import frechetdistance.algo.FrechetDistance;
import frechetdistance.algo.implementations.polyhedral.PolyhedralFrechetDistance;
import frechetdistance.algo.util.PolyhedralDistanceFunction;

public class AngleLInfinityFrechetDistance implements TrajectoryEdgeDistance{

	public double calculate (List<GPSEdge> l1, List<GPSEdge> l2, EdgeDistance ed){
	    FrechetDistance frechet;
	    double[][] curveA, curveB;
	    double dist;
	    // two curves 
	    curveA = new double[l1.size()][];
	    curveB = new double[l2.size()][];
	    
        for(ListIterator<GPSEdge> it = l1.listIterator(); it.hasNext(); ){
            int index = it.nextIndex();
            GPSEdge point = it.next();
            curveA[index] = new double[]{point.getFrom().getX(), point.getFrom().getY(), point.getAngle()};
        }
        
        for(ListIterator<GPSEdge> it = l2.listIterator(); it.hasNext();){
            int index = it.nextIndex();
            GPSEdge point = it.next();
            curveB[index] = new double[]{point.getFrom().getX(), point.getFrom().getY(), point.getAngle()};
        }
        
        // L-infinity in 4 dimensions edited 2
        frechet = new PolyhedralFrechetDistance(PolyhedralDistanceFunction.LInfinity(3));
        dist = frechet.computeDistance(curveA,curveB);
        return dist;
	}
}
