/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import java.util.List;
import java.util.ListIterator;

import de.fub.agg2graph.structs.GPSPoint;
import frechetdistance.algo.FrechetDistance;
import frechetdistance.algo.implementations.polyhedral.PolyhedralFrechetDistance;
import frechetdistance.algo.util.PolyhedralDistanceFunction;

public class LInfinityFrechetDistance implements TrajectoryDistance{

	public double calculate (List<GPSPoint> l1, List<GPSPoint> l2, PointDistance pd){
	    FrechetDistance frechet;
	    double[][] curveA, curveB;
	    double dist;
	    // two curves in 2D
	    curveA = new double[l1.size()][];
	    curveB = new double[l2.size()][];
	    
        for(ListIterator<GPSPoint> it = l1.listIterator(); it.hasNext(); ){
            int index = it.nextIndex();
            GPSPoint point = it.next();
            curveA[index] = new double[]{point.getX(), point.getY()};
        }
        
        for(ListIterator<GPSPoint> it = l2.listIterator(); it.hasNext();){
            int index = it.nextIndex();
            GPSPoint point = it.next();
            curveB[index] = new double[]{point.getX(), point.getY()};
        }
        
        // L-infinity in 4 dimensions edited 2
        frechet = new PolyhedralFrechetDistance(PolyhedralDistanceFunction.LInfinity(2));
        dist = frechet.computeDistance(curveA,curveB);
        return dist;
	}
}
