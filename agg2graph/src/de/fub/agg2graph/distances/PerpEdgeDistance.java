/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import java.util.Iterator;

import de.fub.agg2graph.structs.GPSEdge;
import de.fub.agg2graph.structs.GPSPoint;

public class PerpEdgeDistance implements EdgeDistance{

	public double calculate (double lat1, double lon1, double lat2, double lon2, double lat3, double lon3, double lat4, double lon4){
        GPSPoint p1 = new GPSPoint(lat1, lon1);
        GPSPoint p2 = new GPSPoint(lat2, lon2);
        GPSPoint p3 = new GPSPoint(lat3, lon3);
        GPSPoint p4 = new GPSPoint(lat4, lon4);
		return calculate(p1, p2, p3, p4);
	}
	
	public double calculate (GPSPoint p1, GPSPoint p2, GPSPoint p3, GPSPoint p4){
		return calculate(new GPSEdge(p1, p2), new GPSEdge(p3,p4));
	}
	
	public double calculate (GPSEdge e1, GPSEdge e2){
		Iterator<Double> edIt = e1.retrieveWeightedPerpDistances(e2).iterator();
		Double high = 0.0;
		while(edIt.hasNext()){
			Double cur = edIt.next();
			if (cur > high){
				high = cur;
			}
		}
		return high;
	}
	
}
