/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import android.location.Location;

import de.fub.agg2graph.structs.GPSPoint;

public class AndroidGeodesicDistance implements PointDistance{

	public double calculate (double lat1, double lon1, double lat2, double lon2){
		Location loc1 = new Location("DistanceCalc");
		loc1.setLatitude(lat1);
		loc1.setLongitude(lon1);
		Location loc2 = new Location("DistanceCalc");
		loc2.setLatitude(lat2);
		loc2.setLongitude(lon2);
		return loc1.distanceTo(loc2);
	}
	public double calculate (GPSPoint p1, GPSPoint p2){
		return calculate(p1.getX(), p1.getY(), p2.getX(), p2.getY());
	}
	
}
