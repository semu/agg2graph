/*
 * Copyright 2014 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.distances;

import de.fub.agg2graph.structs.GPSPoint;

public class HaversineDistance implements PointDistance{

	public double calculate (double lat1, double lon1, double lat2, double lon2){
	      double dLat = Math.toRadians(lat2-lat1);  
	      double dLon = Math.toRadians(lon2-lon1);  
	      double a = Math.sin(dLat/2) * Math.sin(dLat/2) +  
	         Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *  
	         Math.sin(dLon/2) * Math.sin(dLon/2);  
	      double c = 2 * Math.asin(Math.sqrt(a));  
	      return 6372797.560856 * c;  
	}
	public double calculate (GPSPoint p1, GPSPoint p2){
		return calculate(p1.getX(), p1.getY(), p2.getX(), p2.getY());
	}
}
