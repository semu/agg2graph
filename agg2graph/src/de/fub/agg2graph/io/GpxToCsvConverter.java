/*
 * Copyright 2013 Sebastian Müller sebastian.mueller@fu-berlin.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fub.agg2graph.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;

import au.com.bytecode.opencsv.CSVWriter;
import de.fub.agg2graph.gpseval.data.Waypoint;
import de.fub.agg2graph.schemas.gpx.GpxType;
import de.fub.agg2graph.schemas.gpx.TrkType;
import de.fub.agg2graph.schemas.gpx.TrksegType;
import de.fub.agg2graph.schemas.gpx.WptType;
import de.fub.agg2graph.structs.GPSCalc;

public final class GpxToCsvConverter {

	public static void convertGpxToCsv(File f, File out) throws IOException,
			JAXBException {
		CSVWriter csvOut = new CSVWriter(new FileWriter(out));
		
		try{
		GpxType gpx = (new FileImport()).parseFile(f);
		String[] firstLine = { "Name", "Art der Aktivität", "Beschreibung" };
		csvOut.writeNext(firstLine);
		String[] secondLine = { "", "", "" }; // u.a. Activity Type
		csvOut.writeNext(secondLine);
		csvOut.writeNext(new String[0]);
		String[] fourthLine = { "Segment", "Punkt", "Breitengrad (°)",
				"Längengrad (°)", "Höhe (m)", "Peilung (°)", "Genauigkeit (m)",
				"Geschwindigkeit (m/s)", "Zeit", "Leistung (W)",
				"Trittfrequenz (Umdrehungen pro Minute)",
				"Herzfrequenz (Schläge pro Minute)", "Akkustatus" };
		csvOut.writeNext(fourthLine);
		Iterator<TrkType> trkIt = gpx.getTrk().iterator();
		// store from previous track
		int i = 0;
		String prevLat = "", prevLon = "";
		Date prevTime = null;
		while (trkIt.hasNext()) {
			TrkType trk = trkIt.next();
			Iterator<TrksegType> segIt = trk.getTrkseg().iterator();
			while (segIt.hasNext()) {
				TrksegType seg = segIt.next();
				Iterator<WptType> wptIt = seg.getTrkpt().iterator();
				while (wptIt.hasNext()) {
					i++;
					WptType wpt = wptIt.next();
					String[] data = new String[9];
					data[0] = ""; // Segment
					data[1] = ""; // Point
					data[2] = String.valueOf(wpt.getLat());
					data[3] = String.valueOf(wpt.getLon());
					data[4] = String.valueOf(wpt.getEle());
					data[5] = ""; // Bearing
					data[6] = ""; // Precision
					SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss.SSSX");
					data[8] = sdf
							.format(wpt.getTime().toGregorianCalendar()
									.getTime()).replaceFirst(" ", "T")
							.replaceFirst("\\+01", "Z");
					// Speed
					if(i == 1) {
						data[7] = "";
					}
					else {
						double startLat = Double.parseDouble(prevLat);
						double startLon = Double.parseDouble(prevLon);
						double endLat = Double.parseDouble(data[2]);
						double endLon = Double.parseDouble(data[3]);
						long timeDiff = (wpt.getTime().toGregorianCalendar().getTime().getTime() - prevTime.getTime()) / 1000; // in seconds
						double result = GPSCalc.getDistVincentyFast(startLat, startLon, endLat, endLon);
						if (timeDiff == 0) data[7] = "";
						else data[7] = "" + (result / timeDiff);
					}
					prevLat = data[2];
					prevLon = data[3];
					prevTime = wpt.getTime().toGregorianCalendar().getTime();
					
					Waypoint wp = new Waypoint(data);
					csvOut.writeNext(wp.toData());
				}
			}
			i = 0;
		}
		
		}
		catch (UnmarshalException e){
			System.out.println(f.getPath());
//			byte[] buffer = new byte[ (int) f.length() ];
//			InputStream in = new FileInputStream( f );
//			in.read( buffer );
//			in.close();
			throw e;
		}

		
		csvOut.flush();
		csvOut.close();
	}
	
	public static void convertGpxFolderToCsv(String gpxDir, String csvDir) throws IOException, JAXBException {

		File tracks = new File(gpxDir);
		File[] track = tracks.listFiles();
		String files = null;

		for (int i = 0; i < track.length; i++) {

			if (track[i].isFile()) {
				files = track[i].getName();
				if (files.toLowerCase().endsWith(".gpx")) {
					File gpxFile = track[i];
					String prename = files.substring(0, files.toLowerCase()
							.indexOf(".gpx"));
					File csvFile = new File(csvDir + File.separator + prename + ".csv");
					convertGpxToCsv(gpxFile, csvFile);
				}
			}
		}

	}
	
}
