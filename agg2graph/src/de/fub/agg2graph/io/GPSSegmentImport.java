/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import de.fub.agg2graph.schemas.gpx.*;
import de.fub.agg2graph.structs.GPSPoint;
import de.fub.agg2graph.structs.GPSSegment;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class GPSSegmentImport implements Converter<List<GPSSegment>> {

    @Override
    public List<GPSSegment> convert(GpxType input) {
        List<GPSSegment> segmentList = new LinkedList<>();
        if(input.getRte() != null){
            for(RteType route : input.getRte()){
                if(route != null){
                    segmentList.add(getSegment(route.getRtept()));
                }
            }
        }
        if(input.getTrk() != null){
            segmentList.addAll(processTrack(input.getTrk()));
        }
        return segmentList;
    }

    @Override
    public Class[] getClasses() {
        return new Class[0];
    }
    
    private GPSSegment getSegment(List<WptType> wayPoints){
        GPSSegment segment = new GPSSegment();
        for(WptType point : wayPoints){
            GPSPoint p = new GPSPoint(
                    point.getLat().doubleValue(),
                    point.getLon().doubleValue());
            segment.add(p);
        }
        return segment;
    }
    
    private List<GPSSegment> processTrack(List<TrkType> tracks){
        List<GPSSegment> list = new LinkedList<>();
        if(tracks == null) return list;
        for(TrkType track : tracks){
            if(track != null){
                for(TrksegType trackSegment : track.getTrkseg()){
                    if(trackSegment != null){
                        list.add(getSegment(trackSegment.getTrkpt()));
                    }
                }
            }
        }
        return list;
    }
}
