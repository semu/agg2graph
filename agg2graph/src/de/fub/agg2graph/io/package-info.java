/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import de.fub.agg2graph.input.GPXReader;

/**
 * <p>This package takes care of importing and exporting data.
 * It is designed to be as flexible and easy to use as possible, but
 * well. 
 * Flexibility and Usability are sometimes contrary goals.</p>
 * <p>
 * It uses JAXB for the import and export.
 * JAXB is a library wich comes with two compilers:
 * </p>
 * <ul>
 * <li>One compiles a XML-schema to a java class</li>
 * <li>The other one can compile a java class to a XML-schema</li>
 * </ul>
 * <p>
 * The usecase is:
 * You want to create a new app, that does fancy stuff with GPS-data.
 * The GPX-format allows extensions that allow you to annotate tracks, points
 * or the complete dataset at all.
 * </p>
 * <p>
 * After you thought about some additional information you need for a point,
 * you want to save these additional informations and perhaps read it.
 * But how you gonna do that?
 * If you think of writing your own parser for your GPX-files, you might get
 * some pain in the ass.
 * Well, the older version of the GPX-import was quite that pain in the ass.
 * Look at the {@link GPXReader} class, that used a SAX-based parser, wich made
 * it difficult to extend that parser.
 * This parser just omitted all information, that was not needed for agg2graph,
 * but it could still be usefull to you.
 * If you want that information, write your own parser or use JAXB.
 * </p>
 * <h2>How to use this package</h2>
 * <p>Let's take an example: </p>
 * <p>You want to annotate each track with informations about the transport 
 * vehicle. 
 * GPX allows you to put a {@code <extensions>} tag on a arbitrary position
 * in the GPX-document.
 * So, you do that.
 * {@code
 * <trk>
 *  <trkseg>
 *  <!-- lots of segments -->
 *  </trkseg>
 *  <extensions>
 *      <my:vehicle>bus</my:vehicle>
 *  </extensions>
 * </trk>
 * }<br />
 * Now you just create a XML-schema with the information about your tag.
 * If you don't know, how to create one, look <a href="http://www.w3schools.com/schema/default.asp"> here </a>.
 * </p>
 * <p>
 * After creating that schema, use xjc (that tool is included in java since 
 * java6, so it must be in your $JAVA_HOME/bin folder) to create the classes:
 * </p>
 * <p>{@code $> xjc -p de.fub.agg2graph.schemas.vehicles -d gen-src vehicle.xsd}
 * </p>
 * <p>
 * You got the classes! 
 * Import them as source into your IDE (NetBeans, eclipse, whatever...)
 * In this new package, there is an ObjectFactory-class.
 * Add that class to the classes array in the {@link FileImport} class or
 * pass it as argument in the constructor.
 * </p>
 * <p>
 * Done. Now, the {@link FileImport} class can also read your extensions.
 * </p>
 * 
 */
