/*
 * Copyright 2014 Christian Windolf christianwindolf@web.de, Sebastian Müller.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import de.fub.agg2graph.schemas.gpx.GpxType;
import java.io.File;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;



/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class FileExport {
    
    public static final Class[] defaultClasses = new Class[]{
        de.fub.agg2graph.schemas.gpx.ObjectFactory.class
    };
    
    
    private JAXBContext jContext;
    
    Class[] classes;
    
    private Marshaller m;
    
    public FileExport() throws JAXBException{
        classes = defaultClasses;
        jContext = JAXBContext.newInstance(classes);
        m = jContext.createMarshaller();
    }
    
    public FileExport(Class ... classes) throws JAXBException{
        this.classes = new Class[classes.length + defaultClasses.length];
        
        for(int i = 0; i < defaultClasses.length; i++){
            this.classes[i] = defaultClasses[i];
        }
        
        for(int i = defaultClasses.length; i < this.classes.length; i++){
            this.classes[i] = classes[i - defaultClasses.length];
        }
        
        jContext = JAXBContext.newInstance(this.classes);
        m = jContext.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    }
    
    public void export(File f, GpxType data) throws JAXBException, IOException{
        
        if(!f.exists()){
            f.createNewFile();
        }
        de.fub.agg2graph.schemas.gpx.ObjectFactory factory =
                new de.fub.agg2graph.schemas.gpx.ObjectFactory();
        JAXBElement<GpxType> rootElement = factory.createGpx(data);
        
        m.marshal(rootElement, f);
    }

}
