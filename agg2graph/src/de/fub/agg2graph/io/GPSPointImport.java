/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import de.fub.agg2graph.schemas.gpx.*;
import de.fub.agg2graph.structs.GPSPoint;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class GPSPointImport implements Converter<List<GPSPoint>>{

    @Override
    public List<GPSPoint> convert(GpxType input) {
        List<GPSPoint> list = extractFromWPT(input.getWpt());
        list.addAll(extractFromRTE(input.getRte()));
        list.addAll(extractFromTrk(input.getTrk()));
        return list;
        
    }

    @Override
    public Class[] getClasses() {
        return new Class[0];
    }
    
    private List<GPSPoint> extractFromWPT(List<WptType> pointList){
        List<GPSPoint> list = new LinkedList<>();
        for(WptType wayPoint : pointList){
            GPSPoint p = new GPSPoint(
                    wayPoint.getLat().doubleValue(), 
                    wayPoint.getLon().doubleValue());
            list.add(p);
        }
        return list;
    }
    
    private List<GPSPoint> extractFromRTE(List<RteType> routeList){
        List<GPSPoint> list = new LinkedList<>();
        for(RteType route : routeList){
            list.addAll(extractFromWPT(route.getRtept()));
        }
        return list;
    }
    
    private List<GPSPoint> extractFromTrk(List<TrkType> trackList){
        List<GPSPoint> list = new LinkedList<>();
        for(TrkType track : trackList){
            
            for(TrksegType segment : track.getTrkseg()){
                list.addAll(extractFromWPT(segment.getTrkpt()));
            }
        }
        return list;
    }
    
    
    
}
