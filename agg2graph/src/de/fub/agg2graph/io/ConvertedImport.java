/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import de.fub.agg2graph.schemas.gpx.GpxType;
import java.io.File;
import java.util.concurrent.Callable;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class ConvertedImport<T> implements Callable<T> {
    private final Converter<T> converter;
    private final FileImport fi;
    private File f;
    
    public ConvertedImport(Converter<T> c, File f) throws JAXBException{
        this.converter = c;
        fi = new FileImport(c.getClasses());
        this.f = f;
    }
    
    public ConvertedImport(Converter<T> c) throws JAXBException{
        this.converter = c;
        fi = new FileImport(c.getClasses());
        
    }
    
    public void setFile(File f){
        this.f = f;
    }

    @Override
    public T call() throws Exception {
        if(f == null){
            throw new IllegalStateException("You must set a file first "
                    + "before you can invoke call()");
        }
        if(!f.exists()){
            throw new IllegalArgumentException("The file " + f.getAbsolutePath() 
                    + " does not exist");
        }
        GpxType gpx = fi.parseFile(f);
        return converter.convert(gpx);
        
    }
}
