/*
 * Copyright 2013 Christian Windolf christianwindolf@web.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import org.apache.log4j.Logger;

/**
 *
 * @author Christian Windolf christianwindolf@web.de
 */
public class ErrorLogger implements ValidationEventHandler {

    private Logger log = Logger.getLogger(ErrorLogger.class);

    @Override
    public boolean handleEvent(ValidationEvent ve) {
        log.warn("Error while parsing: " + ve.getMessage(),
                ve.getLinkedException());
        return true;
    }
}