/*
 * Copyright 2014 Christian Windolf christianwindolf@web.de, Sebastian Müller.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fub.agg2graph.io;

import de.fub.agg2graph.schemas.gpx.GpxType;
import java.io.File;
import java.nio.file.Path;
import javax.xml.bind.*;
import org.apache.log4j.Logger;

/**
 *
 * <p>Takes care of importing GPX-files</p>
 * <p>It uses JAXB to do that. JAXB needs a class to import</p>
 * @author Christian Windolf christianwindolf@web.de
 */
public class FileImport {

    private static Logger log = Logger.getLogger(FileImport.class);
    public final static Class[] defaultClasses = new Class[]{
            de.fub.agg2graph.schemas.gpx.ObjectFactory.class,
            de.fub.agg2graph.schemas.josm.ObjectFactory.class
        };
    private Class[] classes;
    
    private JAXBContext jContext;
    
    private Unmarshaller u;

    public FileImport() throws JAXBException {
        classes = defaultClasses;
        jContext = JAXBContext.newInstance(classes);
        u = jContext.createUnmarshaller();
        u.setEventHandler(new ErrorLogger());
    }
    
    public FileImport(Class ... classes) throws JAXBException{
        this.classes = new Class[classes.length + defaultClasses.length];
        for(int i = 0; i < defaultClasses.length; i++){
            this.classes[i] = defaultClasses[i];
        }
        for(int i = defaultClasses.length; i < this.classes.length; i++){
            this.classes[i] = classes[i - defaultClasses.length];
        }
        
        jContext = JAXBContext.newInstance(this.classes);
        u = jContext.createUnmarshaller();
        u.setEventHandler(new ErrorLogger());
    }
    
    public GpxType parseFile(File f) throws JAXBException{
        JAXBElement e = (JAXBElement) u.unmarshal(f);
        return (GpxType) e.getValue();
    }
    
    public GpxType parseFile(Path p) throws JAXBException{
        JAXBElement e = (JAXBElement) u.unmarshal(p.toFile());
        return (GpxType) e.getValue();
        
    }
    
    
}
