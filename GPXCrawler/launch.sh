#!/bin/bash

case "$1" in
"1")
	echo "launching configuration 1"
	java -jar GPXCrawler.jar -d 1 -k jpjqggfbhtyyrxyr -b 10.0,51.0,11.2,52.3 -r dl/gpsies -t jogging
    ;;
"2")
	echo "launching configuration 2"
	java -jar GPXCrawler.jar -d 1 -k jpjqggfbhtyyrxyr -b 10.0,51.0,11.2,52.3 -f csv -r dl/gpsies -t walking
    ;;
"3")
	echo "launching configuration 3"
	java -jar GPXCrawler.jar -d 2 -u gpxTester -p gpxcrawl -r dl/mmt -c 10
    ;;
"4")
	echo "launching configuration 4"
	java -jar GPXCrawler.jar -d 2 -u gpxTester -p gpxcrawl -f csv -r dl/mmt -c 15
    ;;
"5")
	echo "launching configuration 5"
	java -jar GPXCrawler.jar -d 3 -b 10.0,51.0,10.2,51.5 -r dl/osm
    ;;
"6")
	echo "launching configuration 6"
	java -jar GPXCrawler.jar -d 3 -b 13.0,52.4,13.2,52.6 -f csv -r dl/osm
    ;;

*)
    
    ;;
esac
